/* iosScLib.h - real time process IO system library header file */

/* Copyright 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,21oct03,dat  written, from rtpLib.h, v01i
*/

#ifndef __INCiosScLibh
#define __INCiosScLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "vxWorks.h"
#include "rtpLibCommon.h"

/* defines */

/* externs */

#ifndef	_ASMLANGUAGE

/* function declarations */

extern STATUS 	iosScLibInit (void);

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCiosScLibh */
