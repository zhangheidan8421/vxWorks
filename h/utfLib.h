/* utfLib.h - Unicode encoding library header file */

/* Copyright 2004 Wind River Systems, Inc. */

#ifndef __INCutfLib
#define __INCutfLib

#ifdef __cplusplus
extern "C" {
#endif

/* Constants */
#define	UTF16_TO_8_MULT	3	/* UTF-16 string of length N (2N bytes long)
				   can require up to 3N bytes in UTF-8 format,
				   always not including terminating '\0's */

/* Error Codes */

#define UC_BUFFER 	-1	/* Target buffer cannot hold result */
#define UC_FORMAT	-2	/* Source is in illegal format */
#define UC_NOSRC	-3	/* Insufficient source for legal conversion */

int utf8ToCP(
   const unsigned char *utf8,
   const int length,
   unsigned long *codePoint);

int utf16ToCP(
   const unsigned short *utf16,
   const int length,		/* Length is in shorts */
   const int littleEndian,
   unsigned long *codePoint);

int CPToUtf8(
   const unsigned long codePoint,
   unsigned char *utf8,
   const int length);

int CPToUtf16(
   const unsigned long codePoint,
   unsigned short *utf16,
   const int length,		/* Length is in shorts */
   const int littleEndian);


int utflen8(const unsigned char *utf8);

int utflen16(const unsigned short *utf16);

int proofUtf8String(const unsigned char *utf8);

int utf8ToUtf16String(
    const unsigned char *utf8,
    unsigned short *utf16,
    const int len16,
    int littleEndian);

int utf16ToUtf8String(
    const unsigned short *utf16,
    int littleEndian,
    unsigned char *utf8,
    const int len8);

int utf8ToUtf16StringBOM(
    const unsigned char *utf8,
    unsigned short *utf16,
    const int len16,
    int littleEndian);

int utf16ToUtf8StringBOM(
    const unsigned short *utf16,
    unsigned char *utf8,
    const int len8);

#ifdef __cplusplus
}
#endif

#endif
