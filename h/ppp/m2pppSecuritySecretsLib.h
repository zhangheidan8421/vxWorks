/* m2pppSecuritySecretsLib.h - SNMP pppSecuritySecretsTable library header */

/* Copyright 2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01f,01jun04,ijm add prototypes for m2pppSecretsEntryDelete and
                m2pppSecretsEntryAgentDelete, SPR#90565
01e,28may02,rvr fixed build warnings (teamf1)
01d,14feb00,adb  WRS coding conventions
01c,11feb00,adb  added MAX_LINK and MAX_ID_INDEX
01b,11feb00,adb  minor naming changes.
01a,30jan00,abd  created.
*/

#ifndef __INCm2pppSecuritySecretsLibh
#define  __INCm2pppSecuritySecretsLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "avlLib.h"

/* defines */

#define MAX_LINK 0x7fffffff /* following MIB RFCs specification */
#define INVALID_LINK ~0 

#define MAX_ID_INDEX 0x7fffffff /* following MIB RFCs specification */
#define INVALID_ID_INDEX ~0

/* enums */

typedef enum pppSecurityDirection
    {
    PPP_SECURITY_SECRETS_DIRECTION_UNDEFINED = 0,
    PPP_SECURITY_SECRETS_DIRECTION_LOCAL_TO_REMOTE = 1,
    PPP_SECURITY_SECRETS_DIRECTION_REMOTE_TO_LOCAL = 2
    }   PPP_SECURITY_SECRETS_DIRECTION;  

typedef enum pppSecurityProtocol
    {   
    PPP_SECURITY_UNDEFINED_PROTOCOL = 0,
    PPP_SECURITY_PAP_PROTOCOL = 1,
    PPP_SECURITY_CHAP_MD5_PROTOCOL = 2
    } PPP_SECURITY_PROTOCOL;

typedef enum pppSecurityStatus
    {
    PPP_SECURITY_SECRETS_STATUS_UNDEFINED = 0,
    PPP_SECURITY_SECRETS_STATUS_INVALID = 1,
    PPP_SECURITY_SECRETS_STATUS_VALID = 2
    }   PPP_SECURITY_SECRETS_STATUS;  

/* typedefs */

typedef struct pppSecuritySecretsDataEntry
    /* 
     * This would be the node of the auxiliary tree on which the actual
     * data of a pppSecuritySecretsEntry are stored. The index for this
     * tree will be the ordered quadraple (Link, Direction, Protocol, Identity).
     * Notice that the implementation of node addition and deletion
     * is responsible for deleting a node from both trees atomically
     * and for adding a node to both trees atomically. Hence, implicitly
     * we enforce uniqueness with respect to both main and auxiliary keys.
     */
    {
    AVL_NODE                        avlBase;
    UINT32                          pppSecuritySecretsLink;
                                    /* 
                                     * (0..2^(31)-1)
                                     * 0 is reserved for default choices
                                     */
    UINT32                          pppSecuritySecretsIdIndex;
                                    /* 
                                     * (0..2^(31)-1)
                                     * (Link, IdIndex) indexes the table 
                                     */
    PPP_SECURITY_SECRETS_DIRECTION  pppSecuritySecretsDirection;
    PPP_SECURITY_PROTOCOL           pppSecuritySecretsProtocol;
    char *                          pppSecuritySecretsIdentity;
    char *                          pppSecuritySecretsSecret;
    PPP_SECURITY_SECRETS_STATUS     pppSecuritySecretsStatus;
    }   PPP_SECURITY_SECRETS_DATA_ENTRY;

typedef struct pppSecuritySecretsEntry
    {
    AVL_NODE                            avlBase;
    PPP_SECURITY_SECRETS_DATA_ENTRY *   pSecretsDataEntry;   
                                        /* 
                                         * it points to the auxiliary tree
                                         * node on which the actual data of a
                                         * pppSecuritySecretsEntry are stored.
                                        */
    }   PPP_SECURITY_SECRETS_ENTRY;

/* function declarations */

extern STATUS m2pppSecuritySecretsLibInit(void);
extern STATUS m2pppSecuritySecretsLibClear(void);

/* pppSecuritySecretsData manipulation functions */

extern STATUS m2pppSecretsEntrySet
    (
    UINT32                          link, 
    PPP_SECURITY_SECRETS_DIRECTION  direction,
    PPP_SECURITY_PROTOCOL           protocol,
    char *                          identity,
    char *                          secret,
    PPP_SECURITY_SECRETS_STATUS     status
    );

extern STATUS m2pppSecretsSecretGet
    /* 
     * this routine receives as input the data composing the auxiliary key
     * (link, direction, protocol, identity) and a buffer in which, if an entry
     * with the given key exists, the corresponding secret will be written.
     */
    (
    UINT32                          link, 
    PPP_SECURITY_SECRETS_DIRECTION  direction,
    PPP_SECURITY_PROTOCOL           protocol,
    char *                          identity,
    char *                          secret
    );

/* pppSecuritySecrets manipulation functions */

extern STATUS m2pppSecretsEntryAgentSet
    (
    UINT32                              link, 
    UINT32                              idIndex,
    PPP_SECURITY_SECRETS_DIRECTION *    pDirection,
    PPP_SECURITY_PROTOCOL *             pProtocol,
    char *                              identity,
    char *                              secret,
    PPP_SECURITY_SECRETS_STATUS         status
    );

extern STATUS m2pppSecretsEntryAgentLookup
    (
    /* 
     * This and the next Get routines receive as input the SNMP indexing key
     * (link, idIndex) and if successful they write the appropriate data in the 
     * places pointed by pDirection, pProtocol, identity, secret, and pStatus.
     */
    UINT32                              link, 
    UINT32                              idIndex, 
    PPP_SECURITY_SECRETS_DIRECTION *    pDirection,
    PPP_SECURITY_PROTOCOL *             pProtocol,
    char *                              identity,
    char *                              secret,
    PPP_SECURITY_SECRETS_STATUS *       pStatus
    );

extern STATUS m2pppNextSecretsEntryAgentLookup
    (
    UINT32                              SNMPcompc,
    UINT32 *                            pLink, 
    UINT32 *                            pIdIndex, 
    /* after a successful call the Link and IdIndex of the 
     * lexicographically successor entry will be placed in 
     * the positions pointed by pLink and pIdIndex
     */
    PPP_SECURITY_SECRETS_DIRECTION *    pDirection,
    PPP_SECURITY_PROTOCOL *             pProtocol,
    char *                              identity,
    char *                              secret,
    PPP_SECURITY_SECRETS_STATUS *       pStatus
    );

extern STATUS m2pppSecretsEntryDelete
    (
    UINT32                          link,      /* pppSecuritySecretsLink */
    PPP_SECURITY_SECRETS_DIRECTION  direction, /* pppSecuritySecretsDirection */
    PPP_SECURITY_PROTOCOL           protocol,  /* pppSecuritySecretsProtocol */
    char *                          identity   /* pppSecuritySecretsIdentity */
    );

extern STATUS m2pppSecretsEntryAgentDelete
    (
    UINT32  link,   /* pppSecuritySecretsLink part of input key */
    UINT32  idIndex /* pppSecuritySecretsIdIndex part of input key */
    );

STATUS m2pppSecurityLocalSecretsShow(void);
STATUS m2pppSecurityPeerSecretsShow(void);

#ifdef __cplusplus
}
#endif

#endif /*  __INCm2pppSecurityLibh */
