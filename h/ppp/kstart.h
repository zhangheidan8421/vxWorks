/* kstart.h - basic definitions and enums */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01f,11dec02,ijm changed DWORD to PPPWORD to resolve name conflict with vxSim
01e,25aug01,ijm changed WORD to PPPWORD to resolve name conflict with DHCP
01d,29sep00,sj  merging with TOR2_0-WINDNET_PPP-CUM_PATCH_2
01c,01aug00,adb  Merging with openstack view
01b,10jul00,md  fixed type redefinition
01a,30sep99,sj 	derived from routerware source base
*/

/*
 * $Log:: /Rtrware/include/kstart.h                                                             $
 * 
 * 6     1/06/99 2:06p Nishit
 * Definition of PARAMETER_NOT_USED in parenthesis to keep lint happy
 * 
 * 5     12/04/98 2:19p Rajive
 * Removed pragma warning.
 * 
 * 4     4/30/98 1:16p Rajive
 * Overwrote with INCLUDE v4.2.1
 * 
 * 3     4/27/98 11:42a Release Engineer
 * Additions into include for L2TP and RTM modules.
 * INCLUDE v4.2.1
 * 
 * 1     2/13/98 12:18a Release Engineer
 * code cleanup, code style changes, linted, system level test
 * INCLUDE v4.2.0
 * 
 * 3     9/09/96 5:10p Ross
 * Changes for spanning tree and token ring
 * 
 * 2     3/27/96 7:08p Ross
 * Initial check-in with source safe.
*/
/*	$Modname: kstart.h$  $version: 1.4$      $date: 11/12/92$   */
/*
* 	$lgb$
1.0 03/06/92 ross
1.1 03/20/92 ross Delete some useless #defines and enums
1.2 03/20/92 ross added version control header
1.3 06/11/92 ross added GNU to #define interrupt to nada
1.4 11/12/92 ross added USHORT and ULONG support.
* 	$lge$
*/

#include  "vxWorks.h"

#ifndef __INCkstarth
#define __INCkstarth

#define  IN
#define  OUT
#define BOOLEAN BOOL
#define PARAMETER_NOT_USED(parameter_one) (parameter_one = parameter_one)

#if !defined __RW_UTIL_H__
typedef unsigned char BYTE;
#endif /* __RW_UTIL_H__ */

typedef unsigned char U08;

typedef unsigned short PPPWORD; 
typedef unsigned short U16;

typedef unsigned long PPPDWORD;
typedef unsigned long U32; 


#define ULONG_ENUM(enum_name) ULONG
#define USHORT_ENUM(enum_name) USHORT
#define BYTE_ENUM(enum_name) BYTE

#if !defined __RW_UTIL_H__
typedef enum  { PASS = 1, FAIL = 0} TEST;

typedef enum 
    {
    LESS_THAN = -1,
    EQUAL_TO = 0,
    GREATER_THAN = 1
    }SORT_RETURN;

#define	STRINGS_MATCH ((int) 0)
#endif /* __RW_UTIL_H__ */

#if defined (GLOBAL_FILE)
	#define	GLOBAL	
#else
	#define	GLOBAL	extern
#endif

/* nibble - dword macros */

#define high_byte_of_ulong(double_word)     (((double_word) >> 24) & 0x000000ff)

#define high_low_byte_of_ulong(double_word) (((double_word) >> 16) & 0x000000ff)

#define low_high_of_ulong(double_word)      (((double_word) >> 8) & 0x000000ff) 

#define low_byte_of_ulong(double_word)      ((double_word) & 0x000000ff) 

#define high_byte_of_ushort(ushort)         (((ushort)>>8)&0x00ff) 

#define low_byte_of_ushort(ushort)          ((ushort)&0x00ff) 

#define low_ushort_of_ulong(double_ushort)  (PPPWORD)(((double_ushort) >> 16) & 0x0000ffff)

#define high_ushort_of_ulong(double_ushort) (PPPWORD)((double_ushort) & 0x0000ffff)

/* Combine high byte and low byte */
#if !defined __RW_UTIL_H__
#define two_bytes_to_ushort(high_byte,low_byte) ((((high_byte)&0x00ff)<<8) |\
						    ((low_byte)&0x00ff))
#endif /* __RW_UTIL_H__ */

#define high_byte_of_dword(double_word)     (((double_word) >> 24) & 0x000000ff)

#define high_low_byte_of_dword(double_word) (((double_word) >> 16) & 0x000000ff)

#define low_high_of_dword(double_word)      (((double_word) >> 8) & 0x000000ff) 

#define low_byte_of_dword(double_word)      ((double_word) & 0x000000ff) 

#define low_word_of_dword(double_word)      (PPPWORD)((double_word) & 0x0000ffff) 

#define high_word_of_dword(double_word)     (PPPWORD)(((double_word) >> 16) &\
								    0x0000ffff) 

#define high_byte_of_word(word)             (((word)>>8)&0x00ff) 
#define low_byte_of_word(word)              ((word)&0x00ff) 

  /* Combine high byte and low byte */

#define two_bytes_to_word(high_byte,low_byte) ((((high_byte) & 0x00ff)<<8)|\
						((low_byte)&0x00ff))

#define high_nibble_of_byte(byte)           (((byte)>>4)&0x000f) 

#define low_nibble_of_byte(byte)            ((byte)&0x000f) 

#define two_nibbles_to_byte(a,b)            ((((a)&0x000f)<<4)|((b)&0x000f))

#if !defined (VARIABLE_NUMBER_OF_BYTES)
	#define VARIABLE_NUMBER_OF_BYTES 1
#endif

#endif /* __INCkstarth */
