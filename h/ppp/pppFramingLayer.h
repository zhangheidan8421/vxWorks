/* pppFramingLayer.h - Framing Layer header file */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01b,13jan02,ijm added extern "C" for C++
01a,08nov99,sj  Written
*/

#ifndef __INCpppFramingLayerh
#define __INCpppFramingLayerh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "pfw/pfw.h"

STATUS pppFramingLayerCreate (PFW_OBJ * pfw);
STATUS pppFramingLayerDelete (PFW_OBJ * pfw);

#ifdef __cplusplus
}
#endif

#endif /* __INCpppFramingLayerh */
