/* mpAPI.h - MP user interfaces functions include file */
 
/* Copyright 1999 Wind River Systems, Inc. */
 
#include "copyright_wrs.h"
 
/*
modification history
--------------------
01b,18dec01,ak	support for dynamic assignment of MP Member Stack to Manager 
				Stack

01a,21feb01,as  created
*/

#ifndef __INCmpAPIh
#define  __INCmpAPIh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "pfw/pfw.h"
#include "pfw/pfwStack.h"
#include "ppp/mp.h"
#include "pfw/pfwProfile.h"

/* externs */

extern PFW_STACK_OBJ * mpBundleCreate 
   	(
	PFW_PROFILE_OBJ *pProfileObj,		/* profile object */	
	PFW_STACK_OBJ_CALLBACKS * callbacks,	/* Callback function for pfwStackAdd ()*/
	void					*userHandle 
	);

extern STATUS mpBundleClose
	(
	PFW_STACK_OBJ *pManagerStackObj		/* manger stack object */
	);

extern STATUS mpBundleDelete
	(
	PFW_STACK_OBJ *pManagerStackObj		/* manger stack object */
	);

extern STATUS apiMpLinkAssign
	(
	PFW_STACK_OBJ *pMemberStackObj,		/* member stack object */
	PFW_STACK_OBJ *pManagerStackObj		/* manger stack object */
	);	

extern PFW_STACK_OBJ * mpLinkAdd 
	(
	PFW_PROFILE_OBJ 		*pProfileObj,	/* profile for member stack */
	PFW_STACK_OBJ_CALLBACKS * callbacks,	/* Callback function for 
											   pfwStackAdd ()*/
	void					*userHandle 

	);	

extern STATUS mpBundleListShow 
	(
	PFW_OBJ * pfwObj					/* framework object */
	);

extern STATUS mpMemberList 
	(
	PFW_STACK_OBJ	*managerStackObj	/* manager stack object */
	);

extern TEST mpFindAPortToBundleMapping
	(
	PFW_STACK_OBJ	*pMemberStackObj,	/* member stack object */
	PFW_STACK_OBJ	**pManagerStackObj  /* manager stack object */
	); 

extern TEST mpSetPortToBundleMapping
	(
	PFW_STACK_OBJ	*pMemberStackObj,	/* member stack object */
	PFW_STACK_OBJ	*pManagerStackObj  	/* manager stack object */
	);

extern MP_BUNDLE_STATE mpGetBundleState
	(
	PFW_STACK_OBJ	*pManagerStackObj	/* manager stack object */
	);

extern int mpGetNoOfLinksInTheBundle
	(
	PFW_STACK_OBJ	*pManagerStackObj	/* manager stack object */
	);

extern int mpGetNoOfLinksInTheSendingEndOfTheBundle
	(
	PFW_STACK_OBJ	*pManagerStackObj	/* manager stack object */
	);

extern int mpGetNoOfLinksInTheReceivingEndOfTheBundle
	(
	PFW_STACK_OBJ	*pManagerStackObj	/* manager stack object */
	);

extern USHORT mpGetNoOfBundles
	(
	PFW_OBJ		*pfwObj					/* framework object */
	);

extern STATUS mpIsLinkInSendingEnd 		
	(
	PFW_STACK_OBJ	*pMemberStackObj,	/* member stack object */
	PFW_STACK_OBJ	**pManagerStackObj  /* manager stack object */
	);

extern STATUS mpIsLinkInReceivingEnd 		
	(
	PFW_STACK_OBJ	*pMemberStackObj,	/* member stack object */
	PFW_STACK_OBJ	**pManagerStackObj  /* manager stack object */
	);

extern PFW_STACK_OBJ *mpManagerStackObjForBundleIdGet
	(
	PFW_OBJ					*pfwObj, 	/* framework obj pointer */
	MP_BUNDLE_IDENTIFIER	*id		 	/* bundle identifier 	 */
	);	

extern BOOL isNoEidNoAuthCase 
	(
	MP_BUNDLE_IDENTIFIER	*id			/* bundle identifier     */
	);

extern STATUS mpRemovePortToBundleMapping (PFW_STACK_OBJ *); 

extern STATUS mpIsLinkInReceivingEnd (PFW_STACK_OBJ *, PFW_STACK_OBJ **);  

extern STATUS mpIsLinkInSendingEnd (PFW_STACK_OBJ *, PFW_STACK_OBJ **); 

extern PFW_STACK_OBJ * mpGetManagerStackObj (UINT, PFW_OBJ *); 


#ifdef __cplusplus
}
#endif

#endif /*  __INCmpAPIh */
