/* pppSioAdapter.h - PPP Serial I/O Adapter */

/* Copyright 1984-1999 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,29oct02,ijm updated sio peer types
01b,13jan02,ijm added prototypes for sioAdapterCreate and sioAdapterDelete
01b,17dec00,sj  added sio_maxSendQSize parameter
01a,08nov99,sj 	created
*/

#ifndef __INCpppSioAdapterh
#define __INCpppSioAdapterh

#ifdef __cplusplus
extern "C" {
#endif

/*                    Profile Parameters
 *                   --------------------
 *        NAME                              VALUES/EXPLANATION
 *       ------                            --------------------
 * "sio_channelNum"           Serial port/channel number as would be used
 *                            with  sysSerialChanGet(); e.g: "1", "2"
 * "sio_baudRate"             Baud Rate for the specified channel: e.g. "19200"
 *
 * "sio_maxSendQSize"         Specifies the maximum number of frames that may
 *                            be queued for transmission. Default value is "15" 
 *                            Set a value of "0" to disable queueing.
 *
 * "sio_peerType"             This parameter identifies that the peer is:
 *                            Windows RAS:  "WIN_SERVER"
 *                            Windows DUN: "WIN_CLIENT"
 *                            Modem: MODEM
 *                            All others (LINUX, Solaris, vxWorks...): "REGULAR"
 *
 *                            In the case of Windows the adapter does
 *                            not come up until the exchange of strings "CLIENT"
 *                            and "CLIENTSERVER" is complete
 */

/* User interfaces */

extern STATUS sioAdapterCreate (PFW_OBJ * pfw, UINT32 totalNumberOfChannels);
extern STATUS sioAdapterDelete (PFW_OBJ * pfw);

#ifdef __cplusplus
}
#endif

#endif /* __INCpppSioAdapterh */
