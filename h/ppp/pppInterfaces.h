/* pppInterface.h - PPP user interfaces */
 
/* Copyright 1999 - 2005 Wind River Systems, Inc. */
 
#include "copyright_wrs.h"
 
/*
modification history
--------------------
01f,23feb05,ijm added LCP stopped callback with echos failed notification,
                SPR#70584
01e,17feb02,ak	added MP upcalls 
01d,09feb00,sj  added pppLinkIdGet() + deleted old secrets interfaces
01c,05jan00,sj  adding callbackOperation upcalls
01b,18nov99,sj  updating documentation
01a,30oct99,sj  created
*/
 
#include "ppp/kstart.h"
#include "ppp/kppp.h"
/*#include "ppp/vpppstr.h"*/
#include "ppp/mp.h" 
#include "pfw/pfw.h"
 
#ifndef __INCpppInterfaceh
#define  __INCpppInterfaceh

#ifdef __cplusplus
extern "C" {
#endif

/* typedefs */

typedef void (*PASSWORD_AUTH_CALLBACK)
    (
    PFW_STACK_OBJ *stackObj,
    BYTE id,
    TEST passOrFail
    );

typedef void (*CHALLENGE_AUTH_CALLBACK)
    (
    PFW_STACK_OBJ *stackObj,
    BYTE id,
    TEST passOrFail
    );
/*
 * For each of the callback function pointers defined within the
 * structures "lcpUpcallFunctions", "ncpUpcallFunctions", "authUpcallFunctions"
 * and "pppUpcallFunctions" the 'userHandle' parameter is the cookie installed
 * by the user/application at the time of 'pppConnectionOpen.'
 * The 'stackObj' parameter refers to the stack (created using pfwStackAdd())
 * for which the callback is being made
 *
 * The application may provide some,all or none of these callbacks
 */

/*
 * sub upcall interface for communication between Link Control protocol and the
 * application.
 */
typedef struct lcpUpcallFunctions
    {
    void (*pppLcpUp)                 /* This callback occurs when LCP is UP */
	(                            /* implying that the link establishment */
	void * userHandle,           /* phase is complete */
	PFW_STACK_OBJ *stackObj
	);
    void (*pppLcpDown)               /* This callback occurs when LCP is going*/
	(                            /* down */
	void * userHandle,           
	PFW_STACK_OBJ * stackObj
	);
    void (*pppLcpFinished)           /* This callback occurs when LCP is */
	(                            /* finished and is going to the CLOSED */
	void * userHandle,           /* state */
	PFW_STACK_OBJ * stackObj
	);
    void (*pppLcpTerminationRequest) /* This callback occurs when the LOCAL */
	(                            /* system is about to send a Termination */
	void * userHandle,           /* request to the PEER */
	PFW_STACK_OBJ * stackObj
	);
    void (*pppLcpIdRequest)          /* This callback occurs when the LOCAL */
	(                            /* system is about to send a LCP */
	void * userHandle,           /* identification message to the PEER. */
	PFW_STACK_OBJ * stackObj,    /* The application may fill in/modify the*/
        char * message               /* 'message' buffer upto a size equal to */
	);                           /* length of the string value set for LCP*/
	                             /* profile parameter: */
				     /* "lcp_txIdentificationMessage" */

    void (*pppLcpIdRequestReceived)  /* This callback is a notification to */
	(                            /* the application that the LOCAL system */
	void * userHandle,           /* received a LCP identification packet */
	PFW_STACK_OBJ * stackObj,    /* from the peer. The 'message' parameter*/
        char * message               /* points to the data field of received */
        );                           /* packet */

    void (*pppLcpTimeRemainingRequest) 
	(                            /* This callback occurs when the LOCAL */
	void * userHandle,           /* system is about to send a LCP */
	PFW_STACK_OBJ * stackObj,    /* Time Remaining packet to the peer. */
        ULONG * secondsRemaining,    /* The application may fill in the */
        char * message               /* 'secondsRemaining' and 'message' */
	);                           /* fields of the packet. The message */
				     /* cannot be longer than the length of */
				     /* the string value set for the LCP */
				     /* profile parameter: */
				     /* "lcp_timeRemainingMessage" */

    void (*pppLcpTimeRemainingReceived)
	(                            /* This callback is a notification to the*/
	void * userHandle,           /* application that the LOCAL system */
	PFW_STACK_OBJ * stackObj,    /* received a TimeRemaining packet from */
        ULONG secondsRemaining,      /* the PEER. The 'secondsRemaining' and */
        char * message               /* the 'message' parameters contain the */
	);                           /* information sent by the PEER */

    void (*pppLcpCallbackOperation)
	(                            /* This callback is a notification to the*/
	void * userHandle,           /* application that the LOCAL system */
	PFW_STACK_OBJ * stackObj,    /* has negotiated to call back and */
        BYTE callbackOperation,      /* re-establish the link with the PEER */
        char * message               /* The 'callbackOperation' and 'message'*/
	);                           /* parameters have the relevant */
	                             /* information necessary for callback */

    void (*pppLcpStopped)         /* This callback occurs when LCP is */
        (                         /* going to the STOPPED STATE. */
        void * userHandle,        /* If LCP echoes failed, lcpEchoesFailed */  
        PFW_STACK_OBJ * stackObj, /* is set to TRUE */ 
        BOOL lcpEchoesFailed
        );
    }LCP_UPCALL_FUNCTIONS;
/*
 * sub upcall interface for communication between Network Control protocols and
 * the application 
 */
typedef struct ncpUpcallFunctions
    {
    void (*pppNcpUp)                 /* This callback notifies the application*/
	(                            /* that the Network Control Protocol */
	void * userHandle,           /* whose number is specified by the */
	PFW_STACK_OBJ * stackObj,    /* 'protocol' parameter has completed the*/
	PPP_PROTOCOL_TYPE protocol   /* negotiation phase and is UP */
	);
    void (*pppNcpDown)               /* This callback notifies the application*/
	(                            /* that the Network Control Protocol */
	void * userHandle,           /* whose number is specified by the */
	PFW_STACK_OBJ * stackObj,    /* 'protocol' parameter is going DOWN */
	PPP_PROTOCOL_TYPE protocol
	);
    void (*pppNcpFinished)           /* This callback notifies the application*/
	(                            /* that the Network Control Protocol */
	void * userHandle,           /* whose number is specified by the */
	PFW_STACK_OBJ * stackObj,    /* 'protocol' parameter is finished and */
	PPP_PROTOCOL_TYPE protocol   /* is going to the CLOSED state */
	);
    void (*pppNcpRejected)           /* This callback notifies the application*/
	(                            /* that the Network Control Protocol */
	void * userHandle,           /* whose number is specified by the */
	PFW_STACK_OBJ * stackObj,    /* 'protocol' parameter was rejected by */
	PPP_PROTOCOL_TYPE            /* the PEER */
	);
    } NCP_UPCALL_FUNCTIONS;

/*
 * sub upcall interface for communication between authentication protocols and
 * the application.
 *
 * Of the callback functions defined in the "authUpcallFunctions" structure
 * The first three; viz.,pppAuthRequest() pppChallengeAuthVerify() and 
 * pppPasswordAuthVerify() are received by the application only when the
 * CONTROL_LAYER profile parameter: "ppp_secretsDatabase" is set to the value
 * EXTERNAL_SECRETS_DATABASE.
 */
typedef struct authUpcallFunctions
    {
    void (*pppAuthRequest)           /* This callback occurs when the LOCAL */
	(                            /* system is about to send a */
	void * userHandle,           /* authentication request(PAP) OR a */
	PFW_STACK_OBJ * stackObj ,   /* response to a challenge. */
        char * userName,             /* The application MUST fill in the */
	char *userPassword           /* 'userPassword' for the specified */
	);                           /* 'userName'. */

    void (*pppChallengeAuthVerify)   /* This callback occurs when the LOCAL */
	(                            /* system is a CHAP authenticator(server)*/
	void * userHandle,           /* and it received a respose to its */
	PFW_STACK_OBJ * stackObj,    /* challenge from the PEER. */
	char * localName,            /* The application MUST use the */
	char * peerName,             /* information in these parameters to  */
	char * response,             /* verify the response using MD5  */
	unsigned int responseLen,    /* algorithm and send a PASS or FAIL  */
	char * challenge,            /* via the chapCallback. The 'id'  */
	unsigned int challengeLen,   /* parameter to the chapCallback MUST */
	BYTE id,                     /* match the 'id' parameter here. */
	CHALLENGE_AUTH_CALLBACK chapCallback
	);

    void (*pppPasswordAuthVerify)    /* This callback occurs when the LOCAL */
	(                            /* system is a PAP authenticator(server) */
	void * userHandle,           /* and it received a authentication */
	PFW_STACK_OBJ *stackObj,     /* request from the PEER */
	char * localName,            /* The application must use the */
	char * peerName,             /* information provided in these */
	char *userPassword,          /* parameters to verify the */
	BYTE id,                     /* authentication request and send a */
	                             /* PASS or FAIL message via the */
	                             /* papCallback */
	PASSWORD_AUTH_CALLBACK papCallback
	);

    void (*pppAuthAckTransmitted)    /* This callback occurs when the LOCAL */
	(                            /* being the authenticator sends a  */
	void * userHandle,           /* authentication ACK(Success) message to*/
	PFW_STACK_OBJ * stackObj,    /* the PEER */
        PPP_PROTOCOL_TYPE authProtocol
	);
    void (*pppAuthAckReceived)       /* This callback occurs when the LOCAL */
	(                            /* is successfully authenticated by the */
	void * userHandle,           /* PEER */
	PFW_STACK_OBJ * stackObj, 
        PPP_PROTOCOL_TYPE authProtocol
	);
    void (*pppAuthFailed)            /* This callback occurs when the LOCAL */
	(                            /* system failed to authenticate itself */
	void * userHandle,           /* with the PEER */
	PFW_STACK_OBJ * stackObj,
        PPP_PROTOCOL_TYPE authProtocol
	);
    void (*pppAuthRefused)           /* This callback occurs when the LOCAL */
	(                            /* system being the authenticator refuses*/
	void * userHandle,           /* authentication for the peer */
	PFW_STACK_OBJ * stackObj,
        PPP_PROTOCOL_TYPE authProtocol
	);
    }AUTH_UPCALL_FUNCTIONS;

/*
 * sub upcall interface for communication between MLPPP components and
 * the application.
 */
typedef struct mpUpcallFunctions
	{
	void (*linkNegotiatedMP)					/* This callback occurs when */
	(											/* the link negotiates MRRU  */
	void					*userHandle,		/* option                    */
	PFW_STACK_OBJ			*pStackObj,
	MP_BUNDLE_IDENTIFIER	*bundleId
	);
	void (*linkAddedToBundle)
	(
	void 					*userHandler,		/* This callback occurs when */
	PFW_STACK_OBJ			*pStackObj,			/* the link is added to a    */
	PFW_STACK_OBJ			*pManagerStackObj   /* bundle                    */
	);
	void (*linkRemovedFromBundle)
	(
	void 					*userHandler,		/* This callback occurs when */
	PFW_STACK_OBJ			*pStackObj,			/* the link is removed to a  */
	PFW_STACK_OBJ			*pManagerStackObj	/* bundle                    */
	);
	}MP_UPCALL_FUNCTIONS;

/*
 * Aggregate Upcall interface between PPP and the Application
 */
typedef struct pppUpcallFunctions
    {
    void (*pppOpened)                /* The application receives this callback*/
	(                            /* when the connection is fully UP with */
	void * userHandle,           /* all the configured control protocols */
	PFW_STACK_OBJ * stackObj     /* up and running */
	);
    void (*pppClosed)                /* The application receives this callback*/
	(                            /* when all protocols that were part of */
	void * userHandle,           /* the open connection have been quiesced*/
	PFW_STACK_OBJ * stackObj
	);

    LCP_UPCALL_FUNCTIONS  * lcpUpcalls;
    NCP_UPCALL_FUNCTIONS  * ncpUpcalls;
    AUTH_UPCALL_FUNCTIONS * authUpcalls;
	MP_UPCALL_FUNCTIONS	  * mpUpcalls;
    } PPP_UPCALL_FUNCTIONS;

extern STATUS pppConnectionOpen       /* Open a PPP connection based on the */
    (                                 /* stack referenced by stackObj. The */
    PFW_STACK_OBJ *stackObj,          /* application may provide a reference */
    PPP_UPCALL_FUNCTIONS *upCalls,    /* to callback functions and a */
    void *userHandle                  /* userHandle that will be returned */
    );                                /* during each callback */


extern STATUS pppConnectionClose      /* Close a previously opened connection */
    (
    PFW_STACK_OBJ *stackObj
    );

extern int pppLinkIdGet               /* get the PPP link Id for this stack */
    (
    PFW_STACK_OBJ *stackObj
    );


#ifdef __cplusplus
}
#endif

#endif /*  __INCpppInterfaceh */
