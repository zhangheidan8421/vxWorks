/* m2pppLinkGroup.h - PPP SNMP agent interface library header */

/* Copyright 1999 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01c,24feb00,sj  adding MagicNumber Enabled/Disabled Macros
01b,23feb00,sj  added a few Enabled/Disable macros
01a,06jan00,sj   created.
*/

#include "vxWorks.h"
#include "pfw/pfw.h"
#include "pfw/pfwTable.h"
#include "pfw/pfwMemory.h"
#include "ppp/m2pppLib.h"
#include "ppp/interfaces/lcpInterfaces.h"

#ifndef __INCm2pppLinkGrouph
#define  __INCm2pppLinkGrouph

#ifdef __cplusplus
extern "C" {
#endif

/* defines for enumerated types as specified by RFC 1471 */

#define M2_PPP_OPTION_DISABLED 2

/* possible values for pppLinkStatusLocalToRemoteProtocolCompression */

#define M2_pppLinkStatusLocalToRemoteProtocolCompression_enabled   TRUE
#define M2_pppLinkStatusLocalToRemoteProtocolCompression_disabled  \
							M2_PPP_OPTION_DISABLED


/* possible values for pppLinkStatusRemoteToLocalProtocolCompression */

#define M2_pppLinkStatusRemoteToLocalProtocolCompression_enabled    TRUE
#define M2_pppLinkStatusRemoteToLocalProtocolCompression_disabled  \
							M2_PPP_OPTION_DISABLED

/* possible values for pppLinkStatusLocalToRemoteACCompression */

#define M2_pppLinkStatusLocalToRemoteACCompression_enabled 	TRUE
#define M2_pppLinkStatusLocalToRemoteACCompression_disabled  \
							M2_PPP_OPTION_DISABLED

/* possible values for  pppLinkStatusRemoteToLocalACCompression */

#define M2_pppLinkStatusRemoteToLocalACCompression_enabled	TRUE
#define M2_pppLinkStatusRemoteToLocalACCompression_disabled \
							M2_PPP_OPTION_DISABLED

/* possible values for  pppLinkConfigMagicNumber */

#define M2_pppLinkConfigMagicNumber_enabled	2
#define M2_pppLinkConfigMagicNumber_disabled	1

/* typedefs */

typedef struct PPP_LINK_CONFIG_ENTRY_DATA 
    {
    PFW_STACK_OBJ                   * stackObj;
    PFW_PLUGIN_OBJ_STATE            * state;
    PPP_LINK_CONFIG_ENTRY_INTERFACE * interface;
    }PPP_LINK_CONFIG_ENTRY_DATA;

typedef struct PPP_LINK_STATUS_ENTRY_DATA 
    {
    PFW_STACK_OBJ                   * stackObj;
    PFW_PLUGIN_OBJ_STATE            * state;
    PPP_LINK_STATUS_ENTRY_INTERFACE * interface;
    }PPP_LINK_STATUS_ENTRY_DATA;

extern STATUS m2pppLinkGroupRegister (PFW_OBJ * pfwObj);
extern STATUS m2pppLinkGroupUnregister (PFW_OBJ * pfwObj);
extern STATUS m2pppLinkStatusEntryLookup (UINT32 oidLen, UINT32 * oid,
					    PPP_LINK_STATUS_ENTRY_DATA * data,
					    M2_PPP_ENTRY_MATCH_TYPE match);

extern STATUS m2pppLinkConfigEntryLookup (UINT32 oidLen, UINT32 * oid,
					    PPP_LINK_CONFIG_ENTRY_DATA * data,
					    M2_PPP_ENTRY_MATCH_TYPE match);

#ifdef __cplusplus
}
#endif

#endif /*  __INCm2pppLinkGrouph */
