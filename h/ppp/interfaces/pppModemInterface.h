/* pppModemInterface.h - PPP framework modem interface header file */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01a,19apr00,cn   created.
*/


#ifndef __INCpppModemInterfaceh
#define __INCpppModemInterfaceh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "pfw/pfw.h"
#include "pfw/pfwInterface.h"
 
/* commands */

#define MODEM_FLUSH             0x1	/* flush internal buffers */
#define MODEM_SET_FLAGS         0x2	/* set modem flags */
#define MODEM_GET_FLAGS         0x4	/* get modem flags */

/* flags */

#define MODEM_UP             	0x1	/* modem is up: connection is usable */
#define MODEM_OPEN             	0x2	/* modem is open */
 
/*******************************************************************************
* PPP_MODEM_INTERFACE - PPP modem interface
*
* Interface Name: "PPP_MODEM_INTERFACE"
*
* This interface is published and made available by the SIO adapter component.
* Other components in the system use this interface to establish a dial-up
* connection to an external modem, to initialize the modem, or hang it up.
*
* The modemOpen routine is expected to perform any function required to access
* the modem and communicate with it, i.e. characters are not escaped or 
* interpreted in any way. The modemClose routine is expected to reverse the 
* actions performed by the modemOpen routine.
*
* The modemWrite routine should be used to send an arbitrary sequence 
* of <bufLen> bytes to the modem. Likewise, the modemRead routine 
* should return exactly <rspBufLen> of bytes read from the device in the 
* buffer pointed to by <rsp>. The parameter <modemReadTimeout> may be 
* exploited to specify a timeout in seconds that the calling task may 
* afford to wait before a complete answer from the modem is successfully 
* received. If the timeout expires, ERROR should be returned by the modemRead 
* routine.
* 
* Finally, the modemIoctl routine should implement a device control routine. 
* The following commands should be supported:
* MODEM_FLUSH: this command should be used to cancel any data previously
* sent to or received from the modem.
* MODEM_SET_FLAGS: this command should be used to set any device flag 
* as specified in the parameter <arg>.
* MODEM_GET_FLAGS: this command should be used to get the current device's
* flags.
*
*/
typedef struct pppModemInterface
    {
    PFW_INTERFACE_OBJ interfaceObj;
    STATUS (* modemOpen)(PFW_PLUGIN_OBJ_STATE * pObjState);
    STATUS (* modemClose)(PFW_PLUGIN_OBJ_STATE * pObjState);
    STATUS (* modemRead)(PFW_PLUGIN_OBJ_STATE * pObjState,
                         char * rsp,
                         UINT rspBufLen,
                         UINT modemReadTimeout);
    STATUS (* modemWrite)(PFW_PLUGIN_OBJ_STATE * pObjState,
                          char * buffer,
                          UINT bufLen);
    STATUS (* modemIoctl)(PFW_PLUGIN_OBJ_STATE * pObjState,
                          int cmd,
                          int arg);
    } PPP_MODEM_INTERFACE;

#ifdef __cplusplus
}
#endif

#endif /* __INCpppModemInterfaceh */
