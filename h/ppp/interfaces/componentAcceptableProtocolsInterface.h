/* componentAcceptableProtocolsInterface.h - PPP Component Acceptable */
/* 				             Protocols Interface */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01b,22nov99,sjv  Changed the signature of set and get functions
01a,06nov99,sgv  created
*/

#ifndef __INCcomponentAcceptableProtocolsInterfaceh
#define __INCcomponentAcceptableProtocolsInterfaceh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "pfw/pfw.h"
#include "pfw/pfwInterface.h"

typedef struct acceptableProtocolsArray
    {
    int numberOfProtocols;
    UINT *protocols;
    } ACCEPTABLE_PROTOCOLS_ARRAY;


/*******************************************************************************
* COMPONENT_ACCEPTABLE_PROTOCOLS_INTERFACE -
*
* Interface Name: "COMPONENT_ACCEPTABLE_PROTOCOLS_INTERFACE"
*
* This interface is implemented by components to inform their layers about the 
* accepted protocol types for receive and send paths
*/

typedef struct COMPONENT_ACCEPTABLE_PROTOCOLS_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    STATUS (*receivePathAcceptableProtocolsGet)   (PFW_PLUGIN_OBJ_STATE *state,
			   ACCEPTABLE_PROTOCOLS_ARRAY **recvProtocols
		               			   );
    STATUS (*sendPathAcceptableProtocolsGet) (PFW_PLUGIN_OBJ_STATE *state,
			   ACCEPTABLE_PROTOCOLS_ARRAY **sendProtocols
		               		      );
    } COMPONENT_ACCEPTABLE_PROTOCOLS_INTERFACE;

#ifdef __cplusplus
}
#endif
#endif /* componentAcceptableProtocolsInterfaceh */
