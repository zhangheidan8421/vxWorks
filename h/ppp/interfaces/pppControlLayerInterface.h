/* pppControlLayerInterface.h - PPP control layer interface definitions */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01d,22feb02,ak  support for dynamic assignment of MP Member Stack to Manager 
				Stack
01c,30jun01,nts moved pppMpMemberSet function to the 
			    new MP_CONTROL_LAYER_INTERFACE
01b,30jun01,nts added pppMpMemberSet function to the PPP_CONTROL_LAYER_INTERFACE
01a,15oct99,sj 	created
*/

#include "ppp/kstart.h"
#include "ppp/kppp.h"
#include "pfw/pfwInterface.h"
#include "ppp/pppInterfaces.h" 

#ifndef __INCpppControlLayerInterfaceh
#define  __INCpppControlLayerInterfaceh

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*PAP_CALLBACK)(PFW_PLUGIN_OBJ_STATE *,  BYTE id, TEST passOrFail);

typedef void (*CHAP_CALLBACK)(PFW_PLUGIN_OBJ_STATE *, BYTE id, TEST passOrFail);
/*******************************************************************************
* LCP_UP_EVENT - LCP is up event
*
* Event Name: "LCP_UP_EVENT"
*
* This Event is published by the Control Layer and is raised as soon as
* LCP is UP. All components in the system that need to communicate with LCP
* via LCP's published interfaces(lcpInterfaces.h) must subscribe to this event
* to obtain a pointer to LCP's PFW_PLUGIN_OBJ_STATE which is a needed parameter 
* for all LCP interfaces
*/

/*******************************************************************************
* PPP_CONTROL_LAYER_INTERFACE - control layer interface for control protocols
*
* Interface Name: "PPP_CONTROL_LAYER_INTERFACE"
*
* This Interface is published and made available by the PPP Control Layer in the
* system. It is intended for all control layer protocols, such as LCP, IPCP,CHAP
* etc., in the system and is used by them to communicate status and control
* information to the Control Layer. In each of the functions defined below
* "pluginState" refers to the state of the caller. Also not all the functions
* are applicable to all protocols; e.g. protocolRejected is called by LCP only
*/
typedef struct PPP_CONTROL_LAYER_INTERFACE
    {
    PFW_INTERFACE_OBJ       interfaceObj;
    void (*protocolUp)      (PFW_PLUGIN_OBJ_STATE * pluginState);
    void (*protocolDown)    (
			    PFW_PLUGIN_OBJ_STATE * pluginState,
			    PPP_STATE              endState
			    );
    void (*protocolFinished)(PFW_PLUGIN_OBJ_STATE * pluginState);
    void (*protocolRejected)(
			    PFW_PLUGIN_OBJ_STATE * pluginState,
			    PPP_PROTOCOL_TYPE      rejectedProtocol
			    );

    void (*pppLcpTerminationRequest)     (PFW_PLUGIN_OBJ_STATE * pluginState);
    void (*pppLcpIdRequest)              (PFW_PLUGIN_OBJ_STATE * pluginState,
							   char * message);
    void (*pppLcpIdRequestReceived)      (PFW_PLUGIN_OBJ_STATE * pluginState,
							   char * message);
    void (*pppLcpTimeRemainingRequest)   (PFW_PLUGIN_OBJ_STATE * pluginState, 
					  ULONG * secondsRemaining,
					  char * message);
    void (*pppLcpTimeRemainingReceived)(PFW_PLUGIN_OBJ_STATE * pluginState,
					  ULONG secondsRemaining,
					  char * message);

    void (*pppAuthRequest)        (PFW_PLUGIN_OBJ_STATE *pluginState,
					  char * userName, char *userPassword);
    void (*pppChallengeAuthVerify)(PFW_PLUGIN_OBJ_STATE *pluginState,
				  char * localName, char * peerName,
				  char * response,unsigned int responseLen,
				  char * challenge, unsigned int challengeLen,
				  BYTE id,CHAP_CALLBACK);

    void (*pppPasswordAuthVerify) (PFW_PLUGIN_OBJ_STATE *pluginState,
				      char * localName,char * peerName,
				      char *password, BYTE id,PAP_CALLBACK);
    void (*pppAuthAckTransmitted) (PFW_PLUGIN_OBJ_STATE *pluginState);
    void (*pppAuthAckReceived)    (PFW_PLUGIN_OBJ_STATE *pluginState);
    void (*pppAuthFailed)         (PFW_PLUGIN_OBJ_STATE *pluginState);
    void (*pppAuthRefused)        (PFW_PLUGIN_OBJ_STATE *pluginState);
    }PPP_CONTROL_LAYER_INTERFACE ;

/* WindNet Multilink */

/*******************************************************************************
* MP_CONTROL_LAYER_INTERFACE - control layer interface for MP
*
* Interface Name: "MP_CONTROL_LAYER_INTERFACE"
*
* This Interface is published and made available by the PPP Control Layer in the
* system. It is used to make the CONTROL LAYER MP aware. This function is 
* called by the mpInterfaceLayer in the MEMBER stack on LCP_UP event. 
* It sets the isMember variable to TRUE and sets the MANAGER stack's stack object.
* In the function defined below "pluginState" refers to the state of the caller. 
*/
typedef struct MP_CONTROL_LAYER_INTERFACE
    {
    PFW_INTERFACE_OBJ       interfaceObj;

	void (*pppMpMemberSet)					/* to make the control layer */
	(										/* MP-AWARE */
	PFW_PLUGIN_OBJ_STATE	*pluginState,
	BOOLEAN					isMember,
	PFW_STACK_OBJ			*mpStackObj
    ); 

	void (*mpUpCallsGet)					/* to return reference to    */
	(										/* MP_UPCALLS 				 */
	PFW_PLUGIN_OBJ_STATE	*pluginState,
	MP_UPCALL_FUNCTIONS		**mpUpCalls,
	void					**pUserHandler
	);
    }MP_CONTROL_LAYER_INTERFACE ;

/* WindNet Multilink */

#ifdef __cplusplus
}
#endif

#endif /*  __INCpppControlLayerInterfaceh */
