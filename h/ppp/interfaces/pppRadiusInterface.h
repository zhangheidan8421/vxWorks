/* pppRadiusInterfaces.h - RADIUS published interfaces */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01a,27jun00,bsn  created
*/

#include "pfw/pfw.h"
#include "pfw/pfwInterface.h"

#ifndef __INCpppRadiusInterfacesh
#define __INCpppRadiusInterfacesh

#ifdef __cplusplus
extern "C" {
#endif

typedef struct REMOTE_AUTHENTICATION_CALLBACKS
    {
    void (*authRequestAccepted)
         (PFW_PLUGIN_OBJ_STATE *state, BYTE id);
    void (*authRequestRejected)
         (PFW_PLUGIN_OBJ_STATE *state, BYTE id, char * message);
    void (*authErrorCallback)
         (PFW_PLUGIN_OBJ_STATE *state, BYTE id);
    } REMOTE_AUTHENTICATION_CALLBACKS;

typedef enum
    {
    STANDARD_CHAP = 0x05,
    MICROSOFT_CHAP = 0x80
    } CHALLENGE_AUTH_METHOD;
 
/*******************************************************************************
* REMOTE_AUTHENTICATION_INTERFACE - remote authentication interface
*
* Interface Name: "REMOTE_AUTHENTICATION_INTERFACE"
*
* This interface is published and made available by RADIUS component.
* The PPP control layer uses this interface to verify the
* user identity
*/
typedef struct REMOTE_AUTHENTICATION_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    void (*challengeAuthVerify)
         (PFW_PLUGIN_OBJ_STATE * state, char * userName,
          char * response, unsigned int responseLen,
          char * challenge, unsigned int challengeLen,
          BYTE id, CHALLENGE_AUTH_METHOD method,
          REMOTE_AUTHENTICATION_CALLBACKS * radiusCallbacks);
    void (*passwordAuthVerify)
         (PFW_PLUGIN_OBJ_STATE * state, char * userName,
          char * password, BYTE id,
          REMOTE_AUTHENTICATION_CALLBACKS * radiusCallbacks);
    } REMOTE_AUTHENTICATION_INTERFACE;

/*******************************************************************************
* REMOTE_ACCOUNTING_INTERFACE - remote accounting interface
*
* Interface Name: "REMOTE_ACCOUNTING_INTERFACE"
*
* This interface is published and made available by RADIUS component.
* The PPP control layer uses this interface to start and
* stop accounting
*/
typedef struct REMOTE_ACCOUNTING_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    void (*accountingStart)
         (PFW_PLUGIN_OBJ_STATE * state);
    void (*accountingStop)
         (PFW_PLUGIN_OBJ_STATE * state);
    } REMOTE_ACCOUNTING_INTERFACE;

#ifdef __cplusplus
}
#endif

#endif /* __INCpppRadiusInterfacesh */
