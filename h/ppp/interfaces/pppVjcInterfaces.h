/* pppVjcInterface.h - PPP VJC up/down interface */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01b,17mar00,cn   replaced setSlotNumbers with setConnIdCompress in 
		 PPP_VJC_INTERFACE.
01a,06nov99,sgv  created
*/


#ifndef __INCpppVjcInterfaceh
#define __INCpppVjcpInterfaceh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "pfw/pfw.h"
#include "pfw/pfwInterface.h"

/*******************************************************************************
* PPP_VJC_INTERFACE -
*
* Interface Name: "PPP_VJC_INTERFACE"
*
* This interface is implemented by vjc component of the PPP
* stack. IPCP component will set it to acceptable values
*/

typedef struct PPP_VJC_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    STATUS (*setSlotNumbers)    (PFW_PLUGIN_OBJ_STATE * state,
			        int			slotNumbers
		                );
    STATUS (*setConnIdCompress) (PFW_PLUGIN_OBJ_STATE * state,
			         BOOL			connIdCompress
		                );
    } PPP_VJC_INTERFACE;

#ifdef __cplusplus
}
#endif

#endif /* __INCpppVjcInterfaceh */
