/* pppVsidInterface.h - PPP IP up/down interface */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01a,19may00,cn   created
*/

#ifndef __INCpppVsidInterfaceh
#define __INCpppVsidInterfaceh

#ifdef __cplusplus
extern "C" {
#endif

#ifdef VIRTUAL_STACK

#include "vxWorks.h"
#include "pfw/pfw.h"
#include "pfw/pfwInterface.h"
#include "netinet/vsLib.h"

/*******************************************************************************
* PPP_VSID_INTERFACE
*
* Interface Name: "PPP_VSID_INTERFACE"
*
* This interface is used to get and set the virtual stack identifier (VSID) 
* for a particular PPP framework stack object.
*
* This interface MUST be published by the PPP_END component in the
* stack, only if VIRTUAL_STACK is defined.
*/

typedef struct PPP_VSID_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    STATUS (*pppVsidGet)(PFW_PLUGIN_OBJ_STATE * state, VSID * pVsid);
    STATUS (*pppVsidSet)(PFW_PLUGIN_OBJ_STATE * state, VSID vsid);
    } PPP_VSID_INTERFACE;

#endif /* VIRTUAL_STACK */

#ifdef __cplusplus
}
#endif

#endif /* __INCpppVsidInterfaceh */
