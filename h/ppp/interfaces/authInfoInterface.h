/* authInfoInterface.h -  Authentication Information Interface */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01a,23feb01,ak created
*/

#ifndef __INCauthInfoInterfaceh
#define __INCauthInfoInterfaceh

#ifdef __cplusplus
extern "C" {
#endif

typedef	struct AUTH_INFO_INTERFACE
	{
	PFW_INTERFACE_OBJ interfaceObj;
	void (*localUserNameGet)
				(
				PFW_PLUGIN_OBJ_STATE 	*pState,
				char 					*pLocalUserName
				);
	void (*remoteUserNameGet)
				(
				PFW_PLUGIN_OBJ_STATE 	*pState,
				char 					*pRemoteUserName
				);
	void (*localUserNameSet)
				(
				PFW_PLUGIN_OBJ_STATE 	*pState,
				char 					*pLocalUserName
				);
	void (*remoteUserNameSet)
				(
				PFW_PLUGIN_OBJ_STATE 	*pState,
				char 					*pRemoteUserName
				);

	}AUTH_INFO_INTERFACE;


#ifdef __cplusplus
}
#endif

#endif /* __INCauthInfoInterfaceh */


