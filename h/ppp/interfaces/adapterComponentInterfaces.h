/* adapterComponentInterfaces.h - generic interfaces for a adapter component */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01d,15jul02,emr  This code was developed on the windnet_ppp2_0-ipv6 branch and
                 should have been developed on the windnet_ppp2_0 branch, so
                 this brings it into line.
01c,29sep00,sj  merging with TOR2_0-WINDNET_PPP-CUM_PATCH_2
01b,27jun00,bsn  added STATISTICS and PORT interface
01a,01nov99,sj 	created
*/

#include "pfw/pfw.h"
#include "pfw/pfwInterface.h"

#ifndef __INCadapterComponentInterfacesh
#define __INCadapterComponentInterfacesh

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
* ADAPTER_DESTINATION_AND_PROTOCOL_SET_INTERFACE - 
*
* Interface Name: "ADAPTER_DESTINATION_AND_PROTOCOL_SET_INTERFACE"
*
* This interface may be used by framing components to set the destination
* address and protocol type the adapter component must use when transmitting
* the frame
*
*/
typedef struct ADAPTER_DESTINATION_AND_PROTOCOL_SET_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    STATUS (*destAddrAndProtocolSet) 
				(
				PFW_PLUGIN_OBJ_STATE *adapterState,
				char * destAddr,
				UINT32 addrLength,
				char * protocol,
				UINT32 protocolLength
				);

    } ADAPTER_DESTINATION_AND_PROTOCOL_SET_INTERFACE;

/*******************************************************************************
* ADAPTER_COMPONENT_STATISTICS_INTERFACE - 
*
* Interface Name: "ADAPTER_COMPONENT_STATISTICS_INTERFACE"
*
* This interface may be used by other components to get the adapter
* component statistics.
*/
typedef struct ADAPTER_COMPONENT_STATISTICS_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    void (*statisticsGet) 
	(
	PFW_PLUGIN_OBJ_STATE *adapterState,
        UINT * inputOctets,
        UINT * outputOctets,
        UINT * inputPackets,
        UINT * outputPackets);
    } ADAPTER_COMPONENT_STATISTICS_INTERFACE;


/*******************************************************************************
* PHY_PORT_INTERFACE - 
*
* Interface Name: "PHY_PORT_INTERFACE"
*
* This interface may be used by other components to get the adapter
* component physical port properties.
*/
typedef struct PHY_PORT_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    UINT (*portNumberGet) (PFW_PLUGIN_OBJ_STATE *adapterState);
    UINT (*portTypeGet) (PFW_PLUGIN_OBJ_STATE *adapterState);
    } PHY_PORT_INTERFACE;

#define PORT_TYPE_ASYNC    0
#define PORT_TYPE_SYNC     1
#define PORT_TYPE_VIRTUAL  5

/*******************************************************************************
* ADAPTER_INFO_GET_INTERFACE - 
*
* Interface Name: "ADAPTER_INFO_GET_INTERFACE"
*
* This interface will be used by PPPoE component to get the port identifier for
* a given stack.
*
*/
typedef struct ADAPTER_INFO_GET_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    STATUS (*adapterPortIdGet) 
                               (
                               PFW_PLUGIN_OBJ_STATE * pAdapterState,
                               ULONG * pPortId
                               );

    } ADAPTER_INFO_GET_INTERFACE;

#ifdef __cplusplus
}
#endif

#endif /* __INCadapterComponentInterfacesh */
