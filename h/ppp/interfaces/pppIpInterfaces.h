/* pppIpInterface.h - PPP IP up/down interface */

/* Copyright 2002 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01f,26jun02,ijm added PPP_IPV6_INTERFACE
01f,15jul02,emr  This code was developed on the windnet_ppp2_0-ipv6 branch and
                 should have been developed on the windnet_ppp2_0 branch, so
                 this brings it into line.
01e,11oct00,sj  added PPP_IP_DNS_INTERFACE
01d,29sep00,sj  merging with TOR2_0-WINDNET_PPP-CUM_PATCH_2
01c,27jul00,sj   moved "01b,27jun00,bsn" changes to new PPP_IP_ROUTES_INTERFACE
01b,27jun00,bsn  added ipRouteAdd, Delete, SetIfMask member functions
01a,04nov99,sgv  created
*/

#ifndef __INCpppIpInterfaceh
#define __INCpppIppInterfaceh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "pfw/pfw.h"
#include "pfw/pfwInterface.h"

/*******************************************************************************
* PPP_IP_INTERFACE -
*
* Interface Name: "PPP_IP_INTERFACE"
*
* This interface is implemented by components of the interface layer of the PPP
* stack. This interface is used to mark an IPv4 interface UP or DOWN.
*/

typedef struct PPP_IP_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    STATUS (*ipInterfaceUp)   (PFW_PLUGIN_OBJ_STATE *state,
			       char *source,
 			       char *destination
		               );
    STATUS (*ipInterfaceDown) (PFW_PLUGIN_OBJ_STATE *state,
			       char *source,
 			       char *destination
		               );
    } PPP_IP_INTERFACE;


/*******************************************************************************
* PPP_IPV6_INTERFACE -
*
* Interface Name: "PPP_IPV6_INTERFACE"
*
* This interface is implemented by components of the interface layer of the PPP
* stack. This interface is used to mark an IPv6 interface UP or DOWN.
*/

typedef struct PPP_IPV6_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    STATUS (*ipv6InterfaceUp)   (PFW_PLUGIN_OBJ_STATE *state,
                                 char *source,
                                 char *destination
                                );
    STATUS (*ipv6InterfaceDown) (PFW_PLUGIN_OBJ_STATE *state,
                                 char *source,
                                 char *destination
                                );
    } PPP_IPV6_INTERFACE;


/*******************************************************************************
* PPP_IP_ROUTES_INTERFACE -
*
* Interface Name: "PPP_IP_ROUTES_INTERFACE"
*
* This interface is implemented by components of the interface layer of the PPP
* stack. This interface is used to ADD or DELETE a route and set interface mask.
*/

typedef struct PPP_IP_ROUTES_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    STATUS (*ipRouteAdd)      (PFW_PLUGIN_OBJ_STATE *state,
			       char *destination,
 			       char *gateway,
                               unsigned long mask,
                               int tos,
                               int flags
		               );
    STATUS (*ipRouteDelete)    (PFW_PLUGIN_OBJ_STATE *state,
			       char *destination,
                               unsigned long mask,
                               int tos,
                               int flags
		               );
    STATUS (*ipSetIfMask)      (PFW_PLUGIN_OBJ_STATE *state,
                               unsigned long mask
		               );
    } PPP_IP_ROUTES_INTERFACE;

/*******************************************************************************
* PPP_IP_DNS_INTERFACE -
*
* Interface Name: "PPP_IP_DNS_INTERFACE"
*
* This interface is implemented by components of the interface layer of the PPP
* stack. This interface is used to SET or GET the Primary and Secondary DNS 
* addresses.
*/

typedef struct PPP_IP_DNS_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    void (*ipDnsAddressGet)    (PFW_PLUGIN_OBJ_STATE *state,
			       char *primaryDnsAddr,
 			       char *secondaryDnsAddr
		               );
    void (*ipDnsAddressSet) (PFW_PLUGIN_OBJ_STATE *state,
			       char *primaryDnsAddr,
 			       char *secondaryDnsAddr
		               );
    } PPP_IP_DNS_INTERFACE;

#ifdef __cplusplus
}
#endif

#endif /* __INCpppIpInterfaceh */
