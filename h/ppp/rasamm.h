/* rasamm.h - Remote Access address manager header file */

/* Copyright 2000 - 2001 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01b,16aug01,rp  removing rasAMMpresent flag
01a,30mar01,spm  file creation: copied from version 01b of tor2_0.open_stack
                 branch (wpwr VOB) for unified code base; fixed header entries
*/

#ifndef __INCrasammh
#define __INCrasammh

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern STATUS RAS_AMM_Init();

extern UINT32 RAS_InsAddrPool(UINT32 routerId,
                              UINT32 ipAddr,
                              UINT   prefixLen, 
                              UINT32 poolSz,
                              UINT   mMethod,
                              UINT   supportPMP,
                              UINT   pmpLkupTblSz);

extern STATUS RAS_DelAddrPool(UINT32 poolId);

extern STATUS RAS_GetIpAddr(UINT32 poolId, UINT32 *routerId, UINT32 *ipAddr);

extern STATUS RAS_RelIpAddr(UINT32 poolId, UINT32 ipAddr);

extern UINT32 RAS_getNumOfPools(int doprintf);

extern UINT32 RAS_getMaxPoolId(int doprintf);

extern UINT32 RAS_getAddrPoolSize(UINT32 poolId, int doprintf);

extern UINT32 RAS_getAddrPoolPrefix(UINT32 poolId, int doprintf);

extern UINT32 RAS_getAddrPoolRouterId(UINT32 poolId, int doprintf);

extern UINT32 RAS_getAddrPoolFirstIp(UINT32 poolId, int doprintf);

extern UINT32 RAS_getAddrPoolNumFreeAddrs(UINT32 poolId, int doprintf);

extern UINT32 RAS_getAddrPoolPMPmode(UINT32 poolId, int doprintf);

extern void * RAS_getAddrPoolexptDrv(UINT32 poolId, int doprintf);

extern void *RAS_getAddrPool1stDrv(UINT32 poolId, UINT32 *srcipAddr, UINT32 *dstipAddr, int doprintf);

extern STATUS RAS_setAddrPoolexptDrv(UINT32 poolId, void *drv, int doprintf);

extern UINT32 RAS_getAddrPoolNumDrvs(UINT32 poolId, int doprintf);

extern STATUS RAS_pmpInsertDriver(UINT32 poolId, UINT32 srcipAddr, UINT32 dstipAddr, void *drv);

extern STATUS RAS_pmpRemoveDriver(UINT32 poolId, UINT32 dstipAddr);

extern void *RAS_pmpGetDriver(UINT32 poolId, UINT32 dstipAddr);

extern STATUS RAS_pmpAddrPoolDrvsInit(UINT32 poolId, void (*)(void *, void *));

extern STATUS RAS_printAddrPoolInfo(UINT32 poolId);

#ifdef __cplusplus
}
#endif

#endif /* __INCrasammh */
