/* pppChapComponent.h - PPP Authentication header */

/* Copyright 1999 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01b,13jan02,ijm added extern "C" for C++
01a,23oct99,sgv  created from routerware source
*/

#ifndef __INCpppChapComponenth
#define __INCpppChapComponenth

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "pfw/pfw.h"

/*                    Profile Parameters
 *                   --------------------
 *        NAME                              VALUES/EXPLANATION
 *       ------                            --------------------
 *      chap_localUserName           Name of the local user name 
 *      chap_maxRetries              Maximum number of unacknowledged chap
 *                                   challange or responese messages to send 
 *
 *      chap_retryInterval           Interval (in seconds) between two 
 *                                   unacknowledged chap challenge messages or 
 *                                   two unacknowledged chap response messages
 *
 *      chap_challengeInterval       Interval (in seconds) between periodic 
 *                                   challenge messages 
 */

extern STATUS pppChapComponentCreate (PFW_OBJ *pfw);
extern STATUS pppChapComponentDelete (PFW_OBJ *pfw);

#ifdef __cplusplus
}
#endif

#endif /* __INCpppChapComponenth */

