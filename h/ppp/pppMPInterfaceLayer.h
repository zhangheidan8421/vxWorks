/* pppMPInterfaceLayer.h - MP Interface Layer header file */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01a,13feb01,sd  Written
*/

#ifndef __INCpppMPInterfaceLayerh
#define __INCpppMPInterfaceLayerh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "pfw/pfw.h"

STATUS mpInterfaceLayerCreate (PFW_OBJ * pfw);
STATUS mpInterfaceLayerDelete (PFW_OBJ * pfw);

#ifdef __cplusplus
}
#endif

#endif /* __INCpppMPInterfaceLayerh */
