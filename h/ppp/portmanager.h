/* portmanager.h - port manger function definitions */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01a,19sep01,as 	created
*/

/*
*$Log:: /Rtrware/devdrvrs/ppp/vlcpstr $
 * 
 * 7     10/01/98 11:43a Alex
 * Updated the PPP source code to conform
 * to a single build. 
 * 
 * 6     4/30/98 3:03p Alex
 * Ppp v4.2.0 check in
 * 
 * 1     4/24/98 12:10a Release Engineer
 * code cleanup, code style changes,
 * linted, system level test
 * PPP v4.2.0
*/

#ifndef __INCportmanagerh
#define __INCportmanagerh

#ifdef __cplusplus
extern "C" {
#endif

#include "ppp/kstart.h"
#include "ppp/kppp.h"
#include "private/ppp/vpppstr.h"
#include "ppp/kbacpif.h"
#include "sllLib.h"

/* portmanager.c */

STATUS pmPortFindAndReserve (PORT_CONNECTION_INFO *pPortToReserve);

STATUS pmFindAndReserveMultiplePort (PFW_OBJ *pfwObj, SL_LIST * pPortToReserve);

PFW_STACK_OBJ * pmPortActivate 
	(SL_LIST * portListToCall, PFW_STACK_OBJ * pBundleId);

PFW_STACK_OBJ * pmPortActivateAndWait 
	(SL_LIST * portListToCall, PFW_STACK_OBJ * pBundleId);

STATUS pmPortUnreserve (PORT_CONNECTION_INFO * pPortUnreserve);

STATUS pmPortUnreserveByPort (USHORT portNumber, PFW_STACK_OBJ * pfwStackObj);


/* Port Manager utility function defination */

STATUS pmPortToListAdd (LINK_INFO * pLinkInfo);

STATUS pmPortListDelete (USHORT portNumberToDelete);

void pmPortListPrint ();


#ifdef __cplusplus
}
#endif

#endif /* __INCportmanagerh */
