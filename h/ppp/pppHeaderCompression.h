/* pppHeaderCompression.h - PPP Header Compression header file */

/* Copyright 2002 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01d,14jan03,adb  branching to tor2_2.windnet_ppp2_0
01c,23aug02,adb  testing do not check in
01b,26jun02,adb  restricting nonTcpSpace
01a,23apr02,adb  created
*/

#ifndef __INCpppHeaderCompressionh
#define __INCpppHeaderCompressionh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"

/* defines */

/* defined in RFC 2509 */

#define HC_MIN_WRAP                 3       /* in secs */

#define HC_EXPECT_REORDERING        0

#define HC_MAX_TCP_SPACE            255

#if 0
#define HC_MAX_NON_TCP_SPACE        65535   /* RFC 2507 but we allow only 255 */
#endif
#define HC_MAX_NON_TCP_SPACE        255

#define HC_MAX_NON_TCP_GENERATION   63

/* PPP negotiated */

#define MIN_HC_TCP_SPACE        3
#define DFT_HC_TCP_SPACE        15
#define MAX_HC_TCP_SPACE        HC_MAX_TCP_SPACE

#define MIN_HC_NON_TCP_SPACE    3
#define DFT_HC_NON_TCP_SPACE    15
#define MAX_HC_NON_TCP_SPACE    HC_MAX_NON_TCP_SPACE

#define MIN_HC_F_MAX_PERIOD     32
#define DFT_HC_F_MAX_PERIOD     256
#define MAX_HC_F_MAX_PERIOD     1024

#define MIN_HC_F_MAX_TIME       1       /* in secs */
#define DFT_HC_F_MAX_TIME       5       /* in secs */
#define MAX_HC_F_MAX_TIME       20      /* in secs */

#define MIN_HC_MAX_HEADER       40
#define DFT_HC_MAX_HEADER       168
#define MAX_HC_MAX_HEADER       512

#define HC_RTP_COMPRESSION_ENABLED  0

/* Protocol over PPP Header Compression Configuration Parameters */

#define DFT_HC_TCP_CONTENT_INSPECT              1

#define DFT_HC_TWICE_ALGORITHM_ENABLED          0
#define DFT_HC_USE_DEFAULT_TCP_CHECKSUM_FCTPTR  0
#define DFT_HC_DEFAULT_TCP_CHECKSUM_FCTPTR      0

#define MIN_HC_CONTEXT_STATE_SEND_PERIOD   1
#define DFT_HC_CONTEXT_STATE_SEND_PERIOD   5
#define MAX_HC_CONTEXT_STATE_SEND_PERIOD   25

#define MIN_HC_CONTEXT_STATE_SEND_TIME     100      /* in msecs */
#define DFT_HC_CONTEXT_STATE_SEND_TIME     300      /* in msecs */
#define MAX_HC_CONTEXT_STATE_SEND_TIME     10000    /* in msecs */

#define MIN_HC_STORED_PACKETS_QUEUE_SIZE   0        /* defunct */
#define DFT_HC_STORED_PACKETS_QUEUE_SIZE   0
#define MAX_HC_STORED_PACKETS_QUEUE_SIZE   25

#define MIN_HC_HYSTERESIS_TIME             47   /* in msecs; the minimum value
                                                 * equals 3000/64, which is the 
                                                 * quotient of HC_MIN_WRAP over
                                                 * (HC_MAX_NON_TCP_GENERATION+1)
                                                 */
#define DFT_HC_HYSTERESIS_TIME             60000000 /* in msecs */
#define MAX_HC_HYSTERESIS_TIME             60000000 /* in msecs */

typedef struct hcPppConfigurationOption
    {
    BOOL    isActive;

    USHORT  protocol;

    USHORT  tcpSpace;
    USHORT  nonTcpSpace;
    USHORT  fMaxPeriod;
    USHORT  fMaxTime;
    USHORT  maxHeader;
    BOOL    rtpCompressionEnabled;
    } HC_PPP_CONFIGURATION_OPTION;

#ifdef __cplusplus

#endif

#endif /* __INCpppHeaderCompressionh */
