/* pppIpv6cpComponent.h - IPV6CP component header file */

/* Copyright 2002 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01a,30jul02,ijm created
*/

#ifndef __INCpppIpv6cpComponenth
#define __INCpppIpv6cpComponenth

#ifdef __cplusplus
extern "C" {
#endif


#include "vxWorks.h"
#include "pfw/pfw.h"

/*                    Profile Parameters
 *                   --------------------
 *        NAME                              VALUES/EXPLANATION
 *       ------                            --------------------
 *
 * The following are RFC 2472 defined IPV6CP negotiation options
 *
 * ipv6cp_ipv6InterfaceIdentifier     Set the interface identifier of the 
 *				      the local/peer system
 *
 * ipv6cp_hcParams                    RFC 2507 Ip Header Compression
 *
 *
 * The following are implementation specific IPV6CP configuration parameters;
 *
 * NOTE:All time intervals and timeout paramters are measured in SECONDS. Also
 * all of the parameters must be set using quoted integer values; e.g. "20"
 *
 * ipv6cp_maxConfigRequests           Maximum number of unacknowledged 
 *				      Configuration requests to send in one 
 *				      attempt
 *
 * ipv6cp_configReqSendInterval       Interval between Configuration requests
 *                                    in an attempt
 *
 * ipv6cp_maxTerminationRequests      Maximum number of unacknowledged
 *                                    Termination requests to send
 *
 * ipv6cp_terminationReqInterval      Interval between Termination Requests
 *
 * ipv6cp_maxConfigFailure            Maximum number of Configuration naks
 *                                    sent in one attempt 
 *
 */

STATUS pppIpv6cpComponentCreate (PFW_OBJ * pfw);
STATUS pppIpv6cpComponentDelete (PFW_OBJ * pfw);

#ifdef __cplusplus
}
#endif

#endif /* __INCpppIpv6cpComponenth */
