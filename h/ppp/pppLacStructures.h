/* pppLacStructures.h - header file */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01a,02aug00,md  created
*/

#ifndef __INCpppLacStructuresh
#define __INCpppLacStructuresh

#ifdef __cplusplus
extern "C" {
#endif

#define PPP_MAX_PHONE_NUMBER_LENGTH 64
#define PPP_MAX_SUB_ADDRESS_LENGTH  64

typedef enum
    {
    PPP_ASYNC_FRAMING = 1,
    PPP_SYNC_FRAMING
    } PPP_FRAMING_TYPE;

typedef struct PPP_ATTRIBUTES
    {
    UINT32	framingType;
    char	calledNumber[PPP_MAX_PHONE_NUMBER_LENGTH];
    UINT32      calledNumberLength;
    char	callingNumber[PPP_MAX_PHONE_NUMBER_LENGTH];
    UINT32      callingNumberLength;
    char	subAddress[PPP_MAX_SUB_ADDRESS_LENGTH];
    UINT32      subAddressLength;
    UINT32	txConnectSpeed;
    UINT32	rxConnectSpeed;
    UINT32	physicalChannelID;
    UINT32	nasIpAddress;
    UINT32      nasPort;
    } PPP_ATTRIBUTES;

#ifdef __cplusplus
}
#endif

#endif /* __INCpppLacStructuresh */
