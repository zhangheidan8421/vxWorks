/* bacpInterfaces.h - BACP user interfaces for BAP Calls */
 
/* Copyright 1999 Wind River Systems, Inc. */
 
#include "copyright_wrs.h"
 
/*
modification history
--------------------
02a,05nov01,as  extra parameter memberStackObj has been added to bacp up call
				funtion pointer (*PPP_BACP_UPCALL)
01a,05feb01,as  created
*/

/* 
#include "ppp/kstart.h"
#include "ppp/kppp.h"
#include "pfw/pfw.h"
*/
#ifndef __INCbacpInterfacesh
#define  __INCbacpInterfacesh

#ifdef __cplusplus
extern "C" {
#endif

/* typedefs */

typedef BYTE (*PPP_BACP_UPCALL)
    (
	PFW_STACK_OBJ *managerStackObj,
	USHORT  event,           
	USHORT  port,
	PFW_STACK_OBJ *memberStackObj,
	BYTE    responseCode,
	char ** reason
    );

enum BACP_EVENT_UPCALL
{
	BACP_EVENT_THIS_LAYER_START,
	BACP_EVENT_THIS_LAYER_UP,
	BACP_EVENT_THIS_LAYER_DOWN,
	BACP_EVENT_THIS_LAYER_FINISHED,
	BAP_EVENT_CALL_REQUEST_STATUS,
	BAP_EVENT_CALL_REQUEST_RECEIVED,
	BAP_EVENT_DROP_QUERY_REQUEST,
	BAP_EVENT_REQUEST_TIMEOUT,
	BAP_EVENT_PORT_UP,
	BAP_EVENT_PORT_DOWN,
	BAP_EVENT_BODA_REQUEST_NOT_PROCESSED, 
	BAP_EVENT_REQUEST_FULL_NAK, 		  
	BAP_EVENT_BODA_PROCESS_REQUEST, 	  
	BACP_EVENT_OPTIONS_EXHAUSTED
};

typedef struct bacpUpcallFunctions
    {

	PPP_BACP_UPCALL bacpUpCallFunction;

    }BACP_UPCALL_FUNCTIONS;

extern STATUS bacpUpCallInit
   	(
	PFW_STACK_OBJ *managerStackObj,			/* manager stack object */	
	BACP_UPCALL_FUNCTIONS *upcallFuncPtr	/* Function pointer of the */
	);

extern ULONG bapBundleCurrentBandwidthGet
	(
	PFW_STACK_OBJ *managerStackObj		/* manager stack object */
	);

extern STATUS bapBundleBandwidthAdd
   	(
	PFW_STACK_OBJ *managerStackObj,		/* manager stack object */
	ULONG bandwidth,					/* bandwidth to be added */
	char * ptr_reason_str,				/* reason string */
	BYTE_ENUM(BAP_PACKET_TYPE) call_or_callback	 /* call or callback request */
	);

extern STATUS bapBundleBandwidthDelete
   	(
	PFW_STACK_OBJ *managerStackObj,		/* manager stack object */
	ULONG bandwidth,					/* bandwidth to be deleted */
	char * ptr_reason_str				/* reason string */
	);

extern STATUS bapBundleBandwidthDrop
   	(
	PFW_STACK_OBJ *managerStackObj,		/* manager stack object */
	ULONG bandwidth,					/* bandwidth to be dropped */
	char * ptr_reason_str				/* reason string */
	);

#ifdef __cplusplus
}
#endif

#endif /*  __INCbacpInterfacesh */
