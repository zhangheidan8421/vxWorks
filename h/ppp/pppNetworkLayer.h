/* pppNetworkLayer.h - Network Layer header file */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01b,13jan02,ijm added extern "C" for C++
01a,07nov99,sgv Written
*/

#ifndef __INCpppNetworkLayerh
#define __INCpppNetworkLayerh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "pfw/pfw.h"

STATUS pppNetworkLayerCreate (PFW_OBJ * pfw);
STATUS pppNetworkLayerDelete (PFW_OBJ * pfw);

#ifdef __cplusplus
}
#endif

#endif /* __INCpppNetworkLayerh */
