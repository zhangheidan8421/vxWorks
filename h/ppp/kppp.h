/* kppp.h - ppp core header file */

/* Copyright 2003 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
*$Log:: /Rtrware/devdrvrs/ppp/kppp.h  $
 * 
 * 9     11/13/98 7:55a Nishit
 * MAXIMUM_NUMBER_OF_PPP_PORTS now
 * defined only if not previously
 * defined; IP_PROTOCOL renamed to
 * PPP_IP_PROTOCOL to remove a conflict
 * 
 * 8     10/01/98 11:43a Alex
 * Updated the PPP source code to conform
 * to a single build. 
 * 
 * 7     6/08/98 4:04p Nishit
 * Added more values in  enum
 * LCP_OPTION_TYPE
 * 
 * 6     4/30/98 3:03p Alex
 * Ppp v4.2.0 check in
 * 
 * 1     4/24/98 12:10a Release Engineer
 * code cleanup, code style changes,
 * linted, system level test
 * PPP v4.2.0
*/

/*
modification history
--------------------
01x,13may02,ijm removed adv_net.h
01w,29jul02,ijm added dual stack declarations
01v,08jul02,ijm updated NUMBER_OF_IPCP_OPTIONS
01u,26jun02,ijm added PPP_IPV6_PROTOCOL
01t,17may02,mk  add ___sh__ for hitachi to the WRS_PACK_ALIGN macro
01s,22may02,vk  added IPV6 specific protocol id
01r,21may02,adb added header compression definitions
01q,07feb02,ak  added PPP_FLAGS
01p,29jan02,ijm added mips to _WRS_PACK_ALIGN macro
01o,13jan02,ijm changed _8210D_SPANNING_TREE_BPDU to SPANNING_TREE_BPDU_8021D
                due to C++ compiler warning
01n,07dec01,mk  added macros _WRS_PACK_ALIGN(x),to fix alignment
                problems for PPP for arm
01m,29sep00,sj  merging with TOR2_0-WINDNET_PPP-CUM_PATCH_2
01l,02aug00,adb Merging in LAC modifications
01k,02aug00,md  moved PPP_ATTRIBUTES structure to pppLacStructures.h
01j,01aug00,adb Merging with openstack view
01i,28jul00,md  changes char* field within PPP_ATTRIBUTES
01h,08jul00,bsn merged from the PPP radius-ras branch
01g,29jun00,md  added PPP_ATTRIBUTES structure
01f,14mar00,sj  changed CHALLENGE_VALUE_SIZE to 16
01e,23feb00,cn  added PPP_IPCP_CONTROL_CODES_NUM, suppressed warnings.
01d,17feb00,cn  corrected IPCP_OPTION_TYPE. 
01c,08feb00,sj  added PRIMARY_DNS_ADDRESS and SECONDARY_DNS_ADDRESS for IPCP
01b,17nov99,sj  changing defaults to match RFC 1661
01a,28sep99,sj 	derived from routerware source base
*/

#ifndef __INCkppph
#define __INCkppph

#include "netinet/in.h" /* for htons() */
#include "net/mbuf.h"
#include "string.h"

#ifndef _WRS_PACK_ALIGN
#if (((defined __arm__) || (defined __thumb__) || (defined __mips__)||(defined __sh__)) && (defined __GNUC__))
#define _WRS_PACK_ALIGN(x) __attribute__((packed,aligned(x))) 

#else
#define _WRS_PACK_ALIGN(x) 
#endif
#endif

#if !defined (PROTOCOL_MASKS)

	#define PROTOCOL_MASK 0x00ff
	#define PROTOCOL_MASKS
	#define GET_PROTOCOL_TYPE_MASK 0xff00
	#define OFFSET_OF_STACK_ID 0x08

#endif

#define NUMBER_OF_IPCP_OPTIONS  	               0x6
#define NUMBER_OF_IPV6CP_OPTIONS                       0x2 

 
#define MIB_ENABLED                                    1
#define MIB_DISABLED                                   2

#define SIZE_OF_WAN_CRC                                2
#define NUMBER_OF_ERROR_BYTES                          50
#define HDLC_FLAG_SEQUENCE                             0x7e
#define HDLC_ADDRESS                                   0xff
 
#define DEFAULT_MAXIMUM_MRU                            1500
#define MAXIMUM_CONFIGURATION_REQUESTS                 10
#define MAXIMUM_CONFIGURATION_FAILURES                 5
#define MAXIMUM_TERMINATION_REQUESTS                   2
#define MAXIMUM_CONFIGURATION_REQUEST_SEND_INTERVAL    3
#define MAXIMUM_CONFIGURATION_REQUEST_BACKOFF_INTERVAL 30
#define MAXIMUM_TERMINATION_REQUEST_SEND_INTERVAL      3
#define MAXIMUM_ECHO_REQUEST_SEND_INTERVAL             3
#define MAXIMUM_ECHO_REQUESTS_NOT_ACKNOWLEDGED         20
#define MAXIMUM_STRING_LENGTH                          80
#define MAXIMUM_PACKET_LENGTH                          2048
#define DEFAULT_ASYNC_CONTROL_CHARACTER_MAP            0xffffffffL
#define ASYNC_CONTROL_ESCAPE_SEQUENCE                  0x7d
#define ASYNC_FLAG_SEQUENCE                            0x7e
#define ASYNC_SIXTH_BIT_COMPLEMENT                     0x20
#define INIT_FRAME_CHECK_SEQUENCE                      0xffff
#define PPP_DATA_DUMP_WIDTH                            16

#if ! defined (VARIABLE_NUMBER_OF_BYTES)
	#define VARIABLE_NUMBER_OF_BYTES               0x01
#endif

#define VARIABLE_NUMBER                                0x01
#define ASYNC_MAXIMUM_TX_ENCODED_PACKET_MULTIPLIER     2
#define MAXIMUM_APPLETALK_PACKET_LENGTH                599
#define APPLETALK_STACK_ID                             3
#define MAXIMUM_NUMBER_OF_PROTOCOL_REJECTS_RECEIVED    10
#define DEFAULT_FCS_TABLE_SIZE                         256
#define DEFAULT_MAGIC_NUMBER                           0x00000000L
#define MAXIMUM_NUMBER_OF_NCP_OPTIONS                  8
#define MAXIMUM_SIZE_OF_OPTION                         16
#define PPP_IPX_NAME_SIZE                              48
#define CHALLENGE_VALUE_SIZE                           16
#define RESPONSE_VALUE_SIZE                            16
#define NAME_SIZE                                      128
#define FINAL_GOOD_FRAME_CHECK_SEQUENCE                0xf0b8
#define MAXIMUM_NUMBER_OF_USERS                        50
#define NCP_MASK htons (0x8000)

#define MAXIMUM_NUMBER_OF_OPTION_TYPES                 25
#define MAXIMUM_NUMBER_OF_CONCATENATED_OPTION_VALUES   3
#define MAXIMUM_LENGTH_OF_OPTION_TYPE                  16
#define MAXIMUM_LENGTH_OF_OPTION_NAME                  80
#define MAXIMUM_LENGTH_OF_OPTION_NUMBER                3
#define MAXIMUM_LENGTH_OF_OPTION_STRING                256
#define MAXIMUM_DISCRIMINATOR_ADDRESS_LENGTH           20

typedef enum 
    {
    PPP_IP_NCP_STACK_INDEX,
    PPP_IPX_NCP_STACK_INDEX,
    PPP_APPLETALK_NCP_STACK_INDEX,
    PPP_NETBIOS_NCP_STACK_INDEX,
    PPP_BRIDGING_NCP_STACK_INDEX,

    NUMBER_OF_NCP_STACKS
    }NCP_STACK_INDEX;

typedef enum 
    {
    PPP_INITIAL_STATE =      0x00,
    PPP_STARTING_STATE =     0x01,
    PPP_CLOSED_STATE =       0x02,
    PPP_STOPPED_STATE =      0x03,
    PPP_CLOSING_STATE =      0x04,
    PPP_STOPPING_STATE =     0x05,
    PPP_REQUEST_SENT_STATE = 0x06,
    PPP_ACK_RECEIVED_STATE = 0x07,
    PPP_ACK_SENT_STATE =     0x08,
    PPP_OPENED_STATE =       0x09,

    NUMBER_OF_PPP_STATES,
    NO_CHANGE_TO_PPP_STATE,
    ILLEGAL_PPP_STATE
    }PPP_STATE;

typedef enum 
    {
    PPP_LINK_DEAD, 
    PPP_LINK_UP,	
    PPP_LINK_ESTABLISHING,
    PPP_LINK_ESTABLISHED_FAILURE,
    PPP_LINK_OPENED,
    PPP_LINK_AUTHENTICATING,
    PPP_LINK_AUTHENTICATED,
    PPP_LINK_AUTHENTICATION_FAILED,
    PPP_NETWORK_LAYER_PROTOCOL_PHASE,
    PPP_NETWORK_LAYER_CLOSING,
    PPP_TERMINATE_LINK_PHASE,
    PPP_TERMINATED_LINK_GOING_DOWN
    }PPP_PHASE;

typedef enum 
    {
    PPP_UP_EVENT                                     = 0x0000,
    PPP_DOWN_EVENT                                   = 0x0001,
    PPP_OPEN_EVENT                                   = 0x0002,
    PPP_CLOSE_EVENT                                  = 0x0003,
    PPP_TIMEOUT_WITH_COUNTER_GREATER_THAN_ZERO_EVENT = 0x0004,
    PPP_TIMEOUT_WITH_COUNTER_EXPIRED_EVENT           = 0x0005,
    PPP_RECEIVE_CONFIGURE_REQUEST_GOOD_EVENT         = 0x0006,
    PPP_RECEIVE_CONFIGURE_REQUEST_BAD_EVENT          = 0x0007,
    PPP_RECEIVE_CONFIG_REQUEST_BAD_OPTION_EVENT      = 0x0008,
    PPP_RECEIVE_CONFIGURE_ACK_EVENT                  = 0x0009,
    PPP_RECEIVE_CONFIGURE_NAK_EVENT                  = 0x000a,
    PPP_RECEIVE_CONFIGURE_REJECT_EVENT               = 0x000b,
    PPP_RECEIVE_TERMINATE_REQUEST_EVENT              = 0x000c,
    PPP_RECEIVE_TERMINATE_ACK_EVENT                  = 0x000d,
    PPP_RECEIVE_UNKNOWN_CODE_EVENT                   = 0x000e,
    PPP_RECEIVE_CODE_REJECT_PERMITTED_EVENT          = 0x000f,
    PPP_RECEIVE_PROTOCOL_REJECT_EVENT                = 0x0010,
    PPP_RECEIVE_CODE_REJECT_CATASTROPHIC_EVENT       = 0x0011,
    PPP_RECEIVE_ECHO_REQUEST_EVENT                   = 0x0012,
    PPP_RECEIVE_ECHO_REPLY_EVENT                     = 0x0013,
    PPP_RECEIVE_DISCARD_REQUEST_EVENT                = 0x0014,
    PPP_ECHO_RESPONSE_TIMEOUT                        = 0x0015,
    PPP_ECHO_RESPONSE_TIMEOUT_FAILURE                = 0x0016,

    NUMBER_OF_PPP_EVENTS
    }PPP_EVENT;

typedef enum 
    {
    OPTIONS_RECEIVED_ARE_OK,
    SOME_OPTIONS_ARE_NACKED,
    SOME_OPTIONS_ARE_REJECTED,
    OPTION_PARSING_ERROR
    }OPTION_PARSE_RESULT;

typedef enum 
    {
    OPTION_DEFAULT_STATE,
    OPTION_PARSED_STATE,
    OPTION_ACKED_STATE,
    OPTION_NACKED_STATE,
    OPTION_REJECTED_STATE
    }OPTION_STATE;


typedef enum 
    {
    CONFIGURE_REQUEST       = 0x01,
    CONFIGURE_ACK           = 0x02,
    CONFIGURE_NAK           = 0x03,
    CONFIGURE_REJECT        = 0x04,
    TERMINATION_REQUEST     = 0x05,
    TERMINATION_ACK         = 0x06,
    CODE_REJECT             = 0x07,
    PROTOCOL_REJECT         = 0x08,
    PPP_ECHO_REQUEST        = 0x09,
    PPP_ECHO_REPLY          = 0x0a,
    DISCARD_REQUEST         = 0x0b,
    IDENTIFICATION          = 0x0c,
    TIME_REMAINING          = 0x0d,

#if ( defined (PPP_CCP)	|| defined (PPP_ECP) )
    RESET_REQUEST           = 0x0e,
    RESET_ACK               = 0x0f,
#endif
    NUMBER_OF_PPP_CONTROL_CODES,
    PPP_IPCP_CONTROL_CODES_NUM = CODE_REJECT,

    AUTHENTICATION_REQUEST  = 0x01,
    AUTHENTICATION_ACK      = 0x02,
    AUTHENTICATION_NAK      = 0x03,
    CHAP_AUTHENTICATION_ACK = 0x03,
    CHAP_AUTHENTICATION_NAK = 0x04,

    CHAP_CHALLENGE          = 0x01,
    CHAP_RESPONSE           = 0x02
    }PPP_CONTROL_CODE;

#ifdef _PROTOCOL_NUMBERS_ARE_DEFINED_IN_NETWORK_BYTE_ORDER
typedef enum 
    {
    PPP_IP_PROTOCOL                    = htons (0x0021),
    PPP_IPV6_PROTOCOL                  = htons (0x0057),  
    IPX_PROTOCOL                       = htons (0x002b),
    APPLETALK_PROTOCOL                 = htons (0x0029),
    VAN_JACOBSON_COMPRESSED_PROTOCOL   = htons (0x002d),
    VAN_JACOBSON_UNCOMPRESSED_PROTOCOL = htons (0x002f),
    IP_COMPRESSION_PROTOCOL            = htons (0x0061),  
    BRIDGING_PROTOCOL                  = htons (0x0031),
    NETBIOS_PROTOCOL                   = htons (0x003f),

    SPANNING_TREE_BPDU_8021D           = htons (0x0201),
    SOURCE_ROUTING_BPDU                = htons (0x0203),
    DEC_LAN_BRIDGE_100                 = htons (0x0205),

    IPCP_PROTOCOL                      = htons (0x8021),
    IPV6CP_PROTOCOL                    = htons (0x8057),  
    IPXCP_PROTOCOL                     = htons (0x802b),
    ATCP_PROTOCOL                      = htons (0x8029),
    BCP_PROTOCOL                       = htons (0x8031),
    NBFCP_PROTOCOL                     = htons (0x803f),
	
    LCP_PROTOCOL                       = htons (0xc021),	
    PAP_PROTOCOL                       = htons (0xc023),
    CHAP_PROTOCOL                      = htons (0xc223),
    LINK_QUALITY_PROTOCOL              = htons (0xc025)

#if defined (PPP_CCP)
    ,	/* terminate the previous entry */
    CCP_PROTOCOL                       = htons (0x80fd),
    FIRST_CHOICE_COMPRESSION_PROTOCOL  = htons (0x00fd)
#endif

#if defined (PPP_ECP)
    ,	/* terminate the previous entry */
    ECP_PROTOCOL                       = htons (0x8053),
    FIRST_CHOICE_ENCRYPTION_PROTOCOL   = htons (0x0053)
#endif

    ,	/* terminate the previous entry */
    MULTILINK_PROTOCOL                 = htons (0x003d)

    ,	/* terminate the previous entry */
    BAP_PROTOCOL                       = htons (0xC02D),
    BACP_PROTOCOL                      = htons (0xC02B),

    HC_IPV6                            = htons (0x0057),

    HC_FULL_HEADER                     = htons (0x0061),
    HC_COMPRESSED_TCP                  = htons (0x0063),
    HC_COMPRESSED_TCP_NODELTA          = htons (0x2063),
    HC_COMPRESSED_NON_TCP              = htons (0x0065),

    HC_CONTEXT_STATE                   = htons (0x2065),

    HC_COMPRESSED_RTP_8                = htons (0x0069),
    HC_COMPRESSED_RTP_16               = htons (0x2069),
    HC_COMPRESSED_UDP_8                = htons (0x0067),
    HC_COMPRESSED_UDP_16               = htons (0x2067),
    
    HC_CONTEXT_STATE_TCP_HEADER_REQUEST= htons (0x3)
}PPP_PROTOCOL_TYPE;

#else /* THEY ARE DEFINED IN HOST BYTE ORDER */

typedef enum 
    {
    PPP_IP_PROTOCOL                    = 0x0021,
    PPP_IPV6_PROTOCOL                  = 0x0057,  
    IPX_PROTOCOL                       = 0x002b,
    APPLETALK_PROTOCOL                 = 0x0029,
    VAN_JACOBSON_COMPRESSED_PROTOCOL   = 0x002d,
    VAN_JACOBSON_UNCOMPRESSED_PROTOCOL = 0x002f,
    IP_COMPRESSION_PROTOCOL            = 0x0061,  
    BRIDGING_PROTOCOL                  = 0x0031,
    NETBIOS_PROTOCOL                   = 0x003f,

    SPANNING_TREE_BPDU_8021D           = 0x0201,
    SOURCE_ROUTING_BPDU                = 0x0203,
    DEC_LAN_BRIDGE_100                 = 0x0205,

    IPCP_PROTOCOL                      = 0x8021,
    IPV6CP_PROTOCOL                    = 0x8057,  
    IPXCP_PROTOCOL                     = 0x802b,
    ATCP_PROTOCOL                      = 0x8029,
    BCP_PROTOCOL                       = 0x8031,
    NBFCP_PROTOCOL                     = 0x803f,
	
    LCP_PROTOCOL                       = 0xc021,	
    PAP_PROTOCOL                       = 0xc023,
    CHAP_PROTOCOL                      = 0xc223,
    LINK_QUALITY_PROTOCOL              = 0xc025

#if defined (PPP_CCP)
    ,	/* terminate the previous entry */
    CCP_PROTOCOL                       = 0x80fd,
    FIRST_CHOICE_COMPRESSION_PROTOCOL  = 0x00fd
#endif

#if defined (PPP_ECP)
    ,	/* terminate the previous entry */
    ECP_PROTOCOL                       = 0x8053,
    FIRST_CHOICE_ENCRYPTION_PROTOCOL   = 0x0053
#endif

    ,	/* terminate the previous entry */
    MULTILINK_PROTOCOL                 = 0x003d

    ,	/* terminate the previous entry */
    BAP_PROTOCOL                       = 0xC02D,
    BACP_PROTOCOL                      = 0xC02B,

    HC_IPV6                            = 0x0057,

    HC_FULL_HEADER                     = 0x0061,
    HC_COMPRESSED_TCP                  = 0x0063,
    HC_COMPRESSED_TCP_NODELTA          = 0x2063,
    HC_COMPRESSED_NON_TCP              = 0x0065,

    HC_CONTEXT_STATE                   = 0x2065,

    HC_COMPRESSED_RTP_8                = 0x0069,
    HC_COMPRESSED_RTP_16               = 0x2069,
    HC_COMPRESSED_UDP_8                = 0x0067,
    HC_COMPRESSED_UDP_16               = 0x2067,
    HC_CONTEXT_STATE_TCP_HEADER_REQUEST= 0x3
    }PPP_PROTOCOL_TYPE;
#endif /* _PROTOCOL_NUMBERS_ARE_DEFINED_IN_NETWORK_BYTE_ORDER */

typedef enum 
    {
    IP_ADDRESSES_OPTION_TYPE   = 0x01,
    IP_COMPRESSION_OPTION_TYPE = 0x02,
    IP_ADDRESS_OPTION_TYPE     = 0x03,

    PRIMARY_DNS_ADDRESS        = 0x81,
    SECONDARY_DNS_ADDRESS      = 0x83

    }IPCP_OPTION_TYPE;

typedef enum 
    {
    IPV6_INTERFACE_IDENTIFIER_OPTION_TYPE = 0x01,
    IPV6_COMPRESSION_PROTOCOL_OPTION_TYPE = 0x02
    }IPV6CP_OPTION_TYPE;


typedef enum 
    {
    SUCCESSFUL   = 0x01,
    UNSUCCESSFUL = 0x02
    }AUTHENTICATION_STATUS;

typedef enum 
    {
    PPP_RX_PACKET_TOO_SHORT, 
    PPP_RX_PACKET_TOO_LONG 
    }PPP_ALARM_TYPE;

typedef enum 
	{
	PPP_FLAG_OFF,
	PPP_FLAG_ON
	} PPP_FLAGS; 

typedef enum 
    {
    PPP_MEMORY_PRINTF,
    PPP_ALARM_PRINTF,
    PPP_DATA_PRINTF,
    PPP_LCP_PRINTF,
    PPP_NCP_PRINTF
    ,	/* terminate the previous entry */
    PPP_MP_PRINTF
    }PPP_PRINTF_GROUPS;

typedef enum 
    {
    MD4_ALGORITHM = 0x04,
    MD5_ALGORITHM = 0x05
    }AUTHENTICATION_ALGORITHM;

typedef enum 
    {
    LOCATION_DETERMINED_BY_AUTHENTICATION,
    DIALING_STRING,
    LOCATION_ID,
    _E164_NUMBER,
    DISTINGUISHED_NAME
    }CALLBACK_OPERATION;

typedef enum 
    {
    PPP_EVENT_LCP_THIS_LAYER_UP,
    PPP_EVENT_LCP_THIS_LAYER_DOWN,
    PPP_EVENT_LCP_THIS_LAYER_FINISHED,
    PPP_EVENT_LCP_TERMINATION_REQUEST,
    PPP_EVENT_LCP_ID_REQUEST,
    PPP_EVENT_LCP_ID_REQUEST_RECEIVED,
    PPP_EVENT_LCP_TIME_REMAINING_REQUEST,
    PPP_EVENT_LCP_TIME_REMAINING_RECEIVED,

    PPP_EVENT_NCP_THIS_LAYER_UP,
    PPP_EVENT_NCP_THIS_LAYER_DOWN,
    PPP_EVENT_NCP_THIS_LAYER_FINISHED,

    PPP_EVENT_AUTHENTICATE_REQUEST,
    PPP_EVENT_AUTHENTICATE_LOOKUP,
    PPP_EVENT_AUTHENTICATE_VERIFY,
    PPP_EVENT_AUTHENTICATE_ACK_TRANSMITTED,
    PPP_EVENT_AUTHENTICATE_ACK_RECEIVED,
    PPP_EVENT_AUTHENTICATION_FAILED,
    PPP_EVENT_AUTHENTICATION_REFUSED,

    PPP_EVENT_OPTIONS_EXHAUSTED
    }PPP_EVENT_UPCALL;

#endif /* __INCkppph */

