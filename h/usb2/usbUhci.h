/* usbUhci.h - USB UHCD host controller header */

/* Copyright 2004-2005 Wind River Systems, Inc. 

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according  
   to the terms of their Wind River license agreement(s) applicable to 
   this software.
*/

/*
Modification history
--------------------
01g,28mar05,pdg  non-PCI changes
01f,25feb05,mta  SPR106276
01e,18aug04,hch  remove the DMA_MALLOC and DMA_FREE macro
01d,17aug04,pdg  Fix for removing the dependency on OHCI while building the
                 project
01c,16jul04,hch add UHCI PCI class definition
01b,26jun03,amn changing the code to WRS standards.
01a,25apr03,ram written.
*/

/*
DESCRIPTION

This file contains the constants of the USB UHCD host controller
*/

/*
INTERNAL
 ***************************************************************************
 * Filename         : usbUhci.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 * Description      :  This file contains the constants of the UHCD host
 *                     controller
 *
 */

#ifndef __INCusbUhcih
#define __INCusbUhcih

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "usbOsal.h"
#include "usbHcdInstr.h" /* WindView Instrumentation */
#include <cacheLib.h>
#include "usb/pciConstants.h"    

/* defines */

/* UHCI PCI class definition */

#define UHCI_CLASS              0x0c        /* BASEC value for serial bus */
#define UHCI_SUBCLASS           0x03        /* SCC value for USB */
#define UHCI_PGMIF              0x00        /* no specific pgm i/f defined */

#ifdef __DEBUG_ERROR__
#define USB_UHCD_LOG_ERROR_MESSAGE(str) logMsg(str,0,0,0,0,0,0)
#else
#define USB_UHCD_LOG_ERROR_MESSAGE(str) 
#endif

extern pUSB_HCD_BUS_INFO	pUhciBusInfo[];

/* index of the host controller device */

#define USB_UHCI_DEVICE_INDEX            00


/* Number of maximum resources can be allocated.. */
#define MAX_RESOURCE_ALLOCATED                  0x0E

#define UHCI_FREE(pBfr)		OS_FREE((char *) pBfr)

#define DMA_FLUSH(pBfr, bytes)	    CACHE_DMA_FLUSH (pBfr, bytes)
#define DMA_INVALIDATE(pBfr, bytes) CACHE_DMA_INVALIDATE (pBfr, bytes)

#define USER_FLUSH(pBfr, bytes)     CACHE_USER_FLUSH (pBfr, bytes)
#define USER_INVALIDATE(pBfr,bytes) CACHE_USER_INVALIDATE (pBfr, bytes)

/* Non PCI support macros */

#define USB_UHCD_CONVERT_TO_BUS_MEM(INDEX, ADDRESS) 			\
			(((ADDRESS) == NULL) ? 0:			\
	                 pUhciBusInfo[INDEX]->pFuncCpuToBus((pVOID)ADDRESS))

/* Macro used for converting the bus specific memory to CPU memory */
#define USB_UHCD_CONVERT_FROM_BUS_MEM(INDEX ,ADDRESS)                         \
                    (((ADDRESS) == 0) ? 0: 				       \
			pUhciBusInfo[INDEX]->pFuncBusToCpu((UINT32)ADDRESS))

/* Macro used for swapping the 32 bit values */

#define USB_UHCD_SWAP_DATA(INDEX,VALUE)				       \
                    (((pUhciBusInfo[INDEX]->pFuncDataSwap) == NULL) ? VALUE : \
                                pUhciBusInfo[INDEX]->pFuncDataSwap(VALUE))                     

/* Macro used for swapping the contents of buffers */

#define USB_UHCD_SWAP_BUFDATA(INDEX,BUFFER,SIZE)			       \
                    (((pUhciBusInfo[INDEX]->pFuncBufferSwap) == NULL) ? SIZE : \
                         pUhciBusInfo[INDEX]->pFuncBufferSwap(BUFFER, SIZE))


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCusbUhcih */



