/* usbEhcdEventHandler.h - Utility Functions for EHCI */

/* Copyright 2004 Wind River Systems, Inc. 

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according  
   to the terms of their Wind River license agreement(s) applicable to 
   this software.
*/

/*
DESCRIPTION
This contains interrupt routines which handle the EHCI
interrupts.
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : usbEhcdEventHandler.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 *
 * Description      :  This contains interrupt routines which handle the EHCI
 *                     interrupts.
 *
 *
 ******************************************************************************/
#ifndef __INCehcdEventHandlerh
#define __INCehcdEventHandlerh

#ifdef	__cplusplus
extern "C" {
#endif

extern VOID usbEhcdISR
    (
    pUSB_EHCD_DATA pEHCDData
    );

extern VOID usbEhcdInterruptHandler
    (
    pUSB_EHCD_DATA pEHCDData
    );

#ifdef	__cplusplus
}
#endif

#endif /* End of __INCehcdEventHandlerh */
/************************** End of file usbEhcdISR.h*****************************/

