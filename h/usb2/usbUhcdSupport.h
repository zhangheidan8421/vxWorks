/* usbUhcdSupport.h - header file for USB UHCD HCD register access*/

/* Copyright 2004-2005 Wind River Systems, Inc. 

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according  
   to the terms of their Wind River license agreement(s) applicable to 
   this software.
*/

/*
Modification history
--------------------
01d,28mar05,pdg  non-PCI changes
01c,05oct04,mta  SPR100704- Removal of floating point math
01c,05oct04,mta  SPR100704- Removal of floating point math
01b,26jun03,amn  changing the code to WRS standards.
01a,25apr03,ram  written.
*/

/*
DESCRIPTION

This file contains the constants and function prototypes
which are used for accessing the registers.
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : usbUhcdSupport.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  This file contains the constants and function prototypes
 *                     which are used for accessing the registers.
 *
 *
 */

#ifndef __INCusbUhciSupporth
#define __INCusbUhciSupporth

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "usbOsal.h"
#include "usbUhcdScheduleQueue.h"

/* defines */


#define USB_UHCD_DWORD_OUT( INDEX, ADDRESS, VALUE)     \
                          usbPciDwordOut(ADDRESS, VALUE)
 
#define USB_UHCD_DWORD_IN(p)  \
               usbPciDwordIn(p)

#define USB_UHCD_WRITE_WORD(address, data2Byte)  \
                                    usbPciWordOut(address, data2Byte)

#define USB_UHCD_READ_WORD(address)  \
                                   usbPciWordIn(address)


/* Interrupt status values */

#define USB_UHCD_COMPLETION_INT                0x01
#define USB_UHCD_TRANSACTION_ERR               0x02
#define USB_UHCD_TRANSACTION_ERR_IOC           0x03
#define USB_UHCD_RESUME_DETECT                 0x04
#define USB_UHCD_HST_SYSTEM_ERR                0x08
#define USB_UHCD_HC_PROCESS_ERR                0x10
#define USB_UHCD_HC_HALTED                     0x20


#ifdef __USE_UHCD_MACRO__

#define usbUhcdUpdateReg  USB_UHCD_UPDATE_REG

#define usbUhcdRegxSetBit USB_UHCD_REG_SET_BIT

#define usbUhcdRegxClearBit USB_UHCD_REG_CLEAR_BIT

#define usbUhcdPortSetBit USB_UHCD_PORT_SET_BIT

#define usbUhcdPortClearBitWriting1 USB_UHCD_PORT_CLEAR_BIT_WTITING_1

#define usbUhcdIsBitSet USB_UHCD_IS_BIT_SET

#define usbUhcdIsBitReset USB_UHCD_IS_BIT_RESET

#define usbUhcdGetBit USB_UHCD_GET_BIT 
                           
#else

/* function declarations */

/***************************************************************************
 * Function Name    : usbUhcdHcreset
 * Description      : This function is used to reset the HC.
 * Parameters       : void.
 * Return Type      : N/A
 */

extern void usbUhcdHcReset (PUHCD_DATA pHCDData);

/***************************************************************************
 * Function Name    : usbUhcdRegxSetBit
 * Description      : This function is used to set a bit of the UHCD's register.
 * Parameters       : usbUhcdRegBaseAddr IN Offset from the base address
 *                    setBit             IN Bit number which is to be set
 * Return Type      : N/A
 */

extern void usbUhcdRegxSetBit (PUHCD_DATA pHCDData,
                               UINT8 usbUhcdRegBaseOffset,
                               UINT8 setBit);

/***************************************************************************
 * Function Name    : usbUhcdRegxClearBit
 * Description      : This function is used to clear a bit of the UHCD's
 *                    register.
 * Parameters       : usbUhcdRegBaseAddr  IN Offset from the base address
 *                    clearBit            IN Bit number which is to be cleared
 * Return Type      : N/A
 */

extern void usbUhcdRegxClearBit (PUHCD_DATA pHCDData,
                                 UINT8 usbUhcdRegBaseOffset,
                                 UINT8 clearBit);

/***************************************************************************
 * Function Name    : usbUhcdPortSetBit
 * Description      : This function is used to set a bit of the UHCD's port
 *                    register
 * Parameters       : usbUhcdRegBaseAddr IN Offset from the base address
 *                    setBit             IN Bit number which is to be set
 * Return Type      : N/A
 */

extern void usbUhcdPortSetBit (PUHCD_DATA pHCDData,
                               UINT8 usbUhcdRegBaseOffset,
                               UINT8 setBit);

/***************************************************************************
 * Function Name    : usbUhcdPortClearBit
 * Description      : This function is used to clear a bit of the UHCD's port
 *                    register.
 * Parameters       : usbUhcdRegBaseAddr  IN Offset from the base address
 *                    clearBit            IN Bit number which is to be cleared
 * Return Type      : void
 */

extern void usbUhcdPortClearBit (PUHCD_DATA pHCDData,
                                 UINT8 usbUhcdRegBaseOffset,
                                 UINT8 clearBit);

/***************************************************************************
 * Function Name    : usbUhcdIsBitSet
 * Description      : This function is used to check whether a bit is set
 *                    in the UHCD's register.
 * Parameters       : usbUhcdRegBaseAddr  IN Offset from the base address
 *                    which_bit           IN Bit number which is to be checked
 * Return Type      : BOOLEAN
 *                    Returns
 *                       TRUE  - If bit is set.
 *                       FALSE - If bit is not set.
 */

extern BOOLEAN usbUhcdIsBitSet (PUHCD_DATA pHCDData,
                                UINT8 usbUhcdRegBaseOffset,
                                UINT8 whichBit);

/***************************************************************************
 * Function Name    : usbUhcdIsBitReset
 * Description      : This function is used to check whether a bit is
 *                    reset in the UHCD's register
 * Parameters       : usbUhcdRegBaseAddr  IN Offset from the base address
 *                    which_bit           IN Bit number which is to be checked
 * Return Type      : BOOLEAN
 *                    Returns
 *                       TRUE  - If bit is reset
 *                       FALSE - If bit is not reset
 */

extern BOOLEAN usbUhcdIsBitReset (PUHCD_DATA pHCDData,
                                  UINT8 usbUhcdRegBaseOffset,
                                  UINT8 whichBit);

/***************************************************************************
 * Function Name    : usbUhcdGetBit
 * Description      : This function is used to get the bit value of the
 *                    UHCD's register.
 * Parameters       : usbUhcdRegBaseAddr  IN Offset from the base address
 *                    which_bit           IN Bit number whose value is to be
 *                                           retrieved.
 * Return Type      : UINT8
 *                    Returns the value at the bit specified
 */

extern UINT8 usbUhcdGetBit (PUHCD_DATA pHCDData,
                            UINT8 usbUhcdRegBaseOffset,
                            UINT8 whichBit);


/*******************************************************************************
 * Function Name    : usbUhcdReadReg
 * Description      : This function is used to read a value from the UHCD's register.
 * Parameters       : pHCDData IN pointer to UHC Data Structure
 *                    usbUhcdRegBaseOffse IN Offset from the base address
 * Return Type      : INT16
 * Global Variables : uhcUhcdUsbBase.
 * Calls            : UHCD_READ_WORD
 *                    UHCD_WRITE_WORD
 * Called by        : None
 * To Do            : None
 ******************************************************************************/
extern INT16 usbUhcdReadReg(PUHCD_DATA pHCDData, UINT8 usbUhcdRegBaseOffset );


/*******************************************************************************
 * Function Name    : usbUhcdIsBitSetReg
 * Description      : This function is used to check whether a bit is set
 *                    of the given value.
 * Parameters       : regValue           IN any Value
 *                    whichBit           IN Bit number which is to be checked
 * Return Type      : BOOLEAN
 *                    Returns
 *                       TRUE  - If bit is set.
 *                       FALSE - If bit is not set.
 * Calls            : None
 *                  
 * Called by        : None
 * To Do            : None
 ******************************************************************************/

/***************************************************************************
*
* usbUhcdIsBitSetReg - check whether a bit is set of the UHCI's register
*
* RETURNS: TRUE, FALSE if bit is not set
*/

extern BOOLEAN usbUhcdIsBitSetReg(UINT16 regValue,UINT8 whichBit);

#endif

/***************************************************************************
*
* usbUhciDelay - 
*
* RETURNS: N/A
*/
extern void usbUhciDelay(PUHCD_DATA pHCDData,
                         UINT16     uDelay);

/***************************************************************************
*
* Function Name    : usbUhciLog2
* Description      : This function is used to calculate log to the base 2
*
* Return Type      : UINT32
*                    Returns the log value to the base 2
*/
extern UINT32 usbUhciLog2(UINT32 value);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCusbUhcdSupporth */

