/* usbHubCommon.h - Macro definitions used in USB Hub class driver */

/* Copyright 2004 Wind River Systems, Inc. 

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according  
   to the terms of their Wind River license agreement(s) applicable to 
   this software.
*/

/*
Modification history
--------------------
01b,18aug05,ami  Fix for enumerating hubs with class code 0x101
01a,30may03,nrv Changing the code to WRS standards
*/

/*
DESCRIPTION

This file contains the macro definitions that are used through
out the USB Hub Class Driver.
*/


/*
 INTERNAL 
 ******************************************************************************
 * Filename         : HUB_Common.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      : This contains the macro definitions that are used through
 *                    out the USB Hub Class Driver.
 *
 ******************************************************************************/

#ifndef __USBHUBCOMMON_H__
#define __USBHUBCOMMON_H__

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "usbOsal.h"
#include "usbHst.h"

/* defines */

#define    USB_HUB_VERSION_NUMBER             "Devel_0.01"
#define    USB_HUB_VERSION_DATE               "4th April 2003"

/* Priority of the Hub's Threads */
#define    USB_HUB_THREAD_PRIORITY            (100)

/* The default configuration Index for a Hub */
#define    USB_DEF_ACTIVE_HUB_CONFIG_INDEX    ((UINT8)0x00)

/* Maximum retries for connection attempt */
#define    USB_HUB_CONFIG_RETRY_MAX           ((UINT8)0x03)

/* Maximum frame number 11 bits are allowed for this field */
#define    USB_HUB_MAXIMUM_FRAME_NUMBER       ((UINT16) 2048)

/* The Debounce Time intreval */
#define    USB_HUB_PORT_DEBOUNCE_TIME         ((UINT16) 100)

/* Define for the Maximum power per root hub */
#define    USB_MAXIMUM_POWER_PER_ROOT_HUB     ((UINT8)0x5)

/* Port number not found */
#define    USB_PORT_NUMBER_NOT_FOUND          ((UINT8)255)

/* The Maximum depth that the USB Tree can grow in USB1.1 */
#define    HUB_MAX_ALLOWED_DEPTH_USB1_1       ((UINT8)5)


/* The Maximum depth that the USB Tree can grow in USB2.0 */
#define    HUB_MAX_ALLOWED_DEPTH_USB2_0       ((UINT8)7)


/* The default endpoint */
#define    USB_HUB_DEFAULT_ENDPOINT           ((UINT8)0x0)

/* Flag for indicating a port as not a High Speed port */
#define    USB_NOT_HIGH_SPEED_HUB_PORT        ((UINT8)0xFF)

/* Defines for the Usb Version of Hub */
#define   USB_HUB_VERSION_0200            ((UINT16)0x0200)
#define   USB_HUB_VERSION_0110            ((UINT16)0x0110)
#define   USB_HUB_VERSION_0100		  ((UINT16)0x0100)
#define   USB_HUB_VERSION_0101            ((UINT16)0x0101)

/* Defines Status bits USB2.0 spec- table 11-7 (wValue) */
#define    USB_HUB_LOCAL_POWER                ((UINT8)0X0)
#define    USB_HUB_OVER_CURRENT               ((UINT8)0X1)
#define    USB_PORT_CONNECTION                ((UINT8)0X0)
#define    USB_PORT_ENABLE                    ((UINT8)0X1)
#define    USB_PORT_SUSPEND                   ((UINT8)0X2)
#define    USB_PORT_OVER_CURRENT              ((UINT8)0X3)
#define    USB_PORT_RESET                     ((UINT8)0X4)
#define    USB_PORT_POWER                     ((UINT8)0X8)
#define    USB_PORT_LOW_SPEED                 ((UINT8)0X9)
#define    USB_C_PORT_CONNECTION              ((UINT8)0X10)
#define    USB_C_PORT_ENABLE                  ((UINT8)0X11)
#define    USB_C_PORT_SUSPEND                 ((UINT8)0X12)
#define    USB_C_PORT_OVER_CURRENT            ((UINT8)0X13)
#define    USB_C_PORT_RESET                   ((UINT8)0X14)

/* defines for the bit value of the wHubStatus */
#define    USB_HUB_LOCAL_POWER_VALUE          ((UINT16)0X1)
#define    USB_HUB_OVER_CURRENT_VALUE         ((UINT16)0X2)

/* defines for the bit value of the wHubStatusChange */
#define    USB_C_HUB_LOCAL_POWER_VALUE         ((UINT16)0X1)
#define    USB_C_HUB_OVER_CURRENT_VALUE        ((UINT16)0X2)

/* defines for the bit value of the wPortStatus */
#define    USB_PORT_CONNECTION_VALUE          ((UINT16)0X0001)
#define    USB_PORT_ENABLE_VALUE              ((UINT16)0X0002)
#define    USB_PORT_SUSPEND_VALUE             ((UINT16)0X0004)
#define    USB_PORT_OVER_CURRENT_VALUE        ((UINT16)0X0008)
#define    USB_PORT_RESET_VALUE               ((UINT16)0X0010)
#define    USB_PORT_POWER_VALUE               ((UINT16)0X0100)
#define    USB_PORT_SPEED_VALUE               ((UINT16)0X0600)

/* defines for the bit value of the wPortStatus */
#define    USB_C_PORT_CONNECTION_VALUE        ((UINT16)0X0001)
#define    USB_C_PORT_ENABLE_VALUE            ((UINT16)0X0002)
#define    USB_C_PORT_SUSPEND_VALUE           ((UINT16)0X0004)
#define    USB_C_PORT_OVER_CURRENT_VALUE      ((UINT16)0X0008)
#define    USB_C_PORT_RESET_VALUE             ((UINT16)0X0010)

/* Defines for the Features USB2.0 spec- table 9-4 (bRequest) */
#define    USB_CLEAR_FEATURE                  ((UINT8) 0x01)
#define    USB_SET_FEATURE                    ((UINT8) 0x03)
#define    USB_GET_DESCRIPTOR                 ((UINT8) 0x06)
#define    USB_SET_CONFIGURATION              ((UINT8) 0x09)
#define    USB_GET_STATUS                     ((UINT8) 0x00)
#define    USB_GET_HUB_DESCRIPTOR             ((UINT8) 0xA0)


/* Defines for Hub Class Request */
#define    USB_CLEAR_TT_REQUEST               ((UINT8) 0x08)
#define    USB_RESET_TT                       ((UINT8) 0x09)

/* Defines for the bmRequest Field */
#define    USB_HUB_TARGET_GET                 ((UINT8) 0xA0)
#define    USB_HUB_TARGET_SET                 ((UINT8) 0x20)
#define    USB_PORT_TARGET_GET                ((UINT8) 0xA3)
#define    USB_PORT_TARGET_SET                ((UINT8) 0x23)
#define    USB_DEVICE_TARGET_GET              ((UINT8) 0x80)

/* Defines for the Descriptor Types */
#define    USB_HUB_DESCRIPTOR                 ((UINT8) 0x29)
#define    USB_CONFIGURATION_DESCRIPTOR       ((UINT8) 0x02)

/* Defines for the hub states */
#define    USB_HUB_STATES                     UINT8
#define    USB_HUB_NO_STATE                   ((USB_HUB_STATES) 0x00)
#define    USB_MARKED_FOR_DELETION            ((USB_HUB_STATES) 0x01)
#define    USB_HUB_DEBOUNCE_PENDING           ((USB_HUB_STATES) 0x02)
#define    USB_HUB_RESET_PENDING              ((USB_HUB_STATES) 0x03)
#define    USB_HUB_RESET_COMPLETION_PENDING   ((USB_HUB_STATES) 0x04)
#define    USB_HUB_RESET_COMPLETED            ((USB_HUB_STATES) 0x05)
#define    USB_HUB_PORT_DEFAULT               ((USB_HUB_STATES) 0x06)
#define    USB_HUB_PORT_ADDRESSED             ((USB_HUB_STATES) 0x07)
#define    USB_HUB_PORT_CONFIGURED            ((USB_HUB_STATES) 0x08)


/*******************************************************************************
 * Macro Name  : HUB_DESC_bNbrPorts
 * Description : This returns the number of ports that a specific hub contains
 *               by parsing the hub  descriptor.
 * Parameters  : pHubDescriptor          IN   pointer to the Hub descriptor.
 * Return Type : UINT8
 ******************************************************************************/

#define USB_HUB_DESC_bNbrPorts(pHubDescriptor)                                     \
     ((UINT8*) (pHubDescriptor))[2]                                            \


/*******************************************************************************
 * Macro Name  : HUB_DESC_wHubCharacteristics
 * Description : This parses the hub descriptor and returns the
 *               wHubCharacteristic.
 * Parameters  : pHubDescriptor          IN   pointer to the Hub descriptor.
 * Return Type : UINT16
 ******************************************************************************/

#define USB_HUB_DESC_wHubCharacteristics(pHubDescriptor)                           \
    (   ( ( (UINT8*) (pHubDescriptor))[4]<<8)     |                            \
        ( ( (UINT8*) (pHubDescriptor))[3]) )                                   \


/*******************************************************************************
 * Macro Name  : HUB_DESC_bPwrOn2PwrGood
 * Description : This parses the hub descriptor and returns the bPwrOn2PwrGood.
 * Parameters  : pHubDescriptor          IN   pointer to the Hub descriptor.
 * Return Type : UINT8
 ******************************************************************************/

#define USB_HUB_DESC_bPwrOn2PwrGood(pHubDescriptor)                                \
        ( ( (UINT8*) (pHubDescriptor) )[5] )                                   \


/*******************************************************************************
 * Macro Name  : HUB_DESC_bHubContrCurrent
 * Description : This parses the hub descriptor and returns the bHubContrCurrent.
 * Parameters  : pHubDescriptor          IN   pointer to the Hub descriptor.
 * Return Type : UINT8
 ******************************************************************************/

#define USB_HUB_DESC_bHubContrCurrent(pHubDescriptor)                              \
    (    ( (UINT8*) (pHubDescriptor) )[6]  )                                   \


/*******************************************************************************
 * Macro Name  : HUB_DESC_GET_NUMPORT_OFFSET
 * Description : This parses the hub descriptor and returns the offset required
 *               for data of the DeviceRemovableMap and PortPWrCtrlMask fields
 *               as per the byte granularity.
 * Parameters  : pHubDescriptor          IN   pointer to the Hub descriptor.
 *               uOffset                 OUT  the offset for the number of ports
 * Return Type : None
 ******************************************************************************/

#define USB_HUB_DESC_GET_NUMPORT_OFFSET(pHubDescriptor,uOffset)                    \
{                                                                              \
    UINT8 uNbrOfPorts = HUB_DESC_bNbrPorts(pHubDescriptor);                    \
    /* give the 0th Bit Padding */                                             \
    uNbrOfPorts++;                                                             \
    /* Implement the byte granurality */                                       \
    uOffset = USB_HUB_BYTE_GRANULARITY(uNbrOfPorts);                               \
}                                                                              \


/*******************************************************************************
 * Macro Name  : HUB_DESC_DevRemovableMapStart
 * Description : This parses the hub descriptor and returns the pointer to the
 *               start of the DeviceRemovable bit map
 * Parameters  : pHubDescriptor          IN   pointer to the Hub descriptor.
 * Return Type : PUINT8
 ******************************************************************************/

#define USB_HUB_DESC_DevRemovableMapStart(pHubDescriptor)                          \
    ((UINT8*)(    ( (UINT32) (pHubDescriptor) )+7 )  )                         \


/*******************************************************************************
 * Macro Name  : HUB_DESC_PortPwrCtrlMaskStart
 * Description : This parses the hub descriptor and returns the pointer to the
 *               start of the PortPwrCtrlMask bit map
 * Parameters  : pHubDescriptor          IN   pointer to the Hub descriptor.
 * Return Type : PUINT8
 ******************************************************************************/

#define USB_HUB_DESC_PortPwrCtrlMaskStart(pHubDescriptor,pPortPwrCtrlMask)         \
{                                                                              \
    UINT8 uOffset = 0;                                                         \
    HUB_DESC_GET_NUMPORT_OFFSET (pHubDescriptor,uOffset);                      \
    pPortPwrCtrlMask    =  ((UINT8*)( ( (UINT32) (pHubDescriptor) )            \
                                         +7+uOffset ) );                       \
}                                                                              \


/*******************************************************************************
 * Macro Name  : HUB_DESC_IS_GANG_POWERED
 * Description : This parses the hub descriptor and returns if the device is
 *               Gang powered or not.
 * Parameters  : pHubDescriptor          IN   pointer to the Hub descriptor.
 * Return Type : BOOLEAN
 ******************************************************************************/

#define USB_HUB_DESC_IS_GANG_POWERED(pHubDescriptor)                               \
    (0==(UINT8)(HUB_DESC_wHubCharacteristics(pHubDescriptor)&0x1)?             \
           FALSE:TRUE)                                                         \


/*******************************************************************************
 * Macro Name  : HUB_CONFIG_wTotalLength
 * Description : This parses the config descriptor and returns the total length
 *               of the descriptor
 * Parameters  : pConfigDescriptor        IN   pointer to the config descriptor.
 * Return Type : UINT16
 ******************************************************************************/

#define USB_HUB_CONFIG_wTotalLength(pConfigDescriptor)                             \
    (((pConfigDescriptor)[2]&0xff) | ((pConfigDescriptor)[3]&0xff)<<8)         \


/*******************************************************************************
 * Macro Name  : HUB_DESC_IS_COMPOUND_DEVICE
 * Description : This parses the hub descriptor and returns if the hub is part
 *               of a compound device or not.
 * Parameters  : pHubDescriptor          IN   pointer to the Hub descriptor.
 * Return Type : BOOLEAN
 ******************************************************************************/

#define USB_HUB_DESC_IS_COMPOUND_DEVICE(pHubDescriptor)                            \
    (0==(UINT8)(USB_HUB_DESC_wHubCharacteristics(pHubDescriptor)&0x4)?             \
           FALSE:TRUE)                                                         \


/*******************************************************************************
 * Macro Name  : HUB_DESC_POWER_PROTECTION_SCHEME
 * Description : This parses the hub descriptor and returns the power protection
 *               scheme used - if this field is 0 this is Global protection
 *               scheme, if 1, it has per port reporting. If >1 then it has no
 *               power current protection
 * Parameters  : pHubDescriptor          IN   pointer to the Hub descriptor.
 * Return Type : UINT8
 ******************************************************************************/

#define USB_HUB_DESC_POWER_PROTECTION_SCHEME(pHubDescriptor)                       \
    ( (USB_HUB_DESC_wHubCharacteristics(pHubDescriptor)&0x18)>>3 )                 \


/*******************************************************************************
 * Macro Name  : HUB_IS_HUB_EVENT
 * Description : This parses the Event for a given Status and returns if any
 *               hub event has occurred.
 * Parameters  : uStatus                IN   Change status bitmap of the hub.
 * Return Type : BOOLEAN
 ******************************************************************************/

#define USB_HUB_IS_EVENT(uStatus)                                              \
    ( ( 0==(((UINT8*)(uStatus))[0]&0x01))?FALSE:TRUE)                          \

/*******************************************************************************
 * Macro Name  : HUB_IS_PORT_EVENT
 * Description : This parses the Event for a given Status and returns if any
 *               event has occurred in the port specified.
 * Parameters  : pStatus                IN   Change status bitmap of the hub.
 *               uPortNumber            IN   The port number of the change
 *               bResult                OUT  The result of the check
 * Return Type : None.
 ******************************************************************************/

#define USB_HUB_IS_PORT_EVENT(pStatus, uPortNumber)                                \
     ( (0 != ((pStatus)[(uPortNumber)/8]&((UINT8)0x1<<((uPortNumber)%8))) )?   \
            TRUE:FALSE )                                                       \


/*******************************************************************************
 * Macro Name  : HUB_IS_CONNECTION_CHANGE
 * Description : This parses the event for a given status and returns if any
 *               connect change event has occurred.
 * Parameters  : uStatus                IN   Change status of the port.
 * Return Type : BOOLEAN
 ******************************************************************************/

#define USB_HUB_IS_CONNECTION_CHANGE(uStatus)                                      \
    ((  0==((uStatus)&USB_C_PORT_CONNECTION_VALUE))? FALSE:TRUE)                   \


/*******************************************************************************
 * Macro Name  : HUB_IS_ENABLE_CHANGE
 * Description : This parses the event for a given status and returns if any
 *               enable change event has occurred.
 * Parameters  : uStatus                IN   Change status of the port.
 * Return Type : BOOLEAN
 ******************************************************************************/

#define USB_HUB_IS_ENABLE_CHANGE(uStatus)                                          \
    ((  0==((uStatus)&USB_C_PORT_ENABLE_VALUE))? FALSE:TRUE)                       \


/*******************************************************************************
 * Macro Name  : HUB_IS_SUSPEND_CHANGE
 * Description : This parses the event for a given status and returns if any
 *               suspend change event has occurred.
 * Parameters  : uStatus                IN   Change status of the port.
 * Return Type : BOOLEAN
 ******************************************************************************/

#define USB_HUB_IS_SUSPEND_CHANGE(uStatus)                                         \
    ((  0==((uStatus)&USB_C_PORT_SUSPEND_VALUE))? FALSE:TRUE)                      \


/*******************************************************************************
 * Macro Name  : HUB_IS_RESET_CHANGE
 * Description : This parses the event for a given status and returns if any
 *               reset change event has occurred.
 * Parameters  : uStatus                IN   Change status of the port.
 * Return Type : BOOLEAN
 ******************************************************************************/

#define USB_HUB_IS_RESET_CHANGE(uStatus)                                           \
    ((  0==((uStatus)&USB_C_PORT_RESET_VALUE))? FALSE:TRUE)                        \


/*******************************************************************************
 * Macro Name  : HUB_IS_OVER_CURRENT_CHANGE
 * Description : This parses the event for a given status and returns if any
 *               over current event has occurred.
 * Parameters  : uStatus                IN   Change status of the port.
 * Return Type : BOOLEAN
 ******************************************************************************/

#define USB_HUB_IS_OVER_CURRENT_CHANGE(uStatus)                                    \
    ((  0==((uStatus)&USB_C_PORT_OVER_CURRENT_VALUE))? FALSE:TRUE)                 \


/*******************************************************************************
 * Macro Name  : HUB_DEVICE_SPEED
 * Description : This detects the speed of the device from the port status
 * Parameters  : uStatus                IN   status of the port.
 * Return Type : UINT8
 ******************************************************************************/

#define USB_HUB_DEVICE_SPEED(uStatus)                                              \
        ((UINT8)((((uStatus) & USB_PORT_SPEED_VALUE) >> 9) & 0xFF))                

/*******************************************************************************
 * Macro Name  : HUB_SET_HUB_FEATURE
 * Description : This will submit a blocking call to set a hub feature.
 * Parameters  : pHub                  IN   The Hub pointer.
 *               bResult               OUT  The result will be stored here
 *               Feature               IN   The feature on the hub to be set
 * Return Type : None
 ******************************************************************************/

#define USB_HUB_SET_HUB_FEATURE(pHub,bResult,Feature)                              \
{                                                                              \
    bResult=usbHubSubmitControlRequest(pHub,USB_HUB_TARGET_SET,                     \
    USB_SET_FEATURE,Feature ,0);                                                   \
}                                                                              \


/*******************************************************************************
 * Macro Name  : HUB_CLEAR_HUB_FEATURE
 * Description : This will submit a blocking call to clear a hub feature.
 * Parameters  : pHub                  IN   The Hub pointer.
 *               bResult               OUT  The result will be stored here
 *               Feature               IN   The feature on the hub to be set
 * Return Type : None
 ******************************************************************************/

#define USB_HUB_CLEAR_HUB_FEATURE(pHub,bResult,Feature)                            \
{                                                                              \
    bResult=usbHubSubmitControlRequest(pHub, USB_HUB_TARGET_SET,                     \
    USB_CLEAR_FEATURE,Feature ,0);                                                 \
}                                                                              \


/*******************************************************************************
 * Macro Name  : HUB_SET_PORT_FEATURE
 * Description : This will submit a blocking call to set a port feature.
 *               Converts the port number
 * Parameters  : pHub                  IN   The Hub pointer.
 *               uPortIndex            IN   Port index +1 = port number
 *               Feature               IN   The feature on the hub to be set
 * Return Type : None
 ******************************************************************************/

#define USB_HUB_SET_PORT_FEATURE(pHub,uPortIndex,Feature)                          \
        usbHubSubmitControlRequest(pHub,                                         \
                                 USB_PORT_TARGET_SET,                              \
                                 USB_SET_FEATURE,                                  \
                                 Feature,                                      \
                                 uPortIndex+1)                                 \


/*******************************************************************************
 * Macro Name  : HUB_CLEAR_PORT_FEATURE
 * Description : This will submit a blocking call to clear a port feature.
 * Parameters  : pHub                  IN   The Hub pointer.
 *               uPortIndex            IN   Port index +1 = port number
 *               Feature               IN   The feature on the hub to be set
 * Return Type : None
 ******************************************************************************/

#define USB_HUB_CLEAR_PORT_FEATURE(pHub,uPortIndex,Feature)                        \
        usbHubSubmitControlRequest(pHub,                                         \
                                 USB_PORT_TARGET_SET,                              \
                                 USB_CLEAR_FEATURE,                                \
                                 Feature,                                      \
                                 uPortIndex+1)                                 \


/*******************************************************************************
 * Macro Name  : HUB_GET_HUB_STATUS
 * Description : This will submit a blocking call to get a hub status.
 * Parameters  : pHub                  IN      The Hub pointer.
 *               pBuffer               IN OUT  The buffer to which the status is
 *                                             copied
 *               pBufferLength         IN OUT  The Length of the Buffer
 * Return Type : None
 ******************************************************************************/

#define USB_HUB_GET_STATUS(pHub,pBuffer,pBufferLength)                         \
        usbHubGetStatus(pHub, 0, USB_HUB_TARGET_GET,pBuffer,pBufferLength)           \


/*******************************************************************************
 * Macro Name  : HUB_GET_PORT_STATUS
 * Description : This will submit a blocking call to get a port status.
 * Parameters  : pHub                  IN      The Hub pointer.
 *               uPortIndex            IN      Port index +1 = port number
 *               pBuffer               IN OUT  The buffer to which the status is
 *                                             copied
 *               pBufferLength         IN OUT  The Length of the Buffer
 * Return Type : None
 ******************************************************************************/

#define USB_HUB_GET_PORT_STATUS(pHub,uPortIndex,pBuffer,pBufferLength)             \
        usbHubGetStatus(pHub, uPortIndex+1,USB_PORT_TARGET_GET,pBuffer,pBufferLength)\


/*******************************************************************************
 * Macro Name  : HUB_TIME_DIFF
 * Description : This finds the difference of time between the current
 *               frame number and a different frame number.
 * Parameters  : uCurrentFrame         IN   This is the current frame number.
 *               uLastFrame            IN   This is the last frame number.
 * Return Type : UINT16
 ******************************************************************************/

#define USB_HUB_TIME_DIFF(uCurrentFrame,uLastFrame)                                \
    ( ((uLastFrame)>(uCurrentFrame))?                                          \
    (USB_HUB_MAXIMUM_FRAME_NUMBER - (uLastFrame)+uCurrentFrame ):                  \
    (uCurrentFrame-(uLastFrame)) )                                             \


/*******************************************************************************
 * Macro Name  : HUB_BYTE_GRANULARITY
 * Description : This converts the parameter into nearest largest byte count
 *               (byte granularity form).
 * Parameters  : uNumberOfBits         IN   This is the value that has to be
 *                                          converted to byte granularity form.
 * Return Type : UINT8
 ******************************************************************************/

#define USB_HUB_BYTE_GRANULARITY(uNumberOfBits)		                               \
    ( (((UINT8)(uNumberOfBits)%8)>0)?                                          \
     (((uNumberOfBits)/8)+1): ((uNumberOfBits)/8) )                            \


/*******************************************************************************
 * Macro Name  : MARK_FOR_DELETE_PORT
 * Description : This marks the port for deletion. This takes care of reseting
 *               the global bus state if the device connected to the port is in
 *               default state
 * Parameters  : pHub            IN   pointer to the Hub
 *               pPort           IN   pointer to the port on the hub
 * Return Type : None.
 ******************************************************************************/

#define USB_MARK_FOR_DELETE_PORT(pHub,pPort)                                       \
        {                                                                      \
            /* Check if we have the bus information */                         \
            if ( (NULL != (pHub)->pBus) &                                      \
               (USB_HUB_PORT_CONFIGURED > (pPort)->StateOfPort )  &                \
               (USB_HUB_RESET_PENDING < (pPort)->StateOfPort ) )                   \
            {                                                                  \
                /* Set the bus state as no device is being configured */       \
                (pHub)->pBus->bDeviceBeingConfigured = FALSE;                  \
                if (NULL != pHub->pBus->pResetURB)                             \
                {                                                              \
                    /* Cancel Reset URB -We force this before we delete hub */  \
                    if (USB_HUB_RESET_COMPLETED > (pPort)->StateOfPort)           \
		            {                                                          \
                        if (USBHST_SUCCESS == usbHstURBCancel (pHub->pBus->pResetURB))  \
                        {                                                             \
                            /* Call the reset Callback */                               \
                            usbHubResetCallback(pHub->pBus->pResetURB);                  \
                        }                                                         \
                    }                                                             \
                }/* End of if (NULL !=.. */                                    \
                                                                               \
            } /* End of if (NULL !=.. */                                       \
                                                                               \
            (pPort)->StateOfPort = USB_MARKED_FOR_DELETION;                        \
                                                                               \
        } /* End of if ( (HUB_PORT_CONFIGURED > pPort->StateOfPort .. */       

/*******************************************************************************
 * Macro Name  : VALIDATE_DESCRIPTOR_SEQUENCE
 * Description : This validates the config descriptor
 *               Only valid descriptors Sequence is
 *               1) 0x24210 (Endpoint,AlternateInterface,
 *                           Endpoint,DefaultInterface,,Config)
 *               2) 0x00210 (Endpoint,DefaultInterface,Config)
 * Parameters  : uDescriptorSequence    IN   Value showing the descriptor
 *                                           sequence
 * Return Type : BOOLEAN
 ******************************************************************************/
#define USB_VALIDATE_DESCRIPTOR_SEQUENCE(uDescriptorSequence)                       \
    (( 0 == ((uDescriptorSequence == 0x24210) ||                                \
        (uDescriptorSequence == 0x00210 ) ) ) ? FALSE:TRUE)                     \

/* This Data Structure stores information about the hub  descriptor. */
typedef struct usb_hub_descriptor_info 
{
    UINT8   bNbrPorts;           /* The Number of Ports                       */
    UINT16  wHubCharacteristics; /* Characteristics of the hub                */
    UINT8   bPwrOn2PwrGood;      /* Time to wait before the Power is stable   */
    UINT8   bHubContrCurrent;    /* Power Requiement by the hub electronics   */
} OS_STRUCT_PACKED USB_HUB_DESCRIPTOR_INFO, * pUSB_HUB_DESCRIPTOR_INFO;

/*
 * This Data Structure stores information about the hub Status Change
 * information.
 */
typedef struct usb_hub_status 
{
    UINT16  wHubStatus;          /* Contains the status of the hub            */
    UINT16  wHubChange;          /* Contains the change status of the hub     */
} OS_STRUCT_PACKED USB_HUB_STATUS,* pUSB_HUB_STATUS;

/*
 * This Data Structure stores information about the Port status change
 * information.
 */
typedef struct usb_hub_port_status 
{
    UINT16  wPortStatus;                  /* The port status information      */
    UINT16  wPortChange;                  /* The port status change.          */
} OS_STRUCT_PACKED USB_HUB_PORT_STATUS, * pUSB_HUB_PORT_STATUS;

/*
 * The following have been pre defined here as a method to handle cyclic
 * dependency
 */
typedef struct usb_hub_info       * pUSB_HUB_INFO;
typedef struct usb_hub_bus_info   * pUSB_HUB_BUS_INFO;
typedef struct usb_hub_port_info  * pUSB_HUB_PORT_INFO;

/*
 * This data structure holds the information about a port of a hub as to what
 * device is connected to that port. The port status is also stored in this
 * structure.
 */
typedef struct usb_hub_port_info 
{
    UINT32               uDeviceHandle;   /* Identification of the hub device */
    BOOLEAN              bDebouncePeriod; /* States if the port is in debounce*/
    BOOLEAN              bOldConnectStatus; /* previous connect status        */
    UINT16               uConnectFrame;   /* The frame num when last connected*/
    UINT8                uConnectRetry;   /* number of times connect retried  */
    pUSB_HUB_INFO            pHub;            /* pointer to the hub if this is hub*/
    USB_HUB_STATES           StateOfPort;     /* State of this port               */
} USB_HUB_PORT_INFO;

/*
 * This data structure holds the information about the BUS that is present and
 * this forms the starting point for the entire topology of the bus.
 */
typedef struct usb_hub_bus_info 
{
    UINT8         uBusHandle;             /* Identification of a Bus          */
    OS_THREAD_ID  BusManagerThreadID;     /* Thread ID of the Bus manager     */
    UINT8         uNumberOfHubEvents;     /* Number of Hub events             */
    BOOLEAN       bDeviceBeingConfigured; /* Is any device being configured   */
    UINT8         uNumberOfConfigRetries; /* Number of configuration retries  */
    UINT32        uDeviceHandle;          /* The root hub device handle       */
    UINT8         uBusSpeed;              /* Speed of the bus                 */ 
    pUSB_HUB_INFO     pRootHubInfo;           /* The pointer to root hub structure*/
    pUSBHST_URB   pResetURB;               /* pointer to the reset URB */
    struct usb_hub_bus_info *pNextBus;       /* The pointer to the next bus      */
} USB_HUB_BUS_INFO;

/*
 * This data structure holds the information about a hub and provides the
 * linking info about the devices connected to this hub.
 */
    /*
     * NOTE1: The status change bit map obtained in response to the status change
     * interrupt IN request.
     */
    /*
     * NOTE2: The pointer to the URB structure that is used for the polling of the
     * interrupt endpoint for status change bit map
     */

typedef struct usb_hub_info
{
    UINT32               uDeviceHandle;  /* Identification of the hub device  */
    UINT8        *       pStatus;        /* NOTE1:                            */
    USB_HUB_BUS_INFO   *     pBus;           /* Pointer to the bus structure      */
    UINT8                uPowerPerPort;  /* power that can be supplied to port*/
    /* The Endpoint Address where the Interrupt Pipe resides.                 */
    UINT8                uInterruptEndpointNumber;
    USB_HUB_DESCRIPTOR_INFO  HubDescriptor;  /* fields of the hub descriptor      */
    USBHST_URB           StatusChangeURB;/* NOTE2:                            */
    BOOLEAN              bURBSubmitted;  /* Denotes if the URB Was submitted  */
    UINT8                uCurrentTier;   /* USB Tier of this hub              */

    UINT8                uHubTTInfo;     /* describing the TT organization    */

    USB_HUB_STATES           StateOfHub;     /* State of this Hub              */
    USB_HUB_PORT_INFO*       pPortList[1];   /* List of pointers to ports   */
} USB_HUB_INFO;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* End of #ifndef __HUB_COMMON_H__ */

/**************************** End of file Hub_Common.h ************************/
