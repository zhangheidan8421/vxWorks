/* usbd.h - USBD functional interface definition */

/* Copyright 2004 Wind River Systems, Inc. 

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according  
   to the terms of their Wind River license agreement(s) applicable to 
   this software.
*/

/*
Modification history
--------------------
01a,30may03,nrv Changing the code to WRS standards
*/

/*
DESCRIPTION

This file contains the data structures for usbd module.
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : Usbd.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 * Description      : This file contains the data structures for usbd module.
 *
 ******************************************************************************/

#ifndef __USBD_H__
#define __USBD_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Include Files */
#include "usbOsal.h"
#include "usbHst.h"
#include "objLib.h"
#include "usbHcdInstr.h"

/* To be removed - Start */

#define USBD_MAX_HCD_NUM   4         /* to hold the max # of host controllers */
#define USBD_MAX_BUS_NUM   8         /* to hold the max # if bus in all the HCD */
#define USBD_ADDRESS_MAP_SIZE  (128/(8*sizeof(UINT8))) /* max # of devices */
#define USBD_INVALID_CRC_VALUE  0    /* invalid CRC value for configuration */
#define USBD_PARSE_AND_CREATE 0x01   /* used as flag to parse and create pipe */
#define USBD_PARSE_ONLY       0x02   /* used as flag to parse only */
#define CRC16_MASK          0xFFFF   /* CRC16 info */
#define CRC16_INITIAL_HOLD  0xFFFF   /* CRC16 info */
#define CRC16_GENERATOR_POLY 0x8005  /* 1000 0000 0000 0101b */

/***************************************************************************
* 
* USBD_INCREMENT_FUNCTIONAL_DEVICE_COUNT - Increment the functional 
* device count
* 
* This Macro is used to increment a functional device count on the 
* specified bus <uBusIndex>.
*                              
* RETURNS: N/A
*/
#define USBD_INCREMENT_FUNCTIONAL_DEVICE_COUNT(uBusIndex)                   \
{                                                                           \
    /* Lock the bus before modifying functional count */                    \
    OS_WAIT_FOR_EVENT((gUSBBusInfoList[(uBusIndex)].busOperationLock),     \
                       OS_WAIT_INFINITE);                                   \
                                                                            \
    /* Increment the functional count of the bus */                         \
    gUSBBusInfoList[(uBusIndex)].uFunctionalCount++;                       \
                                                                            \
    /* Release the lock */                                                  \
    OS_RELEASE_EVENT(gUSBBusInfoList[(uBusIndex)].busOperationLock);       \
}

/***************************************************************************
*
* USBD_DECREMENT_FUNCTIONAL_DEVICE_COUNT - decrement a functional device
* count 
*
* This Macro is used to decrement a functional device count on the
* specified bus <uBusIndex>.
*                              
* RETURNS: N/A
*/
#define USBD_DECREMENT_FUNCTIONAL_DEVICE_COUNT(uBusIndex)                   \
{                                                                           \
    /* Lock the bus before modifying functional count */                    \
    OS_WAIT_FOR_EVENT((gUSBBusInfoList[(uBusIndex)].busOperationLock),     \
                      OS_WAIT_INFINITE);                                    \
    /* Just to check the count is not zero */                               \
    OS_ASSERT (gUSBBusInfoList[(uBusIndex)].uFunctionalCount != 0);         \
    /* Decrement the functional count of the bus */                         \
    gUSBBusInfoList[(uBusIndex)].uFunctionalCount--;                       \
                                                                            \
    /* Release the lock */                                                  \
    OS_RELEASE_EVENT((gUSBBusInfoList[(uBusIndex)].busOperationLock));     \
}

/********************** STRUCTURES DEFINITION *********************************/

/*
 * This structure is used to maintain the list of Class Drivers registered
 * with USBD
 */
typedef struct usbd_device_driver_list
    {
    pUSBHST_DEVICE_DRIVER             pDriver;      /* pointer to driver */
    struct usbd_device_driver_list  *pNextDriver;    /* pointer to next driver */
    } USBD_DEVICE_DRIVER_LIST, *pUSBD_DEVICE_DRIVER_LIST;


/* Structure to hold the endpoint information */
typedef struct usbd_endpoint_info
    {
    UINT8  uEndpointAddress; /* Address of the end Point */
    UINT32 hPipe;            /* Ptr to pipe corresponding to the end point */    
    } USBD_ENDPOINT_INFO, *pUSBD_ENDPOINT_INFO;


/*
 * This structure is used to store the information about an interface required
 * by a USB device.
 */
typedef struct usbd_interface_info
    {
    UINT8      uInterfaceNum;      /* Interface number */
    UINT8      uAlternateSetting;  /* Alt settting value for this interface */
    UINT8      uNumEndpoints;      /* # of end points in the interface */
    UINT8      uInterfaceClass;    /* Class code for the interface */
    UINT8      uInterfaceSubClass; /* Sub class ocde for the interface */
    UINT8      uInterfaceProtocol; /* protocol code  for the interface */
    pUSBHST_DEVICE_DRIVER pDriver; /* Ptr to the DEVICE_DRIVER struct */
    void *      pDriverData;        /* Driver data specific to interface */
    pUSBD_ENDPOINT_INFO pEndpointInformation; /* Ptr to end point info */
    } USBD_INTERFACE_INFO, *pUSBD_INTERFACE_INFO;


/* This structure is used to store the information about a USB device */
typedef struct usbd_device_info
{
    UINT32    hDevice;             /* Handle to the device */
    UINT8     uDeviceSpeed;        /* Speed of the device */
    UINT8     uDeviceAddress;      /* Device addr on particular USB bus */ 
    UINT8     uBusIndex;           /* Index of the bus in global bus list */
    UINT16    uHighSpeedHubInfo;   /* Nearest high speed hub and port number 
                                    * info; used for split transfer to full/low
                                    * speed devices. high byte holds high speed 
                                    * address, low byte is port number
                                    */
    UINT32    hDefaultPipe;        /* Ptr to pipe for end point 0 */
    UINT8     uDeviceClass;        /* Class code of the device */
    UINT8     uDeviceSubClass;     /* Sub class code of the device */
    UINT8     uDeviceProtocol;     /* Protocol code of the device */
    UINT16    uVendorID;           /* Vendor Id of the device */
    UINT16    uDeviceID;           /* Device Id for device */
    UINT16    uBCDDevice;          /* USB device release version binary coded decimal */
    pUSBHST_DEVICE_DRIVER pDriver; /* Store the driver data */
    void *     pDriverData;         /* Driver data specific to interface */
    UINT8     uInterfaceNumber;    /* Interface # */
    UINT8     uInterfaceCount;     /* # of interfaces supported */
    UCHAR    *pCurrentConfiguration; /* Ptr to hold config of the device */
    UINT8     uNumConfigurations;   /* # of possible configurations */
    UINT16    uConfigCRCValue;     /* CRC Value */     
    pUSBD_INTERFACE_INFO pInterfacesInfo; /* Info about interfaces */ 
    struct usbd_device_info *pNextUSBDeviceInfo; /* Ptr to next USB devinfo */

    } USBD_DEVICE_INFO, *pUSBD_DEVICE_INFO;


/*
 * To store the information about a bus and also to maintain a list of USB
 * devices connected on the specific bus.
 */
typedef struct usbd_bus_info 
    {
    UINT8  uDeviceAddressMap[USBD_ADDRESS_MAP_SIZE]; /* 
                                                      * Device address map 
                                                      * of devs on bus.
                                                      */
    UINT8   uRelativeBusIndex;  /* rellative bus index in the host controller */
    UINT8   hcdIndex;           /* Host controller driver index */ 
    UINT32  hDefaultPipe;       /* Handle to the default pipe */
    UINT32  uFunctionalCount;   /* Count of functional interfaces/devices */
    OS_EVENT_ID busOperationLock;  /* Lock used while accessing list */
    pUSBD_DEVICE_INFO   pUSBDeviceInfoList; /* info about devices connected */
    } USBD_BUS_INFO, *pUSBD_BUS_INFO;


/**************************** FUNCTIONS DECLARATION ***************************/

/*******************************************************************************
 * Function Name    : usbdMatchDriver
 * Description      : This function scans the driver list for the driver that
 *                    can support the specified device.
 * Parameters       : bFlagVendorSpecific   IN  Flag TRUE if device is
 *                                              ClassSpecific; FALSE if
 *                                              VendorSpecific.
 *                    uParam1               IN  Deivce ID or Class Code
 *                    uParam2               IN  Vendor ID or Sub Class Code
 *                    uParam3               IN  USB revision number
 *                    pDriver               OUT Pointer to structure containing
 *                                              the matched device driver.
 *                                              hDeviceHandle if no driver is
 *                                              matched.
 * Return Type      : USBHST_INVALID_PARAMETER if Parameters are not valid.
 *                    USBHST_SUCCESS if matching driver is found
 *                    USBHST_FAILURE if no matching driver is found
 ******************************************************************************/
LOCAL USBHST_STATUS usbdMatchDriver
                  (
                  BOOL                     bFlagVendorSpecific,
                  UINT16                   uParam1,
                  UINT16                   uParam2,
                  UINT16                   uParam3,
                  pUSBHST_DEVICE_DRIVER    *pDriver
                   );

/*******************************************************************************
 * Function Name    : USBD_FindDevices
 * Description      : This function scans the USB bus for the devices that can
 *                    be supported for the specified class driver. The class
 *                    driver is loaded for all the devices identified.
 * Parameters       : pDeviceDriverInfo IN  Information about the class driver
 *                                          for which the devices are searched.
 * Return Type      : USBHST_INVALID_PARAMETER if Parameters are not valid.
 *                    USBHST_SUCCESS if all supported device by this driver,
 *                    if any, are added successfully.
 ******************************************************************************/
LOCAL USBHST_STATUS usbdFindDevices
                  (
                  pUSBHST_DEVICE_DRIVER pDeviceDriverInfo
                  );

/*******************************************************************************
 * Function Name    : USBD_FindDriver
 * Description      : This function scans the driver list for the driver that
 *                    can support the specified device.
 * Parameters       : pUSBDeviceInfo    IN  Information about the USB device
 *                                          for which the driver is searched.
 * Return Type      : USBHST_INVALID_PARAMETER if Parameters are not valid.
 *                    USBHST_SUCCESS if Drivers for the specified device is
 *                    searched and loaded, if found, successfully.
 ******************************************************************************/
LOCAL USBHST_STATUS usbdFindDriver
                  (
                  pUSBD_DEVICE_INFO pUSBDeviceInfo
                  );

/*******************************************************************************
 * Function Name    : USBHST_NewDevice
 * Description      : This function handles a new device connection. The Hub
 *                    Class Driver calls this function, when a device connection
 *                    is detected.
 * Parameters       : uSpeed        IN  Speed of the device
 *                    uBusIndex     IN  Index of bus on which the device is
 *                                      connected.
 *                    hHighSpeedHub IN  Handle to the nearest high speed hub for
 *                                      the device
 *                    uHighSpeedHubPortNumber
 *                                  IN  Port number of the nearest high speed hub
 *                                      for the device
 *                    pDeviceHanle  OUT Parameter to hold the device handle
 * Return Type      : USBHST_INVALID_PARAMETERS if Parameters are not valid.
 *                    USBHST_INSUFFICIENT_RESOURCES    Resources not sufficient.
 *                    USBHST_INSUFFICIENT_MEMORY   Memory insufficient.
 *                    USBHST_FAILURE   Operation is failed.
 *                    USBHST_SUCCESS   New device is created and set to
 *                    address state successfully.
 ******************************************************************************/

LOCAL USBHST_STATUS usbHstDeviceNew 
                  (
                  UINT8       uSpeed,
                  UINT8       uBusIndex,
                  UINT32      hHighSpeedHub,
                  UINT8       uHighSpeedHubPortNumber,
                  UINT32     *pDeviceHandle
                  );

/*******************************************************************************
 * Function Name    : USBHST_ConfigureDevice
 * Description      : This function loads the class driver for a newly connected
 *                    device.
 * Parameters       : hDevice   IN  Handle to the device being configured.
 * Return Type      : USBHST_INVALID_PARAMETERS if Parameters are not valid.
 *                    USBHST_INSUFFICIENT_MEMORY if Memory is insufficient.
 *                    USBHST_FAILURE if Operation failed.
 *                    USBHST_SUCCESS if Driver for device is loaded, if found.
 ******************************************************************************/
LOCAL USBHST_STATUS usbHstDeviceConfigure
                  (
                  UINT32 hDevice
                  );

/*******************************************************************************
 * Function Name    : USBHST_RemoveDevice
 * Description      : This function will call the remove function of the driver
 *                    for each of the interface in active configuration of the
 *                    device, free the device address and remove the device
 *                    from the corresponding bus.
 * Parameters       : hDevice   IN  Handle to the device to be removed.
 * Return Type      : None
 ******************************************************************************/
LOCAL VOID usbHstDeviceRemove
                  (
                  UINT32 hDevice
                  );

/*******************************************************************************
 * Function Name    : USBHST_SuspendDevice
 * Description      : This function will be called by hub when a device is
 *                    suspended.
 * Parameters       : hDevice   IN  Handle to the device to be suspended.
 * Return Type      : None
 ******************************************************************************/
LOCAL VOID usbHstDeviceSuspend
                  (
                  UINT32 hDevice
                  );

/*******************************************************************************
 * Function Name    : USBHST_ResumeDevice
 * Description      : This function will be called by hub when the device resume
 * Parameters       : hDevice   IN  Handle to the device to be suspended.
 * Return Type      : None.
 ******************************************************************************/
LOCAL VOID usbHstDeviceResume
                  (
                  UINT32 hDevice
                  );

/*******************************************************************************
 * Function Name    : USBD_LINK_DEVICE_INFO
 * Description      : This function is used to link specified device information
 *                    to the specified bus.
 * Parameters       : uBusIndex      IN  Bus Index to which the device
 *                                       information is to be linked.
 *                    pDeviceInfo    IN  Pointer to device Information.
 * Return Type      : None
 ******************************************************************************/
LOCAL void usbdLinkDeviceInfo
          (
          UINT8             uBusIndex,
          pUSBD_DEVICE_INFO pDeviceInfo
          );

/*******************************************************************************
 * Function Name    : USBD_UNLINK_DEVICE_INFO
 * Description      : This function is used to unlink specified device
 *                    information to the specified bus.
 * Parameters       : uBusIndex      IN  Bus Index to which device information
 *                                       is to be linked.
 *                    pDeviceInfo    IN  Pointer to device Information.
 * Return Type      : None
 ******************************************************************************/
LOCAL void usbdUlinkDeviceInfo
          (
          UINT8             uBusIndex,
          pUSBD_DEVICE_INFO pDeviceInfo
          );

/*******************************************************************************
 * Function Name    : USBD_GENERATE_DEVICE_HANDLE
 * Description      : This function is used to generate unique device handle
 *                    for USB device on specified bus.
 * Parameters       : pHandle        OUT Pointer to hold the generated handle.
 *                    uBusIndex      IN  Bus Index.
 *                    uDeviceAddress IN  Address of the device.
 * Return Type      : None
 ******************************************************************************/
LOCAL  void usbdGenerateDeviceHandle
          (
          UINT32 *   pHandle,
          UINT8      uBusIndex,
          UINT8      uDeviceAddress
          );

/*******************************************************************************
 * Function Name    : USBD_TRANSLATE_DEVICE_HANDLE
 * Description      : This function is used to translate device handle
 *                    to USB device information.       .
 * Parameters       : uHandle        IN  Device handle to be translated.
 *                    pDeviceInfo    OUT Pointer to hold the translated device
 *                                       information.
 * Return Type      : None
 ******************************************************************************/
LOCAL  void usbdTranslateDeviceHandle
          (
          UINT32             uHandle,
          pUSBD_DEVICE_INFO  *pDeviceInfo
          );

/*******************************************************************************
 * Function Name    : USBD_GetFreeAddress
  * Description     : This function returns the free address in the given bus.
 * Parameters       : uBusIndex     IN  Index of bus on which the device is
 *                                      connected.
 *                    pAddress      OUT Free address available.
 * Return Type      : USBHST_FAILURE if no free address is found
 *                    USBHST_SUCCESS if free address is found
 ******************************************************************************/
LOCAL USBHST_STATUS usbdGetFreeAddress
                  (
                  UINT8   uBusIndex,
                  UINT8 * pAddress
                  );


/*******************************************************************************
 * Function Name    : USBD_ReserveAddress
  * Description     : This function reserves the specified address on the
 *                    specified bus
 * Parameters       : uBusIndex     IN  Index of bus on which the device is
 *                                      connected.
 *                    uAddress      IN  Address to be reserved.
 * Return Type      : USBHST_FAILURE if the address is not found
 *                    USBHST_SUCCESS if free address is reserved.
 ******************************************************************************/
LOCAL USBHST_STATUS usbdReserveAddress
                  (
                  UINT8  uBusIndex,
                  UINT8  uAddress
                  );

/*******************************************************************************
 * Function Name    : USBD_ReleaseAddress
 * Description      : This function releases the specified address on the
 *                    specified bus.
 * Parameters       : uBusIndex         IN  Index of bus on which the device is
 *                                          connected.
 *                    uAddress          IN  Address to be released
 * Return Type      : USBHST_FAILURE if the parameters are invalid or the
 *                    address not found
 *                    USBHST_SUCCESS if address is released
 ******************************************************************************/
LOCAL USBHST_STATUS usbdReleaseAddress
                  (
                  UINT8   uBusIndex,
                  UINT8   uAddress
                  );


/*******************************************************************************
 * Function Name    : USBD_SetAddress
 * Description      : This function is used to issue SetAddress USB
 *                    Standard Request.
 * Parameters       : pDeviceInfo    IN  Pointer to USB Device info.
 * Return Type      : USBHST_INVALID_PARAMETERS if parameters are not valid.
 *                    USBHST_INSUFFICIENT_MEMORY on insufficient resource.
 *                    USBHST_TIMEOUT if request submitted to Host controller
 *                    driver is timed out.
 *                    USBHST_SUCCESS ifAddress of the device is set successfully
 ******************************************************************************/
LOCAL USBHST_STATUS usbdSetAddress
                  (
                  pUSBD_DEVICE_INFO pDeviceInfo
                  );

/*******************************************************************************
 * Function Name    : USBD_ParseConfiguration
 * Description      : This function is used to parse the configuration
 *                    and update the device info with interface info
 *                    and endpoint info.
 * Parameters       : pDeviceInfo       IN OUT  Pointer to USB Device info.
 *                    uParserFlag       IN      USBD_PARSE_ONLY:
 *                                              Parse without creating pipe
 *                                              USBD_PARSE_AND_CREATE
 *                                              Parse and create pipe
 * Return Type      : USBHST_INSUFFICIENT_MEMORY on insufficient resource.
 *                    USBHST_FAILURE if all the interfaces are not found in the
 *                    configuration.
 *                    USBHST_SUCCESS if configuration is parsed and device info
 *                    updated successfully.
 ******************************************************************************/
LOCAL USBHST_STATUS usbdParseConfiguration
                  (
                  pUSBD_DEVICE_INFO pDeviceInfo,
                  UINT8             uParserFlag
                  );

/*******************************************************************************
 * Function Name    : USBD_GetInterfaceOffset
 * Description      : This function is used to parse the configuration
 *                    and get the start of requested interface descriptor.
 * Parameters       : pConfigBuffer     IN      Pointer to configuration buffer.
 *                    uInterfaceNum     IN      Interface number.
 *                    uAltIndex         IN      Alternate setting index.
 *                    pStartOffset      OUT     Pointer to hold the start
 *                                              offset.
 * Return Type      : USBHST_FAILURE if interface is not found in the
 *                    configuration.
 *                    USBHST_SUCCESS if interface is found.
 ******************************************************************************/
LOCAL USBHST_STATUS usbdGetInterfaceOffset
                  (
                  UCHAR *  pConfigBuffer,
                  UINT16   uInterfaceNum,
                  UINT16   uAltIndex,
                  UINT32 * pStartOffset
                  );

/*******************************************************************************
 * Function Name    : USBD_CreatePipeForInterface
 * Description      : This function is used to parse the interface descriptor
 *                    create and update the endpoint info in device info.
 * Parameters       : pDeviceInfo       IN OUT  Pointer to USB Device info.
 *                    pNewInterfaceDesc IN      Pointer to start of the
 *                                              interface descriptor.
 *                    uInterfaceNum     IN      Interface number.
 * Return Type      : USBHST_INSUFFICIENT_MEMORY on insufficient resource.
 *                    USBHST_SUCCESS if pipe is created for all endpoints
 *                    successfully.
 ******************************************************************************/
LOCAL USBHST_STATUS usbdCreatePipeForInterface
                  (
                  pUSBD_DEVICE_INFO pDeviceInfo,
                  UCHAR *           pNewInterfaceDesc,
                  UINT16            uInterfaceNum
                  );

/*******************************************************************************
 * Function Name    : USBD_ComputeCRC
 * Description      : This function is used to compute the 16-bit CRC value
 *                    for the passed data buffer.
 * Parameters       : pBuffer           IN      Pointer to data buffer.
 *                    uLength           IN      Number of bytes in buffer.
 * Return Type      : 16-bit CRC value for the data buffer.
 *
 ******************************************************************************/
LOCAL UINT16 usbdComputeCRC
           (
           UCHAR *  pBuffer,
           UINT16  uLength
           );

/*******************************************************************************
 * Function Name    : USBD_ReverseHold16
 * Description      : This function is used to reverse the 16-bit hold value.
 * Parameters       : uHold             IN      Hold value to be reversed.
 * Return Type      : 16-bit reversed value for the hold value.
 ******************************************************************************/
LOCAL UINT16 usbdReverseHold16
           (
           UINT16 uHold
           );

/*******************************************************************************
 * Function Name    : USBD_ComputeCRCForByte
 * Description      : This function is used to compute 16-Bit CRC value for
 *                    passed 8-bit data.
 * Parameters       : cData             IN      8-Bit  value.
 *                    uHold             IN      16-Bit initial hold value.
 * Return Type      : 16-bit reversed value for the 8-Bit data.
 ******************************************************************************/
LOCAL  UINT16 usbdComputeCRCForByte
           (
           UCHAR       cData,
           UINT16      uHold
           );

/*******************************************************************************
 * Function Name    : USBD_IsValidEndpoint
 * Description      : This function is used to validate the endpoint descriptor.
 * Parameters       : pDeviceInfo     IN pointer to the device info.
 *                    pEndpointDesc   IN Pointer to endpoint descriptor.
 * Return Type      : TRUE            If the endpoint is valid.
 *                    FALSE           If the endpoint is invalid.
 ******************************************************************************/
LOCAL BOOL usbdIsValidEndpoint
          (
           pUSBD_DEVICE_INFO           pDeviceInfo,
           pUSBHST_ENDPOINT_DESCRIPTOR pEndpointDesc
           );

/*******************************************************************************
 * Function Name    : USBD_SubmitRequest
 * Description      : This function is used to submit a request to the HCD.
 * Parameters       : pDeviceInfo     IN pointer to the device info.
 *                    pEndpointDesc   IN Pointer to endpoint descriptor.
 * Return Type      : TRUE            If the endpoint is valid.
 *                    FALSE           If the endpoint is invalid.
 ******************************************************************************/

LOCAL USBHST_STATUS usbdSubmitRequest (pUSBD_DEVICE_INFO pDeviceInfo,
                                       UINT8          bmRequestType,
                                       UINT8          bRequest,
                                       UINT16         wValue,
                                       UINT16         wIndex,
                                       UINT32   *     pwLength,
                                       UCHAR    *     pBuffer);

 
/*******************************************************************************
 * Function Name    : USBHST_ClearTTRequest
 * Description      : This function is called by the Host controller driver to
 *                    submit Clear TT request to the Hub class driver
 * Parameters       : hHCDriver               IN Handle to HCD
 *                    uRelBusIndex            IN Relative Bus Index of the HCD
 *                    hHighSpeedHubAddress    IN Handle to the High speed hub
 *                    uHighSpeedHubPortNumber IN Port number on the high speed hub
 *                    wValue                  IN Contains additional information
 *                                               such as endpoint number,endpoint
 *                                               direction etc
 *                    pContext                IN HCD specific context
 * Return Type      : a) USBHST_FAILURE   if unsuccessful
 *                    b) USBHST_SUCCESS   if request was successful
 *                    c) USBHST_INVALID_PARAMETER if invalid parameters are passed
 * Global Variables :
 * Calls            :
 *
 * Called by        : None
 * To Do            : None
 ******************************************************************************/
LOCAL USBHST_STATUS usbHstClearTTRequest (UINT32 hHCDriver,
                                          UINT8  uRelBusIndex,
                                          UINT8  uHighSpeedHubAddress,
                                          UINT8  uHighSpeedPortNumber,
                                          UINT16 wValue,
                                          void *  pContext);

/*******************************************************************************
 * Function Name    : USBHST_ClearTTComplete
 * Description      : This function is called by the Hub class driver to
 *                    indicate completion of Clear TT request.
 * Parameters       : nStatus                 IN  Status of the operation.
 *                    hHighSpeedHub           IN  Handle to the High speed hub
 *                    pContext                IN  HCD specific Context
 * Return Type      : a) USBHST_FAILURE   if unsuccessful
 *                    b) USBHST_SUCCESS   if request was successful
 * Global Variables :
 * Calls            :
 *
 * Called by        : None
 * To Do            : None
 ******************************************************************************/
LOCAL USBHST_STATUS   usbHstClearTTComplete (USBHST_STATUS nStatus,
                                             UINT32        hHighSpeedHub,
                                             void *        pContext);
	   
/*******************************Reset************************************************
 * Function Name    : USBHST_ResetTTRequest
 * Description      : This function is called by the Host controller driver to
 *                    submit Reset TT request to the Hub class driver
 * Parameters       : hHCDriver               IN Handle to HCD
 *                    uRelBusIndex            IN Relative Bus Index of the HCD
 *                    hHighSpeedHubAddress    IN Handle to the High speed hub
 *                    uHighSpeedHubPortNumber IN Port number on the high speed hub
 *                    pContext                IN HCD specific context
 * Return Type      : a) USBHST_FAILURE   if unsuccessful
 *                    b) USBHST_SUCCESS   if request was successful
 *                    c) USBHST_INVALID_PARAMETER if invalid parameters are passed
 * Global Variables :
 * Calls            :
 *
 * Called by        : None
 * To Do            : None
 ******************************************************************************/
LOCAL USBHST_STATUS usbHstResetTTRequest (UINT32 hHCDriver,
				                          UINT8  uRelBusIndex,
                                          UINT8  uHighSpeedHubAddress,
                                          UINT8  uHighSpeedPortNumber,
                                          void * pContext);

/*******************************************************************************
 * Function Name    : USBHST_ResetTTComplete
 * Description      : This function is called by the Hub class driver to
 *                    indicate completion of Reset TT request.
 * Parameters       : nStatus   IN  Status of the operation.
 *                    hHighSpeedHub           IN Handle to the High speed hub
 *                    pContext                IN HCD specific context
 * Return Type      : a) USBHST_FAILURE   if unsuccessful
 *                    b) USBHST_SUCCESS   if request was successful
 * Global Variables :
 * Calls            :
 *
 * Called by        : None
 * To Do            : None
 ******************************************************************************/
LOCAL USBHST_STATUS   usbHstResetTTComplete (USBHST_STATUS nStatus,
                                             UINT32        hHighSpeedHub,
                                             void *        pContext);
                                       

#ifdef __cplusplus
}
#endif /* __cplusplus */ 


#endif /* End of __USBD_H__ */
/**************************** End of file Usbd.h ******************************/
