/* pciBusLib.h - PCI bus header file */

/* Copyright (c) 2005 Wind River Systems, Inc. */

/* 
modification history
--------------------
01b,01sep05,mdo  Add vxb prefix
01a,10aug05,mdo  Phase in new access method
17Jan05,tor  written
*/

#ifndef INC_BUSPCILIB_H
#define INC_BUSPCILIB_H

#ifdef __cplusplus
extern "C" {
#endif

/* defines */

#define PCI_CONTROLLER_METHOD_CFG_READ              0x01
#define PCI_CONTROLLER_METHOD_CFG_WRITE             0x02
#define PCI_CONTROLLER_METHOD_BUS_ACCESS_OVERRIDE   0x03

/* typedefs */

typedef struct busPciID         PCI_DEVVEND;
typedef struct busPciRegInfo    PCI_DRIVER_REGISTRATION;
typedef struct busPciDevInfo    PCI_HARDWARE;
typedef struct busPciIntrInfo   PCI_INTERRUPT_RECORD;

/*
 *  The PCI busType uses DEV_ID and VEND_ID to pair devices up
 *  with their drivers.  The driver provides a busPciRegInfo
 *  structure when it registers with the bus subsystem.  If the
 *  bus subsystem finds that the driver registered a bus type
 *  of PCI (or one of the PCI variants), and finds that PCI is
 *  present on the system, then the bus subsystem calls the
 *  PCI-specific device check routine.
 *
 *  In addition, the PCI bus tracks the standard bus, device,
 *  and function values.
 */

struct busPciID
    {
    UINT16  pciDevId;
    UINT16  pciVendId;
    };

struct busPciRegInfo
    {
    struct vxbDevRegInfo b;
    int         idListLen;
    struct busPciID *   idList;
    };

struct busPciDevInfo
    {
    void *  pCookie;
    UINT8   pciBus;
    UINT8   pciDev;
    UINT8   pciFunc;
    UINT16  pciDevId;
    UINT16  pciVendId;
    };

struct busPciIntrInfo
    {
    void *  pCookie;    /* PCI bus cookie: see pciAutoConfigLib */

    /* [b,d,f] */
    UINT8   pciBus;     /* bus: device identification */
    UINT8   pciDev;     /* device: device identification */
                        /* function: not required. */

    /* PCI interrupt lines */
    UINT32  intA;       /* interrupt vector on int-A */
    UINT32  intB;       /* interrupt vector on int-B */
    UINT32  intC;       /* interrupt vector on int-C */
    UINT32  intD;       /* interrupt vector on int-D */

    /* interrupt route functions */
    FUNCPTR intEnable;  /* enable interrupt line on intr controller */
    FUNCPTR intDisable; /* disable interrupt line on intr controller */
    FUNCPTR intAcknowledge; /* acknowledge & clear int line on intr ctrl */
    };

IMPORT STATUS pciBusLibInit();
IMPORT STATUS pciRegister();
IMPORT void pciBusAnnounceDevices
    (
    struct vxbAccessList *      pArg,
    struct vxbDev *             pDev,
    void *                      pCookie
    );

#ifdef __cplusplus
}
#endif

#endif /* INC_BUSPCILIB_H */
