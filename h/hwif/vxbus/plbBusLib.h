/* plbBusLib.h - Processor Local Bus header file */

/* Copyright (c) 2005 Wind River Systems, Inc. */

/* 
modification history
--------------------
01c,16sep05,jb  Adding interrupt level support
01b,01sep05,mdo  Add vxb prefix
01a,10aug05,mdo  Phase in new access method
17Jan05,tor  written
*/

#ifndef INC_PLBBUSLIB_H
#define INC_PLBBUSLIB_H

#ifdef __cplusplus
extern "C" {
#endif


/* defines */

/* typedefs */

typedef struct plbDevEntry  PLB_DEVICE_ENTRY;
typedef struct busPlbRegInfo    PLB_DRIVER_REGISTRATION;
typedef struct plbIntrEntry PLB_INTERRUPT;

struct plbIntrEntry
    {
    int         numVectors;
    UINT32 *    intVecList;
    UINT32 *    intLvlList;
    };

struct plbDevEntry
    {
    void *      baseAddress;
    char        drvName[MAX_DRV_NAME_LEN+1];
    int         numVectors;
    UINT32 *    intVecList;
    UINT32 *    intLvlList;
    };

struct busPlbRegInfo
    {
    struct vxbDevRegInfo    b;
    };

/* global data */

/* forward declarations */

IMPORT STATUS plbInit1();
IMPORT STATUS plbBusLibInit();
IMPORT void plbRegister(void);


#ifdef __cplusplus
}
#endif

#endif /* INC_PLBBUSLIB_H */
