/* taskLib.h - generic kernel interface header */

/*
 * Copyright (c) 1984-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */
 
/*
modification history
--------------------
06w,09sep05,jln  added _WRS_ABSOLUTE_EXTERN(_vx_qNodeToPschedInfo)
06v,30aug05,yvp  Reworked VX_TASK macros to undo struct workaround.
06u,31aug05,jln  added TASK_QNODE_TO_PSCHEDINFO macro
06t,25aug05,kk   fix VX_TASK_INSTANTIATE macro
06s,11aug05,mmi  Change pad from int --> UINT32
06r,09aug05,mmi  Add pCpuPwrMgmtPState
06q,15aug05,yvp  Added include of sys/taskDefs.h.
06p,10aug05,yvp  Added macros VX_TASK and VX_TASK_INITIALIZE.
06o,04aug05,yvp  Added prototype for create/delete library init.
06n,02aug05,yvp  Converted while(0) to while ((0)) - suppress diab warnings.
06m,01apr05,kk   added kernelBaseLibP.h for IS_KERNEL_TASK, added some docs.
06l,04mar05,kk   added cpuTimeInfo1, cpuTimeInfo2, pSchedInfo to TCB
		 fixed apigen errors (SPR# 106451)
06k,25feb05,pcs  Modified prototype of fn. taskLibInit to accept distint stack
                 overflow and underflow sizes for both user and kernel tasks.
06j,01feb05,kk   added IS_KERNEL_TASK() (SPR# 104271)
06i,26oct04,fr   added taskOpenInit() routine (SPR 101491)
06h,04oct04,aeg  removed #ifndef _ASMLANGUAGE around #include msgQLibP.h
06g,03oct04,kk   surround include of msgQLibP.h by #ifndef _ASMLANGUAGE
06f,24sep04,dcc  added accessObjId and taskVarSemId to WIND_TCB.
06e,01oct04,pcm  increased the number of task create/delete hooks from 16 to 20
06d,22sep04,pcm  changed WIND_TCB field name msgPassing to msgQInfo
06c,22sep04,md   moved taskSafe()/taskUnsafe() to taskLibCommon.h
06b,02sep04,aeg  removed NULL pointer check from TASK_ID_VERIFY();
		 changed taskInit2() to taskInitExcStk().
06b,20jul04,tcr  fix TASK_LOCK/TASK_SAFE macros for WV_INSTRUMENTATION
06a,11jun04,aeg  changed definition of VX_DEALLOC_TCB (SPR #98106).
05z,15may04,hya  added VX_DEALLOC_TCB.
05y,05may04,cjj  removed I960 preprocessor directives
05x,30mar04,jb   Renaming I80X86 pad entry to reflect new use
05w,29mar04,dcc  brought back VX_TASK_OBJ_OWNER option bit.
05v,17feb04,dcc  adjusted offsets in WIND_TCB and added currentCtx member.
05u,22jan04,aeg  moved coprocessor related task option bit to taskLibCommon.h
04w,09dec03,dlk  Restored reserved2 field for I80X86 cpu family only.
04v,02apr03,dtr  Reseving TCB slot for compiler.
04u,03sep02,dtr  Adding VX_SPE_TASK option.
05t,07nov03,tcr  allow WV instrumentation to OBJ_VERIFY
05s,26aug03,tcr  update WindView instrumentation
05r,31oct03,dcc  added some members to the WIND_TCB: pEdrInfo, pCompilerData,  
		 pDbgInfo, pShellInfo and pUTcb. Reserved 8bytes for WRAMP use.
		 Removed windxLock member. Adjusted offsets as a result of 
		 OBJ_CORE expansion.
05q,30oct03,pcm  implemented changes from code review
05p,20oct03,pcm  added new TCB field <pExcStackStart>; replaced various
		 priority inheritance TCB fields with <pPriInheritData>
05p,09sep03,kk   change taskLibInit prototype, added excCnt to TCB, 
		 made VX_USER_MODE = 0, added VX_DEALLOC_EXC_STACK option bit,
		 added taskInit2(), renamed pKStackXXX to pExcStackXXX.
05o,05sep03,dcc  moved common definitions with user side to taskLibCommon.h
05n,19aug03,dbt  Added PAL coprocessor support for VxSim.
05m,24jul03,kam  updated offset comments for all simulators
05l,10apr03,pes  PAL Coprocessor Abstraction
05k,21jul03,kk   changed tcbPad[0,1] to pKStackBase and pKStackPtr
05j,08jul03,kk   added VX_USER_MODE option bit for user mode task
05i,15jul03,kam  rebased to Base6 integration branch
05h,23jun03,ymz  added 8-byte alignment padding for MIPS.
05g,20jun03,to   deleted dummyNode, move rtpId, resurrect reserved2.
05f,13may03,to   added rtpId to TCB, added task option VX_TASK_OBJ_OWNER,
		 deleted K&R style prototypes.
05e,14apr03,to   replaced activeNode with dummyNode in TCB for now
05d,14apr03,to   made task name a copy in TASK_DESC instead of a pointer.
05c,20mar03,ymz  moved OBJ_CORE as the first element of WIND_TCB 
05b,16jan02,to   added fpStatus to TCB for ARM.
05a,17apr03,dbt  Added SIMLINUX support.
04z,27mar03,jmp  removed unused fields from SIMNT TCB, removed SUNOS and HPPA
		 simulators refernces.
04y,14mar03,jmp  removed windowsThreadId and windowsThread from SIMNT TCB.
04x,19nov01,aeg  added td_events to TASK_DESC.
04w,09nov01,jhw  Revert WDB_INFO to reside inside WIND_TCB.
04v,06nov01,gls  cleaned up compilation warnings
04u,06nov01,dee  fixed offsets in coldfire specific portion
		 for EXC_INFO and REG_SET per sh-coldfire.tcl
04t,30oct01,jhw  Removed wdbInfo and wdbExitHook from WIND_TCB.
	    gsl  Reused wdbExitHook for pPthread.
04s,22oct01,dee  Mergeed from T2.1.0 ColdFire
04r,04oct01,yvp  Fixed SPR69306: added member pCoprocCtx to PPC section for
		 Altivec support.
04q,27sep01,aeg  changed comment on WIND_TCB reservedX fields.
04p,24sep01,s_l  Fixed a systax error on line "UINT32 regSetPad[2]" missing ';'
04o,18sep01,aeg  deleted selectSem, pad1, & pSelWakeupNode from WIND_TCB,
		 and added pSelContext, events, pWdbTaskInfo, & wdbExitHook;
		 added code barriers to TASK_LOCK and TASK_SAFE macros.
04n,01mar00,frf  Added SH4 support for T2.
04m,10sep98,kab  expanded WDB_INFO struct to match TOR2.x., moved pDspContext
		 to end of TCB, replaced spare4.
04l,22jul98,mem  added pDspContext to TCB, removed spare4. Added VX_DSP_TASK.
04k,23apr97,hk   added SH support.
04j,14mar01,pcs  Added VX_ALTIVEC_TASK.
04i,12mar99,dbs  added COM task-local storage to TCB
04h,29may98,cym  added windowsContextValid flag for SIMNT.
04g,24apr98,kkk  moved wdbInfo after ARM stuff in TCB. (bug created by merge)
04f,19aug97,cym  added SIMNT support.
04e,28nov96,cdp  added ARM support.
04d,06nov97,dbt  modified WIND_TCB for new debugger scheme. Removed Am29K
		 family support.
04c,22jul96,jmb  merged ease patch for HP Sim
04b,26may94,yao  added PPC support.
04a,26may95,ms   added wdbExitHook field to the TCB
03z,16may95,rrr  added WDB_INFO structure to TCB.
03y,12jul95,ism  added simsolaris support
03x,19mar95,dvs  removed #ifdef TRON - tron no longer supported.
03w,15mar94,smb  modified TASK_SAFE macro
03v,24jan94,smb  added instrumentation macros
03u,10dec93,smb  instrumented TASK_LOCK macro
03t,02dec93,pme  added Am29K family support
03s,12nov93,hdn  added support for I80X86
03r,15oct93,cd   removed junk values from MIPS TCB.
03q,16sep93,jmm  added S_taskLib_ILLEGAL_PRIORITY and taskPriRangeCheck
03p,11aug93,gae  vxsim hppa.
03o,20jun93,rrr  vxsim.
03m,11feb93,jcf  added __PROTOTYPE_5_0 for compatibility.
03l,08feb93,smb  added a null check to taskIdVerify.
03k,13nov92,dnw  changed declaration of pSmObjTcb to struct sm_obj_tcb *
		 removed include of smObjTcbP.h (SPR #1768)
		 moved typedef of DBG_INFO here from dbgLib.h
		 removed include of dbgLib.h (SPR #1768)
03j,22sep92,rrr  added support for c++
03i,21sep92,smb  removed exit prototype and added include of stdlib.h
03h,02aug92,jcf  changed reserved3 field to pExcRegSet for exception handling.
03g,29jul92,smb  made modification for the stdio library.
03f,28jul92,jcf  added windxLock/reserved[12]; moved dbgInfo/pSmTcbObj;
		 removed funcRestart.
03e,12jul92,yao  replace pDbgState pointer with data structure DBG_INFO
		 in WIND_TCB.  added dbgPCWSave to i960.
03d,19jul92,pme  added shared memory objects support.
03c,10jul92,jwt  moved padding out of REG_SET in into WIND_TCB for SPARC.
03b,06jul92,ajm  removed taskSummary from forward declarations
03a,04jul92,jcf  cleaned up.
02y,16jun92,yao  made pDbgState available for all architectures.
02x,26may92,rrr  the tree shuffle
		  -changed includes to have absolute path from h/
02w,19mar92,yao  added ANSI prototype for taskStackAllot().
02v,12mar92,yao  removed ifdef CPU.  added taskRegsShow().
02u,10jan92,jwt  added CPU_FAMILY==SPARC architecture dependent prototypes.
02t,11nov91,rrr  Added funcRestart to tcb for signals.
02s,28oct91,wmd  Added changes for i960KB from Intel.
02r,04oct91,rrr  passed through the ansification filter
		  -fixed #else and #endif
		  -changed VOID to void
		  -changed ASMLANGUAGE to _ASMLANGUAGE
		  -changed copyright notice
02q,20sep91,hdn  deleted foroff from WIND_TCB for TRON.
		 added pDbgState to WIND_TCB for TRON.
02p,20aug91,ajm  made architecture independant.
02o,10jun91,del  added pragma for gnu960 alignment.
02n,23may91,wmd  added defines and macros for SPARC architecture.
02m,29apr91,hdn  added defines and macros for TRON architecture.
02l,08apr91,jdi  added NOMANUAL to prevent mangen.
02k,24mar91,del  added pDbgState for use with new dbgLib. And I960 defines.
02j,16oct90,shl  made #else ANSI style.
02i,05oct90,dnw  deleted private functions.
		 made taskSpawn, taskInit, taskCreate take var args.
02h,05oct90,shl  added ANSI function prototypes.
		 made #endif ANSI style.
		 added copyright notice.
02g,01oct90,jcf  added addtional spares to WIND_TCB.
02f,03aug90,jcf  moved arch dependent portion of WIND_TCB to end of struct
02e,13jul90,rdc  added support for environment variables and additional
		 select functionality.
02d,30jun90,jcf  added assembly language defines.
02c,26jun90,jcf  removed obsolete generic status codes
		 changed inheritance protocol.
		 changed safetyQSem to a safetyQHead.
		 reworked priority mananagement.
		 changed topOfStack to endOfStack.
02b,17may90,rdc  changed select semaphores in tcb to be binary semaphores.
02a,17apr90,jcf  added error codes.
		 changed to wind 2.0.
01l,16mar90,rdc  added select semaphore to tcbx.
01k,25may89,gae  added VX_FORTRAN option.
01j,21apr89,jcf  added KERNEL_{UNINIT,VRTX,PSOS,WIND}.
01i,07nov88,rdc  added VX_ADA_DEBUG to task options.
01h,22jun88,dnw  name tweaks.
01g,30may88,dnw  changed to v4 names.
01f,28may88,dnw  deleted obsolete status values.
		 added EXC_INFO to tcbx.
01e,18may88,jcf  added psos semaphore head to tcbx.
		  extended maximum number of hooks to 10.
01d,13may88,rdc  added signal info to tcbx.
01c,28apr88,ecs  added IMPORTs of idle & taskName.
01b,13apr88,gae  added function declarations; option bit VX_STDIO;
		 taskStd[] to TCB extension.
01a,25jan88,jcf  written by extracting from vxLib.h v02l.
*/

#ifndef __INCtaskLibh
#define __INCtaskLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <vxWorks.h>
#include <vwModNum.h>
#include <classLib.h>
#include <qLib.h>
#include <regs.h>
#include <excLib.h>
#include <private/eventP.h>
#include <private/semLibP.h>
#include <private/objLibP.h>
#include <private/eventLibP.h>
#include <private/msgQLibP.h>
#include <private/kernelBaseLibP.h>	/* kernelId for IS_KERNEL_TASK */
#if defined (_WRS_PAL_COPROC_LIB)
#include <private/coprocLibP.h>
#endif /* _WRS_PAL_COPROC_LIB */
#include <stdlib.h>
#include <taskLibCommon.h>
#include <sys/taskDefs.h>

/* miscellaneous */

#define MAX_TASK_ARGS		10	/* max args passed to a task */
#define VX_MAX_TASK_SWITCH_RTNS	16	/* max task switch callout routines */
#define VX_MAX_TASK_SWAP_RTNS	16	/* max task swap callout routines */
#define VX_MAX_TASK_DELETE_RTNS	20	/* max task delete callout routines */
#define VX_MAX_TASK_CREATE_RTNS	20	/* max task create callout routines */

/* kernel only task option bits (see taskLibCommon.h for common option bits) */

#define VX_UNBREAKABLE		0x0002	/* breakpoints ignored */

#define VX_STDIO		0x0010	/* OBSOLETE: need not be set for stdio*/
#define VX_ADA_DEBUG		0x0020	/* 1 = VADS debugger support */
#define VX_FORTRAN		0x0040	/* 1 = NKR FORTRAN support */

/* VX_PRIVATE_ENV		0x0080 	   defined in taskLibCommon.h */
/* VX_NO_STACK_FILL 		0x0100     defined in taskLibCommon.h */
/* VX_TASK_NOACTIVATE  		0x2000	   defined in taskLibCommon.h */
/* VX_NO_STACK_PROTECT		0x4000 	   defined in taskLibCommon.h */

/* the following are now defined in sys/taskDefs.h */

/* #define VX_USER_MODE		0x0000	   INTERNAL: tasks in user mode */
/* #define VX_SUPERVISOR_MODE	0x0001	   INTERNAL: tasks in sup mode */
/* #define VX_DEALLOC_STACK	0x0004	   INTERNAL: deallocate stack */
/* #define VX_TASK_OBJ_OWNER	0x0800	   INTERNAL: task owns objs directly */
/* #define VX_DEALLOC_EXC_STACK	0x1000	   INTERNAL: deallocate the exc stack */
/* #define VX_DEALLOC_TCB	0x8000	   INTERNAL: deallocate the task TCB */

/* typedefs */

#ifndef	_ASMLANGUAGE

typedef struct wdb_info		/* task debugging state info */
    {
    int			wdbState;	/* debug state */
    REG_SET *		wdbRegisters;	/* cause of trace exception */
    struct
	{
	void *	wdb1;
	void *	wdb2;
	} wdbEvtList;			/* dll_t of ptr to breakpoint info */
    int			bpAddr;		/* address of last breakpoint */
					/* encountered */
    int			taskBpAddr;	/* task breakpoint address */
    int			taskPc;		/* task program couner */
    int			taskFp;		/* task frame pointer */
    int			taskSp;		/* task stack pointer */
    VOIDFUNCPTR		wdbExitHook;	/* WDB notify routine on task exit */
  } WDB_INFO;

typedef struct priInheritData
    {
    UINT	priMutexCnt;	/* 0x00: # of inv safe sems a task has */
    UINT	priInheritCnt;	/* 0x04: # of times task priority bumped */
    int		priNormal;	/* 0x08: normal running priority */
    int		priDrop;	/* 0x0c: priority to drop task to */
    SEMAPHORE * pPriMutex;	/* 0x10: pointer to mutex task pends on */
    } PRI_INHERIT_DATA;

/*
 * ############################# WARNING ###############################
 *
 * The WIND_TCB structure below will be moved into the private/taskLibP.h
 * header in the next major release of VxWorks. In future releases, direct 
 * access to fields in the WIND_TCB structure will not be supported. Please
 * migrate to using the available published APIs in the task libraries
 * to access fields in the TCB.
 */

/*
 * INTERNAL: When adding fields to WIND_TCB, be sure to update the
 * offsets in private/taskLibP.h.
 */

typedef struct windTcb		/* WIND_TCB - task control block */
    {
    OBJ_CORE		objCore;	/* 0x00: object management */

    Q_NODE		qNode;		/* 0x40: multiway q node: rdy/pend q */
    Q_NODE		tickNode;	/* 0x50: multiway q node: tick q */

    char *		pExcStackBase;	/* 0x60: ptr to exc stack base */
    char *		pExcStackEnd;	/* 0x64: ptr to exc stack end */
    char *		pExcStackStart; /* 0x68: ptr to start of exc stack */
    int			excCnt;		/* 0x6c: exception next cnt */

    UINT		status;		/* 0x70: status of task */
    UINT		priority;	/* 0x74: task's current priority */
    PRI_INHERIT_DATA *	pPriInheritData;/* 0x78: ptr to inheritance data */
    UINT		lockCnt;	/* 0x7c: preemption lock count */

    /* SH arch. requires for the previous members to be within a 0x7f offset */    
    RTP_ID   		currentCtx;	/* 0x80: task's current context */ 
    RTP_ID		rtpId;		/* 0x84: real time process ID */

    int			options;	/* 0x88: task option bits */
    void *		pUTcb;		/* 0x8c: ptr to user level TCB */
    UINT		tslice;		/* 0x90: current count of time slice */

    UINT16		swapInMask;	/* 0x94: task's switch in hooks */
    UINT16		swapOutMask;	/* 0x96: task's switch out hooks */

    Q_HEAD *		pPendQ;		/* 0x98: q head pended on (if any) */

    UINT		safeCnt;	/* 0x9c: safe-from-delete count */
    Q_HEAD		safetyQHead;	/* 0xa0: safe-from-delete q head */

    FUNCPTR		entry;		/* 0xb0: entry point of task */

    char *		pStackBase;	/* 0xb4: points to bottom of stack */
    char *		pStackEnd;	/* 0xb8: points to end of stack */
    void *		pEdrInfo;	/* 0xbc: ptr to ED&R info */

    int			errorStatus;	/* 0xc0: most recent task error */
    int			exitCode;	/* 0xc4: error passed to exit () */

    struct sigtcb *	pSignalInfo;	/* 0xc8: ptr to signal info for task */
    struct selContext *	pSelectContext;	/* 0xcc: ptr to select info for task */

    UINT		taskTicks;	/* 0xd0: total number of ticks */
    UINT		taskIncTicks;	/* 0xd4: number of ticks in slice */

    struct taskVar *	pTaskVar;	/* 0xd8: ptr to task variable list */
    struct rpcModList *	pRPCModList;	/* 0xdc: ptr to rpc module statics */
#if defined (_WRS_PAL_COPROC_LIB)
    COPROC_TBL_ENTRY *	pCoprocTbl;	/* 0xe0: fpoint coprocessor context */
#else /* _WRS_PAL_COPROC_LIB */
    struct fpContext *	pFpContext;	/* 0xe0: fpoint coprocessor context */
#endif /* _WRS_PAL_COPROC_LIB */

    struct __sFILE *	taskStdFp[3];	/* 0xe4: stdin,stdout,stderr fps */
    int			taskStd[3];	/* 0xf0: stdin,stdout,stderr fds */

    char **		ppEnviron;	/* 0xfc: environment var table */
    int			envTblSize;	/* 0x100: number of slots in table */
    int			nEnvVarEntries; /* 0x104: num env vars used */
    struct sm_obj_tcb *	pSmObjTcb;	/* 0x108: shared mem object TCB */
    void *		pComLocal;	/* 0x10c: COM task-local storage ptr */
    REG_SET *		pExcRegSet;	/* 0x110: exception regSet ptr or NULL*/
    EVENTS		events;		/* 0x114: event info for the task */

    WDB_INFO *		pWdbInfo;	/* 0x120: ptr to WDB info -future use */
    void *		pDbgInfo;       /* 0x124: ptr to DBG info -future use */

    void *		pPthread;	/* 0x128: ptr to pthread data structs */
    void *		pCompilerData;	/* 0x12c: ptr to thread and EH data */
    void *		pShellInfo;	/* 0x130: ptr to kernel shell info */
    MSGQ_TCB		msgQInfo;	/* 0x134: used for message queues */
    OBJ_ID		accessObjId;    /* 0x13c: objId task is accessing */
    SEM_ID	 	taskVarSemId;	/* 0x140: task variable mutex */

    /* extensions provided for plug-in schedulers */

    void *		pSchedInfo;	/* 0x144: ptr to scheduler info */
    UINT64		cpuTimeInfo[2]; /* 0x148: sched extension - CPU time */
    void *              pPwrMgmtPState; /* 0x158: cpu pwr mgmt Task P-State */
    int			reserved1;	/* 0x15c: possible WRS extension */
    int			reserved2;	/* 0x160: possible WRS extension */
    int			spare1;		/* 0x164: possible user extension */
    int			spare2;		/* 0x168: possible user extension */
    int			spare3;		/* 0x16c: possible user extension */
    int			spare4;		/* 0x170: possible user extension */

    /* ARCHITECTURE DEPENDENT */

#if (CPU_FAMILY==MC680X0)
    EXC_INFO		excInfo;	/* 0x174: exception info */

    /* REG_SET must be aligned on a 4 byte boundary */

    REG_SET		regs;		/* 0x188: register set */
    UINT16		foroff;		/* 0x1d0: format/offset from frame */
    UINT16		pad2;		/* 0x1d2: pad format/offset to UINT */
#endif	/* CPU_FAMILY==MC680X0 */

#if (CPU_FAMILY==COLDFIRE)
    EXC_INFO		excInfo;	/* 0x174: exception info */
    REG_SET		regs;		/* 0x188: register set */
#endif	/* CPU_FAMILY==COLDFIRE */

#if (CPU_FAMILY==MIPS)

    /* make excInfo on 8-byte boundary */
    UINT32              pad;            /* 0x174: padding for align */
    EXC_INFO		excInfo;	/* 0x178: exception info */

    /* REG_SET should be on a 8-byte boundary */

    REG_SET		regs;		/* 0x198: register set */
#endif	/* CPU_FAMILY==MIPS */

#if (CPU_FAMILY == PPC)
    EXC_INFO		excInfo;	/* 0x174: exception info */
    UINT		pCoprocCtx;	/* 0x198: pointer to Coprocessor Ctx */

    /* REG_SET must be aligned on a 16 byte boundary */
    int                 pad;            /* 0x19c: padding for 16 bytes align */
    REG_SET		regs;		/* 0x1a0: register set */
#endif /* (CPU_FAMILY == PPC) */

#if (CPU_FAMILY == SPARC)
    EXC_INFO		excInfo;	/* 0x174: exception info */

    /* REG_SET must be aligned on an 8 byte boundary */

    REG_SET		regs;		/* 0x188: register set */
#endif	/* CPU_FAMILY == SPARC */

#if (CPU_FAMILY==SIMSPARCSOLARIS)
    EXC_INFO		excInfo;	/* 0x174: exception info */

    /* REG_SET must be aligned on an 8 byte boundary */
    int                 pad;            /* 0x194: pad for 8 bytes align */
    REG_SET		regs;		/* 0x198: register set */
#endif /* CPU_FAMILY==SIMSPARCSOLARIS*/

#if (CPU_FAMILY==SIMNT)
    EXC_INFO		excInfo;	/* 0x174: exception info */

    /* REG_SET must be aligned on a 4 byte boundary */

    REG_SET             regs;           /* 0x188: register set */
#endif /* CPU_FAMILY==SIMNT */

#if (CPU_FAMILY==SIMLINUX)
    EXC_INFO            excInfo;        /* 0x174: exception info */

    /* REG_SET must be aligned on a 4 byte boundary */

    REG_SET             regs;           /* 0x188: register set */
#endif /* CPU_FAMILY==SIMLINUX */

#if CPU_FAMILY==I960			/* ARCHITECTURE DEPENDENT */
    EXC_INFO		excInfo;	/* 0x174: exception info */

    /* REG_SET must be aligned on a 16 byte boundary */
    int                 pad[3];         /* 0x194: pad for 16 bytes align */
    REG_SET		regs;		/* 0x1a0: register set */
    UINT32		dbgPCWSave;	/* 0x?: PCW saved for debugger */
#if CPU==I960KB
    UINT8		intResumptionRec[16];
#endif
#endif /* CPU_FAMILY==I960 */

#if (CPU_FAMILY==I80X86)
    EXC_INFO		excInfo;	/* 0x174: exception info */
    CTX_SET *   	ctxSet; 	/* 0x1cc: ptr to extra context */

    /* REG_SET must be aligned on an 8 byte boundary for PENTIUM */
    REG_SET		regs;		/* 0x1d0: register set */
#endif /* CPU_FAMILY==I80X86 */

#if (CPU_FAMILY==SH)
    EXC_INFO		excInfo;	/* 0x174: exception info */

    /* REG_SET must be aligned on a 4 byte boundary */

    REG_SET		regs;		/* 0x188: register set */
#endif	/* CPU_FAMILY==SH */

#if (CPU_FAMILY==ARM)
    EXC_INFO		excInfo;	/* 0x174: exception info */

    UINT32		fpStatus;	/* 0x184: fp status word */

    /* REG_SET must be aligned on a 16 byte boundary */

    UINT32		pad[2];		/* 0x188: pad to align REG_SET */
    REG_SET		regs;		/* 0x190: register set */
#endif /* CPU_FAMILY==ARM */

#if !defined (_WRS_PAL_COPROC_LIB)
    struct dspContext *	pDspContext;	/* dsp coprocessor context */
#else /* _WRS_PAL_COPROC_LIB */
    void *		pUnused;
#endif /* _WRS_PAL_COPROC_LIB */
    WDB_INFO		wdbInfo;	/* target debug info */
    } WIND_TCB;

#if CPU_FAMILY==I960
#pragma align 0			/* turn off alignment requirement */
#endif	/* CPU_FAMILY==I960 */

/*******************************************************************************
*
* TASK_ID_VERIFY - check the validity of a task
*
* This macro verifies the existence of the specified object by comparing the
* task's classId with the expected class id.  If the task does not check
* out, errno is set with the appropriate invalid id status.
*
* RETURNS: OK or ERROR if invalid task id
*
* \NOMANUAL
*/

#define TASK_ID_VERIFY(tid)						      \
    (									      \
    (OBJ_VERIFY (&((WIND_TCB *)(tid))->objCore, taskClassId))	      	      \
    )

/*******************************************************************************
*
* TASK_LOCK - lock preemption for calling task
*
* This macro performs the taskLock (2) routine.
* windview - this macro has been instrumented for windview.
*
* \NOMANUAL
*/

#ifdef WV_INSTRUMENTATION
#define TASK_LOCK()							     \
    do									     \
	{								     \
	WRS_ASM ("");							     \
	EVT_CTX_0 (EVENT_TASKLOCK);					     \
	taskIdCurrent->lockCnt++;					     \
	WRS_ASM ("");							     \
	} while ((0))
#else /* WV_INSTRUMENTATION */
#define TASK_LOCK()							     \
    do									     \
	{								     \
	WRS_ASM ("");							     \
	taskIdCurrent->lockCnt++;					     \
	WRS_ASM ("");							     \
	} while ((0))
#endif /* WV_INSTRUMENTATION */

/*******************************************************************************
*
* TASK_UNLOCK - unlock preemption for calling task
*
* This macro performs the taskUnlock (2) routine.
*
* \NOMANUAL
*/

#define TASK_UNLOCK()							\
    (									\
    taskUnlock ()							\
    )

/*******************************************************************************
*
* TASK_SAFE - guard self from deletion
*
* This macro performs the taskSafe (2) routine.
* windview - this macro has been instrumented for windview.
*
* \NOMANUAL
*/

#ifdef WV_INSTRUMENTATION
#define TASK_SAFE()							     \
    do									     \
	{								     \
	WRS_ASM ("");							     \
	EVT_OBJ_2 (taskIdCurrent, taskClassId, EVENT_TASKSAFE,               \
		   taskIdCurrent, taskIdCurrent->safeCnt);                   \
	taskIdCurrent->safeCnt++;					     \
	WRS_ASM ("");							     \
	} while ((0))
#else  /* WV_INSTRUMENTATION */
#define TASK_SAFE()							     \
    do									     \
	{								     \
	WRS_ASM ("");							     \
	taskIdCurrent->safeCnt++;					     \
	WRS_ASM ("");							     \
	} while ((0))
#endif /* WV_INSTRUMENTATION */

/*******************************************************************************
*
* TASK_UNSAFE - unguard self from deletion
*
* This macro performs the taskUnsafe (2) routine.
*
* \NOMANUAL
*/

#define TASK_UNSAFE()							\
    (									\
    taskUnsafe ()							\
    )

/*******************************************************************************
*
* IS_KERNEL_TASK - determine a task is a kernel task
*
* This macro returns 1 if the specified task is a kernel task. Otherwise
* 0 or if tid == 0, return the ID of the current task.
*
* \NOMANUAL
*/

#define IS_KERNEL_TASK( tid ) 						\
    (									\
    (((tid) == 0 ? taskIdCurrent:(WIND_TCB *)(tid))->rtpId == kernelId) \
    )

/******************************************************************************
*
* TASK_QNODE_TO_PSCHEDINFO - get the pSchedInfo from a task qNode
*
* This macro returns the address of "pSchedInfo" of WIND_TCB to which a "qNode"
* belongs. This macro is intended to be used by user specified schedulers, when
* the component INCLUDE_CUSTOM_SCHEDULER is configured.
*
* NOTE: This macro should NOT be used in loadable objects since the absolute 
* symbols may not be resolved by the module loader.
*
* \NOMANUAL
*/
                                                                                
#define TASK_QNODE_TO_PSCHEDINFO(node)  \
    (                                   \
    (UINT)(node) + _vx_qNodeToPSchedInfo \
    )

/*******************************************************************************
*
* VX_TASK - declare storage for a task
*
* This macro declares storage for a task with name <name> with a stack size
* <stackSz> bytes. This macro only generates data declarations for the task.
* This macro is useful for declaring tasks statically (i.e. at compile
* time, without using any dynamic memory allocation at runtime). To fully 
* initialize the task, macro VX_TASK_INITIALIZE must be called. To initialize
* and activate the task, macro VX_TASK_INSTANTIATE must be called. 
*
* SEE ALSO: VX_TASK_INITIALIZE, VX_TASK_INSTANTIATE
*
* \NOMANUAL
*/

#define VX_TASK(name,stackSz)                                                 \
    WIND_TCB name##Tcb;                                                       \
    _WRS_DATA_ALIGN_BYTES(_STACK_ALIGN_SIZE)                                  \
        char name##Stk [STACK_ROUND_UP((stackSz))];                           \
    _WRS_DATA_ALIGN_BYTES(_STACK_ALIGN_SIZE)                                  \
        char name##ExcStk [STACK_ROUND_UP((KERNEL_TASK_EXC_STACK_SIZE))];


/*******************************************************************************
*
* VX_TASK_INITIALIZE - initialize a task declared with the VX_TASK macro
*
* This macro initializes a task declared previously with the VX_TASK macro.
* with name <name> having a stack size of <stackSz> bytes. This macro only 
* initializes the task and leaves it suspended. It returns a task ID for
* the initialized task on successful initialization, or NULL on failure. 
* To activate (i.e. schedule) the task, function taskActivate must be called 
* with the task id returned by this macro. Only then is the task functional.
* The arguments to this macro and their types are the same as those passed to
* function taskSpawn(). Please refer to the documentation for taskSpawn() for
* more details on the arguments.
*
* RETURNS: task ID on success or NULL on error. 
*
* Sample usage of this macro is as follows:
* \cs
*      #include <vxWorks.h>
*      #include <taskLib.h>
* 
*      VX_TASK(myTask,4096);
*      int myTaskId;
* 
*      STATUS initializeFunction (void)
*          {
*          myTaskId = VX_TASK_INITIALIZE(myTask, 100, 0, 4096, pEntry, \
*                                        0,1,2,3,4,5,6,7,8,9);
* 
*         if (myTaskId != NULL)
*             {
*             taskActivate (myTaskId);
*             return (OK);
*             }
*         else
*             return (ERROR);
*          }
* \ce
* 
* SEE ALSO: VX_TASK, VX_TASK_INSTANTIATE, taskSpawn()
* \NOMANUAL
*/

#define VX_TASK_INITIALIZE(name,prio,options,stackSz,entryPt,arg1,arg2,arg3,  \
                           arg4,arg5,arg6,arg7,arg8,arg9,arg10)               \
    ((taskInitExcStk (&name##Tcb, #name, (prio), (options),                   \
                      &name##Stk[STACK_ROUND_UP((stackSz))],                  \
                      STACK_ROUND_UP((stackSz)),                              \
                      &name##ExcStk                                           \
                      [STACK_ROUND_UP((KERNEL_TASK_EXC_STACK_SIZE))],         \
                      STACK_ROUND_UP((KERNEL_TASK_EXC_STACK_SIZE)),           \
                      ((FUNCPTR) (entryPt)), (arg1), (arg2), (arg3), (arg4),  \
                      (arg5), (arg6), (arg7), (arg8), (arg8), (arg10)) == OK) \
     ? ((int) &name##Tcb)                                                     \
     : ((int) NULL))


/*******************************************************************************
*
* VX_TASK_INSTANTIATE - initialize and activate a task declared using VX_TASK
*
* This macro initializes and activates a task declared previously with the 
* VX_TASK macro. The task is fully functional after the call to this macro
* if it succeeds. This macro returns a task ID for the instantiated task on 
* or NULL on failure. The arguments to this macro and their types are the same
* as those passed to function taskSpawn(). Please refer to the documentation
* for taskSpawn() for more details on the arguments.
* 
* RETURNS: task ID on success or ERROR.
* 
* Sample usage of this macro is as follows:
* \cs
*      #include <vxWorks.h>
*      #include <taskLib.h>
* 
*      VX_TASK(myTask,4096);
*      int myTaskId;
* 
*      STATUS initializeFunction (void)
*          {
*          myTaskId = VX_TASK_INSTANTIATE(myTask, 100, 0, 4096, pEntry, \
*                                         0,1,2,3,4,5,6,7,8,9);
* 
*         if (myTaskId != ERROR)
*             return (OK);                /@ instantiation succeeded @/
*         else
*             return (ERROR);
*          }
* \ce
*
* SEE ALSO: VX_TASK, VX_TASK_INITIALIZE, taskSpawn() 
* \NOMANUAL
*/

#define VX_TASK_INSTANTIATE(name,prio,options,stackSz,entryPt,arg1,arg2,arg3, \
                           arg4,arg5,arg6,arg7,arg8,arg9,arg10)               \
    (((VX_TASK_INITIALIZE(name,prio,options,stackSz,entryPt,arg1,arg2,arg3,   \
                          arg4,arg5,arg6,arg7,arg8,arg9,arg10) == NULL) ||    \
      (taskActivate ((int)&name##Tcb) != OK))                                 \
    ? (ERROR)                                                                 \
    : ((int) &name##Tcb))


/* variable declarations */

extern CLASS_ID	taskClassId;		/* task class id */
extern WIND_TCB * taskIdCurrent;	/* always current executing task */
extern BOOL	taskPriRangeCheck;	/* limit priorities to 0-255 */

/* _vx_qNodeToPSchedInfo is a private absolute symbol */

_WRS_ABSOLUTE_EXTERN(_vx_qNodeToPSchedInfo);

/* function declarations */

extern STATUS	taskLibInit 	(int usrDefExcStackSize,
                                 int usrDefExcStackOverflowSize,
                                 int kernelDefExcStackOverflowSize,
                                 int usrDefExecStackOverflowSize,
                                 int usrDefExecStackUnderflowSize,
                                 int kernelDefExecStackOverflowSize,
                                 int kernelDefExecStackUnderflowSize);
extern void	taskCreateLibInit (void);
extern void	taskOpenInit	(void);
extern STATUS	taskInit 	(WIND_TCB * pTcb, char * name, int priority, 
			  	 int options, char * pStackBase, int stackSize, 
			  	 FUNCPTR entryPt, int arg1, int arg2, int arg3, 
			  	 int arg4, int arg5, int arg6, int arg7, 
				 int arg8, int arg9, int arg10);
extern STATUS	taskInitExcStk 	(WIND_TCB * pTcb, char * name, int priority, 
			  	 int options, char * pStackBase, int stackSize, 
			  	 char * pExcStackBase, int excStackSize, 
				 FUNCPTR entryPt, int arg1, int arg2, int arg3, 
			  	 int arg4, int arg5, int arg6, int arg7, 
				 int arg8, int arg9, int arg10);
extern STATUS	taskOptionsSet 	(int tid, int mask, int newOptions);
extern WIND_TCB * taskTcb 	(int tid);
extern int	taskIdListGet 	(int idList [ ], int maxTasks);
extern STATUS	taskStatusString (int tid, char * pString);
extern STATUS	taskOptionsString (int tid, char * pString);
extern STATUS	taskRegsGet 	(int tid, REG_SET * pRegs);
extern STATUS	taskRegsSet 	(int tid, REG_SET * pRegs);
extern void	taskRegsShow 	(int tid);
extern void *	taskStackAllot 	(int tid, unsigned nBytes);
extern void	taskShowInit 	(void);
extern STATUS	taskShow 	(int tid, int level);
extern STATUS	taskLock 	(void);
extern STATUS	taskUnlock 	(void);

#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCtaskLibh */
