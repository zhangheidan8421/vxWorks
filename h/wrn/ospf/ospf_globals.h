/* ospf_globals.h - OSPF globals */

/* Copyright 2000-2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
03b,08jun05,dsk changed declaration of global semaphores 
03a,22aug03,agi ported to Accordion stack
02e,30may03,agi    removed ospfRawInputTaskId
02d,26may03,agi    Removed RWOS-specific references
02c,09may03,agi    Added global OSPF semaphores
02b,17feb02,ram	   SPR 81808 Added OSPF memory partition support
02a,28jan03,ram	   SPR 85050 Added new mutex for external route queue
01a,05jun96,cindy  First Beta Release
*/

/* the meta structure */

#if !defined (_OSPF_GLOBALS_H_)
#define _OSPF_GLOBALS_H_
typedef int (*FP_OSPF_RECEIVE) (register struct mbuf *message_block);

typedef int (*FP_OSPF_SEND) (register struct mbuf *message_block);


typedef struct OSPF_CALLBACK_IF
{
    int             ospfEnabled;
    FP_OSPF_RECEIVE     pOSpfReceive;
    FP_OSPF_SEND        pOSpfSend;

} OSPF_CALLBACK_IF;

/* the following iterators needs to be defined regardless if __OSPF_VIRTUAL_STACK__ preproc
 * is used.
 */

#if !defined (__OSPF_VIRTUAL_STACK__)
extern OSPF_CLASS ospf;

extern SEM_ID  ospf_global_mutex;
extern SEM_ID  ospf_config_mutex;
extern SEM_ID  ospf_external_route_mutex;

extern OSPF_CALLBACK_IF OspfRegistrationInfo;

extern char ospf_configuration_text[OSPF_MAXIMUM_FILE_SIZE + 1];

extern UINT ten_seconds_counter;

/* SPR 81808 -- Begin */
extern ULONG ospfMemPartId;
/* SPR 81808 -- End */

#endif /* __OSPF_VIRTUAL_STACK__ */
/*RFC 1765*/
extern ULONG ospf_db_overflow_timer;

#endif /* _OSPF_GLOBALS_H_ */



