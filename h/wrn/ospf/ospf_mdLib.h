/* mdLib.h - message digest library header file */

/* Copyright 2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01a,23mar04,zhr  created from mdLib.h 
*/

#ifndef __INCmdLibh
#define __INCmdLibh

#ifdef __cplusplus
extern "C" {
#endif

/* Algorithms: MD4 or MD5 */
typedef enum
{
        MD4 = 0x04,
        MD5 = 0x05
} MD_ALGORITHM;

#define HMAC_MD5_RESULT_LENGTH          16      /* HMAC-MD5 result length */
#define MD5_DIGEST_LEN 16

/* Prototypes */
extern void mdString(unsigned char *, unsigned long, unsigned char *,
                     MD_ALGORITHM);
extern void mdHmac5(unsigned char *, int, unsigned char *, int,
                    unsigned char *);

#ifdef __cplusplus
}
#endif

#endif /* __INCmdLibh */
