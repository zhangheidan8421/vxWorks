/* ospf_ip_structures.h  - OSPF IP structures */

/* Copyright 2000-2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
___________________
03a,06aug03,agi  ported to Accordion stack
02b,19nov02,mwv  Merge TMS code SPR 84284
02a,07oct02,agi  Fixed compiler warnings
01b,10oct01,jkw  Added port_number back to IP_ROUTE_ENTRY structure.
01a,22sep01,kc   Removed port_number from IP_ROUTE_ENTRY structure.
*/

#if !defined (__OSPF_IP_STRUCTURES_H__)
	#define __OSPF_IP_STRUCTURES_H__

#if __DCC__
__packed__ enum IP_ROUTE_TYPE
#else
enum IP_ROUTE_TYPE
#endif
{
    OTHER_ROUTE_TYPE	= 0x01,	/* none of the following */
    INVALID_ROUTE_TYPE = 0x02, 	/* an invalidated route */
    DIRECT_ROUTE_TYPE 	= 0x03,	/* route to directly connected (sub-)network */
    INDIRECT_ROUTE_TYPE = 0x04 	/* route to a non-local host/(sub-)network */
#if __DCC__
};
#else
} _pack;
#endif


#if __DCC__
__packed__ enum IP_PROTOCOL_VALUE
#else
enum IP_PROTOCOL_VALUE
#endif
{
	RESERVED_PROTOCOL	= 0,
	ICMP_PROTOCOL		= 1,
	IGMP_PROTOCOL		= 2,
	VX_IP_PROTOCOL		= 4,	/* 
					 * Original name IP_PROTOCOL changed due
					 * to variable name collision in 
					 * rwutil.h- V2 version
					 */
	TCP_PROTOCOL		= 6,
	IGP_PROTOCOL 		= 9,
	UDP_PROTOCOL		= 17,
	IDRP_PROTOCOL 		= 45,
	RSVP_PROTOCOL 		= 46,
	GRE_PROTOCOL		= 47,
	IPSEC_ESP_PROTOCOL	= 50,
	IPSEC_AH_PROTOCOL	= 51,
	IGRP_PROTOCOL		= 88,
	OSPF_PROTOCOL		= 89,
	IPIP_PROTOCOL		= 94,
	ETHERIP_PROTOCOL	= 97
#if __DCC__
};
#else
} _pack;
#endif


#if __DCC__
__packed__ enum IP_ROUTE_PROTOCOL_TYPE
#else
enum IP_ROUTE_PROTOCOL_TYPE
#endif
{
    OTHER_ROUTING_PROTOCOL_TYPE		= 0x01,
    IP_ROUTING_PROTOCOL_TYPE 	  	= 0x01,	/* 
						 * IP_ROUTING_PROTOCOL_TYPE is 
						 * intentionally defined as 1 
						 * as well 
						 */
    LOCALLY_CONFIGURED_TYPE 		= 0x02,
    NETWORK_MANAGEMENT_PROTOCOL_TYPE 	= 0x03,
    ICMP_PROTOCOL_TYPE 			= 0x04,
    EGP_PROTOCOL_TYPE 			= 0x05,
    GGP_PROTOCOL_TYPE 			= 0x06,
    HELLO_PROTOCOL_TYPE 		= 0x07,
    RIP_PROTOCOL_TYPE 			= 0x08,
    IS_IS_PROTOCOL_TYPE 		= 0x09,
    ES_IS_PROTOCOL_TYPE 		= 0x0a,
    CISCO_IGRP_TYPE 			= 0x0b,
    BBN_SPF_IGP_TYPE 			= 0x0c,
    OSPF_PROTOCOL_TYPE 			= 0x0d,
    BGP_PROTOCOL_TYPE 			= 0x0e
#if __DCC__
};
#else
} _pack;
#endif


#if __DCC__
__packed__ enum IP_ROUTE_OPERATION
#else
enum IP_ROUTE_OPERATION
#endif
{
	ADD_IP_ROUTE	= 0x01,
	DELETE_IP_ROUTE	= 0x02
#if __DCC__
};
#else
} _pack;
#endif


typedef _struct SERVICE_TYPE_BIT_STRUCTURE
{
#if (_BYTE_ORDER == _LITTLE_ENDIAN )
	unsigned char 		unused:2;
	unsigned char 		high_reliability:1;
	unsigned char 		high_throughput:1;
	unsigned char 		low_delay:1;
	unsigned char 		precedence:3;
#else /* _BYTE_ORDER == _BIG_ENDIAN */
	unsigned char 		precedence:3;
	unsigned char 		low_delay:1;
	unsigned char 		high_throughput:1;
	unsigned char		high_reliability:1;
	unsigned char		unused:2;
#endif /* _BYTE_ORDER == _LITTLE_ENDIAN */
} _pack SERVICE_TYPE_BIT_STRUCTURE;

typedef _union	UNION_SERVICE_TYPE_BIT_STRUCTURE
{
	SERVICE_TYPE_BIT_STRUCTURE			_bit;
	BYTE										_byte;
} _pack UNION_SERVICE_TYPE_BIT_STRUCTURE;


typedef _union UNION_USER_OPTIONS
{
	BYTE 			char_options[MAXIMUM_IP_OPTION_LENGTH];							/* according to rfc791 format in network order */
	ULONG			ulong_options[MAXIMUM_IP_OPTION_LENGTH / sizeof (ULONG)];
} _pack UNION_USER_OPTIONS;

typedef _struct IP_UPPER_LAYER_PARAMETERS
{
	ULONG 								source_address;
	ULONG 								destination_address;
	BYTE_ENUM (BOOLEAN) 				do_not_fragment_flag;
	SERVICE_TYPE_BIT_STRUCTURE		type_of_service;
	USHORT 								sequence_id;
	BYTE 									time_to_live;
	BYTE 									protocol;
	BYTE 									option_length;
	UNION_USER_OPTIONS 				user_options;
	void 									*vptr_cached_route; 			/* address of cached route stored in user's space */
	USHORT								virtual_port_number;			/* do not route, send out through this port if not NO_SUCH_PORT */
	ULONG 								gateway;
} _pack IP_UPPER_LAYER_PARAMETERS;

typedef _struct VERSION_LENGTH_BIT_STRUCTURE
{
#if (_BYTE_ORDER == _LITTLE_ENDIAN )
	unsigned char		header_length:4;
	unsigned char		version:4;
#else /* _BYTE_ORDER == _BIG_ENDIAN */
	unsigned char		version:4;
	unsigned char		header_length:4;
#endif /* _BYTE_ORDER == _LITTLE_ENDIAN */
} _pack VERSION_LENGTH_BIT_STRUCTURE;


typedef _struct FLAG_FRAGMENT_OFFSET_BIT_STRUCTURE
{
#if (_BYTE_ORDER == _LITTLE_ENDIAN )
	unsigned char		fragment_offset_most_significant_part:5;
	unsigned char		more_fragment_flag:1;
	unsigned char		do_not_fragment_flag:1;
	unsigned char		unused_bit:1;
#else /* _BYTE_ORDER == _BIG_ENDIAN */
	unsigned char		unused_bit:1;
	unsigned char		do_not_fragment_flag:1;
	unsigned char		more_fragment_flag:1;
	unsigned char		fragment_offset_most_significant_part:5;
#endif /* _BYTE_ORDER == _LITTLE_ENDIAN */
} _pack FLAG_FRAGMENT_OFFSET_BIT_STRUCTURE;

typedef _struct IP_HEADER
{
	VERSION_LENGTH_BIT_STRUCTURE 					version_header_length;
	SERVICE_TYPE_BIT_STRUCTURE						service_type;
	USHORT												total_length;
	USHORT												identifier;
	FLAG_FRAGMENT_OFFSET_BIT_STRUCTURE 			flags_fragment_offset;
	BYTE													fragment_offset_least_significant_part;
	BYTE													time_to_live;
	BYTE													protocol;
	USHORT												header_checksum;
	ULONG													source_ip_address;
	ULONG													destination_ip_address;
} _pack IP_HEADER;


typedef struct IP_PARAMETERS
{
	USHORT 												header_length;
	USHORT												total_length;
	ULONG 												source_address;
	ULONG 												destination_address;
	USHORT												offset;
	SERVICE_TYPE_BIT_STRUCTURE						type_of_service;
	USHORT 												id;
	BYTE 													time_to_live;
	BYTE_ENUM (IP_PROTOCOL_VALUE)					protocol;
	BYTE 													options_length;
	BYTE_ENUM (BOOLEAN) 								do_not_fragment_flag;
	BYTE_ENUM (BOOLEAN) 								more_fragment_flag;
	BYTE													version;
	USHORT												checksum;
	USHORT												rx_port_number;
	ULONG 												gateway;
	BYTE_ENUM (BOOLEAN) 								union_ip_packet_is_to_be_freed;
	BYTE_ENUM (BOOLEAN) 								packet_forwarded_by_upper_layer;
	BYTE_ENUM (BOOLEAN) 								packet_recognized_by_upper_layer;
} IP_PARAMETERS;

typedef _union	UNION_OPTIONS_OR_DATA
{
	BYTE 			data;
	BYTE			ip_options;
} _pack UNION_OPTIONS_OR_DATA;


typedef struct PSEUDO_IP_PARAMETERS
{
	ULONG 		source_address;
	ULONG 		destination_address;
	BYTE			zero_field;
	BYTE 			protocol;
	USHORT 		length;
} PSEUDO_IP_PARAMETERS;

#ifndef INVALID_SOCKET_DESCRIPTOR
#define INVALID_SOCKET_DESCRIPTOR 0
#endif


#ifndef SOCKET_OPEN_MODE_WAIT_FOR_MULTIPLE_CONNECTION
#define SOCKET_OPEN_MODE_WAIT_FOR_MULTIPLE_CONNECTION 0xFFFF
#endif

#ifndef WOULD_BLOCK_ERROR
#define WOULD_BLOCK_ERROR 0xFFFF
#endif

#endif /* __OSPF_IP_STRUCTURES_H__ */
