/* ospf_interface_state_machine.h */

/* Copyright 2000 Wind River Systems, Inc. */

/*modification history
___________________
 19,21may01,jkw Updated for point to point
 18,26september00,reshma		Added WindRiver CopyRight
 17,25september00,reshma		RFC-1587 implementation for OSPF NSSA Option, also tested against ANVL.
 16,20july00,reshma				Unix compatibility related changes.
 15,06july00,reshma				Removed unnecessary header files and defines.
 14,04april00,reshma			Added some MIB support (Read only).
								Passed all important ANVL OSPF tests.
 13,23december99,reshma			Compatibility with VxWorks-IP and VxWorks RTM-interface
 12,19may99,jack				redefined OSPF_PATRICIA_32_BITS_KEY_MAX_BIT_POSITION as per fixes in
								patricia
 11,28december98,jack			Compiled and added some comments
 10,11november98,jack			Config changes, linted and big endian changes
 09,30october98,jack			Incorporate changes for compilation on Vxworks
 08,23august98,jack				ANVL tested OSPF with PATRICIA tree route table and no recursion
 07,10august98,jack				PATRICIA Route Table Based OSPF Code Base
 06,08july98,jack				Patricia RT table related changes - need to be tested
 05,04june98,jack				Integration with RTM and BGP
 04,10july97,cindy				Pre-release v1.52b
 03,10february97,cindy			Release Version 1.52
 02,22october97,cindy			Release Version 1.50
 01,05june96,cindy				First Beta Release
*/

#if !defined (_OSPF_INTERFACE_STATE_MACHINE_H_)
#define _OSPF_INTERFACE_STATE_MACHINE_H_

typedef struct OSPF_INTERFACE_STATE_MACHINE
{
	void (*fptr_interface_transition_function) (OSPF_INTERFACE *sptr_interface);
} OSPF_INTERFACE_STATE_MACHINE;

typedef void (*INTERFACE_TRANSITION_FUNCTION) (OSPF_INTERFACE *sptr_interface);

OSPF_INTERFACE_STATE_MACHINE ospf_interface_event_processing_table[OSPF_NUMBER_OF_INTERFACE_EVENTS][OSPF_NUMBER_OF_INTERFACE_STATES] =
{
/* ------------------------------------------------------------------------------------------------------------------ */
/*										      OSPF_INTERFACE_UP         0      	       			 						  */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPF_INTERFACE_IS_DOWN		 					*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_up_event},
/* OSPF_INTERFACE_LOOPBACK	 					*/		{NULL},
/* OSPF_INTERFACE_WAITING		 					*/		{NULL},
/* OSPF_INTERFACE_POINT_TO_POINT				*/		{NULL},
/* OSPF_INTERFACE_DESIGNATED_ROUTER_OTHER	*/		{NULL},
/* OSPF_INTERFACE_BACKUP_DESIGNATED_ROUTER	*/		{NULL},
/* OSPF_INTERFACE_DESIGNATED_ROUTER			*/		{NULL}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*										       OSPF_WAIT_TIMER          1  	            			 					  */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPF_INTERFACE_IS_DOWN		 					*/		{NULL},
/* OSPF_INTERFACE_LOOPBACK	 					*/		{NULL},
/* OSPF_INTERFACE_WAITING		 					*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_wait_timer_event},
/* OSPF_INTERFACE_POINT_TO_POINT				*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_wait_timer_event},
/* OSPF_INTERFACE_DESIGNATED_ROUTER_OTHER	*/		{NULL},
/* OSPF_INTERFACE_BACKUP_DESIGNATED_ROUTER	*/		{NULL},
/* OSPF_INTERFACE_DESIGNATED_ROUTER			*/		{NULL}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*							     		      OSPF_BACKUP_SEEN          2     								  			  */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPF_INTERFACE_IS_DOWN		 					*/		{NULL},
/* OSPF_INTERFACE_LOOPBACK	 					*/		{NULL},
/* OSPF_INTERFACE_WAITING		 					*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_backup_seen_event},
/* OSPF_INTERFACE_POINT_TO_POINT				*/		{NULL},
/* OSPF_INTERFACE_DESIGNATED_ROUTER_OTHER	*/		{NULL},
/* OSPF_INTERFACE_BACKUP_DESIGNATED_ROUTER	*/		{NULL},
/* OSPF_INTERFACE_DESIGNATED_ROUTER			*/		{NULL}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*					   					    OSPF_NEIGHBOR_CHANGE          3       			 			 				  */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPF_INTERFACE_IS_DOWN		 				*/		{NULL},
/* OSPF_INTERFACE_LOOPBACK	 					*/	{NULL},
/* OSPF_INTERFACE_WAITING		 				*/	{NULL},
/* OSPF_INTERFACE_POINT_TO_POINT				*/		{NULL},
/* OSPF_INTERFACE_DESIGNATED_ROUTER_OTHER	*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_neighbor_change_event},
/* OSPF_INTERFACE_BACKUP_DESIGNATED_ROUTER	*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_neighbor_change_event},
/* OSPF_INTERFACE_DESIGNATED_ROUTER			*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_neighbor_change_event}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*										    OSPF_LOOP_INDICATION          4       			 			 				  */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPF_INTERFACE_IS_DOWN		 					*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_loop_indication_event},
/* OSPF_INTERFACE_LOOPBACK	 					*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_loop_indication_event},
/* OSPF_INTERFACE_WAITING		 					*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_loop_indication_event},
/* OSPF_INTERFACE_POINT_TO_POINT				*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_loop_indication_event},
/* OSPF_INTERFACE_DESIGNATED_ROUTER_OTHER	*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_loop_indication_event},
/* OSPF_INTERFACE_BACKUP_DESIGNATED_ROUTER	*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_loop_indication_event},
/* OSPF_INTERFACE_DESIGNATED_ROUTER			*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_loop_indication_event}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*							     		   OSPF_UNLOOP_INDICATION          5         			 			 		      */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPF_INTERFACE_IS_DOWN		 					*/		{NULL},
/* OSPF_INTERFACE_LOOPBACK	 					*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_unloop_indication_event},
/* OSPF_INTERFACE_WAITING		 					*/		{NULL},
/* OSPF_INTERFACE_POINT_TO_POINT				*/		{NULL},
/* OSPF_INTERFACE_DESIGNATED_ROUTER_OTHER	*/		{NULL},
/* OSPF_INTERFACE_BACKUP_DESIGNATED_ROUTER	*/		{NULL},
/* OSPF_INTERFACE_DESIGNATED_ROUTER			*/		{NULL}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*							 	            OSPF_INTERFACE_DOWN          6         			 			 				  */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPF_INTERFACE_IS_DOWN		 					*/		{NULL},
/* OSPF_INTERFACE_LOOPBACK	 					*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_down_event},
/* OSPF_INTERFACE_WAITING		 					*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_down_event},
/* OSPF_INTERFACE_POINT_TO_POINT				*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_down_event},
/* OSPF_INTERFACE_DESIGNATED_ROUTER_OTHER	*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_down_event},
/* OSPF_INTERFACE_BACKUP_DESIGNATED_ROUTER	*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_down_event},
/* OSPF_INTERFACE_DESIGNATED_ROUTER			*/		{(INTERFACE_TRANSITION_FUNCTION) ospf_process_interface_down_event}
}
};


#endif /* _OSPF_INTERFACE_STATE_MACHINE_H_ */
