/* ospf_mib_helper_update.h - helper routines for OSPF MIB API */

/* Copyright 1998-2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/* 
modification history
--------------------
03l,07jan05,xli porting external route redistribution feature from ospf2.3.1
03a,08dec03,agi merged fixes from OSPF 2.1 patch branch
02l,31oct03,kc  Fixed SPR#91541 - added wrnOspf2IpRoutePolicyCreateReqType to
                support wrnOspfToIpRouteTable configuration.
02k,16jun03,kc  Fixed SPR#85532 - modified valueBuf 12-14 for ospfIfCreateReqType.
                Obsolete ospfIfStatus and added wrnOspfLocalIfPassive to valueBuf.
02j,12jun03,kc  Fixed SPR#88863 - added wrnOspfRedistribXxxCreateReqType, 
                mApi2Ospf_configRedistrib() and mApi2Ospf_deleteRedistrib()
                prototypes to support wrnOspfRedistributionTable configuration.
02l,17jul03,kc  Fixed SPR#88975 - added ospf2Mapi_shutdown() prototype.
02k,12jun03,kc  Fixed SPR#88863 - added wrnOspfRedistribXxxCreateReqType, 
                mApi2Ospf_configRedistrib() and mApi2Ospf_deleteRedistrib()
                prototypes to support wrnOspfRedistributionTable configuration.
02j,12jun03,kc  Fixed SPR#88975 - added ospf2Mapi_shutdown() prototype.
02i,26may03,kc  Fixed SPR#87790 - removed ospfShutdownReqType ospf2MapiReqType_t.
02h,25may02,kc  Fixed PR #2700 - Added ospfIfDeleteReqType ospf2MapiReqType_t.
02g.25may02.kc  Fixed PR #2767- Added ospfIfChangeReqType ospf2MapiReqType_t. Also
                added ospf2MapiSysCtl_t data structure.
02f,09may03,kc  Fixed SPR#88389 - modified doc for ospfExtLsdbCreateReqType -
                added route protocol ID.
02e,25apr03,kc  Fixed SPR#87790 - removed ospfShutdownReqType ospf2MapiReqType_t.
02d,18apr02,kc  Removed ospf2Mapi_destroy() prototype.
02c,04apr02,kc  Modified doc for ospfGenGroupUpdateReqType request type - no longer
                include the ABR status.
02b,24mar02,kc  Changed ospf2Mapi_proto_register() return type from void to STATUS.
02a,15feb02,kc  Added wrnOspfIfCreateReqType ospf2MapiReqType_t.
01z,04feb02,kc  Update doc for ospfGenGroupCreateReqType request type.
01y,16jan02,kc  Added ospfLsdbCreateReqType,ospfExtLsdbCreateReqType, 
                wrnOspfLsdbCreateReqType, wrnOspfLocalLsdbCreateReqType, and
                wrnOspfExtLsdbCreateReqType ospf2MapiReqType_t.
01x,26dec01,kc  Update doc for ospfNbrUpdateReqType request type.
01w,19dec01,kc  Redefined ospf2Mapi_proto_register() prototype. Moved ospfProtoType_t
                enums to ospf_mib_api.h.
01v,28nov01,kc  Changed pValueBuf poiner type from uchar_t to char.
01u,21oct01,kc  Added ospfShutdownReqType ospf2MapiReqType_t.
01t,21oct01,kc  Modified NVRAM_DELETE_FUNCPTR arguments.
01s,21oct01,kc  Modified arguments for ospf2Mapi_proto_register()  prototype. Added 
                ospf2Mapi_proto_deregister() prototype.
01r,20oct01,kc  Added Area Aggregation relation prototypes and ospf2MapiReqType_t.
01q,19oct01,kc  Added __OPAQUE_LSA__ preproc.
01p,17oct01,kc  Added ospfProtoType_t enumeration values.
01o,16oct01,kc  Removed __WRN_OSPF_MIB__ preproc.
01n,13oct01,kc  Added more valueBuf[] explanations for ospfHostCreateReqType.
01m,13oct01,kc  Added ospf2MapiReqBuf_t structure def. (moved from ospf_mApi_helper.c)
01l,10oct01,kc  Added mApi2Ospf_configNbr() and mApi2Ospf_deletexxx() prototypes.
01k,08oct01,kc  Removed *arg argument from ospf2Mapi_request() prototype.
01j,07oct01,kc  Added support for WRN-OSPF MIB enumeration types.
01i,07oct01,kc  Added NVRAM_SAVE_FUNCPTR and NVRAM_RETRIEVE_FUNCPTR typedefs.
01h,07oct01,kc  Added ospf2Mapi_destroy() prototype.
01g,06oct01,kc  Removed ospfHostUpdateReqType ospf2MapiReqType_t since there is 
                nothing to update for ospfHostTable.
01f,04oct01,kc  Added protypes for ospf2Mapi_xxx_create()
01e,20sep01,kc  Added numOspfReqBufCnt argument to ospf2Mapi_init().
01d,18sep01,kc  Added ospfStubReqType and ospfIfMetricReqType ospf2MapiReqType_t.
01c,15sep01,kc  Added prototypes for ospf2Mapi_xxx() routines.
01b,12sep01,kc  Added prototypes for mApi2Ospf_xxx() routines.
01a,07sep01,kc  Initial file creation.
*/

/*
DESCRIPTION:

This file contains the functional prototypes for OSPF MIB helper routines. 

*/

#ifndef __INCospf_mib_helper_updateh
#define __INCospf_mib_helper_updateh

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* NOTE: Ideally, the following system dependent function pointers typedefs should
 *       be defined in ospf_mib_api.h. However, that requires OSPF core code to
 *       also include a laundary list of header files that it doesn't need to
 *       know (such as ospf_mib_avl_lib.h). Since ospf_mib_utils.h and this header file 
 *       are the only two header files that are included by OSPF code code, this header 
 *       file becomes the logical place for the following typedefs.
 */

/* this is the function pointer pointed to a system dependent save routine for saving
 * the given object to the nvram.
 */
typedef STATUS (* NVRAM_SAVE_FUNCPTR)
              ( 
              ulong_t   *pInstance,     /* pointer to OID instance array */
              ushort_t  instanceLen,    /* number of sub-ids in instance */
              ulong_t   oidPrefixEnum,  /* enum value for object OID Prefix */
              void      *pValueBuf,     /* pointer to value buffer */
              ulong_t   valueLen       /* value byte length */
              );

/* this is the function pointer pointed to a system dependent delete routine for 
 * deleting a table row from nvram.
 */
typedef STATUS (* NVRAM_DELETE_FUNCPTR) 
              ( 
              ulong_t   *pInstance,     /* pointer to OID instance array */
              ushort_t  instanceLen     /* number of sub-ids in instance */
              );

/* this is the function pointer pointed to a system dependent retrieve routine for
 * retrieving all prevously saved OSPF configuration from nvram. The retrieve routine
 * must invoke the MIB API for read-create objects in each ospf table with the 
 * mApiRequest_t set to MAPI_NVM_SET request type so that MIB API can repopulate its 
 * management database at system startup.
 */
typedef STATUS (* NVRAM_RETRIEVE_FUNCPTR) (void );

/* define the configuration type uses by the underlying OSPF protocol */
typedef enum {

    /* the following enumeration types are used by OSPF to update the statistical
     * and operational status
     */
    ospfGenGroupUpdateReqType = 1, /* update General Group read-only variables */
    ospfAreaUpdateReqType,         /* update an ospf area instance */ 
    ospfLsdbUpdateReqType,         /* update an ospf lsdb instance */
    ospfIfUpdateReqType,           /* update an ospf interface instance */
    ospfVirtIfUpdateReqType,       /* update an ospf virtual interface instance */
    ospfNbrUpdateReqType,          /* update an ospf neighbor instance */
    ospfVirtNbrUpdateReqType,      /* update an ospf virtual neighbor instance */
    ospfExtLsdbUpdateReqType,      /* update an ospf external lsdb instance */

    /* the following enumeration types are used by OSPF to create new instance of link
     * state database (including Opaques Lsa types)
     */
    ospfLsdbCreateReqType,        /* create an instance of lsdb */
    ospfExtLsdbCreateReqType,     /* create an ospf external lsdb instance */

    /* the following enumeration types are used by OSPF to delete an instance of a
     * particular table from MIB API
     */
    ospfLsdbDeleteReqType,         /* delete an ospf lsdb instance */
    ospfNbrDeleteReqType,          /* delete an ospf nbr instance */
    ospfVirtNbrDeleteReqType,      /* delete an ospf virtual neighbor instance */
    ospfExtLsdbDeleteReqType,      /* delete an ospf external lsdb instance */

    /* the following enumeration types are used by OSPF to create an instance of
     * ospf area, stub, interface, interface metric or virtual interface in MIB API 
     * during startup. These enumeration types are for backward compatibility to 
     * support the static configuration methodology. It shall not be used by any aother 
     * applications. These enumeration types will eventually be obsolote
     */
    ospfGenGroupCreateReqType,     /* setup ospf General Group scalar objects */
    ospfAreaCreateReqType,         /* create an instance of ospf area */
    ospfStubCreateReqType,         /* create an instance of ospf stub */
    ospfHostCreateReqType,         /* create an instance of ospf host interface */
    ospfIfCreateReqType,           /* create an instance of ospf interface */
    ospfIfMetricCreateReqType,     /* create an instance of ospf interface metric */
    ospfVirtIfCreateReq,           /* create an instance of ospf virtual interface */
    ospfAreaAggregateCreateReq,    /* create an instance of ospf area aggregate */

    ospfIfChangeReqType,   /* interface flag change event */
    ospfIfDeleteReqType,   /* interface address deleted event */

    /* the following enumeration types are for the wrn-ospf enterprise mib. 
     * wrnOspfLsdbTable is for Type-10 Opaque LSA, wrnOspfLocalLsdbTable
     * is for Type-9 Opaque LSA and wrnOspfExtLsdbTable is for Type-11 
     * Opaque LSA support
     */
#if defined(__OPAQUE_LSA__)
    wrnOspfGenGroupUpdateReqType,  /* update wrnOspf General Group */
    wrnOspfAreaUpdateReqType,      /* update an instance of wrnOspfAreaTable */
    wrnOspfLsdbUpdateReqType,      /* update an instance of wrnOspfLsdbTable */
    wrnOspfLocalLsdbUpdateReqType, /* update an instance of wrnOspfLocalLsdbTable */
    wrnOspfExtLsdbUpdateReqType,   /* update an instance of wrnOspfExtLsdbTable */

    wrnOspfLsdbCreateReqType,      /* create an instance of wrnOspfLsdbTable */
    wrnOspfLocalLsdbCreateReqType, /* create an instance of wrnOspfLocalLsdbTable */
    wrnOspfExtLsdbCreateReqType,   /* create an instance of wrnOspfExtLsdbTable */

    wrnOspfLsdbDeleteReqType,      /* delete an wrnOspfLsdbTable instance */
    wrnOspfLocalLsdbDeleteReqType, /* delete an wrnOspfLocalLsdbTable instance, Type-9 */
    wrnOspfExtLsdbDeleteReqType,    /* delete an wrnOspfExtLsdbTable instance */
#endif /* __OPAQUE_LSA__ */

    /* the following enumeration types are used by OSPF to create an instance of
     * wrn-ospf interface at startup. These enumeration types are for backward 
     * compatibility to support the static configuration methodology. It shall not 
     * be used by any aother applications. These enumeration types will eventually 
     * be obsolote
     */
    wrnOspfIfCreateReqType,        /* update an wrnOsfIfTable instance */

    /* the following enumeration types are used by OSPF to create an instance of
     * redistribution in the wrnOspfRedistributionTable at startup. These enumeration
     * types are for backward compatibility to support static configuration 
     * methodology and shall not be used by any other applications. These
     * enumeration types will eventually be obsolete.
     */
    wrnOspfRedistribStaticCreateReqType, /* for static and default route type */
    wrnOspfRedistribRipCreateReqType,    /* for rip route type */
    wrnOspfRedistribBgpCreateReqType,     /* for bgp route type */
    wrnOspfRedistribLocalCreateReqType,  /* for local route type */
    /* SPR#88863 end */
        
    /* SPR#91541 begin */
    wrnOspf2IpRoutePolicyCreateReqType  /* create an instance of wrnOspfToIpRouteTable */
    /* SPR#91541 end */
} ospf2MapiReqType_t;


/* ospf2MapiReqBuf_t structure is a generic data structure used internally by OSPF
 * protocol to provide operational status and statistics updates to the MIB API. 
 * In order to maintain backward compatibility with the existing static configuration
 * methodology, this data structure is also used by OSPF protocol at init time to update 
 * Management Database for any configuration that are statically created. This involves
 * creating the ospf area, stub area, interface, interface metric, and virtual interface 
 * instances in the Management Database. The ospf2MapiReqBuf_t data structure is 
 * initialized in the ospf2Mapi_request() function (invoked by the OSPF protocol).
 * Using this data strcuture, the OSPF protocol can provide the necessary updates to the
 * OSPF MIB API asynchronously, thus, minimize the disruption to the overall OSPF 
 * operation. Due to the multi-usage of the ospf2MapiReqBuf_t data structure, the 
 * representation of the data structure is highly depending on the type of request 
 * (defined as ospf2MapiReqType_t enumeration type). The following is the 
 * representation of the ospf2MapiReqBuf_t data structure for each ospf2MapiReqType_t 
 * request type:
 * 
 * ospfGeneralGroup (reqType = ospfGenGroupCreateReqType)
 * valueBuf[0] = the value of ospfRouterId  read-write object
 * valueBuf[1] = the value of ospfAdminStat read-write object
 * valueBuf[2] = the value of ospfASBdrRtrStatus read-write object
 * valueBuf[3] = the value of ospfTOSSupport read-write object
 * valueBuf[4] = the value of ospfExtLsdbLimit read-write object
 * valueBuf[5] = the value of ospfExitOverflowInterval read-write object
 * valueBuf[6] = the value of wrnOspfRFC1583Compatibility read-write object
 * valueBuf[7] = the value of wrnOspfOpaqueLsaSupport read-write object
 * valueBuf[8] = the value of wrnOspfRedistributeDefaultRoutes read-write object
 * valueBuf[9] = the value of wrnOspfRedistributeStaticRoutes read-write object
 * valueBuf[10] = the value of wrnOspfRedistributeRIPRoutes read-write object
 * valueBuf[11] = the value of wrnOspfRedistributeBGPRoutes read-write object
 *
 * ospfGeneralGroup (reqType = ospfGenGroupUpdateReqType )
 * valueBuf[0] = the value of ospfExternLsaCount read-only object
 * valueBuf[1] = the value of ospfExternLsaCksumSum read-only object
 * valueBuf[2] = the value of ospfOriginateNewLsas read-only object
 * valueBuf[3] = the value of ospfRxNewLsas read-only object
 * valueBuf[4] to valueBuf[14] = not used
 * pValueBuf = not use
 *
 * ospfAreaTable (reqType = ospfAreaUpdateReqType):
 * valueBuf[0] = the value of ospfAreaId index object
 * valueBuf[1] = the value of ospfSpfRuns read-only object
 * valueBuf[2] = the value of ospfAreaBdrRtrCount read-only object
 * valueBuf[3] = the value of ospfAsBdrRtrCount read-only object
 * valueBuf[4] = the value of ospfAreaLsaCount read-only object
 * valueBuf[5] = the value of ospfAreaLsaCksumSum read-only objects
 * valueBuf[6] to valueBuf[14] = not used
 * pValueBuf = not use
 *
 * ospfAreaTable (reqType = ospfAreaCreateReqType):
 * valueBuf[0] = the value of ospfAreaId index object
 * valueBuf[1] = the value of ospfImportAsExtern read-create object
 * valueBuf[2] = the value of ospfAreaSummary read-create object
 * valueBuf[3] to valueBuf[14] = not used
 * pValueBuf = not use
 *
 * ospfStubTable (reqType = ospfStubCreateReqType):
 * valueBuf[0] = the value of ospfStubAreaId index object
 * valueBuf[1] = the value of ospfStubTOS index object
 * valueBuf[2] = the value of ospfStubMetric read-create object
 * valueBuf[3] = the value of ospfStubMetricType read-create object
 * valueBuf[4] = the value of ospfStubStatus  read-create object
 * valueBuf[4] to valueBuf[14] = not used
 * pValueBuf = not use
 * 
 * ospfLsdbTable (reqType = ospfLsdbUpdateReqType):
 * valueBuf[0] = the value of ospfLsdbAreaId index object
 * valueBuf[1] = the value of ospfLsdbType index object
 * valueBuf[2] = the value of ospfLsdbLsid index object
 * valueBuf[3] = the value of ospfLsdbRouterId index object
 * valueBuf[4] = the value of ospfLsdbSequence read-only object
 * valueBuf[5] = the value of ospfLsdbAge read-only object
 * 
 * ospfLsdbTable (reqType = ospfLsdbCreateReqType):
 * valueBuf[0] = the value of ospfLsdbAreaId index object
 * valueBuf[1] = the value of ospfLsdbType index object
 * valueBuf[2] = the value of ospfLsdbLsid index object
 * valueBuf[3] = the value of ospfLsdbRouterId index object
 * valueBuf[4] = the value of ospfLsdbSequence read-only object
 * valueBuf[5] = the value of ospfLsdbAge read-only object
 * valueBuf[6]= the value of ospfLsdbChecksum read-only object
 * valueBuf[7] = the value of lsdbLen (not a MIB object)
 * valueBuf[8] and valueBuf[14] = not used
 * pValueBuf = pointer to the ospfLsdbAdvertisement read-only object
 * 
 * ospfLsdbTable (reqType = ospfLsdbDeleteReqType)
 * valueBuf[0] = the value of ospfLsdbAreaId index object
 * valueBuf[1] = the value of ospfLsdbType index object
 * valueBuf[2] = the value of ospfLsdbLsid index object
 * valueBuf[3] = the value of ospfLsdbRouterId index object
 * valueBuf[4] and valueBuf[14] = not used
 * pValueBuf = not use
 *
 * ospfHostTable (reqType = ospfHostCreateReqType):
 * valueBuf[0] = the value of ospfHostIpAddress index object
 * valueBuf[1] = the value of ospfHostTOS index object
 * valueBuf[2] = the value of ospfHostMetric read-create object
 * valueBuf[3] = the value of ospfHostAreaId read-only object
 * valueBuf[4] = the value of ospfHostStatus read-create object
 * valueBuf[5] = interface netmask as defined by ospf protocol
 * valueBuf[6] = interface mtu as defined by ospf protocol
 * valueBuf[7] to valueBuf[14] = not used
 * pValueBuf = not use
 *
 * ospfIfTable (reqType = ospfIfUpdateReqType):
 * valueBuf[0] = the value of ospfIfIpAddress index object
 * valueBuf[1] = the value of ospfAddressLessIf index object
 * valueBuf[2] = the value of ospfIfState read-only object
 * valueBuf[3] = the value of ospfIfDesignatedRouter read-only object
 * valueBuf[4] = the value of ospfIfBackupDesignatedRouter read-only object
 * valueBuf[5] = the value of ospfIfEvents read-only object
 * valueBuf[6] to valueBuf[14] = not used
 * pValueBuf = not use
 *
 * ospfIfTable (reqType = ospfIfCreateReqType)
 * valueBuf[0] = the value of ospfIfIpAddress index object
 * valueBuf[1] = the value of ospfAddressLessIf index object
 * valieBuf[2] = the value of ospfIfAreaId read-create object
 * valueBuf[3] = the value of ospfIfType read-create object
 * valueBuf[4] = the value of ospfIfAdminStat read-create object
 * valueBuf[5] = the value of ospfIfRtrPriority read-create object
 * valueBuf[6] = the value of ospfIfTransitDelay read-create object
 * valueBuf[7] = the value of ospfIfRetransInterval read-create object
 * valueBuf[8] = the value of ospfIfHelloInterval read-create object
 * valueBuf[9] = the value of ospfIfRtrDeadInterval read-create object
 * valueBuf[10] = the value of ospfIfPollInterval read-create object
 * valueBuf[11] = the value of ospfIfAuthType read-create object
 * valueBuf[12] = the value of ospfIfStatus read-create object
 * valueBuf[13] = the value of the interface subnet mask
 * valueBuf[14] = the value of the interface mtu size
 * pValueBuf = pointer to the ospfIfAuthKey read-create object
 *
 * ospfIfMetricTable (reqType = ospfIfMetricCreateReqType )
 * valueBuf[0] = the value of ospfIfMetricIpAddress index object
 * valueBuf[1] = the value of ospfIfMetricAddressLessIf index object
 * valueBuf[2] = the value of ospfIfMetricTOS index object
 * valueBuf[3] = the value of ospfIfMetricValue read-create object
 * valueBuf[4] = the value of ospfIfMetricStatus read-create object
 * valueBuf[5] to valueBuf[14] = not used
 * pValueBuf = not use
 *
 * ospfVirtIfTable (reqType = ospfVirtIfUpdateReqType):
 * valueBuf[0] = the value of ospfVirtIfAreaId index object
 * valueBuf[1] = the value of ospfVirtIfNeighbor index object
 * valueBuf[2] = the value of ospfVirtIfState read-only object
 * valueBuf[3] = the value of ospfVirtIfEvents read-only object
 * valueBuf[4] to valueBuf[14] = not used
 * pValueBuf = not use
 *
 * ospfVirtIfTable (reqType = ospfVirtIfCreateReq)
 * valueBuf[0] = the value of ospfVirtIfAreaId index object
 * valueBuf[1] = the value of ospfVirtIfNeighbor index object
 * valieBuf[2] = the value of ospfVirtIfTransitDelay read-create object
 * valueBuf[3] = the value of ospfVirtIfRetransInterval read-create object
 * valueBuf[4] = the value of ospfVirtIfHelloInterval read-create object
 * valueBuf[5] = the value of ospfVirtIfRtrDeadInterval read-create object
 * valueBuf[6] = the value of ospfVirtIfAuthType read-create object
 * valueBuf[7] = the value of ospfVirtIfStatus read-create object
 * valueBuf[8] to valueBuf[14] = not used
 * pValueBuf = pointer to ospfVirtIfAuthKey read-create object
 * 
 * ospfNbrTable (reqType = ospfNbrUpdateReqType):
 * valueBuf[0] = the value of ospfNbrIpAddr index object
 * valueBuf[1] = the value of ospfNbrAddressLessIndex index object
 * valueBuf[2] = the value of ospfNbrRtrId read-only object
 * valueBuf[3] = the value of ospfNbrOptions read-only object
 * valueBuf[4] = the value of ospfNbrState read-only object
 * valueBuf[5] = the value of ospfNbrEvents read-only object
 * valueBuf[6] = the value of ospfNbrLsRetransQLen read-only object
 * valueBuf[7] = the value of ospfNbmaNbrStatus read-only object
 * valueBuf[8] = the value of ospfNbmaNbrPermanence read-only object
 * valueBuf[9] = the value of ospfNbrHelloSuppressed read-only object
 * valueBuf[10] = the value of ospfNbrPriority read-only object
 * valueBuf[11] = Interface IP Address associated with this neighbor
 * valueBuf[12] = Interface IfIndex associated with this neighbor
 * valueBuf[13] and valueBuf[14] = not used
 * pValueBuf = not use
 *
 * ospfNbrTable (reqType = ospfNbrDeleteReqType)
 * valueBuf[0] = the value of ospfNbrIpAddr index object
 * valueBuf[1] = the value of ospfNbrAddressLessIndex index object
 * valueBuf[2] to valueBuf[14] = not used
 * pValueBuf = not use
 *
 * ospfVirtNbrTable (reqType = ospfVirtNbrUpdateReqType):
 * valueBuf[0] = the value of ospfVirtNbrArea index object
 * valueBuf[1] = the value of ospfVirtNbrRtrId index object
 * valueBuf[2] = the value of ospfVirtNbrIpAddr read-only object
 * valueBuf[3] = the value of ospfVirtNbrOptions read-only object
 * valueBuf[4] = the value of ospfVirtNbrState read-only object
 * valueBuf[5] = the value of ospfVirtNbrEvents read-only object
 * valueBuf[6] = the value of ospfVirtNbrLSRetransQLen read-only object
 * valueBuf[7] = the value of ospfVirtNbrHelloSuppressed read-only object
 * valueBuf[8] and valueBuf[14] = not used
 * pValueBuf = not use
 *
 * ospfVirtNbrTable (reqType = ospfVirtNbrDeleteReqType)
 * valueBuf[0] = the value of ospfVirtNbrArea index object
 * valueBuf[1] = the value of ospfVirtNbrRtrId index object
 * valueBuf[2] and valueBuf[14] = not used
 * pValueBuf = not use
 *
 * ospfExtLsdbTable (reqType = ospfExtLsdbUpdateReqType)
 * valueBuf[0] = the value of ospfExtLsdbType index object
 * valueBuf[1] = the value of ospfExtLsdbLsid index object
 * valueBuf[2] = the value of ospfExtLsdbRouterId index object
 * valueBuf[3] = the value of ospfExtLsdbSequence read-only object
 * valueBuf[4] = the value of ospfExtLsdbAge read-only object
 *
 * ospfExtLsdbTable (reqType = ospfExtLsdbCreateReqType)
 * valueBuf[0] = the value of ospfExtLsdbType index object
 * valueBuf[1] = the value of ospfExtLsdbLsid index object
 * valueBuf[2] = the value of ospfExtLsdbRouterId index object
 * valueBuf[3] = the value of ospfExtLsdbSequence read-only object
 * valueBuf[4] = the value of ospfExtLsdbAge read-only object
 * valueBuf[5] = the value of ospfExtLsdbChecksum read-only object
 * valueBuf[6] = the value of lsdbLen (not a MIB object)
 * valueBuf[7] = the protocol ID for the external route (if applicable).
 * valueBuf[8] and valueBuf[14] = not used
 * pValueBuf = pointer to the ospfExtLsdbAdvertisement read-only object
 *
 * ospfExtLsdbTable (reqType = ospfExtLsdbDeleteReqType)
 * valueBuf[0] = the value of ospfExtLsdbType index object
 * valueBuf[1] = the value of ospfExtLsdbLsid index object
 * valueBuf[2] = the value of ospfExtLsdbRouterId index object
 * valueBuf[3] to valueBuf[14] = not used
 * pValueBuf = not use
 *
 * ospfAreaAggregateTable (reqType = ospfAreaAggregateCreateReq )
 * valueBuf[0] = the value of ospfAreaAggregateAreaID index object
 * valueBuf[1] = the value of ospfAreaAggregateLsdbType index object
 * valueBuf[2] = the value of ospfAreaAggregateNet index object
 * valueBuf[3] = the value of ospfAreaAggregateMask index object
 * valueBuf[4] = the value of ospfAreaAggregateEffect read-only object
 * valueBuf[5] = the value of ospfAreaAggregateStatus read-only object
 * valueBuf[6] and valueBuf[14] = not used
 * pValueBuf = pointer to the ospfExtLsdbAdvertisement read-only object
 * 
 * interface flag change notification from ospf (reqType = ospfIfChangeReqType)
 * valueBuf[0] = the value of ospfIfIpAddress index object
 * valueBuf[1] = the value of ospfAddressLessIf index object
 * valueBuf[2] = interface index value (from struct ifnet)
 * valueBuf[3] = interface flags (from struct ifnet)
 * valueBuf[4] to valueBuf[14] = not use
 * pValueBuf = not use
 *
 * interface address deleted notification from ospf (reqType = ospfIfDeleteReqType)
 * valueBuf[0] = the value of ospfIfIpAddress index object
 * valueBuf[1] = the value of ospfAddressLessIf index object
 * valueBuf[2] = interface index value (from struct ifnet)
 * valueBuf[3] to valueBuf[14] = not use
 * pValueBuf = not use
 * 
 ***************** WRN OSPF Enterprise MIB ***************
 * wrnOspfGeneralGroup (reqType = wrnOspfGenGroupUpdateReqType )
 * valueBuf[0] = the value of wrnOspfOriginateNewOpaqueLsas read-only object
 * valueBuf[1] = the value of wrnOspfRxNewOpaqueLsas read-only object
 * valueBuf[2] = the value of wrnOspfType9LsaCount read-only object
 * valueBuf[3] = the value of wrnOspfType9LsaCksumSum read-only object
 * valueBuf[4] = the value of wrnOspfType11LsaCount read-only object
 * valueBuf[5] = the value of wrnOspfType11LsaCksumSum read-only object
 * valueBuf[6] to valueBuf[14] = not used
 * pValueBuf = not use
 *
 * wrnOspfAreaTable  (reqType = wrnOspfAreaUpdateReqType)
 * valueBuf[0] = the value of wrnOspfAreaId index object
 * valueBuf[1] = the value of wrnOspfAreaType10LsaCount read-only object
 * valueBuf[2] = the value of wrnOspfAreaType10LsaCksumSum read-only object
 * valueBuf[3] to valueBuf[14] = not used
 * pValueBuf = not use
 *
 * wrnOspfLsdbTable:Type-10 Opaque LSA (reqType = wrnOspfLsdbUpdateReqType):
 * valueBuf[0] = the value of wrnOspfLsdbAreaId index object
 * valueBuf[1] = the value of wrnOspfLsdbType index object
 * valueBuf[2] = the value of wrnOspfLsdbLsid index object
 * valueBuf[3] = the value of wrnOspfLsdbRouterId index object
 * valueBuf[4] = the value of wrnOspfLsdbSequence read-only object
 * valueBuf[5] = the value of wrnOspfLsdbAge read-only object
 *
 * wrnOspfLsdbTable:Type-10 Opaque LSA (reqType = wrnOspfLsdbCreateReqType):
 * valueBuf[0] = the value of wrnOspfLsdbAreaId index object
 * valueBuf[1] = the value of wrnOspfLsdbType index object
 * valueBuf[2] = the value of wrnOspfLsdbLsid index object
 * valueBuf[3] = the value of wrnOspfLsdbRouterId index object
 * valueBuf[4] = the value of wrnOspfLsdbSequence read-only object
 * valueBuf[5] = the value of wrnOspfLsdbAge read-only object
 * valueBuf[6] = the value of wrnOspfLsdbChecksum read-only object
 * valueBuf[7] = the value of lsdbLen (not a MIB object)
 * valueBuf[8] and valueBuf[14] = not used
 * pValueBuf = pointer to the wrnOspfLsdbAdvertisement read-only object
 *
 * wrnOspfLsdbTable:Type-10 Opaque LSA (reqType = wrnOspfLsdbDeleteReqType)
 * valueBuf[0] = the value of wrnOspfLsdbAreaId index object
 * valueBuf[1] = the value of wrnOspfLsdbType index object
 * valueBuf[2] = the value of wrnOspfLsdbLsid index object
 * valueBuf[3] = the value of wrnOspfLsdbRouterId index object
 * valueBuf[4] and valueBuf[14] = not used
 * pValueBuf = not use
 *
 * wrnOspfLocalLsdbTable:Type-9 Opaque LSA (reqType = wrnOspfLocalLsdbUpdateReqType)
 * valueBuf[0] = the value of wrnOspfLocalLsdbAreaId index object
 * valueBuf[1] = the value of wrnOspfLocalLsdbIpAddress index object
 * valueBuf[2] = the value of wrnOspfLocalLsdbType index object
 * valueBuf[3] = the value of wrnOspfLocalLsdbLsid index object
 * valueBuf[4] = the value of wrnOspfLocalLsdbRouterId index object
 * valueBuf[5] = the value of wrnOspfLocalLsdbSequence read-only object
 * valueBuf[6] = the value of wrnOspfLocalLsdbAge read-only object
 *
 * wrnOspfLocalLsdbTable:Type-9 Opaque LSA (reqType = wrnOspfLocalLsdbCreateReqType)
 * valueBuf[0] = the value of wrnOspfLocalLsdbAreaId index object
 * valueBuf[1] = the value of wrnOspfLocalLsdbIpAddress index object
 * valueBuf[2] = the value of wrnOspfLocalLsdbType index object
 * valueBuf[3] = the value of wrnOspfLocalLsdbLsid index object
 * valueBuf[4] = the value of wrnOspfLocalLsdbRouterId index object
 * valueBuf[5] = the value of wrnOspfLocalLsdbSequence read-only object
 * valueBuf[6] = the value of wrnOspfLocalLsdbAge read-only object
 * valueBuf[7] = the value of wrnOspfLocalLsdbChecksum read-only object
 * valueBuf[8] = the value of lsdbLen (not a MIB object)
 * valueBuf[9] and valueBuf[14] = not used
 * pValueBuf = pointer to the wrnOspfLocalLsdbAdvertisement read-only object
 *
 * wrnOspfLocalLsdbTable:Type-9 Opaque LSA (reqType = wrnOspfLocalLsdbDeleteReqType)
 * valueBuf[0] = the value of wrnOspfLocalLsdbAreaId index object
 * valueBuf[1] = the value of wrnOspfLocalLsdbIpAddress index object
 * valueBuf[2] = the value of wrnOspfLocalLsdbType index object
 * valueBuf[3] = the value of wrnOspfLocalLsdbLsid index object
 * valueBuf[4] = the value of wrnOspfLocalLsdbRouterId index object
 * valueBuf[5] and valueBuf[14] = not used
 * pValueBuf = not use
 * 
 * wrnOspfExtLsdbTable:Type-11 Opaque LSA (reqType = wrnOspfExtLsdbUpdateReqType)
 * valueBuf[0] = the value of wrnOspfExtLsdbType index object
 * valueBuf[1] = the value of wrnOspfExtLsdbLsid index object
 * valueBuf[2] = the value of wrnOspfExtLsdbRouterId index object
 * valueBuf[3] = the value of wrnOspfExtLsdbSequence read-only object
 * valueBuf[4] = the value of wrnOspfExtLsdbAge read-only object
 *
 * wrnOspfExtLsdbTable:Type-11 Opaque LSA (reqType = wrnOspfExtLsdbCreateReqType)
 * valueBuf[0] = the value of wrnOspfExtLsdbType index object
 * valueBuf[1] = the value of wrnOspfExtLsdbLsid index object
 * valueBuf[2] = the value of wrnOspfExtLsdbRouterId index object
 * valueBuf[3] = the value of wrnOspfExtLsdbSequence read-only object
 * valueBuf[4] = the value of wrnOspfExtLsdbAge read-only object
 * valueBuf[5] = the value of wrnOspfExtLsdbChecksum read-only object
 * valueBuf[6] = the value of lsdbLen (not a MIB object)
 * valueBuf[7] and valueBuf[14] = not used
 * pValueBuf = pointer to the wrnOspfExtLsdbAdvertisement read-only object
 *
 * wrnOspfExtLsdbTable:Type-11 Opaque LSA (reqType = wrnOspfExtLsdbDeleteReqType)
 * valueBuf[0] = the value of wrnOspfExtLsdbType index object
 * valueBuf[1] = the value of wrnOspfExtLsdbLsid index object
 * valueBuf[2] = the value of wrnOspfExtLsdbRouterId index object
 * valueBuf[3] and valueBuf[14] = not used
 * pValueBuf = not use
 *
 * wrnOspfIfTable (reqType = wrnOspfIfCreateReqType):
 * valueBuf[0] = the value of wrnOspfIfDstIpAddress index object
 * valueBuf[1] = the value of wrnOspfIfIndex index object
 * valueBuf[3] to valueBuf[14] = not used
 * pValueBuf = not use 
 * wrnOspfRedistributionTable (reqType = wrnOspfRedistribStaticCreateReqType,
 * wrnOspfRedistribRipCreateReqType and/or wrnOspfRedistribBgpCreateReqType):
 * valueBuf[0] = the value of wrnOspfRedistribRouteType index object
 * valueBuf[1] = the value of wrnOspfRedistribSubnet index object
 * valueBuf[2] = the value of wrnOspfRedistribMask index object
 * valueBuf[3] = the value of wrnOspfRedistribType read-create object
 * valueBuf[4] = the value of wrnOspfRedistribMetric read-create object
 * valueBuf[5] and valueBuf[14] = not used
 * pValueBuf = not use 
 */
typedef struct ospf2MapiReqBuf
{
    NODE                node;           /* linked list node */
    ospf2MapiReqType_t  reqType;        /* request type issued by ospf protocol */
    ulong_t             valueBuf[15];   /* value buffer */
    char                *pValueBuf;     /* pointer to data larger than sizeof(ulong_t) */
} ospf2MapiReqBuf_t;

typedef struct ospf2MapiSysCtl
{
    ushort_t if_index;   /* interface index (from struct ifnet) */
    void     *pCtlData;  /* control data, specific to the request message */
} ospf2MapiSysCtl_t;

/* prototypes for functions used by MIB API to dynamically reconfigure OSPF */
IMPORT STATUS mApi2Ospf_configGenGroup( void *pGenParams );
IMPORT STATUS mApi2Ospf_configArea( mApiRequest_t *pRequest );
IMPORT STATUS mApi2Ospf_configStub( mApiRequest_t *pRequest );
IMPORT STATUS mApi2Ospf_configHost( mApiRequest_t *pRequest );
IMPORT STATUS mApi2Ospf_configIf( mApiRequest_t *pRequest );
IMPORT STATUS mApi2Ospf_configIfMetric( mApiRequest_t *pRequest );
IMPORT STATUS mApi2Ospf_configVirtIf( mApiRequest_t *pRequest );
IMPORT STATUS mApi2Ospf_configNbr( mApiRequest_t *pRequest );
IMPORT STATUS mApi2Ospf_configAreaAggregate( mApiRequest_t *pRequest );
IMPORT STATUS mApi2Ospf_configRedistrib( mApiRequest_t *pRequest );
IMPORT STATUS mApi2Ospf_configOspf2IpRoute( mApiRequest_t *pRequest );
IMPORT STATUS mApi2Ospf_deleteArea( void *pRow );
IMPORT STATUS mApi2Ospf_deleteHost( void *pRow );
IMPORT STATUS mApi2Ospf_deleteIf( void *pRow );
IMPORT STATUS mApi2Ospf_deleteIfMetric( void *pRow );
IMPORT STATUS mApi2Ospf_deleteVirtIf( void *pRow );
IMPORT STATUS mApi2Ospf_deleteNbr( void *pRow );
IMPORT STATUS mApi2Ospf_deleteAreaAggregate( void *pRow );
IMPORT STATUS mApi2Ospf_deleteRedistrib( void *pRow );
IMPORT STATUS mApi2Ospf_deleteOspf2IpRoute( void *pRow );

/* prototypes for functions used by OSPF for operational status statistic updates */
IMPORT void ospf2Mapi_request( void *pObjInfo, ospf2MapiReqType_t reqType );

/* prototypes for functions used by OSPF for registration and de-registration */
IMPORT STATUS ospf2Mapi_proto_register( void );
IMPORT void ospf2Mapi_proto_deregister( void );

/* prototype for function used by OSPF to retrieve ospf configuration from MIB API */
IMPORT STATUS ospf2Mapi_query_config( BOOL *readStaticConfig );

/* misc prototypes used by MIB API only */
IMPORT STATUS ospf2Mapi_init( int numOspfReqBufCnt );
IMPORT void ospf2Mapi_shutdown( void );

/* Show routine */
IMPORT STATUS ospf2Mapi_show( void );

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __INCospf_mib_helper_updateh */
