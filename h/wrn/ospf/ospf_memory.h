/* ospf_memory.h */

/* Copyright 2001-2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*modification history
___________________
 02a,17feb02,ram    SPR 81808 Added OSPF memory partition support - initial creation
*/

#if !defined (_OSPF_MEMORY_H_)
#define _OSPF_MEMORY_H_

#ifdef __cplusplus
extern "C" {
#endif

#define OSPF_PARTMEM_LARGE_SIZE (2048 * 1024)  /* 2M Preferred initial memory partition size                                                    * 2 Megs */
#define OSPF_PARTMEM_SMALL_SIZE (128 * 1024)   /* Alternate Initial memory partition size */

/* OSPF memory partition defintions */
/* Undefine older memory macros */

#if defined (buffer_malloc)
#undef buffer_malloc
#endif
#if defined (buffer_free)
#undef buffer_free
#endif
#if defined (table_malloc)
#undef table_malloc
#endif
#if defined (table_free)
#undef table_free
#endif
#if defined (message_malloc)
#undef message_malloc
#endif
#if defined (message_free)
#undef message_free
#endif

/* Define new memory macros */
#define OSPF_PARTMEM_CREATE             ospfMemPartCreate
#define OSPF_PARTMEM_DELETE             ospfMemPartDelete(ospfMemPartId)
#define OSPF_PARTMEM_EXPAND(size)       ospfMemPartExpand(ospfMemPartId,size)

#define OSPF_PARTMEM_OPTION(option)     ospfMemPartSetOptions(ospfMemPartId,option)
#define OSPF_PARTMEM_SHOW(option)       ospfMemPartShow(ospfMemPartId,option)

#define buffer_malloc(size)               ospfMemPartAlloc(ospfMemPartId,size)
#define table_malloc(units,size)          ospfMemPartAlloc(ospfMemPartId,size)
#define message_malloc(size)              ospfMemPartAlloc(ospfMemPartId,size)
#define buffer_free(ptr)                  ospfMemPartFree(ospfMemPartId,(void*)ptr)
#define table_free(ptr)                   ospfMemPartFree(ospfMemPartId,(void*)ptr)
#define message_free(ptr)                 ospfMemPartFree(ospfMemPartId,(void*)ptr)

#ifdef __cplusplus
}
#endif

#endif /* _OSPF_MEMORY_H_ */
