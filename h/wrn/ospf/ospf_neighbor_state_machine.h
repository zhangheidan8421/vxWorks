/* ospf_neighbor_state_machine.h */

/* Copyright 2000 Wind River Systems, Inc. */

/*modification history
___________________
 18,26september00,reshma		Added WindRiver CopyRight
 17,25september00,reshma		RFC-1587 implementation for OSPF NSSA Option, also tested against ANVL.
 16,20july00,reshma				Unix compatibility related changes.
 15,06july00,reshma				Removed unnecessary header files and defines.
 14,04april00,reshma			Added some MIB support (Read only).
								Passed all important ANVL OSPF tests.
 13,23december99,reshma			Compatibility with VxWorks-IP and VxWorks RTM-interface
 12,19may99,jack				redefined OSPF_PATRICIA_32_BITS_KEY_MAX_BIT_POSITION as per fixes in
								patricia
 11,28december98,jack			Compiled and added some comments
 10,11november98,jack			Config changes, linted and big endian changes
 09,30october98,jack			Incorporate changes for compilation on Vxworks
 08,23august98,jack				ANVL tested OSPF with PATRICIA tree route table and no recursion
 07,10august98,jack				PATRICIA Route Table Based OSPF Code Base
 06,08july98,jack				Patricia RT table related changes - need to be tested
 05,04june98,jack				Integration with RTM and BGP
 04,10july97,cindy				Pre-release v1.52b
 03,10february97,cindy			Release Version 1.52
 02,22october97,cindy			Release Version 1.50
 01,05june96,cindy				First Beta Release
*/

#if !defined (_OSPF_NEIGHBOR_STATE_MACHINE_H_)
#define _OSPF_NEIGHBOR_STATE_MACHINE_H_

typedef struct OSPF_NEIGHBOR_STATE_MACHINE
{
	void (*fptr_neighbor_transition_function) (OSPF_INTERFACE *sptr_interface,OSPF_NEIGHBOR *sptr_neighbor,enum OSPF_NEIGHBOR_EVENT event);
} OSPF_NEIGHBOR_STATE_MACHINE;

typedef void (*NEIGHBOR_TRANSITION_FUNCTION) (OSPF_INTERFACE *sptr_interface,OSPF_NEIGHBOR *sptr_neighbor,enum OSPF_NEIGHBOR_EVENT event);

OSPF_NEIGHBOR_STATE_MACHINE ospf_neighbor_event_processing_table[OSPF_NUMBER_OF_NEIGHBOR_EVENTS][OSPF_NUMBER_OF_NEIGHBOR_STATES] =
{
/* ------------------------------------------------------------------------------------------------------------------ */
/*										                  OSPF_HELLO_RECEIVED          0              			 							 */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPF_NEIGHBOR_DOWN		 		*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_hello_received_event},
/* OSPF_NEIGHBOR_ATTEMPT	 		*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_hello_received_event},
/* OSPF_NEIGHBOR_INITIALIZING	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_hello_received_event},
/* OSPF_NEIGHBOR_2_WAY				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_hello_received_event},
/* OSPF_NEIGHBOR_EXCHANGE_START	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_hello_received_event},
/* OSPF_NEIGHBOR_EXCHANGE			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_hello_received_event},
/* OSPF_NEIGHBOR_LOADING			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_hello_received_event},
/* OSPF_NEIGHBOR_FULL				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_hello_received_event}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*										                        OSPF_START          1  	            			 							 */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPF_NEIGHBOR_DOWN		 		*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_start_event},
/* OSPF_NEIGHBOR_ATTEMPT	 		*/		{NULL},
/* OSPF_NEIGHBOR_INITIALIZING	*/		{NULL},
/* OSPF_NEIGHBOR_2_WAY				*/		{NULL},
/* OSPF_NEIGHBOR_EXCHANGE_START	*/		{NULL},
/* OSPF_NEIGHBOR_EXCHANGE			*/		{NULL},
/* OSPF_NEIGHBOR_LOADING			*/		{NULL},
/* OSPF_NEIGHBOR_FULL				*/		{NULL}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*							     							OSPF_TWO_WAY_RECEIVED          2     								  			 			 */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPF_NEIGHBOR_DOWN		 		*/		{NULL},
/* OSPF_NEIGHBOR_ATTEMPT	 		*/		{NULL},
/* OSPF_NEIGHBOR_INITIALIZING	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_2_way_received_event},
/* OSPF_NEIGHBOR_2_WAY				*/	{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_2_way_received_event} /* ### */,
/* OSPF_NEIGHBOR_EXCHANGE_START	*/		{NULL},
/* OSPF_NEIGHBOR_EXCHANGE			*/		{NULL},
/* OSPF_NEIGHBOR_LOADING			*/		{NULL},
/* OSPF_NEIGHBOR_FULL				*/		{NULL}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*										     					OSPF_NEGOTIATION_DONE          3       			 			 						 */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPF_NEIGHBOR_DOWN		 		*/		{NULL},
/* OSPF_NEIGHBOR_ATTEMPT	 		*/		{NULL},
/* OSPF_NEIGHBOR_INITIALIZING	*/		{NULL},
/* OSPF_NEIGHBOR_2_WAY				*/		{NULL},
/* OSPF_NEIGHBOR_EXCHANGE_START	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_negotiation_done_event},
/* OSPF_NEIGHBOR_EXCHANGE			*/		{NULL},
/* OSPF_NEIGHBOR_LOADING			*/		{NULL},
/* OSPF_NEIGHBOR_FULL				*/		{NULL}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*										     					  OSPF_EXCHANGE_DONE          4       			 			 						 */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPF_NEIGHBOR_DOWN		 		*/		{NULL},
/* OSPF_NEIGHBOR_ATTEMPT	 		*/		{NULL},
/* OSPF_NEIGHBOR_INITIALIZING	*/		{NULL},
/* OSPF_NEIGHBOR_2_WAY				*/		{NULL},
/* OSPF_NEIGHBOR_EXCHANGE_START	*/		{NULL},
/* OSPF_NEIGHBOR_EXCHANGE			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_exchange_done_event},
/* OSPF_NEIGHBOR_LOADING			*/		{NULL},
/* OSPF_NEIGHBOR_FULL				*/		{NULL}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*							     					    OSPF_BAD_LINK_STATE_REQUEST          5         			 			 					 */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPF_NEIGHBOR_DOWN		 		*/		{NULL},
/* OSPF_NEIGHBOR_ATTEMPT	 		*/		{NULL},
/* OSPF_NEIGHBOR_INITIALIZING	*/		{NULL},
/* OSPF_NEIGHBOR_2_WAY				*/		{NULL},
/* OSPF_NEIGHBOR_EXCHANGE_START	*/		{NULL},
/* OSPF_NEIGHBOR_EXCHANGE			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_tear_down_and_reestablish_adjacency},
/* OSPF_NEIGHBOR_LOADING			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_tear_down_and_reestablish_adjacency},
/* OSPF_NEIGHBOR_FULL				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_tear_down_and_reestablish_adjacency}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*									     					    OSPF_LOADING_DONE          6         			 			 						 */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPF_NEIGHBOR_DOWN		 		*/		{NULL},
/* OSPF_NEIGHBOR_ATTEMPT	 		*/		{NULL},
/* OSPF_NEIGHBOR_INITIALIZING	*/		{NULL},
/* OSPF_NEIGHBOR_2_WAY				*/		{NULL},
/* OSPF_NEIGHBOR_EXCHANGE_START	*/		{NULL},
/* OSPF_NEIGHBOR_EXCHANGE			*/		{NULL},
/* OSPF_NEIGHBOR_LOADING			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_loading_done_event},
/* OSPF_NEIGHBOR_FULL				*/		{NULL}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*										     					  OSPF_ADJACENCY_OK          7          			 			 						 */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPF_NEIGHBOR_DOWN		 		*/		{NULL},
/* OSPF_NEIGHBOR_ATTEMPT	 		*/		{NULL},
/* OSPF_NEIGHBOR_INITIALIZING	*/		{NULL},
/* OSPF_NEIGHBOR_2_WAY				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_start_forming_an_adjacency_with_the_neighbor},
/* OSPF_NEIGHBOR_EXCHANGE_START	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_maintain_or_destroy_existing_adjacency},
/* OSPF_NEIGHBOR_EXCHANGE			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_maintain_or_destroy_existing_adjacency},
/* OSPF_NEIGHBOR_LOADING			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_maintain_or_destroy_existing_adjacency},
/* OSPF_NEIGHBOR_FULL				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_maintain_or_destroy_existing_adjacency}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*	  						     					    OSPF_SEQUENCE_NUMBER_MISMATCH          8        			 			 					 */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPF_NEIGHBOR_DOWN		 		*/		{NULL},
/* OSPF_NEIGHBOR_ATTEMPT	 		*/		{NULL},
/* OSPF_NEIGHBOR_INITIALIZING	*/		{NULL},
/* OSPF_NEIGHBOR_2_WAY				*/		{NULL},
/* OSPF_NEIGHBOR_EXCHANGE_START	*/		{NULL},
/* OSPF_NEIGHBOR_EXCHANGE			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_tear_down_and_reestablish_adjacency},
/* OSPF_NEIGHBOR_LOADING			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_tear_down_and_reestablish_adjacency},
/* OSPF_NEIGHBOR_FULL				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_tear_down_and_reestablish_adjacency}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*									     					       OSPF_ONE_WAY          9         			 			 						 	 */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPF_NEIGHBOR_DOWN		 		*/		{NULL},
/* OSPF_NEIGHBOR_ATTEMPT	 		*/		{NULL},
/* OSPF_NEIGHBOR_INITIALIZING	*/		{NULL},
/* OSPF_NEIGHBOR_2_WAY				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_1_way_received_event},
/* OSPF_NEIGHBOR_EXCHANGE_START	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_1_way_received_event},
/* OSPF_NEIGHBOR_EXCHANGE			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_1_way_received_event},
/* OSPF_NEIGHBOR_LOADING			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_1_way_received_event},
/* OSPF_NEIGHBOR_FULL				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_1_way_received_event}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*									     					    OSPF_KILL_NEIGHBOR          10         			 			 						 */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPF_NEIGHBOR_DOWN		 		*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_ATTEMPT	 		*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_INITIALIZING	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_2_WAY				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_EXCHANGE_START	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_EXCHANGE			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_LOADING			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_FULL				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*										     			OSPF_INACTIVITY_TIMER          11         			 			 						 	 */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPF_NEIGHBOR_DOWN		 		*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_ATTEMPT	 		*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_INITIALIZING	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_2_WAY				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_EXCHANGE_START	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_EXCHANGE			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_LOADING			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_FULL				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*								OSPF_DOWN_INDICATION_FROM_LOWER_LEVEL_PROTOCOLS_RECEIVED         12      		 						    */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPF_NEIGHBOR_DOWN		 		*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_ATTEMPT	 		*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_INITIALIZING	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_2_WAY				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_EXCHANGE_START	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_EXCHANGE			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_LOADING			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event},
/* OSPF_NEIGHBOR_FULL				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospf_process_neighbor_down_event}
}
};


#endif /* _OSPF_NEIGHBOR_STATE_MACHINE_H_ */
