/* ospfLib.h */

/* Copyright 2000 -2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,27jun05,xli  Fix Workbench compilation warning 
01c,29nov04,tlu  OSPF stack decoupling 
01b,18jun04,mwv  update file for compatibility for new Tornado facility 
01a,05dec02,mwv  Fix tornado linker warning.  SPR 82521.  Modified the OSPF_CALLBACK_IF struct 
*/
#include <stdio.h>
#include <vxWorks.h>
#include <netLib.h>
#include <netinet/in.h>
#include <sockLib.h>

int ospfInitialize (int vsNum);



