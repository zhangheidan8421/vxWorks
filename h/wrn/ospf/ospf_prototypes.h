/* ospf_prototypes.h - OSPF prototypes */

/* Copyright 2000-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
04u,25feb05,xli added ospfShowAreas(),ospfShowInterface()and ospfShowNeighbor() prototypes
                used by ospf_show_routines.c
04t,31jan05,mwv SPR#106007 change the prototype for
                ospf_add_database_entry_to_neighbor_retransmit_list
                ospf_add_neighbor_to_database_retransmit_list
05u,24jan05,mwv SPR#105819 modify ospf_calculate_routes_to_external_destinations ()
05t,03nov04,dsk trace B0456
05s,07oct04,mwv SPR#100658: added 
                ospf_process_old_ls_summary_summaryASBR_and_external_advertisements ()
                to prototypes list
05r,11aug04,mwv SPR#100246 correct dangling pointer in 
                ospf_print_memory_error_message_and_free_buffer_if_necessary ()
05q,23sep04,tlu Fix for ANVL 27.27 and 31.2 - add advertising router parameter to 
                ospf_find_network_LSA
05p,02sep04,ram Fix function prototypes
05o,04aug04,dsk get_port_number_and_string (multiple definition if INCLUDE_NVRAM)
05n,06may04,ram Fix for AVL memory corruption
05m,30apr04,mwv SPR#91272 added ospf_originate_summary_link_into_stub_area ()
                added ospf_dynamic_send_summary_lsa_for_stub_area ()
05l,22apr04,ram AVL & Memory migration to shared library
05k,16apr04,zhr compile with the IP dual stack CD
05j,06apr04,ram OSPF v2/v3 coexistance changes
05i,15mar04,zhr changed prototype of ospf_dynamic_config_redistribution
05h,12mar04,mwv SPR#94408 - modify redistribution API to support route tag
05g,12mar04,ram SPR#94944 OSPF performance modifications
05f,04mar04,mwv SPR#93561 added ospf_long_swap to do byte order swapping
                for BIG ENDIAN
05e,29jan04,ram     NBMA, PTMP, & Unnumbered modifications
05d,16dec03,agi propogated weighted routes fixes
05c,08dec03,agi merged fixes from OSPF 2.1 patch branch
05b,01dec03,agi MD_ALGORITHM mods, ported to merged stack
05a,06aug03,agi ported to Accordion stack
04y,01nov03,kc  SPR#91541 - added ospf_set_ip_route_policy(), ospf_ip_route_policy_get(),
                ospf_dynamic_config_ip_route_policy(), ospf_dynamic_destroy_ip_route_policy(),
                and ospf_sysctl_routeModify() prototypes.
04x,28jul03,ram SPR#89908 Added ability to check for unreachable routing table
                routes in ospf_sysctl_rtableWalk and delete them from OSPF.
04w,18aug03,agi Removed ospf_MD_string(), OSPF now used common mdString()
04v,30jul03,agi Renamed MD_string() to ospf_MD_string() to avoid name
                collision
04u,28jul03,ram SPR#89908 Added ability to check for unreachable routing table routes
                in ospf_sysctl_rtableWalk and delete them from OSPF.
04t,28jul03,mwv SPR#88854 - add the capability to send a LSA that is larger than
                the MTU size.  It will cause IP fragmentation as there is no way to fragment
                the large LSA.  Added function ospf_transmit_update_packets_for_large_lsa ()
04s,17jul03,agi Added ospf_delete_entry_from_list()
04r,30jun03,agi Fixed SPR#88879, OSPF does not clean up external LSDB when
                an interface is brought down
04q,11jun03,ram SPR#88965 Separate route table and LSDB hash parameters
04p,11jun03,agi SPR#8858, added bool ospfPartMemInitialize() prototype
04l,17jul03,agi Added ospf_delete_entry_from_list()
04o,02jun03,ram Removed obsolete memory allocation function prototypes
04k,30jun03,agi Fixed SPR#88879, OSPF does not clean up external LSDB when
                an interface is brought down
04j,11jun03,ram SPR#88965 Separate route table and LSDB hash parameters
04i,11jun03,agi SPR#8858, added bool ospfPartMemInitialize() prototype
04h,06jun03,mwv SPR#88905 SPR#88863 Added functionality to configure the redistribution policy
                added functions ospf_delete_redistribution_bgp, () ospf_delete_redistribution_rip
                ospf_delete_redistribution_static (),
                ospf_configure_redistribution_rip, ospf_configure_redistribution_bgp (),
                ospf_configure_redistribution_static
04g,06jun03,kc  SPR#88905 added  ospf_dynamic_config_redistribution () and
                ospf_dynamic_destroy_redistrib ()
04f,02jun03,ram Removed obsolete memory allocation function prototypes
04e,02jun03,asr Renamed route_proto to route_protocol
04d,09may03,kc  Fixed SPR#88389 - Added route_proto and is_default_route
                arguments to ospf_dynamic_flush_external_routes() and
                ospf_flush_the_link_state_database_of_self_originated_external_lsa()
04c,15may03,mwv Fixed SPR#87784 - added ospf_process_export_route_queue_all ()
                prototype.
04b,15may03,kkz SPR 86134 - Added new function to get cost of address range
04a,12may03,asr Changes to make OSPF virtual stack compatible
                ospf_originate_default_summary_link_into_the_area().
03z,26may03,dsk Fixed SPR 88619, LSAearly retransmission
03y,26may03,agi Added ospf_get_system_elapsed_time_second(), OSPF memory
                and OSPF utilities prototypes
03x,22may03,kc  Added ospf_is_address_range_active() and
                ospf_get_metric_cost_of_address_range() prototypes to remove warnings.
03w,09may03,kc  Fixed SPR#88389 - Added route_proto and is_default_route
                arguments to ospf_dynamic_flush_external_routes() and
                ospf_flush_the_link_state_database_of_self_originated_external_lsa().
03v,24apr03,kc  Fixed SPR#86625 - added
                ospf_dynamic_send_default_summary_lsa() prototype. Also
03u,24apr03,kc  Fixed SPR#86625 - added ospf_dynamic_send_default_summary_lsa()
                prototype. Also added  prematurely_age_advertisement argument to
                ospf_originate_default_summary_link_into_the_area().
03t,27may03,mwv Fixed SPR#87784 - added
                ospf_process_export_route_queue_all () prototype.
03s,22may03,kkz SPR#88613 - Added new functions for area address range
                feature
03r,22may03,dsk SPR#88619 Fix early LSA retransmission
03q,22apr03,ram SPR#76812 Modifications for OSPF performance enhancements
03p,17feb02,ram SPR#81808 Added OSPF memory partition support
03o,17feb03,ram SPR#85893 Removed ospf_find_all_lsa proto
03n,29jan03,mwv SPR#85893 add proto for ospf_find_all_lsa ()
03m,28jan03,ram SPR#85050 Added support for external route redistribution
                based on OSPF external metric values
03l,06jan03,ram SPR#85432 Added new hello timer function prototype
03k,09dec02,ram SPR#83418 Added support for OSPF metric type and value
                configuration
03j,22nov02,htm Added ospf_find_router_or_network_routing_table_node()
                prototype
03i,19nov02,kc  Added prototypes for ospf_notify_areas_for_abr_change() and
                ospf_flush_network_link_advertisement().  SPR#84478,84485,
                84486
03h,19nov02,mwv Merge TMS code SPR 84284
03g,08oct02,agi Added ospf_is_if_unnumbered_up() prototype
03f,24jun02,kc  Changed check_if_status() prototype to return boolean.
03e,13may02,ark Added new function ospf_find_network_LSA for SPR 75793
03d,30may02,kc  Added ospf_sysctl_rtableWalk() prototype.
03c,24may02,kc  Added ospf_sysctl_input() prototype.
03b,18apr02,kc  Fixed SPR #74432 - Added prototypes for
                ospf_dynamic_export_external_routes() and
                ospf_dynamic_flush_external_routes().
03a,25apr02,jkw Additional ospfShowRoutingTable routine.
02z,18apr02,jkw External 1583compatibility flooding.
02y,16apr02,jkw One copy of external and type 11 lsa
02x,24mar02,kc  Changed ospf_startup() return type from void to STATUS.
02w,09apr02,jkw Sequence number wrap.
02v,15feb02,kc  Added ospf_dynamic_init_unnumbered_interface() prototype.
02u,09jan02,jkw Add new function for processing external route queue.
02t,20dec01,jkw Removed sptr_area->sptr_interfaces structure.
02s,13dec01,kc  Added prototype for ospf_dynamic_calculate_spf().
02r,07dec01,kc  Added ospf_dynamic_reset_interface() and
                ospf_dynamic_reinit_interface() prototypes.
02q,23oct01,kc  Added the prototypes ospf_dynamic_create_area_range(),
                ospf_dynamic_destroy_area_range(), ospf_dynamic_config_overflow()
                and ospf_dynamic_flush_opaque_lsa()
02p,13oct01,kc  Dynamic configuration changes.
02o,12oct01,kc  Added dynamic BOOL argument to ospf_add_entry_to_hosts_list()
                and ospf_add_entry_to_virtual_link_list().
02n,06oct01,kc  Added ospf_init_setup() prototype.
02m,28sep01,kc  Added ospf_add_entry_to_hosts_list() and
                ospf_add_entry_to_virtual_link_list() prototypes.
02l,26sep01,kc  Added ospf_put_an_interface_on_areas_interface_list(),
                ospf_clear_advertisements_from_lists() prototypes.
02k,22sep01,kc  Added prototypes for ospf_multicast_group_request() and
                ospf_validate_interface(). Obsolete ospf_router_rx_packet(),
                and ospf_get_port_number() prototypes. Modified function
                arguments for ospf_router_rx_packet().
02j,17sep01,kc  Added prototypes for ospf_show_xxx() routines.
02i,13aug01,kc  Added ospf_register_with_ip() and ospf_deregister_with_ip()
                prototypes.
02h,26jul01,jkw Added opaque lsa APIs
02g,24jul01,jkw Removed WINROUTER preproc
02f,23jul01,jkw Added routine to delete all items in the container and the container.
02e,26jun01,jkw Added updates for multiple path destination.
02d,26sep00,res Added WindRiver CopyRight
02c,25sep00,res RFC-1587 implementation for OSPF NSSA Option, also tested against
                ANVL.
02b,20jul00,res Unix compatibility related changes.
02a,06jul00,res Removed unnecessary header files and defines.
01z,04apr00,res Added some MIB support (Read only). Passed all important ANVL OSPF
                tests.
01y,23dec99,res Compatibility with VxWorks-IP and VxWorks RTM-interface
01x,16aug99,jac Folded the fix in RTM patricia - always return most specific route -
                Sue case into ospf patricia tree.
01w,19may99,jac redefined OSPF_PATRICIA_32_BITS_KEY_MAX_BIT_POSITION as per fixes in
                patricia
01v,19may99,jac Fixes in patricia on insertion
01u,12may99,jac Changes related to equal cost multi path and
                ospf_set_patricia_route_change_status_on_ospf_rt_node
01t,12may99,jac changed the name of ospf_get_new_next_hop_blocks to
                ospf_get_new_next_hop_blocks_and_mark_ospf_rt_node_new
01s,12may99,jac Change in proto type of ospf_get_new_next_hop_blocks
01r,10may99,jac Changes in prototypes and call and declaration of
                ospf_get_new_next_hop_blocks
01q,10may99,jac syntax error fix
01p,10may99,jac Changes related to ospf_get_new_next_hop_blocks
01o,28dec98,jac Compiled and added some comments
01n,01dec98,jac Changes to get rid of vxworks warnings
01m,13nov98,jac Changes related to introducing queuing in OSPF to RTM interface and
                bug fix on the external route additions path (to RTM)
01l,11nov98,jac Config changes, linted and big endian changes
01k,30oct98,jac Incorporate changes for compilation on Vxworks
01j,23aug98,jac ANVL tested OSPF with PATRICIA tree route table and no recursion
01i,10aug98,jac PATRICIA Route Table Based OSPF Code Base
01h,08jul98,jac Patricia RT table related changes - need to be tested
01g,19jun98,jac listroutine changes. OSPF add and delete list routines, eventually
                call the rwutils list routines.
01f,16jun98,jac Changes related to external_routes queuing and the queue processing
01e,04jun98,jac Integration with RTM and BGP
01d,10jul97,cin Pre-release v1.52b
01c,10feb97,cin Release Version 1.52
01b,22oct97,cin Release Version 1.50
01a,05jun96,cin First Beta Release
*/

#include    <in.h>
#include    <net/if.h>
#include    <md5.h>

/************************************************************************/

#if !defined (_OSPF_PROTOTYPES_H_)
#define _OSPF_PROTOTYPES_H_

/*******************/
/* ospf_checksum.c */
/*******************/
USHORT ospf_generate_LS_checksum (void *vptr_data,
                                  USHORT length_of_data,
                                  BYTE *bptr_checksum);
enum TEST ospf_verify_LS_checksum (void *vptr_data,  
                                   USHORT length_of_data /* in network order */, 
                                   USHORT checksum);

/************************/
/* ospf_configuration.c */
/************************/

void ospf_configuration (enum PROTOCOL_CONTROL_OPERATION command,
                         ULONG parameter_0,
                         ULONG parameter_1);
ULONG ospf_get_configuration_table_address (void);
enum TEST ospf_set_default_values (char *cptr_value_string);
ULONG ospf_special_configuration_function_processing (ULONG parameter[]);

enum TEST ospf_set_bgp_subnets_to_redistribute (char *cptr_value_string);
enum TEST ospf_set_rip_subnets_to_redistribute (char *cptr_value_string);
enum TEST ospf_set_static_subnets_to_redistribute (char *cptr_value_string);
enum TEST ospf_set_local_subnets_to_redistribute (char *cptr_value_string);
enum TEST ospf_set_ip_route_policy( char *cptr_value_string );
enum TEST ospf_set_neighbor_as_not_allowed (char *cptr_value_string);
enum TEST ospf_set_origin_as_not_allowed (char *cptr_value_string);
ULONG ospf_get_router_id();
ULONG ospf_get_number_of_areas();
ULONG ospf_get_number_of_virtual_links();
enum TEST ospf_set_port_passive (char *cptr_value_string);

STATUS ospf_configure_redistribution_static(ULONG subnet_address, 
                                            ULONG subnet_mask, 
                                            ULONG metric_type, 
                                            ULONG metric_value, 
                                            ULONG route_tag, 
                                            BYTE options);
void ospf_set_port (char * cptr_value_string);
/* SPR#88905 -- Begin */
STATUS ospf_configure_redistribution_rip(ULONG subnet_address, 
                                         ULONG subnet_mask, 
                                         ULONG metric_type, 
                                         ULONG metric_value, 
                                         ULONG route_tag, BYTE options);
STATUS ospf_configure_redistribution_bgp(ULONG subnet_address, 
                                         ULONG subnet_mask, 
                                         ULONG metric_type, 
                                         ULONG metric_value, 
                                         ULONG route_tag, 
                                         BYTE options);
STATUS ospf_configure_redistribution_local(ULONG subnet_address, 
                                           ULONG subnet_mask, 
                                           ULONG metric_type, 
                                           ULONG metric_value, 
                                           ULONG route_tag, 
                                           BYTE options);

STATUS ospf_delete_redistribution_static(ULONG subnet_address, ULONG subnet_mask);
STATUS ospf_delete_redistribution_rip   (ULONG subnet_address, ULONG subnet_mask);
STATUS ospf_delete_redistribution_bgp   (ULONG subnet_address, ULONG subnet_mask);
STATUS ospf_delete_redistribution_local (ULONG subnet_address, ULONG subnet_mask);
/* SPR#88905 -- End */
USHORT ospf_get_port_number_and_string (char *cptr_port_number_and_value, char *return_string);

/******************/
/* ospf_control.c */
/******************/
void ospf_halt_ospf_router (void);
enum TEST ospf_control (enum PROTOCOL_CONTROL_OPERATION command,ULONG parameter_0,ULONG parameter_1);

void ospf_shut_down_ospf_router (void);
/***************************/
/* ospf_database_summary.c */
/***************************/

enum OSPF_RETURN_FLAGS ospf_build_ls_database_summary (OSPF_INTERFACE *sptr_interface,
                                                       OSPF_NEIGHBOR *sptr_neighbor);
OSPF_LS_DATABASE_SUMMARY *ospf_allocate_ls_database_summary (OSPF_INTERFACE *sptr_interface);
void ospf_free_ls_database_summary (OSPF_LS_DATABASE_SUMMARY *sptr_database_summary);


/******************/
/* ospf_vx_main.c */
/******************/
/*Added change as per TMS PR # 1569.
  Changed the return type from void to STATUS*/
/* void ospf_task_initialize (const char *p_configuration_file);*/

STATUS ospf_task_initialize (const char *p_configuration_file);


/*************************************/
/* ospf_designated_router_election.c */
/*************************************/

void ospf_run_designated_router_election (OSPF_INTERFACE *sptr_interface);

/*******************/
/* ospf_dijkstra.c */
/*******************/

void ospf_run_dijkstra (OSPF_AREA_ENTRY *sptr_area);
void ospf_bring_up_virtual_links_if_necessary (OSPF_AREA_ENTRY *sptr_area);
void ospf_set_intervening_router (OSPF_SHORTEST_PATH_NODE *sptr_vertex_V, 
                                  OSPF_SHORTEST_PATH_NODE *sptr_vertex_W,
                                  OSPF_AREA_ENTRY* sptr_area, 
                                  OSPF_ROUTER_LINK_PIECE *sptr_link );

/*************************************/
/* ospf_external_routes.c            */
/*************************************/

void ospf_process_external_route_to_ospf ();

OSPF_EXTERNAL_ROUTE* ospf_find_originated_external_route (
    OSPF_EXTERNAL_ROUTE *sptr_originated_external_routes,
    OSPF_EXTERNAL_ROUTE *sptr_external_route);

void ospf_add_exported_route_to_ospf (
    ULONG destination_network, ULONG network_mask, ULONG metric,
    ULONG forwarding_address, ULONG tag, ULONG proto);

void ospf_delete_exported_route_from_ospf (
    ULONG destination_network, ULONG  network_mask, ULONG metric,
    ULONG forwarding_address, ULONG   tag, ULONG proto);


/*************************************/
/* ospf_external_route_calculation.c */
/*************************************/

void ospf_calculate_routes_to_external_destinations (BOOLEAN new_summary_lsa_received);
#if defined (__NSSA__)
void ospf_calculate_type_7_as_external_routes (void);
#endif /*__NSSA__*/

void ospf_calculate_routes_to_a_single_external_destination (
                     UNION_OSPF_LINK_STATE_ADVERTISEMENT *advertisement,
                     enum BOOLEAN on_new_external_lsa_received_path);

#if defined (__NSSA__)
void ospf_calculate_type_7_routes_to_a_single_external_destination (
                              UNION_OSPF_LINK_STATE_ADVERTISEMENT *advertisement,
                              enum BOOLEAN on_new_external_lsa_received_path);
void ospf_translate_type_7_routes_into_type_5_lsa (void);
void ospf_invalidate_type_7_route_entry (OSPF_TYPE_7_LINK_ADVERTISEMENT_HEADER *sptr_type_7);
#endif

/* SPR 85050 -- Begin */
enum BOOLEAN ospf_compare_costs_of_external_lsdb_entries (ULONG new_metric, ULONG metric);
/* SPR 85050 -- End */

/****************/
/* ospf_flood.c */
/****************/

enum BOOLEAN ospf_flood_advertisement_out_some_subset_of_the_routers_interfaces (
                                OSPF_ROUTER_LINK_ADVERTISEMENT_HEADER *sptr_advertisement,
                                OSPF_AREA_ENTRY *sptr_area,
                                OSPF_INTERFACE *sptr_interface_advertisement_received_on,
                                OSPF_NEIGHBOR *sptr_neighbor_advertisement_received_from, 
                                enum BOOLEAN advertisement_installed);

/****************/
/* ospf_hello.c */
/****************/

void ospf_send_hello (OSPF_INTERFACE *sptr_interface,OSPF_NEIGHBOR *sptr_neighbor,enum BOOLEAN goingdown);
enum OSPF_PACKET_STATE ospf_hello_packet_received (OSPF_HELLO_HEADER *sptr_hello,
                                                   OSPF_NEIGHBOR *sptr_new_neighbor,
                                                   OSPF_INTERFACE *sptr_interface,
                                                   ULONG source_address,
                                                   ULONG router_id,
                                                   ULONG size_of_packet,
                                                   ULONG cryptographic_sequence_number);
void ospf_set_neighbor_fields (OSPF_NEIGHBOR *sptr_neighbor,
                               ULONG router_id,
                               ULONG address,
                               ULONG cryptographic_sequence_number,
                               OSPF_INTERFACE *sptr_interface,
                               USHORT priority,
                               BYTE options,
                               enum OSPF_NEIGHBOR_STATE state);
/* SPR#76812 */
void ospf_hello_timer_initialize (void);


/*************************/
/* ospf_initialization.c */
/*************************/

enum TEST ospf_initialize_router (ULONG clock_ticks_per_second);

void ospf_put_an_interface_on_areas_interface_list(OSPF_AREA_ENTRY *sptr_area, OSPF_INTERFACE *sptr_interface);

/* Wrapped it with __RAW_SOCKET define */
void ospf_multicast_group_request( ULONG ipAddr, ULONG multicastAddr, int request );


/* NOTE: changed from void returned type to TEST */
enum TEST ospf_add_entry_to_hosts_list (OSPF_INTERFACE *sptr_interface, BOOL dynamic);
enum TEST ospf_add_entry_to_virtual_link_list (OSPF_INTERFACE *sptr_interface, BOOL dynamic);

OSPF_AREA_ENTRY* ospf_create_pseudo_area_for_virtual_link (OSPF_INTERFACE *sptr_interface);

void ospfShutdown (void);

/*************************/
/* ospf_vx_ip_adaptation.c */
/*************************/
/*raw socket - New socket interface function jkw*/
int ospf_vx_ip_receive_socket_input (void);
void ospf_vx_ip_output (OSPF_HEADER *sptr_packet, 
                        ULONG source_address, 
                        bool flag_multicast, 
                        ULONG destination_address, 
                        ULONG length /* in host order */, 
                        enum BOOLEAN free_packet);
struct mbuf * ospf_get_an_ip_get_packet (void);
/*raw socket - End of changes*/
int ospf_vx_ip_user_request_processing (struct socket *socket_handle, 
                                        int type_of_request, 
                                        struct mbuf *message_block,
                                        struct mbuf *name, 
                                        struct mbuf *control);
int ospf_vx_ip_receive_input(OSPF_PACKET *ospf_packet, ULONG rcvif_handle);
int ospf_ctloutput (int option, 
                    struct socket *socket_handle, 
                    int level, 
                    int option_name, 
                    struct mbuf **p_message_block);
void ospf_vx_ip_output (OSPF_HEADER *sptr_packet, 
                        ULONG source_address, 
                        bool flag_multicast, 
                        ULONG destination_address, 
                        ULONG length /* in host order */, 
                        enum BOOLEAN free_packet);
enum TEST ospf_vx_ip_attach (struct socket *socket_handle);
enum TEST ospf_vx_ip_detach ( ULONG source_address);
void ospf_indicate_lower_level_interface_state_change (OSPF_INTERFACE* sptr_ospf_interface, bool new_state);

/* for tOspfRecv task*/
/* Added change as per TMS PR # 1652 */
int ospfReceive( void );

/***************************************/
/* ospf_inter_area_route_calculation.c */
/***************************************/

void ospf_calculate_inter_area_routes (OSPF_AREA_ENTRY *sptr_area);
enum BOOLEAN ospf_calculate_inter_area_routes_for_a_single_summary_advertisement (
                                               OSPF_ADVERTISEMENT_NODE *sptr_advertisement_node,
                                               OSPF_AREA_ENTRY *sptr_area, 
                                               enum BOOLEAN new_summary_lsa_received);
void ospf_invalidate_inter_area_route_entry (OSPF_SUMMARY_LINK_ADVERTISEMENT_HEADER *sptr_advertisement_node, 
                                             OSPF_AREA_ENTRY *sptr_area);

/********************/
/* ospf_interface.c */
/********************/

void ospf_bring_up_interface (OSPF_INTERFACE *sptr_interface);
void ospf_initialize_interface (OSPF_INTERFACE *sptr_interface, BOOL dynamic);
enum BOOLEAN check_if_status( unsigned short if_index );
enum BOOLEAN ospf_is_if_unnumbered_up(OSPF_INTERFACE *sptr_interface);


/********************************/
/* ospf_raw_socket_interface.c */
/********************************/
enum TEST ospf_send_to_raw_ip_packet (void *vptr_packet, UINT packet_length, UINT destination_address);
enum TEST ospf_read_and_process_packet_from_sockets (void);
void ospf_poll_for_packets_on_raw_sockets (void);


/********************************/
/* legacy ip  functions to be deleted later */
/********************************/

USHORT calculate_ip_checksum (PSEUDO_IP_PARAMETERS *sptr_pseudo_header, BYTE *bptr_start_from, USHORT length);


/***************************/
/* ospf_interface_events.c */
/***************************/

void ospf_process_interface_up_event (OSPF_INTERFACE *sptr_interface);
void ospf_process_interface_wait_timer_event (OSPF_INTERFACE *sptr_interface);
void ospf_process_interface_backup_seen_event (OSPF_INTERFACE *sptr_interface);
void ospf_process_interface_neighbor_change_event (OSPF_INTERFACE *sptr_interface);
void ospf_process_interface_loop_indication_event (OSPF_INTERFACE *sptr_interface);
void ospf_process_interface_unloop_indication_event (OSPF_INTERFACE *sptr_interface);
void ospf_process_interface_down_event (OSPF_INTERFACE *sptr_interface);

void ospf_reset_interface_variables_and_timers_and_destroy_all_associated_neighbor_connections (
                                                                OSPF_INTERFACE *sptr_interface,
                                                                enum OSPF_INTERFACE_EVENT event,
                                                                enum OSPF_INTERFACE_STATE new_state);
void ospf_interface_address_delete (USHORT if_index, struct in_addr * if_addr);
void ospf_interface_flag_change ( USHORT interface_index, ULONG interface_flags);

/**********************************/
/* ospf_interface_state_machine.c */
/**********************************/

void ospf_execute_interface_state_machine (enum OSPF_INTERFACE_EVENT event,
                                           enum OSPF_INTERFACE_STATE state,
                                           OSPF_INTERFACE *sptr_interface);

/************************************/
/* ospf_link_state_advertisements.c */
/************************************/

void ospf_originate_new_instance_of_the_link_state_advertisement (
                                            OSPF_LS_DATABASE_ENTRY *sptr_database_entry,
                                            OSPF_AREA_ENTRY *sptr_area);
void ospf_originate_router_links_advertisement (OSPF_AREA_ENTRY *sptr_area);
void ospf_originate_network_links_advertisement (OSPF_INTERFACE *sptr_interface);
void ospf_originate_summary_links_advertisement (OSPF_AREA_ENTRY *sptr_area);
void ospf_originate_a_single_summary_links_advertisement (
                                          OSPF_ROUTING_TABLE_ENTRY *sptr_routing_table_entry,
                                          OSPF_AREA_ENTRY *sptr_area,
    enum BOOLEAN prematurely_age_advertisement);
void ospf_originate_default_summary_link_into_the_area (OSPF_AREA_ENTRY *sptr_area,
    enum BOOLEAN prematurely_age_advertisement);
void ospf_originate_summary_link_into_stub_area (OSPF_AREA_ENTRY *sptr_area, enum BOOLEAN prematurely_age_advertisement);
void ospf_originate_external_links_advertisement (OSPF_EXTERNAL_ROUTE *sptr_external_route);
#if defined (__NSSA__)
OSPF_EXTERNAL_ROUTE *ospf_translate_routing_table_entry_to_external_route (
                                                 OSPF_ROUTING_TABLE_ENTRY *sptr_routing_table_entry, 
                                                 enum BOOLEAN set_forwarding_address);
void ospf_originate_external_links_advertisement_from_type_7_route (OSPF_EXTERNAL_ROUTE *sptr_external_route);
void ospf_originate_type_7_links_advertisement (OSPF_EXTERNAL_ROUTE *sptr_external_route);
#endif /*__NSSA__*/
/*opaque lsa prototypes for originating opaque lsas jkw*/
#if defined (__OPAQUE_LSA__)
void ospf_originate_type_9_links_advertisement (OSPF_INTERFACE *sptr_interface,
    BYTE lsa_type, ULONG opaque_lsid, void *data, USHORT data_size, USHORT age);
void ospf_originate_type_10_links_advertisement (OSPF_AREA_ENTRY *sptr_area,
    BYTE lsa_type, ULONG opaque_lsid, void *data, USHORT data_size, USHORT age);
void ospf_originate_type_11_links_advertisement (OSPF_INTERFACE *sptr_interface, 
                                                 BYTE lsa_type, 
                                                 ULONG opaque_lsid, 
                                                 void *data, 
                                                 USHORT data_size, 
                                                 USHORT age);
#endif /* __OPAQUE_LSA__ */
ULONG ospf_get_metric_cost_of_address_range (
    OSPF_ADDRESS_RANGE_LIST_ENTRY *sptr_address_range);
ULONG ospf_get_metric_cost_of_address_range_with_head (
    OSPF_ADDRESS_RANGE_LIST_ENTRY *sptr_address_range,
    OSPF_ROUTING_TABLE_NODE * sptr_routing_table_node[OSPF_ROUTE_TABLE_MAX][OSPF_RT_HASH_TABLE_SIZE]);
BOOLEAN ospf_is_address_range_active (
     OSPF_ADDRESS_RANGE_LIST_ENTRY *sptr_address_range);
void ospf_generate_summary_lsa_for_address_range (
    OSPF_ADDRESS_RANGE_LIST_ENTRY *addr_range,
    enum BOOLEAN prematurely_age);
void ospf_advertise_lsas_contained_in_address_range (
     OSPF_ADDRESS_RANGE_LIST_ENTRY *sptr_address_range,
     BOOLEAN prematurely_age);


/******************************/
/* ospf_link_state_database.c */
/******************************/

/* SPR 85050 */
OSPF_LS_DATABASE_ENTRY *ospf_install_a_new_advertisement_in_the_link_state_database (
                                                    OSPF_LS_DATABASE_ENTRY *sptr_old_instance,
                                                    UNION_OSPF_LINK_STATE_ADVERTISEMENT *sptr_advertisement,
                                                    OSPF_AREA_ENTRY *sptr_area, 
                                                    enum TEST* different_lsa, 
                                                    int route_protocol);
OSPF_LS_DATABASE_ENTRY *ospf_find_LSA (OSPF_AREA_ENTRY *sptr_area,ULONG id,ULONG advertising_router,BYTE_ENUM (OSPF_LS_TYPE) type);
OSPF_LS_DATABASE_ENTRY *ospf_find_network_LSA (OSPF_AREA_ENTRY *sptr_area,ULONG id,ULONG advertising_router);
OSPF_LS_DATABASE_ENTRY *ospf_free_database_entry (OSPF_LS_DATABASE_ENTRY *sptr_database_entry);
int ospfAvlLsdbCompare (AVL_NODE *node, void* key);
enum TEST ospf_check_if_previous_instance_exists (OSPF_LS_DATABASE_ENTRY *sptr_old_instance);
enum TEST ospf_check_if_new_advertisement_is_different_from_its_previous_instance (
                                           UNION_OSPF_LINK_STATE_ADVERTISEMENT *sptr_advertisement,
                                           OSPF_LS_DATABASE_ENTRY *sptr_old_instance);
void ospf_flush_the_link_state_database_of_router_lsa ();
void ospf_flush_the_link_state_database_of_network_lsa ();
void ospf_flush_the_link_state_database_of_network_summary_lsa ();
void ospf_flush_the_link_state_database_of_asbr_summary_lsa ();
#if defined (__NSSA__)
void ospf_flush_the_link_state_database_of_type_7_lsa ();
#endif /* __NSSA__ */

void ospf_flush_the_link_state_database_of_external_lsa ();
void ospf_flush_the_link_state_database_of_self_originated_external_lsa (
    ULONG route_protocol, enum BOOLEAN is_default_route);
void ospf_flush_lsdb_of_external_lsas_associated_with_neighbor (
    OSPF_NEIGHBOR * sptr_neighbor);
OSPF_LS_DATABASE_ENTRY *ospf_process_old_ls_summary_summaryASBR_and_external_advertisements (
                                                OSPF_LS_DATABASE_ENTRY *sptr_old_instance, 
                                                OSPF_AREA_ENTRY *sptr_area);

/**************************/
/* ospf_neighbor_events.c */
/**************************/

void ospf_process_neighbor_hello_received_event (OSPF_INTERFACE *sptr_interface,OSPF_NEIGHBOR *sptr_neighbor,enum OSPF_NEIGHBOR_EVENT event);
void ospf_process_neighbor_start_event (OSPF_INTERFACE *sptr_interface, OSPF_NEIGHBOR *sptr_neighbor,enum OSPF_NEIGHBOR_EVENT event);
void ospf_process_neighbor_2_way_received_event (OSPF_INTERFACE *sptr_interface,OSPF_NEIGHBOR *sptr_neighbor,enum OSPF_NEIGHBOR_EVENT event);
void ospf_process_neighbor_negotiation_done_event (OSPF_INTERFACE *sptr_interface,OSPF_NEIGHBOR *sptr_neighbor,enum OSPF_NEIGHBOR_EVENT event);
void ospf_process_neighbor_exchange_done_event (OSPF_INTERFACE *sptr_interface,OSPF_NEIGHBOR *sptr_neighbor,enum OSPF_NEIGHBOR_EVENT event);
void ospf_process_neighbor_loading_done_event (OSPF_INTERFACE *sptr_interface,OSPF_NEIGHBOR *sptr_neighbor,enum OSPF_NEIGHBOR_EVENT event);
void ospf_start_forming_an_adjacency_with_the_neighbor (OSPF_INTERFACE *sptr_interface,OSPF_NEIGHBOR *sptr_neighbor,enum OSPF_NEIGHBOR_EVENT event);
void ospf_maintain_or_destroy_existing_adjacency (OSPF_INTERFACE *sptr_interface,OSPF_NEIGHBOR *sptr_neighbor,enum OSPF_NEIGHBOR_EVENT event);
void ospf_tear_down_and_reestablish_adjacency (OSPF_INTERFACE *sptr_interface,OSPF_NEIGHBOR *sptr_neighbor,enum OSPF_NEIGHBOR_EVENT event);
void ospf_process_neighbor_1_way_received_event (OSPF_INTERFACE *sptr_interface,OSPF_NEIGHBOR *sptr_neighbor,enum OSPF_NEIGHBOR_EVENT event);
void ospf_process_neighbor_down_event (OSPF_INTERFACE *sptr_interface,OSPF_NEIGHBOR *sptr_neighbor,enum OSPF_NEIGHBOR_EVENT event);

void ospf_clear_advertisements_from_lists (OSPF_INTERFACE *sptr_interface,OSPF_NEIGHBOR *sptr_neighbor);
/*********************************/
/* ospf_neighbor_state_machine.c */
/*********************************/

void ospf_execute_neighbor_state_machine (enum OSPF_NEIGHBOR_EVENT event,enum OSPF_NEIGHBOR_STATE state,OSPF_INTERFACE *sptr_interface,
    OSPF_NEIGHBOR *sptr_neighbor);

/*************************/
/* ospf_list_utilities.c */
/*************************/

void ospf_insert_node_in_list (OSPF_GENERIC_NODE *sptr_node,OSPF_GENERIC_NODE *sptr_previous_node);
void ospf_add_node_to_end_of_list (OSPF_GENERIC_NODE *sptr_node,OSPF_GENERIC_NODE *sptr_first_node_in_list);
OSPF_GENERIC_NODE *ospf_free_entire_list (OSPF_GENERIC_NODE *sptr_first_node_in_list);
void ospf_remove_node_from_list (OSPF_GENERIC_NODE **ptr_sptr_first_node, OSPF_GENERIC_NODE *sptr_node);
void ospf_add_neighbor (OSPF_INTERFACE *sptr_interface,OSPF_NEIGHBOR *sptr_neighbor_to_add);
OSPF_LS_DATABASE_NODE *ospf_find_advertisement_on_neighbors_retransmit_list (OSPF_NEIGHBOR *sptr_neighbor,OSPF_LS_DATABASE_ENTRY *sptr_advertisement);
void ospf_remove_node_from_neighbors_retransmit_queue (OSPF_NEIGHBOR *sptr_neighbor,OSPF_LS_DATABASE_NODE *sptr_retransmission_node);
void ospf_remove_neighbor_from_advertisements_retransmit_list (OSPF_LS_DATABASE_ENTRY *sptr_advertisement,OSPF_NEIGHBOR *sptr_neighbor);
STATUS ospf_add_database_entry_to_neighbor_retransmit_list (OSPF_NEIGHBOR *sptr_neighbor,OSPF_LS_DATABASE_ENTRY *sptr_database_entry);
STATUS ospf_add_neighbor_to_database_retransmit_list (OSPF_LS_DATABASE_ENTRY *sptr_database_entry,OSPF_NEIGHBOR *sptr_neighbor);
void ospf_clean_up_retransmit_lists_affiliated_with_this_advertisement (OSPF_LS_DATABASE_ENTRY *sptr_advertisement);
void ospf_remove_all_database_pointers_from_neighbor_retransmit_list (OSPF_NEIGHBOR *sptr_neighbor);
void ospf_free_neighbor_database_summary_list (OSPF_NEIGHBOR *sptr_neighbor);
OSPF_LS_REQUEST *ospf_find_advertisement_on_neighbors_ls_request_list (OSPF_NEIGHBOR *sptr_neighbor,OSPF_LS_DATABASE_ENTRY *sptr_advertisement);
void ospf_free_neighbor_ls_request_list (OSPF_NEIGHBOR *sptr_neighbor);
void ospf_free_interface_acknowledgement_list (OSPF_INTERFACE *sptr_interface,enum BOOLEAN check_number_of_neighbors_in_init_state);
/* This fn added to this file (Deleted from ospf_receive_update.c) */
void ospf_remove_current_database_copy_from_all_neighbors_retransmission_lists (OSPF_LS_DATABASE_ENTRY *sptr_database_entry, 
                                                                                OSPF_INTERFACE *sptr_interface);
void ospf_free_ospf_classes_area_lists (void);
void ospf_free_all_the_associated_area_pointers(OSPF_AREA_ENTRY* sptr_area);
/* trace B0456 */
void ospf_free_routing_table_nodes (OSPF_ROUTING_TABLE_NODE **sptr_routing_table_hashbin, 
                                    OSPF_ROUTING_TABLE_NODE *sptr_first_discarded_or_valid_routing_table_node);
void ospf_free_areas_shortest_path_tree_nodes (OSPF_SHORTEST_PATH_NODE* sptr_first_shortest_path_candidate);
void ospf_free_areas_link_state_database(OSPF_AREA_ENTRY *sptr_area);
void ospf_free_interfaces();
void ospf_free_neighbor (OSPF_NEIGHBOR *sptr_neighbor);
void ospf_search_ls_database_and_remove_neighbor_from_that_lsas_neighbor_retx_list_on_event_neighbor_down (OSPF_NEIGHBOR* sptr_neighbor);
enum TEST ospf_check_if_out_going_interface_belongs_to_the_passed_area (ULONG out_going_interfaces_address, OSPF_AREA_ENTRY* sptr_area);
USHORT ospf_host_to_net_short (USHORT host_order_short_value);
USHORT ospf_net_to_host_short (USHORT net_order_short_value);
ULONG ospf_host_to_net_long (ULONG host_order_long_value);
ULONG ospf_net_to_host_long (ULONG net_order_long_value);
ULONG ospf_long_swap (ULONG ulong_to_swap_bytes_with);


/**************************/
/* ospf_print_utilities.c */
/**************************/
#if defined __OSPF_DEBUG__
void ospf_display_packet (OSPF_PACKET *sptr_packet,enum BOOLEAN receive);
void ospf_display_ospf_header (OSPF_HEADER *sptr_packet,enum BOOLEAN receive);
void ospf_display_routing_table (void);
#endif /*__OSPF_DEBUG__*/

void ospf_print_memory_error_message_and_free_buffer_if_necessary (void **vptr_buffer,const char *cptr_string);
void ospf_printf (enum OSPF_PRINTF_GROUPS printf_group,const char *cptr_format, ...);

/**********************************/
/* ospf_receive_acknowledgement.c */
/**********************************/

enum OSPF_PACKET_STATE ospf_ls_acknowledgement_packet_received (OSPF_LS_ACKNOWLEDGEMENT_HEADER *sptr_acknowledgement_packet,
    OSPF_NEIGHBOR *sptr_neighbor,OSPF_INTERFACE *sptr_interface,ULONG size_of_packet);

/***************************/
/* ospf_receive_database.c */
/***************************/

enum OSPF_PACKET_STATE ospf_database_packet_received (OSPF_DATABASE_HEADER *sptr_database_packet_header,
                                                      OSPF_NEIGHBOR *sptr_neighbor,
                                                      OSPF_INTERFACE *sptr_interface,
                                                      ULONG size_of_packet);
/***************************/
/* ospf_receive_packet.c */
/***************************/
void ospf_router_rx_packet(OSPF_INTERFACE *sptr_interface, OSPF_PACKET *sptr_ospf_packet,
                           USHORT packet_size, ULONG source_ip_address,
                           ULONG destination_ip_address);

/**************************/
/* ospf_receive_request.c */
/**************************/

enum OSPF_PACKET_STATE ospf_ls_request_packet_received (OSPF_LS_REQUEST_HEADER *sptr_ls_request_header,OSPF_NEIGHBOR *sptr_neighbor,
    OSPF_INTERFACE *sptr_interface,ULONG size_of_packet);

/*************************/
/* ospf_receive_update.c */
/*************************/

enum OSPF_PACKET_STATE ospf_ls_update_packet_received  (OSPF_LS_UPDATE_HEADER *sptr_ls_update_header,OSPF_NEIGHBOR *sptr_neighbor,
    OSPF_INTERFACE *sptr_interface_packet_received_on,ULONG source_address,ULONG destination_address);

/****************************/
/* ospf_receive_utilities.c */
/****************************/

enum TEST ospf_check_if_advertisements_link_state_id_is_equal_to_one_of_the_routers_own_IP_interface_addresses (
    OSPF_ROUTER_LINK_ADVERTISEMENT_HEADER *sptr_advertisement);

/************************/
/* ospf_routing_table.c */
/************************/

void ospf_schedule_routing_table_build (void);
void ospf_build_routing_table (void);
void ospf_examine_a_single_destination_for_a_better_path (OSPF_ADVERTISEMENT_NODE *sptr_advertisement_node,OSPF_AREA_ENTRY *sptr_area);
enum BOOLEAN ospf_add_new_path_to_routing_table_entry_and_update_rtm (OSPF_ROUTING_TABLE_NODE *sptr_routing_table_entry_for_N,
    ULONG next_hop_router, OSPF_ROUTING_TABLE_ENTRY *sptr_routing_table_entry);
enum BOOLEAN ospf_next_hop_block_changed (OSPF_ROUTING_TABLE_NODE *sptr_old_routing_table_node,
    OSPF_ROUTING_TABLE_NODE *sptr_routing_table_node);
enum BOOLEAN ospf_get_new_next_hop_blocks_and_mark_ospf_rt_node_new (OSPF_ROUTING_TABLE_NODE *sptr_routing_table_node,
    OSPF_NEXT_HOP_BLOCK *sptr_new_next_hop_block, OSPF_NEXT_HOP_BLOCK **ptr_sptr_new_next_hops_copy);

/********************************/
/* ospf_routing_table_lookups.c */
/********************************/
OSPF_ROUTING_TABLE_ENTRY *ospf_find_routing_table_entry (ULONG neighbor_id,enum OSPF_ROUTE_DESTINATION_TYPE destination_type,
    enum OSPF_ROUTE_PATH_TYPE path_type,OSPF_AREA_ENTRY *sptr_area);
/* SPR#76812 */
OSPF_ROUTING_TABLE_ENTRY *ospf_find_routing_table_entry_1583_asbr (ULONG neighbor_id);
OSPF_ROUTING_TABLE_ENTRY *ospf_routing_table_lookup (ULONG destination_ip_address,ULONG type_of_service,
    OSPF_ROUTING_TABLE_NODE *sptr_routing_table_node);
OSPF_ROUTING_TABLE_NODE *ospf_find_routing_table_node (ULONG neighbor_id, enum OSPF_LS_TYPE lsa_header_type,
    enum OSPF_ROUTE_PATH_TYPE path_type, OSPF_AREA_ENTRY *sptr_area);
OSPF_ROUTING_TABLE_NODE *ospf_find_router_or_network_routing_table_node (ULONG destination_id_to_look, enum OSPF_LS_TYPE lsa_header_type,
    enum OSPF_ROUTE_PATH_TYPE path_type,OSPF_AREA_ENTRY *sptr_area);
OSPF_ROUTING_TABLE_ENTRY* ospf_find_routing_table_entry_for_ABR(ULONG destination_id_to_look, ULONG area_id);

/********************************/
/* ospf_routing_table_updates.c */
/********************************/
void ospf_update_routing_table_due_to_receipt_of_a_new_summary_link_advertisement (OSPF_SUMMARY_LINK_ADVERTISEMENT_HEADER *sptr_summary,
    OSPF_AREA_ENTRY *sptr_area_A);
#if defined (__NSSA__)
void ospf_update_routing_table_due_to_receipt_of_a_new_type_7_external_link_advertisement (OSPF_TYPE_7_LINK_ADVERTISEMENT_HEADER *sptr_type_7,
    OSPF_AREA_ENTRY *sptr_area_A);
#endif /*__NSSA__*/
void ospf_invalidate_routing_table_entry (OSPF_ROUTING_TABLE_NODE *sptr_routing_table_node_for_N,ULONG network_mask,
    ULONG destination_N,OSPF_AREA_ENTRY *sptr_area_A);
/* SPR#76812 */
void ospf_add_routing_table_node (OSPF_ROUTING_TABLE_NODE* sptr_routing_table_node);
void ospf_delete_routing_table_node (OSPF_ROUTING_TABLE_NODE* sptr_routing_table_node);

/************************************/
/* ospf_shortest_path_calculation.c */
/************************************/

void ospf_calculate_shortest_path_tree (OSPF_AREA_ENTRY *sptr_area);
enum TEST ospf_check_if_link_exists (UNION_OSPF_LINK_STATE_ADVERTISEMENT *sptr_advertisement,ULONG vertex,OSPF_AREA_ENTRY *sptr_area);
OSPF_NEXT_HOP_BLOCK *ospf_calculate_the_set_of_next_hops (OSPF_SHORTEST_PATH_NODE *sptr_destination,OSPF_SHORTEST_PATH_NODE *sptr_parent,
    OSPF_ROUTER_LINK_PIECE *sptr_link, OSPF_AREA_ENTRY *sptr_area);
OSPF_NEXT_HOP_BLOCK *ospf_inherit_the_set_of_next_hops_from_node_X (OSPF_NEXT_HOP_BLOCK *sptr_node_X_next_hops);
void ospf_schedule_shortest_path_first_job (OSPF_AREA_ENTRY *sptr_area);


/*****************/
/* ospf_system.c */
/*****************/
void ospf_init_stub();


/****************/
/* ospf_timer.c */
/****************/

void ospf_router_timer (void);
/* SPR 85432 -- Begin */
void ospf_hello_timer (void);
/* SPR 85432 -- End */

/*******************/
/* ospf_transmit.c */
/*******************/

void ospf_tx_packet (OSPF_HEADER *sptr_packet,OSPF_INTERFACE *sptr_interface,enum OSPF_UNION_PACKET_TYPES type,ULONG length,
    ULONG destination,enum BOOLEAN free_packet);
void ospf_send_completion_packet (USHORT port_number,void *vptr_txed_packet);

/***********************************/
/* ospf_transmit_acknowledgement.c */
/***********************************/

enum OSPF_ACKNOWLEDGEMENT_RETURN_TYPE ospf_send_acknowledgement (OSPF_INTERFACE *sptr_interface, OSPF_NEIGHBOR *sptr_neighbor,
    OSPF_LS_HEADER_QUEUE **ptr_to_sptr_header_queue);

/****************************/
/* ospf_transmit_database.c */
/****************************/

void ospf_send_an_empty_database_description_packet (OSPF_INTERFACE *sptr_interface,OSPF_NEIGHBOR *sptr_neighbor);
void ospf_send_database_summary (OSPF_INTERFACE *sptr_interface,OSPF_NEIGHBOR *sptr_neighbor);

/***************************/
/* ospf_transmit_request.c */
/***************************/

void ospf_send_ls_request_packet (OSPF_INTERFACE *sptr_interface,OSPF_NEIGHBOR *sptr_neighbor);

/**************************/
/* ospf_transmit_update.c */
/**************************/

void ospf_send_ls_update (OSPF_LS_DATABASE_NODE *sptr_database_list,OSPF_NEIGHBOR *sptr_neighbor,OSPF_INTERFACE *sptr_interface,
enum BOOLEAN retransmit_flag, enum BOOLEAN advertisement_installed);
void ospf_free_timestamps_for_lsdb_entry(OSPF_LS_DATABASE_ENTRY *sptr_database_entry);
enum TEST ospf_send_ls_update_for_large_lsa (ULONG destination,
                                             OSPF_INTERFACE *sptr_interface,
                                             OSPF_LS_DATABASE_ENTRY *sptr_database);


/*****************************/
/* ospf_transmit_utilities.c */
/*****************************/

OSPF_HEADER *ospf_get_an_ospf_send_packet (ULONG size_of_packet);
void ospf_free_an_ospf_send_packet (OSPF_HEADER *sptr_tx_packet);
ULONG ospf_determine_packet_destination (OSPF_INTERFACE *sptr_interface,OSPF_NEIGHBOR *sptr_neighbor);
ULONG ospf_determine_destination (OSPF_INTERFACE *sptr_interface,OSPF_NEIGHBOR *sptr_neighbor);


/********************/
/* ospf_utilities.c */
/********************/

enum BOOLEAN ospf_check_if_addresses_match (ULONG address_1,ULONG address_2);
enum BOOLEAN ospf_check_for_transit_areas (void);
void ospf_generate_network_and_router_link_state_advertisements (OSPF_INTERFACE *sptr_interface);
ULONG ospf_get_authentication_size (OSPF_INTERFACE *sptr_interface);
ULONG ospf_get_interface_mtu (OSPF_INTERFACE *sptr_interface);
enum BOOLEAN ospf_validate_interface(ULONG interface_address);
enum BOOLEAN ospf_check_if_area_border_router (void);
enum TEST ospf_check_if_more_recent (OSPF_LS_HEADER *sptr_new_ls_header,OSPF_LS_HEADER *sptr_current_ls_header,ULONG elapsed_time);
enum TEST ospf_check_if_same_instance (OSPF_LS_HEADER *sptr_new_ls_header,OSPF_LS_HEADER *sptr_current_ls_header,ULONG elapsed_time);
seq_t ospf_increment_sequence_number (seq_t sequence_number);
void ospf_add_entry_to_interfaces_delayed_acknowledgement_list (OSPF_INTERFACE *sptr_interface,OSPF_LS_DATABASE_ENTRY *sptr_database_entry);
enum TEST ospf_check_for_valid_ls_type (OSPF_INTERFACE *sptr_interface,OSPF_LS_HEADER *sptr_ls_header, enum OSPF_UNION_PACKET_TYPES packet_type);
OSPF_HEADER *ospf_new_link_state_update (UNION_OSPF_ADVERTISEMENT_HEADER **ptr_to_uptr_acknowledgement_header,OSPF_INTERFACE *sptr_interface);
enum TEST ospf_check_if_ip_destination_falls_into_address_range (ULONG destination_ip_address,ULONG destination_id,ULONG address_mask);
void ospf_transfer_fields_from_ls_request_structure_to_ls_header_structure (OSPF_LS_REQUEST *sptr_ls_request,OSPF_LS_HEADER *sptr_ls_header);
enum TEST ospf_parse_port_plain_text_or_md5_password (char *cptr_port_password_string);
void ospf_process_export_route_queue (void);
void ospf_process_export_route_queue_all (void);
void ospf_notify_areas_for_abr_change( ulong_t area_id_to_skip );
void ospf_flush_network_link_advertisement( OSPF_INTERFACE *sptr_interface );
UINT ospf_get_system_elapsed_time_second ();
ULONG ospf_get_destination_address_of_PPP_link (ULONG ppp_src_addr);
ULONG ospf_get_mask_length (const struct in_addr *mask);

#if (_BYTE_ORDER == _BIG_ENDIAN )
ULONG ospf_min ( ULONG arg1, ULONG arg2);
#endif /* _BYTE_ORDER == _BIG_ENDIAN */
struct ifnet* dstaddr_to_ifp(struct in_addr addr);

/********************/
/* ospf_to_rtm.c    */
/********************/

enum TEST ospf_export_route_to_other_protocols (enum OSPF_ROUTING_TABLE_UPDATE_ACTION action, 
                                                OSPF_ROUTING_TABLE_ENTRY *sptr_ospf_route_entry_to_export);

extern void ospf_ctlinput(unsigned long intf, unsigned short intf_index, int intf_flags);

STATUS ospf_is_protocol_redistributed (OSPF_IP_ROUTE_ENTRY *sptr_ip_route);

STATUS ospf_add_route(OSPF_IP_ROUTE_ENTRY *   sptr_route_entry);
STATUS ospf_delete_route(OSPF_IP_ROUTE_ENTRY *   sptr_route_entry);
STATUS ospf_change_route(OSPF_IP_ROUTE_ENTRY *   sptr_route_entry);
void ospf_receive_rtm_socket_input ();

STATUS ospf_routing_table_walk (
    enum BOOLEAN asCapable, enum BOOLEAN redistStatic, enum BOOLEAN redistRIP,
    enum BOOLEAN redistBGP, enum BOOLEAN redistDefault, enum BOOLEAN reachable);

void ospf_ip_route_get (
    OSPF_EXTERNAL_ROUTE *   p_external_route,
    OSPF_IP_ROUTE_ENTRY * p_ip_route_entry);

void ospf_protocol_rtm_terminate (OSPF_RTM_HANDLE protoRtmHandle);

enum TEST ospf_send_ls_update_for_large_lsa (ULONG destination,
                                             OSPF_INTERFACE *sptr_interface,
                                             OSPF_LS_DATABASE_ENTRY *sptr_database);

enum TEST ospf_lookup_redistribution_policy (
    ULONG network_address, ULONG route_proto);



/********************/
/* ospf_init.c     */
/********************/

void ospf_register_with_ip (FP_OSPF_RECEIVE pospfReceive, FP_OSPF_SEND pospfSend);
void ospf_deregister_with_ip (FP_OSPF_RECEIVE pospfReceive, FP_OSPF_SEND pospfSend);
bool ospfPartMemInitialize ();
STATUS ospf_startup( void );


/*******************************/
/* ospf_opaque_lsa.c           */
/*******************************/
/*opaque lsa prototypes for setting opaque lsa data jkw*/
#if defined (__OPAQUE_LSA__)
int ospfSetOrigOpaqueLsa(ULONG ip_address, ULONG netmask, BYTE lsa_type,
    BYTE opaque_type);
int ospfRemoveType9(ULONG opaque_lsid, ULONG opaque_advertising_router);
int ospfRemoveType10(ULONG opaque_lsid, ULONG opaque_advertising_router);
int ospfRemoveType11(ULONG opaque_lsid, ULONG opaque_advertising_router);
int ospfRefreshOpaqueLsa(ULONG ip_address, ULONG netmask, BYTE lsa_type,
    ULONG opaque_lsid, ULONG opaque_advertising_router);
ULONG ospfRetrieveNumOpaqueLsa(BYTE lsa_flooding_type);
OSPF_TYPE_9_LSA_LINK *ospfRetrieveType9(enum OSPF_SEARCH_TYPE key, ULONG key_value, ULONG area_id);
OSPF_TYPE_10_LSA_LINK *ospfRetrieveType10(enum OSPF_SEARCH_TYPE key, ULONG key_value, ULONG area_id);
OSPF_TYPE_11_LSA_LINK *ospfRetrieveType11(enum OSPF_SEARCH_TYPE key, ULONG key_value);
int ospfRemoveAllType9();
int ospfRemoveAllType10();
int ospfRemoveAllType11();
int ospfRemoveAllTypes();

int ospfRegisterOpaqueCallback(BYTE lsa_type, 
                               BYTE opaque_type, 
                               int (* new_interface_hook)(struct OSPF_INTERFACE *interface),
                               int (* delete_interface_hook)(struct OSPF_INTERFACE *interface),
                               void (* interface_state_machine_change_hook)(enum OSPF_INTERFACE_EVENT event,
                                       enum OSPF_INTERFACE_STATE state,
                                       struct OSPF_INTERFACE *interface),
                               void (* neighbor_state_machine_change_hook)(enum OSPF_NEIGHBOR_EVENT event,
                                       enum OSPF_NEIGHBOR_STATE state,
                                       struct OSPF_INTERFACE *sptr_interface, 
                                       struct OSPF_NEIGHBOR *neighbor),
                               int (* opaque_lsa_originate_hook)(void *arg, 
                                      BYTE lsa_type, 
                                      BYTE opaque_type, 
                                      USHORT age),
                               void (* opaque_lsa_refresh_hook)(void *arg, BYTE lsa_type, ULONG opaque_lsid, USHORT age),
                               void (* opaque_lsa_received_hook)(UNION_OSPF_LINK_STATE_ADVERTISEMENT *sptr_advertisement));
int ospfDeregisterOpaqueCallback(BYTE lsa_type, BYTE opaque_type);
OSPF_OPAQUE_CALLBACK *ospfRetrieveOpaqueCallback(BYTE lsa_type, BYTE opaque_type);
int ospfDeregAllOpqCallback();
void ospf_opaque_stub();
#endif /* __OPAQUE_LSA__ */
OSPF_TE_PARAMETERS *ospfTeDecodeTlv(USHORT tlvlen, int TElsatype, struct OSPF_TE_TLV_DATA *pTLV);
int ospfTeEncodeTlv (void *arg, OSPF_TE_PARAMETERS *pTLVparms,char **pOpaqueDataBody,USHORT *pOpaqueDataLen);
void ospf_te_build_tlv_header (char **new_lsa, USHORT *length, OSPF_TE_TLV_HEADER *tlv_header);
void ospf_te_build_tlv_link_header (char **new_lsa, USHORT *length, OSPF_TE_PARAMETERS *pTLVparms);

/********************/
/* ospf_show_lsdb.c */
/********************/
void ospf_show_stub();
STATUS ospfShowLsdb();
STATUS ospfShowLsdbSummary();
STATUS ospfShowRouterLsdb();
STATUS ospfShowNetworkLsdb();
STATUS ospfShowSummaryLsdb();
STATUS ospfShowAsbrSummaryLsdb();
STATUS ospfShowExternalLsdb();
#if defined (__NSSA__)
STATUS ospfShowType7Lsdb();
#endif /* __NSSA__ */

#if defined (__OPAQUE_LSA__)
void ospf_print_type_9(OSPF_AREA_ENTRY *sptr_area,OSPF_LS_DATABASE_ENTRY *sptr_database_entry);
void ospf_print_type_10(OSPF_AREA_ENTRY *sptr_area,OSPF_LS_DATABASE_ENTRY *sptr_database_entry);
void ospf_print_type_11(OSPF_LS_DATABASE_ENTRY *sptr_database_entry);
void ospfShowOpaqueLsdb(enum OSPF_SEARCH_TYPE key, ULONG key_value);
#endif /* __OPAQUE_LSA__ */

/********************/
/* ospf_show_lsdb.c */
/********************/
void ospf_show_routing_stub();
STATUS ospfShowRoutingTable();

/********************************************/
/* prototypes for ospf_dynamic_config.c     */
/********************************************/
enum BOOLEAN ospf_dynamic_validate_area( ULONG area_id );
STATUS ospf_dynamic_create_nbma_ptmp_neighbor( OSPF_NEIGHBOR *sptr_neighbor,
                                              ULONG intfAddr, ULONG intfAddrLess );
void ospf_dynamic_destroy_nbma_ptmp_neighbor( OSPF_INTERFACE *sptr_interface,
                                              OSPF_NEIGHBOR *sptr_neighbor );
STATUS ospf_dynamic_config_metric( OSPF_INTERFACE *sptr_interface );
void ospf_dynamic_step_intfSm( enum OSPF_INTERFACE_EVENT event,
                               enum OSPF_INTERFACE_STATE state,
                               OSPF_INTERFACE *sptr_interface );
STATUS ospf_dynamic_create_area( OSPF_AREA_ENTRY *sptr_area );
STATUS ospf_dynamic_reinit_area( OSPF_AREA_ENTRY *sptr_area, enum BOOLEAN delete_stub );
STATUS ospf_dynamic_destroy_area( OSPF_AREA_ENTRY *sptr_area );
void ospf_dynamic_reset_interface( OSPF_INTERFACE *sptr_interface );
STATUS ospf_dynamic_init_unnumbered_interface( OSPF_INTERFACE *sptr_interface );
STATUS ospf_dynamic_reinit_interface( OSPF_AREA_ENTRY *sptr_new_area,
                                      OSPF_INTERFACE *sptr_interface,
                                      enum BOOLEAN  reset_interface );
STATUS ospf_dynamic_create_interface( OSPF_INTERFACE *sptr_interface );
STATUS ospf_dynamic_destroy_interface(OSPF_INTERFACE *sptr_interface,
                                      OSPF_AREA_ENTRY  *sptr_area );
STATUS ospf_dynamic_create_virtIf( OSPF_INTERFACE *sptr_interface,
                                   OSPF_AREA_ENTRY *sptr_area );
STATUS ospf_dynamic_destory_virtIf( OSPF_INTERFACE *sptr_interface );
void ospf_dynamic_send_default_summary_lsa( OSPF_AREA_ENTRY *sptr_area,
                                     enum BOOLEAN send_max_age );
void ospf_dynamic_send_summary_lsa_for_stub_area( OSPF_AREA_ENTRY *sptr_area,
                                                  enum BOOLEAN send_max_age);
void ospf_dynamic_create_area_range( OSPF_AREA_ENTRY *sptr_area,
                                     OSPF_ADDRESS_RANGE_LIST_ENTRY *addr_ranges );
void ospf_dynamic_destroy_area_range( OSPF_AREA_ENTRY *sptr_area,
                                      OSPF_ADDRESS_RANGE_LIST_ENTRY *addr_range );
enum BOOLEAN ospf_dynamic_config_overflow( ULONG new_extLsdb_limit,
                                           ULONG new_overflow_interval );
void ospf_dynamic_flush_opaque_lsa( OSPF_AREA_ENTRY *sptr_area );
void ospf_dynamic_flush_external_routes( ULONG route_protocol, enum BOOLEAN is_default_route );
void ospf_dynamic_export_external_routes( void );
STATUS ospf_dynamic_config_redistribution( LINK *sptr_address_mask_list_head,
                                           ULONG network_address,
                                           ULONG network_mask,
                                           ULONG metric_type,
                                           ULONG metric_value,
                                           ULONG route_type,
                                           ULONG route_tag,
                                           BYTE  options);

void ospf_dynamic_destroy_redistrib (
    LINK *sptr_address_mask_list_head,
    OSPF_ADDRESS_MASK_PAIRS *sptr_address_mask_pair,
    ULONG route_typ);

STATUS ospf_dynamic_config_ip_route_policy(ULONG network_address, ULONG network_mask,
                                           UCHAR route_weight );
void ospf_dynamic_destroy_ip_route_policy( ULONG network_address, ULONG network_mask,
                                           UCHAR route_weight );

/************************/
/* ospf_show_routines.c */
/************************/
STATUS ospfEnableDebug(char*);
STATUS ospfDisableDebug(char*);
STATUS ospfShowDebug();
/* SPR 81808 */
STATUS ospfShowMemPartition(int options);
/* SPR 88613 */
STATUS ospfShowAreaAddressRange();
void ospf_show_routines_stub();

/************************/
/* ospf_cfg_routines.c */
/************************/
STATUS ospfSetPassiveMode (char *commands);
STATUS ospfSetPassivePort (char *cptr_ip_address_string, enum OSPF_MODE mode);
STATUS ospfSetOpaqueEnable();
STATUS ospfSetOpaqueDisable();
STATUS ospfSetTotalAreaAddressRange(ULONG total_number_area_ranges);
void ospf_cfg_routines_stub();


/************************/
/* ospf_ky_driver.c */
/************************/
#if defined (__KY_DRIVER__)
void ospf_ky_driver();
void ospf_ky_stub();
#endif /* __KY_DRIVER__ */

/************************/
/* ospf_sysctl.c */
/************************/

void ospf_sysctl_input (
    int rsock_message, ushort_t interface_index, void *sptr_ctl_data );

STATUS ospf_sysctl_rtableWalk
    (
    enum BOOLEAN asCapable,
    enum BOOLEAN redistributeLocal,
    enum BOOLEAN redistributeStatic,
    enum BOOLEAN redistributeRIP,
    enum BOOLEAN redistributeBGP,
    enum BOOLEAN redistributeDefault,
    enum BOOLEAN reachable
    );

STATUS ospf_sysctl_routeModify( ulong_t destIpAddr, ulong_t netMask );

/***************************************/
/* ospf_ospf_show_routing_table.c      */
/***************************************/

STATUS ospfShowAreas(void);
void   ospfShowInterface(void);
void   ospfShowNeighbor(void);

#endif /* _OSPF_PROTOTYPES_H_ */

