/* ospf_mib_helper_show.h - header file for OSPF MIB API show facility */

/* Copyright 1998-2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
02d,07jul04,kc  SPR 99485, Renamed the "status" display string to "adminStatus.
02c,17dec03,agi propogated weighted routes fixes
02b,10dec03,agi added missing imports
02a,10dec03,agi merged fixes from OSPF 2.1 patch branch
                added WRS copyright
01g,27oct03,kc  Fixed SPR#91541 - added wrnOspfToIpRouteTable support.
01f,13jun03,kc  Fixed SPR#85532 - added wrnOspfLocalIfTable support.
01e,06may03,mwv Fixed SPR#88210 - moved all mib display strings to 
                ospf_mib_helper_show.c and added import statements for those 
                display string here to resolve linker errors when including 
                ospf_mib_helper_show.h from multiple files.
01e,23may03,kc  SPR#88863 - added wrnOspfRedistributionTable support.
01d,06may03,mwv Fixed SPR#88210 - moved all mib display strings to 
                ospf_mib_helper_show.c and added import statements for those 
                display string here to resolve linker errors when including 
                ospf_mib_helper_show.h from multiple files.
01c,05feb03,kc  Fixed SPR#85806 - added prototypes for mib api show routines.
01b,12nov02,mwv Fixed SPR 83797 - correct the display string for OSPF LSDB type
01a,01dec01,kc  Initial file created
*/
 
/* 
DESCRIPTION

This file contains the dispaly string used by ospf_mib_mapi_show.c when
formatting the output.
*/

#ifndef _INCospf_mib_helper_showh
#define _INCospf_mib_helper_showh
                                   
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* row status display strings */
IMPORT char *rowStatus[];

/* true value display strings */
IMPORT char *trueValue[];

/* status display strings */
IMPORT char *adminStatus[];

/* display strings for authentication type */
IMPORT char *authType[];

/* display strings for interface state type */
IMPORT char *ifState[];

/* display strings for OSPF interface type */
IMPORT char *ifType[];

/* display strings for OSPF LSDB type */
IMPORT char *lsdbType[];

/* display strings for OSPF Neighbor state */
IMPORT char *nbrState[];

/* display strings for OSPF AS Type state */
IMPORT char *asType[];

/* display strings for OSPF Area Summary option */
IMPORT char *summaryType[];

/* display strings for OSPF Stub Metric Type */
IMPORT char *stubMetricType[];

/* display strings for OSPF Interface multicast option */
IMPORT char *mcastType[];

/* display strings for OSPF Neighbor Discovery method */
IMPORT char *permanence[];

/* display strings for OSPF AS-External LSA */
IMPORT char *extType;

/* display strings for OSPF Address Aggregate type */
IMPORT char *agType[];

/* display strings for OSPF Area Aggregate Effect */
IMPORT char *agEffect[];

/* display string for Opque LSA type */
IMPORT char *opaqueLsa[];

/* display string for M2 route proto type */
IMPORT char *extLsdbM2ProtoId[];

/* display string for OSPF Redistribution route type */
IMPORT char *redistribRouteType[];

/* display string for OSPF Redistribution metric type */
IMPORT char *redistribType[];


/* prototypes for mib api show routines (for debugging and diagnostic purposes) */

/* display configurations and statistics for Management Interface */    
IMPORT STATUS ospfMapiShow( void );

/* displays operational error statistics for Management Interface */    
IMPORT STATUS ospfMapiErrorShow( void );
        
/* display the OSPF MIB General Group Parameters */
IMPORT STATUS ospfMapiGenGroupShow( void );

/* displays all area instance */    
IMPORT STATUS ospfMapiAreaShow( void );

/* displays all stub area instances */
IMPORT STATUS ospfMapiStubShow( void );

/* displays all link state database instances (except external link state database) */    
IMPORT STATUS ospfMapiLsdbShow( void );

/* displays all host interface instances */    
IMPORT STATUS ospfMapiHostShow( void );

/* displays all interface instance */
IMPORT STATUS ospfMapiIfShow( void );

/* displays all interface metric instances */    
IMPORT STATUS ospfMapiIfMetricShow( void );

/* displays all virtual interfaces instances */    
IMPORT STATUS ospfMapiVirtIfShow( void );

/* displays all neighbor instances */
IMPORT STATUS ospfMapiNbrShow( void );

/* displays all virtual neighbor instances */    
IMPORT STATUS ospfMapiVirtNbrShow( void );

/* displays all external link state database instances */    
IMPORT STATUS ospfMapiExtLsdbShow( void );

/* displays all area aggregate instances */    
IMPORT STATUS ospfMapiAreaAggregateShow( void );

/* displays the WRN-OSPF Enterprise MIB General Group Parameters */
IMPORT STATUS ospfMapiWrnGenGroupShow( void );

/* displays information for Type-10 Opaque link state database for area */
IMPORT STATUS ospfMapiWrnAreaShow( void );

/* displays all Type-10 Opaque link state database instances */
IMPORT STATUS ospfMapiType10LsaShow( void );
IMPORT STATUS ospfMapiWrnLsdbShow( void );

/* displays all Type-9 Opaque link state database instances */
IMPORT STATUS ospfMapiType9LsaShow( void );    
IMPORT STATUS ospfMapiWrnLocalLsdbShow( void );

/* displays all Type-11 Opaque link state database instances */
IMPORT STATUS ospfMapiType11LsaShow( void );    
IMPORT STATUS ospfMapiWrnExtLsdbShow( void );

/* displays all interfaces connected to an area */    
IMPORT STATUS ospfMapiAreaIfShow( void );

/* displays the interface information maintained by OSPF MIB API */    
IMPORT STATUS ospfMapiIfConfigShow( void );   

/* display all the OSPF redistribution configuration */
IMPORT STATUS ospfMapiWrnRedistributionShow( void );

IMPORT STATUS ospfMapiWrnToIpRouteShow( void );
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _INCospf_mib_helper_showh */
