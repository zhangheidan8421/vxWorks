/* ospf_mib_api.h - public interface for managing OSPFv2 */

/* Copyright 1998-2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
04e,06may04,ram Fix for AVL memory corruption
04d,22apr04,ram AVL & Memory migration to shared library
04c,17dec03,agi propogated weighted routes fix
04b,16dec03,agi propogated latest MIB API fixes
04a,08dec03,agi merged fixes from OSPF 2.1 patch branch
                added include WRS copyright
02y,29oct03,kc  Fixed SPR#91541 - added wrnOspfToIpRouteTable configuration
                support to mApiOspfClass_t. Also added ospfMapiGetWrnToIpRoute()
                and ospfMapiGetWrnToIpRoute() prototypes.
03b,23sep03,kc  Added nvmWrnOspfSaveRtn, nvmWrnOspfDelRtn and nvmWrnOspfRetrieveRtn
                function callback to mApiOspfClass_t. Renamed nvramSaveRtn,
                nvramDelRtn and nvramRetrieveRtn to nvmOspfSaveRtn, nvmOspfDelRtn
                and nvmOspfRetrieveRtn. Added nvram callbacks for wrn-ospf mib to
                ospfMapiInit() prototype.
03a,17jun03,kc  Fixed SPR#85532 - added wrnOspfLocalIfTable configuration support
                to mApiOspfClass_t. Also added ospfMapiGetWrnLocalIf() and
                ospfMapiSetWrnLocalIf() prototypes.
02z,23may03,kc  Fixed SPR#88863 - added wrnOspfRedistributionTable configuration
                support to mApiOspfClass_t. Also added ospfMapiGetWrnRedistrib()
                and ospfMapiSetWrnRedistrib() prototypes.
02y,25may03,kc  Renamed the INCLUDE_OSPF_SHOW_ROUTINES define to
                INCLUDE_OSPF_MIB_SHOW_ROUTINES. Added new
                INCLUDE_OSPF_MIB_UTILITIES define.
02x,23may03,kc  Fixed SPR#88863 - added wrnOspfRedistributionTable configuration
                support to mApiOspfClass_t. Also added ospfMapiGetWrnRedistrib()
                and ospfMapiSetWrnRedistrib() prototypes.
02w,28feb03,kc  Fixed SPR#86502 - Added INCLUDE_OSPF_MIB_UTILITIES define. Also
                renamed INCLUDE_OSPF_SHOW_ROUTINES to INCLUDE_OSPF_MIB_SHOW_ROUTINES.
02v,28feb03,kc  Fixed SPR#86493 - changed the max number of  concurrent rows that
                can be configured for the Type-9 Opaque LSAs, Type-10 Opaque LSAs
                and Type-11 Opaque LSAs to match the user's guide.
02u,31jan03,kc  Fixed SPR#86007 - added ospfMapiSetHelper() prototype.
02t,19nov02,mwv Merge TMS code SPR 84284
02s,12nov02,kc  Changed returned type for ospfMapiDestroy prototype. Added
                ospfMapiIsInited() prototype.
02r,17jun02,kc  Added currObjInProcess to mApiOspfClass_t.
02q,10apr02,kc  Remerged changes for unnumbered interface support.
02p,29mar02,kc  Added ospfBackboneActive boolean flag to mApiOspfClass_t.
02o,26mar02,kc  Added ospfMapiDebug and ospfMapiErrorDebug variables to the
                mApiOspfPrintf and mApiOspfError debug macros.
02n,09mar02,kc  Added ospfBackboneExist boolean flag to mApiOspfClass_t.
02m,22feb02,kc  Renamed prototypes for all functions using WRS naming convention.
02l,13feb02,kc  Added currMapiReqType to mApiOspfClass_t.
02k,07feb02,kc  Added mApiNvmSetReqFailedCnt to mApiOspfClass_t.
02j,28dec01,kc  Added ospfProtoType_t enumeration values.
02i,15dec02,kc  Added argument for pObjInfo pointer to ospf_rs2mApi_errorGet().
02h,14dec01,kc  Removed ospf_mApi_avlTreeErase() prototype.
02g,06dec01,kc  Added semTimeout to mApiOspfClass_t.
02f,09nov01,kc  Redefine the DEFAULT_OSPF_MAPI_xxx_MAX system dependent
                configuration parameters.
02e,25oct01,kc  Always define INCLUDE_OSPF_SHOW_ROUTINES for now until Project
                Facility supports it.
02d,21oct01,kc  Added ospf_mApi_avlTreeErase() prototype.
02c,21oct01,kc  Added ospfProtoInit, ospfDbOverflow, ospfNssaEnabled and
                ospfOpaqueEnable boolean variables to mApiOspfClass_t.
02b,20oct01,kc  Added Area Aggregation support to mApiOspfClass_t.
02a,18oct01,kc  Redefine arguments for ospf_mApi_init() prototype.
01z,17oct01,kc  Added ospfRfcProto ospfProtoType_t enum value to mApiOspfClass_t.
01y,16oct01,kc  Removed __WRN_OSPF_MIB__ preproc.
01x,16oct01,kc  Added wrnOspf_mApi_getXXX() prototypes.
01w,16oct01,kc  Fixed OSPF_MIB_BUFLEN_CHECK macro. Exception is not error.
01v,15oct01,kc  Removed pOspfFreePool pointer from mApiOspfAvlClass_t. No longer
                preallocated all memory pool at init time.
01u,15oct01,kc  Changed DEFAULT_OSPF_MAPI_EXT_LSDB_MAX value to use the default
                value for external lsdb. Removed the mApiMaxOspfReqBuf argument
                from ospf_mApi_init().
01t,13oct01,kc  Added various operational error statistics to mApiOspfClass_t.
01s,10oct01,kc  Added ospf_mApi_setNbr() prototype.
01r,10oct01,kc  Moved mApiOspfClientType_t and mApiOspfClient_t declaration to
                ospf_rfc1850_mApiHelper.h.
01q,10oct01,kc  Added ospfNbrTable Row Status handler.
01p,08oct01,kc  Added wrn-ospf mib specific stuffs.
01o.07oct01,kc  Moved NVRAM_SAVE_FUNCPTR and NVRAM_RETRIEVE_FUNCPTR typedefs
                to ospf_mApi_helper.h.
01n,21sep01,kc  Renamed all mApiOspfAvlClass_t instances for clarification,
01m,20sep01,kc  Added DEFAULT_OSPF_REQUEST_BUFFER_CNT define.
01l,18sep01,kc  Added system configuration parameters for ospf enterprise mib.
01k,18sep01,kc  Modified ospf_mApi_init() prototype.
01j,15sep01,kc  Added function pointers for user supplied nvram save/retrieve
                callbacks. Save callback function pointers to mApiOspfClass_t.
01i,12sep01,kc  Added mApiOspfClient_t structure and mApiOspfClientType_t enums.
01h,12sep01,kc  Added INCLUDE_OSPF_SHOW_ROUTINES for command line build.
01g,11sep01,kc  Removed all show routines prototypes (after moved all show routines
                to ospf_mApi_show.c)
01f,04sep01,kc  Added prototypes for show routines.
01e,24aug01,kc  Added SET related function pointers to support the implementation of
                the ospf_mApi_processSetReq() routine.
01d,24aug01,kc  Added prototypes for MIB API compliant SET routines.
01c,21aug01,kc  Added GET related function pointers to support the implementation of
                the ospf_mApi_processGetReq() routine.
01b,16aug01,kc  Added prototypes for MIB API compliant GET routines.
01a,14aug01,kc  Initial file creation.
*/

/*
DESCRIPTION:

This file contains the definitions for constants, data structures and function prototypes
used to implement the OSPF Management Interface.

*/

#ifndef __INCospf_mib_apih
#define __INCospf_mib_apih

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "ospfCommonRowStatusLib.h"

/* initially turn off all the debug switch */
IMPORT int ospfMapiDebug;
IMPORT int ospfMapiErrorDebug;

/* mib api debug macro. */
#undef mApiOspfDbgEnabled
#ifdef mApiOspfDbgEnabled
#define mApiOspfPrintf( _x )  { if (ospfMapiDebug) {printf _x;} }
#else
#define mApiOspfPrintf( _x )
#endif /* mApiOspfDbgEnabled */

#undef mApiOspfErrorEnabled
#ifdef mApiOspfErrorEnabled
#define mApiOspfError( _x )  { if (ospfMapiErrorDebug) {printf _x;} }
#else
#define mApiOspfError( _x )
#endif /* mApiOspfErrorEnabled */

/* always include mib api show routines and mib api command line utilities */
#define INCLUDE_OSPF_MIB_SHOW_ROUTINES
#define INCLUDE_OSPF_MIB_UTILITIES


/* OSPF specific MIB API application defined error */
#define MAPI_INVALID_INSTANCE       10000


/* pointer to function for MIB API GET request */
typedef STATUS (* MAPI_GET_RTN)( mApiRequest_t *pRequest, mApiReqType_t reqType );

/* pointer to function for MIB API SET request */
typedef STATUS (* MAPI_SET_RTN)( mApiRequest_t *pRequest, mApiReqType_t reqType );

/* pointer to function for processing the SET request */
typedef STATUS (* MAPI_HELPER_SET_RTN)( mApiRequest_t *pRequest, mApiReqType_t reqType );

/* pointer to function for dynamically configuring the OSPF protocol */
typedef STATUS (* MAPI_HELPER_CONFIG_RTN)( mApiRequest_t *pRequest );

/* pointer to function for processing the GET/GET NEXT request */
typedef STATUS (* MAPI_HELPER_GET_RTN)( void *pRow, mApiRequest_t *pRequest,
                                        mApiObject_t *pReqObject );

/* pointer to function for setting the next index value for the GET NEXT request */
typedef void (* MAPI_INDEX_SET_RTN)( void *pRow, mApiRequest_t *pRequest );

/* pointer to function that is used by AVL to compare two instances */
typedef int (* MAPI_AVL_COMPARE_RTN)(AVL_NODE * node, void* key);


/* system dependenet configuration parameters. These parameters determine the number
 * of concurrent instances that can be configured for each table in the OSPF-MIB.
 */
#define DEFAULT_OSPF_MAPI_AREA_MAX           8
#define DEFAULT_OSPF_MAPI_STUB_MAX           4
#define DEFAULT_OSPF_MAPI_HOST_MAX           4
#define DEFAULT_OSPF_MAPI_INTF_MAX           16
#define DEFAULT_OSPF_MAPI_VIRT_INTF_MAX      8
#define DEFAULT_OSPF_MAPI_NBR_MAX            24
#define DEFAULT_OSPF_MAPI_LSDB_MAX           512
#define DEFAULT_OSPF_MAPI_EXT_LSDB_MAX       1024
#define DEFAULT_OSPF_MAPI_REDISTRIB_MAX      24
#define DEFAULT_OSPF_MAPI_AREA_AG_MAX        12

/* SPR#86493 - changed the max number of  concurrent rows that can be configured for
 * the Type-9 Opaque LSAs, Type-10 Opaque LSAs and Type-11 Opaque LSAs
 */
#define DEFAULT_OSPF_MAPI_TYPE9_LSA_MAX      12  /* for wrnOspfLocalLsdbTable */
#define DEFAULT_OSPF_MAPI_TYPE10_LSA_MAX     32  /* for wrnOpsfLsdbTable */
#define DEFAULT_OSPF_MAPI_TYPE11_LSA_MAX     12  /* for wrnOspfExtLsdbTable */
#define DEFAULT_OSPF_MAPI_REDISTRIB_MAX      24
#define DEFAULT_OSPF_MAPI_LOCAL_RT_MAX       24
/* the following define is for unnumbered interface support in wrnOspfIfTable. Set the
 * maximum number of instances allowed to be one entry less than the maximum number of
 * instances in ospfIfTable. This is done this way because it may be possible that all
 * but one instances in the ospfIfTable are created as unnumbered interface. In that
 * case there must be at least one interface with IP Address so that those unnumbered
 * interfaces can be initialized.
 */
#define DEFAULT_OSPF_MAPI_WRN_INTF_MAX       DEFAULT_OSPF_MAPI_INTF_MAX-1

/* OSPF_REQUEST_BUFFER_CNT is the number of request buffers that will be allocated.
 * These request buffers are used by ospf to provide operational status and stastitical
 * update to the MIB API asynchronous. Although this number is arbitrary picked and
 * can be tuned according to the system needs, the number of buffers allocated must
 * be able to handle the maximum number of external link state advertisement updates
 * from ospf during the flooding.
 */
#define DEFAULT_OSPF_REQUEST_BUFFER_CNT    DEFAULT_OSPF_MAPI_EXT_LSDB_MAX

/* system dependent application callback functions */
/* The following typedefs have been moved to ospf_mib_helper_update.h. The definitions
 * for these typedefs solely remain here for clarification purposes only.
 *
 * 1) NVRAM_SAVE_FUNCPTR is the function pointer pointed to a system dependent save
 * routine for saving the given object to the nvram.
 *
 * typedef STATUS (* NVRAM_SAVE_FUNCPTR)( ulong_t *pInstance, ushort_t instanceLen,
 *                                        ulong_t oidPrefixEnum, void *pValueBuf,
 *                                        ulong_t valueLen );
 *
 * 2) NVRAM_DELETE_FUNCPTR is the function pointer pointed to a system dependent delete
 * routine for deleting a table row from the nvram.
 *
 * typedef STATUS (* NVRAM_DELETE_FUNCPTR)( ulong_t *pInstance, ushort_t instanceLen );
 *
 * 3) NVRAM_RETRIEVE_FUNCPTR is the function pointer pointed to a system dependent
 *    retrieve routine for retrieving all prevously saved OSPF configuration from nvram.
 *    The retrieve routine must invoke the MIB API for read-create objects in each ospf
 *    table with the mApiRequest_t set to MAPI_NVM_SET request type so that MIB API can
 *    repopulate its management database at system startup.
 *
 * typedef STATUS (* NVRAM_RETRIEVE_FUNCPTR)( void );
 */

/* define the OSPF RFC that is currently using */
typedef enum {
    EospfProtoType_rfc1583 = 1,  /* RFC1583 OSPFv2 */
    EospfProtoType_rfc2178,      /* RFC2178 OSPFv2 */
    EospfProtoType_rfc2328       /* RFC2328 OSPFv2 */
} ospfProtoType_t;


typedef struct mApiOspfAvlClass
{
    LIST                  ospfFreePoolList;    /* free memory list */
    AVL_NODE              *pOspfAvlRoot;       /* AVL tree root */
    int                   memReqFailedCnt;     /* memory request from pool failed */
    MAPI_AVL_COMPARE_RTN  ospfAvlCompareRtn;   /* AVL compare routine */
} mApiOspfAvlClass_t;


/***************************************************************************************
 * Management Interface Data Structure.
 */
typedef struct mApiOspfClass
{
    /* OSPF Protocol Information */
    ospfProtoType_t  ospfRfcProto;      /* version of OSPFv2 RFC that is using */
    BOOL             ospfProtoInit;     /* if true, OSPF protocol is up */
    BOOL             ospfOpaqueEnable;  /* if true, support Opaque LSA */
    BOOL             ospfDbOverflow;    /* if true, support OSPF database overflow */
    BOOL             ospfNssaEnabled;   /* if true, support NSSA */
    BOOL             ospfBackboneExist; /* if true, OSPF backbone area exists */
    BOOL             ospfBackboneActive; /* if true, one more interfaces are active */

    SEM_ID           semMapiMutex;   /* mutex semaphore */
    int              semTimeout;     /* semaphore wait time, used by ospf2Mapi task */
    BOOL             rebootRouter;   /* restart router if true, destroyed MIB API */
    BOOL             dynamicConfig;  /* true if dynamic configuration is required */
    BOOL             dynamicDelete;  /* true if dynamic deletion from nvram required */

    /* system dependent callback functions for saving/retrieving ospf configuration
     * to/from nvram
     */
    NVRAM_SAVE_FUNCPTR      nvmOspfSaveRtn;      /* to save configuration to nvram */
    NVRAM_DELETE_FUNCPTR    nvmOspfDelRtn;       /* to delete a table row from nvram */
    NVRAM_RETRIEVE_FUNCPTR  nvmOspfRetrieveRtn;  /* to retrieve config from nvram */

    /* system dependent callback functions for saving/retrieving wrn-ospf enterprise
     * mib configuration to/from nvram
     */
    NVRAM_SAVE_FUNCPTR      nvmWrnOspfSaveRtn;   /* to save configuration to nvram */
    NVRAM_DELETE_FUNCPTR    nvmWrnOspfDelRtn;    /* to delete a table row from nvram */
    NVRAM_RETRIEVE_FUNCPTR  nvmWrnOspfRetrieveRtn; /* to retrieve config from nvram */

    /* system dependent configuration parameters for standard OSPF-MIB */
    int mApiOspfMaxArea;     /* max number of instances for ospfAreaTable */
    int mApiOspfMaxStub;     /* max number of instances for ospfStubTable */
    int mApiOspfMaxLsdb;     /* max number of instances for ospfLsdbTable */
    int mApiOspfMaxHost;     /* max number of instances for ospfHostTable */
    int mApiOspfMaxIf;       /* max number of instances for ospfIfTable  */
    int mApiOspfMaxIfMetric; /* max number of instances for ospfIfMetricTable */
    int mApiOspfMaxVirtIf;   /* max number of instances for ospfVirtIfTable */
    int mApiOspfMaxNbr;      /* max number of instances for ospfNbrTable */
    int mApiOspfMaxVirtNbr;  /* max numbr of instances for ospfVirtNbrTable */
    int mApiOspfMaxExtLsdb;  /* max number of instances for ospfExtLsdbTable */
    int mApiOspfMaxAreaAg;   /* max number of instances for ospfAreaAggregateTable */

    /* current system capacities */
    int mApiOspfCurrAreaCnt;      /* current number of instances in ospfAreaTable */
    int mApiOspfCurrStubCnt;      /* current number of instances in ospfStubTable */
    int mApiOspfCurrLsdbCnt;      /* current number of instances in ospfLsdbTable */
    int mApiOspfCurrHostCnt;      /* current number of instances in ospfHostTable */
    int mApiOspfCurrIfCnt;        /* current number of instances in ospfIfTable */
    int mApiOspfCurrIfMetricCnt;  /* current number of instances in ospfIfTable */
    int mApiOspfCurrVirtIfCnt;    /* current number of isntances in ospfVirtIfTable */
    int mApiOspfCurrNbrCnt;       /* current number of instances in ospfNbrTable */
    int mApiOspfCurrVirtNbrCnt;   /* current number of instances in ospfVirtNbrTable */
    int mApiOspfCurrExtLsdbCnt;   /* current number of instances in ospfExtLsdbTable */
    int mApiOspfCurrAreaAgCnt; /* current number of instances in ospfAreaAggregateTable */

    /* interanal operational erros statistics due to max capacity reached */
    int mApiOspfAreaMaxReached;      /* max capacility for ospfAreaTable reached */
    int mApiOspfStubMaxReached;      /* max capacility for ospfStubTable reached */
    int mApiOspfLsdbMaxReached;      /* max capacility for ospfLsdbTable reached */
    int mApiOspfHostMaxReached;      /* max capacility for ospfHostTable reached */
    int mApiOspfIfMaxReached;        /* max capacility for ospfIfTable reached */
    int mApiOspfIfMetricMaxReached;  /* max capacility for ospfIfMetricTable reached */
    int mApiOspfVirtIfMaxReached;    /* max capacility for ospfVirtIfTable reached */
    int mApiOspfNbrMaxReached;       /* max capacility for ospfNbrTable reached */
    int mApiOspfVirtNbrMaxReached;   /* max capacility for ospfVirtNbrTable reached */
    int mApiOspfExtLsdbMaxReached;   /* max capacility for ospfExtLsdbTable reached */
    int mApiOspfAreaAgMaxReached;  /* max capacility for ospfAreaAggregateTable reached */

    /* internal operational error statistics due to reasons other than max capacity
     * problem (resulted in instance not being created)
     */
    int mApiOspfAreaFailedCnt;       /* number of creation failure for ospfAreaTable */
    int mApiOspfStubFailedCnt;       /* number of creation failure for ospfStubTable */
    int mApiOspfLsdbFailedCnt;       /* number of creation failure for ospfLsdbTable */
    int mApiOspfHostFailedCnt;       /* number of creation failure for ospfHostTable */
    int mApiOspfIfFailedCnt;         /* number of creation failure for ospfIfTable */
    int mApiOspfIfMetricFailedCnt;  /* number of creation failure for ospfIfMetricTable */
    int mApiOspfVirtIfFailedCnt;     /* number of creation failure for ospfVirtIfTable */
    int mApiOspfNbrFailedCnt;        /* number of creation failure for ospfNbrTable */
    int mApiOspfVirtNbrFailedCnt;    /* number of creation failure for ospfVirtNbrTable */
    int mApiOspfExtLsdbFailedCnt;    /* number of creation failure for ospfExtLsdbTable */
    int mApiOspfAreaAgFailedCnt;  /* num of creation failure for ospfAreaAggregateTable */

    /* OSPF-MIB data strctures */
    void *pMapiOspfGenGroup;   /* ospf general group scalar objects */

    mApiOspfAvlClass_t  ospfAreaAvl;     /* AVL tree for ospfAreaTable */
    mApiOspfAvlClass_t  ospfStubAvl;     /* AVL tree for ospfStubTable */
    mApiOspfAvlClass_t  ospfLsdbAvl;     /* AVL tree for ospfLsdbTable */
    mApiOspfAvlClass_t  ospfHostAvl;     /* AVL tree for ospfHostTable */
    mApiOspfAvlClass_t  ospfIfAvl;       /* AVL tree for ospfIfTable */
    mApiOspfAvlClass_t  ospfIfMetricAvl; /* AVL tree for ospfIfMetric */
    mApiOspfAvlClass_t  ospfVirtIfAvl;   /* AVL tree for ospfVirtIfTable */
    mApiOspfAvlClass_t  ospfNbrAvl;      /* AVL tree for ospfNbrTable */
    mApiOspfAvlClass_t  ospfVirtNbrAvl;  /* AVL tree for ospfVirtNbrTable */
    mApiOspfAvlClass_t  ospfExtLsdbAvl;  /* AVL tree for ospfExtLsdbTable */
    mApiOspfAvlClass_t  ospfAreaAgAvl;   /* AVL tree for ospfAreaAggregateTable */


    /* rowStatus library handlers */
    rsParams_t   *pMapiOspfAreaRs;       /* ospfAreaTable Row Status handler */
    rsParams_t   *pMapiOspfStubRs;       /* ospfStubTable Row Status handler */
    rsParams_t   *pMapiOspfHostRs;       /* ospfHostTable Row Status handler */
    rsParams_t   *pMapiOspfIfRs;         /* ospfIfTable Row Status handler */
    rsParams_t   *pMapiOspfIfmRs;        /* ospfIfMetricTable Row Status handler */
    rsParams_t   *pMapiOspfVirtIfRs;     /* ospfVirtIfTable Row Status handler */
    rsParams_t   *pMapiOspfNbrRs;        /* ospfNbrTable Row Status handler */
    rsParams_t   *pMapiOspfAreaAgRs;     /* ospfAreaAggregateTable Row Status handler */

    ulong_t       currObjInProcess;       /* current varbind to process */
    mApiObject_t  *pMapiCurrObj;          /* current varbind to process */
    mApiReqType_t currMapiReqType;        /* current mApi request type to process */

    /* system dependent configuration parameters for WRN OSPF Enterprise MIB */
    int mApiWrnOspfMaxArea;       /* max number of instances for wrnOspfAreaTable */
    int mApiWrnOspfMaxLsdb;       /* max number of instances for wrnOspfLsdbTable */
    int mApiWrnOspfMaxLocalLsdb;  /* max number of instances for wrnOspfLocalLsdbTable */
    int mApiWrnOspfMaxExtLsdb;    /* max number of instances for wrnOspfExtLsdbTable */
    int mApiWrnOspfMaxIf;         /* max number of instances for wrnOspfIfTable */
    int mApiWrnOspfMaxRedistrib;  /* max num of instances for wrnOspfRedistributionTable */

    int mApiWrnOspfMaxOspf2IpRoute;    /* max number of instance for wrnOspfToIpRouteTable */
    /* current system capacities */
    int mApiWrnOspfCurrAreaCnt;      /* curr. num of instances in wrnOspfAreaTable */
    int mApiWrnOspfCurrLsdbCnt;      /* curr. num of instances in wrnOspfLsdbTable */
    int mApiWrnOspfCurrLocalLsdbCnt; /* curr. num of instances in wrnOspfLocalLsdbTable */
    int mApiWrnOspfCurrExtLsdbCnt;   /* cur. num of instances in wrnOspfExtLsdbTable */
    int mApiWrnOspfCurrIfCnt;        /* curr. number of instances in wrnOspfIfTable */
    int mApiWrnOspfCurrRedistribCnt; /* curr num of instances in wrnOspfRedistributionTable */

    int mApiWrnOspfCurrOspf2IpRouteCnt;    /* curr num of instance in wrnOspfToIpRouteTable */
    /* interanl operational erros statistics due to max capacity reached */
    int mApiWrnOspfAreaMaxReached;      /* max for wrnOspfAreaTable reached */
    int mApiWrnOspfLsdbMaxReached;      /* max for wrnOspfLsdbTable reached */
    int mApiWrnOspfLocalLsdbMaxReached; /* max for wrnOspfLocalLsdbTable reached */
    int mApiWrnOspfExtLsdMaxReached;    /* max for wrnOspfExtTable reached */
    int mApiWrnOspfIfMaxReached;        /* max for wrnOspfIfTable reached */

    int mApiWrnOspfRedistribMaxReached; /* max for wrnOspfRedistributionTable reached */
    int mApiWrnOspf2IpRouteMaxReached;    /* max for wrnOspfToIpRouteTable reached */
    /* internal operational error statistics due to reasons other than max capacity
     * problem (resulted in instance not being created)
     */
    int mApiWrnOspfAreaFailedCnt;    /* num of failure for wrnOspfAreaTable */
    int mApiWrnOspfLsdbFailedCnt;    /* num of failure for wrnOspfLsdbTable */
    int mApiWrnOspfLocalLsdbFailedCnt; /* num of failure for wrnOspfLocalLsdbTable */
    int mApiWrnOspfExtLsdbFailedCnt;   /* number of failure for wrnOspfExtLsdbTable */
    int mApiWrnOspfIfFailedCnt;        /* number of failure for wrnOspfIfTable */
    int mApiWrnOspfRedistribFailedCnt; /* num of failure for wrnOspfRedistributionTable */
    int mApiWrnOspf2IpRouteFailedCnt;    /* num of failure for wrnOspfToIpRouteTable */

    void *pMapiWrnOspfGenGroup;   /* wrn ospf general group scalar objects */

    mApiOspfAvlClass_t  wrnOspfAreaAvl;     /* AVL tree for ospfAreaTable */
    mApiOspfAvlClass_t  wrnOspfLsdbAvl;     /* AVL tree for ospfLsdbTable */
    mApiOspfAvlClass_t  wrnOspfLocalLsdbAvl; /* AVL tree for wrnOspfLocLsdbTable */
    mApiOspfAvlClass_t  wrnOspfExtLsdbAvl; /* AVL tree for wrnOspfExtLsdbTable */
    mApiOspfAvlClass_t  wrnOspfIfAvl;        /* AVL tree for wrnOspfIfTable */

    mApiOspfAvlClass_t  wrnOspfRedistribAvl;   /* AVL tree for wrnOspfRedistributionTable */
    mApiOspfAvlClass_t  wrnOspf2IpRouteAvl;   /* AVL tree for wrnOspfToIpRouteTable */
    /* rowStatus library handlers for wrn-ospf enterprise mib */
    rsParams_t   *pMapiWrnOspfIfRs;         /* wrnOspfIfTable Row Status handler */
    rsParams_t   *pMapiWrnOspfRedistribRs;  /* wrnOspfRedistributionTable Row Status handler */
    rsParams_t   *pMapiWrnOspfLocalIfRs;  /* wrnOspfLocalIfTable Row Status handler */
    rsParams_t   *pMapiWrnOspf2IpRouteRs;    /* wrnOspfToIpRouteTable Row Status handler */

    /* number of active area. An area is said to be active if there is at least one
     * operational interfaces belonging to the area. If the value is greater than
     * one, the router is an area border router
     */
    ushort_t  numActiveArea;

    /* SNMP TEST/COMMIT/UNDO/COMPLETE counters, for state transition monitoring */
    int mApiTestReqCnt;          /* number of SNMP TEST requests processed */
    int mApiCommitReqCnt;        /* number of SNMP COMMIT requests processed */
    int mApiUndoReqCnt;          /* number of SNMP UNDO requests processed */
    int mApiCompleteReqCnt;      /* number of SNMP COMMIT succeed, for debugging */
    int mApiBogusReqCnt;         /* number of incorrect SNMP requests, for debugging */
    int mApiTestReqFailedCnt;    /* number of SNMP TEST failed, for debugging */
    int mApiCommitReqFailedCnt;  /* number of SNMP COMMIT failed, for debugging */
    int mApiNvmSetReqFailedCnt;  /* number of MAPI NVM SET failed, for debugging */
    int mApiGetReqFailedCnt;     /* number of SNMP GET failed, for debugging */
    int mApiGetNextReqFailedCnt; /* number of SNMP GET_NEXT failed, for debugging */
    int mApiDynConfigReqFailedCnt; /* number of dynamic reconfig failed, for debugging */

} mApiOspfClass_t;

/* global variables */
mApiOspfClass_t *thisMapi;           /* the mib api object */

/***************************************************************************************
 * Management Interface Public Macro
 */
#define OSPF_MIB_BUFLEN_CHECK(pRequest, pObject, actualLen) \
       if ( ((pObject)->valueLen < actualLen) || \
            ((pObject)->pValueBuf == NULL) ) \
       { \
         (pObject)->valueLen = actualLen; \
         (pObject)->exception = MAPI_BUFFER_TOO_SHORT; \
         (pRequest)->errorObjIndex = (ushort_t)(pObject)->oidPrefixEnum; \
         (pRequest)->error = (int)0; \
         return OK; \
       }


#define OSPF_MIB_ERROR_SET(pRequest, mApiError, oidPrefixEnum) \
       (pRequest)->error = (int)mApiError; \
       (pRequest)->errorObjIndex = (ushort_t)oidPrefixEnum;

#define OSPF_MIB_EXCEPTION_SET( pObject, mApiException ) \
       (pObject)->exception = mApiException;


/****************************************************************************************
* Management Interface Public Function Prototypes
*/

/* OSPF MIB API init/destroy routines */
IMPORT STATUS ospfMapiInit( NVRAM_SAVE_FUNCPTR nvmOspfSaveRtn,
                            NVRAM_DELETE_FUNCPTR nvmOspfDelRtn,
                            NVRAM_RETRIEVE_FUNCPTR nvmOspfRetrieveRtn,
                            NVRAM_SAVE_FUNCPTR nvmWrnOspfSaveRtn,
                            NVRAM_DELETE_FUNCPTR nvmWrnOspfDelRtn,
                            NVRAM_RETRIEVE_FUNCPTR nvmWrnOspfRetrieveRtn
                          );

IMPORT STATUS ospfMapiDestroy( void );
IMPORT BOOL ospfMapiIsInited( void );

/* prototype for retrieving an instance of columnar object from the OSPF-MIB */

/* ospfGeneralGroup GET/GET_NEXT and TEST/COMMIT routines */
IMPORT STATUS ospfMapiGetGenGroup(mApiRequest_t *pRequest, mApiReqType_t reqType);
IMPORT STATUS ospfMapiSetGenGroup(mApiRequest_t *pRequest, mApiReqType_t reqType);

/* ospfAreaTable GET/GET_NEXT and TEST/COMMIT routines */
IMPORT STATUS ospfMapiGetArea(mApiRequest_t *pRequest, mApiReqType_t reqType);
IMPORT STATUS ospfMapiSetArea(mApiRequest_t *pRequest, mApiReqType_t reqType);

/* ospfStubTable GET/GET_NEXT and TEST/COMMIT routines */
IMPORT STATUS ospfMapiGetStub( mApiRequest_t *pRequest, mApiReqType_t reqType );
IMPORT STATUS ospfMapiSetStub( mApiRequest_t *pRequest, mApiReqType_t reqType );

/* ospfLsdbTable GET/GET_NEXT routine */
IMPORT STATUS ospfMapiGetLsdb(mApiRequest_t *pRequest, mApiReqType_t reqType);

/* ospfHostTable GET/GET_NEXT and TEST/COMMIT routines */
IMPORT STATUS ospfMapiGetHost( mApiRequest_t *pRequest, mApiReqType_t reqType );
IMPORT STATUS ospfMapiSetHost( mApiRequest_t *pRequest, mApiReqType_t reqType );

/* ospfIfTable GET/GET_NEXT and TEST/COMMIT routines */
IMPORT STATUS ospfMapiGetIf(mApiRequest_t *pRequest, mApiReqType_t reqType);
IMPORT STATUS ospfMapiSetIf( mApiRequest_t *pRequest, mApiReqType_t reqType );

/* ospfIfMetricTable GET/GET_NEXT and TEST/COMMIT routines */
IMPORT STATUS ospfMapiGetIfMetric( mApiRequest_t *pRequest, mApiReqType_t reqType );
IMPORT STATUS ospfMapiSetIfMetric( mApiRequest_t *pRequest, mApiReqType_t reqType );

/* ospfVirtIfTable GET/GET_NEXT and TEST/COMMIT routines */
IMPORT STATUS ospfMapiGetVirtIf(mApiRequest_t *pRequest, mApiReqType_t reqType);
IMPORT STATUS ospfMapiSetVirtIf( mApiRequest_t *pRequest, mApiReqType_t reqType );

/* ospfNbrTable GET/GET_NEXT and TEST/COMMIT routines */
IMPORT STATUS ospfMapiGetNbr(mApiRequest_t *pRequest, mApiReqType_t reqType);
IMPORT STATUS ospfMapiSetNbr( mApiRequest_t *pRequest, mApiReqType_t reqType );

/* ospfVirtNbrTable GET/GET_NEXT routine */
IMPORT STATUS ospfMapiGetVirtNbr( mApiRequest_t *pRequest, mApiReqType_t reqType );

/* ospfExtLsdbTable GET/GET_NEXT routine */
IMPORT STATUS ospfMapiGetExtLsdb(mApiRequest_t *pRequest, mApiReqType_t reqType);

/* ospfAreaAggregateTable GET/GET_NEXT and TEST/COMMIT routines */
IMPORT STATUS ospfMapiGetAreaAggregate( mApiRequest_t *pRequest, mApiReqType_t reqType);
IMPORT STATUS ospfMapiSetAreaAggregate( mApiRequest_t *pRequest, mApiReqType_t reqType);

/* wrnOspfGeneralGroup GET/GET_NEXT and TEST/COMMIT routines */
IMPORT STATUS ospfMapiGetWrnGenGroup( mApiRequest_t *pRequest, mApiReqType_t reqType );
IMPORT STATUS ospfMapiSetWrnGenGroup( mApiRequest_t *pRequest, mApiReqType_t reqType );

/* wrnOspfAreaTable GET/GET_NEXT routine */
IMPORT STATUS ospfMapiGetWrnArea( mApiRequest_t *pRequest, mApiReqType_t reqType );

/* wrnOspfLsdbTable GET/GET_NEXT routine */
IMPORT STATUS ospfMapiGetWrnLsdb(mApiRequest_t *pRequest, mApiReqType_t reqType);

/* wrnOspfLocalLsdbTable GET/GET_NEXT routine */
IMPORT STATUS ospfMapiGetWrnLocalLsdb( mApiRequest_t *pRequest, mApiReqType_t reqType );

/* wrnOspfExtLsdbTable GET/GET_NEXT routine */
IMPORT STATUS ospfMapiGetWrnExtLsdb( mApiRequest_t *pRequest, mApiReqType_t reqType );

/* wrnOspfIFTable GET/GET_NEXT and TEST/COMMIT routines */
IMPORT STATUS ospfMapiGetWrnIf( mApiRequest_t *pRequest, mApiReqType_t reqType );
IMPORT STATUS ospfMapiSetWrnIf( mApiRequest_t *pRequest, mApiReqType_t reqType );

/* wrnOspfRedistributionTable GET/GET_NEXT and TEST/COMMIT routines */
IMPORT STATUS ospfMapiGetWrnRedistrib( mApiRequest_t *pRequest, mApiReqType_t reqType );
IMPORT STATUS ospfMapiSetWrnRedistrib( mApiRequest_t *pRequest, mApiReqType_t reqType );

/* wrnOspfLocalIfTable GET/GET_NEXT and TEST/COMMIT routines */
IMPORT STATUS ospfMapiGetWrnLocalIf( mApiRequest_t *pRequest, mApiReqType_t reqType );
IMPORT STATUS ospfMapiSetWrnLocalIf( mApiRequest_t *pRequest, mApiReqType_t reqType );
IMPORT STATUS ospfMapiGetWrnToIpRoute( mApiRequest_t *pRequest, mApiReqType_t reqType );
IMPORT STATUS ospfMapiSetWrnToIpRoute( mApiRequest_t *pRequest, mApiReqType_t reqType );
/* misc conversions routines */
IMPORT STATUS ospf_rs2mApi_errorGet( void *pObjInfo, int rsError );
IMPORT STATUS ospf_rs2mApi_reqTypeGet( int rsRequest, mApiReqType_t *mApiReqType);
IMPORT void ospf_mApi_ipAddr2OctetString( ulong_t ipAddr, uchar_t *octetString );
IMPORT ulong_t ospf_mApi_octetString2IpAddr( uchar_t *octetString );
IMPORT void ospf_mApi_ipAddr2IpIndex( ulong_t ulIpAddr, ulong_t *pIndex );
IMPORT ulong_t ospf_mApi_ipIndex2IpAddr( ulong_t *pIndex );

/* prototype for OSPF MIB API helper wrapper routine to handle the SET operation */
IMPORT STATUS ospfMapiSetHelper();

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __INCospf_mib_apih */
