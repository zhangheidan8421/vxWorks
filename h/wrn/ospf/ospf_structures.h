/* ospf_structures.h - ospf structures*/

/* Copyright 2000-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
03l,07jan05,xli porting external route redistribution feature from ospf2.3.1
03o,29nov04,tlu OSPF stack decoupling
03n,28oct04,dsk trace B0442
03m,12may04,ram remove usless field, ospf.sptr_lsdb_print_queue
03m,30apr04,mwv SPR#91272 replace inject_default_route_if_stub_area with
                the area_summary flag for stub areas
03l,07apr04,ram OSPF v2/v3 coexistance compile
03k,23mar04,zhr Added ls_database_freeme for the build issue.
03j,12mar04,mwv SPR#94408: modify route redistribuition API to support
                route tag
03i,12mar04,ram SPR#94944 OSPF performance modifications
03h,29jan04,ram NBMA, PTMP, & Unnumbered modifications
04f,16dec03,agi propogated weighted routes fixes
04e,14dec03,agi fixed compile error
04d,08dec03,agi merged fixes from OSPF 2.1 patch branch
04c,31oct03,kc  Fixed SPR#91541 - added new OSPF_ROUTE_POLICY_PAIRS structure defs.
                Also added route_policy_list OSPF_ROUTE_POLICY_PAIRS linked list to
                the OSPF_CLASS.
04b,29sep03,agi removed #include "ospf_container.h"
04a,27sep03,agi ported to Accordion stack
03i,22oct03,mwv SPR#91511 added ls_database_freeme_age to calculate ls age
                beyond the maxage.  This age is use for the cleanup
                interval.
                Removed ls_database_pending_delete boolean.
03h,03jul03,ram Backed out SPR#88600 workaround
03g,11jun03,ram SPR#88965 Separate route table and LSDB hash parameters
03f,10jun03,dsk SPR 88600 fix for stuck in EXCHANGE_START (timer added
                to restart if stuck in exchange start state too long)
03e,06jun03,kkz Unpacked HELLO_HEADER structure (makes no difference - struct is naturally
                packed) and OSPF_ROUTER_LINK_ADVERTISEMENT_HEADER
03d,26may03,dsk SPR#88619 Fix early LSA retransmission
03c,26may03,agi Removed RWOS-specific references
03b,26may03,dsk SPR 88600 fix for stuck in EXCHANGE_START (timer added
                to restart if stuck in exchange start state too long)
03a,23may03,agi Renamed OSPF watchdog, semaphores
02z,22may03,dsk SPR#88619 Fix early LSA retransmission
02y,09may03,agi Added OSPF tasks structures
02y,08may03,asr Changes to make OSPF virtual stack compatible
02x,22apr03,ram SPR#76812 Modifications for OSPF performance enhancements
02w,29jan03,mwv SPR 85893 added member ls_database_pending_delete to OSPF_LS_DATABASE_ENTRY
02v,28jan03,ram SPR 85050 Added support for external route redistribution based on
                OSPF external metric values
02u,06jan03,ram SPR 85432 Changes to allow more OSPF external route processing
02t,09dec02,ram SPR 83418 Added support for OSPF metric type and value configuration
02s,24dec02,dsk SPR#75194, ANVL 27.15 - added sptr_old_routing_table
                pointer to the ospf datastructure
02s,20feb03,kc  Fixed SPR#86319 - rename inject_default_route_if_stub_area to
                inject_summary_lsa_into_stub_area in OSPF_AREA_ENTRY and
                OSPF_AREA_CONFIGURATION_CLASS structures.
02q,09dec02,hme Fixed SPR#75796 ANVL 37.1
02p,19nov02,mwv Merge TMS code SPR 84284
02o,05aug02,jkw Fix TSR 288030
02n,28jun02,kc  Fixed SPR 78381 - Added inject_default_route_if_stub_area to
                OSPF_AREA_ENTRY structure.
02m,24may02,kc  Added ifnet_index and ifnet_flags to OSPF_INTERFACE structure.
02l,23apr02,kc  Added number_of_stub_areas and number_of_nssa_areas counters
                to OSPF_CLASS. Removed unused bufPtr array from OSPF_CLASS.
02k,13feb02,jkw Remove preproc around structures. Possible alignment problems.
02j,20dec01,jkw Removed sptr_area->sptr_interfaces structure.
02i,13dec01,kc  Removed unused mib_import_as_external from OSPF_AREA_ENTRY
                structure.
02h,13nov01,jkw Change number of address ranges to ULONG
02g,25oct01,kc  Added area_id to OSPF_ADDRESS_RANGE_LIST_ENTRY.
02f,13oct01,kc  Dynamic configuration changes.
02e,29sep01,kc  Added interface_index in OSPF_INTERFACE structure.
02d,29sep01,kc  Defined separate M_BLK_ID for OSPF_ADDR_ALLSPF and
                OSPF_ADDR_ALLDR in OSPF_INTERFACE structure.
02c,22sep01,kc  Made sptr_interface linked list visible in OSPF_CLASS.
02b,22aug01,jkw Added opaque lsa structure changes.
02a,13aug01,aos Added new  tid member to OSPF class
01z,23jul01,jkw Added in changes for new UNION_OSPF_LS_ID structure
01y,20jun01,jkw Added unnumbered link support
01x,21may01,jkw Updated for point to point
01w,11may01,aos Added new ls_database_last_originated_time_stamp member to
                OSPF_LS_DATABASE_ENTRY
01v,26sep00,res Added WindRiver CopyRight
01u,25sep00,res RFC-1587 implementation for OSPF NSSA Option, also tested
                against ANVL.
01t,20jul00,res Unix compatibility related changes.
01s,06jul00,res Removed unnecessary header files and defines.
01r,04apr00,res Added some MIB support (Read only). Passed all important
                ANVL OSPF tests.
01q,23dec99,res Compatibility with VxWorks-IP and VxWorks RTM-interface
01p,19may99,jac redefined OSPF_PATRICIA_32_BITS_KEY_MAX_BIT_POSITION as per
                fixes in patricia
01o,22jan99,jac unpacked structs that shouldnt be packed
01n,22jan99,jac removed pack on three structures
01m,28dec98,jac Compiled and added some comments
01l,03dec98,jac Structure unpacking
01k,13nov98,jac Changes related to introducing queuing in OSPF to RTM interface
                and bug fix on the external route additions path (to RTM)
01j,11nov98,jac Config changes, linted and big endian changes
01i,30oct98,jac Incorporate changes for compilation on Vxworks
01h,23aug98,jac ANVL tested OSPF with PATRICIA tree route table and no recursion
01g,10aug98,jac PATRICIA Route Table Based OSPF Code Base
01f,08jul98,jac Patricia RT table related changes - need to be tested
01e,04jun98,jac Integration with RTM and BGP
01d,10jul97,cin Pre-release v1.52b
01c,10feb97,cin Release Version 1.52
01b,22oct97,cin Release Version 1.50
01a,05jun96,cin First Beta Release
*/


#if !defined (_OSPF_STRUCTURES_H_)
#define _OSPF_STRUCTURES_H_

/* includes */
#include        <inetLib.h>
#include        <net/if.h>
#include        <sys/socket.h>
#include        <net/socketvar.h>

enum PROTOCOL_STACK_TYPE
{
    IP_PROTOCOL_STACK,
    IPX_PROTOCOL_STACK,
    SPANNING_TREE_STACK,
    SOURCE_ROUTING_STACK,
    NLSP_PROTOCOL_STACK,
    DLSW_PROTOCOL_STACK,
    OSPF_PROTOCOL_STACK,
    APPLETALK_PROTOCOL_STACK,
    NETBIOS_PROTOCOL_STACK,
    VINES_PROTOCOL_STACK,
    CONSOLE_PROTOCOL_STACK,
    L2TP_PROTOCOL_STACK,
    RTM_PROTOCOL_STACK,
    PPTP_DATA_TUNNEL,
    PPP_OVER_ETHERNET_PROTOCOL_STACK,
    VPN_INTERFACE_VIRTUAL_PROTOCOL_STACK
};

enum PROTOCOL_CONTROL_OPERATION
{
    OPEN_PROTOCOL_STACK,
    CLOSE_PROTOCOL_STACK,
    GET_IP_ADDRESS_FOR_PORT,

    IS_PROTOCOL_STACK_ENABLED = 0x0100,
    GET_NUMBER_OF_PROTOCOL_STACKS_VIRTUAL_PORTS,
    GET_PROTOCOL_STACK_TYPE,
    GET_PROTOCOL_STACK_PROTOCOL_ID,
    GET_PROTOCOL_STACK_SSAP,
    GET_PROTOCOL_STACK_DSAP,
    GET_PROTOCOL_STACK_VIRTUAL_PORT_PACKET_TYPE,
    GET_PROTOCOL_STACK_REAL_PORT_NUMBER_USING_VIRTUAL_PORT_NUMBER,

    INITIALIZE_SOCKET_PROTOCOL_STACK_INTERFACE,

    IS_TOKEN_RING_FRAME_SOURCE_ROUTED,
    IS_TOKEN_RING_FRAME_A_MAC,
    GET_LENGTH_OF_TOKEN_RING_RIF,
    CLEAR_SOURCE_ROUTED_BIT,
    MOVE_RIF_INTO_PACKET,
    IS_MAC_ADDRESS_SOURCE_ROUTED,
    REGISTER_TO_IP_FROM_UPPER_LAYER,

    INITIALIZE_STACK_FUNCTION_POINTERS,
    INITIALIZE_PROTOCOL_FUNCTION_POINTERS,
    GET_PROTOCOL_ADDRESS,
    CLEAR_CACHED_ROUTE,
    REINITIALIZE_PROTOCOL_STACK,
    PROTOCOL_STACK_ROUTERWARE_CONTROL,
    UNINITIALIZE_PROTOCOL_STACK,
    PROTOCOL_ROUTE_ADDED,
    PROTOCOL_ROUTE_DELETED,
    PROTOCOL_ROUTE_CHANGED,
    LLC_PROTOCOL_STACK,
    REGISTER_NAT_FILTER = 200,
    DEREGISTER_NAT_FILTER,
    OPEN_PROTOCOL_STACK_PORT,
    CLOSE_PROTOCOL_STACK_PORT,
    ADD_IP_ADDRESS_AND_NETWORK_MASK_FOR_PORT,
    ADD_REMOTE_IP_ADDRESS_FOR_PORT,

    OPEN_VIRTUAL_PORT,
    CLOSE_VIRTUAL_PORT,
    REINITIALIZE_VIRTUAL_PORT,
    UNINITIALIZE_VIRTUAL_PORT
};

enum LSL_PACKET_TYPE
{
    RAW_8023,
    ETHERNET_TYPE_II,
    LLC_PACKET,
    SNAP_PACKET,
    WAN_PACKET,
    SAME_TYPE_AS_RX_PACKET_,

    __ENUM_USHORT_FILLER_ = 0x1000
};


typedef struct OSPF_RETURN_TYPE
{
    enum OSPF_PACKET_STATE  packet_state;
    BYTE_ENUM (BOOLEAN)     exit_routine;
} OSPF_RETURN_TYPE;

/* used for pointer manipulation when inserting and removing nodes in a list */
typedef struct OSPF_GENERIC_NODE
{
    struct  OSPF_GENERIC_NODE   *sptr_forward_link;
    struct  OSPF_GENERIC_NODE   *sptr_backward_link;
} OSPF_GENERIC_NODE;

/* used for pointer manipulation when inserting and removing nodes in a list */
typedef struct OSPF_STACK_NODE
{
    struct  OSPF_STACK_NODE *sptr_forward_link;
    struct  OSPF_STACK_NODE *sptr_backward_link;
    void *vptr_data;
} OSPF_STACK_NODE;

#if (_BYTE_ORDER == _LITTLE_ENDIAN )
typedef _struct OSPF_OPTIONS
{
    BIT_FIELD (enum,BOOLEAN)    tos:1;
    BIT_FIELD (enum,BOOLEAN)    externals:1;
    BIT_FIELD (enum,BOOLEAN)    multicast:1;
    BIT_FIELD (enum,BOOLEAN)    nssa:1;
    BIT_FIELD (enum,BOOLEAN)    dont_propagate:1;
    /*opaque lsa new opaque option bit jkw*/
    BIT_FIELD (enum,BOOLEAN)    demand_circuit:1;
    BIT_FIELD (enum,BOOLEAN)    opaque:1;
    BIT_FIELD (enum,BOOLEAN)    not_used:1;
} _pack OSPF_OPTIONS;
#else /* _BYTE_ORDER == _BIG_ENDIAN */
typedef _struct OSPF_OPTIONS
{
    /*opaque lsa new opaque option bit jkw*/
    BIT_FIELD (enum,BOOLEAN)    not_used:1;
    BIT_FIELD (enum,BOOLEAN)    opaque:1;
    BIT_FIELD (enum,BOOLEAN)    demand_circuit:1;
    BIT_FIELD (enum,BOOLEAN)    dont_propagate:1;
    BIT_FIELD (enum,BOOLEAN)    nssa:1;
    BIT_FIELD (enum,BOOLEAN)    multicast:1;
    BIT_FIELD (enum,BOOLEAN)    externals:1;
    BIT_FIELD (enum,BOOLEAN)    tos:1;
} _pack OSPF_OPTIONS;
#endif /* _BYTE_ORDER == _LITTLE_ENDIAN */

typedef _union  UNION_OSPF_OPTIONS
{
    OSPF_OPTIONS  _bit;
    BYTE          _byte;
} _pack UNION_OSPF_OPTIONS;

#if (_BYTE_ORDER == _LITTLE_ENDIAN )
typedef _struct OSPF_FLAGS
{
    BIT_FIELD (enum,BOOLEAN)    master:1;
    BIT_FIELD (enum,BOOLEAN)    more:1;
    BIT_FIELD (enum,BOOLEAN)    initialize:1;
    BIT_FIELD (enum,BOOLEAN)    not_used:5;
} _pack OSPF_FLAGS;
#else /* _BYTE_ORDER == _BIG_ENDIAN */
typedef _struct OSPF_FLAGS
{
    BIT_FIELD (enum,BOOLEAN)    not_used:5;
    BIT_FIELD (enum,BOOLEAN)    initialize:1;
    BIT_FIELD (enum,BOOLEAN)    more:1;
    BIT_FIELD (enum,BOOLEAN)    master:1;
} _pack OSPF_FLAGS;
#endif /* _BYTE_ORDER == _LITTLE_ENDIAN */

typedef _union  UNION_OSPF_FLAGS
{
    OSPF_FLAGS    _bit;
    BYTE          _byte;
} _pack UNION_OSPF_FLAGS;

typedef _struct OSPF_MD5_AUTHENTICATION
{
    USHORT  offset;
    BYTE    key_ID;
    BYTE    authentication_data_length;
    ULONG   cryptographic_sequence_number;
} _pack OSPF_MD5_AUTHENTICATION;

typedef _union  UNION_OSPF_AUTHENTICATION_FIELD
{
    BYTE  key_or_plain_text_passwd[OSPF_AUTHENTICATION_SIMPLE_SIZE];
    OSPF_MD5_AUTHENTICATION md5;
} _pack UNION_OSPF_AUTHENTICATION_FIELD;

/*opaque lsa new opaque lsa header jkw*/
typedef _struct OSPF_LS_ID
{
    BYTE    opaque_type;
    BYTE    opaque_reserved;
    USHORT  opaque_instance;
} _pack OSPF_LS_ID;

typedef _union UNION_OSPF_LS_ID
{
    OSPF_LS_ID      opaque_lsid;
    ULONG           lsid;
} _pack UNION_OSPF_LS_ID;

typedef _struct OSPF_LS_OPAQUE_HEADER
{
    USHORT                    age;
    UNION_OSPF_OPTIONS        options;
    BYTE_ENUM (OSPF_LS_TYPE)  type;   /* Type of advertisement */
    UNION_OSPF_LS_ID          id;     /* Link State Id */
    ULONG                     advertising_router;
    seq_t                     sequence_number;
    USHORT                    checksum;
    USHORT                    length;  /* length of advertisement*/
} OSPF_LS_OPAQUE_HEADER;

typedef _struct OSPF_LS_HEADER
{
    USHORT                    age;
    UNION_OSPF_OPTIONS        options;
    BYTE_ENUM (OSPF_LS_TYPE)  type;    /* Type of advertisement */
    ULONG                     id;      /* Link State Id */
    ULONG                     advertising_router;
    seq_t                     sequence_number;
    USHORT                    checksum;
    USHORT                    length;   /* length of advertisement*/
} OSPF_LS_HEADER;

typedef _struct OSPF_HELLO_HEADER
{
    ULONG               network_mask;    /* network mask associated with this interface */
    USHORT              hello_interval;  /* number of seconds between this router's Hello
                                            packets */
    UNION_OSPF_OPTIONS  options;   /* optional capabilities supported by this router */
    BYTE                router_priority;   /* if set to 0, this router is not eligible to
                                              become designated router */
    ULONG               router_dead_interval;   /* number of seconds before declaring a
                                                   silent router down */
    ULONG               designated_router;   /* identity of the designated router for
                                                this network */
    ULONG               backup_designated_router;  /* identity of the backup designated
                                                      router for this network */
    ULONG               neighbor;    /* IDs of each router from whom valid Hello packets
                                        have been seen recently on this network */
} OSPF_HELLO_HEADER;

typedef _struct OSPF_DATABASE_HEADER
{
    /* RFC 2178 G.9 */
    USHORT              interface_mtu;
    UNION_OSPF_OPTIONS  options;
    UNION_OSPF_FLAGS    flags;
    ULONG               sequence;
    OSPF_LS_HEADER      link_state_advertisement_header;
} _pack OSPF_DATABASE_HEADER;

typedef _struct OSPF_LS_REQUESTED_ADVERTISEMENT
{
    BYTE    reserved_bytes[3];
    BYTE    type;      /* Type of advertisement */
    ULONG   id;        /* Link State Id */
    ULONG   advertising_router;
} _pack OSPF_LS_REQUESTED_ADVERTISEMENT;

typedef _struct OSPF_LS_REQUEST_HEADER
{
    OSPF_LS_REQUESTED_ADVERTISEMENT requested_advertisement;
} _pack OSPF_LS_REQUEST_HEADER;

#if (_BYTE_ORDER == _LITTLE_ENDIAN )
typedef _struct OSPF_ROUTER_LINK_BIT_OPTIONS
{
    BIT_FIELD (enum,BOOLEAN)   bit_B:1;
    BIT_FIELD (enum,BOOLEAN)   bit_E:1;
    BIT_FIELD (enum,BOOLEAN)   bit_V:1;
    BIT_FIELD (enum,BOOLEAN)   not_used:5;
} _pack OSPF_ROUTER_LINK_BIT_OPTIONS;
#else /* _BYTE_ORDER == _BIG_ENDIAN */
typedef _struct OSPF_ROUTER_LINK_BIT_OPTIONS
{
    BIT_FIELD (enum,BOOLEAN)   not_used:5;
    BIT_FIELD (enum,BOOLEAN)   bit_V:1;
    BIT_FIELD (enum,BOOLEAN)   bit_E:1;
    BIT_FIELD (enum,BOOLEAN)   bit_B:1;
} _pack OSPF_ROUTER_LINK_BIT_OPTIONS;
#endif /* _BYTE_ORDER == _LITTLE_ENDIAN */

typedef _union  UNION_OSPF_ROUTER_LINK_BIT_OPTIONS
{
        OSPF_ROUTER_LINK_BIT_OPTIONS    _bit;
        USHORT                                  _ushort;
} _pack UNION_OSPF_ROUTER_LINK_BIT_OPTIONS;

typedef _struct OSPF_ROUTER_LINK_METRIC_PIECE
{
    USHORT  tos;
    USHORT  metric;
} _pack OSPF_ROUTER_LINK_METRIC_PIECE;

typedef _struct OSPF_ROUTER_LINK_PIECE
{
    ULONG                              link_id;
    ULONG                              link_data;
    BYTE_ENUM (OSPF_ROUTER_LINK_TYPE)  type;
    BYTE                               number_of_metrics;
    USHORT                             tos0_metric;
} _pack OSPF_ROUTER_LINK_PIECE;

typedef _struct OSPF_ROUTER_LINK_ADVERTISEMENT_HEADER
{
    OSPF_LS_HEADER                      ls_header;
    UNION_OSPF_ROUTER_LINK_BIT_OPTIONS  bit_options;
    USHORT                              number_of_links;
    OSPF_ROUTER_LINK_PIECE              link;
} OSPF_ROUTER_LINK_ADVERTISEMENT_HEADER;

typedef _struct OSPF_ROUTER_LSA_LINK
{
    _struct OSPF_ROUTER_LSA_LINK       *sptr_forward_link;
    _struct OSPF_ROUTER_LSA_LINK       *sptr_backward_link;
    OSPF_LS_HEADER                     ls_header;
    UNION_OSPF_ROUTER_LINK_BIT_OPTIONS bit_options;
    USHORT                             number_of_links;
    OSPF_ROUTER_LINK_PIECE             link;
} _pack OSPF_ROUTER_LSA_LINK;

typedef _struct OSPF_NETWORK_LINK_PIECE
{
    ULONG   link_id;
} _pack OSPF_NETWORK_LINK_PIECE;

typedef _struct OSPF_NETWORK_LINK_ADVERTISEMENT_HEADER
{
    OSPF_LS_HEADER           ls_header;
    ULONG                    network_mask;
    OSPF_NETWORK_LINK_PIECE  attached_router;
} _pack OSPF_NETWORK_LINK_ADVERTISEMENT_HEADER;

typedef _struct OSPF_NETWORK_LSA_LINK
{
    _struct OSPF_NETWORK_LSA_LINK  *sptr_forward_link;
    _struct OSPF_NETWORK_LSA_LINK  *sptr_backward_link;
    OSPF_LS_HEADER                 ls_header;
    ULONG                          network_mask;
    OSPF_NETWORK_LINK_PIECE        attached_router;
} _pack OSPF_NETWORK_LSA_LINK;


typedef _struct OSPF_SUMMARY_LINK_ADVERTISEMENT_HEADER
{
    OSPF_LS_HEADER  ls_header;
    ULONG           network_mask;
    ULONG           tos0_metric;
} _pack OSPF_SUMMARY_LINK_ADVERTISEMENT_HEADER;

typedef _struct OSPF_SUMMARY_LSA_LINK
{
    _struct OSPF_SUMMARY_LSA_LINK  *sptr_forward_link;
    _struct OSPF_SUMMARY_LSA_LINK  *sptr_backward_link;
    OSPF_LS_HEADER                 ls_header;
    ULONG                          network_mask;
    ULONG                          tos0_metric;
} _pack OSPF_SUMMARY_LSA_LINK;


typedef _struct OSPF_EXTERNAL_LINK_METRIC_PIECE
{
    USHORT  tos;
    USHORT  metric;
    ULONG   forwarding_address;
    ULONG   external_route_tag;
} _pack OSPF_EXTERNAL_LINK_METRIC_PIECE;


typedef _struct OSPF_EXTERNAL_LINK_ADVERTISEMENT_HEADER
{
    OSPF_LS_HEADER  ls_header;
    ULONG           network_mask;
    ULONG           tos0_metric;
    ULONG           tos0_forwarding_address;
    ULONG           tos0_external_route_tag;
} _pack OSPF_EXTERNAL_LINK_ADVERTISEMENT_HEADER;

typedef _struct OSPF_EXTERNAL_LSA_LINK
{
    _struct OSPF_EXTERNAL_LSA_LINK  *sptr_forward_link;
    _struct OSPF_EXTERNAL_LSA_LINK  *sptr_backward_link;
    OSPF_LS_HEADER                  ls_header;
    ULONG                           network_mask;
    ULONG                           tos0_metric;
    ULONG                           tos0_forwarding_address;
    ULONG                           tos0_external_route_tag;
} _pack OSPF_EXTERNAL_LSA_LINK;


typedef _struct OSPF_TYPE_7_LINK_ADVERTISEMENT_HEADER
{
        OSPF_LS_HEADER  ls_header;
        ULONG           network_mask;
        ULONG           tos0_metric;
        ULONG           tos0_forwarding_address;
        ULONG           tos0_external_route_tag;
} _pack OSPF_TYPE_7_LINK_ADVERTISEMENT_HEADER;

typedef _struct OSPF_TYPE_7_LSA_LINK
{
    _struct OSPF_TYPE_7_LSA_LINK  *sptr_forward_link;
    _struct OSPF_TYPE_7_LSA_LINK  *sptr_backward_link;
    OSPF_LS_HEADER                ls_header;
    ULONG                         network_mask;
    ULONG                         tos0_metric;
    ULONG                         tos0_forwarding_address;
    ULONG                         tos0_external_route_tag;
} _pack OSPF_TYPE_7_LSA_LINK;


/*opaque lsa type 9, 10, 11 lsa jkw*/
typedef _struct OSPF_OPAQUE_LSA
{
    OSPF_LS_OPAQUE_HEADER  ls_header;
    void                   *data;
} _pack OSPF_OPAQUE_LSA;

typedef _struct OSPF_TYPE_9_LINK_ADVERTISEMENT_HEADER
{
    OSPF_LS_OPAQUE_HEADER    ls_header;
    void                     *data;
    struct OSPF_INTERFACE    *opaque_interface;
} _pack OSPF_TYPE_9_LINK_ADVERTISEMENT_HEADER;

typedef _struct OSPF_TYPE_9_LSA_LINK
{
    _struct OSPF_TYPE_9_LSA_LINK   *sptr_forward_link;
    _struct OSPF_TYPE_9_LSA_LINK   *sptr_backward_link;
    OSPF_LS_OPAQUE_HEADER          ls_header;
    void                           *data;
    struct OSPF_INTERFACE          *opaque_interface;
} _pack OSPF_TYPE_9_LSA_LINK;

typedef _struct OSPF_TYPE_10_LINK_ADVERTISEMENT_HEADER
{
    OSPF_LS_OPAQUE_HEADER          ls_header;
    void                           *data;
    struct OSPF_AREA_ENTRY         *opaque_area;
} _pack OSPF_TYPE_10_LINK_ADVERTISEMENT_HEADER;

typedef _struct OSPF_TYPE_10_LSA_LINK
{
    _struct OSPF_TYPE_10_LSA_LINK  *sptr_forward_link;
    _struct OSPF_TYPE_10_LSA_LINK  *sptr_backward_link;
    OSPF_LS_OPAQUE_HEADER          ls_header;
    void                           *data;
    struct OSPF_AREA_ENTRY         *opaque_area;
} _pack OSPF_TYPE_10_LSA_LINK;

typedef _struct OSPF_TYPE_11_LINK_ADVERTISEMENT_HEADER
{
    OSPF_LS_OPAQUE_HEADER  ls_header;
    void                   *data;
} _pack OSPF_TYPE_11_LINK_ADVERTISEMENT_HEADER;

typedef _struct OSPF_TYPE_11_LSA_LINK
{
    OSPF_LS_OPAQUE_HEADER          ls_header;
    void                           *data;
    _struct OSPF_TYPE_11_LSA_LINK  *sptr_forward_link;
    _struct OSPF_TYPE_11_LSA_LINK  *sptr_backward_link;
} _pack OSPF_TYPE_11_LSA_LINK;


typedef _union  UNION_OSPF_ADVERTISEMENT_HEADER
{
    OSPF_ROUTER_LINK_ADVERTISEMENT_HEADER   router;
    OSPF_NETWORK_LINK_ADVERTISEMENT_HEADER  network;
    OSPF_SUMMARY_LINK_ADVERTISEMENT_HEADER  summary;
    OSPF_EXTERNAL_LINK_ADVERTISEMENT_HEADER external;
    OSPF_TYPE_7_LINK_ADVERTISEMENT_HEADER type_7;
    /*opaque lsa add new opaque lsa types to union jkw*/
    OSPF_TYPE_9_LINK_ADVERTISEMENT_HEADER type_9;
    OSPF_TYPE_10_LINK_ADVERTISEMENT_HEADER type_10;
    OSPF_TYPE_11_LINK_ADVERTISEMENT_HEADER type_11;
} _pack UNION_OSPF_ADVERTISEMENT_HEADER;

typedef _struct OSPF_LS_UPDATE_HEADER
{
    ULONG                             number_of_advertisements;
    UNION_OSPF_ADVERTISEMENT_HEADER   advertisement_header;
} _pack OSPF_LS_UPDATE_HEADER;

typedef _struct OSPF_LS_ACKNOWLEDGEMENT_HEADER
{
    OSPF_LS_HEADER ls_advertisement_header;
} _pack OSPF_LS_ACKNOWLEDGEMENT_HEADER;

typedef _union  UNION_OSPF_PACKET_TYPES
{
    OSPF_HELLO_HEADER               hello;
    OSPF_DATABASE_HEADER            database;
    OSPF_LS_REQUEST_HEADER          ls_request;
    OSPF_LS_UPDATE_HEADER           ls_update;
    OSPF_LS_ACKNOWLEDGEMENT_HEADER  ls_acknowledgement;
} _pack UNION_OSPF_PACKET_TYPES;

typedef _struct OSPF_HEADER     /* the standard 24 byte header for OSPF (page 178) */
{
    BYTE                                version;   /* set to 2 for this implementation */
    BYTE_ENUM(OSPF_UNION_PACKET_TYPES)  type;

    /* length of entire OSPF protocol packet in bytes, including the standard OSPF
     * header
     */
    USHORT                              length;

    /* identity of the router originating the packet */
    ULONG                               router_id;

    /* a 32 bit number identifying the area this packet belongs to */
    ULONG                               area_id;

    /* checksum of the entire contents of the packet starting with the OSPF packet header
     * but excluding the 64 bit authentication field
     */
    USHORT                              checksum;
    USHORT                              authentication_type;
    UNION_OSPF_AUTHENTICATION_FIELD     authentication_field;   /* a 64 bit field for use by the authentication scheme */
    UNION_OSPF_PACKET_TYPES             rest_of_packet;
} _pack OSPF_HEADER;


typedef _struct OSPF_PACKET
{
    struct ip                           ip_header;
    OSPF_HEADER                         header;
} _pack OSPF_PACKET;

typedef struct  OSPF_LS_DATABASE_SUMMARY
{
        struct OSPF_LS_DATABASE_SUMMARY *sptr_forward_link;
        struct OSPF_LS_DATABASE_SUMMARY *sptr_backward_link;
        OSPF_HEADER                                     *sptr_ospf_header;
        USHORT                                          size_of_packet;                                         /* length of this packet including ospf header size */
        USHORT                                          number_of_database_entries;
} OSPF_LS_DATABASE_SUMMARY;


typedef struct  OSPF_LS_REQUEST
{
    struct OSPF_LS_REQUEST      *sptr_forward_link;
    struct OSPF_LS_REQUEST      *sptr_backward_link;

    ULONG                               id;
    ULONG                               advertising_router;
    seq_t                               sequence_number;
    USHORT                              checksum;
    USHORT                              age;
} OSPF_LS_REQUEST;

typedef union UNION_OSPF_LINK_STATE_ADVERTISEMENT
{
    struct OSPF_ROUTER_LINK_ADVERTISEMENT_HEADER                *sptr_router;
    struct OSPF_NETWORK_LINK_ADVERTISEMENT_HEADER       *sptr_network;
    struct OSPF_SUMMARY_LINK_ADVERTISEMENT_HEADER       *sptr_summary;
    struct OSPF_EXTERNAL_LINK_ADVERTISEMENT_HEADER      *sptr_external;
        struct OSPF_TYPE_7_LINK_ADVERTISEMENT_HEADER    *sptr_type_7;
    /*opaque lsa structure for opaque lsas jkw*/
    struct OSPF_TYPE_9_LINK_ADVERTISEMENT_HEADER        *sptr_type_9;
    struct OSPF_TYPE_10_LINK_ADVERTISEMENT_HEADER   *sptr_type_10;
    struct OSPF_TYPE_11_LINK_ADVERTISEMENT_HEADER       *sptr_type_11;

} UNION_OSPF_LINK_STATE_ADVERTISEMENT;

/****************************/
/* Routing Table Structures */
/****************************/

typedef struct OSPF_NEXT_HOP_BLOCK
{
        struct OSPF_NEXT_HOP_BLOCK      *sptr_forward_link;
        struct OSPF_NEXT_HOP_BLOCK      *sptr_backward_link;
        ULONG                                   outgoing_router_interface;
        ULONG                                   next_hop_router;
        enum OSPF_ROUTE_STATUS route_status;

} OSPF_NEXT_HOP_BLOCK;

typedef struct OSPF_ROUTING_TABLE_ENTRY
{
        enum OSPF_ROUTE_DESTINATION_TYPE                        destination_type;
        ULONG                                                           destination_id;
        ULONG                                                           address_mask;
        BYTE_ENUM (BOOLEAN)                                     type_5_route_originated;
        UNION_OSPF_OPTIONS                                      optional_capabilities;
        UNION_SERVICE_TYPE_BIT_STRUCTURE        type_of_service;
        struct OSPF_AREA_ENTRY                                  *sptr_area;
        enum OSPF_ROUTE_PATH_TYPE                               path_type;
        ULONG                                                           path_cost;
        ULONG                                                           type2_cost;
        ULONG                                                           link_state_id;
        ULONG                                                           cached_ip_address;
        BYTE_ENUM (OSPF_LS_TYPE)                                        link_type;
        UNION_OSPF_LINK_STATE_ADVERTISEMENT             *sptr_link_state_origin;
        OSPF_NEXT_HOP_BLOCK                                             *sptr_next_hop;
        ULONG                                                           advertising_router;
        ULONG abr;
        enum OSPF_ROUTING_TABLE_UPDATE_ACTION                        action_history;

} OSPF_ROUTING_TABLE_ENTRY;

typedef struct  OSPF_ROUTING_TABLE_NODE
{
        struct OSPF_ROUTING_TABLE_NODE  *sptr_forward_link;
        struct OSPF_ROUTING_TABLE_NODE  *sptr_backward_link;
        BYTE_ENUM (BOOLEAN)                                     active_areas_discarded_entry; /* discard_entry */
        enum OSPF_ROUTE_STATUS  route_node_status;
        OSPF_ROUTING_TABLE_ENTRY                *sptr_routing_table_entry;
} OSPF_ROUTING_TABLE_NODE;

typedef union UNION_OSPF_VIRTUAL_ROUTE
{
        OSPF_ROUTING_TABLE_ENTRY        *sptr_ospf_route;
} UNION_OSPF_VIRTUAL_ROUTE;

typedef struct OSPF_LSA_TIMESTAMP_PER_INTERFACE_LIST
{
    struct OSPF_LSA_TIMESTAMP_PER_INTERFACE_LIST   *sptr_forward_link;
    struct OSPF_LSA_TIMESTAMP_PER_INTERFACE_LIST   *sptr_backward_link;
    ULONG   if_index;
    ULONG   neighbor_id;
    ULONG   timestamp;
} OSPF_LSA_TIMESTAMP_PER_INTERFACE_LIST;

typedef struct  OSPF_LS_DATABASE_ENTRY
{
        struct OSPF_LS_DATABASE_ENTRY           *sptr_forward_link;
        struct OSPF_LS_DATABASE_ENTRY           *sptr_backward_link;
        UNION_OSPF_LINK_STATE_ADVERTISEMENT     advertisement;
        UNION_OSPF_VIRTUAL_ROUTE                *vptr_route;    /* may point to a routing table entry or an
                                                                 * area boarder router */
        OSPF_ROUTING_TABLE_ENTRY                *sptr_database_autonomous_system_border_router; /* if it is ASB Router */
        struct OSPF_LS_DATABASE_ENTRY           *sptr_database_border_router;                                   /* sum or ase - ls_database of border rtr */
        struct OSPF_AREA_ENTRY                  *sptr_ls_database_area;                                                         /* for keeping count of db's in each area */
        USHORT                                  ls_database_hash;                                                               /* this db's hash */
        enum OSPF_LS_TYPE                       ls_database_type;
        OSPF_NEXT_HOP_BLOCK                     *sptr_next_hop;
        ULONG                                   ls_database_time_stamp;                                                 /* for keeping age - stamped when arrived */
        ULONG                                   ls_database_last_originated_time_stamp;         /* for sending database copy back */

        ULONG lock_time_MinLSInterval;  /*  amount of delay or loc-time required before the advertisement can be flood out of eligible interfaces */

        struct OSPF_NEIGHBOR_LIST               *sptr_ls_database_retrans;                                              /* neighbors pointing to this ls_database */
        enum OSPF_SEQUENCE_STATE                ls_database_sequence_state;
        BYTE_ENUM (BOOLEAN)                     ls_database_freeme;
        ULONG                                   route_protocol; /* Used for external route */
        ULONG                                   ls_database_freeme_age; /* age of LS entry beyond MAXAGE */
        OSPF_LSA_TIMESTAMP_PER_INTERFACE_LIST   *sptr_lsa_retransmit_timestamps_per_neighbor;
} OSPF_LS_DATABASE_ENTRY;

typedef struct OSPF_LS_DATABASE_NODE
{
        struct OSPF_LS_DATABASE_NODE    *sptr_forward_link;
        struct OSPF_LS_DATABASE_NODE    *sptr_backward_link;

        OSPF_LS_DATABASE_ENTRY                          *sptr_ls_database_entry;
        ULONG                                                   periodic_retransmit_time_counter;
        BYTE_ENUM (BOOLEAN)                                     flood;                                                                  /* true if flooding this one */
} OSPF_LS_DATABASE_NODE;

typedef struct OSPF_DATABASE_INFORMATION_STRUCTURE                                                              /* used in ospf_flood.c */
{
        BYTE_ENUM (BOOLEAN)                                     found_link_state_advertisement_in_database;
        OSPF_LS_DATABASE_ENTRY                          *sptr_database_entry;
        struct OSPF_INTERFACE                   *sptr_interface;
        struct OSPF_NEIGHBOR                                    *sptr_neighbor;
        UNION_OSPF_LINK_STATE_ADVERTISEMENT     new_advertisement;
        ULONG                                                   time_difference;
        ULONG                                                   source_address;
        ULONG                                                   destination_address;
} OSPF_DATABASE_INFORMATION_STRUCTURE;

typedef struct  OSPF_NEIGHBOR
{
        struct OSPF_NEIGHBOR         *sptr_forward_link;
        struct OSPF_NEIGHBOR         *sptr_backward_link;
        struct OSPF_INTERFACE     *sptr_interface;
        enum OSPF_NEIGHBOR_STATE  state;
        ULONG                     events;  /* Number of state changes */
        ULONG                     last_hello;     /* Time last hello was received from this neighbor */
        ULONG                     periodic_inactivity_time_counter;
        BYTE_ENUM (BOOLEAN)       inactivity_timer_enabled;
        enum OSPF_NEIGHBOR_MODE   mode;    /* Master or slave mode */
        UNION_OSPF_FLAGS          flags;   /* For passing initialize, more and master/slave bits */
        UNION_OSPF_OPTIONS        options;
        seq_t                     database_description_sequence_number;
        ULONG                     cryptographic_sequence_number;  /* Used when Cryptographic Authentication is employed */
        ULONG                     last_exchange;    /* Time last exchange was received from this neighbor - hold timer */
        ULONG                     periodic_slave_hold_time_counter;
        ULONG                     id;
        USHORT                    priority;  /* 0 means not eligible to become the Designated Router */
        ULONG                     address;

        ULONG                     designated_router;
        ULONG                     backup_designated_router;
        OSPF_LS_DATABASE_NODE     *sptr_retransmit;        /* Link state advertisements waiting for acknowledgements */
        OSPF_LS_DATABASE_SUMMARY  *sptr_database_summary; /* Database summary packets that make up area database */

        OSPF_LS_REQUEST           *sptr_ls_request[OSPF_LS_MAX]; /*SPR 81628: Should not use a hardcoded number */

        USHORT                    retransmit_queue_count;
        USHORT                    database_summary_queue_count;
        USHORT                    ls_request_queue_count;

        BYTE_ENUM (BOOLEAN)       ls_request_list_has_changed;

        ULONG                     mib_address_less_index;
        BYTE_ENUM (BOOLEAN)       mib_nbma_status;
        BYTE_ENUM (BOOLEAN)       mib_nbma_permanence;
        BYTE_ENUM (BOOLEAN)       mib_hello_suppressed;
        ULONG                     mib_area_id;
        enum BOOLEAN              opaque_capability;
} OSPF_NEIGHBOR;

typedef struct  OSPF_DESIGNATED_ROUTER_NODE
{
        ULONG                     id;
        ULONG                     address;
        enum OSPF_NEIGHBOR_STATE  state;
        USHORT                    priority; /* 0 means not eligible to become the Designated Router */
} OSPF_DESIGNATED_ROUTER_NODE;

typedef _struct OSPF_AUTHENTICATION
{
    USHORT   type;  /* Type of authentication */
    BYTE     key_or_plain_text_passwd[OSPF_AUTHENTICATION_SIZE]; /* Authentication */

} _pack OSPF_AUTHENTICATION;

typedef struct OSPF_AUTHENTICATION_KEY
{
        struct OSPF_AUTHENTICATION_KEY  *sptr_forward_link;
        struct OSPF_AUTHENTICATION_KEY  *sptr_backward_link;

        BYTE    key_ID;
        BYTE            md5_16byte_password[OSPF_AUTHENTICATION_MD5_SIZE];

        ULONG           key_start_accept;                       /* time router will start accepting packets created with given key */
        ULONG           key_start_generate;             /* time router will start using key for packet generation */
        ULONG           key_stop_generate;              /* time router will stop using key for packet generation */
        ULONG           key_stop_accept;                        /* time router will stop accepting packets created with given key */
} OSPF_AUTHENTICATION_KEY;

#if (_BYTE_ORDER == _LITTLE_ENDIAN )
typedef _struct OSPF_INTERFACE_FLAGS
{
        BIT_FIELD (enum, BOOLEAN)       network_scheduled:1;            /* When designated router: semaphore for generating link state advertisements */
        BIT_FIELD (enum, BOOLEAN)       enable:1;                                       /* Interface is enabled */
        BIT_FIELD (enum, BOOLEAN)       build_network:1;                        /* Flag to build_net_lsa */
        BIT_FIELD (enum, BOOLEAN)       neighbor_change:1;                      /* Schedule neighbor change */
        BIT_FIELD (enum, BOOLEAN)       multicast:1;                            /* Interface is multicast capable */
        BIT_FIELD (enum, BOOLEAN)       cost_set:1;                                     /* Cost was manually configured */
        BIT_FIELD (enum, BOOLEAN)       secondary_authentication:1;     /* Secondary authentication key */
        BIT_FIELD (enum,BOOLEAN)        not_used:1;
} _pack OSPF_INTERFACE_FLAGS;
#else /* _BYTE_ORDER == _BIG_ENDIAN */
typedef _struct OSPF_INTERFACE_FLAGS
{
        BIT_FIELD (enum,BOOLEAN)        not_used:1;
        BIT_FIELD (enum, BOOLEAN)       secondary_authentication:1;     /* Secondary authentication key */
        BIT_FIELD (enum, BOOLEAN)       cost_set:1;                                     /* Cost was manually configured */
        BIT_FIELD (enum, BOOLEAN)       multicast:1;                            /* Interface is multicast capable */
        BIT_FIELD (enum, BOOLEAN)       neighbor_change:1;                      /* Schedule neighbor change */
        BIT_FIELD (enum, BOOLEAN)       build_network:1;                        /* Flag to build_net_lsa */
        BIT_FIELD (enum, BOOLEAN)       enable:1;                                       /* Interface is enabled */
        BIT_FIELD (enum, BOOLEAN)       network_scheduled:1;            /* When designated router: semaphore for generating link state advertisements */
} _pack OSPF_INTERFACE_FLAGS;
#endif /* _BYTE_ORDER == _LITTLE_ENDIAN */

typedef _union UNION_OSPF_INTERFACE_FLAGS
{
        OSPF_INTERFACE_FLAGS    _bit;
        BYTE                                    _byte;
} _pack UNION_OSPF_INTERFACE_FLAGS;

typedef struct OSPF_LS_HEADER_QUEUE
{
    struct OSPF_LS_HEADER_QUEUE *sptr_forward_link;
    struct OSPF_LS_HEADER_QUEUE *sptr_backward_link;
    OSPF_LS_HEADER                              ls_header;
} OSPF_LS_HEADER_QUEUE;

typedef struct OSPF_INTERFACE
{
    struct OSPF_INTERFACE       *sptr_forward_link;
    struct OSPF_INTERFACE       *sptr_backward_link;

    struct ifnet                *vx_ospf_interface;

    USHORT                      ifnet_index;   /* if_index from struct ifnet */
    USHORT                      ifnet_flags;
    ULONG                       interface_index;
    ULONG                       area_id;
    BYTE_ENUM (BOOLEAN)         port_enabled;

    enum OSPF_INTERFACE_TYPE    type;
    enum OSPF_INTERFACE_STATE   state;
    ULONG                       events;  /* Number of state changes */
    ULONG                       address;
    ULONG                       netmask;
    ULONG                       mtu;
    struct OSPF_AREA_ENTRY      *sptr_area;  /* The area this interface is in */
    struct OSPF_AREA_ENTRY      *sptr_transit_area;
    ULONG                       source_area_id_for_virtual_link;

    ULONG                       router_dead_interval;  /* Number of seconds before the router's sptr_neighbors will declare it down */
    ULONG                       transmit_delay;       /* Estimated number of seconds it takes to transmit a link state update packet over this interface */
    USHORT                      priority;   /* If set to 0, this router is not eligible to become designated router on the attached network */

    OSPF_NEIGHBOR               *sptr_neighbor;
    OSPF_NEIGHBOR               potential_neighbor;
    ULONG                       virtual_neighbor_rid;

    USHORT                      number_of_neighbors_in_init_state;
    USHORT                      number_of_neighbors_in_exchange_state;
    USHORT                      number_of_neighbors_in_full_state;

    OSPF_DESIGNATED_ROUTER_NODE designated_router;
    OSPF_DESIGNATED_ROUTER_NODE backup_designated_router;
    USHORT                      cost;   /* One for each type of service */

    ULONG                       hello_interval;     /* Length of time, in seconds, between Hello packets sent on this interface */
    ULONG                       poll_interval;      /* the default poll interval is 4 * the hello interval */
    ULONG                       periodic_hello_time_counter;
    ULONG                       periodic_poll_time_counter;
    BYTE_ENUM (BOOLEAN)     hello_timer_enabled;
    /* SPR 85432 -- Begin */
    ULONG           last_hello_time;
    /* SPR 85432 -- End */

    ULONG                       retransmit_interval;
    ULONG                       periodic_retransmit_time_counter;
    BYTE_ENUM (BOOLEAN)         retransmit_timer_enabled;
    ULONG                       periodic_wait_time_counter;  /* single shot timer */
    BYTE_ENUM (BOOLEAN)         wait_timer_enabled;

    ULONG                       periodic_delayed_acknowledgement_time_counter;  /* single shot timer - sort of */
    BYTE_ENUM (BOOLEAN)         delayed_acknowledgement_timer_enabled;

    OSPF_AUTHENTICATION         authentication;
    OSPF_AUTHENTICATION         authentication2;  /* Alternate authentication */
    OSPF_AUTHENTICATION_KEY     *sptr_authentication_key;

    UNION_OSPF_INTERFACE_FLAGS  flags;
    OSPF_LS_HEADER_QUEUE        *sptr_head_of_delayed_acknowledgement_list;
    ULONG                       number_of_acknowledgements_on_delayed_acknowledgement_list;

    ULONG                       lock_time;
    ULONG                       up_time;  /* Time interface came up */

    BYTE_ENUM (BOOLEAN)         point_timer_enabled;

    BYTE_ENUM (BOOLEAN)         passive;
    ULONG                       unnumbered_dest_ip;    /* unnumbered link dest ip */
    ULONG                       unnumbered_router_id;  /* unnumbered link router id */

    BYTE_ENUM (BOOLEAN)         unnumbered_nbma;

} OSPF_INTERFACE;

typedef struct OSPF_INTERFACE_NODE
{
    struct OSPF_INTERFACE_NODE *sptr_forward_link;
    struct OSPF_INTERFACE_NODE *sptr_backward_link;
    OSPF_INTERFACE *sptr_interface;
} OSPF_INTERFACE_NODE ;

typedef struct OSPF_HOSTS
{
    struct OSPF_HOSTS       *sptr_forward_link;
    struct OSPF_HOSTS       *sptr_backward_link;
    ULONG                   interface_address;
    ULONG                   type_of_service;
    USHORT                  cost;
    BYTE_ENUM (BOOLEAN)     status;
    ULONG                   area_id;
} OSPF_HOSTS;

typedef struct OSPF_SHORTEST_PATH_NODE
{
        struct OSPF_SHORTEST_PATH_NODE  *sptr_forward_link;
        struct OSPF_SHORTEST_PATH_NODE  *sptr_backward_link;
        ULONG                                           vertex;
        BYTE_ENUM (OSPF_LS_TYPE)                        vertex_type;
        OSPF_LS_DATABASE_ENTRY                  *sptr_database_entry;
        ULONG                                           cost;
        ULONG                                           intervening_router;
        OSPF_NEXT_HOP_BLOCK                             *sptr_next_hop;
} OSPF_SHORTEST_PATH_NODE;

#if (_BYTE_ORDER == _LITTLE_ENDIAN )
typedef _struct

{
        BIT_FIELD (enum, BOOLEAN)       transit:1;                      /* This is a transit area */
        BIT_FIELD (enum, BOOLEAN)       virtual_up:1;           /* One or more virtual links in this area are up */
        BIT_FIELD (enum, BOOLEAN)       stub:1;                         /* This is a  area (no external routes) */
        BIT_FIELD (enum, BOOLEAN)       nssa:1;                         /* This is a not so stubby area */
        BIT_FIELD (enum, BOOLEAN)       stub_default:1;         /* Inject default into this stub area */
        BIT_FIELD (enum,BOOLEAN)        not_used:3;
} _pack OSPF_AREA_FLAGS;
#else /* _BYTE_ORDER == _BIG_ENDIAN */
typedef _struct OSPF_AREA_FLAGS
{
        BIT_FIELD (enum,BOOLEAN)        not_used:3;
        BIT_FIELD (enum, BOOLEAN)       stub_default:1;         /* Inject default into this stub area */
        BIT_FIELD (enum, BOOLEAN)       nssa:1;                         /* This is a not so stubby area */
        BIT_FIELD (enum, BOOLEAN)       stub:1;                         /* This is a  area (no external routes) */
        BIT_FIELD (enum, BOOLEAN)       virtual_up:1;           /* One or more virtual links in this area are up */
        BIT_FIELD (enum, BOOLEAN)       transit:1;                      /* This is a transit area */
} _pack OSPF_AREA_FLAGS;
#endif /* _BYTE_ORDER == _LITTLE_ENDIAN */

typedef _union  UNION_OSPF_AREA_FLAGS
{
        OSPF_AREA_FLAGS _bit;
        BYTE                    _byte;
} _pack UNION_OSPF_AREA_FLAGS;

typedef struct OSPF_LS_DATABASE_HEAD
    {
    AVL_NODE *                  sptr_avl_database_entry;
    OSPF_LS_DATABASE_ENTRY *    sptr_linear_database_entry;
    BYTE_ENUM (BOOLEAN)         rerun;                                                                                          /* rerun this row - partial update */
    } OSPF_LS_DATABASE_HEAD;

typedef struct OSPF_ADDRESS_RANGE_LIST_ENTRY    /* list of networks associated with an area (section 6 of OSPF Version 2 specification, dated June 1995) */
{
        struct OSPF_ADDRESS_RANGE_LIST_ENTRY            *sptr_forward_link;
        struct OSPF_ADDRESS_RANGE_LIST_ENTRY            *sptr_backward_link;
    ULONG                               area_id;

        ULONG                                                           network;
        ULONG                                                           mask;
        BYTE_ENUM (BOOLEAN)                                             advertise;
        BYTE_ENUM (BOOLEAN)                                             active;
        ULONG                                                           cost;
        struct OSPF_ROUTING_TABLE_NODE          *sptr_discard_entries;
} OSPF_ADDRESS_RANGE_LIST_ENTRY;


typedef struct OSPF_AREA_ENTRY
{
        struct OSPF_AREA_ENTRY                          *sptr_forward_link;
        struct OSPF_AREA_ENTRY                          *sptr_backward_link;
        ULONG                                           area_id;

        /* area address ranges */
        ULONG                                           number_of_address_ranges;               /* for this area */
        OSPF_ADDRESS_RANGE_LIST_ENTRY                   *sptr_address_ranges;

        /* associated router interfaces */
        OSPF_INTERFACE                                  *sptr_interfaces;                       /* this is setup at configuration time */

        /* summary links advertisements */
        struct OSPF_ADVERTISEMENT_NODE                  *sptr_summary_advertisement_list_head;

        UNION_OSPF_AREA_FLAGS                           flags;                                  /* Various state bits */
        ULONG                                           stub_default_cost;

        /* shortest-path tree */
        OSPF_SHORTEST_PATH_NODE                         shortest_path_first_tree;               /* area's shortest path first tree; head is this router */
        ULONG                                           shortest_path_first_run_count;          /* # times spf has been run for this area */
        BYTE_ENUM (BOOLEAN)                             run_shortest_path_calculation;
        ULONG                                           shortest_path_calculation_time_counter;
        OSPF_SHORTEST_PATH_NODE                         *sptr_candidate;                        /* area's candidate list for dijkstra */

        OSPF_LS_DATABASE_HEAD                           ls_database_hash_table[TYPE_7_LSA + 1][OSPF_HASH_TABLE_SIZE];
        /* opaque lsa new lsdb for opaque lsa jkw*/
        OSPF_LS_DATABASE_HEAD                           opaque_ls_database_hash_table[MAX_OPAQUE_NUM + 1][OSPF_HASH_TABLE_SIZE];
        USHORT                                          number_of_autonomous_system_boundary_routers;
        OSPF_ROUTING_TABLE_ENTRY                        autonomous_system_boundary_routers;

        USHORT                                          number_of_area_border_routers;
        OSPF_ROUTING_TABLE_ENTRY                        area_border_routers;

        USHORT                                          number_of_neighbors_in_init_state;
        USHORT                                          number_of_neighbors_in_exchange_state;
        USHORT                                          number_of_neighbors_in_full_state;

        USHORT                                          number_of_interfaces_in_up_state;

        ULONG                                           lock_time;
        BYTE                                            build_router;
        BYTE_ENUM (BOOLEAN)                             bring_up_virtual_links;
        ULONG                                           periodic_MinLSInterval_time_counter;
        BYTE_ENUM (BOOLEAN)                             MinLSInterval_timer_enabled;
        /* SPR#91272*/
        BYTE_ENUM (BOOLEAN)                             area_summary;                           /* area summary for stub area */

        USHORT                                          mib_number_of_link_state_advertisements;
        ULONG                                           mib_checksum_sum;

        ULONG                                           mib_stub_metric_type;                   /* 1 = ospf metric, 2 = comparable cost, 3 = non comparable */

        USHORT                                          mib_number_of_type10_lsa;
        ULONG                                           mib_type10_checksum_sum;
        ULONG                                           nssa_abr_to_convert_type_7_to_type_5;


} OSPF_AREA_ENTRY;

typedef struct OSPF_STUB_AREA_ENTRY
{
        struct OSPF_STUB_AREA_ENTRY     *sptr_forward_link;
        struct OSPF_STUB_AREA_ENTRY     *sptr_backward_link;
        struct OSPF_AREA_ENTRY                  *sptr_area;
} OSPF_STUB_AREA_ENTRY;

typedef struct OSPF_NSSA_AREA_ENTRY
{
        struct OSPF_NSSA_AREA_ENTRY     *sptr_forward_link;
        struct OSPF_NSSA_AREA_ENTRY     *sptr_backward_link;
        struct OSPF_AREA_ENTRY                  *sptr_area;
} OSPF_NSSA_AREA_ENTRY;



typedef struct OSPF_TRANSIT_AREA_ENTRY
{
        struct OSPF_TRANSIT_AREA_ENTRY  *sptr_forward_link;
        struct OSPF_TRANSIT_AREA_ENTRY  *sptr_backward_link;
        struct OSPF_AREA_ENTRY                  *sptr_area;
} OSPF_TRANSIT_AREA_ENTRY;

/**********************************/
/* Link State Database Structures */
/**********************************/

typedef struct OSPF_ADVERTISEMENT_NODE
{
        struct OSPF_ADVERTISEMENT_NODE  *sptr_forward_link;
        struct OSPF_ADVERTISEMENT_NODE  *sptr_backward_link;
        UNION_OSPF_LINK_STATE_ADVERTISEMENT     advertisement;
} OSPF_ADVERTISEMENT_NODE;

typedef struct OSPF_NEIGHBOR_LIST
{
    struct OSPF_NEIGHBOR_LIST   *sptr_forward_link;
    struct OSPF_NEIGHBOR_LIST   *sptr_backward_link;
    struct OSPF_NEIGHBOR                *sptr_neighbor;
} OSPF_NEIGHBOR_LIST;

typedef struct OSPF_PORT_CONFIGURATION_CLASS
{
        BYTE_ENUM (BOOLEAN)                     port_enabled;
        USHORT                                  virtual_port_number;
        ULONG                                   ip_address;
        ULONG                                   subnetmask;
        ULONG                                   area_id;
        ULONG                                   transit_area_id;
        ULONG                                   source_area_id_for_virtual_link; /* The area to which VLink belongs */
        ULONG                                   type;

        USHORT                                  authentication_type;

        USHORT                                  md5_authentication_key_ID;
        BYTE                                    simple_plain_text_or_md5_password[OSPF_AUTHENTICATION_SIZE];

        ULONG                                   router_dead_interval;
        ULONG                                   transmit_delay;
        USHORT                                  priority;
        USHORT                                  cost;
        ULONG                                   hello_interval;

        ULONG                                   virtual_neighbor_rid;   /* Router ID */
        OSPF_AUTHENTICATION_KEY                         *sptr_authentication_key;
        BYTE_ENUM (BOOLEAN)                     passive;
        ULONG                                   unnumbered_dest_ip;                                                     /* unnumbered dest ip*/
        ULONG                                   poll_interval;
        ULONG                                   retransmit_interval;
        ULONG                                   nbma_ptmp_neighbor_address;   /* __NBMA_PTMP__ */

} OSPF_PORT_CONFIGURATION_CLASS;

typedef struct OSPF_PORT_CLASS
{
        OSPF_PORT_CONFIGURATION_CLASS                   config;                                 /* this portion is configured by NVRAM */
} OSPF_PORT_CLASS;

typedef struct OSPF_AREA_CONFIGURATION_CLASS
{
        ULONG                                   area_id;
        BYTE_ENUM (BOOLEAN)                     address_range_active;
        BYTE_ENUM (BOOLEAN)                     advertise_address_range;
        ULONG                                   address_range_cost;
        BYTE_ENUM (BOOLEAN)                     external_routing_capability_enabled;
        BYTE_ENUM (BOOLEAN)                     nssa_enabled;
        BYTE_ENUM (BOOLEAN)                     inject_default_route_if_stub_area;
        BYTE_ENUM (BOOLEAN)                     area_summary;
        ULONG                                   stub_default_cost;
        ULONG                                   number_of_area_address_ranges;                                                                          /* number of areas, 0 will always be the backbone */

} OSPF_AREA_CONFIGURATION_CLASS;

typedef struct OSPF_AREA_CLASS
{
        OSPF_AREA_CONFIGURATION_CLASS                   config;                                 /* this portion is configured by NVRAM */
} OSPF_AREA_CLASS;

/* RFC 2178 G.3 */
typedef struct OSPF_AREA_ADDRESS_RANGE_CONFIGURATION_CLASS
{
        ULONG                                                           network;
        ULONG                                                           mask;
        BYTE_ENUM (BOOLEAN)                                     advertise;
        BYTE_ENUM (BOOLEAN)                                     active;
        ULONG                                                           cost;
        ULONG                                                           area_id;
} OSPF_AREA_ADDRESS_RANGE_CONFIGURATION_CLASS;

/* RFC 2178 G.3 */
typedef struct OSPF_AREA_ADDRESS_RANGE_CLASS
{
        OSPF_AREA_ADDRESS_RANGE_CONFIGURATION_CLASS config;
} OSPF_AREA_ADDRESS_RANGE_CLASS;

typedef struct OSPF_EXTERNAL_ROUTE
    {
    struct OSPF_EXTERNAL_ROUTE *    sptr_forward_link;
    struct OSPF_EXTERNAL_ROUTE *    sptr_backward_link;
    ULONG                           destination_network;
    ULONG                           destination_mask;
    ULONG                           metric;
    ULONG                           forwarding_address;
    ULONG                           tag;
    ULONG                           time_stamp;
    ULONG                           advertising_router;
    ULONG                           route_protocol;
    } OSPF_EXTERNAL_ROUTE;

typedef struct OSPF_MIB_GENERAL_ENTRY
{
        ULONG                                   ospfVersionNumber;
        BYTE_ENUM (BOOLEAN)     ospfAreaBdrRtrStatus;
        ULONG                                   ospfExternLsaCount;
        ULONG                                   ospfExternLsaCksumSum;
        ULONG                                   ospfOriginateNewLsas;
        ULONG                                   ospfRxNewLsas;
} OSPF_MIB_GENERAL_ENTRY;

typedef struct WRN_OSPF_MIB_GENERAL_ENTRY
{
    BYTE_ENUM (BOOLEAN)    wrnOspfRFC1583Compatibility;
    BYTE_ENUM (BOOLEAN)    wrnOspfOpaqueLsaSupport;
    ULONG                  wrnOspfOriginateNewOpaqueLsas;
    ULONG                  wrnOspfRxNewOpaqueLsas;
    ULONG                  wrnOspfType9LsaCount;
    ULONG                  wrnOspfType9LsaCksumSum;
    ULONG                  wrnOspfType11LsaCount;
    ULONG                  wrnOspfType11LsaCksumSum;
} WRN_OSPF_MIB_GENERAL_ENTRY;


#if (_BYTE_ORDER == _LITTLE_ENDIAN )
typedef _struct OSPF_REDISTRIBUTION_BIT_OPTIONS
{
    BIT_FIELD (enum,BOOLEAN)   metric:1;
    BIT_FIELD (enum,BOOLEAN)   route_tag:1;
    BIT_FIELD (enum,BOOLEAN)   not_used:6;
} _pack OSPF_REDISTRIBUTION_OPTIONS;
#else /* _BYTE_ORDER == _BIG_ENDIAN */
typedef _struct OSPF_REDISTRIBUTION__BIT_OPTIONS
{
    BIT_FIELD (enum,BOOLEAN)   not_used:6;
    BIT_FIELD (enum,BOOLEAN)   route_tag:1;
    BIT_FIELD (enum,BOOLEAN)   metric:1;
} _pack OSPF_REDISTRIBUTION_OPTIONS;
#endif /* _BYTE_ORDER == _LITTLE_ENDIAN */
typedef _union  UNION_OSPF_REDISTRIBUTION_BIT_OPTIONS
{
        OSPF_REDISTRIBUTION_OPTIONS    _bit;
        BYTE                           _byte;
} _pack UNION_OSPF_REDISTRIBUTION_BIT_OPTIONS;

typedef struct OSPF_ADDRESS_MASK_PAIRS
{
        struct OSPF_ADDRESS_MASK_PAIRS *sptr_forward_link;
        struct OSPF_ADDRESS_MASK_PAIRS *sptr_backward_link;
        ULONG network_address;
        ULONG network_mask;
        ULONG metric_type;
        ULONG metric_value;
        ULONG route_tag;
        UNION_OSPF_REDISTRIBUTION_BIT_OPTIONS bit_options;
} OSPF_ADDRESS_MASK_PAIRS;

typedef struct OSPF_REDISTRIBUTION_CONFIGURATION
{
        enum BOOLEAN redistribute_all_bgp;
        enum BOOLEAN redistribute_all_rip;
        enum BOOLEAN redistribute_all_static;
        enum BOOLEAN redistribute_all_local;
        enum BOOLEAN redistribute_ip_default_route;

        enum BOOLEAN allow_bgp_default;

        OSPF_GENERIC_NODE ospf_imported_rip_subnets;
        OSPF_GENERIC_NODE ospf_imported_static_subnets;
        OSPF_GENERIC_NODE ospf_imported_local_subnets;
        OSPF_GENERIC_NODE ospf_imported_bgp_subnets;

        USHORT number_of_do_not_allow_neighbors;
        USHORT do_not_allow_neighbor_as[0xff];

        USHORT number_of_do_not_allow_origins;
        USHORT do_not_allow_origin_as[0xff];

} OSPF_REDISTRIBUTION_CONFIGURATION;

typedef struct ROUTE_FLAGS
    {
    unsigned char   local_interface_route:1;    /* route is for local network interface */
    unsigned char   route_down:1;               /* local net/subnet route is down due to local port down */
    unsigned char   private_route:1;
    unsigned char   overriding_allowed:1;       /* this flag is applicable only to static routes
                                                 * i.e. to a remote end of a p-to-p link or a loopback */
    unsigned char   route_to_myself:1;
    unsigned char   do_no_aging:1;              /* do not apply aging timer */
    unsigned char   route_changed:1;            /* route changed and wait for RIP Update message to send */
    unsigned char   no_subnet_mask:1;           /* route is awaiting icmp address mask reply from destination */
    } ROUTE_FLAGS;


typedef struct OSPF_IP_ROUTE_ENTRY_LINK
    {
    struct OSPF_IP_ROUTE_ENTRY *    sptr_forward_link;
    struct OSPF_IP_ROUTE_ENTRY *    sptr_backward_link;
    } OSPF_IP_ROUTE_ENTRY_LINK;

/* IP routing table entry */

typedef struct OSPF_IP_ROUTE_ENTRY
    {
    OSPF_IP_ROUTE_ENTRY_LINK    links;
#if defined (__LEXICOGRAPHICAL_ORDERING__)
    IP_ROUTE_ENTRY_LINK         mib_links;
#endif /* (__LEXICOGRAPHICAL_ORDERING__) */
    ULONG                       target;                             /* net, subnet or full host ip address */
    ULONG                       mask;
    ULONG                       gateway;                                /* 0: local attached subnet, or point-to-point link */
    ULONG                       forwarding_address;
    USHORT                      port_number;
    ULONG                       use_count;
    BYTE                        number_of_subnet_mask_bits;
    ULONG                       metric;                             /* primary metric */
    ULONG                       metric_2;                           /* alternative metric 2 */
    ULONG                       metric_3;                           /* alternative metric 3 */
    ULONG                       metric_4;                           /* alternative metric 4 */
    ULONG                       metric_5;                           /* alternative metric 5 */
    ROUTE_FLAGS                 flags;
    BYTE                        weight;
    USHORT                                  aging_timer;                        /* aging timeout value in seconds */
    BYTE_ENUM (OSPF_RTM_IP_ROUTE_TYPE)          ipRouteType;
    BYTE_ENUM (OSPF_RTM_IP_ROUTE_PROTOCOL_TYPE) ipRouteProto;
    ULONG                       ipRouteInfo[2];
    struct OSFP_IP_ROUTE_CACHE_ENTRY *      sptr_cache_entry;
    USHORT                      route_tag;
    ULONG                       sender_ip_address;
    void*                       p_ifnet;
    } OSPF_IP_ROUTE_ENTRY;


typedef struct OSPF_ROUTE_QUEUE_ENTRY
    {
    struct OSPF_ROUTE_QUEUE_ENTRY *sptr_forward_link;
    struct OSPF_ROUTE_QUEUE_ENTRY *sptr_backward_link;
    enum OSPF_ROUTING_TABLE_UPDATE_ACTION action;
    OSPF_IP_ROUTE_ENTRY ip_route_entry;
    } OSPF_ROUTE_QUEUE_ENTRY;

typedef struct OSPF_ROUTE_POLICY_PAIRS
{
        struct OSPF_ROUTE_POLICY_PAIRS *sptr_forward_link;
        struct OSPF_ROUTE_POLICY_PAIRS *sptr_backward_link;

        ULONG network_address;
        ULONG network_mask;
        UCHAR route_weight;
        UCHAR reserved[3];   /* for long word align */
} OSPF_ROUTE_POLICY_PAIRS;

typedef struct OSPF_OPAQUE_CALLBACK
{
        struct OSPF_OPAQUE_CALLBACK     *sptr_forward_link;
        struct OSPF_OPAQUE_CALLBACK     *sptr_backward_link;
        BYTE lsa_type;
        BYTE opaque_type;
        int (* new_interface_hook)(struct OSPF_INTERFACE *interface);
        int (* delete_interface_hook)(struct OSPF_INTERFACE *interface);
        void (* interface_state_machine_change_hook)(enum OSPF_INTERFACE_EVENT event,enum OSPF_INTERFACE_STATE state,struct OSPF_INTERFACE *interface);
        void (* neighbor_state_machine_change_hook)(enum OSPF_NEIGHBOR_EVENT event,enum OSPF_NEIGHBOR_STATE state,struct OSPF_INTERFACE *sptr_interface, struct OSPF_NEIGHBOR *neighbor);
        int (* opaque_lsa_originate_hook)(void *arg, BYTE lsa_type, BYTE opaque_type, USHORT age);
        void (* opaque_lsa_refresh_hook)(void *arg, BYTE lsa_type, ULONG opaque_lsid, USHORT age);
        void (* opaque_lsa_received_hook)(UNION_OSPF_LINK_STATE_ADVERTISEMENT *opaque_advertisement);
} OSPF_OPAQUE_CALLBACK;


typedef struct OSPF_PROTOCOL_RTM_HANDLE
    {
    char * pBuf;    /* Socket Message  Buffer */
    int taskId;     /* Rtm Task Id */
    int sockfd;     /* Routing Socket Descriptor */
    } OSPF_PROTOCOL_RTM_HANDLE;


typedef struct OSPF_TASK_DESCRIPTOR
{
    int                 task_id;
    int                 task_priority;
    int                 task_stack_size;
    FUNCPTR             task_start_func;
    OSPF_TASK_STATES    task_state;
    char                task_name[OSPF_TASK_NAME_SIZE];

} OSPF_TASK_DESCRIPTOR;

typedef struct OSPF_NBMA_PTMP_NBR_ENTRY
{
    struct OSPF_NBMA_PTMP_NBR_ENTRY     *sptr_forward_link;
    struct OSPF_NBMA_PTMP_NBR_ENTRY     *sptr_backward_link;

    OSPF_NEIGHBOR                      *sptr_nbma_ptmp_nbr;
}OSPF_NBMA_PTMP_NBR_ENTRY;


/*********************OSPF Meta STRUCTURE ***********************************/
typedef struct OSPF_CLASS
{
    enum BOOLEAN    protocol_enabled;
    /* asr: added to ensure that if ospf_init is called twice,
       we dont' go through initialization sequence again
     */
    enum BOOLEAN    protocol_initialized;
    ULONG           tid;          /* ospf task ID */
    ULONG           ospf_vsid;    /* ID for virtual stack on which OSPF will run */
    ULONG           router_id;    /* uniquely identifies this router in the Autonomous System */
    enum BOOLEAN    type_of_service_capability;     /* TRUE means this router will calculate separate routes based on type of service */
    enum BOOLEAN    autonomous_system_border_router;
    enum BOOLEAN    ip_multicast;

    /* NOTE: port[] shall only be used during startup for processing configuration
     *       read from ospf_cfg()
     */
    OSPF_PORT_CLASS port[NUMBER_OF_OSPF_PORTS];

    /* make sptr_interface linked list visible here. Was in OSPF_PORT_CLASS */
    OSPF_INTERFACE   *sptr_interface_list;

    ULONG            number_of_ports;
    ULONG            starting_port_number;
    ULONG            number_of_interfaces;

    /* areas */
    ULONG            number_of_areas;    /* number of areas */
    ULONG            number_of_area_address_ranges;  /* total number of area address ranges for all areas */
    ULONG            number_of_stub_areas;
    ULONG            number_of_nssa_areas;
    ULONG            number_of_redistribution_entry; 
    /* NOTE: area[] shall only be used during startup for processing configuration
     *       read from ospf_cfg()
     */
    OSPF_AREA_CLASS         area[NUMBER_OF_OSPF_AREAS];
    struct OSPF_AREA_ENTRY  *sptr_area_list;    /* an array of areas connected to this router which will be
                                                 * allocated at initialization time, area 0 is the backbone */
    /* RFC 2178 G.3 */
    OSPF_AREA_ADDRESS_RANGE_CLASS area_address_range[NUMBER_OF_OSPF_AREA_ADDRESS_RANGES]; /* area address ranges for a particular area */

    struct OSPF_AREA_ENTRY          *sptr_backbone_area;
    struct OSPF_STUB_AREA_ENTRY     *sptr_stub_area_list;

    struct OSPF_NSSA_AREA_ENTRY     *sptr_nssa_area_list;

    /* virtual links */
    ULONG             number_of_virtual_links;
    ULONG             number_of_virtual_links_in_Up_state;
    OSPF_INTERFACE_NODE     *sptr_configured_virtual_links;


    enum BOOLEAN          nssa_enabled;
    OSPF_OPAQUE_CALLBACK    *sptr_type_9_callback_list_head;
    OSPF_OPAQUE_CALLBACK    *sptr_type_10_callback_list_head;
    OSPF_OPAQUE_CALLBACK    *sptr_type_11_callback_list_head;

    OSPF_EXTERNAL_ROUTE     *sptr_configured_external_routes;
    OSPF_EXTERNAL_ROUTE     *sptr_originated_external_routes;
    OSPF_EXTERNAL_ROUTE     *sptr_external_route_queue;

    ULONG                  external_lsa_queue_process_interval;
    ULONG                  external_route_queue_counter;
    ULONG                  ospf_rtm_sequence;

    /* neighbors */
    ULONG                  number_of_neighbors;       /* number of neighbors known to this router */
    ULONG                  number_of_neighbors_in_init_state;
    ULONG                  number_of_neighbors_in_exchange_state;
    ULONG                  number_of_neighbors_in_full_state;

    OSPF_ROUTING_TABLE_NODE *   sptr_routing_table_head[OSPF_ROUTE_TABLE_MAX][OSPF_RT_HASH_TABLE_SIZE];

    enum BOOLEAN           build_routing_table;
    ULONG                  routing_table_build_time_counter;

    enum BOOLEAN           initialization_breakpoint;
    enum BOOLEAN           configuration_breakpoint;
    enum BOOLEAN           runtime_breakpoint;

    enum BOOLEAN           timer_enabled;
    ULONG                  _1_second_counter;
    ULONG                  clock_ticks_per_second;

    OSPF_MIB_GENERAL_ENTRY ospfGeneralGroupEntry;

    WRN_OSPF_MIB_GENERAL_ENTRY wrnOspfGeneralGroupEntry;
    OSPF_HOSTS             *sptr_host_list;

    enum BOOLEAN           printing_enabled;
    enum BOOLEAN           interface_printing_enabled;
    enum BOOLEAN           neighbor_printing_enabled;
    enum BOOLEAN           memory_printing_enabled;
    enum BOOLEAN           alarm_printing_enabled;
    enum BOOLEAN           snmp_printing_enabled;
    enum BOOLEAN           packet_printing_enabled;
    enum BOOLEAN           routing_table_printing_enabled;
    enum BOOLEAN           debug_printing_enabled;
    enum BOOLEAN           rtm_printing_enabled; /* ### RTM */
    enum BOOLEAN           db_overflow_printing_enabled;
    enum BOOLEAN           search_printing_enabled;
    enum BOOLEAN           prologue_printing_enabled;


    ULONG                  routing_table_revision_number;
    OSPF_RTM_HANDLE        ospf_rtm_handle; /* rtm addition */

    OSPF_ROUTE_QUEUE_ENTRY *sptr_ospf_export_route_queue;
    ULONG                  ospf_export_route_queue_process_interval;
    ULONG                  ospf_queue_interval_counter;
    ULONG                  ospf_routes_dequeued_per_run;

    ULONG                  ospf_maxaged_lsas_removed_per_tick;
    ULONG                  ospf_maxaged_lsas_removed_this_tick;

    OSPF_REDISTRIBUTION_CONFIGURATION ospf_redistribution_configuration;

        OSPF_ROUTE_POLICY_PAIRS *route_policy_list;
    /* opaque lsa neighbor opaque enabled jkw
      RFC 2370 Section 4.0 item 1*/
    enum BOOLEAN            opaque_capability;    /* TRUE means this router will be opaque-capable for opaque lsas */
    int                     ospf_multicast_extensions;
    enum BOOLEAN            ospf_demand_extensions;
    /* RFC 2178 G.7 */
    enum BOOLEAN            ospf_rfc1583_compatibility;
    /* RFC 2178 G.6 */
    ULONG                   ospf_point_to_point_router_lsa_option;
    /* RFC 1765 */
    ULONG                   ospf_external_lsdb_limit;
    ULONG                   ospf_exit_overflow_interval;
    ULONG                   ospf_external_lsa_count;
    enum BOOLEAN            in_overflow_state;
    enum BOOLEAN            force_out_of_overflow_state;
    enum BOOLEAN            reset_ospf_exit_overflow_interval;

    /* SPR 85432 -- Begin */
    ULONG               hello_1_second_counter;

    /* OSPF display */
    OSPF_LS_DATABASE_ENTRY  *sptr_external_lsdb_print_queue;
    OSPF_ROUTING_TABLE_NODE *sptr_table_route_print_queue;
    /* SPR 85432 -- End */

    const char              *ftp_configuration_file;
    const char              *ftp_server_address;
    const char              *ftp_dir;
    const char              *ftp_user;
    const char              *ftp_pwd;
    OSPF_LS_DATABASE_HEAD   external_database_hash_table[2][OSPF_HASH_TABLE_SIZE];
    OSPF_LS_DATABASE_HEAD   type_11_database_hash_table[2][OSPF_HASH_TABLE_SIZE];

    /* ANVL 27.15 start */
    OSPF_ROUTING_TABLE_NODE *sptr_old_routing_table[OSPF_ROUTE_TABLE_MAX][OSPF_RT_HASH_TABLE_SIZE];
                         /* pointer to old routing table,
                            valid during SPF computation, NULL otherwise,
                            Inter-area SPF computation can be invoked:
                            from receive task at reception of type 3 or type 4 LSA
                            from timer task (or SPF task) during regular SPF computation.
                            This "sptr_old_routing_table" is used during inter-area SPF
                            to prevent flooding of type 3 and 4 LSAs from receive task
                            and enable flooding of type 3 LSAs from timer task depending
                            on result of SPF computation.
                            Reason: RFC 2328 specifies that type 3 LSAs should only
                            be issued based on routing table entries. */
    /* ANVL 27.15 end */

    /*
     * OSPF 1 second watchdog: runs every second.
     * Gives timer_task_sem_id and hello_task_sem_id to tOspfTimer task
     * and tOspfHello task respectively.
     */
    WDOG_ID watchdog;

    /*
     * tOspfTimer task pends on this semaphore, the watchdog timer gives this
     * semaphore every second.
     */
    SEM_ID  timer_task_sem_id;

    /*
     * tOspfHello task pends on this semaphore, the watchdog timer gives this
     * semaphore every second.
     */
    SEM_ID  hello_task_sem_id;

     /*
     * Semaphore to synchronize startup of OSPF tasks. Taken by every OSPF task
     * after finishing own initialization. Given synchronously via semFlush to
     * all ospf tasks pending on it when task state of all tasks is
     * OSPFV3_TASK_STATE_INIT_COMPLETE
     */

    OSPF_TASK_DESCRIPTOR ospf_tasks[OSPF_NUM_TASKS];    /* ospf tasks */

    OSPF_NBMA_PTMP_NBR_ENTRY *ospf_nbma_ptmp_nbr_list;

    int rtmSocket;  /* routing table socket */
    int ipSocket;   /* raw ip socket */
    ULONG                   exchange_state_timers_active;
} OSPF_CLASS;

/* OSPF Traffic Engineering Extension Structure */
typedef struct OSPF_TE_TLV_HEADER
{
        USHORT type;
        USHORT length;
} OSPF_TE_TLV_HEADER;

typedef struct OSPF_TE_TLV_DATA
{
        struct OSPF_TE_TLV_DATA *sptr_forward_link;
        struct OSPF_TE_TLV_DATA *sptr_backward_link;
        void *data;
        ULONG data_length;
        int TElsatype;
} OSPF_TE_TLV_DATA;

typedef struct OSPF_TE_PARAMETERS
{
        struct OSPF_TE_PARAMETERS *sptr_forward_link;
        struct OSPF_TE_PARAMETERS *sptr_backward_link;
        ULONG ospf_te_router_address;
        OSPF_TE_TLV_HEADER ospf_te_link_header;
        unsigned char ospf_te_link_type;
        ULONG ospf_te_link_id;
        ULONG ospf_te_local_address[1];
        ULONG ospf_te_remote_address[1];
        ULONG ospf_te_metric;
        ULONG ospf_te_maximum_bandwidth;
        ULONG ospf_te_maximum_reservable_bandwidth;
        ULONG ospf_te_unreserved_bandwidth[8];
        ULONG ospf_te_resource_class_color;
} OSPF_TE_PARAMETERS;

/* Router Address TLV */
typedef struct OSPF_TE_ROUTER_ADDRESS_TLV
{
        struct OSPF_TE_TLV_HEADER header;
        ULONG value;
} OSPF_TE_ROUTER_ADDRESS_TLV;

/* Link TLV */
typedef struct OSPF_TE_LINK_TLV
{
        struct OSPF_TE_TLV_HEADER header;
} OSPF_TE_LINK_TLV;

/* Link Type */
typedef struct OSPF_TE_LINK_TYPE
{
        struct OSPF_TE_TLV_HEADER header;
        struct {
                #define POINT_TO_POINT_LINK_TYPE 1
                #define MULTIACCESS_LINK_TYPE 2
                unsigned char value;
                unsigned char padding[3];
        } link_type;
} OSPF_TE_LINK_TYPE;

/* Link ID */
typedef struct OSPF_TE_LINK_ID
{
        struct OSPF_TE_TLV_HEADER header;
        ULONG value;
} OSPF_TE_LINK_ID;

/* Local Interface IP Address */
typedef struct OSPF_TE_LOCAL_ADDRESS
{
        struct OSPF_TE_TLV_HEADER header;
        ULONG value[1];
} OSPF_TE_LOCAL_ADDRESS;

/* Remote Interface IP Address */
typedef struct OSPF_TE_REMOTE_ADDRESS
{
        struct OSPF_TE_TLV_HEADER header;
        ULONG value[1];
} OSPF_TE_REMOTE_ADDRESS;

/* Traffic Engineering Metric */
typedef struct OSPF_TE_TRAFFIC_ENGINEERING_METRIC
{
        struct OSPF_TE_TLV_HEADER header;
        ULONG value;
} OSPF_TE_TRAFFIC_ENGINEERING_METRIC;

/* Maximum Bandwidth */
typedef struct OSPF_TE_MAXIMUM_BANDWIDTH
{
        struct OSPF_TE_TLV_HEADER header;
        ULONG value;
} OSPF_TE_MAXIMUM_BANDWIDTH;

/* Maximum Reservable Bandwidth */
typedef struct OSPF_TE_MAXIMUM_RESERVABLE_BANDWIDTH
{
        struct OSPF_TE_TLV_HEADER header;
        ULONG value;
} OSPF_TE_MAXIMUM_RESERVABLE_BANDWIDTH;

/* Unreserved Bandwidth */
typedef struct OSPF_TE_UNRESERVED_BANDWIDTH
{
        struct OSPF_TE_TLV_HEADER header;
        ULONG value[8];
} OSPF_TE_UNRESERVED_BANDWIDTH;

/* Resource Class/Color */
typedef struct OSPF_TE_RESOURCE_CLASS_COLOR
{
        struct OSPF_TE_TLV_HEADER header;
        ULONG value;
} OSPF_TE_RESOURCE_CLASS_COLOR;


typedef struct OSPF_AVL_LSDB_NODE
{
    AVL_NODE                  node;
    OSPF_LS_DATABASE_ENTRY    *sptr_database_entry;
} OSPF_AVL_LSDB_NODE;


typedef struct OSPF_AVL_LSDB_KEY
{
    ULONG   id;
    ULONG   router;
    BYTE_ENUM (OSPF_LS_TYPE) type;
} OSPF_AVL_LSDB_KEY;

#endif /* _OSPF_STRUCTURES_H_ */

