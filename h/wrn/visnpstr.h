/*
 * $Log:: /Tornado/target/h/NB/visnpstr.h                                                       $
 * 
 * 1     2/09/00 4:42p Admin
 * 
 * 1     8/26/99 7:46p Alex
 * 
 * 1     5/26/99 9:53a Alex
 * 
 * 1     4/14/99 5:11p Rajive
 * Initial vesrion of Next Gen RouterWare Source code
 * 
 * 1     1/18/99 5:10p Mahesh
 * 
 * 1     1/18/99 4:24p Mahesh
 * 
 * 4     4/30/98 1:16p Rajive
 * Overwrote with INCLUDE v4.2.1
 * 
 * 3     4/27/98 11:42a Release Engineer
 * Additions into include for L2TP and RTM modules.
 * INCLUDE v4.2.1
 * 
 * 1     2/13/98 12:18a Release Engineer
 * code cleanup, code style changes, linted, system level test
 * INCLUDE v4.2.0
 * 
 * 4     5/21/96 7:32p Ross
 * Fixes for token ring header files
 * 
 * 3     3/27/96 7:08p Ross
 * Initial check-in with source safe.
*/
/*
modification history
--------------------
02  May0902,ark		Added fix for SPR 70535
*/
/************************************************************************/
/*	Copyright (C) 1993 - 1998 RouterWare, Inc.									*/
/*	Unpublished - rights reserved under the Copyright Laws of the			*/
/*	United States.  Use, duplication, or disclosure by the 					*/
/*	Government is subject to restrictions as set forth in 					*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 			*/
/*	Computer Software clause at 252.227-7013.										*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach Ca	*/
/************************************************************************/
#if !defined (_VISNPSTR_H_)
#define _VISNPSTR_H_

#define NO_PORT_NEEDED 0xffff

#if !defined (__MIB_OPERATION_DEFINED__)
	#define __MIB_OPERATION_DEFINED__

	enum MIB_OPERATION
	{
		GET_OPERATION,
		GET_NEXT_OPERATION,
		SET_OPERATION,
		VERIFY_OPERATION
	};
#endif

enum MIB_TABLE_TYPE
{
	MIB_TABLE_AS_AN_ARRAY 									= 1,
	MIB_TABLE_AS_AN_ARRAY_OF_LINKED_LISTS 				= 2,
	MIB_TABLE_AS_A_LINKED_LIST 							= 3,
	MIB_TABLE_AS_A_LINKED_LIST_OF_ARRAYS 				= 4,
	MIB_TABLE_AS_A_LINKED_LIST_OF_LINKED_LISTS 		= 5
};


enum SNMP_LEXICAL_TYPE
{
	SNMP_INVALID_TYPE 		= 1,
	SNMP_IP_ADDRESS_TYPE 	= 2,
	SNMP_IPX_ADDRESS_TYPE 	= 3,
	SNMP_SCALAR_TYPE 			= 4,
	SNMP_STRING_TYPE 			= 5
};


typedef	struct	TABLE_ENTRY_INDEX
{
	BYTE_ENUM (SNMP_LEXICAL_TYPE) 		index_type;
	ULONG 								size_of_index;
	ULONG								offset_to_index;
	BOOLEAN								need_to_increment;
	void								*vptr_index_value;
	BOOLEAN								need_to_swap;
	void								*vptr_maximum_value_of_index;
	BOOLEAN								should_be_used_to_get_pointer_to_next_array_element;
} TABLE_ENTRY_INDEX;


typedef	struct	SNMP_TABLE_ENTRY_PARAMETERS
{
	ULONG 									address_of_structure_containing_mib_variable;

	BOOLEAN									is_a_table_entry;
	BYTE									number_of_indices;
	TABLE_ENTRY_INDEX						table_indices[6];

	BYTE_ENUM (MIB_TABLE_TYPE)				table_type;
	void 									*vptr_first_row_in_mib_table;
	ULONG 									size_of_array_element;
	ULONG 									number_of_array_elements;
	ULONG 									offset_of_first_linked_list_pointer;
	ULONG 									offset_of_second_linked_list_pointer;
} SNMP_TABLE_ENTRY_PARAMETERS;


typedef	struct	SNMP_TABLE_ENTRY
{
	char									*mib_name;
	USHORT 									size_of_mib_variable;

	enum TEST 								(*fptr_mib_access_function) (USHORT port_number,
												enum MIB_OPERATION mib_operation, ULONG offset_into_class,
												ULONG class_address, ULONG size_of_port_class, void *vptr_mib_value,
												USHORT *usptr_length_of_mib_obtained);

	BOOLEAN								need_to_increment_mib_value_for_get_operation;
	BOOLEAN								need_to_swap;
	ULONG 								offset_of_mib_variable;
	SNMP_TABLE_ENTRY_PARAMETERS 		*sptr_parameters;			/* This should never be NULL except for the terminal table entry */
} SNMP_TABLE_ENTRY;


typedef	struct	MIB_ACCESS_PARAMETERS
{
	enum MIB_OPERATION	 					mib_operation;
	char 									*cptr_mib_string;
	BYTE 									*bptr_variable_value;
	USHORT 									*usptr_size_of_variable_value;
	USHORT 									*usptr_size_of_table_indices;
	ULONG 									*ulptr_table_indices;
	BOOLEAN 								*eptr_end_of_table;
	char 									**ptr_to_cptr_next_variable_name;
} MIB_ACCESS_PARAMETERS;

typedef	enum TEST (*SNMP_ACCESS_FUNCTION) (USHORT port_number,
				 	enum MIB_OPERATION mib_operation, ULONG offset_into_class,
					ULONG class_address, ULONG size_of_port_class, void *vptr_mib_value,
					USHORT *usptr_length_of_mib_obtained);

#endif /* _VISNPSTR_H_*/



