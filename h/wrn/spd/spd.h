/* spd.h - API routines for IPsec Security Policy Database */

/* Copyright 2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,26apr05,rlm  Creation.
*/

#ifndef __SPD_H
#define __SPD_H

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif


/*
DESCRIPTION

INCLUDE FILES:
*/

/* includes */

/* defines */

/* typedefs */

/* public functions */

STATUS spdSetAHXform
    (
    char *pConfStr /* configuration string */
    );

STATUS spdSetDefault
    (
    char *pConfStr
    );

STATUS spdSetESPXform
    (
    char *pConfStr /* parameter string */
    );

STATUS spdSetProp
    (
    char *pConfStr /* parameter string */
    );

STATUS spdClrProp
    (
    char *pConfStr /* parameter string */
    );

STATUS spdSetPropAttrib
    (
    char *pConfStr
    );

STATUS spdSetSA
    (
    char *pConfStr /* parameter string */
    );

STATUS spdAddTunnel
    (
    char *pConfStr /* parameter string */
    );

STATUS spdAddBypass
    (
    char *pConfStr /* parameter string */
    );

STATUS spdAddDiscard
    (
    char *pConfStr /* parameter string */
    );

STATUS spdDeletePolicy
    (
    char *pConfStr /* parameter string */
    );

STATUS spdAddTransport
    (
    char *pConfStr /* parameter string */
    );

STATUS spdShowAHXform
    (
    char *pConfStr /* parameter string */
    );

STATUS spdShowESPXform
    (
    char *pConfStr /* parameter string */
    );

STATUS spdShowProp
    (
    char *pConfStr /* parameter string */
    );

STATUS spdShowSA
    (
    char *pConfStr /* parameter string */
    );

STATUS spdShowPolicy
    (
    char *pConfStr /* parameter string */
    );

STATUS spdShow
    (
    void
    );
    
void spdDump
    (
    void
    );

int spdMon
    (
    char *cptr_action /* parameter string */
    );

int spdDebug
    (
    char *cptr_action /* parameter string */
    );

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif /* __cplusplus */

#endif /* __SPD_H */

