/************************************************************************/
/*	Copyright (C) 1999 RouterWare, Inc.									*/
/*	Unpublished - rights reserved under the Copyright Laws of the		*/
/*	United States.  Use, duplication, or disclosure by the 				*/
/*	Government is subject to restrictions as set forth in 				*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 		*/
/*	Computer Software clause at 252.227-7013.							*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach, CA */
/************************************************************************/
#if !defined (__UDP_IF_H__)
#define __UDP_IF_H__

typedef UINT LOWER_LAYER_IF_HANDLE;

typedef struct UDP_MESSAGE
{
	IP_ADDRESS source_address;
	IP_ADDRESS destination_address;
	UINT type_of_service;			/*ip parameter*/
	UINT time_to_live;				/*ip parameter*/
	UINT sequence_identifier;		/*ip parameter*/
	UINT do_not_fragment_flag;		/*ip parameter*/
	UINT source_port_number;
	UINT destination_port_number;
	RW_PACKET_HANDLE payload;
} UDP_MESSAGE;



/****************************************************************************/
typedef struct UDP_CALLBACK_IF
{
	bool (*fn_udp_callback_if_receive) (struct UDP_CALLBACK_IF* p_callback_if, UDP_MESSAGE* p_udp_message, LOWER_LAYER_IF_HANDLE lower_layer_if_handle);
	
} UDP_CALLBACK_IF;

/****************************************************************************/
typedef struct UDP_IF
{
	bool (*fn_udp_if_open_port) (struct UDP_IF *p_udp_if, UDP_CALLBACK_IF *p_callback_if, UINT port_number);

	bool (*fn_udp_if_close_port) (struct UDP_IF *p_udp_if, UDP_CALLBACK_IF *p_callback_if, UINT port_number);			

	bool (*fn_udp_if_send) (struct UDP_IF *p_udp_if, UDP_MESSAGE *p_udp_message, LOWER_LAYER_IF_HANDLE lower_layer_if_handle);
	
	bool (*fn_udp_if_send_next_hop) (struct UDP_IF *p_udp_if, UDP_MESSAGE *p_udp_message, IP_ADDRESS *p_next_hop, LOWER_LAYER_IF_HANDLE lower_layer_if_handle);

}UDP_IF;


#endif /*__UDP_IF_H__*/
