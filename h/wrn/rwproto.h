/*
 * $Log:: /Tornado/target/h/NB/rwproto.h                                                                                           $
 * 
 * 1     2/09/00 4:42p Admin
 * 
 * 2     8/26/99 7:42p Alex
 * 
 * 1     8/25/99 7:01p Alex
 * 
 * 1     5/26/99 9:53a Alex
 * 
 * 2     4/21/99 12:04p Rajive
 * Added prototype for closing udp
 * 
 * 1     4/14/99 5:09p Rajive
 * Initial vesrion of Next Gen RouterWare Source code
 * 
 * 3     1/22/99 7:31p Mahesh
 * Added function prototype for new function to set the close event.
 * 
 * 2     1/21/99 10:38p Mahesh
 * Changes for the unified RWOS interface.
 * 
 * 1     1/18/99 5:10p Mahesh
 * 
 * 1     1/18/99 4:24p Mahesh
 * 
 * 24    1/06/99 2:57p Nishit
 * Added prototypes for BOOTP Client and Relay Agent, DHCP Client and Server, TFTP Client, and Telnet Server
 * 
 * 23    12/23/98 3:39p Nishit
 * Since PPPOE init function name changed...
 * 
 * 22    12/02/98 7:54p Jack
 * Added __WINROUTER__ ifdef in the  enum LSL_CONTROL_OPERATION around LSL_ROUTERWARE_CONTROL - potential bug identified while
 * testing TCP on big_endian
 * 
 * 21    12/01/98 11:43a Rajive
 * Uncommented the prototypes for OPEN_VIRTUAL_PORT, CLOSE_VIRTUAL_PORT, REINITIALIZE_VIRTUAL_PORT and UNINITIALIZE_VIRTUAL_PORT
 * 
 * 20    11/30/98 4:03p Nishit
 * Added prototypes for init and close for PPPoE and RADIUS
 * 
 * 19    11/24/98 12:46p Nishit
 * Added support for NAT
 * 
 * 18    11/20/98 11:37a Nishit
 * Added init and shutdown prototypes for single build
 * 
 * 17    11/18/98 5:53p Rajive
 * Fixed syntax error
 * 
 * 16    11/18/98 5:50p Rajive
 * Added and delted new enums from the enum PROTOCOL_CONTROL_OPERATION.
 * 
 * 15    11/06/98 6:19p Nishit
 * Added prototypes for RTM, L2TP, Socket... initialize
 * 
 * 14    10/30/98 10:06a Alex
 * New prototype declaration for the initialization of the internal network.
 * 
 * 13    9/11/98 10:11a Nishit
 * Added SET_IP_ADDRESS_AND_MASK_FOR_PORT in enum PROTOCOL_CONTROL_OPERATION
 * 
 * 12    9/03/98 4:11p Nishit
 * Added VPN Interface Virtual Protocol Stack
 * 
 * 11    8/01/98 6:48p Ross
 * Added PPP_OVER_ETHERNET protocol stack type.
 * 
 * 10    7/05/98 4:12p Ross
 * Had to add __KERNEL__ in dll function if defined...a must to build the .vxd's
 * 
 * 9     6/01/98 10:55a Nishit
 * Added values for PAC
 * 
 * 8     5/14/98 1:56p Nishit
 * Added PPTP_GRE_V2_TRANSPORT in enum TRANSPORT_TYPE and PPTP_TUNNEL_CONTROL in enum APPLICATION_TYPE - for PNS
 * 
 * 7     4/30/98 1:16p Rajive
 * Overwrote with INCLUDE v4.2.1
 * 
 * 3     4/27/98 11:42a Release Engineer
 * Additions into include for L2TP and RTM modules.
 * INCLUDE v4.2.1
 * 
 * 1     2/13/98 12:18a Release Engineer
 * code cleanup, code style changes, linted, system level test
 * INCLUDE v4.2.0
 * 
 * 6     9/25/96 7:01p Ross
 * Added transport control changes
 * 
 * 5     9/09/96 5:10p Ross
 * Changes for spanning tree and token ring
 * 
 * 4     5/21/96 7:32p Ross
 * Fixes for token ring header files
 * 
 * 3     4/11/96 6:44p Ross
 * SNMP changes.
 * 
 * 2     3/27/96 7:08p Ross
 * Initial check-in with source safe.
*/
/*	$Modname: lslproto.h$  $version: 1.5$      $date: 02/02/94$   */
/*
* 	$lgb$
1.0 10/11/93 ross adding version control.
1.1 10/11/93 ross added some prototypes.
1.2 10/18/93 ross added lsl frame type and ip header.
1.3 12/03/93 ross fixes for lsl version 3.0
1.4 01/08/94 ross More LSL version 3 changes
1.5 02/02/94 ross
* 	$lge$
*/
/************************************************************************/
/*	Copyright (C) 1989 - 1998 RouterWare, Inc										*/
/*	Unpublished - rights reserved under the Copyright Laws of the			*/
/*	United States.  Use, duplication, or disclosure by the 					*/
/*	Government is subject to restrictions as set forth in 					*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 			*/
/*	Computer Software clause at 252.227-7013.										*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach Ca  	*/
/************************************************************************/
#ifndef _LSLPROTO_H_
#define _LSLPROTO_H_

#define ILLEGAL_DEVICE_ID 0xffffffffL

#define VIRTUAL_PORT_TYPE	(USHORT) 0x8000

#if !defined (LINK_TYPEDEF_DEFINED)

	#define LINK_TYPEDEF_DEFINED

typedef	struct	LINK
{
	struct	LINK	*sptr_forward_link;
	struct	LINK	*sptr_backward_link;
} LINK;

typedef	struct	SINGLE_LINK
{
	struct	SINGLE_LINK	*sptr_forward_link;
} SINGLE_LINK;

#endif

enum LINKED_LIST_COMPARISON_RESULT
{
	LESS_THAN_CURRENT_ELEMENT,
	GREATER_THAN_CURRENT_ELEMENT,
	SAME_AS_CURRENT_ELEMENT,
	ERROR_IN_COMPARISON
};

typedef	struct	LINKED_LIST_INDEX
{
	USHORT 					offset;
	USHORT 					size;
	BOOLEAN					_swap;
} LINKED_LIST_INDEX;

typedef	struct	LINKED_LIST_SORT_PARAMETERS
{
	LINK					*sptr_list;
	LINK					*sptr_entry_to_add;

	LINKED_LIST_INDEX	index[5];

} LINKED_LIST_SORT_PARAMETERS;



typedef	struct	LSL_MESSAGE_LINK
{
	struct LSL_MESSAGE_LINK	*sptr_forward_link;
	struct LSL_MESSAGE_LINK	*sptr_backward_link;
} LSL_MESSAGE_LINK;


enum SNMP_BOOLEAN
{
	SNMP_BOOLEAN_ENABLED = 0x01,
	SNMP_BOOLEAN_DISABLED = 0x02
};

enum SORT_RETURN
{
	LESS_THAN = -1,
	EQUAL_TO = 0,
	GREATER_THAN = 1
};



#if !defined (__MIB_OPERATION_DEFINED__)
	#define __MIB_OPERATION_DEFINED__

	enum MIB_OPERATION
	{
		GET_OPERATION,
		GET_NEXT_OPERATION,
		SET_OPERATION,
		VERIFY_OPERATION
	};
#endif



#endif /* _LSLPROTO_H_ */
