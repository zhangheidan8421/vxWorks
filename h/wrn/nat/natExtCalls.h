/* natExtCalls.h */

/* NAT Application Programming Interface */

/* Copyright 2001-2005 Wind River Systems, Inc. */

/* @format.tab-size 4, @format.use-tabs true, @format.new-line lf */

/*
modification history
--------------------
01g,10mar05,svk  Add SIP ALG init function, move DMZ Host and port triggering
		 functions to nat_api.h
01f,04mar05,svk  Add port trigger timer to config params
01e,02mar05,myz  added configuration parameter structures
01d,14feb05,zhu  added port triggering support
01c,17jan05,zhu  Updated for DMZ host
01b,15may03,zhu  added nat PPTP and IPSEC ALGs
01a,25apr03,zhu  updated copyright
101901	tk	Created to declare external calls from prjConfig.c to suppress
		warning messages when building the VxWorks AE image.
*/

#ifndef natExtCallsh
#define natExtCallsh

/* IP static entry configuration */

typedef struct {
    char * pIpLocAddr;        /* local address */
    char * pIpGlbAddr;        /* global address */
    } NATCFG_IP_STATIC_ENTRY;

/* TCP/UDP static entry configuration */

typedef struct {
    char * pIpLocAddr;        /* local address */
    USHORT locPort;           /* local port */
    USHORT glbPort;           /* global port */
    } NATCFG_PORT_STATIC_ENTRY;

/* NAT configuration parameters */

typedef struct {
    int numOfIfs;  /*derived. number of network interfaces(locals + global)*/
    char * pGlbIf; /* global network interface name string */
    char * pLocIfs;             /* a list of local interface name string */
    BOOL  natEnabled;           /* NAT global enable/disable flag */
    BOOL  staticEntryEnabled;   /* NAT static entry enable/disable flag */
    BOOL  singleGlbAddrEnabled; /* NAPT or basic mode */
    ULONG glbIpAddr;            /* NAT global IP address */
    ULONG glbIpMask;            /* NAT global IP address mask */
    BOOL  dynmicGlbAddr;        /* TRUE/FALSE, dynamic global IP address */
    BOOL  fltNonCorpAddr;       /* TRUE/FALSE, filter non corp. addresses*/
    BOOL  fltUnknownProtos;     /* TRUE/FALSE, filter unknown protocols */

    ULONG tcpDiscnntTimer;      /* TCP disconnected timer */
    ULONG tcpCnntTimer;         /* TCP connecting timer */
    ULONG tcpCnntedTimer;       /* TCP connected timer */
    ULONG tcpClsTimer;          /* TCP closing timer */
    ULONG ipTranEntryTimer;     /* IP translation entry timer */
    ULONG udpTranEntryTimer;    /* UDP translation entry timer */
    ULONG icmpTranEntryTimer;   /* ICMP translation entry timer */
    ULONG seqEntryTimer;        /* TCP sequence delta timer */
    ULONG portTriggerTimer;     /* Port trigger entry timer */

    /* NAT printf debug flags. TRUE: flag enabled, FALSE: flag disabled */

    BOOL prtEnabled;            /* use printf() to send debug messages */ 
    BOOL prtDebug;              /* add source code file names/line to output*/ 
    BOOL prtInitEnabled;        /* printing of initialization event info. */
    BOOL prtTraceEnabled;       /* printing of trace info. */
    BOOL prtDataEnabled;        /* printing of data packet */
    BOOL prtErrEnabled;         /* printing of error info. */
 
    /* 
     * NAT log debug flags. identical to NAT printf debug message 
     * except the debug messages are send through logMsg() instead of printf().
     */

    BOOL logEnabled;            /* use logMsg() to send debug messages */
    BOOL logInitEnabled;
    BOOL logTraceEnabled;
    BOOL logDataEnabled;
    BOOL logErrEnabled;

    ULONG glbAddrPoolSize;      /* global address pool size */
    ULONG glbAddrStart;         /* global address start */
    ULONG icmpDftAddr;          /* icmp default IP address */

    NATCFG_IP_STATIC_ENTRY * pIpEntries;    /* IP static entries */
    int numOfIpEntries;                    
    NATCFG_PORT_STATIC_ENTRY * pTcpEntries; /* TCP static entries */
    int numOfTcpEntries;
    NATCFG_PORT_STATIC_ENTRY * pUdpEntries; /* UDP static entries */
    int numOfUdpEntries;

    } NAT_CONFIG_PARAMS;

extern  STATUS  natAgentInit (NAT_CONFIG_PARAMS *); /* NAT init function */
extern  STATUS  natFtpInit(u_short ftpPort); /* FTP ALG agent init function */
extern  STATUS  natH323Init(u_short h323Port); /* H.323 ALG agent init function */
extern  STATUS natPPTPPTInit(void); /* PPTP Pass-thru ALG agent init function */
extern  STATUS natIPSecPTInit(u_short); /* IPSec Pass-thru ALG agent init function */
extern  STATUS natSipInit(void); /* SIP ALG agent init function */

#endif /* Dont' add anything after this line */
