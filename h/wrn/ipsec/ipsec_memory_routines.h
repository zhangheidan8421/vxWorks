/* ipsec_memory_routines.h - Memory routines for IPsec */
/* Copyright 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,30nov04,jfb  Beautified again
01c,29nov04,jfb  Beautified
01b,28sep04,cdw  Added configuration for IPsec memory pool.  Added
                 documentation for IPsec memory pool configuration.
01a,17sep04,cdw  created
*/

#ifndef __IPSEC_MEMORY_ROUTINES_H__
#define __IPSEC_MEMORY_ROUTINES_H__

#ifdef __cplusplus
extern "C"
    {
#endif
#include <vxWorks.h>

/* Use the netbufLib memory pools to allocate small buffers.  Using
 * this feature requires tuning of the data clusters used by the
 * memory pool.  The following is recommended as a starting point, but
 * additional tuning may be required:
 *
 *    64 byte clusters - IPSEC_NUM_64    1500
 *   128 byte clusters - IPSEC_NUM_128   1500
 *   256 byte clusters - IPSEC_NUM_256   50
 *   512 byte clusters - IPSEC_NUM_512   50
 *  1024 byte clusters - IPSEC_NUM_1024  25
 *  2048 byte clusters - IPSEC_NUM_2048  10
 *
 * Use ipsecMemoryPoolShow() to observe remaining free clusters.
 */
#define IPSEC_MEMORY_USE_MEMORY_POOL
#ifdef IPSEC_MEMORY_USE_MEMORY_POOL
void ipsecMemoryPoolShow (void);
#define IPSEC_NUM_64    1500
#define IPSEC_NUM_128   1500
#define IPSEC_NUM_256   50
#define IPSEC_NUM_512   50
#define IPSEC_NUM_1024  25
#define IPSEC_NUM_2048  10
#endif

/* Enable IPsec memory usage statistics - Observe results with
 * ipsec_memory_stats() */
#define IPSEC_MEMORY_STATISTICS

/* taskSuspend() on memory allocation in tNetTask context.  Controlled
 * at runtime by setting/clearing the global BOOL tNetTaskAllocationDebugging */
#undef IPSEC_MEMORY_NETTASK_ALLOC_DEBUGGING

/* taskSuspend() the calling task on memory allocation failure */
#undef IPSEC_MEMORY_SUSPEND_ON_FAILURE

/* Initialize all buffers to IPSEC_MEMORY_INITIALIZATION_FIELD after
 * allocation and prior to freeing. This is meant as a debugging aid,
 * it should normally be undefined for performance reasons. */
#undef IPSEC_MEMORY_INITIALIZE
#define IPSEC_MEMORY_INITIALIZATION_FIELD ((unsigned char)0xfd)

extern BOOL tNetTaskAllocationDebugging;
STATUS ipsecMemoryStats (void);
void *ipsecMemoryAllocate
    (
    UINT elemSize
    );

void *ipsecMemoryCalloc
    (
    UINT elemNum,
    UINT elemSize
    );

void ipsecMemoryFree (
void *p_object);

/* IPSEC_MEMORY_ALLOCATED_MAGIC indicates that the buffer was
 * allocated using the ipsecMemory allocation routines */
#define IPSEC_MEMORY_ALLOCATED_MAGIC 0xadfacade

typedef struct
    {
    UINT reserved;     /* Set to IPSEC_MEMORY_ALLOCATED_MAGIC */
    UINT bufferType;   /* May indicate type of buffer (use is optional) */
    UINT bufferLength; /* The allocated length of *buffer */
    } IPSEC_MEMORY_NODE;

/* buffer types */
    #define IPSEC_MEMORY_MBLK_ALLOCATED 0x80000000

    #ifdef __cplusplus
    }
    #endif
#endif /* __IPSEC_MEMORY_ROUTINES_H__ */
