/* ipsecStats.h - WindNet IPsec: Statistics Operations API's    */

/* Copyright 2000-2004 Wind River Systems, Inc.     */

/* 
modification history
--------------------
01d,30nov04,jfb  Beautified again
01c,29nov04,jfb  Beautified
01b,02jun04,swc  Moved enum to ipsec_stats.h and add prototypes. 
01a,05may04,jfb  created
*/

/*
DESCRIPTION

INCLUDE FILES: vxWorks.h

IPsec Statistics Header File. Provides structures and API's for 
retrieving counts associated with IPsec.
*/

#if !defined (__IPSEC_STATS_H__)
#define __IPSEC_STATS_H__

#ifdef __cplusplus
extern "C"
    {
#endif
#ifdef INCLUDE_COUNTERS_IPSEC

/* enable/disable. Do a #undef or comment out to exclude a counter category
 */
#define INCLUDE_COUNTERS_NETWORK_INTERFACE
#define INCLUDE_COUNTERS_POLICIES
#define INCLUDE_COUNTERS_PROTECTION_SUITES
#endif /* INCLUDE_COUNTERS_IPSEC */
/* The following definitions provide API's for retrieving statistics associated
 * with IPsec.
 */

/* NETWORK_INTERFACE_COUNTS
   (this is included by ipsec_network_interface.h)
 */

typedef struct interface_counts
    {
    UINT64 byteCount;
    UINT32 packetCount;
    UINT32 noPSDiscardCount;
    UINT32 noPolicyDiscardCount;
    UINT32 explicitDiscardCount;
    UINT32 explicitBypassCount;
    UINT32 policyErrorCount;
    } INTERFACE_COUNTS;

/* PROTECTION_SUITE_COUNTS
 */
typedef struct network_interface_counts
    {
    INTERFACE_COUNTS inbound;
    INTERFACE_COUNTS outbound;
    } NETWORK_INTERFACE_COUNTS;

/* IPSEC_COUNTS used for both Protection Suite and Policy counts
 */

    #define MAX_NAME_LENGTH 256

typedef struct ipsec_counts
    {
    UINT64 preProcessedByteCount;
    UINT64 postProcessedByteCount;
    UINT64 failedProcessingByteCount;
    UINT32 packetCount;
    UINT32 espDecryptionErrors;
    UINT32 ahAuthenticationErrors;
    UINT32 espAuthenticationErrors;
    UINT32 ahSequenceErrors;
    UINT32 espSequenceErrors;
    UINT32 otherErrors;
    UINT32 ahSpi;
    UINT32 espSpi;
    UINT32 upTime;
    UINT32 remainingTimeHard;
    UINT32 remainingTimeSoft;
    } IPSEC_COUNTS;

/* SECURITY_POLICY_COUNTS
*/
typedef IPSEC_COUNTS SECURITY_POLICY_COUNTS;

/* PROTECTION_SUITE_COUNTS
 */
typedef struct protection_suite_counts
    {
    UINT32 proposalNumber;
    char proposalName[MAX_NAME_LENGTH];
    char transform[MAX_NAME_LENGTH];
    IPSEC_COUNTS inbound;
    IPSEC_COUNTS outbound;
    } PROTECTION_SUITE_COUNTS;

/* function declarations */

    #if defined(__STDC__) || defined(__cplusplus)

    /* populates a NETWORK_INTERFACE_COUNTS structure */
    extern STATUS ipsecIfStatsGet
        (
        char *cptrAddrString,
        NETWORK_INTERFACE_COUNTS *stats
        );

    extern void ipsecIfStatsPrint
        (
        NETWORK_INTERFACE_COUNTS *stats
        );

    extern void ipsecIfStatsDump
        (
        char *cptrAddrString
        );

    /* populates a SECURITY_POLICY_COUNTS structure */
    extern STATUS ipsecPolicyStatsGet
        (
        char *p_configuration_string,
        SECURITY_POLICY_COUNTS *inboundStats,
        SECURITY_POLICY_COUNTS *outboundStats
        );

    extern void ipsecPolicyStatsPrint
        (
        SECURITY_POLICY_COUNTS *inboundStats
        );

    extern void ipsecPolicyStatsDump
        (
        char *p_configuration_string
        );

    /* populates a PROTECTION_SUITE_COUNTS structure */
    extern STATUS protectionSuiteStatsGet
        (
        char *p_configuration_string,
        PROTECTION_SUITE_COUNTS *stats
        );

    extern STATUS protectionSuiteStatsGetBySPI
        (
        int spi,
        PROTECTION_SUITE_COUNTS *stats
        );

    extern void protectionSuiteStatsPrint
        (
        PROTECTION_SUITE_COUNTS *stats
        );

    extern void protectionSuiteStatsDump
        (
        char *p_configuration_string
        );
    #else /* __STDC__ */
    extern STATUS ipsecIfStatsGet ();
    extern STATUS ipsecIfStatsPrint ();
    extern STATUS ipsecIfStatsDump ();
    extern STATUS ipsecPolicyStatsGet ();
    extern STATUS ipsecPolicyStatsPrint ();
    extern STATUS ipsecPolicyStatsDump ();
    extern STATUS protectionSuiteStatsGet ();
    extern STATUS protectionSuiteStatsGetBySPI ();
    extern STATUS protectionSuiteStatsPrint ();
    extern STATUS protectionSuiteStatsDump ();
    #endif /* __STDC__ */
    #ifdef __cplusplus
    }
    #endif
#endif /* __IPSEC_STATS_H__*/
