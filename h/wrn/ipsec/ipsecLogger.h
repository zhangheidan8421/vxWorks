/************************************************************************/
/* Copyright 2000-2004 Wind River Systems, Inc. 						*/
/************************************************************************/
/*
modification history
--------------------
01g,13apr05,djp  Fixed include paths
01f,11apr05,djp  added ipsecLoggerInit
01e,30nov04,jfb  Beautified again
01d,29nov04,jfb  Beautified
01c,26may04,djp  Added additional reasons for IKE packet discards
01b,13may04,hms added sa_family parm to callback. added ipsecLoggerIsEnabled()
01a,05may04,djp Initial version of IPsec Logger Header File
*/

#if !defined (__IPSEC_LOGGER_H__)
#define __IPSEC_LOGGER_H__

#ifdef __cplusplus
extern "C"
    {
#endif
#include <vxWorks.h>
#include <wrn/rwos/rw_util.h>

/* The following definitions provide compile time control over events that are
 * enabled for callback purposes.
 * To enable a specific event at compile time, the line of interest is written 
 * as:
 * #define <ipsec_event>
 * where <ipsec_event> refers to the event of interest. To disable a specific
 * event at compile time, the line of interest is modified to be:
 * #undef <ipsec_event>.
 *
 * NOTE: The following macro definitions correspond to the enums below for
 * the purpose of clarity. For example,
 * the macro definition SPD_PACKET_DISCARD_ON corresponds to the enum
 * SPD_PACKET_DISCARD. 
 */

#ifdef INCLUDE_LOGGING_IPSEC

/* enable/disable. Do a #undef or comment out to exclude a logging category.
 */
#define INCLUDE_LOGGING_SPD_PACKET_DISCARD
#define INCLUDE_LOGGING_IKE_PACKET_DISCARD
#define INCLUDE_LOGGING_IPSEC_PACKET_DISCARD
#define INCLUDE_LOGGING_IKE_PHASE_I_FAIL
#define INCLUDE_LOGGING_IKE_PHASE_I_SUCCESS
#define INCLUDE_LOGGING_IKE_PHASE_II_FAIL
#define INCLUDE_LOGGING_IKE_PHASE_II_SUCCESS
#define INCLUDE_LOGGING_SEQUENCE_NUM_OVERFLOW
#define INCLUDE_LOGGING_SOFT_LIFE_TIME_EXPIRED
#define INCLUDE_LOGGING_HARD_LIFE_TIME_EXPIRED
#define INCLUDE_LOGGING_SOFT_LIFE_SIZE_EXPIRED
#define INCLUDE_LOGGING_HARD_LIFE_SIZE_EXPIRED
#endif /* INCLUDE_LOGGING_IPSEC */
#define UNDEFINED_SA ((u_char)0)
#define UNDEFINED_IP NULL
#define UNDEFINED_PORT 0X7FFFFFFF                         /*INT_MAX*/ /*TRACKSPR #99463: including limits.h causes doulbe decl'n of INT_MAX in target/h/wrn\cci\gmp-impl.h*/
#define UNDEFINED_PROTOCOL TRANSPORT_PROTOCOL_ANY
#define UNDEFINED_SPI 0
#define UNDEFINED_SUITE_HANDLE 0xFFFFFFFF                           /*UINT_MAX*/ /*TRACKSPR #99463: including limits.h causes dougle decl'n of UINT_MAX in target/h/wrn/gmp-impl.h*/
/* The IPSEC_LOG_EVENT_ID enum identifies the event Id (for example, SPD Packet
 * discard) associated with an auditable (logged) event.
 */

typedef enum IPSEC_LOG_EVENT_ID
    {
    UNKNOWN_EVENT = 0,
    SPD_PACKET_DISCARD,
    IKE_PACKET_DISCARD,
    IPSEC_PACKET_DISCARD,
    IKE_PHASE_I_FAIL,
    IKE_PHASE_I_SUCCESS,
    IKE_PHASE_II_FAIL,
    IKE_PHASE_II_SUCCESS,
    SEQUENCE_NUM_OVERFLOW,
    SOFT_LIFE_TIME_EXPIRED,
    HARD_LIFE_TIME_EXPIRED,
    SOFT_LIFE_SIZE_EXPIRED,
    HARD_LIFE_SIZE_EXPIRED,
    IPSEC_LOG_EVENT_ID_MAX = HARD_LIFE_SIZE_EXPIRED
    }

IPSEC_LOG_EVENT_ID;

/* The IPSEC_LOG_REASON enum identifies a second level of detail for an event
 * Id. For example, an IKE_PHASE_I_FAIL (IKE Phase 1 Failure) can occur due
 * to a variety of reasons (preshared key mismatch, algorithm mismatches, etc.
 */

typedef enum IPSEC_LOG_REASON
    {
    UNDEFINED_REASON = 0,
    NO_POLICY, /* SPD events         */
    NO_PROTECTION_SUITE,
    EXPLICIT_DISCARD_POLICY,
    NEGOTIATION_TIMEOUT,
    AUTHENTICATION_ERROR,
    ENCRYPTION_ERROR,
    DECRYPTION_ERROR,
    INVALID_SPI_ERROR,
    REPLAY_ERROR,
    POLICY_ERROR,
    PSK_MISMATCH, /* IKE Phase I events */
    HASH_ALG_MISMATCH,
    ENCRYPTION_ALG_MISMATCH,
    AUTHENTICATION_METHOD_MISMATCH,
    PROPOSAL_MISMATCH, /* Phase II events    */
    NULL_ESP,
    NULL_AH,
    ISAKMP_PACKET_ERROR,
    IPSEC_INTERNAL_ERROR,
    IKE_INTERNAL_ERROR,
    IPSEC_LOG_REASON_MAX = IKE_INTERNAL_ERROR
    }

IPSEC_LOG_REASON;

/* Logger callback function pointer */
typedef void (*FP_IPSEC_LOGGER_GLOBAL_CALLBACK) (u_char sa_family,
BYTE* srcIPAddr,
BYTE* dstIPAddr,
unsigned int srcPort,
unsigned int dstPort,
int protocol, /*enum IP_TRANSPORT_PROTOCOL. don't want to include ip_message_if.h which includes rwos files*/
int spi,
void* eventData,
IPSEC_LOG_EVENT_ID eventId,
IPSEC_LOG_REASON reasonId);

extern STATUS ipsecLoggerInit
    (
    void
    );

extern BOOL ipsecLoggerIsEnabled
    (
    void
    );

extern STATUS ipsecLoggerCallbackSet
    (
    FP_IPSEC_LOGGER_GLOBAL_CALLBACK funcptr,
    BOOL enable
    );

extern FP_IPSEC_LOGGER_GLOBAL_CALLBACK ipsecLoggerCallbackGet
    (
    void
    );

extern STATUS ipsecLoggerCallbackClear
    (
    void
    );

extern STATUS ipsecLoggerLogEventEnabledSet
    (
    IPSEC_LOG_EVENT_ID eventId,
    BOOL enable
    );

extern BOOL ipsecLoggerIsLogEventEnabled
    (
    IPSEC_LOG_EVENT_ID eventId
    );
#ifdef __cplusplus
}
#endif
#endif /* __IPSEC_LOGGER_H__*/
