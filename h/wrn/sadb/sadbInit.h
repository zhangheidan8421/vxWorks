/* sadbInit.h - header file for sadb_init.c */
/* Copyright 2000-2005 Wind River Systems, Inc. */
/************************************************************************/

#if !defined (__SADB_INIT_H__)
#define __SADB_INIT_H__

#ifdef __cplusplus
extern "C"{
#endif

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
  #pragma align 1 /* tell gcc960 not to optimize alignments */
#endif          /* CPU_FAMILY==I960 */

/* sadb configuration parameters */

typedef struct sadbcfgparams
    {
    unsigned int task_priority;
    } SADB_CFG_PARAMS;

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
  #pragma align 0 /* turn off alignment requirement */
#endif /* CPU_FAMILY==I960 */

#ifndef SADB_TASK_PRIORITY
  #define SADB_TASK_PRIORITY 100
#endif /* SADB_TASK_PRIORITY */

/* function declarations */
extern STATUS sadbInit
    (
    char         * sadbCfg,
    unsigned int   task_priority
    );

STATUS usrSadbInit
    (
    void
    );

#ifdef __cplusplus
}
#endif

#endif /* __SADB_INIT_H__ */
