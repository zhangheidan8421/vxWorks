/* ospfv3Lib.h - OSPFv3 library */

/* Copyright 2002-2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01l,05may04,dsk fix build issue with clarinet.h
01k,06apr04,ram OSPF v2/v3 coexistance changes
01j,23oct03,agi updated include file paths
01i,25sep03,ram Second general modifications submission
01h,30jun03,agi changed inlude file names
01g,29jul03,ram Added sockets, processing, and external route changes
01f,23jun03,agi added RWOS removal changes
                updated copyright
01e,23jan03,htm added changes for IPv6 and some cleanup
01d,23dec02,agi removed include of ospfv3SearchUtils.h
01c,18dec02,agi corrected path to rtm.h
01b,14nov02,agi first pass at clean-up
01a,23oct02,agi written
*/

/*
DESCRIPTION

Include this library to get all the necessary OSPFv3 header files.
*/

#ifndef __INCospfv3Initializeh
#define __INCospfv3Initializeh

int ospfv3Initialize (void);

#endif /* __INCospfv3Initializeh */

