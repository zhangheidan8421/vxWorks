/* ospfv3Mapi.h - public interface for managing OSPFv3 */

/* Copyright 2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
 * Modification history
 * --------------------
 01h,07mar05,xli correct parameter data type in ospfv3_mApi_ipv6Addr2OctetString
 01g,12may04,xli Modified the ospfv3MApiInit function prototype
 01f,06may04,ram Fix for AVL memory corruption
 01e,29apr04,ram Fix mapi AVL, compile, and warnings
 01d,22apr04,ram AVL & Memory migration to shared library
 01c,29may03,xli  included Xinzhi's changes from integration with Xinzhi
                  mapi counters are unsigned int instead of int
 01b,05jan03,xli  Modified the data structures to reflect OSPFv3
                  MIB definition and OSPFv3 Mapi layer method routine
                  declaration
 01a,14novo2,xli  Initial file creation.
 */

/*
DESCRIPTION:
This file contains the definitions for constants, data structures and
function prototypes used to implement the OSPFv3 Management Interface.

*/

#ifndef __INCospfv3Mapi_h
#define __INCospfv3Mapi_h

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* initially turn off all the debug switch */
IMPORT int ospfv3MapiDebug;
IMPORT int ospfv3MapiErrorDebug;

/* mib api debug macro. */
#define mApiOspfv3DbgEnabled TRUE
#ifdef mApiOspfv3DbgEnabled
#define mApiOspfv3Printf( _x )  { if (ospfv3MapiDebug) {printf _x;} }
#else
#define mApiOspfv3Printf( _x )
#endif /* mApiOspfv3DbgEnabled */

/* #undef mApiOspfv3ErrorEnabled */
#define mApiOspfv3ErrorEnabled TRUE
#ifdef mApiOspfv3ErrorEnabled
#define mApiOspfv3Error( _x )  { if (ospfv3MapiErrorDebug) {printf _x;} }
#else
#define mApiOspfv3Error( _x )
#endif /* mApiOspfv3ErrorEnabled */


/* the following only apply if not using the Tornado Project Facility. If using
 * Tornado Project Facility, select INCLUDE_OSPFV3_SHOW (for now, always include
 * show routines since Project Facility is not configured to support this yet)
 */

/* OSPFv3 specific MIB API application defined error */
#define OSPFV3_MAPI_INVALID_INSTANCE       1000


/* pointer to function for MIB API GET request */
typedef STATUS (* OSPFV3_MAPI_GET_RTN)( mApiRequest_t *pRequest, mApiReqType_t reqType );

/* pointer to function for MIB API SET request */
typedef STATUS (* OSPFV3_MAPI_SET_RTN)( mApiRequest_t *pRequest, mApiReqType_t reqType );

/* pointer to function for processing the SET request */
typedef STATUS (* OSPFV3_MAPI_HELPER_SET_RTN)( mApiRequest_t *pRequest, mApiReqType_t reqType );

/* pointer to function for dynamically configuring the OSPFv3 protocol */
typedef STATUS (* OSPFV3_MAPI_HELPER_CONFIG_RTN)( mApiRequest_t *pRequest );

/* pointer to function for processing the GET/GET NEXT request */

typedef STATUS (* OSPFV3_MAPI_HELPER_GET_RTN)( void *pRow, mApiRequest_t *pRequest,
                                        mApiObject_t *pReqObject );

/* pointer to function for setting the next index value for the GET NEXT request */
typedef void (* OSPFV3_MAPI_INDEX_SET_RTN)( void *pRow, mApiRequest_t *pRequest );

/* pointer to function that is used by AVL to compare two instances */
typedef int (* OSPFV3_MAPI_AVL_COMPARE_RTN)(AVL_NODE * node, void* key);


/* system dependenet configuration parameters. These parameters determine the number
 * of concurrent instances that can be configured for each table in the OSPFv3-MIB.
 */
#define DEFAULT_OSPFV3_MAPI_AREA_MAX          8
#define DEFAULT_OSPFV3_MAPI_AS_LSDB_MAX       1024
#define DEFAULT_OSPFV3_MAPI_AREA_LSDB_MAX     1024
#define DEFAULT_OSPFV3_MAPI_LINK_LSDB_MAX     1024
#define DEFAULT_OSPFV3_MAPI_HOST_MAX          4
#define DEFAULT_OSPFV3_MAPI_INTF_MAX           16
#define DEFAULT_OSPFV3_MAPI_VIRT_INTF_MAX      8
#define DEFAULT_OSPFV3_MAPI_NBR_MAX            24
#define DEFAULT_OSPFV3_MAPI_NBMA_NBR_MAX       24
#define DEFAULT_OSPFV3_MAPI_VIRT_NBR_MAX       24
#define DEFAULT_OSPFV3_MAPI_AREA_AG_MAX        12


/* the following define is for unnumbered interface support in
 * wrnOspfv3IfTable. Set the maximum number of instances allowed
 * to be one entry less than the maximum number of instances in
 * ospfv3IfTable. This is done this way because it may be possible
 * that all but one instances in the ospfv3IfTable are created as
 * unnumbered interface. In that case there must be at least one
 * interface with IP Address so that those unnumbered interfaces
 * can be initialized.
 */
#define DEFAULT_OSPFV3_MAPI_WRN_INTF_MAX  DEFAULT_OSPFV3_MAPI_INTF_MAX-1

/* OSPFV3_REQUEST_BUFFER_CNT is the number of request buffers that
 * will be allocated. These request buffers are used by ospfv3 to
 * provide operational status and stastitical update to the MIB API
 * asynchronous. Although this number is arbitrary picked and can be
 * tuned according to the system needs, the number of buffers allocated
 * must be able to handle the maximum number of external link state
 * advertisement updates from ospfv3 during the flooding.
 */
#define DEFAULT_OSPFV3_REQUEST_BUFFER_CNT  DEFAULT_OSPFV3_MAPI_AS_LSDB_MAX


/* define the OSPFv3 RFC that is currently using */
typedef enum {
    Eospfv3ProtoType_rfc2740 = 1  /* RFC2740 OSPFv3 */
} ospfv3ProtoType_t;


typedef struct mApiOspfv3AvlClass
{
    LIST          ospfv3FreePoolList;  /* free memory list */
    AVL_NODE     *pOspfv3AvlRoot;      /* AVL tree root */
    int           memReqFailedCnt;     /* memory request from pool failed */
    OSPFV3_MAPI_AVL_COMPARE_RTN
                  ospfv3AvlCompareRtn; /* AVL compare routine */
} mApiOspfv3AvlClass_t;


/***************************************************************************
 * Management Interface Data Structure.
 */
typedef struct mApiOspfv3Class
{
  /* OSPFv3 Protocol Information */
  ospfv3ProtoType_t ospfv3RfcProto;      /* version of OSPFv3 RFC that is using */
  BOOL              ospfv3ProtoInit;     /* if true, OSPFv3 protocol is up */
  BOOL              ospfv3OpaqueEnable;  /* if true, support Opaque LSA */
  BOOL              ospfv3DbOverflow;    /* if true, support OSPFv3 database overflow */
  BOOL              ospfv3NssaEnabled;   /* if true, support NSSA */
  BOOL              ospfv3BackboneExist; /* if true, OSPFv3 backbone area exists */
  BOOL              ospfv3BackboneActive; /* if true, one more interfaces are active */

  SEM_ID         semMapiMutex;   /* mutex semaphore */
  int            semTimeout;     /* semaphore wait time, used by ospfv3ToMapi task */
  BOOL           rebootRouter;   /* restart router if true, destroyed MIB API */
  BOOL           dynamicConfig;  /* true if dynamic configuration is required */
  BOOL           dynamicDelete;  /* true if dynamic deletion from nvram required */

  /* system dependent callback functions for saving/retrieving ospfv3 configuration
   * to/from nvram
   */
  OSPFV3_NVRAM_SAVE_FUNCPTR     nvramSaveRtn;     /* to save configuration to nvram */
  OSPFV3_NVRAM_DELETE_FUNCPTR   nvramDelRtn;      /* to delete a table row from nvram */
  OSPFV3_NVRAM_RETRIEVE_FUNCPTR nvramRetrieveRtn; /* to retrieve configuration from nvram */

  /* system dependent configuration parameters for standard OSPFv3-MIB */
  int mApiOspfv3MaxArea;     /* max number of instances for ospfv3AreaTable */
  int mApiOspfv3MaxAsLsdb;   /* max number of instances for ospfv3AsLsdbTable */
  int mApiOspfv3MaxAreaLsdb; /* max number of instances for ospfv3AreaLsdbTable */
  int mApiOspfv3MaxLinkLsdb; /* max number of instances for ospfv3LinkLsdbTable */
  int mApiOspfv3MaxHost;     /* max number of instances for ospfv3HostTable */
  int mApiOspfv3MaxIf;       /* max number of instances for ospfv3IfTable  */
  int mApiOspfv3MaxVirtIf;   /* max number of instances for ospfv3VirtIfTable */
  int mApiOspfv3MaxNbr;      /* max number of instances for ospfv3NbrTable */
  int mApiOspfv3MaxNBMANbr;  /* max number of instances for ospfv3NBMANbrTable */
  int mApiOspfv3MaxVirtNbr;  /* max numbr of instances for ospfv3VirtNbrTable */
  int mApiOspfv3MaxAreaAg;   /* max number of instances for ospfv3AreaAggregateTable */

  /* current system capacities */
  int mApiOspfv3CurrAreaCnt;     /* current number of instances in ospfv3AreaTable */
  int mApiOspfv3CurrAsLsdbCnt;   /* current number of instances in ospfv3AsLsdbTable */
  int mApiOspfv3CurrAreaLsdbCnt; /* current number of instances in ospfv3AreaLsdbTable */
  int mApiOspfv3CurrLinkLsdbCnt; /* current number of instances in ospfv3LinkLsdbTable */
  int mApiOspfv3CurrHostCnt;     /* current number of instances in ospfv3HostTable */
  int mApiOspfv3CurrIfCnt;       /* current number of instances in ospfv3IfTable */
  int mApiOspfv3CurrVirtIfCnt;   /* current number of isntances in ospfv3VirtIfTable */
  int mApiOspfv3CurrNbrCnt;      /* current number of instances in ospfv3NbrTable */
  int mApiOspfv3CurrNBMANbrCnt;  /* current number of instances in ospfv3NBMANbrTable */
  int mApiOspfv3CurrVirtNbrCnt;  /* current number of instances in ospfv3VirtNbrTable */
  int mApiOspfv3CurrAreaAgCnt;   /* current number of instances in ospfv3AreaAggregateTable */

  /* interanl operational erros statistics due to max capacity reached */
  int mApiOspfv3AreaMaxReached;     /* max capacility for ospfv3AreaTable reached */
  int mApiOspfv3AsLsdbMaxReached;   /* max capacility for ospfv3AsLsdbTable reached */
  int mApiOspfv3AreaLsdbMaxReached; /* max capacility for ospfv3AreaLsdbTable reached */
  int mApiOspfv3LinkLsdbMaxReached; /* max capacility for ospfv3LinkLsdbTable reached */
  int mApiOspfv3HostMaxReached;     /* max capacility for ospfv3HostTable reached */
  int mApiOspfv3IfMaxReached;       /* max capacility for ospfv3IfTable reached */
  int mApiOspfv3VirtIfMaxReached;   /* max capacility for ospfv3VirtIfTable reached */
  int mApiOspfv3NbrMaxReached;      /* max capacility for ospfv3NbrTable reached */
  int mApiOspfv3NBMANbrMaxReached;  /* max capacility for ospfv3NBMANbrTable reached */
  int mApiOspfv3VirtNbrMaxReached;  /* max capacility for ospfv3VirtNbrTable reached */
  int mApiOspfv3AreaAgMaxReached;   /* max capacility for ospfv3AreaAggregateTable reached */

  /* internal operational error statistics due to reasons other than max capacity
   * problem (resulted in instance not being created)
   */
  unsigned int mApiOspfv3AreaFailedCnt;     /* number of creation failure for ospfv3AreaTable */
  unsigned int mApiOspfv3AsLsdbFailedCnt;   /* number of creation failure for ospfv3AsLsdbTable */
  unsigned int mApiOspfv3AreaLsdbFailedCnt; /* number of creation failure for ospfv3AreaLsdbTable */
  unsigned int mApiOspfv3LinkLsdbFailedCnt; /* number of creation failure for ospfv3LinkLsdbTable */
  unsigned int mApiOspfv3HostFailedCnt;     /* number of creation failure for ospfv3HostTable */
  unsigned int mApiOspfv3IfFailedCnt;       /* number of creation failure for ospfv3IfTable */
  unsigned int mApiOspfv3VirtIfFailedCnt;   /* number of creation failure for ospfv3VirtIfTable */
  unsigned int mApiOspfv3NbrFailedCnt;      /* number of creation failure for ospfv3NbrTable */
  unsigned int mApiOspfv3NBMANbrFailedCnt;  /* number of creation failure for ospfv3NBMANbrTable */
  unsigned int mApiOspfv3VirtNbrFailedCnt;  /* number of creation failure for ospfv3VirtNbrTable */
  unsigned int mApiOspfv3AreaAgFailedCnt;   /* num of creation failure for ospfv3AreaAggregateTable */

  /* OSPFv3-MIB data strctures */
  void *pMapiOspfv3GenGroup;   /* ospfv3 general group scalar objects */

  mApiOspfv3AvlClass_t  ospfv3AreaAvl;     /* AVL tree for ospfv3AreaTable */
  mApiOspfv3AvlClass_t  ospfv3AsLsdbAvl;   /* AVL tree for ospfv3AsLsdbTable */
  mApiOspfv3AvlClass_t  ospfv3AreaLsdbAvl; /* AVL tree for ospfv3AreaLsdbTable */
  mApiOspfv3AvlClass_t  ospfv3LinkLsdbAvl; /* AVL tree for ospfv3LinkLsdbTable */
  mApiOspfv3AvlClass_t  ospfv3HostAvl;     /* AVL tree for ospfv3HostTable */
  mApiOspfv3AvlClass_t  ospfv3IfAvl;       /* AVL tree for ospfv3IfTable */
  mApiOspfv3AvlClass_t  ospfv3VirtIfAvl;   /* AVL tree for ospfv3VirtIfTable */
  mApiOspfv3AvlClass_t  ospfv3NbrAvl;      /* AVL tree for ospfv3NbrTable */
  mApiOspfv3AvlClass_t  ospfv3NbmaNbrAvl;   /* AVL tree for ospfv3NBMANbrTable */
  mApiOspfv3AvlClass_t  ospfv3VirtNbrAvl;  /* AVL tree for ospfv3VirtNbrTable */
  mApiOspfv3AvlClass_t  ospfv3AreaAgAvl;   /* AVL tree for ospfv3AreaAggregateTable */

  /* rowStatus library handlers */
  rsParams_t   *pMapiOspfv3AreaRs;     /* ospfv3AreaTable Row Status handler */
  rsParams_t   *pMapiOspfv3HostRs;     /* ospfv3HostTable Row Status handler */
  rsParams_t   *pMapiOspfv3IfRs;       /* ospfv3IfTable Row Status handler */
  rsParams_t   *pMapiOspfv3VirtIfRs;   /* ospfv3VirtIfTable Row Status handler */
  rsParams_t   *pMapiOspfv3NBMANbrRs;  /* ospfv3NBMANbrTable Row Status handler */
  rsParams_t   *pMapiOspfv3AreaAgRs;   /* ospfv3AreaAggregateTable Row Status handler */

  ulong_t       currObjInProcess;       /* current varbind to process */
  mApiObject_t  *pMapiCurrObj;          /* current varbind to process */
  mApiReqType_t currMapiReqType;        /* current mApi request type to process */

  void *pMapiWrnOspfv3GenGroup;   /* wrn ospfv3 general group scalar objects */

  /* number of active area. An area is said to be active if there is at least one
   * operational interfaces belonging to the area. If the value is greater than
   * one, the router is an area border router
   */
  ushort_t  numActiveArea;

  /* SNMP TEST/COMMIT/UNDO/COMPLETE counters, for state transition monitoring */
  unsigned int mApiTestReqCnt;           /* number of SNMP TEST requests processed */
  unsigned int mApiCommitReqCnt;         /* number of SNMP COMMIT requests processed */
  unsigned int mApiUndoReqCnt;           /* number of SNMP UNDO requests processed */
  unsigned int mApiCompleteReqCnt;       /* number of SNMP COMMIT succeed, for debugging */
  unsigned int mApiBogusReqCnt;          /* number of incorrect SNMP requests, for debugging */
  unsigned int mApiTestReqFailedCnt;     /* number of SNMP TEST failed, for debugging */
  unsigned int mApiCommitReqFailedCnt;   /* number of SNMP COMMIT failed, for debugging */
  unsigned int mApiNvmSetReqFailedCnt;   /* number of MAPI NVM SET failed, for debugging */
  unsigned int mApiGetReqFailedCnt;      /* number of SNMP GET failed, for debugging */
  unsigned int mApiGetNextReqFailedCnt;  /* number of SNMP GET_NEXT failed, for debugging */
  unsigned int mApiDynConfigReqFailedCnt; /* number of dynamic reconfig failed, for debugging */

} mApiOspfv3Class_t;

/* global variables */
mApiOspfv3Class_t *thisOspfv3Mapi;    /* the OSPFV3 mib api object */

/********************************************************************************
 * Management Interface Public Macro
 */
#define OSPFV3_MIB_BUFLEN_CHECK(pRequest, pObject, actualLen) \
       if ( ((pObject)->valueLen < actualLen) || \
            ((pObject)->pValueBuf == NULL) ) \
       { \
         (pObject)->valueLen = actualLen; \
         (pObject)->exception = MAPI_BUFFER_TOO_SHORT; \
         (pRequest)->errorObjIndex = (ushort_t)(pObject)->oidPrefixEnum; \
         (pRequest)->error = (int)0; \
         return OK; \
       }

#define OSPFV3_MIB_ERROR_SET(pRequest, mApiError, oidPrefixEnum) \
       (pRequest)->error = (int)mApiError; \
       (pRequest)->errorObjIndex = (ushort_t)oidPrefixEnum;

#define OSPFV3_MIB_EXCEPTION_SET( pObject, mApiException ) \
       (pObject)->exception = mApiException;

/******************************************************************************
* Management Interface Public Function Prototypes
*/

/* OSPFv3 MIB API init/destroy routines */

IMPORT STATUS ospfv3MapiInit     ( void );
IMPORT STATUS ospfv3MapiDestroy  ( void );
IMPORT BOOL   ospfv3MapiIsInited ( void );


/* prototype for retrieving an instance of columnar object from the OSPFv3-MIB */

/* ospfv3GeneralGroup GET/GET_NEXT and TEST/COMMIT routines */

IMPORT STATUS ospfv3MapiGetGenGroup(mApiRequest_t *pRequest, mApiReqType_t reqType);

IMPORT STATUS ospfv3MapiSetGenGroup(mApiRequest_t *pRequest, mApiReqType_t reqType);

/* ospfv3AreaTable GET/GET_NEXT and TEST/COMMIT routines */

IMPORT STATUS ospfv3MapiGetArea(mApiRequest_t *pRequest, mApiReqType_t reqType);

IMPORT STATUS ospfv3MapiSetArea(mApiRequest_t *pRequest, mApiReqType_t reqType);

/* ospfv3AsLsdbTable GET/GET_NEXT routine */

IMPORT STATUS ospfv3MapiGetAsLsdb(mApiRequest_t *pRequest, mApiReqType_t reqType);

/* ospfv3AreaLsdbTable GET/GET/_NEXT routine */

IMPORT STATUS ospfv3MapiGetAreaLsdb(mApiRequest_t *pRequest, mApiReqType_t reqType);

/* ospfv3LinkLsdbTable GET/GET_NEXT routine */

IMPORT STATUS ospfv3MapiGetLinkLsdb(mApiRequest_t *pRequest, mApiReqType_t reqType);

/* ospfv3IfTable GET/GET_NEXT and TEST/COMMIT routines */

IMPORT STATUS ospfv3MapiGetIf(mApiRequest_t *pRequest, mApiReqType_t reqType);

IMPORT STATUS ospfv3MapiSetIf( mApiRequest_t *pRequest, mApiReqType_t reqType );

/* ospfv3NbrTable GET/GET_NEXT */

IMPORT STATUS ospfv3MapiGetNbr(mApiRequest_t *pRequest, mApiReqType_t reqType);


/* misc conversions routines */

IMPORT STATUS ospfv3_rs2mApi_errorGet( void *pObjInfo, int rsError );

IMPORT STATUS ospfv3_rs2mApi_reqTypeGet( int rsRequest, mApiReqType_t *mApiReqType);

IMPORT void ospfv3_mApi_ipAddr2OctetString( ulong_t ipAddr, uchar_t *octetString );

IMPORT void ospfv3_mApi_ipv6Addr2OctetString( struct in6_addr ipv6Addr, u_int8_t *octetString);

IMPORT ulong_t ospfv3_mApi_octetString2IpAddr( uchar_t *octetString );

IMPORT void ospfv3_mApi_ipAddr2IpIndex( ulong_t ulIpAddr, ulong_t *pIndex );

IMPORT void ospfv3_mApi_ipv6Addr2Index( struct in6_addr* ipv6Addr, ulong_t *pIndex );

IMPORT ulong_t ospfv3_mApi_ipIndex2IpAddr( ulong_t *pIndex );

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCospfv3Mapi_h */
