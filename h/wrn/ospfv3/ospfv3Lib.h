/* ospfv3Lib.h - OSPFv3 library */

/* Copyright 2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01t,07mar05,xli added sockLib.h 
01s,17feb05,xli include m2Ipv6Lib.h
01r,16feb05,xli include sys/sysctl.h, m2Lib.h and remove adv_net.h, routeEnhLib.h
01q,03feb05,xli remove clarinet.h
01p,10dec04,tlu OSPF stack decoupling (interface)
01o,29nov04,tlu OSPF stack decoupling (header files)
01n,16sep04,dsk fix for trace B0189, allow build with VIRTUAL_STACK defined
01m,10jun04,dsk spcify full path to ospfCommonLib.h for build from Project facility
01l,05may04,dsk fix build issue with clarinet.h
01k,06apr04,ram OSPF v2/v3 coexistance changes
01j,23oct03,agi updated include file paths
01i,25sep03,ram Second general modifications submission
01h,30jun03,agi changed inlude file names
01g,29jul03,ram Added sockets, processing, and external route changes
01f,23jun03,agi added RWOS removal changes
                updated copyright
01e,23jan03,htm added changes for IPv6 and some cleanup
01d,23dec02,agi removed include of ospfv3SearchUtils.h
01c,18dec02,agi corrected path to rtm.h
01b,14nov02,agi first pass at clean-up
01a,23oct02,agi written
*/

/*
DESCRIPTION

Include this library to get all the necessary OSPFv3 header files.
*/

#ifndef __INCospfv3Libh
#define __INCospfv3Libh

#if !defined (__VxWORKS__)
#define __VxWORKS__
#endif /* __VxWORKS__ */

#if !defined (__RTM__)
#define __RTM__
#endif /* __RTM__ */

#if !defined (__BIG_ENUMS__)
#define __BIG_ENUMS__
#endif /* __BIG_ENUMS__ */

#include <vxWorks.h>
#include <selectLib.h>
#include <sockLib.h>
#include <sysLib.h>
#include <private/selectLibP.h> 
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include <ioLib.h>
#include <avlLib.h>
#include <m2Lib.h>
#include <m2Ipv6Lib.h>

#include <time.h>
#include <sys/socket.h>
#include <net/if.h>
#include <net/if_var.h>
#include <netinet/in.h>
#include <netinet/in_var.h>
#include <netLib.h>
#include <net/route.h>
#include <netinet6/ip6.h>
#include <netinet6/ip6_var.h>

/* Rabih */
/*#include <ip6protosw.h>*/

#include <ip6Lib.h>
#include <net/socketvar.h>
#include <tickLib.h>
#include <errno.h>
#include <taskLib.h>
#include <sys/sysctl.h> 

#if defined (__OSPFV3_FTP__)
#include <ftpLib.h>
#include <ramDrv.h>
#include <dosFsLib.h>
#endif /*__OSPFV3_FTP__*/

#include <wrn/ospfCommon/ospfCommonLib.h>

#if defined (__OSPFV3_MAPI__)
#include   "ospfCommonMibApi.h"
#include   "ospfv3MapiHelperUpdate.h"
#endif /*__OSPFV3_MAPI__*/

/* fix for trace B0189 */     
#if 0
#if defined (VIRTUAL_STACK) /* Mistral added for Synth compatibility */
#include <netinet/ip_var.h>
#include <netinet/vsIp.h>
#include <netinet/vsLib.h>
#include <netinet/vsSock.h>
#endif /* VIRTUAL_STACK */
#endif

#include "ospfv3Constants.h"
#include "ospfv3Enums.h"
#include "ospfv3IpStructures.h"
#include "ospfv3Structures.h"
#include "ospfv3Globals.h"
#include "ospfv3Prototypes.h"
#include "ospfv3Memory.h"

#define ROUNDUP(a) \
        ((a) > 0 ? (1 + (((a) - 1) | (sizeof(long) - 1))) : sizeof(long))
#define ADVANCE(x, n) (x += ROUNDUP((n)->sa_len))

#endif /* __INCospfv3Libh */

