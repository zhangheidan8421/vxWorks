/* ospfv3MapiHelperUpdate.h  - helper routines for OSPFv3 MIB API */

/* Copyright 2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/* 
modification history
--------------------
01e,25jun04,xli added the ospfv3ToMapiReceive( void ) prototype 
                added global variable ospfv3_mapi_task_id
01d,28oct03,agi removed OSPFV3TOMAPI_TASK_PRIORITY
01d,28oct03,ram Modifications for traffic, aging, LSAs, & cleanup
01c,02jul03,agi Fixed compiler warning - comma at end of enumerator list
                Fixed copyright
01b,20jan03,xli Add OSPFv3 specific update request types
                valueBuf[] usage explanation and MibHelperUpdate
                routine declaration 
01a,07nov02,xli Initial file creation.
*/

/*
DESCRIPTION:

This file contains the functional prototypes for OSPFv3 MIB helper routines. 
*/

#ifndef __INCospfv3MibHelperUpdate_h
#define __INCospfv3MibHelperUpdate_h

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

int ospfv3_mapi_task_id;

/* this is the function pointer pointed to a system dependent save routine for saving
 * the given object to the nvram.
 */
typedef STATUS (* OSPFV3_NVRAM_SAVE_FUNCPTR)
              ( 
              ulong_t  *pInstance,      /* pointer to OID instance array */
              ushort_t  instanceLen,    /* number of sub-ids in instance */
              ulong_t   oidPrefixEnum,  /* enum value for object OID Prefix */
              void     *pValueBuf,      /* pointer to value buffer */
              ulong_t   valueLen        /* value byte length */
              );

/* this is the function pointer pointed to a system dependent delete routine for 
 * deleting a table row from nvram.
 */
typedef STATUS (* OSPFV3_NVRAM_DELETE_FUNCPTR) 
              ( 
              ulong_t  *pInstance,     /* pointer to OID instance array */
              ushort_t  instanceLen    /* number of sub-ids in instance */
              );

/* this is the function pointer pointed to a system dependent retrieve routine for
 * retrieving all prevously saved OSPFv3 configuration from nvram. The retrieve routine
 * must invoke the MIB API for read-create objects in each ospfv3 table with the 
 * mApiRequest_t set to MAPI_NVM_SET request type so that MIB API can repopulate its 
 * management database at system startup.
 */

typedef STATUS (* OSPFV3_NVRAM_RETRIEVE_FUNCPTR) (void );

/* define the configuration type uses by the underlying OSPFv3 protocol */
typedef enum {

    /* the following enumeration types are used by OSPFv3 to update the statistical
     * and operational status
     */
    ospfv3GenGroupUpdateReqType = 1,  /* update General Group read-only variables */
    ospfv3AreaUpdateReqType,          /* update an ospfv3 area instance */ 
    ospfv3AsLsdbUpdateReqType,        /* update an ospfv3 as_lsdb instance */
    ospfv3AreaLsdbUpdateReqType,      /* update an ospfv3 area_lsdb instance */
    ospfv3LinkLsdbUpdateReqType,      /* update an ospfv3 link_lsdb instance */ 
    ospfv3HostUpdateReqType,          /* update an ospfv3 host instance */ 
    ospfv3IfUpdateReqType,            /* update an ospfv3 interface instance */
    ospfv3VirtIfUpdateReqType,        /* update an ospfv3 virtual interface instance */
    ospfv3NbrUpdateReqType,           /* update an ospfv3 neighbor instance */
    ospfv3NbmaNbrUpdateReqType,       /* update an ospfv3 Nbma neighbor instance */
    ospfv3VirtNbrUpdateReqType,       /* update an ospfv3 virtual neighbor instance */
    ospfv3AreaAggregateUpdateReqType, /* update an ospfv3 Area Aggregate instance */

    /* the following enumeration types are used by OSPFv3 to create new instance of link
     * state database 
     */
    ospfv3AsLsdbCreateReqType,        /* create an instance of as_lsdb */
    ospfv3AreaLsdbCreateReqType,      /* create an instance of area_lsdb */
    ospfv3LinkLsdbCreateReqType,      /* create an instance of link_lsdb */

    /* the following enumeration types are used by OSPFv3 to delete an instance of a
     * particular table from MIB API
     */
    ospfv3AsLsdbDeleteReqType,    /* delete an ospfv3 as_lsdb instance */
    ospfv3AreaLsdbDeleteReqType,  /* delete an ospfv3 area_lsdb instance */
    ospfv3LinkLsdbDeleteReqType,  /* delete an ospfv3 link_lsdb instance */
    ospfv3NbrDeleteReqType,       /* delete an ospfv3 nbr instance */
    ospfv3NbmaNbrDeleteReqType,   /* delete an ospfv3 Nbma nbr instance */
    ospfv3VirtNbrDeleteReqType,   /* delete an ospfv3 virtual neighbor instance */

    /* the following enumeration types are used by OSPFv3 to create an instance of
     * ospfv3 area, host,interface, or virtual interface in MIB API during startup. 
     * These enumeration types are for backward compatibility to support the static 
     * configuration methodology. It shall not be used by any aother applications. 
     */

    ospfv3GenGroupCreateReqType,     /* setup ospfv3 General Group scalar objects */
    ospfv3AreaCreateReqType,         /* create an instance of ospfv3 area */
    ospfv3HostCreateReqType,         /* create an instance of ospfv3 host interface */
    ospfv3IfCreateReqType,           /* create an instance of ospfv3 interface */
    ospfv3VirtIfCreateReq,           /* create an instance of ospfv3 virtual interface */
    ospfv3AreaAggregateCreateReq,    /* create an instance of ospfv3 area aggregate */

    /* this is a special request type from ospfv3 to notify MIB API that OSPFv3 is halted */
    ospfv3ShutdownReqType,               

    ospfv3IfChangeReqType,   /* interface flag change event */
    ospfv3IfDeleteReqType    /* interface address deleted event */

    /* the following enumeration types are used by OSPFv3 to create an instance of
     * wrn-ospfv3 interface during startup. These enumeration types are for backward 
     * compatibility to support the static configuration methodology. It shall not 
     * be used by any aother applications. These enumeration types will eventually 
     * be obsolote
     */
} ospfv3ToMapiReqType_t;


/* ospfv3ToMapiReqBuf_t structure is a generic data structure used internally by 
 * OSPFv3 protocol to provide operational status and statistics updates to the 
 * MIB API. In order to maintain backward compatibility with the existing static 
 * configuration methodology, this data structure is also used by OSPFv3 protocol 
 * at init time to update the Management Database for any configuration that are 
 * statically created. This involves creating the ospfv3 area, host, interface, 
 * virtual interface, etc. instances in the Management Database. The 
 * ospfv3ToMapiReqBuf_t data structure is initialized in the ospfv3ToMapi_request() 
 * function (invoked by the OSPFv3 protocol). Using this data strcuture, the OSPFv3 
 * protocol can provide the necessary updates to the OSPFv3 MIB API asynchronously, 
 * thus, minimize the disruption to the overall OSPFv3 operation. Due to the multi-usage 
 * of the ospfv3ToMapiReqBuf_t data structure, the representation of the data structure 
 * is highly depending on the type of request (defined as ospfv3ToMapiReqType_t 
 * enumeration type). The following is the representation of the ospfv3ToMapiReqBuf_t 
 * data structure for each ospfv3ToMapiReqType_t request type:
 * 
 * ospfv3GeneralGroup (reqType = ospfv3GenGroupCreateReqType)
 * valueBuf[0]  = the value of ospfv3RouterId  read-write object
 * valueBuf[1]  = the value of ospfv3AdminStat read-write object
 * valueBuf[2]  = the value of ospfv3ASBdrRtrStatus read-write object
 * valueBuf[3]  = the value of ospfv3ExtAreaLsdbLimit read-write object
 * valueBuf[4]  = the value of ospfv3ExitOverflowInterval read-write object
 * valueBuf[5]  = the value of ospfv3DemandExtensions read-write object
 * valueBuf[6]  = the value of ospfv3TrafficEngineeringSupport read-write object
 * wrnospfv3GeneralGroup (reqType = ospfv3GenGroupCreateReqType)
 * valueBuf[7]  = the value of wrnOspfv3RedistributeDefaultRoutes read-write object
 * valueBuf[8]  = the value of wrnOspfv3RedistributeStaticRoutes read-write object
 * valueBuf[9] = the value of wrnOspfv3RedistributeRIPRoutes read-write object
 * valueBuf[10] = the value of wrnOspfv3RedistributeBGPRoutes read-write object
 *
 * ospfv3GeneralGroup (reqType = ospfv3GenGroupUpdateReqType ) 
 * valueBuf[0] = the value of ospfv3ExternLsaCount read-only object
 * valueBuf[1] = the value of ospfv3ExternLsaCksumSum read-only object
 * valueBuf[2] = the value of ospfv3OriginateNewLsas read-only object
 * valueBuf[3] = the value of ospfv3RxNewLsas read-only object
 * valueBuf[4] to valueBuf[14] = not used
 * pValueBuf = the value of ospfv3MulticastExtension read-write object 
 *

 * ospfv3AreaTable (reqType = ospfv3AreaCreateReqType):
 * valueBuf[0] = the value of ospfv3AreaId index object
 * valueBuf[1] = the value of ospfv3ImportAsExtern read-create object
 * valueBuf[2] = the value of ospfv3AreaSummary read-create object
 * valueBuf[3] = the value of ospfv3StubMetric read-create object
 * valueBuf[4] = the value of ospfv3AreaNssaTranslatorRole read-create object
 * valueBuf[5] = the value of ospfv3AreaNssaTranslatorState read-create object
 * valueBuf[6] = the value of ospfv3AreaNssaTranslatorStabilityInterval read-create object
 * valueBuf[7] to valueBuf[14] = not used
 * pValueBuf = not use
 *
 * ospfv3AreaTable (reqType = ospfv3AreaUpdateReqType)
 * valueBuf[0] = the value of ospfv3AreaId index object
 * valueBuf[1] = the value of ospfv3SpfRuns read-only object
 * valueBuf[2] = the value of ospfv3AreaBdrRtrCount read-only object
 * valueBuf[3] = the value of ospfv3AsBdrRtrCount read-only object
 * valueBuf[4] = the value of ospfv3AreaScopeLsaCount read-only object
 * valueBuf[5] = the value of ospfv3AreaScopeLsaCksumSum read-only object
 * valueBuf[6] = the value of ospfv3AreaNssaTranslatorState read-only object
 * valueBuf[7] = the value of ospfv3AreaNssaTranslatorEvents read-only object
 * valueBuf[8] to valueBuf[14] = not used
 * pValueBuf = not use
 *
 * ospfv3AsLsdbTable (reqType = ospfv3AsLsdbUpdateReqType):
 * valueBuf[0] = the value of ospfv3AsLsdbType index object
 * valueBuf[1] = the value of ospfv3AsLsdbRouterId index object
 * valueBuf[2] = the value of ospfv3AsLsdbLsid index object
 * valueBuf[3] = the value of ospfv3AsLsdbSequence read-only object
 * valueBuf[4] = the value of ospfv3AsLsdbAge read-only object
 *
 * ospfv3AsLsdbTable (reqType = ospfv3AsLsdbCreateReqType):
 * valueBuf[0] = the value of ospfv3AsLsdbType index object
 * valueBuf[1] = the value of ospfv3AsLsdbRouterId index object
 * valueBuf[2] = the value of ospfv3AsLsdbLsid index object
 * valueBuf[3] = the value of ospfv3AsLsdbSequence read-only object
 * valueBuf[4] = the value of ospfv3AsLsdbAge read-only object
 * valueBuf[5] = the value of ospfv3AsLsdbChecksum read-only object
 * valueBuf[6] = the value of AsLsdbLen (not a MIB object)
 * valueBuf[7] and valueBuf[14] = not used
 * pValueBuf = pointer to the ospfv3AsLsdbAdvertisement read-only object
 *
 * ospfv3AsLsdbTable (reqType = ospfv3AsLsdbDeleteReqType)
 * valueBuf[0] = the value of ospfv3AsLsdbType index object
 * valueBuf[1] = the value of ospfv3AsLsdbRouterId index object
 * valueBuf[2] = the value of ospfv3AsLsdbLsid index object
 * valueBuf[3] and valueBuf[14] = not used
 * pValueBuf = not use

 * ospfv3AreaLsdbTable (reqType = ospfv3AreaLsdbUpdateReqType)
 * valueBuf[0] = the value of ospfv3AreaLsdbAreaId index object
 * valueBuf[1] = the value of ospfv3AreaLsdbType index object
 * valueBuf[2] = the value of ospfv3AreaLsdbRouterId index object
 * valueBuf[3] = the value of ospfv3AreaLsdbLsid index object
 * valueBuf[4] = the value of ospfv3AreaLsdbSequence read-only object
 * valueBuf[5] = the value of ospfv3AreaLsdbAge read-only object
 *
 * ospfv3AreaLsdbTable (reqType = ospfv3AreaLsdbCreateReqType)
 * valueBuf[0] = the value of ospfv3AreaLsdbAreaId index object
 * valueBuf[1] = the value of ospfv3AreaLsdbType index object
 * valueBuf[2] = the value of ospfv3AreaLsdbRouterId index object
 * valueBuf[3] = the value of ospfv3AreaLsdbLsid index object
 * valueBuf[4] = the value of ospfv3AreaLsdbSequence read-only object
 * valueBuf[5] = the value of ospfv3AreaLsdbAge read-only object
 * valueBuf[6] = the value of ospfv3AreaLsdbChecksum read-only object
 * valueBuf[7] = the value of areaLsdbLen (not a MIB object)
 * valueBuf[8] and valueBuf[14] = not used
 * pValueBuf = pointer to the ospfv3AreaLsdbAdvertisement read-only object
 *
 * ospfv3AreaLsdbTable (reqType = ospfv3AreaLsdbDeleteReqType)
 * valueBuf[0] = the value of ospfv3AreaLsdbType index object
 * valueBuf[1] = the value of ospfv3AreaLsdbRouterId index object
 * valueBuf[2] = the value of ospfv3AreaLsdbLsid index object
 * valueBuf[3] and valueBuf[14] = not used
 * pValueBuf = not use
 
 * ospfv3LinkLsdbTable (reqType = ospfv3LinkLsdbUpdateReqType)
 * valueBuf[0] = the value of ospfv3LinkLsdbIfIndex index object
 * valueBuf[1] = the value of ospfv3LinkLsdbType index object
 * valueBuf[2] = the value of ospfv3LinkLsdbRouterId index object
 * valueBuf[3] = the value of ospfv3LsdbLinkLsid index object
 * valueBuf[4] = the value of ospfv3LinkLsdbSequence read-only object
 * valueBuf[5] = the value of ospfv3LinkLsdbAge read-only object
 *
 * ospfv3LinkLsdbTable (reqType = ospfv3LinkLsdbCreateReqType)
 * valueBuf[0] = the value of ospfv3LinkLsdbIfIndex index object
 * valueBuf[1] = the value of ospfv3LinkLsdbType index object
 * valueBuf[2] = the value of ospfv3LinkLsdbRouterId index object
 * valueBuf[3] = the value of ospfv3LinkLsdbLsid index object
 * valueBuf[4] = the value of ospfv3LinkLsdbSequence read-only object
 * valueBuf[5] = the value of ospfv3LinkLsdbAge read-only object
 * valueBuf[6] = the value of ospfv3LinkLsdbChecksum read-only object
 * valueBuf[7] = the value of linkLsdbLen (not a MIB object)
 * valueBuf[8] and valueBuf[14] = not used
 * pValueBuf = pointer to the ospfv3LinkLsdbAdvertisement read-only object
 *
 * ospfv3LinkLsdbTable (reqType = ospfv3LinkLsdbDeleteReqType)
 * valueBuf[0] = the value of ospfv3LinkLsdbIfIndex index object
 * valueBuf[1] = the value of ospfv3LinkLsdbType index object
 * valueBuf[2] = the value of ospfv3LinkLsdbRouterId index object
 * valueBuf[3] = the value of ospfv3LinkLsdbLsid index object
 * valueBuf[4] and valueBuf[14] = not used
 * pValueBuf = not use

 * ospfv3HostTable (reqType = ospfv3HostCreateReqType):
 * valueBuf[0] = the value of ospfv3HostAddressType index object
 * valueBuf[1] = the value of ospfv3HostAddress index object
 * valueBuf[2] = the value of ospfv3HostMetric read-create object
 * valueBuf[3] = the value of ospfv3HostStatus read-create object
 * valueBuf[4] = the value of ospfv3HostAreaId read-only object
 * valueBuf[5] = interface netmask as defined by ospfv3 protocol
 * valueBuf[6] = interface mtu as defined by ospfv3 protocol
 * valueBuf[7] to valueBuf[14] = not used
 * pValueBuf = not use
 *
 * ospfv3IfTable (reqType = ospfv3IfUpdateReqType):
 * valueBuf[0] = the value of ospfv3IfIndex index object
 * valueBuf[1] = the value of ospfv3IfState read-only object
 * valueBuf[2] = the value of ospfv3IfDesignatedRouter read-only object
 * valueBuf[3] = the value of ospfv3IfBackupDesignatedRouter read-only object
 * valueBuf[4] = the value of ospfv3IfEvents read-only object
 * valueBuf[5] = the value of ospfv3IfLinkScopeLsaCount read-only object
 * valueBuf[6] = the value of ospfv3IfLinkLsaCksumSum read-only object
 * valueBuf[7] to valueBuf[14] = not used
 * pValueBuf = not use
 *
 * ospfv3IfTable (reqType = ospfv3IfCreateReqType)
 * valueBuf[0] = the value of ospfv3IfIndex index object
 * valieBuf[1] = the value of ospfv3IfAreaId read-create object
 * valueBuf[2] = the value of ospfv3IfType read-create object
 * valueBuf[3] = the value of ospfv3IfAdminStat read-create object
 * valueBuf[4] = the value of ospfv3IfRtrPriority read-create object
 * valueBuf[5] = the value of ospfv3IfTransitDelay read-create object
 * valueBuf[6] = the value of ospfv3IfRetransInterval read-create object
 * valueBuf[7] = the value of ospfv3IfHelloInterval read-create object
 * valueBuf[8] = the value of ospfv3IfRtrDeadInterval read-create object
 * valueBuf[9] = the value of ospfv3IfPollInterval read-create object
 * valueBuf[10] = the value of ospfv3IfStatus read-create object
 * valueBuf[11] = the value of ospfv3IfMulticastForwarding read-create object
 * valueBuf[12] = the value of ospfv3IfDemand read-create object
 * valueBuf[13] = the value of ospfv3IfMetricValue read-create object
 * valueBuf[14] = the value of ospfv3IfInstId read-create object
 * valueBuf[15] = the value of the interface mtu size
 * pValueBuf = the value of the interface subnet mask
 *
 * ospfv3VirtIfTable (reqType = ospfv3VirtIfUpdateReqType):
 * valueBuf[0] = the value of ospfv3VirtIfAreaId index object
 * valueBuf[1] = the value of ospfv3VirtIfNeighbor index object
 * valueBuf[2] = the value of ospfv3VirtIfState read-only object
 * valueBuf[3] = the value of ospfv3VirtIfEvents read-only object
 * valueBuf[4] = the value of ospfv3VirtIfLinkScopeLsaCount read-only object
 * valueBuf[5] = the value of ospfv3VirtIfLinkLsaCksumSum read-only object
 * valueBuf[6] to valueBuf[14] = not used
 * pValueBuf = not use
 *
 * ospfv3VirtIfTable (reqType = ospfv3VirtIfCreateReq)
 * valueBuf[0] = the value of ospfv3VirtIfAreaId index object
 * valueBuf[1] = the value of ospfv3VirtIfNeighbor index object
 * valieBuf[2] = the value of ospfv3VirtIfTransitDelay read-create object
 * valueBuf[3] = the value of ospfv3VirtIfRetransInterval read-create object
 * valueBuf[4] = the value of ospfv3VirtIfHelloInterval read-create object
 * valueBuf[5] = the value of ospfv3VirtIfRtrDeadInterval read-create object
 * valueBuf[7] = the value of ospfv3VirtIfStatus read-create object
 * valueBuf[8] to valueBuf[14] = not used
 * pValueBuf = not use
 * 
 * ospfv3NbrTable (reqType = ospfv3NbrUpdateReqType):
 * valueBuf[0] = the value of ospfv3NbrIfIndex index object
 * valueBuf[1] = the value of ospfv3NbrRtrId read-only object
 * valueBuf[2] = the value of ospfv3NbrAddressType read-only objecta
 * pValueBuf   = the value of ospfv3NbrAddress read-only objecta
 * valueBuf[3] = the value of ospfv3NbrOptions read-only object
 * valueBuf[4] = the value of ospfv3NbrPriority read-only object
 * valueBuf[5] = the value of ospfv3NbrState read-only object
 * valueBuf[6] = the value of ospfv3NbrEvents read-only object
 * valueBuf[7] = the value of ospfv3NbrLsRetransQLen read-only object
 * valueBuf[8] = the value of ospfv3NbrHelloSuppressed read-only object
 * valueBuf[9] = the value of ospfv3NbrIfId read-only object
 * valueBuf[10] and valueBuf[14] = not used
 * pValueBuf = the value of the ospfv3NbrAddress 
 *
 * ospfv3NbrTable (reqType = ospfv3NbrDeleteReqType)
 * valueBuf[0] = the value of ospfv3NbrIfIndex index object
 * valueBuf[1] = the value of ospfv3NbrRtrId index object
 * valueBuf[2] to valueBuf[14] = not used
 * pValueBuf = not use
 *
 * ospfv3NbmaNbrTable (reqType = ospfv3NbmaNbrUpdateReqType):
 * valueBuf[0] = the value of ospfv3NbmaNbrIfIndex index object
 * valueBuf[1] = the value of ospfv3NbmaNbrAddressType index object
 * valueBuf[2] = the value of ospfv3NbmaNbrRtrId read-only object
 * valueBuf[3] = the value of ospfv3NbmaNbrState read-only object
 * valueBuf[4] and valueBuf[14] = not used
 * pValueBuf = the value of the ospfv3NbmaNbrAddress
 *
 * ospfv3NbrTable (reqType = ospfv3NbrDeleteReqType)
 * valueBuf[0] = the value of ospfv3NbrIfIndex index object
 * valueBuf[1] = the value of ospfv3NbrAddressType index object
 * valueBuf[2] to valueBuf[14] = not used
 * pValueBuf = the value of the ospfv3NbmaNbrAddress
 *
 * ospfv3VirtNbrTable (reqType = ospfv3VirtNbrUpdateReqType):
 * valueBuf[0] = the value of ospfv3VirtNbrArea index object
 * valueBuf[1] = the value of ospfv3VirtNbrRtrId index object
 * valueBuf[2] = the value of ospfv3VirtNbIfIndex read-only object
 * valueBuf[3] = the value of ospfv3VirtNbrAddressType read-only object
 * valueBuf[4] = the value of ospfv3VirtNbrOptions read-only object
 * valueBuf[5] = the value of ospfv3VirtNbrState read-only object
 * valueBuf[6] = the value of ospfv3VirtNbrEvents read-only object
 * valueBuf[7] = the value of ospfv3VirtNbrLSRetransQLen read-only object
 * valueBuf[8] = the value of ospfv3VirtNbrHelloSuppressed read-only object
 * valueBuf[9] = the value of ospfv3VirtNbrIfId read-only object
 * valueBuf[10] and valueBuf[14] = not used
 * pValueBuf = the value of the ospfv3VirtNbrAddress
 *
 * ospfv3VirtNbrTable (reqType = ospfv3VirtNbrDeleteReqType)
 * valueBuf[0] = the value of ospfv3VirtNbrArea index object
 * valueBuf[1] = the value of ospfv3VirtNbrRtrId index object
 * valueBuf[2] and valueBuf[14] = not used
 * pValueBuf = not use
 *
 * ospfv3AreaAggregateTable (reqType = ospfv3AreaAggregateCreateReq )
 * valueBuf[0] = the value of ospfv3AreaAggregateAreaID index object
 * valueBuf[1] = the value of ospfv3AreaAggregateAreaLsdbType index object
 * valueBuf[2] = the value of ospfv3AreaAggregatePrefixType  index object
 * valueBuf[3] = the value of ospfv3AreaAggregatePrefixLength index object
 * valueBuf[4] = the value of ospfv3AreaAggregateEffect read-create object
 * valueBuf[5] = the value of ospfv3AreaAggregateStatus read-create  object
 * valueBuf[6] and valueBuf[14] = not used
 * pValueBuf = pointer to the ospfv3AreaAggregatePrefix
 * 
 * shutdown notification from ospfv3 (reqType = ospfv3ShutdownReqType)
 * valueBuf[0] = EmApiOspfv3_disabled
 * valueBuf[1] to valueBuf[14] = not use
 * pValueBuf = not use
 *
 * interface flag change notification from ospfv3 (reqType = ospfv3IfChangeReqType)
 * valueBuf[0] = the value of ospfv3IfIndex index object
 * valueBuf[1] = interface index value (from struct ifnet)
 * valueBuf[2] = interface flags (from struct ifnet)
 * valueBuf[3] to valueBuf[14] = not use
 * pValueBuf = not use
 *
 * interface address deleted notification from ospfv3 (reqType = ospfv3IfDeleteReqType)
 * valueBuf[0] = the value of ospfv3IfIndex index object
 * valueBuf[1] = interface index value (from struct ifnet)
 * valueBuf[2] to valueBuf[14] = not use
 * pValueBuf = not use
 * 
 */
typedef struct ospfv3ToMapiReqBuf
{
    NODE                   node;         /* linked list node */
    ospfv3ToMapiReqType_t  reqType;      /* request type issued by ospfv3 protocol */
    ulong_t                valueBuf[15]; /* value buffer */
    char                   *pValueBuf;   /* pointer to data larger than sizeof(ulong_t) */
} ospfv3ToMapiReqBuf_t;

typedef struct ospfv3ToMapiSysCtl
{
    ushort_t if_index;   /* interface index (from struct ifnet) */
    void     *pCtlData;  /* control data, specific to the request message */
} ospfv3ToMapiSysCtl_t;

void ospfv3ToMapiReceive( void );

/* prototypes for functions used by MIB API to dynamically reconfigure OSPFv3 */
IMPORT STATUS mApi2Ospfv3_configGenGroup( void *pGenParams );
IMPORT STATUS mApi2Ospfv3_configArea( mApiRequest_t *pRequest );
IMPORT STATUS mApi2Ospfv3_configIf( mApiRequest_t *pRequest );

IMPORT STATUS mApi2Ospfv3_deleteArea( void *pRow );
IMPORT STATUS mApi2Ospfv3_deleteIf( void *pRow );

/* prototypes for functions used by OSPFv3 for operational status statistic updates */
IMPORT void ospfv3ToMapi_request( void *pObjInfo, ospfv3ToMapiReqType_t reqType );

/* prototypes for functions used by OSPFv3 for registration and de-registration */
IMPORT STATUS ospfv3ToMapi_proto_register( void );
IMPORT void ospfv3ToMapi_proto_deregister( void );

/* prototype for function used by OSPFv3 to retrieve ospfv3 configuration from MIB API */
IMPORT STATUS ospfv3ToMapi_query_config( BOOL *readStaticConfig );

/* misc prototypes used by MIB API only */
IMPORT STATUS ospfv3ToMapi_init( int numOspfv3ReqBufCnt );

/* Show routine */
IMPORT STATUS ospfv3ToMapi_show( void );

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __INCospfv3MibHelperUpdate_h */
