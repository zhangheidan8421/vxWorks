/* ospfv3Globals.h - OSPFv3 global variables header file */

/* Copyright 2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01n,08jun05,dsk changed declaration of global semaphores and global structures
01m,12apr04,ram Memory and AVL modifications
01l,08apr04,ram OSPF v2/v3 coexistance compile
01k,15mar04,dsk Mods according to OSPFv3 ANVL conformance testing
01j,17oct03,ram Modifications for memory corruption, traffic, & debug output
01i,25sep03,ram Second general modifications submission
01h,29jul03,ram Added sockets, processing, and external route changes
01g,29may03,dsk added definition for OSPF global semaphore to replace rwos
                added definition of socket for route updates from OSPF to RS
01f,18feb03,dsk ported from OSPFv2 to OSPFv3 structures
01e,24jan03,htm removed unused OSPFV3_CALLBACK_IF, Ospfv3RegistrationInfo
01d,06jan02,agi removed unused variable ospfv3_ten_seconds_counter
01c,23dec02,agi propogated TMS code merge for SPR#84284
01b,14nov02,agi first pass at clean-up
01a,23oct02,agi written
*/

#ifndef __INCospfv3Globalsh
#define __INCospfv3Globalsh

#ifdef __cplusplus
extern "C" {
#endif

typedef int (*FP_OSPFV3_RECEIVE) (register struct mbuf *message_block);

typedef int (*FP_OSPFV3_SEND) (register struct mbuf *message_block);

#if !defined (__OSPFV3_VIRTUAL_STACK__)

/* the global variable "ospfv3" is defined in file ospfv3System.c */
extern OSPFV3_CLASS ospfv3;
/* Initialize special OSPFv3 addresses as per RFC 2740, section A1, page 46:
AllSPFRouters = "FF02::5", AllDRRouters = "FF02::6"
*/
extern struct in6_addr   ospfv3_addr6_allspf;
extern struct in6_addr   ospfv3_addr6_alldr;

extern SEM_ID ospfv3_global_mutex;
/* SPR 85050 -- Begin */
extern SEM_ID ospfv3_external_route_mutex;
/* SPR 85050 -- End */

int ospfv3RTPrototypeTaskId;

char ospfv3_configuration_text[OSPFV3_MAXIMUM_FILE_SIZE + 1];

#if defined (__OSPFV3_FTP__)
char ospfv3_configuration_data[OSPFV3_MAXIMUM_FILE_SIZE + 1];
#endif /*__OSPFV3_FTP__*/

BOOL ospfv3ReadStaticConfig;

#endif /* __OSPFV3_VIRTUAL_STACK__ */

/*RFC 1765*/
ULONG ospfv3_db_overflow_timer;

extern OSPF_NVRAM_CLASS ospfv3_nvram;

extern ULONG ospfv3MemPartId;

#ifdef __cplusplus
}
#endif

#endif /* __INCospfv3Globalsh */
