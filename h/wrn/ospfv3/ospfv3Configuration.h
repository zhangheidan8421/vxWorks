/* ospfv3Configuration.h - OSPFv3 configuration header file */

/* Copyright 2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01i,14apr04,ram Performance optimization changes
01h,06apr04,ram OSPF v2/v3 coexistance changes
01g,16dec03,ram Mods per type 3 & 4 testing
01f,10oct03,ram Modifications for LSDB, dynamic config, & NSSA cleanup
01e,25sep03,ram Second general modifications submission
01d,30jul03,ram Changes for sockets, external routes, and cleanup
01c,23jul03,agi added RWOS removal changes
                updated copyright
01c,18feb03,dsk ported from OSPFv2 to OSPFv3 structures
01b,14nov02,agi first pass at clean-up
01a,23oct02,agi written
*/

#ifndef __INCospfv3Configurationh
#define __INCospfv3Configurationh

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __OSPF_VIRTUAL_STACK__
#include "ospf_vs_lib.h"
#endif /* __OSPF_VIRTUAL_STACK__ */

/******************************************************************************/
#ifndef __OSPFV3_VIRTUAL_STACK__
OSPF_CONFIGURATION_TABLE ospfv3_configuration_table =
{
    1,
    {
/*___________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_default_values,
        NULL,
        "OSPFV3 Use Default Values ="
    },

/*___________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_router_id,
        NULL,
        "OSPFV3 Router ID =",
    },

/* __________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_tos_capability,
        NULL,
        "OSPFV3 Type of Service Capability =",
    },
/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_asbr,
        NULL,
        "OSPFV3 Autonomous System Border Router =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_multicast,
        NULL,
        "OSPFV3 IP Multicast =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_number_of_areas,
        NULL,
        "OSPFV3 Number of Areas =",
    },


/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_area_id,
        NULL,
        "OSPFV3 Area ID =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_number_of_area_address_range,
        NULL,
        "OSPFV3 Total Number of Area Address Ranges =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_area_address_range_address,
        NULL,
        "OSPFV3 Area Address Range Address =",
    },
/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_area_address_range_length,
        NULL,
        "OSPFV3 Area Address Range Address Length =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_area_address_range_enable,
        NULL,
        "OSPFV3 Area Address Range Advertise =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_area_address_range_area_id,
        NULL,
        "OSPFV3 Area Address Range Area ID =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_area_external_routing_capability,
        NULL,
        "OSPFV3 Area External Routing Capability =",
    },
/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_area_stub_inject_default_route,
        NULL,
        "OSPFV3 Area Inject Default Route Into Stub Area =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_area_stub_cost,
        NULL,
        "OSPFV3 Area Stub Default Cost =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_number_of_ports,
        NULL,
        "OSPFV3 Number of Ports =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_port_enable,
        NULL,
        "OSPFV3 Port =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_port_index,
        NULL,
        "OSPFV3 Port Interface Index =",
    },

#ifdef __UNNUMBERED_LINK__  /* -bt- */
/* __________________________________________________________ unnumbered dest ip */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_unnumbered_dest_ip,
        NULL,
        "OSPFV3 Port Unnumbered Dest IP =",
    },
#endif  /* __UNNUMBERED_LINK__ -bt- */

/* __________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_port_nbma_neighbor_address,
        NULL,
        "OSPFV3 Port NBMA or PTMP Neighbor Address =",
    },
/* __________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_port_nbma_neighbor_id,
        NULL,
        "OSPFV3 Port NBMA or PTMP Neighbor ID =",
    },
/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_port_area_id,
        NULL,
        "OSPFV3 Port Area ID =",
    },
/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_port_type,
        NULL,
        "OSPFV3 Port Type =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_port_transit_area_id,
        NULL,
        "OSPFV3 Port Transit Area ID =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_port_source_area_id,
        NULL,
        "OSPFV3 Port Source Area ID =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_port_router_dead_interval,
        NULL,
        "OSPFV3 Port Router Dead Interval =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_port_transmit_delay,
        NULL,
        "OSPFV3 Port Transmit Delay =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_port_priority,
        NULL,
        "OSPFV3 Port Priority =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_port_cost,
        NULL,
        "OSPFV3 Port Cost =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_port_hello_interval,
        NULL,
        "OSPFV3 Port Hello Interval =",
    },

/* __________________________________________________________________________ */
{
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_port_neighbor_router_id,
        NULL,
        "OSPFV3 Port Neighbor Router ID =",
    },

/* __________________________________________________________________________ */
{
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_port_poll_interval,
        NULL,
        "OSPFV3 Port Poll Interval =",
    },

/* __________________________________________________________________________ */
{
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_port_retxmt_interval,
        NULL,
        "OSPFV3 Port Retransmit Interval =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_port_passive,
        NULL,
        "OSPFV3 Port Passive =",
    },

/* __________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_port_passive_all,
        NULL,
        "OSPFV3 Port Passive-Interface ="
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_printf,
        NULL,
        "OSPFV3 Printf =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_printf_interface,
        NULL,
        "OSPFV3 Printf Interface =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_printf_neighbor,
        NULL,
        "OSPFV3 Printf Neighbor =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_printf_memory,
        NULL,
        "OSPFV3 Printf Memory =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_printf_alarm,
        NULL,
        "OSPFV3 Printf Alarm =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_printf_snmp,
        NULL,
        "OSPFV3 Printf SNMP =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_printf_packet,
        NULL,
        "OSPFV3 Printf Packets =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_printf_routing_table,
        NULL,
        "OSPFV3 Printf Routing Table =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_printf_debug,
        NULL,
        "OSPFV3 Printf Debug =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_printf_rtm,
        NULL,
        "OSPFV3 Printf Rtm =",
    },

/* __________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_printf_prologue,
        NULL,
        "OSPFV3 Printf Prologue =",
    },

/*_____________________________________________________________________________________________________*/
{
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_printf_db_overflow,
        NULL,
        "OSPFV3 Printf DB Overflow =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_bgp_redistribute_all,
        NULL,
        "OSPFV3 Redistribute BGP =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_rip_redistribute_all,
        NULL,
        "OSPFV3 Redistribute RIP =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_static_redistribute_all,
        NULL,
        "OSPFV3 Redistribute Static =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_default_redistribute,
        NULL,
        "OSPFV3 Redistribute Default =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_bgp_subnets_to_redistribute,
        NULL,
        "OSPFV3 Redistribute BGP Subnets =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_static_subnets_to_redistribute,
        NULL,
        "OSPFV3 Redistribute Static Subnets =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_rip_subnets_to_redistribute,
        NULL,
        "OSPFV3 Redistribute RIP Subnets =",
    },

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_point_to_point_lsa_option,
        NULL,
        "OSPFV3 Point to Point Router Lsa Option =",
    },

/* __________________________________________________________________________ */
/* RFC 1765 */
#ifdef __OSPFV3_DB_OVERFLOW_SUPPORT__

    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_external_lsdb_limit,
        NULL,
        "OSPFV3 External LSDB Limit =",
    },
/*____________________________________________________________________________*/

    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_exit_overflow_interval,
        NULL,
        "OSPFV3 Exit Overflow Interval =",
    },

#endif /* __OSPFV3_DB_OVERFLOW_SUPPORT__ */

/* __________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospfv3_set_max_lsa_deleted,
        NULL,
        "OSPFV3 Maximum LSAs Deleted Per Second =",
    },

/*___________________________________________________________________________ */
    {
        NULL,
        NULL,
    "",
        (ULONG) NULL,
        (ULONG) NULL,
        (ULONG) NULL
    }

    }
};
#endif /* __OSPFV3_VIRTUAL_STACK__ */


#ifdef __cplusplus
}
#endif

#endif /* __INCospfv3Configurationh */
