/* ospfv3Memory.h - OSPFv3 memory header file */

/* Copyright 2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01b,08apr04,ram OSPF v2/v3 coexistance compile
01a,06apr04,ram Initial cration
*/

#ifndef __INCospfv3Memoryh
#define __INCospfv3Memoryh

#ifdef __cplusplus
extern "C" {
#endif

#define OSPFV3_PARTMEM_LARGE_SIZE (2048 * 1024)  /* 2M Preferred initial memory partition size                                                    * 2 Megs */
#define OSPFV3_PARTMEM_SMALL_SIZE (128 * 1024)   /* Alternate Initial memory partition size */

/* OSPF memory partition defintions */
/* Undefine older memory macros */

#if defined (table_malloc)
#undef table_malloc
#endif
#if defined (table_free)
#undef table_free
#endif


/* Define new memory macros */
#define OSPFV3_PARTMEM_CREATE             ospfMemPartCreate
#define OSPFV3_PARTMEM_DELETE             ospfMemPartDelete(ospfv3MemPartId)
#define OSPFV3_PARTMEM_EXPAND(size)       ospfMemPartExpand(ospfv3MemPartId,size)

#define OSPFV3_PARTMEM_OPTION(option)     ospfMemPartSetOptions(ospfv3MemPartId,option)
#define OSPFV3_PARTMEM_SHOW(option)       ospfMemPartShow(ospfv3MemPartId,option)

#define table_malloc(units,size)          ospfMemPartAlloc(ospfv3MemPartId,size)
#define table_free(ptr)                   ospfMemPartFree(ospfv3MemPartId,(void*)ptr)

#ifdef __cplusplus
}
#endif

#endif /* __INCospfv3Memoryh */
