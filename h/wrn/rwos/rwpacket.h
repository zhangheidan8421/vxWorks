/*
modification history
--------------------
02a,20Sep02 rks(teamf1): added rw_packet_get_ipsec_reserved_size.
01  May0902,ark		Added in fix for SPR 70535	
*/
/************************************************************************/
/*	Copyright (C) 1999 RouterWare, Inc.     							*/
/*	Unpublished - rights reserved under the Copyright Laws of the		*/
/*	United States.  Use, duplication, or disclosure by the 				*/
/*	Government is subject to restrictions as set forth in 				*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 		*/
/*	Computer Software clause at 252.227-7013.							*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach, CA	*/
/************************************************************************/
#if !defined (__RW_PACKET_INTERFACE_H__)
#define __RW_PACKET_INTERFACE_H__

typedef UINT RW_PACKET_HANDLE;
typedef UINT RW_PACKET_MBUF_HANDLE;

RW_PACKET_HANDLE rw_packet_create (UINT buffer_size, UINT reserved_header_size);

bool rw_packet_destroy (RW_PACKET_HANDLE);

BYTE* rw_packet_get_writable_header (RW_PACKET_HANDLE, UINT header_size);
BYTE* rw_packet_get_writable_trailer (RW_PACKET_HANDLE, UINT trailer_size);

bool rw_packet_extend_data_front (RW_PACKET_HANDLE, UINT extension);
bool rw_packet_reduce_data_front (RW_PACKET_HANDLE, UINT reduction);

bool rw_packet_extend_data_back (RW_PACKET_HANDLE, UINT extension);
bool rw_packet_reduce_data_back (RW_PACKET_HANDLE, UINT reduction);

BYTE* rw_packet_get_data (RW_PACKET_HANDLE);
UINT rw_packet_get_data_size (RW_PACKET_HANDLE);
UINT rw_packet_get_buffer_size (RW_PACKET_HANDLE);
UINT rw_packet_get_max_writable_header_size (RW_PACKET_HANDLE packet);

bool rw_packet_clone (RW_PACKET_HANDLE* p_new_packet_handle, RW_PACKET_HANDLE original_packet_handle);

bool rw_packet_is_built_from_mbuf (RW_PACKET_HANDLE packet);

/*******************************************************************************/
/* MBUF Methods																   */
/*******************************************************************************/
RW_PACKET_HANDLE rw_packet_create_from_mbuf(RW_PACKET_MBUF_HANDLE mbuf_handle );
RW_PACKET_MBUF_HANDLE rw_packet_extract_mbuf_and_destroy_packet(RW_PACKET_HANDLE packet);  
UINT	rw_packet_get_ipsec_reserved_size();
/*******************************************************************************/
/* Serialize  Methods	   									   			       */
/*******************************************************************************/

void rw_packet_serialize_byte_at_front (BYTE source, RW_PACKET_HANDLE packet);  
void rw_packet_serialize_ushort_at_front(USHORT source, RW_PACKET_HANDLE packet);
void rw_packet_serialize_ulong_at_front (ULONG source, RW_PACKET_HANDLE packet);

void rw_packet_serialize_uint_at_front (UINT source, UINT size_of_var, RW_PACKET_HANDLE packet);
void rw_packet_serialize_uint_at_back (UINT source, UINT size_of_var, RW_PACKET_HANDLE packet);

void rw_packet_serialize_byte_at_back (BYTE source, RW_PACKET_HANDLE packet);  
void rw_packet_serialize_ushort_at_back (USHORT source, RW_PACKET_HANDLE packet);
void rw_packet_serialize_ulong_at_back (ULONG source, RW_PACKET_HANDLE packet);
void rw_packet_serialize_bit_field (UINT size, RW_PACKET_HANDLE packet, ... );

/*******************************************************************************/
/* Deserialize Methods											  			   */
/*******************************************************************************/
BYTE rw_packet_deserialize_byte_at_front (RW_PACKET_HANDLE packet);
USHORT rw_packet_deserialize_ushort_at_front (RW_PACKET_HANDLE packet);
ULONG rw_packet_deserialize_ulong_at_front (RW_PACKET_HANDLE packet);

UINT rw_packet_deserialize_uint_at_front (UINT size_of_var, RW_PACKET_HANDLE packet);
UINT rw_packet_deserialize_uint_at_back (UINT size_of_var, RW_PACKET_HANDLE packet);

BYTE rw_packet_deserialize_byte_at_back (RW_PACKET_HANDLE packet);
USHORT rw_packet_deserialize_ushort_at_back (RW_PACKET_HANDLE packet);
ULONG rw_packet_deserialize_ulong_at_back (RW_PACKET_HANDLE packet);
void rw_packet_deserialize_bit_field (UINT size, RW_PACKET_HANDLE packet, ...);

#endif /* __RW_PACKET_INTERFACE_H__ */
