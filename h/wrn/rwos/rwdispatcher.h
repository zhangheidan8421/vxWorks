
/* Copyright 2001 Wind River Systems, Inc. */

/*
modification history
--------------------
03a,19Feb03,teamf1	added function protoype for seconds version of
			rwos_time_stamp routines for IPSEC.
02a,20nov02,ram		SPR 84312 added function prototype of system time in seconds
01  Aug1301,aos		Added RWOS show routine for version number
*/




/*
 * $Log: rwdispatcher.h,v $
 * Revision 1.5  2003/02/19 06:49:09  rajendra
 * ipsec now uses second version of rwos_time_stamp routines
 *
 * Revision 1.4  2003/01/07 07:51:54  bindu
 * 1. Merged 2nd Jan IPSEC code from WRS.
 *
 *
 * 1     2/09/00 4:44p Admin
 *
 * 2     9/15/99 5:14p Alex
 *
 * 1     8/27/99 1:00p Alex
 *
 * 1     5/20/99 11:46a Alex
 *
 * 1     4/14/99 5:10p Rajive
 * Initial vesrion of Next Gen RouterWare Source code
 *
 * 2     2/04/99 10:21p Mahesh
 * Added mutex functions and related definitions.
 *
 * 1     1/18/99 5:10p Mahesh
 *
 * 1     1/18/99 4:25p Mahesh
 *
 * 3     11/13/98 9:20p Mahesh
 *
 * 2     9/18/98 4:35p Mahesh
 * Changed C++ style comments to C style comments
 *
 * 1     9/03/98 7:38p Mahesh
 * initial check in
 */
/************************************************************************/
/*	Copyright (C) 1998 RouterWare, Inc.	 								*/
/*	Unpublished - rights reserved under the Copyright Laws of the		*/
/*	United States.  Use, duplication, or disclosure by the 				*/
/*	Government is subject to restrictions as set forth in 				*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 		*/
/*	Computer Software clause at 252.227-7013.							*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach, CA	*/
/************************************************************************/
#if !defined (__RWDISPATCHER_H__)
#define __RWDISPATCHER_H__

#define RWOS_TIMESTAMP_INFINITE_DURATION 0xffffffffL
#define RWOS_DEFAULT_MUTEX 0xffffffffL

typedef UINT RWOS_EVENT;

typedef UINT RWOS_DISPATCHER;

typedef UINT RWOS_DISPATCH_QUEUE;

typedef UINT RWOS_CRITICAL_SECTION;

typedef UINT RWOS_MUTEX;

/* Note: All time are in milliseconds. */
typedef struct RWOS_TIME_STAMP
{
	/* Do not access the following members directly */
	UINT last_imprint;

	UINT duration;
} RWOS_TIME_STAMP;

typedef void (*FPTR_DISPATCH_QUEUE_ITEM_HANDLER) (void* p_user, RW_CONTAINER_ITEM* p_item);

typedef void (*FPTR_DISPATCHER_HANDLER) (void* p_user);

typedef enum RWOS_DISPATCHER_PRIORITY
{
	RWOS_DISPATCHER_PRIORITY_LOWEST,

	RWOS_DISPATCHER_PRIORITY_LOW,

	RWOS_DISPATCHER_PRIORITY_NORMAL,

	RWOS_DISPATCHER_PRIORITY_HIGH,

	RWOS_DISPATCHER_PRIORITY_HIGHEST,

	/* ensure that the following is always the last element of this enum. */
	NUMBER_OF_DISPATCHER_PRIORITIES
} RWOS_DISPATCHER_PRIORITY;

/***************************************************************************/
RW_EXPORT RWOS_EVENT rwos_event_create (void);

RW_EXPORT bool rwos_event_set (RWOS_EVENT);

RW_EXPORT bool rwos_event_reset (RWOS_EVENT);

RW_EXPORT void rwos_event_destroy (RWOS_EVENT);

RW_EXPORT bool rwos_time_stamp_imprint (RWOS_TIME_STAMP*);

RW_EXPORT bool rwos_time_stamp_set_duration (RWOS_TIME_STAMP*, UINT duration);

RW_EXPORT UINT rwos_time_stamp_get_remaining_time (RWOS_TIME_STAMP);

RW_EXPORT bool rwos_time_stamp_imprint_second (RWOS_TIME_STAMP*);

RW_EXPORT bool rwos_time_stamp_set_duration_second (RWOS_TIME_STAMP*, UINT duration);

RW_EXPORT UINT rwos_time_stamp_get_remaining_time_second (RWOS_TIME_STAMP);

RW_EXPORT UINT rwos_time_stamp_calculate_remaining_time (RWOS_TIME_STAMP, UINT system_elapsed_time);

RW_EXPORT bool rwos_time_stamp_expire_now (RWOS_TIME_STAMP*);

/* GENERAL SYSTEM FUNCTION */
RW_EXPORT UINT rwos_get_system_elapsed_time (void); /* in milliseconds */
/* SPR 84312 -- Begin */
RW_EXPORT UINT rwos_get_system_elapsed_time_second (void); /* in seconds */
/* SPR 84312 -- End */

/* Dispatcher functions. */
RW_EXPORT RWOS_DISPATCHER rwos_dispatcher_create (RWOS_DISPATCHER_PRIORITY);

RW_EXPORT void rwos_dispatcher_destroy (RWOS_DISPATCHER);

RW_EXPORT bool rwos_dispatcher_register_user (RWOS_DISPATCHER, void* p_user);

RW_EXPORT bool rwos_dispatcher_register_event_handler (RWOS_DISPATCHER, RWOS_EVENT, FPTR_DISPATCHER_HANDLER p_event_handler);

RW_EXPORT bool rwos_dispatcher_register_idle_timer_handler (RWOS_DISPATCHER, FPTR_DISPATCHER_HANDLER p_idle_timer_handler);

RW_EXPORT bool rwos_dispatcher_set_sleep_duration (RWOS_DISPATCHER, UINT sleep_duration);

RW_EXPORT bool rwos_dispatcher_wakeup (RWOS_DISPATCHER);

/* critical section functions */
RW_EXPORT bool rwos_critical_section_create (RWOS_CRITICAL_SECTION* p_critical_section);

RW_EXPORT void rwos_critical_section_destroy (RWOS_CRITICAL_SECTION* p_critical_section);

RW_EXPORT bool rwos_critical_section_enter (RWOS_CRITICAL_SECTION* p_critical_section);

RW_EXPORT bool rwos_critical_section_leave (RWOS_CRITICAL_SECTION* p_critical_section);

/* mutex functions */
RW_EXPORT RWOS_MUTEX rwos_mutex_create (const char* p_name);

RW_EXPORT void rwos_mutex_destroy (RWOS_MUTEX mutex);

RW_EXPORT bool rwos_mutex_acquire (RWOS_MUTEX mutex, UINT max_wait_time);

RW_EXPORT bool rwos_mutex_release (RWOS_MUTEX mutex);

RW_EXPORT RWOS_MUTEX rwos_get_global_mutex (void);

/* dispatch queue functions */
RW_EXPORT RWOS_DISPATCH_QUEUE rwos_dispatch_queue_create (RWOS_DISPATCHER_PRIORITY priority);

RW_EXPORT bool rwos_dispatch_queue_register_user (RWOS_DISPATCH_QUEUE, void* p_user);

RW_EXPORT bool rwos_dispatch_queue_register_item_handler (RWOS_DISPATCH_QUEUE, FPTR_DISPATCH_QUEUE_ITEM_HANDLER);

RW_EXPORT bool rwos_dispatch_queue_register_idle_timer_handler (RWOS_DISPATCH_QUEUE, FPTR_DISPATCHER_HANDLER p_idle_timer_handler);

RW_EXPORT bool rwos_dispatcher_set_sleep_duration (RWOS_DISPATCH_QUEUE, UINT sleep_duration);

RW_EXPORT bool rwos_dispatcher_wakeup (RWOS_DISPATCH_QUEUE);

RW_EXPORT bool rwos_dispatch_queue_add_item (RWOS_DISPATCH_QUEUE, RW_CONTAINER_ITEM* p_item);

RW_EXPORT void rwos_dispatch_queue_destroy (RWOS_DISPATCH_QUEUE);

RW_EXPORT void rwosShowVersion (void);


#endif /* __RWDISPATCHER_H__ */
