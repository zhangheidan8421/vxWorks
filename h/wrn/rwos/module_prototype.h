#if !defined (_MODULE_PROTOTYPE_H_)
#define _MODULE_PROTOTYPE_H_

/***************************************************************************/
/* Function Prototypes																		*/
/***************************************************************************/
void bgp4_task_initialize (const char*p_configuration_file);

void initialize_ipsec_modules (const char* p_configuration_data);

void pns_task_initialize (const char*p_configuration_file);

void pns_task_cleanup (void);

void pptp_task_initialize (const char*p_configuration_file);

void pptp_task_cleanup (void);

void nat_task_initialize(const char* p_configuration_data);	/* nat_task_init.c */
void nat_cleanup(void);	/* nat_cleanup.c */

void l2tp_task_initialize (const char*p_configuration_file);
void l2tp_task_cleanup (void);

void radius_task_initialize (const char*p_configuration_file);

#endif /*_MODULE_PROTOTYPE_H_*/
