/* Copyright 2001 Wind River Systems, Inc. */

/*
modification history
--------------------
02  May0902,ark		Added in fix for SPR 70535	
01  Aug1301,aos		Added new RWOS heap manager interface functions
*/



/*
 * $Log:: /Tornado/target/h/NB/rwos/rwmemory.h                                                     $
 * 
 * 1     2/09/00 4:44p Admin
 * 
 * 1     8/27/99 1:00p Alex
 * 
 * 1     5/20/99 11:46a Alex
 * 
 * 1     4/14/99 5:10p Rajive
 * Initial vesrion of Next Gen RouterWare Source code
 * 
 * 1     1/18/99 5:10p Mahesh
 * 
 * 1     1/18/99 4:25p Mahesh
 * 
 * 2     11/13/98 9:20p Mahesh
 * 
 * 1     11/13/98 9:14p Mahesh
 * initial check in
 * 
 */
/************************************************************************/
/*	Copyright (C) 1998 RouterWare, Inc.	 								*/
/*	Unpublished - rights reserved under the Copyright Laws of the		*/
/*	United States.  Use, duplication, or disclosure by the 				*/
/*	Government is subject to restrictions as set forth in 				*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 		*/
/*	Computer Software clause at 252.227-7013.							*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach, CA	*/
/************************************************************************/
#if !defined (__RWMEMORY_H__)
#define __RWMEMORY_H__

/* memory management functions */
RW_EXPORT UINT rw_memory_initialize (UINT memSize);

RW_EXPORT UINT rw_memory_delete (void);

RW_EXPORT void* rw_memory_allocate (UINT size_of_object);

RW_EXPORT void rw_memory_free (void* p_object);

RW_EXPORT void* rw_memory_calloc (UINT elemNum, UINT elemSize);
#endif /* __RWMEMORY_H__ */

