/************************************************************************/
/*	Copyright (C) 1999 RouterWare, Inc.									*/
/*	Unpublished - rights reserved under the Copyright Laws of the		*/
/*	United States.  Use, duplication, or disclosure by the 				*/
/*	Government is subject to restrictions as set forth in 				*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 		*/
/*	Computer Software clause at 252.227-7013.							*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach, CA	*/
/************************************************************************/
#if !defined (__ROUTE_MANAGER_IF_H__)
#define __ROUTE_MANAGER_IF_H__

#define LOOPBACK_INTERFACE	0xffffffff

typedef int ROUTE_MANAGER_CONTAINER_ITERATOR;

typedef enum IP_ROUTE_PROTOCOL
{
	IP_ROUTE_PROTOCOL_OTHER=1,
	IP_ROUTE_PROTOCOL_LOCAL=2,
	IP_ROUTE_PROTOCOL_NETMGMT=3,
	IP_ROUTE_PROTOCOL_ICMP=4,
	IP_ROUTE_PROTOCOL_EGP=5,
	IP_ROUTE_PROTOCOL_GGP=6,
	IP_ROUTE_PROTOCOL_HELLO=7,
	IP_ROUTE_PROTOCOL_RIP=8,
	IP_ROUTE_PROTOCOL_IS_IS=9,
	IP_ROUTE_PROTOCOL_ES_IS=10,
	IP_ROUTE_PROTOCOL_CISCO_IGRP=11,
	IP_ROUTE_PROTOCOL_BBNSPFIGP=12,
	IP_ROUTE_PROTOCOL_OSPF=13,
	IP_ROUTE_PROTOCOL_BGP=14
} IP_ROUTE_PROTOCOL;

typedef struct IP_ROUTE_ENTRY
{
	IP_ADDRESS					ipRouteDest;
	IP_ADDRESS					ipRouteMask;
	IP_INTERFACE_HANDLE			ip_if_handle;
	UINT						ipRouteIfIndex;
	IP_ADDRESS					ipRouteNextHop;
	IP_ROUTE_PROTOCOL			ipRouteProto;
	UINT						ipRouteMetric1;
} IP_ROUTE_ENTRY;

/* This structure specifies the mask that is used for searching entry(s) in the Routing Table.
 * The ipRouteDest specifies that the ipRouteDest and ipRouteMask are (also) used for searching for
 * a match.
 */

typedef struct IP_ROUTE_ENTRY_MASK
{
	bool						ipRouteDestMask;
	bool						ip_if_handle;
	bool						ipRouteProto;
} IP_ROUTE_ENTRY_MASK;

/************************************************************************/

typedef struct ROUTE_MANAGER
{
	/* IP to RouteManager Interface */
	bool (*fp_route_manager_if_up) (struct ROUTE_MANAGER* p_route_manager_instance, IP_INTERFACE_HANDLE ip_if_handle);

	bool (*fp_route_manager_if_down) (struct ROUTE_MANAGER* p_route_manager_instance, IP_INTERFACE_HANDLE ip_if_handle);

	bool (*fp_route_manager_add_ip_address) (struct ROUTE_MANAGER* p_route_manager_instance, IP_INTERFACE_HANDLE ip_if_handle,
		IP_ADDRESS ip_address, IP_ADDRESS subnet_mask);

	bool (*fp_route_manager_remove_address) (struct ROUTE_MANAGER* p_route_manager_instance, IP_INTERFACE_HANDLE ip_if_handle, 
		IP_ADDRESS ip_address, IP_ADDRESS subnet_mask);

	/* Interface to add/remove entries from RouteManager */
	bool (*fp_route_manager_add_route) (struct ROUTE_MANAGER* p_route_manager_instance, IP_ROUTE_ENTRY* p_route_entry);

	ROUTE_MANAGER_CONTAINER_ITERATOR (*fp_route_manager_create_iterator) (struct ROUTE_MANAGER* p_route_manager_instance);

	bool (*fp_route_manager_table_find) (ROUTE_MANAGER_CONTAINER_ITERATOR* p_iterator_matched_item, 
		IP_ROUTE_ENTRY* p_route_entry_criteria, IP_ROUTE_ENTRY_MASK* p_route_entry_mask);

	/* This function will return the first route if this is the first time we are making a call to this function 
	 * otherwise returns the route being pointed to by this iterator.
	 * This function will either return the pointer to IP_ROUTE_ENTRY structure or return NULL.
	 */
	IP_ROUTE_ENTRY* (*fp_route_manager_get_route) (ROUTE_MANAGER_CONTAINER_ITERATOR iterator);

	/* 
	 * This function will return the iterator to the first route if this is the first time we are calling this function,
	 * otherwise it advances the iterator to the next route.
	 * This function will return true if there are more routes entries otherwise return false.
	 */
	bool (*fp_route_manager_goto_next_route) (ROUTE_MANAGER_CONTAINER_ITERATOR* p_iterator_matched_item);

	bool (*fp_route_manager_remove_route) (ROUTE_MANAGER_CONTAINER_ITERATOR* p_iterator_matched_item);

	bool (*fp_route_manager_free_iterator) (ROUTE_MANAGER_CONTAINER_ITERATOR iterator_matched_item);

	/* route and source address lookup interface */
	bool (*fp_route_manager_get_source_address) (struct ROUTE_MANAGER* p_route_manager_instance, IP_ADDRESS destination, 
		IP_ADDRESS* p_source_ip_address);

	bool (*fp_route_manager_resolve_destination) (struct ROUTE_MANAGER* p_route_manager_instance, IP_ADDRESS destination, 
		IP_INTERFACE_HANDLE* p_out_interface, IP_ADDRESS* p_source_ip_address, IP_ADDRESS* p_gateway);

} ROUTE_MANAGER;

ROUTE_MANAGER* route_manager_create_instance (void);

bool route_manager_destroy_instance (ROUTE_MANAGER* p_route_manager_instance);

#endif /* __ROUTE_MANAGER_IF_H__ */
