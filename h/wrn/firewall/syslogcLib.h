/* syslogcLib.h - syslog client interface header */

/* Copyright 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,27feb04,myz  removed syslogcBinToAscStrConvert()
01b,21nov03,svk  Fix copyright notice
01a,03sep03,myz  written.
*/

#ifndef __INCslogClientLibh
#define __INCslogClientLibh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"

/* syslog port */

#define SYSLOGC_DEFAULT_PORT   514

/* syslog facility code */

#define SYSLOGC_KERNEL_MSG             0
#define SYSLOGC_USER_LEVEL_MSG         1
#define SYSLOGC_MAIL_SYSTEM            2
#define SYSLOGC_SYSTEM_DAEMONS         3     
#define SYSLOGC_SECURITY_MSG           4     
#define SYSLOGC_MSG_FROM_SYSLOGD       5     
#define SYSLOGC_LINE_PRINTER           6     
#define SYSLOGC_NETWORK_NEWS           7     
#define SYSLOGC_UUCP_SUBSYS            8     
#define SYSLOGC_CLOCK_DAEMON           9     
#define SYSLOGC_FTP_DAEMON             11     
#define SYSLOGC_NTP_SUBSYSTEM          12     
#define SYSLOGC_LOG_AUDIT              13     
#define SYSLOGC_LOG_ALERT              14     
#define SYSLOGC_LOCAL0                 16     
#define SYSLOGC_LOCAL1                 17 
#define SYSLOGC_LOCAL2                 18 
#define SYSLOGC_LOCAL3                 19 
#define SYSLOGC_LOCAL4                 20 
#define SYSLOGC_LOCAL5                 21 
#define SYSLOGC_LOCAL6                 22 
#define SYSLOGC_LOCAL7                 23 

/* syslog severity code */

#define SYSLOGC_SEVERITY_EMERGENCY         0
#define SYSLOGC_SEVERITY_ALERT             1
#define SYSLOGC_SEVERITY_CRITICAL          2
#define SYSLOGC_SEVERITY_ERROR             3
#define SYSLOGC_SEVERITY_WARNING           4
#define SYSLOGC_SEVERITY_NOTICE            5
#define SYSLOGC_SEVERITY_INFORMATIONAL     6
#define SYSLOGC_SEVERITY_DEBUG             7

/* misc. */

#define SYSLOGC_CODE_DEFAULT      (0xFFFF)

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern int syslogcLibInit (char *);
extern void syslogcShutdown (void);
extern STATUS syslogcMdataSend(M_BLK_ID,int,char *, UINT16,ULONG);
extern STATUS syslogcBinDataSend (UINT8 *, int, char *,UINT16,ULONG);
extern STATUS syslogcStringSend (char *, UINT16,ULONG);

#else	/* __STDC__ */

extern int syslogcLibInit ();
extern void syslogcShutdown ();
extern STATUS syslogcMdataSend();
extern STATUS syslogcBinDataSend ();
extern STATUS syslogcStringSend ();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCslogClientLibh */
