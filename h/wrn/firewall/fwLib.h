/* fwLib.h - Firewall Rule Engine header */

/* Copyright 2004-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01r,02sep05,zhu  added per host rate limiting
01q,30aug05,myz  added HTTP content filter 
01p,04mar05,myz  added FW_INET6
01o,24feb05,svk  Update version
01n,13dec04,myz  added IPV6 support
01m,13sep04,svk  Add fwLogLibInit, fwLogSyslocInstall, fwLogLibInit
01l,26jul04,svk  Update version
01k,04mar04,svk  Add default state entry timeouts, prototype for
                 fwFtpExtHandle
01j,24feb04,myz  removed obsoleted APIs and added two new ones
01i,18feb04,myz  added FW_FIELD_GROUPNAME
01h,09feb04,myz  added fwRuleFieldSet/Get and fwRule(Group)Create changes
01g,30jan04,myz  changed name of public APIs, typedefs, and others after review.
01f,29jan04,myz  modified FW_RULE_ENTRY and FW_GROUP_ATTR
01e,26jan04,myz  added fwRuleLoggingIfInstall() and fwMacLoggingIfInstall()
01c,22jan04,svk  Add version number
01b,14jan04,svk  Add public function fwStringSearch
01a,08jan04,myz  created
*/


#ifndef __INCfwRuleLibh
#define __INCfwRuleLibh

#ifdef __cplusplus
extern "C" {
#endif

#include "lstLib.h"
#include "dllLib.h"
#include "endLib.h"
#include "netinet/in.h"
#include "netinet/ip.h"

#include "netinet6/in6.h"
#include "netinet6/ip6.h"

#if defined(INET6) || defined(INCLUDE_IPV6)
#define FW_INET6
#endif

/*
 * Action codes 
 *
 * Certain combinations do not make sense:
 * FW_ICMP, FW_TCP_RESET, FW_ACCEPT, FW_FRAG_REASSEMBLE, FW_USER_ACTION 
 * and FW_NEXT_RULE/(GROUP) are mutually exclusive. FW_FRAG_REASSEMBLE 
 * reassembles all fragments when the fragment flag is set in the rule. This 
 * action is only for the rules at pre-input location. FW_USER_ACTION will take
 * the return code of the user specified post processing function as the action
 * code. The FW_NEXT_RULE/(GROUP) action code will evaluate
 * the packet against the next rule or group. FW_ICMP and FW_TCP_RESET are only
 * used with the FW_REJECT action code.
 * When FW_REJECT and FW_ICMP is specified, the lower 16 bits 
 * contain the ICMP code  (upper 8 bits) and type (lower 8 bits), if any.
 *
 * FW_LOCAL and FW_FILTER are for future use.
 */

#define FW_REJECT           0x00000000  /* Reject the packet */
#define FW_ACCEPT           0x80000000  /* Accept the packet */
#define FW_LOG              0x40000000  /* Log the packet */
#define FW_ICMP             0x20000000  /* Send back an ICMP message */
#define FW_LOCAL            0x10000000  /* Force packet to be local  */
#define FW_USER_ACTION      0x00800000  /* Action code taken from usr function*/
#define FW_FRAG_REASSEMBLE  0x00400000  /* Reassemble all fragments */
#define FW_TCP_RESET        0x00200000  /* Send back an TCP RESET */
#define FW_NEXT_RULE        0x00100000  /* Evaluate next rule */
#define FW_NEXT_RULEGROUP   0x00080000  /* Evaluate next rule group */
#define FW_RETCODE          0xf0e00000  /* valid bits in return code, mask */
#define FW_ICMPTYPE         0x0000ff00  /* ICMP type to return */
#define FW_ICMPCODE         0x000000ff  /* ICMP code to return */
#define FW_FILTER           0x0f000000  /* Mask of the filter bits */

/* rule field set/get field identification defines */

#define FW_FIELD_NETIF       1   /* source and dest network interface */
#define FW_FIELD_IPADDR      2   /* source and dest IP address range */
#define FW_FIELD_HDRLEN      3   /* IP header length */
#define FW_FIELD_PROTO       4   /* protocol type  */
#define FW_FIELD_FRAG        5   /* IP fragment */ 
#define FW_FIELD_PKTSIZE     6   /* packet size range */
#define FW_FIELD_TCP         8   /* TCP parameters */
#define FW_FIELD_UDP         9   /* UDP parameters */
#define FW_FIELD_ICMP        10  /* ICMP parameters */
#define FW_FIELD_TIME        11  /* time of day range */
#define FW_FIELD_STATE       12  /* stateful inspection parameters */
#define FW_FIELD_ACTION      13  /* action code */
#define FW_FIELD_IPADDRSTR   14  /* IP address range in string format */
#define FW_FIELD_GROUPNAME   15  /* name of rule group */
#define FW_FIELD_EXTHDRS     16  /* extension header rules */
#define FW_FIELD_ICMPV6      17  /* ICMPV6 parameters */
#define FW_FIELD_FLOWLABEL   18  /* IPV6 flowlabel */
#define FW_FIELD_RATELIMIT   19  /* rate limit */
#define FW_FIELD_ALL         200 /* for internal use only */

/* Rule comparison operators, only apply to some rules */

#define FW_EQ_OP               0   /* equal operator */
#define FW_LT_OP               1   /* less than operator */
#define FW_GT_OP               2   /* greater than operator */
#define FW_NONE_OP             0   /* operation field not used */
#define FW_AND_OP              1   /* AND operator */
#define FW_OR_OP               2   /* OR operator */
#define FW_EXACT_OP            3   /* exact sequence and number operator */
#define FW_RANGE_OP            4   /* extension header ranger operator */

/* Host tracking options */

#define FW_SRC_TRK_ON 1 /* host tracking based on source IP address*/
#define FW_DST_TRK_ON 2 /* host tracking based on destination IP address*/
#define FW_ALL_TRK_OFF 0 /* no host tracking*/
#define FW_HOST_TRK_DFLT_SIZE 57 /* default host tracking table size*/ 


/* Stateful inspection operation. 
 * This operation always works on a pair of firewall rules. Typically a 
 * rule with value FW_CONN_INITIATOR and FW_CONN_STATE_ALL in connTrack
 * element creates a state tracking entry at either output, pre-output or 
 * forward location and another rule with value FW_CONN_RESPONDER and
 * FW_CONN_STATE_NEW or FW_CONN_STATE_ESTABLISHED  at pre-input, input
 * or forward location checks the state of the connection to see if it 
 * matches the state in the given rule. When the rule specified in the 
 * forward location, use the source and destination interfaces to 
 * differentiate the two rules.
 * FW_CONN_STATE_CREATE should be only used with FW_CONN_INITIATOR. 
 */

#define FW_CONN_INITIATOR  1  /* connection initiator */ 
#define FW_CONN_RESPONDER  2  /* connection responder */ 
#define FW_CONN_STATE_CREATE 1     /* create the tracking entry */
#define FW_CONN_STATE_NEW    2     /* connection is in new state */
#define FW_CONN_STATE_ESTABLISHED 4  /* connection is in established state */
#define FW_CONN_STATE_ALL   (FW_CONN_STATE_CREATE | FW_CONN_STATE_NEW | \
                             FW_CONN_STATE_ESTABLISHED)

/* State entry default timeout values (in seconds) */

#define FW_CONN_ICMP_TIMEOUT  120  /* ICMP state entry default timeout */
#define FW_CONN_UDP_TIMEOUT   120  /* UDP state entry default timeout */
#define FW_CONN_TCP_TIMEOUT   120  /* UDP state entry default timeout */
#define FW_FTP_DC_TIMEOUT     30  /* FTP data channel creation default timeout*/

/* Rule/group rearrangement operation types */

#define FW_MOVE_TO_TAIL  1    /* move the rule/group to tail */
#define FW_MOVE_TO_HEAD  2    /* move the rule/group to head */
#define FW_MOVE_AFTER    3    /* move the rule/group after the specified one */ 

/* To match all types of ICMP packet, this value INVALID_ICMP_TYPE
 * should be initialized in the icmp.type field
 */

#define FW_INVALID_ICMP_TYPE   0xFF

/* time of day defines */

#define FW_DAY_SUN   1        /* sunday */
#define FW_DAY_MON   (1 << 1) /* monday */
#define FW_DAY_TUE   (1 << 2) /* tuesday */
#define FW_DAY_WED   (1 << 3) /* wednesday */
#define FW_DAY_THU   (1 << 4) /* thursday */
#define FW_DAY_FRI   (1 << 5) /* friday */
#define FW_DAY_SAT   (1 << 6) /* saturday */
#define FW_DAY_ALL   (FW_DAY_SUN | FW_DAY_MON | FW_DAY_TUE | FW_DAY_WED | \
                         FW_DAY_THU | FW_DAY_FRI | FW_DAY_SAT)

#define FW_EVT_FUNC_NUM    3  /* max number of event function, internal use */

/* logging defines */

#define FW_LOG_TO_SYSLOG    1  /* send log messages to syslog server */
#define FW_LOG_TO_CONSOLE   2  /* send log messages to console */
#define FW_LOG_TO_NONE      0  /* don't send log messages anywhere */

/* MAC filter types, values are directly used as index, don't change */

#define FW_MAC_FILTER_RX   0
#define FW_MAC_FILTER_TX   1

/* Firewall version number */

#define FIREWALL_VERSION       "2.1"

#define FW_RULE_TYPE_IPV4  0x1
#define FW_RULE_TYPE_IPV6  0x2

#define FW_EXT_TYPE_NUM    5

/* content filter */

#define FW_URL_SPECIFIC_PATH 1   /* URL path match */
#define FW_URL_KEYWORD       2   /* URL keyword match */

/* required return code using the generic service registration mechanism */
#define FW_SVC_MATCH_DONE    1   /* match and done, don't go to next service */
#define FW_SVC_MATCH_NEXT    2   /* match and go to next service */
#define FW_SVC_NOT_MATCH     3   /* not match and go to next service */




/* typedefs */

/* The four installing locations of the rule filter for either IPV4 or IPV6.
 * Do not change the order of these enum values 
 */

typedef enum {
    FW_PREIN_LOC = 0,   
    FW_IN_LOC,          
    FW_FORW_LOC,
    FW_OUT_LOC,
#ifdef FW_INET6
    FW_PREIN_LOC_V6,
    FW_IN_LOC_V6,
    FW_FORW_LOC_V6,
    FW_OUT_LOC_V6,
#endif
    FW_END_LOC
    } FW_LOC_TYPE;

/* Individual fields of the rule entry */

/* Destination and source IP address.
 * Specify a range of the IP addresses between start and end inclusive.
 * If a single address, then start and end fields must be same.
 * If start field is 0, then the match condition is <= end field 
 * If end field is 0,   then the match condition is >= start field
 */

typedef struct {
    struct in_addr start;  /* the start IP address */
    struct in_addr end;    /* the end IP address */
    } FW_RULE_ADDR;

/* TCP/UDP port and IP packet size. 
 * Specify a range as described above for IP addresses
 */
 
typedef struct {
    UINT16 start;  
    UINT16 end;
    } FW_RULE_PORT, FW_RULE_PKTSIZE, FW_RULE_RANGE;

/* interface, only name and unit must specified. index is derived */

typedef struct {
    char name[END_NAME_MAX]; /* NULL terminated interface name string */
    UINT8  unit;             /* unit number */
    UINT8  pad0;
    UINT16 index;
    } FW_RULE_IF;

/* TCP flag rule */

typedef struct {
    UINT8 flag;    /* the TCP flag bits  */
    UINT8 mask;    /* the bits the rule wants to check */ 
    UINT16 operation;     /* AND (flag==(packet flag) & mask)  or 
                           * OR  (flag & mask & (packet flag)) operation */
    } FW_RULE_TCPFLAG;

typedef void (*FW_EVT_FUNC_PTR)(void *);

/* Specify an optional event function in the form of this structure to be called
 * when a TCP session transits to a certain state. Used for state rule. 
 * Internal use Only.
 */  

typedef struct {
    int state;  /* defined above, only TCP_SYN_SENT, TCP_ESTABLISH_STATE and
                 * TCP_END_STATE are valid  */
    FW_EVT_FUNC_PTR pEventFunc;   /* the callback function */
    void *  eventFuncArg;         /* callback function argument */
    } FW_EVENT_FUNC_ENTRY;


typedef struct {
    FW_RULE_PORT srcPort;
    FW_RULE_PORT dstPort;
    FW_RULE_TCPFLAG tcpFlag;
    } FW_RULE_TCP;

typedef struct {
    FW_RULE_PORT srcPort;
    FW_RULE_PORT dstPort;
    } FW_RULE_UDP;

typedef struct {
    UINT8 type;
    UINT8 code;
    } FW_RULE_ICMP;

/* Time of the day - specify a range. */
 
typedef struct {
    UINT32 days; /* bit0: sunday, ..., bit6: saturday, 0x7f: whole week */
    UINT32 secStart; /* start time in second, for example 8am, 8 * 60 * 60 */
    UINT32 secEnd;   /* end time in second, for example 6pm, 18 * 60 * 60 */ 
    } FW_RULE_TIMEOFDAY;

/* connection tracking */

typedef struct {
    UINT16 entity;   /* connection end identification */
    UINT16 state;    /* the connection state */
    } FW_RULE_CONN_TRACK;

/* rate limit */

typedef struct {
    UINT32 startTick; /*record the start time of each time duration in runtime*/
    UINT32 intPktNum; /* the number of intercepted packets in runtime */ 
    } FW_RATE_LIMIT_INFO;

typedef struct {
    UINT16 hostTrkMax; /* the max number of hosts for tracking */
    void * pHostTrkTbl; /* the IP track table */
    } FW_HOST_TRK_INFO;    

typedef struct {
    UINT32 limit;     /* the given packet limit */
    UINT16 duration;  /* each time duration for the packet limit, in second */
    UINT8 op;        /* operation type for the limit, great-than or less-than*/
    UINT8 trkSltor; /* select track method */
    union {
        FW_RATE_LIMIT_INFO rateInfo; /* overall rate limit information */
        FW_HOST_TRK_INFO trkInfo; /* host tracking information */
        } u; /* overall or per host rate limiting information */
    } FW_RULE_RATE_LIMIT;

#define rateStartTick    u.rateInfo.startTick
#define rateIntPktNum    u.rateInfo.intPktNum
#define trkMax     u.trkInfo.hostTrkMax
#define pTrkTbl  u.trkInfo.pHostTrkTbl

typedef struct {
    struct in_addr ipAddr; /* Source or destination IP address */
    UINT32 startTick; /*record the start time of each time duration in runtime*/
    UINT32 intPktNum; /* the number of intercepted packets in runtime */ 
    } FW_IP_TRK_ENTRY;

#ifdef FW_INET6
typedef struct {
    struct in6_addr ipV6Addr; /* Source or destination IPV6 address */
    UINT32 startTick; /*record the start time of each time duration in runtime*/
    UINT32 intPktNum; /* the number of intercepted packets in runtime */ 
    } FW_IPV6_TRK_ENTRY;
#endif
    
/* rule entry definition. */

typedef struct {
    FW_RULE_ADDR srcAddr; /* source IP address range */
    FW_RULE_ADDR dstAddr; /* dest IP address range */
    UINT8 hdrLen; /* IP header length, the 2 LSBs specify EQ, LT and GT op */
    UINT8 proto;  /* protocol type, only support TCP, UDP and ICMP types */
    UINT8 option; /* for future use */
    UINT8 frag;   /* 1 match fragment, 0 don't care */
    FW_RULE_PKTSIZE pktSize;  /* IP packet size range */
    } FW_RULE_IPV4;

#ifdef FW_INET6
typedef struct {
    struct in6_addr start;  /* the start IP address */
    struct in6_addr end;    /* the end IP address */
    } FW_RULE_V6ADDR;

typedef struct {
    UINT8 num;
    UINT8 types[FW_EXT_TYPE_NUM];
    } FW_V6EXT_TYPES;

typedef struct {
    UINT8 proto;
    UINT8 opType;      /* one of them, all of them, only them */
    union {
	FW_V6EXT_TYPES hdr;
        FW_RULE_RANGE  range;  /* number of ext header range */
        } u;
    } FW_RULE_V6EXT_HDRS;

typedef struct {
    UINT32 start;
    UINT32 end;
    } FW_RULE_V6LABEL;

typedef struct {
    FW_RULE_V6ADDR srcAddr; /* source IP address range */    
    FW_RULE_V6ADDR dstAddr; /* dest IP address range */
    FW_RULE_V6EXT_HDRS extList; /* ext header type rule */   
    FW_RULE_V6LABEL flowLabel;   
    FW_RULE_PKTSIZE pktSize;  /* IP packet size range */
    } FW_RULE_IPV6;
#endif
 
typedef struct {
    FW_RULE_IF srcIf; /* source IF rule, only for pre-input,input and forward*/
    FW_RULE_IF dstIf; /* dest IF rule, only for forward, pre-output and output*/
    union {
#ifdef FW_INET6
        FW_RULE_IPV6 ipv6;
#endif
        FW_RULE_IPV4 ipv4;
        } uip;
    union {
        FW_RULE_TCP tcp;      /* TCP header rule */
        FW_RULE_UDP udp;      /* UDP header rule */  
        FW_RULE_ICMP icmp;    /* ICMP header rule */
        } tport;              /* transport layer */
    FW_RULE_TIMEOFDAY timeOfDay;   /* time of day rule */
    FW_RULE_CONN_TRACK connTrack;  /* connection tracking  */
    FW_RULE_RATE_LIMIT rateLimit;
    UINT32 actions;  /* action to take when rule matches, such as FW_ACCEPT */
    } FW_RULE_ENTRY;

#ifdef FW_INET6
#define srcAddrRangeV6    uip.ipv6.srcAddr
#define dstAddrRangeV6    uip.ipv6.dstAddr
#define extListV6         uip.ipv6.extList
#define srcStartV6Addr8   uip.ipv6.srcAddr.start.s6_addr8
#define srcEndV6Addr8     uip.ipv6.srcAddr.end.s6_addr8
#define srcStartV6Addr    uip.ipv6.srcAddr.start
#define srcEndV6Addr      uip.ipv6.srcAddr.end
#define srcStartV6Addr32  uip.ipv6.srcAddr.start.s6_addr32
#define srcEndV6Addr32    uip.ipv6.srcAddr.end.s6_addr32
#define dstStartV6Addr8   uip.ipv6.dstAddr.start.s6_addr8
#define dstEndV6Addr8     uip.ipv6.dstAddr.end.s6_addr8
#define dstStartV6Addr32  uip.ipv6.dstAddr.start.s6_addr32
#define dstEndV6Addr32    uip.ipv6.dstAddr.end.s6_addr32
#define protoV6           uip.ipv6.extList.proto
#define pktSizeV6         uip.ipv6.pktSize
#define extListV6         uip.ipv6.extList
#define extRangeV6        extListV6.u.range
#define extHdrV6          extListV6.u.hdr
#define fLabelV6          uip.ipv6.flowLabel

#endif

#define srcStartV4Addr    uip.ipv4.srcAddr.start.s_addr
#define srcEndV4Addr      uip.ipv4.srcAddr.end.s_addr
#define srcAddrRangeV4    uip.ipv4.srcAddr
#define dstAddrRangeV4    uip.ipv4.dstAddr
#define dstStartV4Addr    uip.ipv4.dstAddr.start.s_addr
#define dstEndV4Addr      uip.ipv4.dstAddr.end.s_addr
#define hdrLenV4          uip.ipv4.hdrLen
#define protoV4           uip.ipv4.proto
#define fragV4            uip.ipv4.frag
#define pktSizeV4         uip.ipv4.pktSize

#define tcpSrcPortStart  tport.tcp.srcPort.start
#define tcpSrcPortEnd    tport.tcp.srcPort.end
#define tcpDstPortStart  tport.tcp.dstPort.start
#define tcpDstPortEnd    tport.tcp.dstPort.end
#define udpSrcPortStart  tport.udp.srcPort.start
#define udpSrcPortEnd    tport.udp.srcPort.end
#define udpDstPortStart  tport.udp.dstPort.start
#define udpDstPortEnd    tport.udp.dstPort.end

/* TCP/UDP service descriptor for registering an extension module */

typedef struct {
    UINT16 srcPort; /* source port */ 
    UINT16 dstPort; /* destination port */
    UINT8  proto;   /* protocol type: TCP or UDP */
    } FW_SERVICE_DESC;

typedef UINT32 (*FW_EXT_FUNC_PTR)(FW_LOC_TYPE loc,void * groupId, 
                           struct mbuf * pMblk, void * funcArg);

/* descriptor for registering an extension module */

typedef struct {
    FW_SERVICE_DESC service;     /* the service to match */
    FW_EXT_FUNC_PTR pUserFunc;   /* the user registered function */ 
    void *  usrFuncArg;          /* the user function argument   */
    FW_EVENT_FUNC_ENTRY evtFuncs[FW_EVT_FUNC_NUM]; /* TCP event functions, this
			is only for internal use. User should zero it out */
    } FW_EXT_HANDLING_DESC;

    
/* Rule entry node, user defined rule + extension handling + internal flag */

typedef struct {
    NODE    node;        /* link to another same type entry */
    UINT32  fieldsFlag;  /* rule entry fields set flag */
    void *  ownerId;     /* for future use */
    FW_EXT_HANDLING_DESC * pExtDesc; /* extension handling descriptor array */
    UINT16 eDescInx;        /* the current index of pExtDesc array */
    UINT16 eDescMaxEntries; /* the max number of entries of pExtDesc */
    FW_RULE_ENTRY rule;     /* the rule entry */
    } FW_RULE_ENTRY_NODE;

/* rule group attributes */

typedef struct {
    void * groupId;  /* unique value to identify this group */
    char * pGrpName; /* name of the group */
    UINT16 logLen;   /* logged len of the packet if FW_LOG specified */
    UINT16 type;     /* a bitwise value to indicate the rule group type */
    } FW_GROUP_ATTR;

/* a generic get string buffer */

typedef struct {
    char * pBuf;   /* point to the buffer to hold the string */
    int    len;    /* length of the buffer */
    } FW_STR_GET_BUF;

/* Group head entry, rule group attributes + a head rule */

typedef struct {
    NODE node;       /* link to another same type entry */
    LIST ruleList;   /* a list of rules in this group including head rule */ 
    FW_GROUP_ATTR attr;  /* rule group attributes */
    FW_RULE_ENTRY_NODE ruleEntry;   /* head rule node */
    } FW_GROUP_HEAD_ENTRY;

/* Non-Volatile storage interface function typedefs */

typedef int (*FW_NV_GET_FUNC_PTR)
    (
    char * pParamsId,    /* the parameter block ID, 0 terminated less than or
                          * equal 8 byte(including 0 character) string */
    char * pParamsBuf,   /* buffer to store the parameters */
    int    len,          /* the length of the buffer */
    int    offset        /* offset from the start of the storage */
    );

typedef int (*FW_NV_SET_FUNC_PTR)
    (
    char * pParamsId, /* the parameter block ID, 0 terminated less than or
                       * equal 8 byte(including 0 character) string */
    char * pParams,   /* parameter block for storing */
    int    len,       /* the length of the parameter block */
    int    offset     /* offset from the start of the storage */
    );

typedef int (*FW_NV_CLOSE_FUNC_PTR)
    (
    char * pParamsId    /* the parameter block ID, 0 terminated less than or
                         * equal 8 byte(including 0 character) string */
    );

/* logging interface function definition */

typedef void (*FW_LOG_FUNC_PTR)(struct mbuf *, UINT, FW_GROUP_ATTR *);

/* MAC Filter typedefs */

/* MAC interface Identification */

typedef struct {
    char name[END_NAME_MAX];  /* interface name string, NULL terminated */
    int  unit;                /* interface unit number */
    } FW_MAC_IF_ID;

/* MAC address integer format representation, used for fast comparison */

typedef struct {
    UINT32 first4;         /* first 4 bytes of MAC address */
    UINT16 last2;          /* last 2 bytes of MAC address */
    } FW_MAC_ADDR_INT_FORM;

/* Use the addr field to initialize a MAC address. The addrInternal field is
 * intended for internal use only
 */

typedef union {
    u_char               addr[6];      
    FW_MAC_ADDR_INT_FORM addrInternal; /* for internal use only */
    } FW_MAC_ADDR_TYPE;

/* MAC header rule specification
 * The mask will be used in the comparison equation as: 
 * (packet MAC address) & mask == (rule MAC address) & mask
 */

typedef struct
    {
    FW_MAC_ADDR_TYPE src;        /* source  address */
    FW_MAC_ADDR_TYPE srcMask;    /* source address mask */
    FW_MAC_ADDR_TYPE dst;        /* destination  address */
    FW_MAC_ADDR_TYPE dstMask;    /* destination address mask */
    UINT16 type;                 /* the ethernet type */
    UINT32 actions;  /* action codes (FW_REJECT, FW_ACCEPT and FW_LOG only) */
    } FW_MAC_USER_RULE;

typedef UINT32 (*FW_MAC_EXT_FUNCPTR)(UINT32 type,struct mbuf * pMblk,
                FW_MAC_USER_RULE *, void * callbackArg);

/* content filter */

/* registered filtering service function prototype */
typedef UINT32 (*FW_EXTSVC_FUNCPTR)(M_BLK_ID,void *);


/* function prototypes */

#if defined(__STDC__) || defined(__cplusplus)

extern void * fwRuleGroupCreate    (FW_LOC_TYPE,char *,UINT32);
extern void * fwRuleCreate         (void *);
extern int    fwRuleGroupDelete    (FW_LOC_TYPE, void *);
extern int    fwRuleDelete         (void *, void *);
extern int    fwRuleGroupMove      (void *, void *, int);
extern int    fwRuleMove           (void *, void *, void *, int);
extern void * fwRuleGroupIdGet     (FW_LOC_TYPE, int);
extern void * fwRuleIdGet          (void *, int);
extern int    fwRuleGroupCount     (FW_LOC_TYPE);
extern STATUS fwRuleFieldGet       (void *,UINT32,void *);
extern STATUS fwRuleFieldSet       (void *,UINT32,UINT32, ...);
extern int    fwExtHandlerInstall  (void *, FW_SERVICE_DESC *, FW_EXT_FUNC_PTR,
                                    void *, FW_EVENT_FUNC_ENTRY *);
extern UINT32 fwExtFtpHandler      (FW_LOC_TYPE, void *, struct mbuf *, 
				    void *);
extern BOOL   fwRuleFilterIsInstalled (FW_LOC_TYPE);
extern UINT32 fwRuleFilterDftActionGet (FW_LOC_TYPE);
extern STATUS fwRuleLogInstall     (FW_LOG_FUNC_PTR);

extern STATUS fwRuleFilterRemove   (FW_LOC_TYPE);
extern STATUS fwRuleFilterInstall  (FW_LOC_TYPE, UINT32,FW_EXT_FUNC_PTR,void *,
				    char *, UINT32);
extern STATUS fwRuleListDelete     (FW_LOC_TYPE);
extern int    fwNvRuleLibInit      (void);
extern STATUS fwNvRuleListSave     (FW_LOC_TYPE);

extern STATUS fwLogLibInit         (int, char *, void *, void *);
extern void   fwLogSyslogcInstall  (FUNCPTR, FUNCPTR, FUNCPTR);
extern void   fwLog                (struct mbuf *, UINT, FW_GROUP_ATTR *);

extern STATUS fwStateEntryTimeoutsSet (int,int,int);
extern void   fwStateInit          (void);
extern void   fwStateCleanup       (void);

extern STATUS fwMacFilterInstall    (int, FW_MAC_IF_ID *, UINT32,
                                     FW_MAC_EXT_FUNCPTR, void *);

extern STATUS fwMacFilterBind       (int, char *, int);
extern STATUS fwMacFilterUnbind     (int, char *, int);
extern STATUS fwMacFilterDefaultSet (int,UINT32,FW_MAC_EXT_FUNCPTR,void *);
extern UINT32 fwMacFilterRuleAdd    (int,FW_MAC_USER_RULE *,FW_MAC_EXT_FUNCPTR,
                                     void *);
extern STATUS fwMacCacheAdd         (int,UINT8 *,BOOL);
extern STATUS fwMacCacheDelete      (int,UINT8 *);
extern STATUS fwMacFilterRuleDelete (int type, UINT32);
extern void   fwMacFilterShow       (int);
extern STATUS fwMacLogInstall       (FW_LOG_FUNC_PTR);

extern void   fwNvFuncsInstall     (FW_NV_GET_FUNC_PTR, FW_NV_SET_FUNC_PTR,
				    FW_NV_CLOSE_FUNC_PTR);
extern int    fwNvParamGet         (char *, char *, int, int);
extern int    fwNvParamSet         (char *, char *, int, int);
extern int    fwNvParamClose       (char *);
extern BOOL   fwNvInstalled        (void);

extern UINT32 fwUrlFilter (M_BLK_ID, void *);
extern UINT32 fwCookieFilter (M_BLK_ID, void *);
extern UINT32 fwJavaAppletFilter (M_BLK_ID, void *);
extern UINT32 fwActiveXFilter (M_BLK_ID, void *);
extern UINT32 fwProxyFilter (M_BLK_ID, void *);
extern BOOL   fwExtSvcReg (void *,char *, FW_EXTSVC_FUNCPTR, void *, UINT32);
extern UINT32 fwExtSvcProcess (FW_LOC_TYPE, void *, struct mbuf *, void *);
extern void * fwExtSvcCreate (void);
extern BOOL   fwExtSvcDelete (void *);
extern BOOL   fwExtSvcUnreg (void *, char *, FW_EXTSVC_FUNCPTR);

extern void * fwUrlListCreate (void);
extern BOOL   fwUrlListDelete (void *);
extern BOOL   fwUrlDelete (void *, void *);
extern BOOL   fwUrlMoveAfter (void *, void *, void *);
extern void * fwUrlAdd (void *, char *, int);


#else   /* __STDC__ */

extern void * fwRuleGroupCreate    ();
extern void * fwRuleCreate         ();
extern int    fwRuleGroupDelete    ();
extern int    fwRuleDelete         ();
extern int    fwRuleGroupMove      ();
extern int    fwRuleMove           ();
extern void * fwRuleGroupIdGet     ();
extern void * fwRuleIdGet          ();
extern int    fwRuleGroupCount     ();
extern STATUS fwRuleFieldGet       ();
extern STATUS fwRuleFieldSet       ();
extern int    fwExtHandlerInstall  ();
extern UINT32 fwExtFtpHandler      ();
extern STATUS fwRuleFilterRemove   ();
extern STATUS fwRuleFilterInstall  ();
extern STATUS fwRuleListDelete     ();
extern BOOL   fwRuleFilterIsInstalled ();
extern UINT32 fwRuleFilterDftActionGet ();
extern int    fwNvRuleLibInit      ();
extern STATUS fwNvRuleListSave     ();
extern STATUS fwRuleLogInstall     ();

extern STATUS fwLogLibInit         ();
extern void   fwLogSyslogcInstall  ();
extern void   fwLog                ();

extern STATUS fwStateEntryTimeoutsSet ();
extern void   fwStateInit           ();
extern void   fwStateCleanup        ();

extern STATUS fwMacFilterInstall    ();

extern STATUS fwMacFilterBind       ();
extern STATUS fwMacFilterUnbind     ();
extern STATUS fwMacFilterDefaultSet ();
extern UINT32 fwMacFilterRuleAdd    ();
extern STATUS fwMacCacheAdd         ();
extern STATUS fwMacCacheDelete      ();
extern STATUS fwMacFilterRuleDelete ();
extern void   fwMacFilterShow       ();
extern STATUS fwMacLogInstall       ();

extern void   fwNvFuncsInstall      ();
extern int    fwNvParamGet          ();
extern int    fwNvParamSet          ();
extern int    fwNvParamClose        ();
extern BOOL   fwNvInstalled         ();

extern UINT32 fwUrlFilter ();
extern UINT32 fwCookieFilter ();
extern UINT32 fwJavaAppletFilter ();
extern UINT32 fwActiveXFilter ();
extern UINT32 fwProxyFilter ();
extern BOOL   fwExtSvcReg ();
extern UINT32 fwExtSvcProcess ();
extern void * fwExtSvcCreate ();
extern BOOL   fwExtSvcDelete ();
extern BOOL   fwExtSvcUnreg ();

extern void * fwUrlListCreate ();
extern BOOL   fwUrlListDelete ();
extern BOOL   fwUrlDelete ();
extern BOOL   fwUrlMoveAfter ();
extern void * fwUrlAdd ();

#endif  /* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif  /* __INCfwRuleLibh */
