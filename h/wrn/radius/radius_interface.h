/* radius_interface.h */

/* Declarations of RADIUS APIs										*/

/* Copyright 1984 - 2005 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
____________________
01k,22oct05,snd Changed fwd declaration of radius_ipv6_server_create()
01j,10jul05,dps Added code for IPv6 Support
01i,07jan05,snd Fix for SPR 105482, added new declaration for function
                radius_add_concatenated_attribute_to_list
01h,25jun03,snd add support for new defines for NAS-port-type for EAP
01g,13mar02,md  add support for EAP
01f,27feb02,md  add a new function declaration radius_remove_attribute
01e,16feb01,tk  add a new function declaration radius_any_server_request
01d,08feb01,tk  add a new callback error code and #define for ANY_RADIUS_SERVER
01c,26jan01,tk	ifdef out radius_get_server_index_in_mib_table() with __RADIUS_MIB__
01b,19dec00,md  added radius_get_server_index_in_mib_table()
01a,19dec00,md  merged from visual source safe
*/

/************************************************************************/
/*	Copyright (C) 1993 - 2000 RouterWare, Inc.   								*/
/*	Unpublished - rights reserved under the Copyright Laws of the			*/
/*	United States.  Use, duplication, or disclosure by the 					*/
/*	Government is subject to restrictions as set forth in 					*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 			*/
/*	Computer Software clause at 252.227-7013.										*/
/*	RouterWare, Inc., 3961 MacArthur Suite 212 Newport Beach, CA 92660	*/
/************************************************************************/
#ifndef __INCradius_interfaceh
#define __INCradius_interfaceh

typedef UINT RADIUS_SERVER_HANDLE;
typedef UINT RADIUS_REQUEST_HANDLE;
typedef UINT RADIUS_ATTRIBUTE_LIST_HANDLE;

#define ANY_RADIUS_SERVER	NULL

enum RADIUS_CODE
{
	RADIUS_ACCESS_REQUEST = 1,
	RADIUS_ACCESS_ACCEPT = 2,
	RADIUS_ACCESS_REJECT = 3,
 	RADIUS_ACCOUNTING_REQUEST = 4,
  	RADIUS_ACCOUNTING_RESPONSE = 5,
   	RADIUS_ACCESS_CHALLENGE = 11,
   	RADIUS_STATUS_SERVER = 12,
  	RADIUS_STATUS_CLIENT = 13,
   	RADIUS_RESERVED = 255
};

enum RADIUS_ERROR_CALLBACK_CODE
{
	RADIUS_ILLEGAL_ATTRIBUTE_LIST,
	RADIUS_UDP_SEND_ERROR,
	RADIUS_MAXIMUM_NUMBER_OF_OUTSTANDING_REQUEST_REACHED,
	RADIUS_RETRY_LIMIT_REACHED,
	RADIUS_INVALID_SERVER_SPECIFIED,
	RADIUS_SERVER_NOT_FOUND
};

enum RADIUS_ATTRIBUTE_TYPE
{
	RADIUS_USER_NAME = 1,
	RADIUS_USER_PASSWORD = 2,
	RADIUS_CHAP_PASSWORD = 3,
	RADIUS_NAS_IP_ADDRESS = 4,
	RADIUS_NAS_PORT = 5,
	RADIUS_SERVICE_TYPE = 6,
	RADIUS_FRAMED_PROTOCOL = 7,
	RADIUS_FRAMED_IP_ADDRESS = 8,
	RADIUS_FRAMED_IP_NETMASK = 9,
	RADIUS_FRAMED_ROUTING = 10,
	RADIUS_FILTER_ID = 11,
	RADIUS_FRAMED_MTU = 12,
	RADIUS_FRAMED_COMPRESSION = 13,
	RADIUS_LOGIN_IP_HOST = 14,
	RADIUS_LOGIN_SERVICE = 15,
	RADIUS_LOGIN_TCP_PORT = 16,
	RADIUS_UNASSIGNED_17 = 17,
	RADIUS_REPLY_MESSAGE = 18,
	RADIUS_CALLBACK_NUMBER = 19,
	RADIUS_CALLBACK_ID = 20,
	RADIUS_UNASSIGNED_21 = 21,
	RADIUS_FRAMED_ROUTE = 22,
	RADIUS_FRAMED_IPX_NETWORK = 23,
	RADIUS_STATE = 24,
	RADIUS_CLASS_ATTRIBUTE = 25,
	RADIUS_VENDOR_SPECIFIC = 26,
	RADIUS_SESSION_TIMEOUT = 27,
	RADIUS_IDLE_TIMEOUT = 28,
	RADIUS_TERMINATION_ACTION = 29,
	RADIUS_CALLED_STATION_ID = 30,
	RADIUS_CALLING_STATION_ID = 31,
	RADIUS_NAS_IDENTIFIER = 32,
	RADIUS_PROXY_STATE = 33,
	RADIUS_LOGIN_LAT_SERVICE = 34,
	RADIUS_LOGIN_LAT_NODE = 35,
	RADIUS_LOGIN_LAT_GROUP = 36,
	RADIUS_FRAMED_APPLETALK_LINK = 37,
	RADIUS_FRAMED_APPLETALK_NETWORK = 38,
	RADIUS_FRAMED_APPLETALK_ZONE = 39,
	
	/* ACCOUNTING 40 - 59*/
   	RADIUS_ACCT_STATUS_TYPE = 40,
   	RADIUS_ACCT_DELAY_TIME = 41,
   	RADIUS_ACCT_INPUT_OCTETS = 42,
   	RADIUS_ACCT_OUTPUT_OCTETS = 43,
   	RADIUS_ACCT_SESSION_ID = 44,
   	RADIUS_ACCT_AUTHENTIC = 45,
   	RADIUS_ACCT_SESSION_TIME = 46,
   	RADIUS_ACCT_INPUT_PACKETS = 47,
   	RADIUS_ACCT_OUTPUT_PACKETS = 48,
   	RADIUS_ACCT_TERMINATE_CAUSE = 49,
   	RADIUS_ACCT_MULTI_SESSION_ID = 50,
   	RADIUS_ACCT_LINK_COUNT = 51,

	RADIUS_CHAP_CHALLENGE = 60,
	RADIUS_NAS_PORT_TYPE = 61,
	RADIUS_PORT_LIMIT = 62,
	RADIUS_LOGIN_LAT_PORT = 63,
	RADIUS_TUNNEL_TYPE = 64,
	RADIUS_TUNNEL_MEDIUM_TYPE = 65,
	RADIUS_TUNNEL_CLIENT_ENDPOINT = 66,
	RADIUS_TUNNEL_SERVER_ENDPOINT = 67,
	RADIUS_ACCT_TUNNEL_CONNECTION = 68,
	RADIUS_TUNNEL_PASSWORD = 69,
#ifdef __EAP__
	RADIUS_EAP_MESSAGE = 79,
	RADIUS_MESSAGE_AUTHENTICATOR = 80,
#endif /* __EAP__ */
	RADIUS_TUNNEL_PRIVATE_GROUP_ID = 81,
	RADIUS_TUNNEL_ASSIGNMENT_ID = 82,
	RADIUS_TUNNEL_PREFERENCE = 83,
	RADIUS_ACCT_TUNNEL_PACKETS_LOST = 86,
	RADIUS_TUNNEL_CLIENT_AUTH_ID = 90,
	RADIUS_TUNNEL_SERVER_AUTH_ID = 91,

	RADIUS_NAS_IPV6_ADDRESS = 95,
	RADIUS_FRAMED_INTERFACE_ID = 96,
	RADIUS_FRAMED_IPV6_PREFIX = 97,
	RADIUS_LOGIN_IPV6_HOST = 98,
	RADIUS_FRAMED_IPV6_ROUTE = 99,
	RADIUS_FRAMED_IPV6_POOL = 100,

	MAXIMUM_RADIUS_ATTRIBUTE_TYPE
};

enum RADIUS_SERVICE_TYPES
{
	RADIUS_LOGIN_SERVICE_TYPE = 1,
	RADIUS_FRAMED_SERVICE_TYPE = 2,
	RADIUS_CALLBACK_LOGIN_SERVICE_TYPE = 3,
	RADIUS_CALLBACK_FRAMED_SERVICE_TYPE = 4,
	RADIUS_OUTBOUND_SERVICE_TYPE = 5,
	RADIUS_ADMINISTRATIVE_SERVICE_TYPE = 6,
	RADIUS_NAS_PROMPT_SERVICE_TYPE = 7,
	RADIUS_AUTNENTICATE_ONLY_SERVICE_TYPE = 8,
	RADIUS_CALLBACK_NAS_PROMPT_SERVICE_TYPE = 9,

	MAXIMUM_RADIUS_SERVICE_TYPE
};

enum RADIUS_FRAMED_PROTOCOLS
{
	RADIUS_PPP_PROTOCOL	= 1,
	RADIUS_SLIP_PROTOCOL = 2,
	RADIUS_ARAP_PROTOCOL = 3,
	RADIUS_GANDALF_PROPRIETARY_PROTOCOL	= 4,
	RADIUS_XYLOGICS_PROPRIETARY_PROTOCOL = 5,

	MAXIMUM_RADIUS_FRAMED_PROTOCOL_TYPE
};

enum RADIUS_FRAMED_ROUTING_METHODS
{
	RADIUS_NONE_ROUTING,
	RADIUS_SEND_ROUTING_PACKETS,
	RADIUS_LISTEN_FOR_ROUTING_PACKETS,
	RADIUS_SEND_AND_LISTEN,

	MAXIMUM_RADIUS_FRAMED_ROUTING_METHOD_TYPE
};

enum RADIUS_FRAMED_COMPRESSION_METHODS
{
	RADIUS_NONE_COMPRESSION,
	RADIUS_VJ_TCPIP_HEADER_COMPRESSION,
	RADIUS_IPX_HEADER_COMPRESSION,
	RADIUS_STAC_LZS_COMPRESSION,

	MAXIMUM_RADIUS_FRAMED_COMPRESSION_METHOD_TYPE
};

enum RADIUS_TERMINATION_ACTIONS
{
	RADIUS_DEFAULT_TERMINATION_ACTION,
	RADIUS_REQUEST_TERMINATION_ACTION,

	MAXIMUM_RADIUS_TERMINATION_ACTION_TYPE
};

enum RADIUS_LOGIN_SERVICE_TYPES
{
	RADIUS_TELNET_LOGIN_SERVICE,
	RADIUS_RLOGIN_LOGIN_SERVICE,
	RADIUS_TCP_CLEAR_LOGIN_SERVICE,
	RADIUS_PORTMASTER_LOGIN_SERVICE,
	RADIUS_LAT_LOGIN_SERVICE,

	MAXIMUM_RADIUS_LOGIN_SERVICE_TYPE
};

enum RADIUS_ACCOUNTING_STATUS_TYPES
{
	RADIUS_ACCOUNTING_START = 1,
	RADIUS_ACCOUNTING_STOP = 2,
	RADIUS_ACCOUNTING_ON = 7,
	RADIUS_ACCOUNTING_OFF = 8,
	RADIUS_TUNNEL_START = 9,
	RADIUS_TUNNEL_STOP = 10,
	RADIUS_TUNNEL_REJECT = 11,
	RADIUS_LINK_START = 12,
	RADIUS_LINK_STOP = 13,
	RADIUS_LINK_REJECT = 14,

	MAXIMUM_RADIUS_ACCOUNTING_STATUS_TYPE
};

enum RADIUS_ACCOUNTING_AUTHENTIC_TYPES
{
	RADIUS_ACCOUNTING_RADIUS_AUTHENTICATION = 1,
	RADIUS_ACCOUNTING_LOCAL_AUTHENTICATION = 2,
	RADIUS_ACCOUNTING_REMOTE_AUTHENTICATION = 3,

	MAXIMUM_RADIUS_ACCOUNTING_AUTHENTIC_TYPE
};
	
enum RADIUS_ACCOUNTING_TERMINATE_CAUSE
{
	RADIUS_ACCOUNTING_TERMINATION_USER_REQUEST = 1,
	RADIUS_ACCOUNTING_TERMINATION_LOST_CARRIER = 2,
	RADIUS_ACCOUNTING_TERMINATION_LOST_SERVICE = 3,
	RADIUS_ACCOUNTING_TERMINATION_IDLE_TIMEOUT = 4,
	RADIUS_ACCOUNTING_TERMINATION_SESSION_TIMEOUT = 5,
	RADIUS_ACCOUNTING_TERMINATION_ADMIN_RESET = 6,
	RADIUS_ACCOUNTING_TERMINATION_ADMIN_REBOOT = 7,
	RADIUS_ACCOUNTING_TERMINATION_PORT_ERROR = 8,
	RADIUS_ACCOUNTING_TERMINATION_NAS_ERROR = 9,
	RADIUS_ACCOUNTING_TERMINATION_NAS_REQUEST = 10,
	RADIUS_ACCOUNTING_TERMINATION_NAS_REBOOT = 11,
	RADIUS_ACCOUNTING_TERMINATION_PORT_UNNEEDED = 12,
	RADIUS_ACCOUNTING_TERMINATION_PORT_PREEMPTED = 13,
	RADIUS_ACCOUNTING_TERMINATION_PORT_SUSPENDED = 14,
	RADIUS_ACCOUNTING_TERMINATION_SERVICE_UNAVAILABLE = 15,
	RADIUS_ACCOUNTING_TERMINATION_CALLBACK = 16,
	RADIUS_ACCOUNTING_TERMINATION_USER_ERROR = 17,
	RADIUS_ACCOUNTING_TERMINATION_HOST_REQUEST = 18,

	MAXIMUM_RADIUS_ACCOUNTING_TERMINATE_CAUSE_TYPE
};

enum RADIUS_NAS_PORT_TYPES
{
	RADIUS_ASYNC_PORT_TYPE,
	RADIUS_SYNC_PORT_TYPE,
	RADIUS_ISDN_SYNC_PORT_TYPE,
	RADIUS_ISDN_ASYNCV120_PORT_TYPE,
	RADIUS_ISDN_ASYNCV110_PORT_TYPE,
	RADIUS_VIRTUAL_PORT_TYPE,

#ifdef __EAP__
	RADIUS_PIAFS_PORT_TYPE,
        RADIUS_HDLC_CLEARCHANNEL_PORT_TYPE,
        RADIUS_X25_PORT_TYPE,
        RADIUS_X75_PORT_TYPE,
        RADIUS_G3FAX_PORT_TYPE,
        RADIUS_SDSL_PORT_TYPE,
        RADIUS_ADSL_CAP_PORT_TYPE,
        RADIUS_ADSL_DMT_PORT_TYPE,
        RADIUS_IDSL_PORT_TYPE,
        RADIUS_ETHERNET_PORT_TYPE,
        RADIUS_XDSL_PORT_TYPE,
        RADIUS_CABLE_PORT_TYPE, 
        RADIUS_WIRELESS_OTHER_PORT_TYPE,
        RADIUS_WIRELESS_IEEE80211_PORT_TYPE,

#endif /* __EAP__ */

	MAXIMUM_RADIUS_NAS_PORT_TYPE
};

enum RADIUS_TUNNEL_TYPES
{
	RADIUS_TUNNEL_PPTP = 1,
	RADIUS_TUNNEL_L2F = 2,
	RADIUS_TUNNEL_L2TP = 3,
	RADIUS_TUNNEL_ATMP = 4,
	RADIUS_TUNNEL_VTP = 5,
	RADIUS_TUNNEL_AH = 6, 
	RADIUS_TUNNEL_IP_IP = 7,
	RADIUS_TUNNEL_MIN_IP_IP = 8,
	RADIUS_TUNNEL_ESP = 9,
	RADIUS_TUNNEL_GRE = 10,
	RADIUS_TUNNEL_DVS = 11,
	RADIUS_TUNNEL_IP_IP_TUNNELING = 12,

	MAXIMUM_RADIUS_TUNNEL_TYPE
};

enum RADIUS_TUNNEL_MEDIUM_TYPES
{
	RADIUS_TUNNEL_MEDIUM_IPV4 = 1,
	RADIUS_TUNNEL_MEDIUM_IPV6 = 2,
	RADIUS_TUNNEL_MEDIUM_NSAP = 3,
	RADIUS_TUNNEL_MEDIUM_HDLC = 4,
	RADIUS_TUNNEL_MEDIUM_BBN1822 = 5,
	RADIUS_TUNNEL_MEDIUM_802 = 6,
	RADIUS_TUNNEL_MEDIUM_E163 = 7,
	RADIUS_TUNNEL_MEDIUM_E164 = 8,
	RADIUS_TUNNEL_MEDIUM_F69 = 9,
	RADIUS_TUNNEL_MEDIUM_X121 = 10,
	RADIUS_TUNNEL_MEDIUM_IPX = 11,
	RADIUS_TUNNEL_MEDIUM_APPLETALK = 12,
	RADIUS_TUNNEL_MEDIUM_DECNETIV =	13,
	RADIUS_TUNNEL_MEDIUM_BANYAN_VINES = 14,
	RADIUS_TUNNEL_MEDIUM_E164_WITH_NSAP = 15,

	MAXIMUM_RADIUS_TUNNEL_MEDIUM_TYPE
};

enum RADIUS_SERVER_TYPE
{
	RADIUS_AUTHENTICATION_SERVER_TYPE,
	RADIUS_ACCOUNTING_SERVER_TYPE,
	RADIUS_AUTHENTICATION_AND_ACCOUNTING_SERVER_TYPE,
	RADIUS_TOTAL_TYPES_OF_SERVERS
};

/* following will be used by the RADIUS client to perform a call-back on the RADIUS user when a response is received from the RADIUS 
   server, or a timeout occured */

typedef struct RADIUS_REQUEST_CALLBACKS
{
	void	(*fptr_radius_normal_callback) (RADIUS_REQUEST_HANDLE radius_request_handle, 
							enum RADIUS_CODE code, RADIUS_ATTRIBUTE_LIST_HANDLE attribute_list_handle);
	void 	(*fptr_radius_error_callback) (RADIUS_REQUEST_HANDLE radius_request_handle, 
							enum RADIUS_ERROR_CALLBACK_CODE error_code);
} RADIUS_REQUEST_CALLBACKS;

	/* gets the server index based on the server handle */
#ifdef __RADIUS_MIB__
UINT radius_get_server_index_in_mib_table (RADIUS_SERVER_HANDLE server_handle);
#endif

	/* gets the handle to the first server of the type specified - returns INVALID_HANDLE if fails to do so */
RADIUS_SERVER_HANDLE radius_get_first_server (enum RADIUS_SERVER_TYPE type);

	/* gets the handle to the server next to the one the input handle currently points to - returns INVALID_HANDLE if fails to do so */
RADIUS_SERVER_HANDLE radius_get_next_server (RADIUS_SERVER_HANDLE server_handle, enum RADIUS_SERVER_TYPE type);

	/* gets the handle to the server with specified IP address, port and type - returns INVALID_HANDLE if fails to do so */
RADIUS_SERVER_HANDLE radius_find_server_with_specified_parameters (char* cp_ip_address, 
							UINT port, enum RADIUS_SERVER_TYPE type);

/* frees the server handle */
void radius_free_server_handle (RADIUS_SERVER_HANDLE handle);

/* end server access routines */


/* it is the RADIUS client user's responsibility to ensure that the server does not already exist */
RADIUS_SERVER_HANDLE radius_server_create (enum RADIUS_SERVER_TYPE type, 
							IP_ADDRESS ip_address, UINT port,
							UINT max_retransmit_count, 
							UINT retransmission_interval, 
							char* cp_secret);

/* adds a type to an existing server - the handle must point to an existing server using one of the server access routines above */
bool radius_server_add_type (RADIUS_SERVER_HANDLE server_handle, enum RADIUS_SERVER_TYPE type);

/* modifies the secret of an existing server - the handle must point to an existing server using one of the server access routines above */
bool radius_server_change_secret (RADIUS_SERVER_HANDLE server_handle, char* cp_secret);

/* it is the RADIUS client user's responsibility to ensure that the server already exists */
bool radius_server_destroy (RADIUS_SERVER_HANDLE server_handle);

/* end server editing routines*/

/* create a new attribute list*/
RADIUS_ATTRIBUTE_LIST_HANDLE radius_create_attribute_list(void);

/* adds an attribute to list*/
bool radius_add_attribute_to_list (RADIUS_ATTRIBUTE_LIST_HANDLE attribute_list_handle, 
							enum RADIUS_ATTRIBUTE_TYPE attribute_type, 
							UINT value_length, BYTE *bptr_value);

/* adds an attribute which could be possibly be concatenated to list */
bool radius_add_concatenated_attribute_to_list (RADIUS_ATTRIBUTE_LIST_HANDLE attribute_list_handle, enum RADIUS_ATTRIBUTE_TYPE attribute_type, UINT value_length, BYTE *bptr_value);

/* remove an atrribute represented by attribute_list_handle from the attribute list*/
bool radius_remove_attribute_from_list (RADIUS_ATTRIBUTE_LIST_HANDLE attribute_list_handle);

/* remove an attribute of choice from the attribute list */
bool radius_remove_attribute (RADIUS_ATTRIBUTE_LIST_HANDLE attribute_list_handle, enum RADIUS_ATTRIBUTE_TYPE attribute_type);

/* delete entire attribute list */
bool radius_delete_attribute_list (RADIUS_ATTRIBUTE_LIST_HANDLE attribute_list_handle);

/* end /attribute list editing routines*/

/* attribute list iteration routines*/
bool radius_attribute_list_goto_first_attribute (RADIUS_ATTRIBUTE_LIST_HANDLE attribute_list_handle);
bool radius_attribute_list_goto_next_attribute (RADIUS_ATTRIBUTE_LIST_HANDLE attribute_list_handle);

/* the user have to make sure that the attribute_iterator is pointing to a valid entry*/
enum RADIUS_ATTRIBUTE_TYPE radius_attribute_list_get_attribute_type 
							(RADIUS_ATTRIBUTE_LIST_HANDLE attribute_list_handle);
UINT radius_attribute_list_get_attribute_length 
							(RADIUS_ATTRIBUTE_LIST_HANDLE attribute_list_handle);
bool radius_attribute_list_get_attribute_value 
							(RADIUS_ATTRIBUTE_LIST_HANDLE attribute_list_handle, 
							BYTE* p_buffer, UINT buffer_length);

/* end attribute access routines*/

/* following will be used by the RADIUS user to make the RADIUS client send an Access Request; the server_handle should not be used by the user till a normal/error callback is made by the Client */
bool radius_server_request (RADIUS_SERVER_HANDLE server_handle, 
							RADIUS_REQUEST_HANDLE radius_request_handle, 
							RADIUS_REQUEST_CALLBACKS *p_callbacks, 
							enum RADIUS_CODE code, 
							RADIUS_ATTRIBUTE_LIST_HANDLE attribute_list_handle);

bool radius_any_server_request (RADIUS_SERVER_HANDLE server_handle, 
							RADIUS_REQUEST_HANDLE radius_request_handle, 
							RADIUS_REQUEST_CALLBACKS *p_callbacks, 
							enum RADIUS_CODE code, 
							RADIUS_ATTRIBUTE_LIST_HANDLE attribute_list_handle);

#ifdef INET6
RADIUS_SERVER_HANDLE radius_ipv6_server_create (enum RADIUS_SERVER_TYPE type, 
char * ipv6_address, UINT port, UINT max_retransmit_count, 
		UINT retransmission_interval, char* cp_secret);

RADIUS_SERVER_HANDLE radius_find_ipv6_server_with_specified_parameters (char* cp_ipv6_address, 
		UINT port, enum RADIUS_SERVER_TYPE type);

#endif

#endif /*__INCradius_interfaceh*/












