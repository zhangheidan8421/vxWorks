/* radiusLib.h */

/* Declarations ofRADIUS module entry point prototype						*/

/* Copyright 1984 - 2000 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
____________________
01a,19dec00,md  merged from visual source safe
*/


#ifndef __INCradiuslibh
#define __INCradiuslibh

STATUS radiusLibInit ();
#endif /* __INCradiuslibh */
