/* radiusCfg.h */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01a,03oct01,md  created for Tornado III
*/

#ifndef __INCradiusCfgh
#define __INCradiusCfgh

#ifdef __cplusplus
extern "C" {
#endif

void radiusCfg();

#ifdef __cplusplus
}
#endif

#endif /* __INCradiusCfgh */
