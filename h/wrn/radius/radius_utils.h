/* radius_utils.h */

/* Declarations of RADIUS utility functions.									*/

/* Copyright 1984 - 2000 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
____________________
01a,19dec00,md  merged from visual source safe
*/

#ifndef __INCradius_utilsh
#define __INCradius_utilsh

void radius_utils_load ();

bool radius_util_serialize_ulong (UINT ulong_to_be_converted, BYTE* buffer);
bool radius_util_serialize_ushort (USHORT ushort_to_be_converted, BYTE* buffer);

bool radius_util_deserialize_ulong (BYTE* buffer, ULONG *p_converted_ulong);
bool radius_util_deserialize_ushort (BYTE* buffer, USHORT *p_converted_ushort);

#endif /* __INCradius_utilsh */
