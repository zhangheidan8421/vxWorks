/************************************************************************/
/*	Copyright (C) 1999 RouterWare, Inc.     							*/
/*	Unpublished - rights reserved under the Copyright Laws of the		*/
/*	United States.  Use, duplication, or disclosure by the 				*/
/*	Government is subject to restrictions as set forth in 				*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 		*/
/*	Computer Software clause at 252.227-7013.							*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach, CA	*/
/************************************************************************/ 

/* pppctrl_if.h */

/* C Header file, use 3 space tab stops */

#ifndef __PPPCTRL_IF_H__
#define __PPPCTRL_IF_H__

#include <rwos.h>							/* bool, BYTE, UINT, and ULONG definitions */
#include <rw_driver_manager_if.h>	/* RW_PORT_BINDING_HANDLE */

typedef enum
{
	PPP_ILLEGAL_PROTOCOL_TYPE						= 0x0000,
	PPP_IP_PROTOCOL									= 0x0021,
	PPP_IPX_PROTOCOL									= 0x002b,
	PPP_APPLETALK_PROTOCOL							= 0x0029,
	PPP_VAN_JACOBSON_COMPRESSED_PROTOCOL		= 0x002d,
	PPP_VAN_JACOBSON_UNCOMPRESSED_PROTOCOL		= 0x002f,
	PPP_BRIDGING_PROTOCOL							= 0x0031,
	PPP_NETBIOS_PROTOCOL								= 0x003f,

	PPP_8021D_SPANNING_TREE_BPDU					= 0x0201,
	PPP_SOURCE_ROUTING_BPDU							= 0x0203,
	PPP_DEC_LAN_BRIDGE_100							= 0x0205,

	PPP_IPCP_PROTOCOL									= 0x8021,
	PPP_IPXCP_PROTOCOL								= 0x802b,
	PPP_ATCP_PROTOCOL									= 0x8029,
	PPP_BCP_PROTOCOL									= 0x8031,
	PPP_NBFCP_PROTOCOL								= 0x803f,
	
	PPP_LCP_PROTOCOL									= 0xc021,	
	PPP_PAP_PROTOCOL									= 0xc023,
	PPP_CHAP_PROTOCOL									= 0xc223,
	PPP_LINK_QUALITY_REPORT_PROTOCOL				= 0xc025

} PPP_PROTOCOL_TYPE;

typedef struct
{
	PPP_PROTOCOL_TYPE					protocol_type;
	RW_PACKET_HANDLE					payload;
	UINT									header_length;
	RW_PORT_BINDING_HANDLE			port_binding_handle;

} PPP_MESSAGE;

typedef enum
{
	PPP_FCS_NULL	= 1,
	PPP_FCS_16BIT	= 2,
	PPP_FCS_32BIT	= 4

} PPP_FCS_OPTION_FLAG;

typedef BYTE PPP_FCS_OPTION;

#define PPP_DEFAULT_MRU				1500				/* RFC1661 section 6.1 */
#define PPP_DEFAULT_SYNC_ACCM		0					/* RFC1662 section 7.1 */
#define PPP_DEFAULT_ASYNC_ACCM	0xffffffff		/* RFC1662 section 7.1 */
#define PPP_DEFAULT_PFC				false				/* RFC1661 section 6.5 */
#define PPP_DEFAULT_ACFC			false				/* RFC1661 section 6.6 */
#define PPP_DEFAULT_FCS				PPP_FCS_16BIT	/* RFC1570 section 2.1 */
#define PPP_DEFAULT_ASYNC			true

typedef struct PPPCTRL_CALLBACK_IF
{
	bool 	(*fp_bring_upper_layer_up) (struct PPPCTRL_CALLBACK_IF*, UINT port_number, PPP_PROTOCOL_TYPE protocol);
	void 	(*fp_bring_upper_layer_down) (struct PPPCTRL_CALLBACK_IF*, UINT port_number, PPP_PROTOCOL_TYPE protocol);

	bool 	(*fp_send) (struct PPPCTRL_CALLBACK_IF*, UINT port_number, PPP_MESSAGE* p_message);

	bool 	(*fp_set_my_ip_address) (struct PPPCTRL_CALLBACK_IF*, UINT port_number, IP_ADDRESS);

	/* Set My Data Link Settings */
	bool	(*fp_set_my_mru) (struct PPPCTRL_CALLBACK_IF*, UINT port_number, UINT value);
	bool	(*fp_set_my_accm) (struct PPPCTRL_CALLBACK_IF*, UINT port_number, ULONG value);
	bool	(*fp_set_my_pfc) (struct PPPCTRL_CALLBACK_IF*, UINT port_number, bool);
	bool	(*fp_set_my_acfc) (struct PPPCTRL_CALLBACK_IF*, UINT port_number, bool);
	bool	(*fp_set_my_fcs) (struct PPPCTRL_CALLBACK_IF*, UINT port_number, PPP_FCS_OPTION);
	bool	(*fp_set_my_async) (struct PPPCTRL_CALLBACK_IF*, UINT port_number, bool);

	/* Set Peer Data Link Settings */
	bool	(*fp_set_peer_mru) (struct PPPCTRL_CALLBACK_IF*, UINT port_number, UINT value);
	bool	(*fp_set_peer_accm) (struct PPPCTRL_CALLBACK_IF*, UINT port_number, ULONG value);
	bool	(*fp_set_peer_pfc) (struct PPPCTRL_CALLBACK_IF*, UINT port_number, bool);
	bool	(*fp_set_peer_acfc) (struct PPPCTRL_CALLBACK_IF*, UINT port_number, bool);
	bool	(*fp_set_peer_fcs) (struct PPPCTRL_CALLBACK_IF*, UINT port_number, PPP_FCS_OPTION);
	bool	(*fp_set_peer_async) (struct PPPCTRL_CALLBACK_IF*, UINT port_number, bool);

} PPPCTRL_CALLBACK_IF;

typedef struct PPPCTRL_IF
{
	bool	(*fp_register) (struct PPPCTRL_IF*, PPPCTRL_CALLBACK_IF*);
	bool	(*fp_unregister) (struct PPPCTRL_IF*);

	bool 	(*fp_port_up) (struct PPPCTRL_IF* p_if, UINT port_number);
	void 	(*fp_port_down) (struct PPPCTRL_IF* p_if, UINT port_number);

	bool 	(*fp_packet_received) (struct PPPCTRL_IF* p_if, UINT port_number, PPP_MESSAGE* p_message);

} PPPCTRL_IF;

/* Temporary Kludge until Module Manager is done */
PPPCTRL_IF* pppctrl_create (void);
bool pppctrl_destroy (PPPCTRL_IF* pp_if);

#endif	/* Don't add anything after this line */