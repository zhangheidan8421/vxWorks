/* dot1xCommonIoctls.h - 802.1X required shadow ioctls */

/* Copyright 2002-2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01j,31aug05,eja  Fix for trace bug B0086
01i,17may05,eja  Add API
01h,10may05,eja  Add user mode ioctls
01g,20apr05,eja  Add Base6.1 ioctls
01f,01mar05,rb  Modified definitions of WIOCSDOT1XPORTSTATUS and
                 WIOCSAUTHCALLBACK
01e,23feb05,rb  Updated to new IOCTL scheme for WED 2.1
01d,04feb05,eja  80211i/WPA integration
01c,18jan05,eja  Add structure and enums for 802.11i integration
01b,11jan05,rb  Pruned IOCTL list for new integration with wireless driver
01a,10jan05,rb  Added new IOCTLs for 2.0 release
*/

/*
DESCRIPTION
*/

#ifndef __INCdot1xCommonIoctlsh
#define __INCdot1xCommonIoctlsh

#ifdef __cplusplus
extern "C" {
#endif

/*****************************************************************************
* Public DOT11 IOCTL Commands 
*****************************************************************************/
#include "sys/ioctl.h"

/* For backwards compatability, define the "user mode" IOCTL macros manually
if they are not already defined.  They should be defined in vxWorks 6.1 and 
higher */

#ifndef IOC_USER
#define IOC_USER 0x8000
#endif /* IOC_USER */

#ifndef _IOU
#define _IOU(x,y)              (IOC_USER | _IO(x,y))
#endif

#ifndef _IORU
#define _IORU(x,y,t)           (IOC_USER | _IOR(x,y,t))
#endif

#ifndef _IOWU
#define _IOWU(x,y,t)           (IOC_USER | _IOW(x,y,t))
#endif

#ifndef _IOWRU
#define _IOWRU(x,y,t)          (IOC_USER | _IOWR(x,y,t))
#endif

/* These are types used to define the access lengths in the "WIOC" IOCTLS */
#define DOT11_ADDR_LEN                  6
typedef UINT8 DOT11_ADDR[DOT11_ADDR_LEN];

#define DOT11_PMK_LEN                32
typedef UINT8 DOT11_PMK[DOT11_PMK_LEN];

/* 802.1X port status enumeration */
typedef enum 
    {
    DOT1X_PORTSTATUS_UNCONTROLLED = 0,
    DOT1X_PORTSTATUS_CLOSED,
    DOT1X_PORTSTATUS_OPEN
    } DOT1X_PORT_STATUS;

/* 802.1X port event type enumeration */
typedef enum 
    {
    DOT1X_PORTEVENT_ASSOCIATION = 1,
    DOT1X_PORTEVENT_DISASSOCIATION
    } DOT1X_PORT_EVENT;

typedef struct
    {
    INT32 status; /* Port status */
    UINT8 macAddr[6]; /* mac address of the port */
    } _WRS_PACK_ALIGN(1) DOT1X_PORTSTATUS;

typedef struct param_s
    {
    UINT32 callback; /* Callback event handler */
    UINT32 cookie; /* private data */
    } _WRS_PACK_ALIGN(1) DOT1X_PARAMS;

/***************************************************************************
* WIOCSCIPHPOL - Set the available cipher suites 
****************************************************************************/
typedef unsigned long int  DOT11_KEY_TYPE;
#define DOT11_KEY_SEARCH       (65536)
#define DOT11_CIPHPOL_NONE     0
#define DOT11_CIPHPOL_WEP40    (0x01)
#define DOT11_CIPHPOL_WEP104   (0x02)
#define DOT11_CIPHPOL_TKIP     (0x04)
#define DOT11_CIPHPOL_AES      (0x08)
#define DOT11_KEY_TYPE_NONE    0
#define DOT11_KEY_TYPE_WEP40   DOT11_CIPHPOL_WEP40
#define DOT11_KEY_TYPE_WEP104  DOT11_CIPHPOL_WEP104
#define DOT11_KEY_TYPE_TKIP    DOT11_CIPHPOL_TKIP
#define DOT11_KEY_TYPE_AES     DOT11_CIPHPOL_AES
#define DOT11_WEP40_KEY_SIZE   5  /* Bytes */
#define DOT11_WEP104_KEY_SIZE  13 /* Bytes */
#define DOT11_AES_KEY_SIZE     16 /* Bytes */
#define DOT11_TKIP_KEY_SIZE    32 /* 16 Bytes key, 2x8 bytes MIC */
typedef struct
    {
    DOT11_KEY_TYPE keyType;
    INT32 keySlot;                      /* Keyslot key was installed to  */
    UINT8 macAddr[DOT11_ADDR_LEN];
    union
        {
        UINT8 wep40[DOT11_WEP40_KEY_SIZE];
        UINT8 wep104[DOT11_WEP104_KEY_SIZE];
        UINT8 aes[DOT11_AES_KEY_SIZE];
        UINT8 tkip[DOT11_TKIP_KEY_SIZE];
        } type;
    } DOT11_KEY;

/* These parameters shadow the current wlan ioctls but are defined 
here to reduce unneeded intermodule dependencies */ 
#define EIOCGWEPSTATUS       (0x20f) /* Get WEP status*/
#define EIOCSWEP             (0x210) /* Enable/disable WEP */
#define EIOCSWEPKEY0         (0x211) /* Set WEP key 0*/
#define EIOCSWEPKEY1         (0x212) /* Set WEP key 1*/
#define EIOCSWEPKEY2         (0x213) /* Set WEP key 2*/
#define EIOCSWEPKEY3         (0x214) /* Set WEP key 3*/
#define EIOCSWEPDEFAULTKEY   (0x215) /* Set default WEP key */
#define EIOCSWEPTYPE         (0x21d) /* Set WEP type - 64 bit or 128 bit */
#define EIOCSUNICASTKEYNUM   (0x22b) /* Set unicast key slot */
#define EIOCSMULTICASTKEYNUM (0x22d) /* Set multicast key slot */
#define EIOCGCONNECTEDBSSID  (0x220) /* Get BSSID */
#define EIOCSAUTHCALLBACK    (0x24B) /* Register authentication callback */
#define EIOCS802DOT1XMODE    (0x24C) /* Set 802.1x mode */
#define EIOCG802DOT1XMODE    (0x24D) /* Get 802.1x mode status */

#define WIOCGCONNECTEDBSSID    _IORU('w', 0x006, DOT11_ADDR)
#define WIOCGSECPOL            _IORU('w', 0x051, UINT32)
#define WIOCSPMK               _IOWU('x', 0x03, DOT11_PMK)
#define WIOCSAUTHCALLBACK      _IOW('x', 0x02,DOT1X_PARAMS)             
#define WIOCSDOT1XPORTSTATUS   _IOW('x', 0x01,DOT1X_PORTSTATUS)
#define WIOCAPAIRWISEKEY       _IOWU('w', 0x04f, DOT11_KEY)
#define WIOCDPAIRWISEKEY       _IOWU('w', 0x62, DOT11_ADDR)
#define WIOCSENCRYPTTYPE       _IOU('w', 0x016) 
#define WIOCDPAIRWISEKEY       _IOWU('w', 0x62, DOT11_ADDR)

#define ONEX_MODE_ENABLED      (1)
#define DOT11_40_BIT_KEY_SIZE  (5) /* Num bytes in 40 bit key */
#define DOT11_128_BIT_KEY_SIZE (13) /* Num bytes in 128 bit key */
#define WLAN_WEP_ENABLED       (TRUE) /* Use wep */
#define WLAN_WEP_NUM_KEYS      (4) /* Max number of static keys stored */
#define WLAN_WEP_TYPE_64       (64) /* 40/64 bit encryption    */
#define WLAN_WEP_TYPE_128      (128) /* 104/128 bit encryption  */
#define DOT1X_80211_PMK_LEN    (32)
#define DOT1X_80211_ADDR_LEN   (6)
#define DOT1X_SECPOL_NONE      (0)
#define DOT1X_SECPOL_TSN       (1)
#define DOT1X_SECPOL_WPA       (2)
#define DOT1X_SECPOL_11i       (4)

/* 802.1x user ioctls */
typedef struct dot1x_rtp_args
    {
    void *arg0; /* The first argument is the device id */
    char arg1[256]; /* Each arg there after has 256 bytes allocated */
    char arg2[256];
    char arg3[256];
    char arg4[256];
    char arg5[256];
    char arg6[256];
    } DOT1X_RTP_ARGS;

/* Ioctl defines */

/* dot1xSuppLibInit() - Create root task */
#define EIOCSUPPLIBINIT          _IORU('d', 0x000, DOT1X_RTP_ARGS)

/* dot1xSuppCreate()  - Create and add a supplicant to the root task list */
#define EIOCSUPPCREATE           _IORU('d', 0x001, DOT1X_RTP_ARGS)

/* dot1xSuppDelete() - Destroy and delete a supplicant from root task list */
#define EIOCSUPPDELETE           _IORU('d', 0x002, DOT1X_RTP_ARGS) 

/* dot1xSuppLogonSet() - Set the username and password for a supplicant */
#define EIOCSUPPLOGONSET         _IOWU('d', 0x003, DOT1X_RTP_ARGS) 

/* dot1xSuppLogonGet() - Get the username and password for a supplicant */
#define EIOCSUPPLOGONGET         _IORU('d', 0x004, DOT1X_RTP_ARGS)

/* dot1xSuppCertInfoSet() - Set information used by cert based authentication */
#define EIOCSUPPCERTINFOSET      _IOWU('d', 0x005, DOT1X_RTP_ARGS)

/* dot1xSuppCertInfoGet() - Get information used by cert based authentication */
#define EIOCSUPPCERTINFOGET      _IORU('d', 0x006, DOT1X_RTP_ARGS)

/* dot1xSuppPacketTrace() - Enable or Disable packet trace */
#define EIOCSUPPPACKETTRACE      _IORU('d', 0x007, DOT1X_RTP_ARGS)

/* dot1xSuppEventLogFlush() - Flush supplicant event log */
#define EIOCSUPPEVENTLOGFLUSH    _IORU('d', 0x008, DOT1X_RTP_ARGS)

/* dot1xSuppIdGet() - Get supplicant ID based on instance number */
#define EIOCSUPPIDGET            _IORU('d', 0x009, DOT1X_RTP_ARGS)

/* dot1xSuppConnect() - Force supplicant to connect to NAS */
#define EIOCSUPPCONNECT          _IORU('d', 0x00A, DOT1X_RTP_ARGS) 

/* dot1xSuppShutdownAll() - Shutdown all supplicants and delete all tasks */
#define EIOCSUPPSHUTDOWNALL      _IORU('d', 0x00B, DOT1X_RTP_ARGS)

/* dot1xSuppEapTypeEnable() - Enable EAP method - Kernel/User mode */
#define EIOCSUPPEAPTYPEENABLE    _IORU('d', 0x00C, DOT1X_RTP_ARGS) 

/* dot1xSuppEapTypeDisable() - Disable EAP method */
#define EIOCSUPPEAPTYPEDISABLE   _IORU('d', 0x00D, DOT1X_RTP_ARGS)

/* dot1xSuppHandlerCacheInstall() - Install handler cache into supplicant */
#define EIOCSUPPHDLRCACHEINSTALL _IORU('d', 0x00E, DOT1X_RTP_ARGS) 

/* dot1xAuthServerLibInit() - Init authentication server */
#define EIOCAUTHSVRLIBINIT       _IORU('d', 0x00F, DOT1X_RTP_ARGS) 

/* dot1xAuthServerLibShutDown() - Shutdown authentication server */
#define EIOCAUTHSVRLIBSHUTDOWN   _IORU('d', 0x010, DOT1X_RTP_ARGS) 

/* dot1xAuthInit() - Shutdown authentication server */
#define EIOCAUTHINIT             _IORU('d', 0x020, DOT1X_RTP_ARGS) 

/* dot1xAuthShutdown() - Shutdown authenticator */
#define EIOCAUTHSHUTDOWN         _IORU('d', 0x030, DOT1X_RTP_ARGS) 

/* dot1xAuthAttach() - Attach to authenticator */
#define EIOCAUTHATTACH           _IORU('d', 0x040, DOT1X_RTP_ARGS) 

/* dot1xAuthDetach() - Detach from authenticator */
#define EIOCAUTHDETACH           _IORU('d', 0x050, DOT1X_RTP_ARGS) 

/* dot1xAuthWlanConfig() - Configure wlan device on authenticator */
#define EIOCAUTHWLANCONFIG       _IORU('d', 0x060, DOT1X_RTP_ARGS) 

/* Virtual END driver init method - Base6.1 only */
void *dot1xVirtEndDevInit(void);

#ifdef __cplusplus
}
#endif

#endif /* __INCdot1xCommonIoctlsh */
