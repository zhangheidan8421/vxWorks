/* dot1xAuthLib.h - 802.1X Authenticator Definitions */

/* Copyright 2002-2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01p,23sep05,eja  Break SNMP dependency
01o,05aug05,eja  Version 2.1 update
01n,30may05,eja  cleanup
01m,17may05,eja  Clean up header dependencies
01l,04feb05,eja  80211i/WPA integration
01k,17jan05,eja  Add mode element to PAE object
01j,03jan05,eja  Cleanup
01i,30apr03,ss  added version info
01h,21apr03,ggg  File name, location change
01g,21feb03,ggg  Update copyright dates
01f,22jan03,ggg  Add key slots ifConfig members
01e,31oct02,ggg  Remove warning
01d,13may02,mef  to clean-up the code
01c,25mar02,mef  to add more configure option to interface
01b,04mar02,mef  to add interface configuration structure
01a,26Dec01,mef written.

*/

/*
DESCRIPTION

This file contains the definitions that are used by different
components of the system to interact with 802.1X Framework.

*/

#ifndef __INCdot1xAuthLibh
#define __INCdot1xAuthLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <vxWorks.h>

#define DOT1X_AUTH_VERSION "2.1"
IMPORT UCHAR *dot1xAuthVersion;

/* typedefs */

/* Result code types used between 802.1X SNMP MIB 
 * support and 1X Framework.
 */
typedef enum
    {
    DOT1X_AUTH_MIB_SET_DONE,
    DOT1X_AUTH_MIB_SET_WAIT,
    DOT1X_AUTH_MIB_SET_ERROR
    } DOT1X_AUTH_MIB_SET_STATUS;

/* Physical Network Interface Type */

typedef enum
    {
    DOT1X_AUTH_WIRED_IF = 1,
    DOT1X_AUTH_WIRELESS_IF
    } DOT1X_AUTH_IF_TYPE;

/* Interface Operation Type */

typedef enum
    {
    DOT1X_AUTH_WIRELESS = 1,
    DOT1X_AUTH_MULTIPLE_HOST,
    DOT1X_AUTH_SINGLE_HOST
    } DOT1X_AUTH_IF_OPR_TYPE;

/* WEP Key Types */

typedef enum
    {
    DOT1X_AUTH_40BIT_KEY = 1,
    DOT1X_AUTH_128BIT_KEY
    } DOT1X_AUTH_WEP_KEY_TYPE;

/* UCAST Key Support Types */

typedef enum
    {
    DOT1X_AUTH_NO_UCAST_KEY = 1,
    DOT1X_AUTH_FAKE_UCAST_KEY,
    DOT1X_AUTH_REAL_UCAST_KEY
    } DOT1X_AUTH_UCAST_SUPPORT;

typedef struct
    {
    /* Interface Type */
    DOT1X_AUTH_IF_TYPE type;

    /* Interface operation type.  If interface is "WIRELESS", it must be
     * "WIRELESS".  If interface is "WIRED", all three possible values can
     * be used. */
    DOT1X_AUTH_IF_OPR_TYPE operationType;

    /* Inactivity Timeout for a port. (second) */
    int inactivityTimeout;

    /* Connectivity Timeout for a port.(second) */
    int connectivityTimeout;

    /* Following configuration fields are usable only for "WIRELESS"
     * interfaces. */

    /* WEP Key Length   */
    DOT1X_AUTH_WEP_KEY_TYPE wepKeyType;

    /* Interface uses slot 0 as static WEP key.  It may be  used to support
     * MD5 supplicants. In that case 1X AUTH will only renew MCAST key in
     * slot 1.  */
    BOOL staticWepKeySupport;

    /* If the static WEP key will be supported, the key info must be
     *provided.  */
    char *staticWepKey;

    /* Renewal Timeout for MCAST Key. (second) */  
    int mcastKeyPeriod;

    /* UCAST Key support*/ 
    DOT1X_AUTH_UCAST_SUPPORT  ucastKeySupportType;

    /* Use session key as UCAST key or generate UCAST key and deliver it. */ 
    BOOL ucastKeyIsSessionKey;

    /* Renewal Timeout in for UCAST Key. (second) */
    int ucastKeyPeriod;

    /* Slots used for uCast */
    UINT32 ucastKeySlots;

    /* Slots uses for mCast */
    UINT32 mcastKeySlots;
    } DOT1X_AUTH_IF_CONFIG;

typedef struct
    {
    UINT32  taskPriority;  /*  1X AUTH Task Priority*/
    UINT32  nbrOfPorts;    /* number of estimated active ports at a moment */
    FUNCPTR eapolAsfAlert; /* pointer to the EAPOL_ASF_ALERT messages */
    int     replayCntType; /* Replay Cnt Type in EAPOL-Key message */ 
    int     ntpExecMode;  
    char *  ntpSvrIpAddr;  /* NTP Sever IP Address */
    FUNCPTR ntpTimeGetFunc;  
    void *  pMib;
    } DOT1X_AUTH_SYS_CONFIG;

/* definitions */

#define DOT1X_AUTH_INVALID_PORT_NBR 0

/* Function Prototypes */

IMPORT STATUS dot1xAuthInit (DOT1X_AUTH_SYS_CONFIG *);
STATUS dot1xAuthPortListShow(int);
IMPORT STATUS dot1xAuthShutdown(VOID);
IMPORT STATUS dot1xAuthAttach(INT32, UCHAR *, DOT1X_AUTH_IF_CONFIG *);
IMPORT STATUS dot1xAuthDetach(INT32, UCHAR *);
IMPORT STATUS dot1xAuthTimerTimeoutSend (UINT32, int, UINT8);
IMPORT DOT1X_AUTH_MIB_SET_STATUS dot1xAuthMibSet(UINT32, INT32, VOID *, 
                                                 UINT32, FUNCPTR, INT32);
#ifdef __cplusplus
}
#endif

#endif /* __INCdot1xAuthLibh */
