/* dot1xAuthTimerLib.h - 802.1X Authenticator Timer Manager Definitions */

/* Copyright 2002-2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01c,21apr03,ggg  File name, location change
01b,13may02,mef to clean up the code.
01a,26Dec01,mef written.

*/

/*
DESCRIPTION

*/

#ifndef __INCdot1xAuthTimerLibh
#define __INCdot1xAuthTimerLibh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"

/* typedefs */

typedef enum                   /* DOT1X Timer Types  */
			       /* For detailed Information
			          check standard pg.34
			       */
    {
    DOT1X_AUTH_TIMER_AWHILE,     /* aWhile Timer         */
    DOT1X_AUTH_TIMER_QUIETWHILE, /* quietWhile Timer     */
    DOT1X_AUTH_TIMER_REAUTHWHEN, /* reAuthWhen Timer     */
    DOT1X_AUTH_TIMER_TXWHEN,     /* txWhen Timer         */
    DOT1X_AUTH_TIMER_INTERNAL    /* 1x Auth Internal Timer */
    } DOT1X_AUTH_TIMER_TYPE;

/* definitions */

#define DOT1X_AUTH_TIMER_NBR     5 /* # of timers */

/* Function Prototypes */

IMPORT int    dot1xAuthTimerManagerInit (INT32);
IMPORT UINT32 dot1xAuthTimerTickGet ();
IMPORT STATUS dot1xAuthTimerManagerShutdown ();
IMPORT STATUS dot1xAuthTimerStart (UINT32, DOT1X_AUTH_TIMER_TYPE, UINT16, UINT8);
IMPORT STATUS dot1xAuthTimerStop (UINT32, DOT1X_AUTH_TIMER_TYPE, UINT8);
IMPORT STATUS dot1xAuthTimerStopAll (UINT32);

#ifdef __cplusplus
}
#endif

#endif /* __INCdot1xAuthTimerLibh */

