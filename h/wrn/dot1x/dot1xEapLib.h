/* dot1xEapLib.h - 802.1X EAP specific library header file */

/* Copyright 2002-2005 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01d,22sep05,eja  API change
01c,09sep05,eja  Add APIs
01b,05aug05,eja  Version 2.1 update
01a,17may05,eja  Remove dependencies on EAPOL lib
*/

#ifndef __INCdot1xEapLibh
#define __INCdot1xEapLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <vxWorks.h>

/* Maximum amount of data an EAP_REQRSP_PACKET will hold */
#define EAP_REQRSP_PACKET_MAX_DATA ETHERMTU

/* The EAP types (methods), values are ID's from applicable RFC's */
#define EAP_TYPE_IDENTITY           1
#define EAP_TYPE_NOTIFY             2
#define EAP_TYPE_NAK                3
#define EAP_TYPE_MD5                4
#define EAP_TYPE_OTP                5
#define EAP_TYPE_GTC                6
#define EAP_TYPE_TLS                13
#define EAP_TYPE_LEAP               17
#define EAP_TYPE_TTLS               21
#define EAP_TYPE_PEAP               25
#define EAP_TYPE_MSCHAPV2           26
#define EAP_TYPE_TLV                33
#define EAP_TYPE_VENDOR             255

/* EAP header */
typedef struct eap_pkt_hdr
    {
    char   code;  /* kind of EAP packet, DOT1X_EAP_CODE */
    char   ident; /* identifier of which request or response */
    UINT16 size;  /* size of packet */
    char   type;  /* type of EAP, EAP_TYPE_XXX */
    } _WRS_PACK_ALIGN(1) EAP_PKT_HDR;

typedef struct eap_hdr_s
    {
    char   code;       /* kind of EAP packet, DOT1X_EAP_CODE */
    char   identifier; /* identifier of which request or response */
    UINT16 length;     /* size of packet */
    } _WRS_PACK_ALIGN(1) EAP_HDR;

/* EAP Packet codes */
typedef enum
    {
    EAP_REQUEST = 1,
    EAP_RESPONSE,
    EAP_SUCCESS,
    EAP_FAILURE
    } DOT1X_EAP_PACKET_CODE;

/* EAP and management */
STATUS dot1xSuppEapPktReceive(int, char *, char, char, UINT16, void *);
STATUS dot1xSuppEapPktSend(int, void *);
STATUS dot1xSuppEapPktError(int, char *, UINT32);
void ** dot1xEapTypeToString(char, void **);
void dot1xEapCodeToString(char, void **);
int dot1xEapStringToType(void *);
void dot1xEapTypesShow(void);
void dot1xEapCodesShow(void);

#ifdef __cplusplus
}
#endif

#endif /* __INCdot1xEapLibh */
