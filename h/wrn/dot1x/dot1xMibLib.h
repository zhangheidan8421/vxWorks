/* dot1xMibLib.h - 802.1X MIB API to SNMP Agent header file */

/* Copyright 2002-2005 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01d,22sep05,eja  Add new structure to allow for IDE based module binding
01c,05jul05,eja  Add support for supplicant MIB
01b,06jun02,myz  changed name UINT64 to UINT64_S
01a,15Jan02,myz  written
*/

#ifndef __INCdot1xMibLibh
#define __INCdot1xMibLibh

#ifdef __cplusplus
extern "C" {
#endif

/**** MIB variable IDs ****/

#define D1XM_VAR_PaeSystemAuth               0x10

/* Group IDs */

#define D1XM_VAR_PORT_ENTRY                  0x100
#define D1XM_VAR_AUTH_CFG_ENTRY              0x200
#define D1XM_VAR_AUTH_STATS_ENTRY            0x300
#define D1XM_VAR_AUTH_DIAG_ENTRY             0x400
#define D1XM_VAR_AUTH_SESSION_ENTRY          0x500
#define D1XM_VAR_SUPP_CFG_ENTRY              0x600
#define D1XM_VAR_SUPP_STATS_ENTRY            0x700

/* PAE Port Variables */

#define D1XM_VAR_PaePortNumber               0x110
#define D1XM_VAR_PaePortProtocolVersion      0x120
#define D1XM_VAR_PaePortCapabilities         0x130
#define D1XM_VAR_PaePortInitialize           0x140
#define D1XM_VAR_PaePortReauthenticate       0x150

/* Authenticator Configuration variables */

#define D1XM_VAR_AuthPaeState                   0x210
#define D1XM_VAR_AuthBackendAuthState           0x220
#define D1XM_VAR_AuthAdminControlledDirections  0x230
#define D1XM_VAR_AuthOperControlledDirections   0x240
#define D1XM_VAR_AuthAuthControlledPortStatus   0x250
#define D1XM_VAR_AuthControlledPortControl      0x260
#define D1XM_VAR_AuthQuietPeriod                0x270
#define D1XM_VAR_AuthTxPeriod                   0x280
#define D1XM_VAR_AuthSuppTimeout                0x290
#define D1XM_VAR_AuthServerTimeout              0x2A0 
#define D1XM_VAR_AuthMaxReq                     0x2B0
#define D1XM_VAR_AuthReauthPeriod               0x2C0
#define D1XM_VAR_AuthReauthEnabled              0x2D0
#define D1XM_VAR_AuthKeyTxEnabled               0x2E0

/* Authenticator Stats Variables */

#define D1XM_VAR_AuthEapolFramesRx              0x310
#define D1XM_VAR_AuthEapolFramesTx              0x320
#define D1XM_VAR_AuthEapolStartFramesRx         0x330
#define D1XM_VAR_AuthEapolLogoffFramesRx        0x340
#define D1XM_VAR_AuthEapolRespIdFramesRx        0x350
#define D1XM_VAR_AuthEapolRespFramesRx          0x360
#define D1XM_VAR_AuthEapolReqIdFramesTx         0x370
#define D1XM_VAR_AuthEapolReqFramesTx           0x380
#define D1XM_VAR_AuthInvalidEapolFramesRx       0x390
#define D1XM_VAR_AuthEapLengthErrorFramesRx     0x3A0 
#define D1XM_VAR_AuthLastEapolFrameVersion      0x3B0
#define D1XM_VAR_AuthLastEapolFrameSource       0x3C0

/* Authenticator Diagnostic Variables */

#define D1XM_VAR_AuthEntersConnecting                       0x410
#define D1XM_VAR_AuthEapLogoffsWhileConnecting              0x420
#define D1XM_VAR_AuthEntersAuthenticating                   0x430
#define D1XM_VAR_AuthAuthSuccessWhileAuthenticating         0x440
#define D1XM_VAR_AuthAuthTimeoutsWhileAuthenticating        0x450
#define D1XM_VAR_AuthAuthFailWhileAuthenticating            0x460
#define D1XM_VAR_AuthAuthReauthsWhileAuthenticating         0x470
#define D1XM_VAR_AuthAuthEapStartsWhileAuthenticating       0x480
#define D1XM_VAR_AuthAuthEapLogoffWhileAuthenticating       0x490
#define D1XM_VAR_AuthAuthReauthsWhileAuthenticated          0x4A0 
#define D1XM_VAR_AuthAuthEapStartsWhileAuthenticated        0x4B0
#define D1XM_VAR_AuthAuthEapLogoffWhileAuthenticated        0x4C0
#define D1XM_VAR_AuthBackendResponses                       0x4D0
#define D1XM_VAR_AuthBackendAccessChallenges                0x4E0
#define D1XM_VAR_AuthBackendOtherRequestsToSupplicant       0x4E2
#define D1XM_VAR_AuthBackendNonNakResponsesFromSupplicant   0x4E4
#define D1XM_VAR_AuthBackendAuthSuccesses                   0x4E6
#define D1XM_VAR_AuthBackendAuthFails                       0x4E8

/* Authenticator Session Stats Variables */

#define D1XM_VAR_AuthSessionOctetsRx              0x510
#define D1XM_VAR_AuthSessionOctetsTx              0x520
#define D1XM_VAR_AuthSessionFramesRx              0x530
#define D1XM_VAR_AuthSessionFramesTx              0x540
#define D1XM_VAR_AuthSessionId                    0x550
#define D1XM_VAR_AuthSessionAuthenticMethod       0x560
#define D1XM_VAR_AuthSessionTime                  0x570
#define D1XM_VAR_AuthSessionTerminateCause        0x580
#define D1XM_VAR_AuthSessionUserName              0x590

/* Supplicant Configuration Varibles */

#define D1XM_VAR_SuppPaeState              0x610
#define D1XM_VAR_SuppHeldPeriod            0x620
#define D1XM_VAR_SuppAuthPeriod            0x630
#define D1XM_VAR_SuppStartPeriod           0x640
#define D1XM_VAR_SuppMaxStart              0x650

/* Supplicant Statistics Varibles */

#define D1XM_VAR_SuppEapolFramesRx            0x710
#define D1XM_VAR_SuppEapolFramesTx            0x720
#define D1XM_VAR_SuppEapolStartFramesTx       0x730
#define D1XM_VAR_SuppEapolLogoffFramesTx      0x740
#define D1XM_VAR_SuppEapolRespIdFramesTx      0x750
#define D1XM_VAR_SuppEapolRespFramesTx        0x760
#define D1XM_VAR_SuppEapolReqIdFramesRx       0x770
#define D1XM_VAR_SuppEapolReqFramesRx         0x780
#define D1XM_VAR_SuppInvalidEapolFramesRx     0x790
#define D1XM_VAR_SuppEapLengthErrorFramesRx   0x7A0
#define D1XM_VAR_SuppLastEapolFrameVersion    0x7B0
#define D1XM_VAR_SuppLastEapolFrameSource     0x7C0

/* simple type used for imitating 64 bit integers */

typedef struct
    {
    UINT32 high;
    UINT32 low;
    } UINT64_S;

/* SystemAuthControl */

typedef UINT32 D1XM_SYSTEM_AUTH_CONTROL;

/* dot1xPaePortTable   9.6.1 System Configuration */

typedef enum {
    D1XM_dot1xPaePortAuthCapable=0,
    D1XM_dot1xPaePortSuppCapable
    } D1XM_PORT_CAP_TYPE;

typedef struct {
    UINT32 paePortNumber;    /* port number */ 
    UINT32 paePortProtocolVersion;   /*Protocol version */
    UINT8  paePortCapabilities[1];      /*PAE capabilities  */
    UINT8  padding[3];
    UINT32 paePortInitialize;   /* Initialize Port */
    UINT32 paePortReauthenticate;
    } D1XM_PAE_PORT_ENTRY;

/* dot1xAuthConfigTable   9.4.1 Authenticator Configuration  */

typedef struct {
    UINT32 paePortNumber;    /* port number */ 
    UINT32 authPaeState;   /* Authenticator PAE State  */
    UINT32 authBackendAuthState;   /* Backend Authentication State  */
    UINT32 authAdminControlledDirections;   /* AdminControlledDirections  */
    UINT32 authOperControlledDirections;   /* OperControlledDirections  */
    UINT32 authAuthControlledPortStatus;   /* AuthControlledPortStatus  */
    UINT32 authAuthControlledPortControl;   /* AuthControlledPortControl  */
    UINT32 authQuietPeriod;   /* quietPeriod  */
    UINT32 authTxPeriod;   /* txPeriod  */
    UINT32 authSuppTimeout;   /* suppTimeout  */
    UINT32 authServerTimeout;   /* serverTimeout  */
    UINT32 authMaxReq;   /* maxReq  */
    UINT32 authReAuthPeriod;   /* reAuthPeriod  */
    UINT32 authReAuthEnabled;   /* reAuthEnabled  */
    UINT32 authKeyTxEnabled;   /* KeyTransmissionEnabled  */
    UINT32 PaePortReauthenticate;   /* Reauthenticate  */
    } D1XM_AUTH_CONFIG_ENTRY;

/* dot1xAuthStatsTable    9.4.2 Authenticator Statistics  */

typedef struct {
    UINT32 paePortNumber;    /* port number */ 
    UINT32 authEapolFramesRx;   /* EAPOL frames received  */
    UINT32 authEapolFramesTx;   /* EAPOL frames transmitted  */
    UINT32 authEapolStartFramesRx;   /* EAPOL Start frames received  */
    UINT32 authEapolLogoffFramesRx;   /* EAPOL Logoff frames received  */
    UINT32 authEapolRespIdFramesRx;   /* EAP Resp/Id frames received  */
    UINT32 authEapolRespFramesRx;   /* EAP Response frames received  */
    UINT32 authEapolReqIdFramesTx;   /* EAP Req/Id frames transmitted  */
    UINT32 authEapolReqFramesTx;   /* EAP Request frames transmitted  */
    UINT32 authInvalidEapolFramesRx;   /* Invalid EAPOL frames received  */
    UINT32 authEapLengthErrorFramesRx;   /* EAP length error frames received  */
    UINT32 authLastEapolFrameVersion;   /* Last EAPOL frame version  */
    UINT8  authLastEapolFrameSource[6];   /* Last EAPOL frame source  */
    } D1XM_AUTH_STATS_ENTRY;

/* dot1xAuthDiagTable    9.4.3 Authenticator Diagnostics  */

typedef struct {
    UINT32 paePortNumber;    /* port number */ 
    UINT32 authEntersConnecting;   
    UINT32 authEapLogoffsWhileConnecting;   
    UINT32 authEntersAuthenticating;   
    UINT32 authAuthSuccessWhileAuthenticating;   
    UINT32 authAuthTimeoutsWhileAuthenticating; 
    UINT32 authAuthFailWhileAuthenticating;   
    UINT32 authAuthReauthsWhileAuthenticating; 
    UINT32 authAuthEapStartsWhileAuthenticating; 
    UINT32 authAuthEapLogoffWhileAuthenticating;
    UINT32 authAuthReauthsWhileAuthenticated;  
    UINT32 authAuthEapStartsWhileAuthenticated;  
    UINT32 authAuthEapLogoffWhileAuthenticated; 
    UINT32 authBackendResponses;   
    UINT32 authBackendAccessChallenges;   
    UINT32 authBackendOtherRequestsToSupplicant;  
    UINT32 authBackendNonNakResponsesFromSupplicant; 
    UINT32 authBackendAuthSuccesses;   
    UINT32 authBackendAuthFails;   
   } D1XM_AUTH_DIAG_ENTRY;

/* dot1xAuthSessionStatsTable   9.4.4 Authenticator Session Statistics  */

typedef struct {
    UINT32 paePortNumber;    /* port number */ 
    UINT64_S authSessionOctetsRx;   /* Session Octets Received  */
    UINT64_S authSessionOctetsTx;   /* Session Octets Transmitted  */
    UINT32 authSessionFramesRx;   /* Session Frames Received  */
    UINT32 authSessionFramesTx;   /* Session Frames Transmitted  */
    char * authSessionId;         /* Session Identifier  */
    UINT32 authSessionAuthenticMethod;   /* Session Authentication Method  */
    UINT32 authSessionTime;       /* Session Time  */
    UINT32 authSessionTerminateCause;   /* Session Terminate Cause  */
    char * authSessionUserName;   /* Session User Name  */
    } D1XM_AUTH_SESSION_STATS_ENTRY;

/* dot1xSuppConfigTable   9.5.1 Supplicant Configuration  */

typedef struct {
    UINT32 paePortNumber;    /* port number */ 
    UINT32 suppPaeState;   /* Supplicant PAE State  */
    UINT32 suppHeldPeriod;   /* heldPeriod  */
    UINT32 suppAuthPeriod;   /* authPeriod  */
    UINT32 suppStartPeriod;   /* startPeriod  */
    UINT32 suppMaxStart;   /* maxStart  */
    } D1XM_SUPP_CONFIG_ENTRY;

/* dot1xSuppStatsTable    9.5.2 Supplicant Statistics  */

typedef struct {
    UINT32 paePortNumber;    /* port number */ 
    UINT32 suppEapolFramesRx;   /* EAPOL frames received  */
    UINT32 suppEapolFramesTx;   /* EAPOL frames transmitted  */
    UINT32 suppEapolStartFramesTx;   /* EAPOL Start frames transmitted  */
    UINT32 suppEapolLogoffFramesTx;   /* EAPOL Logoff frames transmitted  */
    UINT32 suppEapolRespIdFramesTx;   /* EAP Resp/Id frames transmitted  */
    UINT32 suppEapolRespFramesTx;   /* EAP Response frames transmitted  */
    UINT32 suppEapolReqIdFramesRx;   /* EAP Req/Id frames received  */
    UINT32 suppEapolReqFramesRx;   /* EAP Request frames received  */
    UINT32 suppInvalidEapolFramesRx;   /* Invalid EAPOL frames received  */
    UINT32 suppEapLengthErrorFramesRx;  /* EAP length error frames received  */
    UINT32 suppLastEapolFrameVersion;   /* Last EAPOL frame version  */
    UINT8  suppLastEapolFrameSource[6];   /* Last EAPOL frame source  */
    } D1XM_SUPP_STATS_ENTRY;

/* dot1xPaeSystem group */

typedef struct {
    D1XM_SYSTEM_AUTH_CONTROL authControl;
    D1XM_PAE_PORT_ENTRY      portTable;
    } D1XM_PAE_SYSTEM_GROUP;

/* dot1xPaeAuthenticator group */

typedef struct {
    D1XM_AUTH_CONFIG_ENTRY cfgTbl;    /* configuration table */
    D1XM_AUTH_STATS_ENTRY  statsTbl;  /* STATS table */
    D1XM_AUTH_DIAG_ENTRY   diagTbl;   /* diagnostics table */
    D1XM_AUTH_SESSION_STATS_ENTRY sessionTbl; /* authenticator session stats*/
    } D1XM_PAE_AUTHENTICATOR_GROUP;

/* dot1xPaeSupplicant Group */

typedef struct {
    D1XM_SUPP_CONFIG_ENTRY cfgTbl;   /* supplicant configuration table */
    D1XM_SUPP_STATS_ENTRY  statsTbl; /* supplicant stats table */
    } D1XM_PAE_SUPPLICANT_GROUP;

typedef struct {
    D1XM_PAE_PORT_ENTRY          portGrp;
    D1XM_PAE_AUTHENTICATOR_GROUP authGrp;
    D1XM_PAE_SUPPLICANT_GROUP    suppGrp;
    } D1XM_PORT_ENTRY;

/* MIB handler type */
typedef enum 
    {
    DOT1X_AUTH_BINDING = 0, /* authenticator MIB */
    DOT1X_SUPP_BINDING      /* supplicant MIB */
    } DOT1X_MIB_BIND;

/* MIB module attachment */
typedef struct 
    {
    STATUS (*pInstallHandler)();
    STATUS (*pRemoveHandler)();
    } DOT1X_MIB_MODULE_BINDING;

/* MIB handler structure */
typedef struct
    {
    int (*get)();  /* get method */
    int (*next)(); /* next method */
    int (*set)();  /* set method */
    } DOT1X_MIB_FUNC_TABLE;

/* authenticator and supplicant MIB handlers */
typedef struct 
    {
    DOT1X_MIB_FUNC_TABLE *authHandler; /* authenticator table */
    DOT1X_MIB_FUNC_TABLE *suppHandler; /* supplicant table */
    } DOT1X_MIB_HANDLER;


/* function declarations */
 
#if defined(__STDC__) || defined(__cplusplus)

STATUS dot1xMibVarGet (UINT32, int, void*, int);
STATUS dot1xMibVarSet (UINT32, int, void*, int);
STATUS dot1xMibNewPortAdded (UINT32,D1XM_AUTH_CONFIG_ENTRY *);
STATUS dot1xMibPortRemoved (UINT32);
STATUS dot1xMibSnmpEntryGet (UINT32, int, void*, int);
STATUS dot1xMibSnmpNextEntryGet (UINT32 *, int, void*, int);
STATUS dot1xMibSnmpVarSet (UINT32, UINT32, UINT32);
STATUS dot1xMibNextPortVarGet (UINT32*, int, void*, int);
STATUS dot1xMibLibInit(void);
STATUS dot1xMibHandlerInstall(void *, int);
STATUS dot1xMibHandlerRemove(void *, int);
STATUS dot1xMibAdd(void);

#else   /* __STDC__ */

STATUS dot1xMibVarGet ();
STATUS dot1xMibVarSet ();
STATUS dot1xMibNewPortAdded ();
STATUS dot1xMibPortRemoved ();
STATUS dot1xMibSnmpEntryGet ();
STATUS dot1xMibSnmpNextEntryGet ();
STATUS dot1xMibSnmpVarSet ();
STATUS dot1xMibNextPortVarGet ();
STATUS dot1xMibLibInit();
STATUS dot1xMibHandlerInstall();
STATUS dot1xMibHandlerRemove();
STATUS dot1xMibAdd();
#endif  /* __STDC__ */


#ifdef __cplusplus
}
#endif

#endif /* __INCdot1xMibLibh */

