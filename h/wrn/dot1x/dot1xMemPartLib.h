/* dot1xMemPartLib.h - 802.1X memory partition specific library header */

/* Copyright 2002-2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01g,26jul05,eja  Add malloc macro
01f,27jun05,eja  Set released pointer to NULL.
01e,25apr05,eja  Remove setting released pointer to NULL to fix DIAB error
01d,20apr05,eja  Changes to DOT1X_FREE macro
01c,28mar05,eja  Increase minimum memory partition to 512KB
01b,15dec04,eja  Reduce the memory partition size from 1MB to 128KB
01a,08dec04,eja  Fix for Trace bug B004
*/

/*
DESCRIPTION
*/

#ifndef __INCdot1xMemPartLibh
#define __INCdot1xMemPartLibPh

#ifdef __cplusplus
extern "C" {
#endif

#include <string.h>
#include <memPartLib.h>

/* Macros for memory partition access */
#define DOT1X_CALLOC(n,s) dot1xMemBlockCalloc(n,s)
#define DOT1X_MALLOC(n,s) dot1xMemBlockMalloc(n,s)
#define DOT1X_FREE(m) \
{ \
dot1xMemBlockFree((void*)m); \
m = NULL; \
}

/* Set the maximum memory size of the partition */
#define DOT1X_MEM_PART_SZ_MAX (0x00080000) /* 512KB  of memory max */
#define DOT1X_MEM_PART_SZ_MIN (0x00010000) /* 64 KB memory min */
#define DOT1X_MEM_PART_STATS_MODE (1) /* 1 - verbose partition stats */

STATUS dot1xMemPartLibInit(void);
void * dot1xMemBlockCalloc(size_t, size_t);
void * dot1xMemBlockMalloc(size_t, size_t);
STATUS dot1xMemBlockFree(void *);
STATUS dot1xMemPartShow(void);

#ifdef __cplusplus
}
#endif

#endif /* __INCdot1xSuppLibPh */
