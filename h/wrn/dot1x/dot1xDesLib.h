/* dot1xDesLib.h - IEEE 802.1X DES interface file*/

/* Copyright 2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01a,21Apr03,ggg  written
*/


#ifndef __INCdot1xDesLibh
#define __INCdot1xDesLibh

/*
 * this DES implementation derived from Phil Karn's public domain DES.
 */

#ifdef __cplusplus
extern"C" {
#endif

#include "vxWorks.h"

typedef ULONG DES_KS[16][2];     /* Single-key DES key schedule */

typedef struct 
    {
    int                 decrypt;
    DES_KS              keysched;
    UCHAR       iv[8];
    }  DES_CTX;

#ifndef _EXCLUDE_PROTOTYPES_

void deskey   (DES_KS, UCHAR *,int);
void des      (DES_KS, UCHAR *, UCHAR *);

#endif /* ifndef _EXCLUDE_PROTOTYPES_ */

IMPORT void dot1xDesKey(DES_KS , UCHAR *, int);
IMPORT void dot1xDes(DES_KS, UCHAR *, UCHAR *);


#ifdef __cplusplus
}
#endif

#endif /* __INCdot1xDesLibh */

