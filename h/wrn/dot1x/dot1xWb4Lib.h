/* dot1xWb4Lib.h - 802.1X supplicant stream cipher library header file */

/* Copyright 2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01a,21apr03,ggg  written.
*/

#ifndef __INCdot1xWb4Libh
#define __INCdot1xWb4Libh

#ifdef __cplusplus
extern "C" {
#endif

/* Stream cipher key format */
typedef struct
{      
     unsigned char state[256];       
     unsigned char x;        
     unsigned char y;
} DOT1X_WB4_KEY;

/* Prototypes */
void dot1xWb4KeyGen(unsigned char *key_data_ptr, int key_data_len,
                    DOT1X_WB4_KEY *key);
void dot1xWb4(unsigned char *buffer_ptr, int buffer_len, DOT1X_WB4_KEY * key);

#ifdef __cplusplus
}
#endif

#endif /* __INCdot1xWb4Libh */

