/* dot1xSuppPeapLib.h - 802.1X PEAP Authentication header */

/* Copyright 2002-2005 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01f,09sep05,eja  Add member to enum
01e,05aug05,eja  Version 2.1 update
01d,25jul05,eja  API change and cleanup
01c,18mar05,eja  Reduced session count to 1
01b,01mar05,eja  Code cleanup
01a,15dec04,eja  Remove show prototype
*/

/*
DESCRIPTION
*/
#ifndef __INCdot1xSuppPeapLibh
#define __INCdot1xSuppPeapLibh

#ifdef __cplusplus
extern "C" {
#endif

/* Library header dependencies */
#include <vxWorks.h>
#include <wrn/dot1x/dot1xSuppLib.h>

/* Max peap sessions and password length */
#define PEAP_MAX_SESSIONS (1)
#define PEAP_PASSWORD_MAX_LEN (sizeof(char)*16) /* 16 octets */
#define PEAP_ENCRYPT_LABEL "client EAP encryption"

/* Inner authentication handler init type */ 
typedef SUPP_SESSION_OBJ * (*PEAP_AUTH_HDLR)(); 

/* PEAP inner authentication methods */
typedef enum
    {
    PEAP_INNER_AUTH_MS_CHAP_V2 = 0, /* MS-CHAP-V2 authentication */
    PEAP_INNER_AUTH_UNSUPPORTED
    } PEAP_INNER_AUTH_METHOD;

/* PEAP authentication states */
typedef enum
    {
    PEAP_AUTH_IDENTITY = 0,
    PEAP_AUTH_CHALLENGE,
    PEAP_AUTH_SUCCESS,
    PEAP_AUTH_EXTENSIONS
    } PEAP_AUTH_STATE; 

#define PEAP_AUTH_IDENTITY_CODE ((UCHAR)(0x01))

/* Defines a peap authentication object */
typedef struct peap_auth_s
    {
    SUPP_SESSION_OBJ curr;  /* Current Authentication object */
    SUPP_SESSION_OBJ next;  /* Next Authentication object */
    SUPP_ENV_OBJ *   env;   /* Supplicant parameters */
    PEAP_AUTH_STATE  state; /* PEAP authetication current state */
    } PEAP_SESSION_OBJ;

/* Types of inner authentication packets packets */ 
typedef enum
    {
    PEAP_INNER_AUTH_REQUEST = 1, /* Authentication request */
    PEAP_INNER_AUTH_ACK, /* Authentication success */
    PEAP_INNER_AUTH_NAK /* Authentication failure */
    } PEAP_INNER_AUTH_PKT_TYPE;

/* This structure defines the PEAP control object */
typedef struct peap_control_s
    {
    PEAP_SESSION_OBJ * session[PEAP_MAX_SESSIONS]; /* Allowed sessions */
    BOOL               sessionActive;              /* Session state */
    } PEAP_CONTROL_OBJ;


void * dot1xSuppPeapAuthInit(void *);

#ifdef __cplusplus
}
#endif

#endif /* __INCdot1xSuppPeapLibh */
