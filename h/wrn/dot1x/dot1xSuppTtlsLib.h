/* dot1xSuppTtlsLib.h - 802.1X TTLS Authentication header */

/* Copyright 2002-2005 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01g,09sep05,eja  Code cleanup
01f,01sep05,eja  Add support for CHAP as an inner auth type
01e,05aug05,eja  Version 2.1 update
01d,25jul05,eja  API change and cleanup
01c,18mar05,eja  Reduced session count to 1
01b,01mar05,eja  Code cleanup
01a,21dec04,eja  Remove show routine prototype
*/

/*
DESCRIPTION
*/
#ifndef __INCdot1xSuppTtlsLibh
#define __INCdot1xSuppTtlsLibh

#ifdef __cplusplus
extern "C" {
#endif

/* Library header dependencies */
#include <vxWorks.h>
#include <wrn/dot1x/dot1xSuppLib.h>

/* Max ttls sessions and password length */
#define TTLS_MAX_SESSIONS           (1)
#define TTLS_PAP_PASSWORD_MAX_LEN   (sizeof(char) * 16) /* 16 octets */
#define TTLS_CHAP_PASSWORD_MAX_LEN  TTLS_PAP_PASSWORD_MAX_LEN
#define TTLS_CHAP_CHALLENGE_MAX_LEN TTLS_CHAP_PASSWORD_MAX_LEN
#define TTLS_ENCRYPT_LABEL          "ttls keying material" 
#define TTLS_CHALLENGE_MATERIAL     "ttls challenge" 

/* AVP flag settings */
#define TTLS_AVP_VENDOR_SPECIFIC ((UCHAR)(0x80))
#define TTLS_AVP_MANDITORY_FLAG  ((UCHAR)(0x40))
#define TTLS_AVP_FLAGS_POS       (24) 

/* AVP alignment macro */
#define TTLS_AVP_PAD(out, in) (out) = ((((in & 0x03) ^ 0x03) + 1) & 0x03)

/* Inner authentication handler init type */ 
typedef SUPP_SESSION_OBJ * (* TTLS_AUTH_HDLR)(); 

/* This enumeration defines RADIUS attributes as per rfc2865 and rfc2869
that are to be used for inner authentication */
typedef enum
    {
    TTLS_RADIUS_ATTR_USER_NAME     = 1,   /* RADIUS user name attribute */
    TTLS_RADIUS_ATTR_USER_PASSWORD = 2,   /* RADIUS password attribute */
    TTLS_RADIUS_ATTR_CHAP_PASSWORD = 3,   /* RADIUS CHAP password attribute */
    TTLS_RADIUS_ATTR_CHAP_CHALLENGE = 60, /* RADIUS CHAP challenge attribute */
    TTLS_RADIUS_ATTR_EAP_MESSAGE   = 79   /* RADIUS EAP message attribute */
    } TTLS_RADIUS_AVP_CODE;

/* This structure defines the AVP (Attribute Value Pair) packet 
header format used by TTLS (draft-ietf-pppext-eap-ttls-05.txt) for
passing data between the peer and authentiction server during the
inner authentication phase of a given TTLS session. */
typedef struct ttls_avp_pkt_hdr_s
    {
    UINT32 code;        /* 32 bits */
    UINT32 flagsLength; /* Flags [VMrrrrrr] r - 0 V - [0,1] M - length */
    } _WRS_PACK_ALIGN(1) TTLS_AVP_PKT_HDR;

/* This structure defines the AVP (Attribute Value Pair) packet 
format used by TTLS (draft-ietf-pppext-eap-ttls-05.txt) for
passing data between the peer and authentiction server during the
inner authentication phase of a given TTLS session when the 
vendor specific bit is set within the flags field of the 
packet header . */
typedef struct ttls_avp_vendor_pkt_s
    {
    TTLS_AVP_PKT_HDR header;   /* AVP packet header information */
    UINT32           vendorId; /* Vendor ID */
    char             message;  /* AVP message body */
    } _WRS_PACK_ALIGN(1) TTLS_AVP_VENDOR_PKT;

/* This structure defines the AVP (Attribute Value Pair) packet 
format used by TTLS (draft-ietf-pppext-eap-ttls-05.txt) for
passing data between the peer and authentiction server during the
inner authentication phase of a given TTLS session when the 
vendor specific bit is cleared within the flags field of the 
packet header. The message portion may contain inner authentication
packet data (i.e. PAP, CHAP packet) */
typedef struct ttls_avp_pkt_s
    {
    TTLS_AVP_PKT_HDR header;  /* AVP packet header information */
    char             message; /* AVP message body */
    } _WRS_PACK_ALIGN(1) TTLS_AVP_PKT;


/* TTLS inner authentication methods */
typedef enum
    {
    TTLS_INNER_AUTH_PAP = 0,    /* PAP authentication */
    TTLS_INNER_AUTH_CHAP,       /* CHAP authentication */
    TTLS_INNER_AUTH_MS_CHAP,    /* MS-CHAP authentication */
    TTLS_INNER_AUTH_EAP         /* EAP authentication */  
    } TTLS_INNER_AUTH_METHOD;

/* TTLS authentication states */
typedef enum
    {
    TTLS_AUTH_REQUEST_STATE = 0, /* Waiting for a request */
    TTLS_AUTH_RESPONSE_STATE     /* Waiting for a response */
    } TTLS_AUTH_STATE; 

/* Defines a ttls authentication object */
typedef struct ttls_auth_s
    {
    SUPP_SESSION_OBJ   curr;     /* Current Authentication object */
    SUPP_SESSION_OBJ   next;     /* Next Authentication object */
    SUPP_ENV_OBJ *     env;      /* Supplicant parameters */
    int                authType; /* TTLS inner auth type */
    } TTLS_SESSION_OBJ;

/* This structure defines the PAP control object */
typedef struct ttls_control_s
    {
    TTLS_SESSION_OBJ * session[TTLS_MAX_SESSIONS]; /* Allowed sessions */
    BOOL               sessionActive;
    } TTLS_CONTROL_OBJ;

void * dot1xSuppTtlsAuthInit(void *);

#ifdef __cplusplus
}
#endif

#endif /* __INCdot1xSuppTtlsLibh */
