/* dot1xEapolLib.h - 802.1X EAPOL Definitions */

/* Copyright 2002-2005 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01j,18mar05,eja  Added logging
01i,03mar05,eja  Fix prototype misspelling
01h,01mar05,eja  API change and file cleanup
01g,04feb05,eja  80211i/WPA integration
01f,12jan05,eja  Add PAE MAC address for wireline mode
01e,21dec04,eja  Add function prototype
01d,21apr03,ggg  File name, location change
01c,25apr02,mef  to clean up the code
01b,04mar02,mef  to add EAPOL Header Len Macro
01a,26Dec01,mef written.

*/

/*
DESCRIPTION

This file contains the definitions and the function prototype
that is used by EAPOL (EAP Over LAN) protcol layer.

*/

#ifndef __INCdot1xEapolLibh
#define __INCdot1xEapolLibh

#ifdef __cplusplus
extern "C" {
#endif

/* vxworks types */
#include <vxWorks.h>
#include <wrn/dot1x/dot1xMdLib.h>
#include <end.h>

/* typedefs */

/* DOT1X EAPOL Msg Types - For detailed Information check standard 
pg.15 7.5.4 */
typedef enum        
    {
    DOT1X_EAPOL_EAP_PACKET = 0, /* EAP Packet */
    DOT1X_EAPOL_START, /* EAPOL Start */
    DOT1X_EAPOL_LOGOFF, /* EAPOL Logoff */
    DOT1X_EAPOL_KEY, /* EAPOL Key */
    DOT1X_EAPOL_ASF_ALERT /* EAPOL Encapsulated ASF Alert */
    } DOT1X_EAPOL_PACKET_TYPE;

/* definitions */
#define SIOCGIFNDX _IOR('i', 130, struct ifreq) /* get ifnet index */
#define SIOCGIFTYPE _IOR('i', 140, struct ifreq) /* get if type */
#define SIOCGADDR EIOCGADDR /* get src MAC address */

/* dot1xEapol Receive Error Definitions */
#define DOT1X_EAPOL_RCV_INVALID_FRAME_ERR ((UINT32)0)
#define DOT1X_EAPOL_RCV_FRAME_LEN_ERR ((UINT32)1)

/* EAPOL Header Length */
#define DOT1X_EAPOL_HEADER_LEN 4
#define DOT1X_EAPOL_PROTOCOL_VERSION ((UCHAR)0x01)
#define DOT1X_EAPOL_PAE_MAC "\x01\x80\xC2\x00\x00\x03"

/* EAPOL, especially key handling 
(duplicated from dot1xEapolMuxAdapter.c) */
#define DOT1X_EAPOL_ETHERNET_HEADER_SIZE 4

/* Length of initialization vector for decrypting keys */
#define DOT1X_EAPOL_KEY_IV_SIZE 16

/* Length of timestamp in EAPOL Key messages */
#define DOT1X_EAPOL_KEY_REPLAY_COUNTER_SIZE 8

/* EAPOL Key type */
#define EAPOL_KEY_TYPE_WB4 1

/* Size of an EAPOL key descriptor without the key 
1 + 2 + 8 + 16 + 1 + 16 */
#define EAPOL_KEY_DESCRIPTOR_MIN_LENGTH  44  

/* Biggest key to handle for 802.11 */
#define EAPOL_KEY_MAX_SIZE DOT11_128_BIT_KEY_SIZE

/* Unicast keys have the high bit set in the index */
#define EAPOL_KEY_UNICAST_BIT 0x80
#define EAPOL_KEY_INDEX_MASK 0x7F

#define EAPOL_LOG_FATAL "(f) " __FILE__ " (s) FATAL (r) "
#define EAPOL_LOG_ERROR "(f) " __FILE__ " (s) ERROR (r) "
#define EAPOL_LOG_INFO  "(f) " __FILE__ " (s) INFO  (r) "
#define EAPOL_LOG_DEBUG "(f) " __FILE__ " (s) DEBUG (r) "

/* EAPOL header */
typedef struct
    {
    char version; /* EAPOL version */
    char type; /* DOT1X_EAPOL_PACKET_TYPE in dot1xEapolP.h */
    UINT16 length; /* EAPOL packet length */
    } _WRS_PACK_ALIGN(1) EAPOL_HDR;

/* EAPOL Key message */
typedef struct
    {
    EAPOL_HDR eapolHeader; /* EAPOL header needed for signature */
    char type; /* Type of key: WB4 = 0x01 */
    UINT16 length; /* Key size in bytes */
    char replay[DOT1X_EAPOL_KEY_REPLAY_COUNTER_SIZE]; /* Replay */
    char iv[DOT1X_EAPOL_KEY_IV_SIZE]; /* IV for decrypt of key */
    char index; /* Key slot.  Bit 8 set for unicast */
    char signature[HMAC_MD5_RESULT_LENGTH]; /* Signature HMAC-MD5 */
    char keyStart; /* Start key data. none: use session key */
    } _WRS_PACK_ALIGN(1) EAPOL_KEY_DESCRIPTOR;

/* Function Prototypes */
IMPORT STATUS dot1xEapolSend
    (
    VOID *cookie,/* EAPOL cookie of device  */
    char *dstMacAddress, /* destination MAC address */
    UINT16 packetType, /* EAPOL packet type */
    UINT16 packetLen, /* length of packet body */
    char* packetBody /* packet body */
    );
IMPORT VOID *dot1xEapolFindByDevice
    (
    INT32 unit, /* unit number */
    char* name /* device name */
    );
IMPORT STATUS dot1xEapolIoctl
    (
    VOID *cookie, /* cookie indentifying the device */
    INT32 cmd, /* command to pass to ioctl      */
    caddr_t data /* data needed for command in cmd  */                
    );
IMPORT STATUS dot1xEapolLibInit
    (
    VOID
    );
IMPORT STATUS dot1xEapolInit
    (
    UINT32 eapolMaxUnits /* max bindings */
    );
IMPORT STATUS dot1xEapolAttach
    (
    INT32 unit, /* unit number */
    char *name /* device name */
    );
IMPORT STATUS dot1xEapolDetach
    (
    INT32 unit, /* unit number */
    char *name /* device name */
    );
IMPORT STATUS dot1xEapolBind
    (
    INT32 unit,/* unit number of interface */
    char *name, /* device name of interface */
    VOID *cookie, /* cookie of Binding PDU */
    VOID *rcvRtn, /* function ptr to receive routine of protocol */
    VOID *rcvErrRtn /* function ptr to received packet error routine */
    );
IMPORT STATUS dot1xEapolUnbind
    (
    INT32 unit, /* unit number */
    char *name, /* device name */
    VOID *rcvRtn /* pointer to receive routine of upper layer protocol */
    );
IMPORT VOID *dot1xEapolIfNameGet
    (
    VOID *pObj /* Device object */ 
    );
IMPORT INT32 dot1xEapolIfNumGet
    (
    VOID *pObj /* Device object */ 
    );
STATUS dot1xEapolLogCallbackInstall
    (
    VOID *cookie,/* EAPOL cookie of device  */
    VOID *callback /* Device object */ 
    );

#ifdef __cplusplus
}
#endif

#endif /* __INCdot1xEapolLibh */

