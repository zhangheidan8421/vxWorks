/* ip6_ext_out.h - typedefs and externs for the external hooks
 * used by the IPv6 stack on the output side */

/* Copyright 2002 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,26apr05,kch  Added ipv6 multicast forwarding hook.
01b,20nov03,niq  Remove copyright_wrs.h file inclusion
01a,07aug03,nee  Merging to Base6 from ACCORDION_BASE6_MERGE_BASELINE label on
                 Accordion codeline

*/

#ifndef __INCip6ExtOuth
#define __INCip6ExtOuth

#ifdef __cplusplus
extern "C" {
#endif

/*
 * This file contains typedefs for some of the hooks used by the IPv6 stack.
 * The similar hooks used by the IPv4 stack can be found in a different file.
 * The functions are separated in order to minimize the include files required
 * to use all one set of functions.
 */

#ifdef INET6 
/* We don't do anything if IP6 isn't installed */

typedef int (*IPSEC_OUTPUT_IPV6_FUNCPTR) (struct mbuf        ** m,
                                          struct ip6_pktopts  * options,
                                          struct route_in6    * ro,
                                          int                   flags,
                                          struct ip6_moptions * imo,
                                          struct sockaddr_in6 * dest,
                                          struct ifnet        * ifp);


#ifndef VIRTUAL_STACK
/* We only define the externals and macros here if we aren't supporting
 * virtual stacks.  If we are supporting them then the corresponding
 * definitions are in vsIp6.h
 */

/* ipv6 multicast forwarding hook */
extern FUNCPTR _mCastRouteFwdHookIPv6;

/*
 * Externals for the currently defined hooks.
 */
extern IPSEC_OUTPUT_IPV6_FUNCPTR _ipsecOutputIPv6;
extern IPSEC_OUTPUT_IPV6_FUNCPTR _ipsecForwardIPv6;

#define SET_IPSECOUTPUTIPV6(foo)  (_ipsecOutputIPv6 = foo)
#define SET_IPSECFORWARDIPV6(foo) (_ipsecForwardIPv6 = foo)

#endif /* #ifndef VIRTUAL_STACK */

#endif /* INET6 */

#ifdef __cplusplus
}
#endif

#endif /* __INCip6ExtOuth */

