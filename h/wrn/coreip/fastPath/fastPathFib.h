/* fastPathFib.h - Definitions for the Fast Forwarder FIB  */

/* Copyright 1984-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01m,02oct04,niq  Performance changes
01l,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE
01k,20nov03,niq  Remove copyright_wrs.h file inclusion
01j,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01i,04nov03,rlm  Ran batch header path update for header re-org.
01h,15sep03,vvv  updated path for new headers
01g,20mar03,niq  Reorganize the locking model and fix locking bugs
01f,11mar03,niq  Use the RIB API to access RIB data structure
01e,03mar03,niq  Add some comments and use RIB macros to access RIB fields
01d,12feb03,deg  FIB_ENTRY_MAC_ADDR changed according sockaddr_dl structure
01c,24jan03,deg  #include "ptRouteNodeLib.h" and "avlRouteNodeLib.h" removed
01b,02dec02,deg  Lock macros added, FIB entry flags updated
01a,07oct02,deg  written
*/

/*
DESCRIPTION
This file contains FIB related definitions used by the IPv4 & IPv6 Fast 
Forwarder modules 

*/

#ifndef __INCfastpathFibh
#define __INCfastpathFibh

/* includes */

#include <route/ipRouteNodeLib.h>
#include <net/route.h>

/* extern */ 

IMPORT struct ipRouteDispatchTable ptRibDispatchTable;
IMPORT struct ipRouteDispatchTable avlRibDispatchTable;

/* defines */

#define FIB_CREATE               RIB_CREATE     
#define FIB_DESTROY              RIB_DESTROY
#define FIB_NODE_ADD             RIB_NODE_ADD
#define FIB_NODE_DELETE          RIB_NODE_DELETE 
#define FIB_NODE_BEST_MATCH      RIB_NODE_BEST_MATCH
#define FIB_NODE_EXACT_MATCH     RIB_NODE_EXACT_MATCH
#define FIB_NODE_LEX_NEXT_GET    RIB_NODE_LEX_NEXT_GET
#define FIB_NODE_LEX_NEXT_REPEAT RIB_NODE_LEX_NEXT_REPEAT

#define FIB_DISPATCH_TBL         struct ipRouteDispatchTable

#define FIB_FORMAT_IPV4          RIB_FORMAT_IPV4
#define FIB_FORMAT_IPV6          RIB_FORMAT_IPV6

/* Definitions for FIB entry flags */

#define FFF_UP	     RTF_UP	 /* 0x0001 */
#define FFF_INDIRECT RTF_GATEWAY /* 0x0002 */
#define FFF_HOST     RTF_HOST	 /* 0x0004 */
#define FFF_MAC	     RTF_LLINFO	 /* 0x0400 */
#define FFF_VALID    RTF_PROTO2	 /* 0x4000 */
#define FFF_COMPLETE RTF_PROTO1	 /* 0x8000 */

/* Fastpath defines these values for fastpath interface use */

#define FFF_USABLE     0x01000000	/* Route is usable (VALID & COMPLETE)*/
#define FFF_IF_ROUTE   0x02000000	/* Interface route */
#define FFF_PTP        0x04000000	/* PTP interface route */
#define FFNODE_DELETE  0x08000000	/* Internal: for delayed delete of FIB node*/

/* Mask to mask with the RTM flags */

#define FFF_FLAGS_MASK	(FFF_UP | FFF_INDIRECT | FFF_HOST | FFF_MAC)


/* Conversion macros between FIB and FF entry */

#define FIB2FF_ENTRY(x) ((FF_ENTRY_ID)(RIB_RTM_ENTRY_GET (x)))
#define FF2FIB_ENTRY(x) ((IP_NODE_ID)((x)->pFibNode))

/* Access macros on FIB entry member */

#define FF_ENTRY_DEST_ADDR(x) ((x)->pDstAddr)
#define FF_ENTRY_NET_MASK(x) ((x)->pNetmask)
#define FF_ENTRY_GATE_ADDR(x) ((x)->pGateway)	

#define FF_ENTRY_MAC_ADDR(x) (SDL ((x)->pGateway)->sdl_data)
#define FF_ENTRY_GATE_ROUTE(x) ((x)->pGwRoute)
#define FF_ENTRY_ROUTE_METRIC(x) 1
#define FF_ENTRY_IF_MTU(x) ((x)->ifMtu)
#define FF_ENTRY_IF_INDEX(x) ((x)->ifIndex)
#define FF_ENTRY_MUX_COOKIE(x) ((x)->pMuxCookie)
#define FF_ENTRY_IF_COOKIE(x) ((x)->pIfCookie)	
#define FF_ENTRY_USE_COUNT(x) ((x)->useCnt)
#define FF_ENTRY_REFCNT(x) ((x)->refCnt)
#define FF_ENTRY_FLAGS(x) ((x)->flags)

/* 
 * Macros to test if cache entry is direct, mac, complete, has a gateway route,
 * is up, valid, an interface route, complete and valid, full cache valid
 */

#define IS_FF_ENTRY_INDIRECT(x) ((FF_ENTRY_FLAGS (x) & FFF_INDIRECT))
#define IS_FF_ENTRY_MAC(x)      ((FF_ENTRY_FLAGS (x) & FFF_MAC))
#define IS_FF_ENTRY_COMPLETE(x) ((FF_ENTRY_FLAGS (x) & FFF_COMPLETE))
#define IS_FF_ENTRY_UP(x) (FF_ENTRY_FLAGS (x) & FFF_UP)
#define IS_FF_ENTRY_VALID(x) (FF_ENTRY_FLAGS (x) & FFF_VALID)
#define IS_FF_ENTRY_USABLE(x) (FF_ENTRY_FLAGS (x) & FFF_USABLE)
#define IS_FF_ENTRY_IF_ROUTE(x) (FF_ENTRY_FLAGS (x) & FFF_IF_ROUTE)
#define IS_GATEWAY_ROUTE_SET(x) FF_ENTRY_GATE_ROUTE (x)

/*
 * An entry is COMPLETE_AND_VALID if it is both
 *   valid : the interface is up and enabled for fast forwarding   AND
 *   COMPLETE : The forwarding information is available
 */

#define IS_FF_ENTRY_COMPLETE_AND_VALID(x)	 \
               ((FF_ENTRY_FLAGS (x) & (FFF_VALID | FFF_COMPLETE)) \
		== (FFF_VALID | FFF_COMPLETE))

/* 
 * If it is a direct entry, is the entry valid, else if the
 * gateway route is set, is the gateway route entry valid
 */

#define IS_FULL_CACHE_ENTRY_VALID(x) \
               (IS_FF_ENTRY_DIRECT (x) ? IS_FF_ENTRY_VALID (x) : \
               (IS_GATEWAY_ROUTE_SET (entry) && \
               IS_FF_ENTRY_VALID (FF_ENTRY_GATE_ROUTE (x))))


/* Macros to manipulate the DELAYED_DELETE flag */
#define MARK_FIB_NODE_DELAYED_DELETE(x)	((x)->delayedDelete)++
#define CLEAR_FIB_NODE_DELAYED_DELETE(x)	((x)->delayedDelete)--
#define FIB_NODE_DELAYED_DELETE_MARKED(x)    ((x)->delayedDelete)

/* Macros to manipulate the FFNODE_DELETE flag */
#define MARK_FIB_NODE_DELETE(x)	 \
	  ((x)->flags |= FFNODE_DELETE)

#define CLEAR_FIB_NODE_DELETE(x)	 \
	  ((x)->flags &= ~FFNODE_DELETE)
#define FIB_NODE_DELETE_MARKED(x)	 \
	  ((x)->flags & FFNODE_DELETE)

/* typedefs */

typedef struct ipRibHead FIB_COOKIE;
typedef FIB_COOKIE *FIB_COOKIE_ID;
typedef struct ipRouteNode FIB_ENTRY;
typedef FIB_ENTRY *FIB_ENTRY_ID;

typedef struct ffEntry
{
  void            *pFibNode;
  struct sockaddr *pDstAddr;
  struct sockaddr *pNetmask;
  struct sockaddr *pGateway;
  struct ffEntry  *pGwRoute;
  unsigned int     flags;
  unsigned int     useCnt;
  unsigned int     refCnt;
  void            *pIfCookie;   /* maybe struct ifnet * */
  unsigned int     ifMtu;       /* was struct rt_metrics */  
  void            *pMuxCookie;  /* maybe struct ifaddr * */
  unsigned int     ifIndex; 
  int		  delayedDelete;
} FF_ENTRY;

typedef FF_ENTRY *	FF_ENTRY_ID;
  
typedef struct ffCacheEntry
    {
    SOCKADDR_STORAGE_T	dstAddr;
    FF_ENTRY_ID		pFFEntry;
    FF_ENTRY_ID		pFFEntryGw;
    } FF_CACHE_ENTRY;

#endif /* __INCfastpathFibh */
