/* fastPathIpLib.h - Backward compatibility with the previous Fastpath Interface  */

/* Copyright 1984-2001 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,04nov03,rlm  Ran batch header path update for header re-org.
01d,07aug01,niq  Add more prototypes
01c,02may01,niq  Correcting mod history
01b,01may01,niq  Copying over file from tor2_0.open_stack branch version 01d
                 (fastPathIpLib.h@@/main/tor2_0.open_stack/4)
01a,29mar01,spm  file creation: copied from version 01a of tor2_0.open_stack
                 branch (wpwr VOB) for unified code base; fixed title entry

*/

/*
DESCRIPTION
 
This include file contains definitions used for backward compatibility with
the previous Fastpath interface. This should not be used by new applications
as it might be phased out in later releases.
 
INCLUDE FILES:
*/

#ifndef _INCfastPathIpLibh
#define _INCfastPathIpLibh

#ifdef __cplusplus
extern "C" {
#endif


#include <netBufLib.h> 
#include <routeEnhLib.h>
#include <fastPath/fastPathLib.h> 
#include <fastPath/fastPathIp.h> 


IMPORT STATUS ffInit(void);
IMPORT STATUS ffShutdown(void);
IMPORT STATUS ffModeSet(int mode);
IMPORT BOOL   ffModeGet(void);
IMPORT STATUS ffIntEnable(char *pIfName, int unitNo);
IMPORT STATUS ffIntDisable(char *pIfName, int unitNo);
IMPORT void   ffCacheShow(void);
IMPORT STATUS ffCachePopulate(void);
IMPORT STATUS ffCacheFlush(void);

#ifdef __cplusplus
}
#endif

#endif /* __INCfastPathIpLibh */
