/* ftpLibP.h - private header for ftpLib */

/* Copyright 1984-1992 Wind River Systems, Inc. */

/*
modification history
--------------------
01f,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01e,04nov03,rlm  Ran batch header path update for header re-org.
01d,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01c,14feb02,ant  added routines ftpUserPassSet, ftpUserPassPrint
01b,22sep92,rrr  added support for c++
01a,20sep92,kdl	 written.
*/

#ifndef __INCftpLibPh
#define __INCftpLibPh

#ifdef __cplusplus
extern "C" {
#endif

#include <ftpLib.h>

#if defined(__STDC__) || defined(__cplusplus)

extern STATUS	ftpLs (char *dirName);

#else

extern STATUS	ftpLs ();

#endif  /* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCftpLibPh */
