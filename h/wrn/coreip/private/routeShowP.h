/* routeShowP.h - private header for routeShow.c */

/* Copyright 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,24sep04,dlk  Move name-polluting macros to routeShow.c, and
                 rename typedefs (SPR #102647).
01a,20sep04,dlk	 written.
*/

#ifndef __INCrouteShowPh
#define __INCrouteShowPh

#ifdef __cplusplus
extern "C" {
#endif

/* Route show buffering styles */

#define ROUTE_BUFFER_NONE	1
#define ROUTE_BUFFER_SNPRINTF	2
#define ROUTE_BUFFER_NBIO	3

/* typedefs */

typedef STATUS (*ROUTE_SHOW_BEGIN_FUNC) (void * pContext, int fd, int timeout);
typedef STATUS (*ROUTE_SHOW_END_FUNC) (void * pContext);
typedef int (*ROUTE_SHOW_APPEND_FUNC) (void * pContext,  const char * fmt,
				       ...);
typedef STATUS (*ROUTE_SHOW_FLUSH_FUNC) (void * pContext);
typedef STATUS (*ROUTE_SHOW_SYNC_FUNC) (void * pContext);
typedef STATUS (*ROUTE_SHOW_DROP_FUNC) (void * pContext);
typedef void (*ROUTE_SHOW_FAIL_FUNC) (void * pContext);

typedef struct _ROUTE_SHOW_CTX
    {
    int rtType;
    void * pContext;
    ROUTE_SHOW_BEGIN_FUNC begin;
    ROUTE_SHOW_END_FUNC end;
    ROUTE_SHOW_APPEND_FUNC append;
    ROUTE_SHOW_FLUSH_FUNC flush;
    ROUTE_SHOW_SYNC_FUNC sync;
    ROUTE_SHOW_DROP_FUNC drop;
    ROUTE_SHOW_FAIL_FUNC fail;
    } ROUTE_SHOW_CTX;

extern BOOL routeShowFail;

extern ROUTE_SHOW_CTX routeShowCtx;

#ifdef __cplusplus
}
#endif

#endif /* __INCrouteShowPh */
