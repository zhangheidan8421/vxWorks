/* clarinet.h - misc definitions for Clarinet-Tornado integration */

/* Copyright 2002-2005 Wind River Systems, Inc. */
 
/*
modification history
--------------------
01m,27sep05,nee  removed types and htons etc.
01l,24jan05,vvv  added deprecation message
01k,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01j,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
01i,09feb04,niq  Add definitions for MEM* macros
01h,30sep03,rp   merging from base6_networking_itn2-int
01g,05may03,ppp  adding some type specific definations
01f,23apr03,pas  removed ptr_t
01e,11jul02,ham  removed int64_t
01d,06jun02,rvr  changed typedef of int64_t to long long 
01c,04mar02,ann  adding the macro for select list
01b,03feb02,ham  map BSD byte-order macros to vxWorks macros
01a,03feb02,ham  wrote.
*/

#ifndef __INCclarineth
#define __INCclarineth

#ifdef __cplusplus
extern "C" {
#endif

#warning "The use of this file is deprecated."

/* definitions from cdefs.h */
#if defined(__STDC__) || defined(__cplusplus)
#define __P(protos)     protos          /* full-blown ANSI C */
#else   /* !(__STDC__ || __cplusplus) */
#define __P(protos)     ()              /* traditional C preprocessor */
#endif

#if defined(__cplusplus)
#define __BEGIN_DECLS   extern "C" {
#define __END_DECLS     }
#else
#define __BEGIN_DECLS
#define __END_DECLS
#endif

#define LOCK		SEM_ID

#define OBTAIN_LOCK(a,b)	semTake((a),(b))
#define RELEASE_LOCK(a)		semGive(a)
#define SELECT_LIST             SEL_WAKEUP_LIST

#ifndef MEMCPY
#define MEMCPY(x, y, z)		memcpy((x), (y), (z))
#endif

#ifndef MEMCMP
#define MEMCMP(x, y, z)		memcmp((x), (y), (z))
#endif

#ifndef MEMSET
#define MEMSET(x, y, z)		memset((x), (y), (z))
#endif

#ifndef BCOPY
#define BCOPY(src, dst, nbytes) bcopy((const char *)(src), (char *)(dst), (int)(nbytes))
#endif
 
#ifndef BCMP
#define BCMP(buf1, buf2, nbytes) bcmp((char *)(buf1), (char *)(buf2), (int)(nbytes))
#endif

#ifndef BZERO
#define BZERO(buf, nbytes)		bzero((char *)(buf), (int)(nbytes))
#endif
 
#ifndef STRLEN
#define STRLEN(s)               strlen(s)
#endif
 
#ifndef STRCMP
#define STRCMP(s1, s2)          strcmp((s1), (s2))
#endif

#ifndef STRNCMP
#define STRNCMP(s1, s2, s3)     strncmp((s1), (s2), (s3))
#endif

#ifndef STRICMP
#define STRICMP(s1, s2)         stricmp((s1), (s2))
#endif

#ifndef STRNICMP
#define STRNICMP(s1, s2, sz)   strnicmp((s1), (s2), (sz))
#endif

#ifndef STRCASECMP
#define STRCASECMP(s1, s2)      stricmp((s1), (s2))
#endif

#ifndef STRNCASECMP
#define STRNCASECMP(s1, s2, sz) strnicmp((s1), (s2), (sz))
#endif

#ifndef STRCPY
#define STRCPY(s1, s2)          strcpy((s1), (s2))
#endif

#ifndef STRNCPY
#define STRNCPY(src, dst, sz)	strncpy(src, dst, sz)
#endif

/*
 * Round p (pointer or byte index) up to a correctly-aligned value
 * for all data types (int, long, ...).   The result is unsigned int
 * and must be cast to any desired pointer type. This definition is 
 * available in the different arch-specific copies of param.h but it
 * is safe to have a common definition here since all the architectures
 * define this the same way.
 */

#ifndef _ALIGNBYTES
#define _ALIGNBYTES     (sizeof (int) - 1)
#endif

#ifndef _ALIGN
#define _ALIGN(p)       (((unsigned) (p) + _ALIGNBYTES) & ~_ALIGNBYTES)
#endif

#ifdef __cplusplus
}
#endif

#endif /* __INCclarineth */
