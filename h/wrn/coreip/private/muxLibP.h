/* muxLibP.h - private definitions for the MUX library */

/* Copyright 1998-2002 Wind River Systems, Inc. */

/*
modification history
--------------------
02i,25aug05,dlk  Add section tags.
02h,27jul05,dlk  Added muxEndRxJobQueue(). May be temporary.
02g,21nov03,wap  Merge in changes from Snowflake
02f,20nov03,niq  Remove copyright_wrs.h file inclusion
02e,04nov03,rlm  Ran batch header path update for header re-org.
02d,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
02c,01jul03,ann  removed obsoleted code
02b,10sep01,ann  correcting the directory paths
02a,15may01,ann  major changes as per the MUX design doc for clarinet
                 - obsoleted the BIB and all its associated routines.
01r,25feb01,rae  RFC2233 changes
01q,19apr00,ham  merged TOR2_0-NPT-FCS (01a,06oct98,sj-01p,05oct99,pul).
01p,05oct99,pul  removing rtRequest and mCastMap
01o,29apr99,pul  Upgraded NPT phase3 code to tor2.0.0
01n,24mar99,sj   changed tkFlag field in muxBindEntry to flags; added defines
01m,24mar99,sj   added defines for flags field of NET_PROTOCOL structure
01l,18mar99,sj   cleaned up: completed the cleanup postponed in 01j,05mar99,sj
01k,08mar99,sj   added place holder for stackrcv routine in BIB entry
01j,05mar99,sj   eliminated hooks; not cleanly though. next version will do that
01i,24feb99,sj   added netSvcType and netDrvType fields to the BIB entry
01h,26jan99,sj   moved BIB entry definition from muxTkLib.c to here
01g,05nov98,sj   PCOOKIE_TO_ENDOBJ must check if pCookie is NULL
01f,03nov98,pul  modified IPPROTO_MCAST_MAP_GET 
01e,03nov98,sj   doc update
01d,20oct98,pul	 don't return ERROR if hook is not valid
01c,15oct98,pul	 added ipProto Hooks 
01b,12oct98,sj 	 changed MUX_TK_ADDR_RES_FUNC_UPDATE, added WRS_*_MODULE 
01a,06oct98,sj 	 written.

*/
 
/*
DESCRIPTION
This file includes macros used by muxLib to avail features of the NPT
architecture if it is installed.


INCLUDE FILES:
*/

#ifndef __INCmuxLibPh
#define __INCmuxLibPh

#ifdef __cplusplus
extern "C" {
#endif

#include <vxWorks.h>
#include <muxLib.h>

/* we steal these defines from a END driver */

#define END_IOCTL(pEnd)                                             \
		(pEnd->pFuncTable->ioctl)

/* Alternate defines for the above till all drivers have been changed 
 * to follow rfc2233 implementation
 */
#define END_ALT_HADDR(pEnd) \
                ((pEnd)->pMib2Tbl->m2Data.mibIfTbl.ifPhysAddress.phyAddress)
#define END_ALT_HADDR_LEN(pEnd) \
                ((pEnd)->pMib2Tbl->m2Data.mibIfTbl.ifPhysAddress.addrLength)


/*
 * The following three #defines will be OBSOLETED. Do not use.
 * Use the RFC2233 #defines instead
 */

#define END_HADDR(pEnd)                                                 \
                ((pEnd)->mib2Tbl.ifPhysAddress.phyAddress)

#define END_HADDR_LEN(pEnd)                                             \
                ((pEnd)->mib2Tbl.ifPhysAddress.addrLength)

#define END_MIB2_TYPE(pEnd)                                             \
                ((pEnd)->mib2Tbl.ifType)

#define BIND_STYLE_MUXBIND	0
#define BIND_STYLE_MUXTKBIND	1

typedef struct proto_entry PROTO_ENTRY; /* forward declaration typedef */

/*
 * The PROTOCOL_BINDING structure represents the binding of a protocol to
 * an END or NPT device. Additional or duplicate information, particularly
 * that needed for receive, is stored in the PROTO_ENTRY structure in the
 * interface's protocol table. The PROTO_ENTRY structure in turn contains
 * a pointer back to this structure.
 */
typedef struct protocol_binding
    {
    END_OBJ * pEnd;		    /* Interface to which protocol is bound */
    PROTO_ENTRY * pEntry;	    /* information needed by receive */
    UINT16 netSvcType;              /* Network service type */
    UINT8 flags;
    UINT8 bindStyle;                /* BIND_STYLE_{MUXBIND|MUXTKBIND} */
    void * netCallbackId;	    /* Callback ID passed to muxBind */
    BOOL (*stackRcvRtn) ();         /* protocol's receive routine */
    STATUS (*stackTxRestartRtn) (); /* Callback for restarting blocked tx. */
    STATUS (*stackShutdownRtn) ();  /* The routine to call to shutdown */
				    /* the protocol stack. */
    void (*stackErrorRtn) ();       /* Callback for device errors/events */
    long netIfType;		    /* Interface type */
    FUNCPTR addrResFunc;	    /* Address resolution function */
    char name[END_PROTO_NAME_MAX];  /* String name for this protocol. */
    } PROTOCOL_BINDING;

/*
 * Each device has a table of PROTO_ENTRY structures corresponding to
 * the protocols bound to it. This structure contains information needed
 * for receive, as well as a pointer to the corresponding PROTOCOL_BINDING
 * structure. The caching of information needed for receive in the small
 * PROTO_ENTRY structure is done purely for performance reasons.
 *
 * The protocol receive routine stored in the PROTO_ENTRY is in certain
 * cases a wrapper function around the actual protocol receive routine.
 */

struct proto_entry
    {
    UINT16 type;
    UINT16 reserved;

    union	/* protocol receive routine */
	{
	BOOL (*endRcv) (void * pCookie, long type, M_BLK_ID pMblk,
			LL_HDR_INFO * pLinkHdrInfo, void * pSpare);
	BOOL (*nptRcv) (void * pCookie, long type, M_BLK_ID pMblk,
			void * pSpareData);
	} rr;

    void * recvRtnArg;

    PROTOCOL_BINDING * pBinding;
    };

/* A little bit of backwards compatibility */

typedef PROTOCOL_BINDING * MUX_ID;

/*
 * Convert from a void * cookie (which is a PROTOCOL_BINDING) to an
 * END_OBJ pointer:
 */

#define PCOOKIE_TO_ENDOBJ(pCookie) ((pCookie) ? ((MUX_ID)pCookie)->pEnd : NULL)

/*
 * Note, a device cookie returned by muxDevLoad(), passed to muxDevStart(),
 * etc., is not the same as a binding cookie returned by muxBind().
 * Presently, a device cookie is just a pointer to the END_OBJ.
 */

#define PDEVCOOKIE_TO_ENDOBJ(pCookie) ((END_OBJ *)(pCookie))

/*
 * These macros are used by muxAddrRecFuncXXX functions
 */
 #define TK_ADDR_RES_GET 1
 #define TK_ADDR_RES_ADD 2
 #define TK_ADDR_RES_DEL 3

/* global variables */ /* XXX dlk - needed? */

extern SEM_ID muxLock;

/* prototypes */

extern STATUS muxTkLibInit (void);
extern void * muxTkBindCommon (int bindStyle, char * pName, int unit,
			       BOOL (*stackRcvRtn) (),
			       STATUS (*stackShutdownRtn) (),
			       STATUS (*stackTxRestartRtn) (),
			       void   (*stackErrorRtn) (), long type,
			       char * pProtoName, void * pNetCallbackId,
			       void * pNetSvcInfo, void * pNetDrvInfo);

PROTO_ENTRY * muxEndProtoRegister (PROTOCOL_BINDING * pBinding,
				   BOOL (*recvWrapper) ());
STATUS muxEndProtoFree (PROTOCOL_BINDING * pProto);
long muxIfTypeGet (END_OBJ * pEnd);

_WRS_FASTTEXT
STATUS _muxTkSendEnd (END_OBJ * pEnd, M_BLK_ID pNBuff, char * dstMacAddr,
		      USHORT netType, void * pSpareData, FUNCPTR sendRtn);
_WRS_FASTTEXT
STATUS _muxTkSendNpt (END_OBJ * pEnd, M_BLK_ID pNBuff, char * dstMacAddr,
		      USHORT netType, void * pSpareData, FUNCPTR sendRtn);

extern JOB_QUEUE_ID  muxEndRxJobQueue (END_OBJ * pEnd);

#ifdef __cplusplus
}
#endif

#endif /* __INCmuxLibPh */
