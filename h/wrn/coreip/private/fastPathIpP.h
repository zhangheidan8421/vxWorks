/* fastPathIpP.h - Private definitions for the IP Fast Forwarder module  */

/* Copyright 1984-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01g,02oct04,niq  Performance changes
01f,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01e,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
01d,20nov03,niq  Remove copyright_wrs.h file inclusion
01c,11nov03,cdw  Removal of unnecessary _KERNEL guards.
01b,06nov03,rlm  Ran batch header update for header re-org.
01a,12mar03,niq  written - consolidated v4/v6 files
*/

/*
DESCRIPTION
This file contains private definitions used only by the IP Fast Forwarder
module.
 
*/

#ifndef __INCfastPathIpPh
#define __INCfastPathIpPh

/* includes */

#include <fastPath/fastPathLib.h>

/* defines */

/* Access macros for the IP Fast Forwarder object */

#define IP_FF_FIB_COOKIE(pFFObj) \
	(((IP_FF_OBJ_ID)(pFFObj))->ipFFFibCookie)
#define IP_FF_ONE_LEVEL_CACHE(pFFObj) \
  	(&(((IP_FF_OBJ_ID)(pFFObj))->ipFFOneLevelCache))
#define IP_FF_FLUSH_ONE_LEVEL_CACHE(pFFObj) \
  	IP_FF_ONE_LEVEL_CACHE(pFFObj)->pFFEntryGw = NULL

/* typedefs */

/* IP Fast Forwarder object - contains the generic Fast Forwarder object */

typedef struct ipFFObj
{
  FF_OBJ		ipFFObj;	    /* Generic Fast Forwarder object */
  void   *		ipFFFibCookie;      /* FIB cookie */ 
  FF_CACHE_ENTRY   	ipFFOneLevelCache;  /* One level cache */
} IP_FF_OBJ;

typedef struct ipFFObj *IP_FF_OBJ_ID;

typedef struct userFuncArg
{
  FF_USER_WALK_FUNC_ID	pUserFunc;
  void                 *pUserArg;
} USER_FUNC_ARG;

#endif /* __INCfastPathIpPh */
