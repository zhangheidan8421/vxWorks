/* gifNpt.h - RFC2893 Configured Tunnels pseudo interface header */

/* Copyright 2003 Wind River Systems, Inc. */
/*
modification history
--------------------
01a,20mar03,kal  written
*/

#ifndef __INCgifNpth
#define __INCgifNpth

#ifdef __cplusplus
extern "C" {
#endif

#if defined(__STDC__) || defined(__cplusplus)
IMPORT END_OBJ * gifLoad (char * initString, void * pBsp);
#else
IMPORT END_OBJ * gifLoad ();
#endif /* defined(__STDC__) || defined(__cplusplus) */

#ifdef __cplusplus
}
#endif

#endif /* __INCgifNpth */
