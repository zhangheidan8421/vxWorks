/* vlanTagLib.h - defines for Layer 2 VLAN tagging library */

/*
 * Copyright (c) 1994-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */


/*
modification history
--------------------
01i,25aug05,kch  Changed formAddress and packetDataGet arguments for
                 ipVlanTagAttach() to have unspecified argument list.
                 Clean up unused prototypes.
01h,18mar05,kch  Corrected the SPR# for previous checkin.
01g,17mar05,kch  Removed ether_vlan_qtag_header structure definition
                 (SPR#106521).
01f,10sep04,kc   Added pVlanTagHdrCheck FUNCPTR to ipVlanTagAttach()
                 prototype.
01e,19jun04,kc   Fixed SPR#98401 - added vlanTagInstInit() and
                 vlanTagInstDestroy() prototypes.
01d,03jun04,kc   Added vlanTagIfIngressTypeSet(), vlanTagIfIngressTypeGet(),
                 vlanTagIfIngressRulesSet(), vlanTagIfIngressRulesGet(),
                 vlanTagIfEgressTypeSet(), vlanTagIfEgressTypeGet(),
                 vlanTagIfEgressRulesSet() and vlanTagIfEgressRulesGet()
                 prototypes. Removed the obsolete vlanTagIfFlagsSet() and 
                 vlanTagIfFlagsGet() prototypes.
01c,01jun04,kc   Added VLAN_INGRESS_ALL_FRAMES and VLAN_INGRESS_SKIP_TAG 
                 defines. Also added prototype for vlanTagSysDefaultVidSet().
                 Added typedef for the TOS-to-Vlan-Priority conversion function
                 pointer and redefined function arguments for the routine
                 vlanTagIfTosToPriorityRegister().
01b,19may04,kc   Fixed compiler warnings and issues from the code review.
                 Renamed all defines from IF8021PQ_XXX to VLAN_XXX.
                 Renamed all prototypes from if8021pqXxx() to vlanTagIfXxx().
                 Moved data structure and defines used internally to 
                 vlanTagLibP.h private header file. Also added defines for
                 VLAN_EGRESS_PRIORITY_FRAMES and VLAN_EGRESS_UNTAGGED_FRAMES 
                 as well as vlanTagLibInit() and vlanTagIfShow() prototypes.
01a,24mar04,chw  written.
*/
 
#ifndef __INCvlantagLibh
#define __INCvlantagLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <end.h>

/* this is the function pointer pointed to a user defined conversion function
 * for converting the IP TOS field to the 3-bits vlan priority. The 
 * conversion routine takes the IP TOS value as its input and returns the
 * vlan priority value (0-7) that needs to be inserted to the outgoing vlan tag
 */
typedef uchar_t (* TOS2_PRIORITY_FUNCPTR) (uchar_t tos);
              

/* defines */ 

/*
 * The acceptable egress frame type:
 * VLAN_EGRESS_TAGGED_FRAMES  - send tagged frame with non-zero vlan id only.
 *
 * VLAN_EGRESS_PRIORITY_FRAMES - send priority-tagged frame with vlan id zero.
 *                               If the VLAN_TAG_EGRESS_IP_TOS is not specified
 *                               as well, the value of the priority bits 
 *                               configured for the interface via 
 *                               vlanTagIfPriorityBitsSet() will be applied 
 *                               to the vlan priority bit fields.
 *
 * VLAN_EGRESS_UNTAGGED_FRAMES - send untagged frame only. If the interface 
 *                               is previously configured to send tagged or 
 *                               priority-tagged frames, this flag essentially 
 *                               unwinds the interface configuration to send
 *                               frames without any tag at all.
 */
#define VLAN_EGRESS_TAGGED_FRAMES      0x01  
#define VLAN_EGRESS_PRIORITY_FRAMES    0x02
#define VLAN_EGRESS_UNTAGGED_FRAMES    0x04

/*
 * The acceptable ingress frame type:
 * VLAN_INGRESS_TAGGED_FRAMES - receive tagged frame with non-zero vlan id 
 *                              only, untagged frame and priority frame (with
 *                              zero vlan id) will be discarded. If this flag
 *                              is not explicitly set, the interface will 
 *                              receive all the tagged, priority-tagged and 
 *                              untagged frames.
 *
 * VLAN_INGRESS_ALL_FRAMES - receive all frames (tagged, untagged and
 *                           priority-tagged).
 */
#define VLAN_INGRESS_TAGGED_FRAMES     0x01 
#define VLAN_INGRESS_ALL_FRAMES        0x02

/* 
 * Ingress rules that apply to every received frame:
 * VLAN_INGRESS_SKIP_TAG - ignore the vlan tagged header if presnet. No vlan 
 *                         ingress filtering operations will be performed on 
 *                         the received frame to validate the vlan tag found 
 *                         in the received frame.
 */
#define VLAN_INGRESS_SKIP_TAG          0x01

/*
 * Egress rules that apply to every frame trasmitted:
 * VLAN_EGRESS_IP_TOS - send priority-tagged frame by converting the IP TOS 
 *                      fields to the vlan priority bit fields. The flag
 *                      VLAN_EGRESS_PRIORITY_FRAME flag must be used in 
 *                      conjunction with this flag.
 *
 * VLAN_EGRESS_CFI_SET - set the CFI bit in the vlan tagged header. The CFI 
 *                       bit always takes the value of 0 for ethernet and 1 
 *                       for token ring.
 */

#define VLAN_EGRESS_IP_TOS             0x01
#define VLAN_EGRESS_CFI_SET            0x02

/* Structure definitions */  

/* 
 * The structure definition for the 802.1p/q vlan tagged ethernet header
 * is now defined in if_ether.h::struct ether_tag_header
 */   

typedef M_BLK_ID (*ADDR_FORM_FUNC) (M_BLK_ID, M_BLK_ID, M_BLK_ID, BOOL); 

typedef STATUS (*PACKET_DATA_GET_FUNC) (M_BLK_ID, LL_HDR_INFO *); 

/* Forward declarations */ 

IMPORT void vlanTagLibInit (void);

IMPORT STATUS vlanTagIfIngressTypeSet (char * pDevName, uint32_t unit,
                                       ushort_t ingressType, char * pIfAddress);
IMPORT STATUS vlanTagIfIngressTypeGet (char * pDevName, uint32_t unit, 
                                       ushort_t * pIngressType, 
                                       char * pIfAddress);
     
IMPORT STATUS vlanTagIfEgressTypeSet (char * pDevName, uint32_t unit,
                                      ushort_t egressType, char * pIfAddress);
IMPORT STATUS vlanTagIfEgressTypeGet (char * pDevName, uint32_t unit, 
                                      ushort_t * pEgressType, 
                                      char * pIfAddress);

IMPORT STATUS vlanTagIfIngressRulesSet(char * pDevName, uint32_t unit, 
                                       ushort_t ingressRules, 
                                       char * pIfAddress);
IMPORT STATUS vlanTagIfIngressRulesGet(char * pDevName, uint32_t unit, 
                                       ushort_t * pIngressRules, 
                                       char * pIfAddress);

IMPORT STATUS vlanTagIfEgressRulesSet (char * pDevName, uint32_t unit, 
                                       ushort_t egressRules, char * pIfAddress);
IMPORT STATUS vlanTagIfEgressRulesGet (char * pDevName, uint32_t unit, 
                                       ushort_t * pIngressRules, 
                                       char * pIfAddress);

IMPORT STATUS vlanTagIfVidSet (char * pDevName, uint32_t unit, 
                               ushort_t vlanId, char * pIfAddress); 
IMPORT STATUS vlanTagIfVidGet (char * pDevName, uint32_t unit, 
                               ushort_t * pVlanId, char * pIfAddress); 

IMPORT STATUS vlanTagIfPriorityBitsSet (char * pDevName, uint32_t unit, 
                                        uchar_t priorityBits, 
                                        char * pIfAddress); 
IMPORT STATUS vlanTagIfPriorityBitsGet (char * pDevName, uint32_t unit, 
                                        uchar_t * pPriorityBits,
                                        char * pIfAddress); 

IMPORT STATUS vlanTagIfTosToPriorityRegister (char * pDevName, uint32_t unit, 
                                      TOS2_PRIORITY_FUNCPTR tos2PriorityConvRtn,
                                      char * pIfAddress); 

IMPORT STATUS vlanTagIfShow (char * pDevName, uint32_t unit, 
                             char * pIfAddress);

IMPORT STATUS ipVlanTagAttach (int unit, char *pDevName, BOOL updateFuncs, 
                               M_BLK_ID (*formAddress)(),
                               STATUS (*packetDataGet)(),
                               FUNCPTR  pVlanTagHdrCheck); 

#ifdef __cplusplus
}
#endif

#endif /* __INCvlantagLibh */

