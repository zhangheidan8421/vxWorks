/* stfNpt.h - 6to4 Pseudo-NPT Driver header */

/* Copyright 2003 Wind River Systems, Inc. */
/*
modification history
--------------------
01a,20mar03,kal  written
*/

#ifndef __INCstfNpth
#define __INCstfNpth

#ifdef __cplusplus
extern "C" {
#endif

#if defined(__STDC__) || defined(__cplusplus)
IMPORT END_OBJ * stfLoad (char * initString, void * pBsp);
#else
IMPORT END_OBJ * stfLoad ();
#endif /* defined(__STDC__) || defined(__cplusplus) */

#ifdef __cplusplus
}
#endif

#endif /* __INCstfNpth */
