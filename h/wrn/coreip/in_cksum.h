/* Copyright 1984-2005 Wind River Systems, Inc. */

#include "copyright_wrs.h"
/*
modification history
--------------------
01e,14jan05,vvv  osdep.h cleanup
01d,23sep04,kc   Added coldfire arch support.
01c,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5-int/2)
01b,08dec03,dat  fix CPU_FAMILY names
01a,04dec03,xli  creation
*/

/*
DESCRIPTION
This file is a wrapper of in_cksum.h which include specific in_cksum.h  
depend on the CPU_FAMILY
*/

#include <vxWorks.h>

/* The following includes are from target/h/arch */

#if(CPU_FAMILY==ARM)
#include <arch/arm/in_cksum.h>

#elif (CPU_FAMILY==MIPS) 
#include <arch/mips/in_cksum.h>

#elif (CPU_FAMILY==I80X86)
#include <arch/i86/in_cksum.h>

#elif (CPU_FAMILY==PPC)
#include <arch/ppc/in_cksum.h>

#elif (CPU_FAMILY==SH)
#include <arch/sh/in_cksum.h>

#elif (CPU_FAMILY==COLDFIRE)
#include <arch/coldfire/in_cksum.h>

#elif (CPU_FAMILY==SIMNT)
#include<arch/simnt/in_cksum.h>

#elif (CPU_FAMILY==SIMLINUX)
#include<arch/simlinux/in_cksum.h>

#elif (CPU_FAMILY==SIMSPARCSOLARIS)
#include <arch/simsolaris/in_cksum.h>

#endif


