/* sctpLib.h - configuration data structure for SCTP */

/*
 * Copyright (c) 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. No license to Wind River intellectual property rights
 * is granted herein. All rights not licensed by Wind River are reserved
 * by Wind River.
 */

/*
modification history
--------------------
01a,29jul05,kch  written
*/

#ifndef __INCsctpLibh
#define __INCsctpLibh

#include <cfgDefs.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct sctp_config_params
    {
    CFG_DATA_HDR cfgh;

    /* definitions from sctp_usrreq.c */
    int cfg_sctp_sendspace;
    int cfg_sctp_recvspace;
    int cfg_sctp_auto_asconf;
    int cfg_sctp_ecn;
    int cfg_sctp_ecn_nonce;
    int cfg_sctp_strict_sacks;
    int cfg_sctp_no_csum_on_loopback;
    int cfg_sctp_strict_init;
    int cfg_sctp_peer_chunk_oh;
    int cfg_sctp_max_burst_default;
    unsigned int cfg_sctp_max_chunks_on_queue;
    unsigned int cfg_sctp_delayed_sack_time_default;
    unsigned int cfg_sctp_heartbeat_interval_default;
    unsigned int cfg_sctp_pmtu_raise_time_default;
    unsigned int cfg_sctp_shutdown_guard_time_default;
    unsigned int cfg_sctp_secret_lifetime_default;
    unsigned int cfg_sctp_rto_max_default;
    unsigned int cfg_sctp_rto_min_default;
    unsigned int cfg_sctp_rto_initial_default;
    unsigned int cfg_sctp_init_rto_max_default;
    unsigned int cfg_sctp_valid_cookie_life_default;
    unsigned int cfg_sctp_init_rtx_max_default;
    unsigned int cfg_sctp_assoc_rtx_max_default;
    unsigned int cfg_sctp_path_rtx_max_default;
    unsigned int cfg_sctp_nr_outgoing_streams_default;
    u_int32_t cfg_sctp_debug_on;

    FUNCPTR	cfg_privInitSysctl;
    } SCTP_CONFIG_PARAMS;

typedef struct sctp6_config_params
    {
    CFG_DATA_HDR cfgh;

    FUNCPTR cfg_privInitSysctl;
    } SCTP6_CONFIG_PARAMS;

/* prototypes */

IMPORT STATUS sctpInstInit (void *InitValues);
IMPORT STATUS sctpv6InstInit (void *InitValues);

#ifdef VIRTUAL_STACK
IMPORT STATUS sctpDestructor (int vsNum);
#endif /* VIRTUAL_STACK */

#ifdef __cplusplus
}
#endif

#endif /* __INCsctpLibh */
