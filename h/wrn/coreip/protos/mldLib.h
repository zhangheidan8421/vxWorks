/* mldLib.h - configuration data for MLD */

/*
 * Copyright (c) 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,29apr05,rp   written
*/

#ifndef __INCmldLibh
#define __INCmldLibh

/* includes */


#include <cfgDefs.h>

/* defines */

/* typedefs */

typedef struct mld_config_params
    {
    CFG_DATA_HDR 	cfgh;
    int				cfg_mldmaxsrcfilter;
    int				cfg_mldsomaxsrc;
    int				cfg_mld_version;
    FUNCPTR			cfg_privInitSysctl;
    } MLD_CONFIG_PARAMS;

#endif /* __INCmldLibh */

