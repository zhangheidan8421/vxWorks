/* udpLib.h - VxWorks UDP header file */

/* Copyright 2002-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01h,09feb05,wap  Allow sysctl init routines to be scaled out
01g,20nov03,niq  Remove copyright_wrs.h file inclusion
01f,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01e,04nov03,rlm  Ran batch header path update for header re-org.
01d,15sep03,vvv  updated path for new headers
01c,21nov02,hgo  udpcksum not private anymore
01b,28aug02,kal  fixed location of cfgDefs.h
01a,18jun02,hgo  creation
*/

#ifndef __INCudpLibh
#define __INCudpLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */
#include <cfgDefs.h>

typedef struct udp_config_params {
    CFG_DATA_HDR         cfgh;

    /* defined in udp_usrreq.c */

    int	                 cfg_udpcksum;  		/* UDP protocol implementation.
                                                         * Per RFC 768, August, 1980 */
    int	                 cfg_udp_log_in_vain;
    int                  cfg_priv_udp_blackhole;
    u_long               cfg_udp_sendspace;
    u_long               cfg_udp_recvspace;
    FUNCPTR		 cfg_privInitSysctl;
    } UDP_CONFIG_PARAMS;


typedef struct udp6_config_params {
    CFG_DATA_HDR         cfgh;
    FUNCPTR		 cfg_privInitSysctl;
    } UDP6_CONFIG_PARAMS;

#ifdef __cplusplus
}
#endif

#endif /* __INCudpLibh */
