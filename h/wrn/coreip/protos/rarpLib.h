/* rarpLib.h - the default configuration parameter structure for RARP */

/* Copyright 2002 - 2003 Wind River Systems, Inc. */
/*
modification history
--------------------
01e,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01d,04nov03,rlm  Ran batch header path update for header re-org.
01c,15sep03,vvv  updated path for new headers
01b,28aug02,kal  fixed location of cfgDefs.h
01a,16jul02,vlk  created 
*/

#ifndef __INCrarpLibh
#define __INCrarpLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */
#include <cfgDefs.h>

/* typedefs */
typedef struct
    {
    BOOL                ipAddrInitialized;
    struct in_addr	ipAddr;
    END_OBJ             *pEndId;
    BOOL                rarpDebug;
    } RARPSTATE;

typedef struct rarp_config_params {

    CFG_DATA_HDR cfgh;

    RARPSTATE cfg_rarpState;

    } RARP_CONFIG_PARAMS;

#ifdef VIRTUAL_STACK
extern VS_REG_ID rarpRegistrationNum;
#endif /* VIRTUAL_STACK */
extern RARP_CONFIG_PARAMS rarpDefaultConfigParams;

#if defined(__STDC__) || defined(__cplusplus)
extern STATUS   rarpInstInit (void *);
#ifdef VIRTUAL_STACK
extern STATUS   rarpDestructor (VSNUM);
#endif

#else	/* __STDC__ */
extern STATUS   rarpInstInit ();
#ifdef VIRTUAL_STACK
extern STATUS   rarpDestructor ();
#endif

#endif	/* __STDC__ */


#ifdef __cplusplus
}
#endif


#endif /* !INCrarpLibh */

