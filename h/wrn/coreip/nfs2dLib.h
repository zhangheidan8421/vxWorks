/* nfs2dLib.h - Network File System Server library header */

/* Copyright 1994 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,08aug03,bmk  added extern declaration for group_export (SPR #4220)
01c,21apr94,jmm  more cleanup of prototypes; added NFSD_ARGUMENT
01b,20apr94,jmm  added new prototypes
01a,31mar94,jmm  written.
*/

#ifndef __INCnfsdLibh
#define __INCnfsdLibh

#ifdef __cplusplus
extern "C" {
#endif

#include "limits.h"
#include "xdr_nfs2.h"

/* Extern declaration for group_export */

extern int group_export;

typedef struct
    {
    int    volumeId;        /* Volume identifier of the File System */
    ULONG  fsId;            /* Inode of the exported directory */
    ULONG  inode;           /* Inode of the file as returned from stat */
    INT8   reserved[20];    /* NFS v2 File Handle has 32 bytes */
    } NFS2_FILE_HANDLE;

/* 
 * Structure to hold the statistics of
 * number of NFS v2 RPC calls received from remote clients. 
 */

typedef struct
    {
    int nullCalls;         /* Number of calls to NFSPROC_NULL */
    int getattrCalls;      /* Number of calls to NFSPROC_GETATTR */
    int setattrCalls;      /* Number of calls to NFSPROC_SETATTR */
    int rootCalls;         /* Number of calls to NFSPROC_ROOT */
    int lookupCalls;       /* Number of calls to NFSPROC_LOOKUP */
    int readlinkCalls;     /* Number of calls to NFSPROC_READLINK */
    int readCalls;         /* Number of calls to NFSPROC_READ */
    int writecacheCalls;   /* Number of calls to NFSPROC_WRITECACHE */
    int writeCalls;        /* Number of calls to NFSPROC_WRITE */
    int createCalls;       /* Number of calls to NFSPROC_CREATE */
    int removeCalls;       /* Number of calls to NFSPROC_REMOVE */  
    int renameCalls;       /* Number of calls to NFSPROC_RENAME */
    int linkCalls;         /* Number of calls to NFSPROC_LINK */
    int symlinkCalls;      /* Number of calls to NFSPROC_SYMLINK */ 
    int mkdirCalls;        /* Number of calls to NFSPROC_MKDIR */ 
    int rmdirCalls;        /* Number of calls to NFSPROC_RMDIR */
    int readdirCalls;      /* Number of calls to NFSPROC_READDIR */
    int statfsCalls;       /* Number of calls to NFSPROC_STATFS */
    } NFS2_SERVER_STATUS;

/* 
 * Union of Expected Argument for NFS v2 procedure 
 * call (wherever applicable).
 * Description against each argument not given as the member
 * name is descriptive enough.
 * The definitions of types are available in "xdr_nfs2.h" file
 */

typedef union
    {
    nfs_fh2       nfsproc_getattr_2_arg; 
    sattrargs     nfsproc_setattr_2_arg; 
    diropargs     nfsproc_lookup_2_arg;  
    nfs_fh2       nfsproc_readlink_2_arg;
    readargs      nfsproc_read_2_arg;   
    writeargs     nfsproc_write_2_arg;  
    createargs    nfsproc_create_2_arg; 
    diropargs     nfsproc_remove_2_arg; 
    renameargs    nfsproc_rename_2_arg; 
    linkargs      nfsproc_link_2_arg;   
    symlinkargs   nfsproc_symlink_2_arg;
    createargs    nfsproc_mkdir_2_arg;  
    diropargs     nfsproc_rmdir_2_arg;  
    readdirargs   nfsproc_readdir_2_arg;
    nfs_fh2       nfsproc_statfs_2_arg; 
    } NFS2D_ARGUMENT;

#ifdef __cplusplus
}
#endif

#endif /* __INCnfsdLibh */
