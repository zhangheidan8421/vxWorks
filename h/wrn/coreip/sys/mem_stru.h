/* mem_stru.c - support routines for allocating/de-allocating data structures */

/* Copyright 2001-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
02k,29mar05,spm  fixed virtual stack build error
02j,28jan05,wap  Fix cast for DS_FREE()
02i,18jan05,wap  Update DS_MALLOC() to avoid use of netClPoolIdGet()
02h,20nov03,niq  Remove copyright_wrs.h file inclusion
02g,04nov03,rlm  Ran batch header path update for header re-org.
02f,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
02e,24oct03,cdw  update include statements post header re-org.
02d,06aug03,nee  Accordion Base6 merge from ACCORDION_BASE6_MERGE_BASELINE
                 label
02c,23apr03,pas  removed ptr_t
02b,02dec02,spm  merged from WINDNET_IPV6-1_0-FCS label (ver 02b,06sep02,hsh)
02a,12jun02,pas  changed DS_MALLOC() to a wrapper around netClusterGet()
01g,13nov01,pas  added memory pool metrics
01f,21sep01,ppp  Chanding the _DEBUG_ option to DEBUGGING
01e,20sep01,qli  adding more data struct util functions
01d,06sep01,ppp  Changing the first parameter of dstru_mem_free from u_char* to
                 void*
01c,23aug01,ppp  Adding an include   
01b,07aug01,qli  changing a struct field name
01a,06aug01,qli  created
*/


#ifndef _MEM_STRU_H_
#define _MEM_STRU_H_

#include <netBufLib.h>
#include <net/mbuf.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef ALIGN_MEM
#define ALIGN_MEM(cp, n)   cp = (u_char *)(((u_long)cp + n - 1) & (~(n - 1)))
#endif

#ifndef ALIGN_SZ
#define ALIGN_SZ 	4
#endif

#ifndef VIRTUAL_STACK
extern CL_POOL_ID _dsMallocMap[];
#endif /* VIRTUAL_STACK */

#define DS_MALLOC(x, y) \
	(void *) clusterGet(_pNetSysPool, (CL_POOL_ID)_dsMallocMap[y])

#define DS_FREE(x, y) \
	clFree (_pNetSysPool, (char *)(x))

#ifdef __cplusplus
}
#endif

#endif
