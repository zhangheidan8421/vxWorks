/* sntpcLib.h - Simple Network Time Protocol client include file */

/* Copyright 1984-1997 Wind River Systems, Inc. */

/*
Modification history 
--------------------
01f,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01e,04nov03,rlm  Ran batch header path update for header re-org.
01d,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01c,25aug99,cno  Add extern "C" definition (SPR21825)
01b,15jul97,spm  code cleanup, documentation, and integration; entered in
                 source code control
01a,20apr97,kyc  written

*/

#ifndef __INCsntpch
#define __INCsntpch

/* includes */

#include <sntp.h>

/* defines */

#define S_sntpcLib_INVALID_PARAMETER         (M_sntpcLib | 1)
#define S_sntpcLib_INVALID_ADDRESS           (M_sntpcLib | 2)
#define S_sntpcLib_TIMEOUT                   (M_sntpcLib | 3)
#define S_sntpcLib_VERSION_UNSUPPORTED       (M_sntpcLib | 4)
#define S_sntpcLib_SERVER_UNSYNC             (M_sntpcLib | 5)

#define SNTP_CLIENT_REQUEST 0x0B             /* standard SNTP client request */

#ifdef __cplusplus
extern "C" {
#endif

#ifndef INET6
/* SNTP data retrived from SNTP IPv4 protocol message in mSntpcTimeGet */
typedef struct sntpData
    {
    unsigned char     stratum;                 
    char              poll;
    char              precision;
    } SNTP_DATA;

IMPORT STATUS mSntpcTimeGet (char *, u_int, struct timespec *, char *, 
                             SNTP_DATA *);

#endif

IMPORT STATUS sntpcInit (u_short, char *);
IMPORT STATUS sntpcTimeGet (char *, u_int, struct timespec *);

#ifdef __cplusplus
}
#endif

#endif /* __INCsntpch */

