/* dhcp6Lib.h - header file for DHCPv6 related definitions */

/* 
 * Copyright (c) 2004-2005 Wind River Systems, Inc. 
 * 
 * The right to copy, distribute, modify or otherwise make use 
 * of this software may be licensed only pursuant to the terms 
 * of an applicable Wind River license agreement. 
 */

/*
modification history
--------------------
01d,09jul05,ijm  Added authentication options and api prototypes
01c,10aug04,syy  Code review changes
01b,01jul04,syy  Add prevention of multiple inclusion
01a,24jun04,syy  Written
*/

#ifndef __INCdhcp6Libh
#define __INCdhcp6Libh

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

struct dhcp6sInitParams {
    u_int  ramDiskSize;
    u_int  downStreamPort;
    u_int  upStreamPort;
    u_int  priority;   
    u_int  dhcp6sSize;
    };

struct dhcp6cInitParams {
    char   *uplinkIfName;
    char   *downlinkIfName;
    u_int   downStreamPort;
    u_int   upStreamPort;
    u_int   priority;
    u_int   dhcp6cSize;
    char   *dhcp6cKeyRealm;  /* e.g., domain name such as "example.com" */
    char   *dhcp6cKeySecret; /* base 64 encoded secret */
    char   *dhcp6cKeyExpire; /* e.g., "forever", "600", "200" (seconds) */
    };

struct dhcp6rInitParams {
    char  *srcAddr;
    char  *servAddr;
    char  *uIfName;
    u_int  hopLimit;
    u_int  downStreamPort;
    u_int  upStreamPort;
    u_int  priority;
    u_int  dhcp6rSize;
    };

typedef enum {DNS_LIST=0, SIP_LIST, NTP_LIST, DNS_NAME_LIST,
              SIP_NAME_LIST, PREFIX_LIST} dh6InfoType_t;
    
/* function prototypes */

/* DHCPv6 client */

extern STATUS dhcp6c __P((char *ifname, int option, int debug_level));
extern STATUS dhcp6cStop __P((void));
extern STATUS dhcp6cAuthInfoSet (char * dhcp6cKeyRealm,
                                 char * dhcp6cKeySecret,
                                 char * dhcp6cKeyExpire);
extern void dhcp6cLibInit (char *uplinkName, char *downlinkName, u_int dPort,
                           u_int uPort, u_int priority, u_int taskSize,
                           char * dhcp6cKeyRealm, char *dhcp6cKeySecret,
                           char * dhcp6cKeyExpire);

/* DHCPv6 server */

extern STATUS dhcp6s __P((char *confFileName, /* configuration file name */
                          char  *ifName,      /* interface to monitor */
                          u_int  debugLevel   /* verbosity */
                        ));
extern STATUS dhcp6sStop __P((void));
extern STATUS dhcp6sLibInit (u_int ramDiskSize, u_int dPort, u_int uPort,
                             u_int priority, u_int taskSize);

/* DHCPv6 relay */

extern STATUS dhcp6relay __P((char *ifName, u_int debugLevel));
extern STATUS dhcp6rStop __P((void));
extern STATUS dhcp6rLibInit (char *srcAddr, char *servAddr, char *uIfName,
                             u_int hopLimit, u_int dPort, u_int uPort,
                             u_int priority, u_int taskSize);

extern int dh6cInfoGet (dh6InfoType_t, char **, int, int);
    
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* ! __INCdhcp6Libh */
