/* rtsolLib.h - Router Solicitation for IPv6 */

/* Copyright 1992-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01h,18jun04,niq  Virtualization changes
01g,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01f,04nov03,rlm  Ran batch header path update for header re-org.
01e,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01d,06aug03,nee  Accordion Base6 merge from ACCORDION_BASE6_MERGE_BASELINE
                 label
01c,10jun03,vvv  include netVersion.h
01b,16may02,kal  added rtsolStop()
01a,13Feb02,kal  written.
*/

#ifndef __INCrtsolLibh
#define __INCrtsolLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <netVersion.h>

#include <cfgDefs.h>

typedef struct rtsol_config_params {
    CFG_DATA_HDR         cfgh;

    /* only used for parameter string */

    char                *cfg_pParameterStr;
    } RTSOL_CONFIG_PARAMS;

#if defined(__STDC__) || defined(__cplusplus)

extern STATUS rtsolStart (char *cmdString);
extern STATUS rtsolStop (void);

#else	/* __STDC__ */

extern STATUS rtsolStart ();
extern STATUS rtsolStop ();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCrtsolLibh */
