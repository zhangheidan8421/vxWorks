/* ptRouteNodeLibh - Patricia tree definitions for the IP RIB */

/* Copyright 1984 - 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01g,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE
01f,10nov03,cdw  Rebase from base6 iteration 1 view
01e,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01d,04nov03,rlm  Ran batch header path update for header re-org.
01c,15sep03,vvv  updated path for new headers
01b,17oct02,spm  replaced private globals to permit multiple tree instances
01a,09oct02,spm  written
*/

/*
DESCRIPTION
This file includes the route entry structures for an implementation of
the IP Routing Information Base component that uses the Patricia tree
algorithm. 
*/

#ifndef _INCptRouteNodeLibh
#define _INCptRouteNodeLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <route/ipRouteNodeLib.h>
#include <net/radix.h>

IMPORT struct ipRouteDispatchTable ptRibDispatchTable;

struct ptRouteNode 
    {
    struct radix_node   rt_nodes [2];  /* Internal and leaf nodes for tree */
    ULONG		netmask;      /* netmask/prefix length */
    struct ipRouteNode 	ribNodeEntry;  /* Required common header for RIB */
    };

struct ptRibHead
    {
    struct ipRibHead ribHead;            /* Required common structure */
    struct radix_node_head * pRadixHead; /* Dispatch table and data for PT */
    struct radix_node_head * pMaskHead;  /* Shared storage for netmasks */
    char * pPrivateData;                 /* Temporary mask and boundary keys */

    ULONG routeFormat; 	/* supported route format, determines key size. */
    ULONG readLock1Id;   /* task holding lock for basic operations */
    ULONG readLock2Id;   /* task holding lock for table traversal */
    ULONG readLock1Count;   /* number of active locks within task */
    ULONG readLock2Count;
    };

/* Macros to access some Patricia tree routines and the root node. */

#define PT_RADIX_HEAD(pRibHead) \
   (( (struct ptRibHead *) (pRibHead))->pRadixHead)

#define RADIX_NODE_ADD(pRibHead) \
    PT_RADIX_HEAD(pRibHead)->rnh_addaddr

#define RADIX_NODE_DELETE(pRibHead) \
    PT_RADIX_HEAD(pRibHead)->rnh_deladdr

#define RADIX_NODE_MATCH(pRibHead) \
    PT_RADIX_HEAD(pRibHead)->rnh_matchaddr

#define RADIX_NODE_LOOKUP(pRibHead) \
    PT_RADIX_HEAD(pRibHead)->rnh_lookup

#define PT_ROOT_NODE(pRibHead) \
    PT_RADIX_HEAD(pRibHead)->rnh_treetop

/* Macros for some fields of the ptRouteNode structure */
 
#define PT_RADIX_DATA_NODE(pIpRouteNode) \
   ( ((struct ptRouteNode *) (pIpRouteNode))->rt_nodes[0])

#define ROUTE_ENTRY_KEY(pIpRouteNode) \
    (PT_RADIX_DATA_NODE(pIpRouteNode).rn_u.rn_leaf.rn_Key)

     /*
      * The PT implementation uses nodes within a separate tree
      * to store netmask values in a specific internal format.
      * Any RIB implementation also stores netmask values in a
      * predefined public format, accessible with the RIB_NETMASK_GET
      * macro.
      */

#define ROUTE_ENTRY_MASK_NODE(pIpRouteNode) \
    (PT_RADIX_DATA_NODE(pIpRouteNode).rn_u.rn_leaf.rn_Mask)

#ifdef __cplusplus
}
#endif

#endif /* _INCptRouteNodeLibh */
