/* avlLib.h - VxWorks AVL API definitions for the IP RIB */

/* Copyright 2002 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,24oct02,syy  Code cleanup.
01c,23oct02,niq  Comment out definitions of NTOHL and use the ones in
                 endian.h
01b,17oct02,syy  Added constants for IPv4.
01a,20sep02,syy  written
*/

/*
DESCRIPTION

*/

#ifndef _INCavlLibh
#define _INCavlLibh

#ifdef __cplusplus
extern "C" {
#endif

/* Length of IPv4 address in 8-bit bytes */
#define IPV4_ADDR_LEN        4

/* Length of IPv4 address in 32-bit unsigned long word */
#define IPV4_ADDR_ULONG_LEN  IPV6_ADDR_LEN / 4

/* Length of IPv4 address in bits */
#define IPV4_ADDR_BITS_LEN   IPV4_ADDR_LEN * 8
    
/* Length of IPv6 address in 8-bit bytes */
#define IPV6_ADDR_LEN        16

/* Length of IPv6 address in 32-bit unsigned long word */
#define IPV6_ADDR_ULONG_LEN  IPV6_ADDR_LEN / 4
    
/* Length of IPv6 address in bits */
#define IPV6_ADDR_BITS_LEN   IPV6_ADDR_LEN * 8
    
/*
 * Length of the longest printed form of an IPv6 address, in characters,
 * including a trailing null character.
 */
#define IPV6_ADDR_STRING_LEN sizeof("ffff:ffff:ffff:ffff:ffff:ffff:255.255.255.255")
#define NETMASK_STRING_LEN sizeof(ULONG)+1;
    
#ifdef __cplusplus
}
#endif

#endif /* _INCavlLibh */
