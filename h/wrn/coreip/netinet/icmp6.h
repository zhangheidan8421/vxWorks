/* icmp6.h - ICMPv6 header file */

/*
 * Copyright (c) -2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,29jun05,vvv  written
*/

#ifndef __INCicmp6h
#define __INCicmp6h

/*
 * This file has been created to satisfy the requirement in RFC3542 that
 * icmp6.h should be in netinet. The copy in netinet6 is retained for 
 * backward-compatbility.
 */

#include <netinet6/icmp6.h>

#endif /* not __INCicmp6h */
