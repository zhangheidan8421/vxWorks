/* vsNetCore.h - virtual stack data for core network variables */

/* Copyright 2000 - 2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01k,19jan05,wap  Add DS_MALLOC pool array
01j,04nov03,rlm  Ran batch header path update for header re-org.
01i,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01h,24oct03,cdw  update include statements post header re-org.
01g,17mar03,spm  renamed allocatedMemHead to fix SLIST_* embedded macros
01f,17oct02,ism  added allocatedMemHead, used in uipc_mbuf.c
01e,27aug02,kal  fixed tornado location of socket.h
01d,26aug02,kal  build party
01c,22aug02,kal  ported to Accordion
01b,26oct01,spm  isolated virtual stack data structures (SPR #71092);
                 renamed file and changed description to accurate meaning
01a,30mar01,spm  file creation: copied from version 01a of tor2_0.open_stack
                 branch (wpwr VOB) for unified code base
*/

#ifndef __INCvsNetCoreh
#define __INCvsNetCoreh


/* includes */

#include <net/domain.h>      /* for struct domain        */
#include <netLib.h> 	    /* for IP[6]_PROTO_NUM_MAX  */
#include <sys/socket.h>      /* for if.h                 */
#include <netinet/ipprotosw.h>   /* for struct protosw       */
#include <protos/ip6protosw.h>  /* for struct ip6protosw    */
#include <netinet/in.h>          /* Needed before ipProto.h  */
#include <net/if.h>          /* Needed before ipProto.h  */
#include <ipProto.h>     /* for IP_DRV_CTRL          */
#include <netBufLib.h>   /* for NET_POOL             */
#include <sys/queue.h>                /* for SLIST_HEAD           */
#include <sys/ds_conf.h> /* for MAX_NUM_POOLS */

/* defines */

/* typedefs */

typedef struct vs_net_core
    {

    /* Domain List */

    struct domain *     domains;                        /* uipc_domain.c    */

    /* inet[6]sw and associated variables */

    struct ipprotosw    inetsw [IP_PROTO_NUM_MAX];      /* in_proto.c       */
    struct ip6protosw   inet6sw [IP6_PROTO_NUM_MAX];    /* in6_proto.c      */

    int                 _protoSwIndex;          /* Configlette usrNetInit.c */
    int                 _proto6SwIndex;         /* Configlette usrNetInit.c */

    /* ipProto Stuff */

    IP_DRV_CTRL *   ipDrvCtrl;              /* Configlette usrNetIpProto.c  */
    unsigned int	ipMaxUnits;             /* Configlette usrNetIpProto.c  */

    /* Network Semaphore (per-stack) */
    
    int    splTid;			                                /* unixLib.c    */
    SEM_ID splSemId;		                                /* unixLib.c    */

    /* Mem management */

    NET_POOL  _netDpool; 	                                /* uipc_mbuf.c  */
    NET_POOL  _netSysPool;	                                /* uipc_mbuf.c  */

    NET_POOL_ID _pNetDpool;                                 /* uipc_mbuf.c  */
    NET_POOL_ID _pNetSysPool;                               /* uipc_mbuf.c  */

    int max_linkhdr;                                        /* uipc_mbuf.c  */
    int max_protohdr;                                       /* uipc_mbuf.c  */
    int max_hdr;                                            /* uipc_mbuf.c  */
    int nmbclusters;                                        /* uipc_mbuf.c  */ 

    void * _dsMallocMap [MAX_NUM_POOLS];                    /* uipc_mbuf.c  */

    SLIST_HEAD (memListHead, memList) _allocatedMemHead;

    void *priv_pffasttimoHandle;                            /* uipc_domain.c */
    void *priv_pfslowtimoHandle;                            /* uipc_domain.c */

    } VS_NET_CORE;

/* XXX Can we make IP6 stuff ifdef INCLUDE_IPV6 / INET6 only ? */

/* macros */

#define VS_CORE_DATA ((VS_NET_CORE *)vsTbl[myStackNum]->pCoreGlobals)

#define domains         VS_CORE_DATA->domains
#define inetsw          VS_CORE_DATA->inetsw
#define inet6sw         VS_CORE_DATA->inet6sw
#define _protoSwIndex   VS_CORE_DATA->_protoSwIndex
#define _proto6SwIndex  VS_CORE_DATA->_proto6SwIndex
#define ipDrvCtrl       VS_CORE_DATA->ipDrvCtrl
#define ipMaxUnits      VS_CORE_DATA->ipMaxUnits
#define splTid          VS_CORE_DATA->splTid
#define splSemId        VS_CORE_DATA->splSemId
#define _netDpool       VS_CORE_DATA->_netDpool
#define _netSysPool     VS_CORE_DATA->_netSysPool
#define _pNetDpool      VS_CORE_DATA->_pNetDpool
#define _pNetSysPool    VS_CORE_DATA->_pNetSysPool
#define max_linkhdr     VS_CORE_DATA->max_linkhdr
#define max_protohdr    VS_CORE_DATA->max_protohdr
#define max_hdr         VS_CORE_DATA->max_hdr
#define nmbclusters     VS_CORE_DATA->nmbclusters
#define _dsMallocMap	VS_CORE_DATA->_dsMallocMap

#endif /* __INCvsNetCoreh */

