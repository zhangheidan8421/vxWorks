/* vsUdp.h - virtual stack data for UDP */

/* Copyright 2000 - 2002 Wind River Systems, Inc. */

/*
modification history
--------------------
01f,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01e,04nov03,rlm  Ran batch header path update for header re-org.
01d,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01c,19sep02,hgo  removed #ifdef's from VS_UDP
01b,17sep02,syy  Merge from version 2 of Accordion Munich branch:
                 (modifications for IPv4/6 design: 01b,21jun02,hgo)
01a,11jul02,spm  automatic merge from version 1 of Accordion Munich branch:
                 (copied from Router Stack release: 01b,26oct01,spm)
*/

#ifndef __INCvsUdph
#define __INCvsUdph

/*
 * Remaining globals: shared between stacks (not part of virtual structure).
 *
 *  1) udp_usrreqs
 *
 */

/* includes */
#include <netinet/in_pcb.h>
#include <netinet/udp_var.h>
#include <sys/socket.h>

/* typedefs */

    /* Encapsulated (former) globals for UDP. */

typedef struct vsUdp
    {
    /* defined in udp_usrreq.c */

    int	                 _udpcksum;		/* UDP protocol implementation.
                                                 * Per RFC 768, August, 1980 */
    int	                 _udp_log_in_vain;
    int                  _priv_udp_blackhole;
    struct inpcbhead     _udb;
    struct inpcbinfo     _udbinfo;
    struct udpstat       _udpStat;
    struct sockaddr_in   _priv_udp_in;
#ifdef INET6
    struct udp_in6       _priv_udp_in6;
    struct udp_ip6       _priv_udp_ip6;
#endif
    u_long               _udp_sendspace;
    u_long               _udp_recvspace;
    } VS_UDP;

/* macros for "non-private" global variables */

#define VS_UDP_DATA ((VS_UDP *)vsTbl[myStackNum]->pUdpGlobals)

#define udpcksum            VS_UDP_DATA->_udpcksum
#define udp_log_in_vain     VS_UDP_DATA->_udp_log_in_vain
#define udb                 VS_UDP_DATA->_udb
#define udbinfo             VS_UDP_DATA->_udbinfo
#define udpStat             VS_UDP_DATA->_udpStat
#define udp_sendspace       VS_UDP_DATA->_udp_sendspace
#define udp_recvspace       VS_UDP_DATA->_udp_recvspace

#endif /* __INCvsUdph */
