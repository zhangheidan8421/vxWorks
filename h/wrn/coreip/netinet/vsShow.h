/* vsShow.h - virtual stack data for show routines */

/* Copyright 2000 - 2002 Wind River Systems, Inc. */

/*
modification history
--------------------
01g,11aug04,niq  VS data structure reorganization
01f,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01e,04nov03,rlm  Ran batch header path update for header re-org.
01d,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01c,19sep02,spm  automatic merge from version 3 of Accordion Munich branch:
                 (bookkeeping only: no changes).
01b,17sep02,syy  Merge from version 2 of Accordion Munich branch:
                 (changes for stack component registration, 01b,04jul02,hgo)
01a,11jul02,spm  automatic merge from version 1 of Accordion Munich branch:
                 (copied from Router Stack release: 01b,26oct01,spm)
*/

#ifndef __INCvsshowh
#define __INCvsshowh


/* includes */

#include <netinet/in_pcb.h>

/* Encapsulated (former) globals for UDP show. */

typedef struct vsUdpShow
    {
    struct inpcbhead       *_pUdpPcbHead;
    } VS_UDPSHOW;


/* Encapsulated (former) globals for TCP show. */

typedef struct vsTcpShow
    {
    struct inpcbhead       *_pTcpPcbHead;
    } VS_TCPSHOW;

/* defines */

/* macros for "non-private" global variables */

#define VS_UDPSHOW_DATA (&(vsTbl[myStackNum]->udpShowGlobals))

#define _pUdpPcbHead        VS_UDPSHOW_DATA->_pUdpPcbHead


#define VS_TCPSHOW_DATA (&(vsTbl[myStackNum]->tcpShowGlobals))

#define _pTcpPcbHead        VS_TCPSHOW_DATA->_pTcpPcbHead

#endif /* __INCvsshowh */
