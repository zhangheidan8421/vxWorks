/* ip_var.h - internet protocol protocol variable header file */

/*
 * Copyright (c) 2001-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. No license to Wind River intellectual property rights
 * is granted herein. All rights not licensed by Wind River are reserved
 * by Wind River.
 */

/*
 * Copyright (c) 2002 INRIA. All rights reserved.
 *
 * Implementation of Internet Group Management Protocol, Version 3.
 * Developed by Hitoshi Asaeda, INRIA, February 2002.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of INRIA nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Copyright (c) 1982, 1986, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)ip_var.h	8.2 (Berkeley) 1/9/95
 * $FreeBSD: src/sys/netinet/ip_var.h,v 1.50.2.14 2003/09/09 19:09:22 bms Exp $
 */

/*
modification history
--------------------
01z,25aug05,dlk  Add section tags.
01y,20aug05,dlk  Merge IPv4 reassembly improvements from Kame/FreeBSD.
01x,19apr05,rp   merged from comp_wn_ipv6_mld_interim-dev
01w,25feb05,niq  router alert changes
01v,31jan05,niq  merge mroute changes from comp_wn_ipv6_multicast_interim-dev
                 branch  (ver 1)
01u,07feb05,vvv  _KERNEL cleanup
01t,28oct04,dlk  Change ip_mrouter tentative definition to an extern
                 declaration (SPR #103234).
01s,13sep04,rae  ip_mrouter is a UINT now
01r,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01q,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
01p,06jan04,vvv  modified ipq structure for reassembly changes (SPR #88742)
01o,20nov03,niq  Remove copyright_wrs.h file inclusion
01n,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01m,04nov03,rlm  Ran batch header path update for header re-org.
01l,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01k,07aug03,niq  Merging from Accordion label ACCORDION_BASE6_MERGE_BASELINE
01j,09may03,vvv  included in.h
01i,29apr03,syy  Added #ifdef for __cplusplus
01h,01feb03,pas  merge from FreeBSD 4.7 - added ip_rtaddr(), ip_randomid()
01g,03may02,hsh  add mbuf* as parameter to ip_srcroute to avoid mbuf leaking
01f,08apr02,hsh  change ip_stripoptions so it return number of bytes
                 it saved before stripping module can access it
01e,28mar02,ppp  modified it for backward compatibility with AE 1.1
01d,03oct01,ann  commenting the prototype for ipintr
01c,18sep01,ham  enabled rsvp_input.
01b,27aug01,ann  adding the prototype for ipintr from ip_input.c
01a,23aug01,ppp  Created from FreeBSD 4.3
*/


#ifndef _NETINET_IP_VAR_H_
#define	_NETINET_IP_VAR_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _WRS_KERNEL
#include <net/socketvar.h>
#include <sys/queue.h>
#include <netinet/in.h>

/*
 * Overlay for ip header used by other protocols (tcp, udp).
 */
struct ipovly {
	u_char	ih_x1[9];		/* (unused) */
	u_char	ih_pr;			/* protocol */
	u_short	ih_len;			/* protocol length */
	struct	in_addr ih_src;		/* source internet address */
	struct	in_addr ih_dst;		/* destination internet address */
};

/*
 * Ip reassembly queue structure.  Each fragment
 * being reassembled is attached to one of these structures.
 * They are timed out after ipq_ttl drops to 0, and may also
 * be reclaimed if memory becomes tight.
 */
struct ipq {
	struct	ipq *next,*prev;	/* to other reass headers */
	u_char	ipq_ttl;		/* time for reass q to live */
	u_char	ipq_p;			/* protocol of this fragment */
	u_short	ipq_id;			/* sequence id for reassembly */
	struct mbuf *ipq_frags;		/* to ip headers of fragments */
	struct	in_addr ipq_src,ipq_dst;
	u_short	ipq_nfrags;		/* # frags in this packet */
};

/*
 * Socket argument of multicast source filters.
 */
LIST_HEAD(msf_head, sock_msf_source);

struct sock_msf_source {
	struct	  sockaddr_storage src;	/* source address */
	LIST_ENTRY(sock_msf_source) list; /* list of source addresses */
	u_int	refcount;		/* reference count of the source */
};

struct sock_msf {
/*	u_int32_t msf_mode;		 * source filter mode */
	u_int	  msf_grpjoin;		/* number of (*,G) join requests */
	struct	  msf_head *msf_head;	/* head of joined source list chain */
	struct	  msf_head *msf_blkhead;/* head of muted source list chain */
	u_int16_t msf_numsrc;		/* no. of joined src on this socket */
	u_int16_t msf_blknumsrc;	/* no. of muted src on this socket */
};

/*
 * Structure stored in mbuf in inpcb.ip_options
 * and passed to ip_output when ip options are in use.
 * The actual length of the options (including ipopt_dst)
 * is in m_len.
 */
#define MAX_IPOPTLEN	40

struct ipoption {
	struct	in_addr ipopt_dst;	/* first-hop dst if source routed */
	char	ipopt_list[MAX_IPOPTLEN];	/* options proper */
};

/*
 * Structure attached to inpcb.ip_moptions and
 * passed to ip_output when IP multicast options are in use.
 */
struct ip_moptions {
	struct	ifnet *imo_multicast_ifp; /* ifp for outgoing multicasts */
	struct in_addr imo_multicast_addr; /* ifindex/addr on MULTICAST_IF */
	u_char	imo_multicast_ttl;	/* TTL for outgoing multicasts */
	u_char	imo_multicast_loop;	/* 1 => hear sends if a member */
	u_short	imo_num_memberships;	/* no. memberships this socket */
	struct	in_multi *imo_membership[IP_MAX_MEMBERSHIPS];
	u_long	imo_multicast_vif;	/* vif num outgoing multicasts */
	struct  sock_msf *imo_msf [IP_MAX_MEMBERSHIPS];
};
#endif /* _WRS_KERNEL */

struct	ipstat {
	u_long	ips_total;		/* total packets received */
	u_long	ips_badsum;		/* checksum bad */
	u_long	ips_tooshort;		/* packet too short */
	u_long	ips_toosmall;		/* not enough data */
	u_long	ips_badhlen;		/* ip header length < data size */
	u_long	ips_badlen;		/* ip length < ip header length */
	u_long	ips_fragments;		/* fragments received */
	u_long	ips_fragdropped;	/* frags dropped (dups, out of space) */
	u_long	ips_fragtimeout;	/* fragments timed out */
	u_long	ips_forward;		/* packets forwarded */
	u_long	ips_fastforward;	/* packets fast forwarded */
	u_long	ips_cantforward;	/* packets rcvd for unreachable dest */
	u_long	ips_redirectsent;	/* packets forwarded on same net */
	u_long	ips_noproto;		/* unknown or unsupported protocol */
	u_long	ips_delivered;		/* datagrams delivered to upper level*/
	u_long	ips_localout;		/* total ip packets generated here */
	u_long	ips_odropped;		/* lost packets due to nobufs, etc. */
	u_long	ips_reassembled;	/* total packets reassembled ok */
	u_long	ips_fragmented;		/* datagrams successfully fragmented */
	u_long	ips_ofragments;		/* output fragments created */
	u_long	ips_cantfrag;		/* don't fragment flag was set, etc. */
	u_long	ips_badoptions;		/* error in option processing */
	u_long	ips_noroute;		/* packets discarded due to no route */
	u_long	ips_badvers;		/* ip version != 4 */
	u_long	ips_rawout;		/* total raw ip packets generated */
	u_long	ips_toolong;		/* ip length > max ip packet size */
	u_long	ips_notmember;		/* multicasts for unregistered grps */
	u_long	ips_nogif;		/* no match gif found */
	u_long	ips_badaddr;		/* invalid address on header */
};

#ifdef _WRS_KERNEL

/* flags passed to ip_output as last parameter */
#define	IP_FORWARDING		0x1		/* most of ip header exists */
#define	IP_RAWOUTPUT		0x2		/* raw ip header exists */
#define	IP_SENDONES		0x4		/* send all-ones broadcast */
#define	IP_ROUTETOIF		SO_DONTROUTE	/* bypass routing tables */
#define	IP_ALLOWBROADCAST	SO_BROADCAST	/* can send broadcast packets */

struct ip;
struct inpcb;
struct route;
struct sockopt;

#ifndef VIRTUAL_STACK
extern struct	ipstat	_ipstat;

#ifndef RANDOM_IP_ID
extern u_short	_ip_id;				/* ip packet ctr, for ids */
#endif /* !RANDOM_IP_ID */

extern int	ip_defttl;			/* default IP ttl */
extern int	ipforwarding;			/* ip forwarding */
extern struct route ipforward_rt;		/* ip forwarding cached route */
extern u_char	ip_protox[];

extern int rsvp_on;
extern struct socket *ip_mrouter; /* multicast routing daemon */
#endif /* !VIRTUAL_STACK */

extern int	(*legal_vif_num)(int);
extern u_long	(*ip_mcast_src)(int);

extern struct	pr_usrreqs rip_usrreqs;
extern int	igmpmaxsrcfilter;	/* maximum num .of msf per interface */
extern int	igmpsomaxsrc;		/* maximum num .of msf per socket */

int	 ip_ctloutput(struct socket *, struct sockopt *sopt);
void	 ip_drain(void);
void	 ip_freemoptions(struct ip_moptions *);
_WRS_INITTEXT
void	 ip_init(void);
_WRS_FASTTEXT
int	 ip_output(struct mbuf *,
	    struct mbuf *, struct route *, int, struct ip_moptions *);
struct in_ifaddr *
	 ip_rtaddr(struct in_addr, struct route *);
void	 ip_savecontrol(struct inpcb *, struct mbuf **, struct ip *,
		struct mbuf *);
void	 ip_slowtimo(void);
struct mbuf *
	 ip_srcroute(struct mbuf *);
int	 ip_chksrcroute (void);
int	 ip_stripoptions(struct mbuf *, struct mbuf *);
#ifdef RANDOM_IP_ID
u_int16_t	
	 ip_randomid(void);
#endif
struct ifnet *
	ip_multicast_if (struct in_addr *, int *);
int	ip_check_router_alert (struct ip *);
int	ip_getmopt_msflist (struct sock_msf *, u_int16_t *,
			    struct sockaddr_storage **, u_int *);
int	rip_ctloutput(struct socket *, struct sockopt *);
void	rip_ctlinput(int, struct sockaddr *, void *);
void	rip_init(void);
void	rip_input(struct mbuf *, int, int);
int	rip_output(struct mbuf *, struct socket *, u_long);
/* void	ipip_input(struct mbuf *, int, int); */
void	rsvp_input(struct mbuf *, int, int);
int	ip_rsvp_init(struct socket *);
int	ip_rsvp_done(void);

_WRS_FASTTEXT
void	in_delayed_cksum(struct mbuf *m);

#endif /* _WRS_KERNEL */

#ifdef __cplusplus
}
#endif

#endif /* !_NETINET_IP_VAR_H_ */
