/* vsM2.h - virtual stack data for m2 libraries */

/* Copyright 2000 - 2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01p,25jan05,sar  Modify sysuptime to be based on 64 bit ticks
01o,19sep04,spm  fixed modification history after previous checkin
01n,19sep04,spm  updated virtual stack startup: removed packing/unpacking of
                 configuration parameters
01m,12aug04,kc   VS data structure reorganization for IPv6 MIB2  variables.
                 Also added m2Ipv6Initialized bool to VS_M2_IPV6_IF.
01l,11aug04,niq  VS data structure reorganization
01k,02apr04,spm  fixed virtual stack build: updated for change to avlUintLib
01j,15mar04,rp   merged from orion
01i,04nov03,rlm  Ran batch header path update for header re-org.
01h,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01g,09dec02,ant  added two definitions for VS_M2_IF_DATA: locals changed to
		 globals
01f,30aug02,ant  locals renamed to priv_xxx
01e,27aug02,ism  added m2Ipv6RouteTableSaved global
01d,26aug02,ism  added IPv6 M2 global components
01c,26oct01,spm  isolated virtual stack data structures (SPR #71092)
01b,13jul01,ann  removed the mibStyle flag from vs_m2_if as we now maintain
                 the RFC2233 flag in the END object.
01a,29mar01,spm  file creation: copied from version 01d of tor2_0.open_stack
                 branch (wpwr VOB) for unified code base
*/

#ifndef __INCvsm2h
#define __INCvsm2h


/* includes */

#include <vxWorks.h>
#include <m2Lib.h>
#ifdef INET6
#include <m2Ipv6Lib.h>
#endif

/* defines */

/* typedefs */

typedef struct vs_m2_sys
    {
    /*
     * The system group is supported by the structure defined below.  
     * All changes to the system group are reflected in this structure.
     */

    M2_SYSTEM priv_m2SystemVars;

    /* This semaphore protects the m2SystemVars from multiple readers 
     * and writers */

    SEM_ID priv_m2SystemSem;

    /* 64 bit ticks at start */
    UINT64 priv_startTicks;
    } VS_M2_SYS;

typedef struct vs_m2_if
    {
    AVLU_TREE      pM2IfRoot;
    AVLU_TREE *    pM2IfRootPtr;

    /* Network I/F table (Allocated Dynamically) */
    M2_IFINDEX *  pm2IfTable;

    /* Number of network interfaces */
    int           m2IfCount;
    int           ifsInList;

    /* Pointer to trap routine supplied by the user */
    FUNCPTR       pM2TrapRtn;

    /* Pointer to trap routine argument supplied by */
    void *        pM2TrapRtnArg; 

    /* last time a row was added/removed from the ifTable/ifXTable */
    ULONG         m2IfTableLastChange;

    /* 
     * last time a row was added/removed from the ifStackTable or when the
     * ifStackStatus was last changed 
     */
    ULONG         m2IfStackLastChange;

    /* Semaphore used to protect the Interface group */
    SEM_ID        m2InterfaceSem;

    /*
     * Global variable used to keep the group startup time in 64 bit ticks.
     * The functionality here is the same as the functionality 
     * provided in the module m2SysLib.c.  It is duplicated to allow for 
     * both modules to exist independently.
     */
    UINT64 priv_startTicks; /* 64 bit ticks at start */

    /* Allow 32-bit counters to roll-over or stop at maximum? */

    BOOL m2IfCounterOverflowFlag;

    /* Use larger RFC 2233 counters if available? */

    BOOL m2If64BitCounters;
    } VS_M2_IF;

#ifdef INET6
typedef struct vs_m2_ipv6_if
    {
    BOOL              m2Ipv6Initialized;
    int               m2Ipv6Interfaces;
    ULONG             m2Ipv6IfTableLastChange;
    int               priv_m2Ipv6RouteTableSaved;
    unsigned long     priv_m2Ipv6RouteTableSize;
    unsigned long     priv_m2Ipv6NumRouteEntries;
    unsigned long     priv_m2Ipv6DiscardedRoutes;
    M2_IPV6ROUTETBL * priv_m2Ipv6RouteTable;
    SEM_ID            priv_m2Ipv6RouteSem;
    } VS_M2_IPV6_IF;
#endif

/* macros */

#define VS_M2_SYS_DATA ((VS_M2_SYS *)vsTbl[myStackNum]->pM2SysGlobals)
#define VS_M2_IF_DATA (&(vsTbl[myStackNum]->m2IfGlobals))

#ifdef INET6
#define VS_M2_IPV6_IF_DATA (&(vsTbl[myStackNum]->m2Ipv6IfGlobals))
#endif

/* Defines for the m2 layer globals */

/* m2SysLib.c */

/* m2IfLib.c */

#define m2IfCount         	VS_M2_IF_DATA->m2IfCount
#define ifsInList         	VS_M2_IF_DATA->ifsInList
#define pM2IfRoot         	VS_M2_IF_DATA->pM2IfRoot
#define pM2IfRootPtr      	VS_M2_IF_DATA->pM2IfRootPtr
#define pm2IfTable        	VS_M2_IF_DATA->pm2IfTable
#define pM2TrapRtn        	VS_M2_IF_DATA->pM2TrapRtn
#define pM2TrapRtnArg     	VS_M2_IF_DATA->pM2TrapRtnArg
#define m2IfTableLastChange 	VS_M2_IF_DATA->m2IfTableLastChange
#define m2IfStackLastChange 	VS_M2_IF_DATA->m2IfStackLastChange
#define m2InterfaceSem    	VS_M2_IF_DATA->m2InterfaceSem

#ifdef INET6
/* m2Ipv6IfLib.c */

#define m2Ipv6Interfaces        VS_M2_IPV6_IF_DATA->m2Ipv6Interfaces
#define m2Ipv6IfTableLastChange VS_M2_IPV6_IF_DATA->m2Ipv6IfTableLastChange
#define m2Ipv6Initialized       VS_M2_IPV6_IF_DATA->m2Ipv6Initialized 
#endif

#endif /* __INCvsm2h */
