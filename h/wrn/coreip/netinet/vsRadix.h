/* vsRadix.h - virtual stack data for radix/routing storage */

/* Copyright 2000-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01i,07apr05,spm  fixed memory leak from netmask storage (SPR #97721)
01h,17sep04,niq  Code cleanup
01g,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01f,04nov03,rlm  Ran batch header path update for header re-org.
01e,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01d,17oct02,spm  replaced private globals to permit multiple tree instances
01c,23sep02,niq  Changes for Accordion's RTM
01b,21aug02,ant  taken from synth
01a,21apr00,spm  written
*/

#ifndef __INCvsRadixh
#define __INCvsRadixh

#include <netLib.h>
#include <net/protosw.h>

/*
 * Unresolved globals: (should these be part of structure?)
 * 
 *  1) netLib.c: _rtMissMsgHook (constant function pointer)
 *  2) radix.c:  struct radix_mask *rn_mkfreelist; (unused?)
 *  3) radix.c: static char normal_chars[];  (constant read-only array)
 *  4) route.c: struct sockaddr wildcard;   (unused?)
 */
 
/* typedefs */

    /*
     * Encapsulated (former) globals for routing storage. Structure elements
     * marked XXX might be left global if any sharing between virtual stacks
     * is permitted. (Some are read-only globals, others are constant timers).
     */

typedef struct vs_radix_global_data
    {
    /* definitions from radix.c */
    int			priv_numRibsInstantiated;
#ifdef RN_DEBUG
    int				rn_nodenum;
    struct radix_node *		rn_clist;
    int				rn_saveinfo;
    int				rn_debug;
#endif
    } VS_RADIX;

#define VS_RADIX_DATA ((VS_RADIX *)vsTbl[myStackNum]->pRadixGlobals)

/* definitions from radix.c */

#ifdef RN_DEBUG
#define rn_nodenum	VS_RADIX_DATA->rn_nodenum
#define rn_clist		VS_RADIX_DATA->rn_clist
#define rn_saveinfo	VS_RADIX_DATA->rn_saveinfo
#define rn_debug		VS_RADIX_DATA->rn_debug
#endif

#endif /* __INCvsRadixh */
