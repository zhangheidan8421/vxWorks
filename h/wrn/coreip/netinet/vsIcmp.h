/* vsIcmp.h - virtual stack data for ICMP */

/* Copyright 2000 - 2003 Wind River Systems, Inc. */
 
/*
modification history
--------------------
01f,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01e,04nov03,rlm  Ran batch header path update for header re-org.
01d,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01c,07mar03,spm  fixed VS support after merge from CLARINET-1_1-KAME-UPDATE
01b,14jan03,hgo  removed icmpUtilLib part
01a,18jun02,ant  taken from from synth
*/

#ifndef __INCvsIcmph
#define __INCvsIcmph


/* includes */

#include <sys/socket.h> 
#include <netinet/ip_icmp.h>
#include <netinet/icmp_var.h>

/* defines */

/* typedefs */

typedef struct vs_icmp_global_data
    {
    /* definitions from ip_icmp.c */
    struct icmpstat _icmpstat;
    int priv_icmpmaskrepl;
    int	priv_drop_redirect;
    int	priv_log_redirect;
    int priv_ICMPMAXPAYLOAD_LEN;
    int priv_icmplim;
    int priv_icmplim_output;
    int	priv_icmpbmcastecho;
#ifdef ICMPPRINTFS
    int icmpprintfs;
#endif
    struct icmp_info priv_icmpsrc;
    struct sockaddr_in priv_icmpdst;
    struct sockaddr_in priv_icmpgw;

     /* definition from badport_bandlim() */
    int priv_lticks[BANDLIM_MAX + 1];
    int priv_lpackets[BANDLIM_MAX + 1];

    } VS_ICMP;


/* macros */

#define VS_ICMP_DATA ((VS_ICMP *)vsTbl[myStackNum]->pIcmpGlobals)

#define _icmpstat         VS_ICMP_DATA->_icmpstat
#ifdef ICMPPRINTFS
#define icmpprintfs       VS_ICMP_DATA->icmpprintfs
#endif

#endif /* __INCvsIcmph */

