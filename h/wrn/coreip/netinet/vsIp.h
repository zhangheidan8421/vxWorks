/* vsIp.h - virtual stack data for IP */

/*
 * Copyright (c) 2000-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. No license to Wind River intellectual property rights
 * is granted herein. All rights not licensed by Wind River are reserved
 * by Wind River.
 */

/*
modification history
--------------------
01w,15aug05,dlk  Port reassembly improvements from FreeBSD/Kame.
                 Removed priv_ip_nfragpackets and priv_ip_maxfragpackets,
		 added priv_maxfragsperpacket;
01v,25jun05,dlk  Added ipintrq, ipintrqJob for loopback.
01u,27apr05,kch  Move ip_mrouter definition here from VS_IP_MROUTE.
01t,02mar05,niq  Make router alert support scalable
01s,25feb05,niq  Add support for router alert
01r,31jan05,niq  merge mroute changes from comp_wn_ipv6_multicast_interim-dev
                 branch  (ver 1)
01q,20jan05,sar  Removal of divert, dummynet and fw code.
01p,13sep04,vvv  renamed icmpErrorLen to fix sysctl/virtual stack
                 issue (SPR #91565)
01o,23jun04,sar  Added rate filter for completeness
01n,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
01m,11may04,sar  Added fields for IPFW routines
01l,21feb04,dlk  Rename _icmperr_mcopy to icmperr_mcopy.
01k,11feb04,dlk  Added _icmperr_mcopy.
01j,11nov03,cdw  Removal of unnecessary _KERNEL guards.
01i,10nov03,rlm  2nd pass of include update for header re-org.
01h,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01g,04nov03,rlm  Ran batch header path update for header re-org.
01f,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01e,07mar03,spm  fixed VS support after merge from CLARINET-1_1-KAME-UPDATE
01d,11dec02,sar  Virtualize the external hooks
01c,02sep02,vlk  added proxyBroadcastHook variable
01b,26aug02,kal  moved some to VS_NET_CORE
01a,20jun02,ant  taken from from synth
*/

#ifndef __INCvsIph
#define __INCvsIph


/* includes */
#include <net/socketvar.h>
#include <sys/socket.h>
#include <netinet/in_systm.h>
#include <netinet/in.h>
#include <netinet/in_var.h>
#include <netinet/ip.h>
#include <netinet/ip_var.h>
#include <netinet/ip_flow.h>
#include <netinet/ipprotosw.h>
#include <netinet/in_pcb.h>
#include <netLib.h>
#include <net/domain.h>
#include <netinet/ip4_ext_in.h>
#include <netinet/ip4_ext_out.h>
#include <time.h>
#include <netinet/ipfw.h>

LIST_HEAD(in_multihead, in_multi);
TAILQ_HEAD(in_ifaddrhead, in_ifaddr);

/* defines */

/* typedefs */

typedef struct vs_ip_global_data
    {
    /* definitions from in.c */
    int priv_subnetsarelocal;
    struct in_multihead _in_multihead;
    int priv_in_interfaces;

    /* definitions from in_pcb.c */
    int	ipport_lowfirstauto;
    int	ipport_lowlastauto;
    int	ipport_firstauto;
    int	ipport_lastauto;
    int	ipport_hifirstauto;
    int	ipport_hilastauto;

    /* definitions from in_proto.c */
    struct domain inetdomain;

    /* definitions from in_rmx.c */
    int priv_rtq_reallyold;
    int priv_rtq_minreallyold;
    int priv_rtq_toomany;
    int priv_rtq_timeout;
    time_t priv_last_adjusted_timeout; /* in_rtqtimo() */
	void *priv_rtqTimerHandle;

    /* definitions from in_flow.c */
    LIST_HEAD(ipflowhead, ipflow) priv_ipflows[IPFLOW_HASHSIZE];
    int priv_ipflow_inuse;
    int priv_ipflow_active;

    /* definitions from ip_input.c */
    int rsvp_on;
    int priv_ip_rsvp_on;
    /*
     * Removed ip_rsvpd  as we support RSVP through the IP_ROUTER_ALERT option
     */
    FUNCPTR proxyBroadcastHook;
    int	ipforwarding;
    int	priv_ipsendredirects;
    int	ip_defttl;
    int	priv_ip_dosourceroute;
    int	priv_ip_acceptsourceroute;
    struct mbuf * icmperr_mcopy;
#if 0
    int	ip_keepfaith;
#endif
    int icmpErrorLength;
    int	priv_ip_checkinterface;
#ifdef DIAGNOSTIC
    int	priv_ipprintfs;
#endif
    u_char	ip_protox[IPPROTO_MAX];
    struct  in_ifaddrhashhead *in_ifaddrhashtbl;
    u_long  in_ifaddrhmask;

    int	priv_ipqmaxlen;
    struct	in_ifaddrhead _in_ifaddrhead;
    struct ipstat _ipstat;
    struct ipq priv_ipq[IPREASS_NHASH];
    int    priv_nipq;
    int    priv_maxnipq;
    int    priv_maxfragsperpacket;
#ifdef IPSTEALTH
    int	priv_ipstealth;
#endif

    /* from intrq.c */
    struct ifqueue ipintrq;
    INTRQ_QJOB ipintrqJob;

    int	priv_ip_nhops;
    struct ip_srcrt {
	    struct	in_addr dst;
	    char	nop;
	    char	srcopt[IPOPT_OFFSET + 1];
	    struct	in_addr route[MAX_IPOPTLEN/sizeof(struct in_addr)];
    } priv_ip_srcrt;
    struct	sockaddr_in priv_ipaddr;
    struct	route ipforward_rt;

    /* external hooks from ip_input.c */
    INPUT_HOOK_IPV4_FUNCPTR   ipFilterHook;
    IPSEC_FILTER_HOOK_FUNCPTR ipsecFilterHook;
    IPSEC_INPUT_FUNCPTR       ipsecInput;
    /*    PROXY_BROADCAST_FUNCPTR    proxyBroadcastHook;*/
    MCAST_ROUTE_FWD_FUNCPTR   mCastRouteFwdHook;
    FUNCPTR       	      _mCastRouteCmdHook;

    /* external hook from sizing.c */
    SIZING_ROUTINE_FUNCPTR    ipsecSizing;

    /* definitions from ip_output.c */
    u_short _ip_id;
    struct route priv_sro_fwd; /* ip_output() */

    /* external hooks from ip_output.c */
    IPSEC_OUTPUT_FUNCPTR      ipsecOutput;

    /* definitions from raw_ip.c */
    struct	inpcbhead ripcb;
    struct	inpcbinfo ripcbinfo;
    struct	sockaddr_in priv_ripsrc;
    u_long	rip_sendspace;
    u_long	rip_recvspace;

#ifdef ROUTER_ALERT
    struct ralcb_list_head _ralcb_list;
#endif /* ROUTER_ALERT */

#ifdef INCLUDE_IPFW_HOOKS
    /* external hooks from ipfw_var.c, these are for ip_input.c and
       ip_output.c */
    int ipfw_serial;

    ipfw_anchor_t ipfw_preinput;
    ipfw_anchor_t ipfw_input;
    ipfw_anchor_t ipfw_forward;
    ipfw_anchor_t ipfw_output;
    ipfw_anchor_t ipfw_call;
    ipfw_anchor_t ipfw_rate;

    /* push points for the ipfw code, from ipfw_var.c */
    ipfw_type_t ipfw_types[IPFW_MAX_PUSH_POINTS];
#endif /* INCLUDE_IPFW_HOOKS */

    /* multicast routing daemon */
    struct socket *ip_mrouter;

    } VS_IP;

/* macros */

#define VS_IP_DATA ((VS_IP *)vsTbl[myStackNum]->pIpGlobals)

/* definitions from in.c */
#define _in_multihead         VS_IP_DATA->_in_multihead

/* definitions from in_pcb.c */
#define ipport_lowfirstauto   VS_IP_DATA->ipport_lowfirstauto
#define ipport_lowlastauto    VS_IP_DATA->ipport_lowlastauto
#define ipport_firstauto      VS_IP_DATA->ipport_firstauto
#define ipport_lastauto       VS_IP_DATA->ipport_lastauto
#define ipport_hifirstauto    VS_IP_DATA->ipport_hifirstauto
#define ipport_hilastauto     VS_IP_DATA->ipport_hilastauto

/* definitions from in_proto.c */
#define inetdomain            VS_IP_DATA->inetdomain

/* definitions from in_rmx.c */

/* definitions from in_flow.c */

/* definitions from ip_input.c */
#define ipforward_rt          VS_IP_DATA->ipforward_rt
#define rsvp_on               VS_IP_DATA->rsvp_on
#define ipforwarding          VS_IP_DATA->ipforwarding
#define proxyBroadcastHook    VS_IP_DATA->proxyBroadcastHook
#define ip_defttl             VS_IP_DATA->ip_defttl
#define icmpErrorLength       VS_IP_DATA->icmpErrorLength
#define ip_protox             VS_IP_DATA->ip_protox
#define in_ifaddrhashtbl      VS_IP_DATA->in_ifaddrhashtbl
#define in_ifaddrhmask        VS_IP_DATA->in_ifaddrhmask
#define icmperr_mcopy         VS_IP_DATA->icmperr_mcopy

#define _in_ifaddrhead        VS_IP_DATA->_in_ifaddrhead
#define _ipstat               VS_IP_DATA->_ipstat
#define ip_mrouter            VS_IP_DATA->ip_mrouter

/* external hooks from ip_input.c */
#define _ipFilterHook         VS_IP_DATA->ipFilterHook
#define _ipsecFilterHook      VS_IP_DATA->ipsecFilterHook
#define _ipsecInput           VS_IP_DATA->ipsecInput
#define proxyBroadcastHook    VS_IP_DATA->proxyBroadcastHook
#define mCastRouteFwdHook     VS_IP_DATA->mCastRouteFwdHook
#define _mCastRouteCmdHook    VS_IP_DATA->_mCastRouteCmdHook

#define SET_IPFILTERHOOK(foo)      (VS_IP_DATA->ipFilterHook = foo)
#define SET_IPSECFILTERHOOK(foo)   (VS_IP_DATA->ipsecFilterHook = foo)
#define SET_IPSECINPUT(foo)        (VS_IP_DATA->ipsecInput = foo)
#define SET_PROXYBROADCASTHOOK(foo)(VS_IP_DATA->proxyBroadcastHook = foo)
#define SET_MCASTROUTEFWDHOOK(foo) (mCastRouteFwdHook = foo)

/* and for backwards compatibility in ip_input.c */
#define _func_ipsecFilterHook  VS_IP_DATA->ipsecFilterHook
#define _func_ipsecInput       VS_IP_DATA->ipsecInput

/* from intrq.c (loopback) */
#define ipintrq		       VS_IP_DATA->ipintrq
#define ipintrqJob	       VS_IP_DATA->ipintrqJob

/* external hook from sizing.c */
#define _ipsecSizing          VS_IP_DATA->ipsecSizing
#define SET_IPSECSIZING(foo) (VS_IP_DATA->ipsecSizing = foo)

/* definitions from ip_output.c */
#define _ip_id                VS_IP_DATA->_ip_id

/* external hooks from ip_output.c */
#define _ipsecOutput          VS_IP_DATA->ipsecOutput
#define SET_IPSECOUTPUT(foo) (VS_IP_DATA->ipsecOutput = foo)

/* definitions from raw_ip.c */
#define ripcb                 VS_IP_DATA->ripcb
#define ripcbinfo             VS_IP_DATA->ripcbinfo
#define rip_sendspace         VS_IP_DATA->rip_sendspace
#define rip_recvspace         VS_IP_DATA->rip_recvspace
#ifdef ROUTER_ALERT
#define ralcb_list            VS_IP_DATA->_ralcb_list
#endif /* ROUTER_ALERT */

/* definitions from ipfw_var.c */
#define ipfw_serial   VS_IP_DATA->ipfw_serial
#define ipfw_preinput VS_IP_DATA->ipfw_preinput
#define ipfw_input    VS_IP_DATA->ipfw_input
#define ipfw_forward  VS_IP_DATA->ipfw_forward
#define ipfw_output   VS_IP_DATA->ipfw_output
#define ipfw_call     VS_IP_DATA->ipfw_call
#define ipfw_rate     VS_IP_DATA->ipfw_rate
#define ipfw_types    VS_IP_DATA->ipfw_types

#endif /* __INCvsIph */
