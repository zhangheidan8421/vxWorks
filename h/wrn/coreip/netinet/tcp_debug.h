/* tcp_debug.c - TCP debugging definitions */

/* Copyright 2001-2005 Wind River Systems, Inc. */

/*
 * Copyright (c) 1982, 1986, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)tcp_debug.h	8.1 (Berkeley) 6/10/93
 * $FreeBSD: src/sys/netinet/tcp_debug.h,v 1.11 2000/01/29 11:49:05 shin Exp $
 */

/*
modification history
--------------------
01m,08feb05,vvv  _KERNEL cleanup
01l,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01k,08jun04,niq  Include header file for correct compilation in project
                 facility
01j,20nov03,niq  Remove copyright_wrs.h file inclusion
01i,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01h,04nov03,rlm  Ran batch header path update for header re-org.
01g,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01f,08aug03,niq  Merging from Accordion label ACCORDION_BASE6_MERGE_BASELINE
01e,10jun03,vvv  include netVersion.h
01d,16sep02,pas  merged changes from Veloce stack, added tcpDebugPrint()
01c,10sep02,hsh  add c++ protection
01b,08feb02,pas  EAR code cleanup
01a,13aug01,pas  Created from FreeBSD4.3-RELEASE(July-17-01).
*/  

#ifndef _NETINET_TCP_DEBUG_H_
#define	_NETINET_TCP_DEBUG_H_

#include <netinet/tcpip.h>
#include <netinet/in_systm.h>
#include <netinet/tcp_var.h>
#ifdef INET6
#include <netinet/in.h>
#include <netinet/in_var.h>
#include <netinet6/ip6.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif
     
#include <netVersion.h>

struct	tcp_debug {
	n_time	td_time;
	short	td_act;
	short	td_ostate;
	caddr_t	td_tcb;
	caddr_t	td_iphdr;
	caddr_t	td_tcphdr;
	int	td_family;
	/*
	 * Co-existense of td_ti and td_ti6 below is ugly, but it is necessary
	 * to achieve backword compatibility to some extent.
	 */
	struct	tcpiphdr td_ti;
	struct {
		u_char	ip6buf[40]; /* sizeof(struct ip6_hdr) */
		struct	tcphdr th;
	} td_ti6;
#define	td_ip6buf	td_ti6.ip6buf
	short	td_req;
	struct	tcpcb td_cb;
};

#define	TA_INPUT 	0
#define	TA_OUTPUT	1
#define	TA_USER		2
#define	TA_RESPOND	3
#define	TA_DROP		4

#ifdef TANAMES
static char	*tanames[] =
    { "input", "output", "user", "respond", "drop" };
#endif

#define TCP_NDEBUG		100	/* length of debug info array */
#define TCP_DEBUG_NUM_DEFAULT	20	/* default display number */

IMPORT VOIDFUNCPTR tcpTraceRtn;
IMPORT VOIDFUNCPTR tcpReportRtn;

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern void	tcpTraceInit (void);
extern STATUS	tcp_debugInit (void);

#else	/* __STDC__ */

extern void	tcpTraceInit ();
extern STATUS	tcp_debugInit ();

#endif	/* __STDC__ */
 
#ifdef __cplusplus
}
#endif
    
#endif /* !_NETINET_TCP_DEBUG_H_ */
