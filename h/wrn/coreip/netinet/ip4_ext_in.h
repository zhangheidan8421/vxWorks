/* ip4_ext_in.h - typedefs and externs for the external hooks
 * used by the IPv4 stack on the input side
 */

/*
 * Copyright (c) 2002-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. No license to Wind River intellectual property rights
 * is granted herein. All rights not licensed by Wind River are reserved
 * by Wind River.
 */

/*
modification history
--------------------
01h,07jul05,kch  Added _igmpGetRouterAlertHook function pointer.
01g,25feb05,niq  merge mroute changes from comp_wn_ipv6_multicast_interim-dev
                 branch  (ver 1)
01f,20sep04,rae  fix prototype for MCAST_ROUTE_FWD_FUNCPTR
01e,13may04,sar  Remove natpt hook and handle vs better
01d,20nov03,niq  Remove copyright_wrs.h file inclusion
01c,07aug03,nee  Merging to Base6 from ACCORDION_BASE6_MERGE_BASELINE label on
                 Accordion codeline
01b,26feb03,vvv  added multicast hook
01a,11sep02,sar  written
*/

#ifndef __INCip4ExtInh
#define __INCip4ExtInh

#ifdef __cplusplus
extern "C" {
#endif

/*
 * This file contains typedefs for some of the hooks used by the IPv4 stack.
 * The similar hooks used by the IPv6 stack can be found in a different file.
 * The functions are separated in order to minimize the include files required
 * to use all one set of functions.
 */

/*
 * This is the general typedef for the input side for IPv4.
 * It should be used by all hooks that don't require a more specific API,
 * however due to pragmatic concerns about changing pre-existing APIs
 * several older hooks use different APIs.
 */

typedef BOOL (*INPUT_HOOK_IPV4_FUNCPTR)   (struct ifnet  * rcvif,
                                           struct mbuf  ** m,
                                           struct ip    ** ip,
                                           int             hlen);
/*
 * Typedef used for proxy arp
 */
typedef void (*PROXY_BROADCAST_FUNCPTR)       (struct mbuf   * m,
                                               struct ifnet  * rcvif);

/*
 * The typedefs used by the WindRiver IP Security functions.
 */
typedef int (*IPSEC_FILTER_HOOK_FUNCPTR) (struct mbuf ** m,
                                          struct ip   ** ip,
                                          int            hlen);

typedef int (*IPSEC_INPUT_FUNCPTR)       (struct mbuf ** m,
                                          int            hlen,
                                          struct ip   ** ip);

/*
 * And the typedef used for multicast forwarding
 */
typedef int (*MCAST_ROUTE_FWD_FUNCPTR)   (struct ip          * ip,
                                          struct ifnet       * rcvif,
					  struct mbuf        * m,
                                          struct ip_moptions * imo);

/*
 * Externals and macros for the currently defined hooks
 * If we are compiling for virtual stacks we specify them
 * in the vs .h file instead of here.
 */

#ifndef VIRTUAL_STACK

/* general use */
extern INPUT_HOOK_IPV4_FUNCPTR   _ipFilterHook;
#define SET_IPFILTERHOOK(foo) (_ipFilterHook = foo)

/* IP security */
extern IPSEC_FILTER_HOOK_FUNCPTR _ipsecFilterHook;
extern IPSEC_INPUT_FUNCPTR       _ipsecInput;

#define SET_IPSECFILTERHOOK(foo) (_ipsecFilterHook = foo)
#define SET_IPSECINPUT(foo)      (_ipsecInput = foo)

/* for backwards compatibility */
#define _func_ipsecFilterHook _ipsecFilterHook
#define _func_ipsecInput      _ipsecInput

/* Proxy broadcast */
extern PROXY_BROADCAST_FUNCPTR    proxyBroadcastHook;
#define SET_PROXYBROADCASTHOOK(foo) (proxyBroadcastHook = foo)

/* Multicast separation */
extern MCAST_ROUTE_FWD_FUNCPTR    mCastRouteFwdHook;
extern FUNCPTR                    _mCastRouteCmdHook;
#define SET_MCASTROUTEFWDHOOK(foo) (mCastRouteFwdHook = foo)

/* IGMP hooks */
extern FUNCPTR  _igmpGetRouterAlertHook;

#endif /* #ifndef VIRTUAL_STACK */

#ifdef __cplusplus
}
#endif

#endif /* __INCip4ExtInh */
