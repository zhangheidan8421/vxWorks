/* vsLib.h - virtual stack routines */

/* Copyright 2000 - 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01y,13aug04,vvv  added vsStart()
01x,06aug04,spm  cleanup virtual stack routine names
01w,27jul04,kc   removed warnings from application startup routines with no
                 parameters: disabled type checking.
01v,09jun04,spm  removed VIRTUAL_STACK to enable contents whenever included
01u,04apr03,wie  revision after code review - rearranged VIRTUAL_STACK 
01t,17feb03,wie  added vsMgrDestructor, vsMgrInfo prototypes and new error codes
01s,27jan03,wie  better support for init-failures added, workaround for not
                 working unload function of end-drivers added
01r,10jan03,wie  vsNonCoreComponentRegister prototype added
01q,18nov02,kal  vsLibInit() for -DVIRTUAL_STACK only
01p,15nov02,wie  prototype for vsSo... removed, added new parameters for 
                 vsDownToSocketsSend 
01o,21oct02,wie  new error codes added
01n,10oct02,wie  added new prototype for vsSocketList()
01m,13sep02,wie  renaming vsIdCheck
01l,12sep02,wie  added prototype for vsStatusSet()
01k,10sep02,wie  new stack component type added (VS_COMP) 
01j,14aug02,wie  preliminary version including partial support for automatic 
                 vs creation and stack deletetion on demand
01i,04jul02,wie  modifications for ACCORDION (see Virtual IP Addr Space Design
                 document 1.0 for all the details)
01h,29jul02,wie  changes for Accordion
01c,11jul02,spm  fixed modification history after automatic merge
01b,11jul02,wrs  automatic merge from version 3 of Accordion Munich branch:
                 (xxjul02,wie  initial development and design review changes)
01a,24jun02,spm  automatic merge from version 1 of Accordion Munich branch:
                 (initial version of file copied from ver. 7 of tor3_x.synth
                  branch in /wind/river VOB: modhist 01g,13jan02,spm)
*/


#ifndef __INCvsLibh
#define __INCvsLibh

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
	      VS_OK                 = OK,
          VS_NON_SPECIFIC_ERR   = ERROR,
		  
          VS_SPECIFIC_INFO_START = 100,
		  VS_INFO_NO_MORE_DEVICES,
		  
          VS_SPECIFIC_ERR_START = 1000,
	      VS_ERR_SEM_CREATE_FAILED,
	      VS_ERR_VSNUM_EXCEEDED,
	      VS_ERR_DUPLICATED_STACK_NAME,
	      VS_ERR_MEM_ALLOC_FOR_GLOBAL_DATA_FAILED,
	      VS_ERR_MEM_ALLOC_FOR_CORE_GLOBALS_FAILED,
		  VS_ERR_SC_LIST_MEM_ALLOC_FAILED,
	      VS_ERR_VSLIB_NOT_INITIALIZED,
	      VS_ERR_INVALID_VSID,
	      VS_ERR_NO_MATCH,
	      VS_ERR_INVALID_VSNUM,
	      VS_ERR_VS_DOES_NOT_EXIST,
	      VS_ERR_INVALID_FCT_PARAM,
	      VS_ERR_ACCESSING_TASK_VAR,
	      VS_ERR_CALL_WITH_NO_FCT_PTR,
		  VS_ERR_INVALID_SC_TYPE,
		  VS_ERR_MEM_ALLOC_FOR_SC_FAILED,
		  VS_ERR_SC_NOT_FOUND,	      
	      VS_ERR_TASK_VAR_ALRDY_EXIST,
		  VS_ERR_REGISTRATION_NOT_POSSIBLE,
	      VS_ERR_INIT_ALREADY_DONE,
		  VS_ERR_NON_OPT_SC_CANT_BE_DISABLED,
		  VS_ERR_INVALID_CFG_PARAMS_LEN,
		  VS_ERR_NO_CFG_PARAMS_AVAILABLE,
		  VS_ERR_INIT_FAILED,
		  VS_ERR_ADD_NEW_SOCK_ENTRY_FAILED,
		  VS_ERR_MGR_INIT_FAILED,
		  VS_ERR_STACK_IS_DOWN,
		  VS_ERR_SOCKET_IN_INVALID_CONTEXT,
		  VS_ERR_CAN_NOT_CREATE_MSG_QUEUE,
		  VS_ERR_CAN_NOT_CREATE_MGR_TASK,
		  VS_ERR_CAN_NOT_CREATE_SUBMGR_TASK,
		  VS_ERR_CAN_NOT_DELETE_SUBMGR_TASK,
		  VS_ERR_COMM_WITH_MGR_TASK_FAILED,
		  VS_ERR_STACK_IS_ALREADY_GOING_DOWN,
		  VS_ERR_STACK_IS_ALREADY_DOWN,
		  VS_ERR_DESTRUCTOR_FAILED,
		  VS_ERR_SOCKET_OPEN,
		  VS_ERR_MGR_TASK_TIMEOUT,
		  VS_ERR_CAN_NOT_DELETE_MGR_TASK,
		  VS_ERR_MGR_NOT_INACTIVE,
		  
	      VS_ERR_MAX
          } VS_STATUS;

/* States of a virtual stack instance */ 
typedef enum {
          VS_NOT_AVAILABLE,      
	      /* the stack number refers to an empty entry in the vsTbl - */
          /*   i.e. the stack was never created or has been deleted */
	      VS_STARTS_UP = 100,         
	      /* the virtual stack is currently in the initialization phase, */                     
          /*   i.e. vsCreate has been already performed but not all the  */
          /*   remaining initialization work is done. */
          VS_RUNNING,            
	      /* the virtual stack instance has been successfully initialized */
          /*   and is at least ready to run */
          VS_DOWN_REQUESTED,          
	      /* stack deletion was requested (after failure or on user */
          /*   request). In this state all sockets have to be terminated */
          /*   etc. */
          VS_GOES_DOWN,          
	      /* stack components are shut down now - stack is not operating */
		  /*   anymore */
          VS_INIT_INCOMPL,      
	      /* the state indicates that the initialization of SCs failed */
		  /*   it is only a temporary state, the stack instance has to be */
		  /*   deleted afterwards */
          VS_DEL_INCOMPLETE      
	      /* the state indicates that the shutdown failed, manual */
          /*   intervention might be needed. This entry is in this state */
          /*   not reusable */
          } VS_STATUS_INFO;


/* Indications for optional and non-optional stack components (needed 
   for component registration) */ 
typedef enum {
           VS_SC_OPTIONAL = 1, 
	       VS_SC_NON_OPTIONAL
	     } VS_SC_OPT;

/* Indications for optional stack components (needed for component 
   registration) if enabled for a particular stack instance or not */
typedef enum {
          VS_SC_ENABLED = 1, 
	      VS_SC_DISABLED
	     } VS_SC_STATE;

/* Types of stack components */
typedef enum {
          VS_COMP, 
          VS_NONOPT_COMP, 
	      VS_OPT_COMP, 
	      VS_APPL,
		  VS_NON_CORE
	     } VS_TYPE;

/* Type of the "cookie" delivered back after component registration) */ 
typedef int VS_REG_ID;

/* The virtual stack number (actually an index to the vsTbl) */
typedef int VSNUM;

typedef unsigned VS_INSTANCE_FLAGS;

#define VIRTUAL_STACK_DELETE

/* A mask isolate the vsnum part from the VSID */
#define VSNUMMASK 0xFFF
#define VSNUM_INVALID_MASK 0x800

/* The maximum virtual stack number (determines also the size of vsTbl) */
#define VSNUM_MAX 64        /* actually max+1 value! */ 
#define VSFLAGS_MAX \
((VSNUM_MAX+(sizeof(VS_INSTANCE_FLAGS)*8)-1)/(sizeof(VS_INSTANCE_FLAGS)*8)) 

#define VSID_MAX  VSNUM_MAX

#define VS_MGMT_STACK 0

typedef unsigned VSID;


#define VS_NAME_MAX     32

/* VS Layer.  Things the VS system needs. */
typedef struct vs_global_data_hdr{
           char  name[VS_NAME_MAX+1]; /* Name of this virtual stack. */
	       char *pName;
           VSID vsid;                 /* for easier conversion for the new vsid */
           void *pVsMgrGlobals;
           } VS_GLOBAL_DATA_HDR;


typedef struct vsScListEntry_s {
	/* general virtual stack component infos */
	char       *pVsScName;
	VS_TYPE 	vsScType;
	VS_INSTANCE_FLAGS vsScEnabledFlgs[VSFLAGS_MAX];
	
	/* instance initialization related fields */
	STATUS     (*pVsInstInitRtn)(void *);
	void       *pVsDefaultCfgParams;
	unsigned   vsDefaultCfgParamsLen;
	void       *pVsIndividualTmpCfgParams;
	unsigned   vsIndividualTmpCfgParamsLen;
	VS_INSTANCE_FLAGS vsScInitExecFlgs[VSFLAGS_MAX];
	VS_INSTANCE_FLAGS vsScInitCurrentlyExecFlgs[VSFLAGS_MAX];
	int        vsInstInitErrorCode;
	VSID	   vsInstInitErrorCodeRelatedVsid;
	
	/* instance deletion related fields */
	STATUS     (*pVsScDestructorRtn)(VSNUM vsnum);
	VS_INSTANCE_FLAGS vsScLateDestrRegistrationFlgs[VSFLAGS_MAX];
	VS_INSTANCE_FLAGS vsScDestrExecFlgs[VSFLAGS_MAX];
	VS_INSTANCE_FLAGS vsScDestrCurrentlyExecFlgs[VSFLAGS_MAX];
	int        vsInstDeleteErrorCode;
	VSID	   vsInstDeleteErrorCodeRelatedVsid;
	
	} vsScListEntry_t;

typedef vsScListEntry_t VS_SC_DESCRIPTOR;

typedef struct {
	BOOL	 VsDebEnabled;
	unsigned VsDeleteTimeo;
	unsigned VsSocketTimeo;
	unsigned VsApplTimeo;
	} vsMgrCfgPar_t;
	
typedef vsMgrCfgPar_t VS_MGR_CFG_DATA;

typedef struct {
	unsigned VsInUse;
	unsigned VsnumMax;
    unsigned vsInstancesCreated;
    unsigned vsInstancesDeleted;
	unsigned StackCreationsFailed;
    unsigned StackDeletionsFailed;
	BOOL     CreateInProgress;
	BOOL     DeleteInProgress;
	} vsMgrInf_t;  

typedef vsMgrInf_t VS_MGR_INFO; 

IMPORT const unsigned vsNumMax;

IMPORT int myStackNum;

IMPORT STATUS (*pMuxDevUnload)(char *pName, int unit);


/* The following prototypes are only provided for backward compatibility */
IMPORT STATUS virtualStackLibInit();
IMPORT STATUS virtualStackCreate(const char* pName, VSID* pVSID);
IMPORT STATUS virtualStackDelete(VSID vsid);
IMPORT STATUS virtualStackIdGet(const char* pName, VSID* pVSID);
IMPORT void virtualStackShow(VSID vsid);
IMPORT void virtualStackListShow(void);
STATUS virtualStackNameTaskIdSet(const char* pName);
STATUS virtualStackNumTaskIdSet(int vsNum);
STATUS virtualStackIdCheck();
STATUS virtualStackNameGet (char *pName, int vsid);
STATUS virtualStackNameSet (const char *pName, int vsid);
STATUS vsMakeStack(const char * vsName, int maxUnits);
STATUS virtualStackNumGet (VSID, int *);

/* new prototypes - beginning with vs... */

VS_STATUS vsCreate
    (
    const char * pName, /* Unique stack name, or NULL for default */
    VSID * pVID         /* Buffer for storing virtual stack identifier */
    );

VS_STATUS vsDelete( VSID vsid);


VS_STATUS vsIdGet
    (
    const char * pName, /* Stack name from virtualStackCreate routine */
    VSID * pVSID        /* Storage for VSID associated with stack name */
    );

VS_STATUS vsNameGet
    (
    char *    pName,    /* Buffer for the stack name (32 characters + EOS) */
    const VSNUM vsnum   /* Virtual stack number */
    );

VS_STATUS vsShow
    (
    const VSNUM vsnum   /* Virtual stack number  */
    );

VSNUM vsMaxNumGet
    (
	void
    );

VS_STATUS vsNumGet
    (
    const VSID vsid,    /* Virtual stack identifier from virtualStackCreate */
    VSNUM *vsnum        /* Storage for virtual stack number */
    );

VS_STATUS vsNumConvert
    (
    const VSNUM vsnum, 	/* virtual stack number (0 to VSNUM_MAX) */
    VSID  *vsid 	/* Storage for Virtual stack identifier from virtualStackCreate */
    );

VS_STATUS vsListShow (void);

VS_STATUS vsNameContextSet
    (
    const char * pName  /* Stack name from vsCreate routine */
    );

VS_STATUS vsIdContextSet
    (
    const VSID vsid 	/* vsid from vsCreate routine */
    );
    
VS_STATUS vsMyStackNumSet
    (
    const VSNUM       vsnum     /* Virtual stack number */
    );
    
VS_STATUS vsQuickContextSet
    (
    const VSID vsid 	/* vsid from vsCreate routine */
    );

VS_STATUS vsIndexGet
    (
    VSNUM       *vsnum          /* mem space for Virtual stack number */
    );

VS_STATUS vsNameSet
    (
    const char * pName,    /* Name for virtual stack (max 32 bytes) */
    const VSNUM vsnum              /* Virtual stack number */
    );

VS_STATUS vsIndexCreate (void);


int vsExec
    (
    int vsnum,          /* Virtual stack number */
    int (*pFunc)(),     /* Function to call */
    int   arg1,         /* First of 10 arguments for supplied function */
    int   arg2,
    int   arg3,
    int   arg4,
    int   arg5,
    int   arg6,
    int   arg7,
    int   arg8,
    int   arg9,
    int   arg10);

VS_STATUS vsNameVsnumAppend
    (
    const char* basename, 
    int stackNum, 
    char* destString, 
    int maxLen
    );



VS_STATUS vsComponentRegister
    (
    VS_REG_ID *registrationReference,
    const char *ComponentName,
    const VS_SC_STATE enabledIndication,
    const VS_SC_OPT optionIndication,
    STATUS (*initRtn)(),
    const size_t ConfigParamLen,
    void *cfgParams,
    STATUS (*destructorRtn)(VSNUM vsnum)
    );

VS_STATUS vsNonCoreComponentRegister
    (
    VS_REG_ID *registrationReference,		/* registration reference */
    const char *NonCoreCompName,			/* name of the component */
    const VS_SC_STATE enabledIndication, 	/* enabled for vsnum 0 indication */
    STATUS (*initRtn)(),		/* pointer to instInit function */
    const size_t ConfigParamLen,			/* size of default cfg parameters */
    void *cfgParams,						/* pointer to default cfg params */
    STATUS (*destructorRtn)(VSNUM vsnum)	/* pointer to destructor function */
    );

VS_STATUS vsApplicationRegister
    (
    VS_REG_ID *registrationReference,
    const char *ApplicationName,
    const VS_SC_STATE enabledIndication, 
    STATUS (*initRtn)(),
    const size_t ConfigParamLen,
    void *cfgParams,
    STATUS (*destructorRtn)(VSNUM vsnum)
    );

VS_STATUS vsComponentStatusSet
    (
    const VSNUM vsnum,
    const VS_REG_ID registrationReference, 
    const VS_SC_STATE enabledIndication,
    void *cfgParams
    );

BOOL vsComponentStatusGet
    (
    const VSNUM vsnum,
    const VS_REG_ID registrationReference
    );

VS_STATUS vsComponentGet
    (
    const VS_REG_ID registrationReference,
    VS_SC_DESCRIPTOR *SCdescriptor
    );

VS_STATUS vsComponentShow
    (
    const VS_REG_ID registrationReference
    );

VS_STATUS vsComponentErrorShow
    (
   	const VS_REG_ID registrationReference
    );

VS_STATUS vsScShortShow
    (
   	VSNUM vsnum,
	BOOL DisabledOnly
    );

VS_STATUS vsParamsGet
    (
    const VS_REG_ID registrationReference,
    void *pCfgParams,
    size_t paramsLength
    );

VS_STATUS vsComponentIdGet
    (
    const char *Name,
    VS_REG_ID *registrationReference
    );

VS_STATUS vsInitRtnsCall
    (
    const VSNUM vsnum, 
    const VS_TYPE ComponentType
    );

VS_STATUS_INFO vsStatusGet
    (
	void
    );
	
void vsStatusShow
    (
	VSID vsid
    );
	

VS_STATUS vsStart
    (
    const VSNUM vsnum,
    const VS_TYPE ComponentType
    );

VS_STATUS vsStop
    (
    const VSNUM vsnum,
    VS_TYPE ComponentType
    );


int vsNumOfOpenSocketsGet
	(
	void
	);


int vsDownToSocketsSend 
    (
    int SocketTimeout
    );


VS_STATUS vsMgrStackInstanceCreate 
	(
	const char *StackName,
	void (*pUsrScStateChange)(const char *StackName, const VSNUM vsnum),
	void (*pUsrNetworkDevAttach)(const char *StackName),
	VSID *vsid
	);
 
 
VS_STATUS vsMgrStackInstanceDelete
	(
	VSID vsid
	);

	
VS_STATUS vsMgrDestructor
	(
	BOOL forced
	);


VS_STATUS vsMgrInfoGet
	(
	VS_MGR_INFO *pInfo
	);

	
VS_STATUS vsStatusSet
	(
	VS_STATUS_INFO vsStatus
	);


VS_STATUS vsSocketList
	(
	void
	);
	
VS_STATUS vsDevRestart 
	(
	char * DevName, 
	int Unit
	);

VS_STATUS vsIfDetach(void);

VSNUM vsNextGet
	(
	void
	);

VS_STATUS vsRegistrationStopSet
	(
	BOOL value	/* value, if TRUE, then registration is stopped, wehen FALSE
	               it is enabled again */
	);
	


	STATUS vsMgrInit(VS_MGR_CFG_DATA *cfgData);
	STATUS vsExampleInit(void);
	STATUS vsDebInit(void);
	STATUS vsOldInit(void);
	STATUS vsShowInit(void);

    VS_STATUS vsLibInit (VS_MGR_CFG_DATA *cfgData);

#ifdef __cplusplus
}
#endif

#endif /* __INCvsLibh */
