/* vsFastUdp.h - virtual stack data for Fast UDP */

/* Copyright 2002 Wind River Systems, Inc. */

/*
modification history
--------------------
01f,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01e,04nov03,rlm  Ran batch header path update for header re-org.
01d,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01c,26nov02,hgo  modifications for accordion
01b,01aug02,spm  fixed modification history after automatic merge
01a,18jul02,wrs  automatic merge from version 1 of Accordion Munich branch:
                 (copied from Router Stack release: 01a,04jun02,vvv)
*/

#ifndef __INCvsfastudph
#define __INCvsfastudph


/* includes */

#include <net/fastUdpLib.h>

/* defines */

/* typedefs */

typedef struct vs_fastudp
    {
    /* locals from fastUdpLib.c */

    FASTUDP_ID  _priv_fastUdpHead;
    u_int       _priv_fastUdpDebug;
    SEM_ID      _priv_mutexSem;
    LIST        _priv_portList;
    SEM_ID      _priv_portListSem;
    BOOL        _priv_vsWaitingForFastUdp;
    SEM_ID      _priv_vsFastUdpListSync;
    } VS_FASTUDP;


/* macros */

#define VS_FASTUDP_DATA ((VS_FASTUDP *) vsTbl[myStackNum]->pFastUdpGlobals)

#endif /* __INCvsfastudph */
