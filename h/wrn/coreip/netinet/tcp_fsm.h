/* tcp_fsm.h - TCP finite state machine */

/* Copyright 2001 - 2003 Wind River Systems, Inc. */

/*
 * Copyright (c) 1982, 1986, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)tcp_fsm.h	8.1 (Berkeley) 6/10/93
 * $FreeBSD: src/sys/netinet/tcp_fsm.h,v 1.14 1999/11/07 04:18:30 jlemon Exp $
 */

/*
modification history
--------------------
01l,20nov03,niq  Remove copyright_wrs.h file inclusion
01k,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01j,04nov03,rlm  Ran batch header path update for header re-org.
01i,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01h,08aug03,niq  Merging from Accordion label ACCORDION_BASE6_MERGE_BASELINE
01g,10jun03,vvv  include netVersion.h
01f,15may03,spm  Tornado 2.2 FCS merge (SPR #70380; ver 01c,07dec01,rae:
                 TOR2_2-FCS-COPY label, tor2 branch, /wind/river VOB)
01e,07mar03,pas  moved tcp_outflags to tcp_output.c
                 moved tcpstates to tcp_usrreq.c, removed pTcpstates
                 removed tcp_acounts
01d,16sep02,pas  merged changes from Veloce stack, added pTcpstates
01c,10sep02,hsh  add c++ protection
01b,08feb02,pas  EAR code cleanup
01a,13aug01,pas  Created from FreeBSD4.3-RELEASE(July-17-01).
*/  

#ifndef _NETINET_TCP_FSM_H_
#define _NETINET_TCP_FSM_H_

#ifdef __cplusplus
extern "C" {
#endif
     
#include <netVersion.h>

/*
 * TCP FSM state definitions.
 * Per RFC793, September, 1981.
 */

#define	TCP_NSTATES	11

#define	TCPS_CLOSED		0	/* closed */
#define	TCPS_LISTEN		1	/* listening for connection */
#define	TCPS_SYN_SENT		2	/* active, have sent syn */
#define	TCPS_SYN_RECEIVED	3	/* have send and received syn */
/* states < TCPS_ESTABLISHED are those where connections not established */
#define	TCPS_ESTABLISHED	4	/* established */
#define	TCPS_CLOSE_WAIT		5	/* rcvd fin, waiting for close */
/* states > TCPS_CLOSE_WAIT are those where user has closed */
#define	TCPS_FIN_WAIT_1		6	/* have closed, sent fin */
#define	TCPS_CLOSING		7	/* closed xchd FIN; await FIN ACK */
#define	TCPS_LAST_ACK		8	/* had fin and close; await FIN ACK */
/* states > TCPS_CLOSE_WAIT && < TCPS_FIN_WAIT_2 await ACK of FIN */
#define	TCPS_FIN_WAIT_2		9	/* have closed, fin is acked */
#define	TCPS_TIME_WAIT		10	/* in 2*msl quiet wait after close */

/* for KAME src sync over BSD*'s */
#define	TCP6_NSTATES		TCP_NSTATES
#define	TCP6S_CLOSED		TCPS_CLOSED
#define	TCP6S_LISTEN		TCPS_LISTEN
#define	TCP6S_SYN_SENT		TCPS_SYN_SENT
#define	TCP6S_SYN_RECEIVED	TCPS_SYN_RECEIVED
#define	TCP6S_ESTABLISHED	TCPS_ESTABLISHED
#define	TCP6S_CLOSE_WAIT	TCPS_CLOSE_WAIT
#define	TCP6S_FIN_WAIT_1	TCPS_FIN_WAIT_1
#define	TCP6S_CLOSING		TCPS_CLOSING
#define	TCP6S_LAST_ACK		TCPS_LAST_ACK
#define	TCP6S_FIN_WAIT_2	TCPS_FIN_WAIT_2
#define	TCP6S_TIME_WAIT		TCPS_TIME_WAIT

#define	TCPS_HAVERCVDSYN(s)	((s) >= TCPS_SYN_RECEIVED)
#define	TCPS_HAVEESTABLISHED(s)	((s) >= TCPS_ESTABLISHED)
#define	TCPS_HAVERCVDFIN(s)     ((s) > TCPS_ESTABLISHED && \
                                    (s) != TCPS_FIN_WAIT_1 && \
                                    (s) != TCPS_FIN_WAIT_2)

extern	char *tcpstates[];

#ifdef __cplusplus
}
#endif
    
#endif
