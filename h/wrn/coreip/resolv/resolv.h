/* resolv.h - resolver internal data structures header file */

/* Copyright 1996 - 2004 Wind River Systems, Inc. */

/* $NetBSD: resolv.h,v 1.8 1994/10/26 00:56:16 cgd Exp $	*/
/*
 * Copyright (c) 1983, 1987, 1989 The Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the University of
 *    California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)resolv.h	5.15 (Berkeley) 4/3/91
 */

/*
modification history
--------------------
01m,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01l,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/2)
01k,15mar04,rp   merged from orion
01j,09feb04,xli  add <netinet/in.h> header to the include for base6 itn4 changes
01i,20nov03,niq  Remove copyright_wrs.h file inclusion
01h,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01g,04nov03,rlm  Ran batch header path update for header re-org.
01f,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01e,06aug03,nee  Accordion Base6 merge from ACCORDION_BASE6_MERGE_BASELINE
                 label
01d,21aug02,vlk	 Added second param into __p_time() function
01c,20sep01,vlk  Added structure "__res_state_ext" and the declaration of routines:
                  resSearch(), resSearchN(), resolvQueryN(), res_dnok(), res_hnok() 
01b,02apr97,jag  Change types from u_int*_t to uint*_t. Added C++ and function
         prototypes.
01a,11dec96,jag  created from BSD4.4 release. Deleted constants that are not
         used by vxWorks.  Changed bit fields to meet ANSI specs.
*/
#ifndef __INCresolvh
#define	__INCresolvh

#ifdef _WRS_KERNEL 
#include <netconf.h>
#endif
#include <stdio.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#ifdef INET6
#include <netinet6/in6.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif
/*
 * revision information.  this is the release date in YYYYMMDD format.
 * it can change every day so the right thing to do with it is use it
 * in preprocessor commands such as "#if (__RES > 19931104)".  do not
 * compare for equality; rather, use it to determine whether your resolver
 * is new enough to contain a certain feature.
 */
#define	__RES	20040310
/*
 * Global defines and variables for resolver stub.
 */
#define	MAXNS		  3	/* max # name servers we'll track */
#define	MAXDFLSRCH	  3	/* # default domain levels to try */
#define	MAXDNSRCH	  6	/* max # domains in search path */
#define	LOCALDOMAINPARTS  2	/* min levels in name that is "local" */
#define	MAXDNSLUS	  4	/* max # of host lookup types */

#define	RES_TIMEOUT	  5	/* min. seconds between retries */
#define MAXRESOLVSORT	  10	/* number of net to sort on */
#define RES_MAXNDOTS	  15	/* should reflect bit field size */

    struct __res_state
        {
        int retrans;            /* retransmition time interval */
        int retry;              /* number of times to retransmit */
        long    options;        /* option flags - see below. */
        int nscount;            /* number of name servers */
        struct  sockaddr_in nsaddr_list[MAXNS]; /* address of name server */
#define	nsaddr	nsaddr_list[0]	/* for backward compatibility */
        u_short id;             /* current packet id */
        char    *dnsrch[MAXDNSRCH+1];   /* components of domain to search */
        char    defdname[MAXDNAME];     /* default domain */
        long    pfcode;         /* RES_PRF_ flags - see below. */
        u_int   ndots:4,        /* threshold for initial abs. query */
        nsort:4,                /* number of elements in sort_list[] */
        :0;                     /* ANSI padding field */
        struct
            {
            struct in_addr addr;
            uint32_t mask;
            } sort_list[MAXRESOLVSORT];
        char    lookups[MAXDNSLUS];
        };


/**********************from KAME *************************************/
/* for INET6 */
/*
 * replacement of __res_state, separated to keep binary compatibility.
 */
#ifdef INET6
    struct __res_state_ext
        {

        struct sockaddr_storage nsaddr_list[MAXNS];

        struct
            {

            int af; /* address family for addr, mask */

            union
                {

                struct  in_addr ina;

                struct  in6_addr in6a;

                } addr, mask;

            } sort_list[MAXRESOLVSORT];

        };
#endif /* INET6 */
/*
 * Resolver options
 */
#define RES_INIT	0x0001		/* address initialized */
#define RES_DEBUG	0x0002		/* print debug messages */
#define RES_AAONLY	0x0004		/* authoritative answers only */
#define RES_USEVC	0x0008		/* use virtual circuit */
#define RES_PRIMARY	0x0010		/* query primary server only */
#define RES_IGNTC	0x0020		/* ignore trucation errors */
#define RES_RECURSE	0x0040		/* recursion desired */
#define RES_DEFNAMES	0x0080		/* use default domain name */
#define RES_STAYOPEN	0x0100		/* Keep TCP socket open */
#define RES_DNSRCH	0x0200		/* search up local domain tree */
#define	RES_INSECURE1	0x00000400	/* type 1 security disabled */
#define	RES_INSECURE2	0x00000800	/* type 2 security disabled */
#define	RES_NOALIASES	0x00001000	/* shuts off HOSTALIASES feature */
#define	RES_USE_INET6	0x00002000	/* use/map IPv6 in gethostbyname() */
#define	RES_NOTLDQUERY	0x00004000	/* Don't query TLD names */

#define RES_DEFAULT	(RES_RECURSE | RES_DEFNAMES | RES_DNSRCH)
/*
 * Resolver "pfcode" values.  Used by dig.
 */
#define	RES_PRF_STATS	0x0001
/*			0x0002  */
#define	RES_PRF_CLASS	0x0004
#define	RES_PRF_CMD	0x0008
#define	RES_PRF_QUES	0x0010
#define	RES_PRF_ANS	0x0020
#define	RES_PRF_AUTH	0x0040
#define	RES_PRF_ADD	0x0080
#define	RES_PRF_HEAD1	0x0100
#define	RES_PRF_HEAD2	0x0200
#define	RES_PRF_TTLID	0x0400
#define	RES_PRF_HEADX	0x0800
#define	RES_PRF_QUERY	0x1000
#define	RES_PRF_REPLY	0x2000
#define	RES_PRF_INIT	0x4000
/*			0x8000  */
    
    struct res_sym
        {
        int number;         /* Identifying number, like T_MX */
        char *  name;       /* Its symbolic name, like "MX" */
        char *  humanname;  /* Its fun name, like "mail exchanger" */
        };


/* this shouldn't be used by normal users */
    struct res_target
        {
        struct res_target *next;
        const char *name;   /* domain name */
        int qclass, qtype;  /* class and type of query */
        u_char *answer;     /* buffer to put answer */
        int anslen;     /* size of answer buffer */
        int n;          /* result length */
        };

    IMPORT struct __res_state _res;

/**************************from KAME***************************/
/* for INET6 */
#ifdef INET6
    struct __res_state_ext _res_ext;

    IMPORT const struct res_sym __p_class_syms[];
    IMPORT const struct res_sym __p_type_syms[];
#endif
/***************************************************************/

/* Private routines shared between libc/net, named, nslookup and others.  
#define	res_hnok	__res_hnok
#define	res_ownok	__res_ownok
#define	res_mailok	__res_mailok
#define	res_dnok	__res_dnok
#define	sym_ston	__sym_ston
#define	sym_ntos	__sym_ntos
#define	sym_ntop	__sym_ntop */
#define	dn_skipname	__dn_skipname
#define	fp_query	__fp_query
#define	hostalias	__hostalias
#define	putlong		__putlong
#define	putshort	__putshort
#define p_class		__p_class
#define p_time		__p_time
#define p_type		__p_type

#if defined(__STDC__) || defined(__cplusplus)

#define	__P(protos)	protos	/* Use ANSI C proto for resolver */

#else   /* __STDC__ */

#define	__P(protos)

#endif  /* __STDC__ */

#ifdef _WRS_KERNEL

    int  __dn_skipname __P((const u_char *, const u_char *));
    void     __fp_query __P((const u_char *, FILE *));
    char    *__hostalias __P((const char *));
    void     __putlong __P((register uint32_t, u_char *));
    void     __putshort __P((register uint16_t, u_char *));
    uint32_t _getlong __P((register const u_char *));
    const char  *__p_class __P((int));
    const char  *__p_time __P((uint32_t,char[]));
    const char  *__p_type __P((int));

    int  resolvQuery __P((char *, int, int, u_char *, int));
    int  resSearch __P((const char *, int, int, u_char *, int));
    int  resolvMkQuery __P((int, const char *, int, int, const char *, int,
                            const char *, char *, int));
    int  resolvQueryN __P((const char *,struct res_target *));
    int  resSearchN __P((const char *,struct res_target *));  
    int  resolvSend __P((const char *, int, char *, int));
    int  res_dnok __P((const char *));

#endif /* _WRS_KERNEL */

    uint16_t _getshort __P((register const u_char *));
    uint16_t  res_getshort __P((register const u_char *));
    int  res_hnok __P((const char *));

#ifdef __cplusplus
}
#endif

#endif /* __INCresolvh */
