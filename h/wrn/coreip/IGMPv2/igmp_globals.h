/* igmp_globals.h - file with the single global */

/* Copyright 1996 - 2001 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,20nov03,niq  Remove copyright_wrs.h file inclusion
01a,29mar01,spm  file creation: copied from version 01c of tor2_0.open_stack
                 branch (wpwr VOB) for unified code base
*/

#if !defined (_IGMP_GLOBAL_H_)
#define _IGMP_GLOBAL_H_

#ifndef VIRTUAL_STACK
IGMP_CLASS vs_igmp;
#endif /* VIRTUAL_STACK */

#endif /* _IGMP_GLOBAL_H_ */
