/* igmp_state_machine_structures.h - supporting structures for state machine */

/* Copyright 1997 - 2001 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,20nov03,niq  Remove copyright_wrs.h file inclusion
01b,29mar01,spm  file creation: copied from version 01a of tor2_0.open_stack
                 branch (wpwr VOB) for unified code base
*/

#if !defined (_IGMP_STATEMACH_STRUCTURES_H_)
#define _IGMP_STATEMACH_STRUCTURES_H_

typedef struct IGMP_ROUTER_PORT_STATE_TRANSITION_TABLE_ENTRY
{
	void (*fptr_action_1) (UINT port, ULONG group_ip_address);
	void (*fptr_action_2) (UINT port, ULONG group_ip_address);
	void (*fptr_action_3) (UINT port, ULONG group_ip_address);

	enum IGMP_ROUTER_PORT_STATE next_state;
} IGMP_ROUTER_PORT_STATE_TRANSITION_TABLE_ENTRY;

typedef struct IGMP_ROUTER_GROUP_STATE_TRANSITION_TABLE_ENTRY
{
	void (*fptr_action_1) (UINT port, ULONG group_ip_address);
	void (*fptr_action_2) (UINT port, ULONG group_ip_address);
	void (*fptr_action_3) (UINT port, ULONG group_ip_address);

	enum IGMP_ROUTER_GROUP_STATE next_state;
} IGMP_ROUTER_GROUP_STATE_TRANSITION_TABLE_ENTRY;

typedef struct IGMP_HOST_STATE_TRANSITION_TABLE_ENTRY
{
	void (*fptr_action_1) 			(UINT port, ULONG group_ip_address);
	void (*fptr_action_2) 			(UINT port, ULONG group_ip_address);
	void (*fptr_action_3) 			(UINT port, ULONG group_ip_address);

	enum IGMP_HOST_GROUP_STATE 	next_state;
} IGMP_HOST_STATE_TRANSITION_TABLE_ENTRY;

#endif /* _IGMP_STATEMACH_STRUCTURES_H_*/
