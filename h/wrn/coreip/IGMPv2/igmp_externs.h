/* igmp_externs.h - publicly accessible routines from Routerware igmp code */

/* Copyright 1997 - 2001 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,20nov03,niq  Remove copyright_wrs.h file inclusion
01d,04nov03,rlm  Ran batch header path update for header re-org.
01c,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01b,03nov03,rlm  Relocated from .wind_vxw_h to /vobs/Clarinet_IPv6 vob for
                 header re-org.
01a,29mar01,spm  file creation: copied from version 01a of tor2_0.open_stack
                 branch (wpwr VOB, written by rae) for unified code base;
                 fixed header entries
*/

#ifndef __INCigmp_externsh
#define __INCigmp_externsh

/* added (or changed ...) by rae */

void igmpCallBack(char * buf, struct in_addr  srcAddr, UINT port);
void terminate_igmp_port (UINT port);
/* igmp_control.c */
int igmp_shutdown (void);

int  initialize_igmp (ULONG clock_ticks_per_second);
void initialize_igmp_port (UINT port);

void igmp_timer (void);



#endif /* __INCigmp_externsh */
