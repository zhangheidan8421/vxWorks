/* igmpRouterLib.h - igmp include file for router side code */

/*
 * Copyright (c) 2000-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
02b,30mar05,kch  Removed igmpCapSend() prototype.
02a,02aug04,rae  Ported to Dual Stack 
01m,04nov03,rlm  Ran batch header path update for header re-org.
01l,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01k,03nov03,rlm  Relocated from .wind_vxw_h to /vobs/Clarinet_IPv6 vob for
                 header re-org.
01j,13jan03,rae  Merged from velocecp branch (SPRs 83920, 72379)
01i,21dec01,niq  added missing #else for non-ANSI C prototypes
01h,18dec01,spm  added modification history entry for automatic merge
01g,18dec01,wrs  automatic merge from version 5 of tor3_x.synth.irvine remote
                 branch: added callback to report interface status to PIM
01f,31jul01,rae  add new error code
01e,28jul01,spm  added modification history entry for automatic merge; fixed
                 version numbers from 01f changes
01d,27jul01,cjh  fixed prototype for igmpSetPimCallback
01c,28jun01,cjh  added APIs for new mroute/PIM
01b,29may01,rae  added fixes from Beta
01a,29mar01,spm  file creation: copied from version 01d of tor2_0.open_stack
                 branch (wpwr VOB, written by rae) for unified code base
*/

#ifndef __INCigmpRouterLibh
#define __INCigmpRouterLibh

#include <netinet/igmp.h>
#include <netinet/igmp_var.h>
#include <netinet/ip_mroute.h>
#include "lstLib.h"

#ifdef __cplusplus
extern "C" {
#endif

/* defines */

#define S_igmpRouterLib_NOT_INITIALIZED      (M_igmpRouterLib | 1)
#define S_igmpRouterLib_VIF_OUT_OF_RANGE     (M_igmpRouterLib | 2)
#define S_igmpRouterLib_NO_FREE_VIFS         (M_igmpRouterLib | 3)
#define S_igmpRouterLib_INVALID_PORT_NUM     (M_igmpRouterLib | 4)
#define S_igmpRouterLib_NOT_ALL_IFS_DOWN     (M_igmpRouterLib | 5)
#define S_igmpRouterLib_THRESHOLD_REQUIRED   (M_igmpRouterLib | 6)
#define MAX_PORT_NUM MAXVIFS


/* typedefs and enum */

enum iCapType { IGMP_TIMER_CAP, IGMP_MSG_CAP };

enum igmpIfMsgType { IGMP_IF_DOWN, IGMP_IF_UP };



typedef struct		
    {
    struct in_addr   addr;
    struct in_addr   mask;
    struct ifnet *   pIfnet;
    int              index;
    } IGMP_PORT_INFO;
    
typedef struct
    {
    int              type;
    int              index;
    struct in_addr   source;
    struct igmp      igmp;
    } IGMP_CAPSULE;
    
typedef struct
    {
    NODE node;
    VOIDFUNCPTR pNotifyHook;
    } IGMP_HOOK_LIST;

/* function declarations */

extern STATUS igmpInterfaceEnable (char* pDrv, int unit, UINT32 threshold, 
				   UINT32 rate_limit);
extern STATUS igmpInterfaceDisable (char* pDrv, int unit);
extern STATUS igmpGroupDelete(int port, struct in_addr groupAddr);
extern STATUS igmpGroupAdd(int port, struct in_addr groupAddr);
extern int igmpPktSend(int port, caddr_t buf, int bufLen, struct sockaddr * to,
                int toLen);
extern struct in_addr * portToIP(int port);
extern UINT32 portToIf(int port);
extern struct ifnet * portToIfnet(int port);
extern STATUS igmpRouterLibInit(void);
extern STATUS igmpRouterLibQuit(void);

extern STATUS igmpPimCallbackSet(VOIDFUNCPTR _pimHook);
extern STATUS igmpPimInterfaceCallbackSet(VOIDFUNCPTR _pimInterfaceHook);
extern STATUS igmpAssertSet(FUNCPTR _pAssertHook);
extern struct ifnet* igmpPortToIfnet (int port);
extern STATUS igmpNameToPort(char*pDrv, int unit);
extern STATUS igmpDebugSet(int debug);
extern STATUS igmpMfcSet(int messageType, struct mfcctl* gCtl);
extern STATUS igmpNotifyHookAdd (VOIDFUNCPTR);
extern STATUS igmpNotifyHookDelete (VOIDFUNCPTR);

#ifdef __cplusplus
}
#endif

#endif /* __INCigmpRouterLibh */
