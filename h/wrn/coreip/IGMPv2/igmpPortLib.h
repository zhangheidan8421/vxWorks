/* igmpPortLib.h - header */

/* Copyright 2000 - 2001  Wind River Systems, Inc. */

/*
modification history
--------------------
02a,02aug04,rae  Ported to Dual Stack 
01e,20nov03,niq  Remove copyright_wrs.h file inclusion
01d,04nov03,rlm  Ran batch header path update for header re-org.
01c,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01b,03nov03,rlm  Relocated from .wind_vxw_h to /vobs/Clarinet_IPv6 vob for
                 header re-org.
01a,29mar01,spm  file creation: copied from version 01b of tor2_0.open_stack
                 branch (wpwr VOB, written by rae) for unified code base
*/

#ifndef __INCigmpPortLibh
#define  __INCigmpPortLibh

#ifdef __cplusplus
extern "C" {
#endif
#include <avlUintLib.h>

/* defines */

#define MAX_IF_INDEX 0x7fffffff /* following MIB RFCs specification */
#define INVALID_IF_INDEX ~0

/* typedefs */

typedef struct
    {
    AVLU_NODE    avlBase;
    UINT32      vif;
    } IGMP_PORT_ENTRY;

/* function declarations */
extern STATUS igmpPortLibInit(void);
extern STATUS igmpPortAdd(UINT32 ifIndex, UINT32 vif);
extern STATUS igmpPortDel(UINT32 ifIndex);
extern STATUS igmpPortGet(UINT32 ifIndex, UINT32 * pvif);
extern STATUS igmpPortNextGet(UINT32 ifIndex, UINT32 * nextIfIndex,
                              UINT32 * pvif);
extern STATUS igmpRootGet(AVLU_TREE ** pigmpPortTreePtr);
extern STATUS igmpPortLibQuit(void);

#ifdef __cplusplus
}
#endif

#endif /*  __INCigmpPortLibh */
