/* igmpCacheLib.h - header */

/* Copyright 2000 - 2001 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,20nov03,niq  Remove copyright_wrs.h file inclusion
01d,04nov03,rlm  Ran batch header path update for header re-org.
01c,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01b,03nov03,rlm  Relocated from .wind_vxw_h to /vobs/Clarinet_IPv6 vob for
                 header re-org.
01a,29mar01,spm  file creation: copied from version 01b of tor2_0.open_stack
                 branch (wpwr VOB, written by rae) for unified code base
*/

#ifndef __INCigmpCacheLibh
#define  __INCigmpCacheLibh

#ifdef __cplusplus
extern "C" {
#endif

/* defines */

#define MAX_IF_INDEX 0x7fffffff /* following MIB RFCs specification */
#define INVALID_IF_INDEX ~0

/* typedefs */

/* function declarations */

extern STATUS igmpCacheLibInit(void);
extern STATUS igmpCacheAdd(UINT32 mcastAddr, UINT32 ifIndex, void* groupClass);
extern STATUS igmpCacheDel(UINT32 mcastAddr, UINT32 ifIndex);
extern STATUS igmpCacheGet(UINT32 mcastAddr, UINT32 ifIndex, void* * pgroupClass);
extern STATUS igmpCacheNextGet(UINT32 mcastAddr, UINT32 ifIndex, UINT32 * nextMcastAddr,
                               UINT32 * nextIfIndex, void * * pgroupClass);
extern STATUS igmpCacheLibQuit (void);

#ifdef __cplusplus
}
#endif

#endif /*  __INCigmpCacheLibh */
