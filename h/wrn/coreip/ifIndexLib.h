/* ifIndexLib.h - interface index library header */

/* Copyright 2000 - 2001 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,19apr04,rae  update API, ifIndex is now UINT32
01a,29mar01,spm  file creation: copied from version 01c of tor2_0.open_stack
                 branch (wpwr VOB) for unified code base
*/

#ifndef __INCifIndexLibh
#define __INCifIndexLibh

#ifdef __cplusplus
extern "C" {
#endif

#include "wrn/netVersion.h"
/* function declarations */

extern void ifIndexLibInit(void);
extern void ifIndexLibShutdown(void);
extern UINT32  ifIndexAlloc(void);
extern BOOL ifIndexTest(UINT32 ifIndex);
extern STATUS ifIndexLibInit2 (FUNCPTR, FUNCPTR, FUNCPTR, void *,
                               FUNCPTR, FUNCPTR, void *);
extern struct ifnet * ifIndexLookup (UINT32);
extern UINT32 ifIndexAlloc2 (struct ifnet *);
extern STATUS ifIndexFree (UINT32);

#ifdef __cplusplus
}
#endif

#endif /* __INCifIndexLibh */
