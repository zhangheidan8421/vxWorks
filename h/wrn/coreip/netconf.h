/* netconf.h - network component configuration file */

/*
 * Copyright (c) 1984-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. No license to Wind River intellectual property rights
 * is granted herein. All rights not licensed by Wind River are reserved
 * by Wind River.
 */

/*
modification history
--------------------
01s,25feb05,niq  merge from niqbal.tor3.mroute for IP router alert changes
01r,21jan05,sar  Removal of divert, dummynet and fw code.
01q,20nov03,niq  Remove copyright_wrs.h file inclusion
01p,06aug03,nee  Accordion Base6 merge from ACCORDION_BASE6_MERGE_BASELINE
                 label
01o,29apr03,syy  Added #ifdef for __cplusplus
01n,26feb03,vvv  added RSVP definition to control inclusion of RSVP code
01m,16jan03,ann  created a definition for IP flow and changed the way DUMMYNET
                 is defined
01l,10feb03,vvv  removed INET6 definition
01k,25jul02,pas  added TINY_LOMTU
01j,25feb02,nee  setting COMPAT_RFC2292
01i,01oct01,ppp  linking in ipsec
01h,21sep01,pas  added TCPDEBUG
01g,18sep01,ham  reversed the mod 01f.
01f,18sep01,ham  diabled INET6 temporary for build party.
01e,06sep01,ppp  Removing the defination for INET , it is defines in vxWorks.h
01d,06sep01,qli  adding more defintions
01c,28aug01,ppp  adding two more definations for in_proto.c
01b,27aug01,ppp  Adding a defination for ICMP
01a,23aug01,ppp  Removed the definition of INET
*/

#ifndef _NETCONF_H_
#define _NETCONF_H_

#ifdef __cplusplus
extern "C" {
#endif

#define COMPAT_RFC2292          1
#define MROUTING		0
#define MROUTE_LKM		0

#if 0 /* Clarinet */
#define IPSEC 1
#endif /* Clarinet */			

#define ICMP_BANDLIM    1

#define TCPDEBUG	1

#undef IPFLOW           /* IP Flow undefined by default */

#define TINY_LOMTU

#ifdef __cplusplus
}
#endif

#endif
