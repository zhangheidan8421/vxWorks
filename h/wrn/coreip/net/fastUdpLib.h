/* fastUdpLib.h - defines for the Fast UDP library */

/* Copyright 2002 - 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01g,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01f,04nov03,rlm  Ran batch header path update for header re-org.
01e,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01d,15sep03,vvv  updated path for new headers
01c,08nov02,hgo  added configuration parameters, fastUdpInitialized
01b,01aug02,spm  fixed modification history after automatic merge
01a,18jul02,wrs  automatic merge from version 1 of Accordion Munich branch:
                 (copied from Router Stack release: 01a,04jun02,vvv)
*/

#ifndef __INCfastudpLibh
#define __INCfastudpLibh

/* includes */

#include <semLib.h>
#include <lstLib.h>
#include <netinet/in.h>
#include <netBufLib.h>
#include <zbufLib.h>
#include <sys/socket.h>
#include <cfgDefs.h>

/* defines */

/* Fast UDP error numbers */

#define S_fastUdpLib_INVALID_PORT          (M_fastUdpLib | 1)
#define S_fastUdpLib_INVALID_COOKIE        (M_fastUdpLib | 2)
#define S_fastUdpLib_INVALID_ARGS          (M_fastUdpLib | 3)
#define S_fastUdpLib_MEM_ERROR             (M_fastUdpLib | 4)
#define S_fastUdpLib_INVALID_OPTION        (M_fastUdpLib | 5)
#define S_fastUdpLib_NOT_INITIALIZED       (M_fastUdpLib | 6)


/* Fast UDP options */

#define RCV_BUFSIZE   0x0001      /* size of receive buffer in bytes */
#define RCV_LOWAT     0x0002      /* receive low-water mark */

/* Fast UDP cookie */

typedef struct fastUdp
    {
    struct fastUdp *next;
    SEM_ID   recvSem;               /* synchronization sem for receive queue */
    M_BLK_ID recvData;              /* head of receive queue */
    u_int    recvDataLen;           /* amount of data in receive queue */
    M_BLK_ID lastPkt;               /* last packet in receive queue */
    struct   udpOptions *options;   /* Fast UDP options */
    u_short  port;                  /* Fast UDP port */
    u_short  error;                 /* error number */
    struct sockaddr_in errorAddr;   /* address which sent error notification */
    int      applTaskId;
    } *FASTUDP_ID;

struct udpOptions
    {
    u_int rcvBuf;     /* limit on receive buffer */
    u_int rcvLowat;   /* receive buffer low-water mark */
    };

/* Entry in port configuration table */

typedef struct portTbl
    {
    u_short startPort;
    u_short endPort;
    } PORT_TBL;

typedef struct
    {
    NODE node;
    u_short startPort;
    u_short endPort;
    } PORTENTRY;

/* fastUdp configuration parameters */
    
typedef struct fastUdp_config_params {
    CFG_DATA_HDR         cfgh;

    u_int	         cfg_priv_fastUdpDebug;
    PORT_TBL	        *cfg_pPortCfgTbl;
    u_int                cfg_portCfgTblEntries;
    } FASTUDP_CONFIG_PARAMS;

#ifdef __cplusplus
extern "C" {
#endif

/* forward declarations */

STATUS fastUdpLibInit (PORT_TBL *tbl, int numEntries);
STATUS fastUdpAdd (u_short startPort, u_short endPort);
STATUS fastUdpDel (u_short startPort, u_short endPort);
FASTUDP_ID fastUdpBind (u_short port);
STATUS fastUdpUnbind (FASTUDP_ID pCookie);
STATUS fastUdpSend (u_short port, ZBUF_ID data, int dataLen,
		    struct sockaddr_in * dstAddr);
ZBUF_ID fastUdpRecv (FASTUDP_ID pCookie, int *dataLen,
		     struct sockaddr_in *srcAddr);
STATUS  fastUdpOptionSet (FASTUDP_ID pCookie, u_short option, void *val);
void    fastUdpDebugSet (u_int debugStatus);

#ifdef __cplusplus
}
#endif

#endif __INCfastudpLibh
