/* Copyright 2005 Wind River Systems, Inc. */

/*-
 * Copyright (c) 1982, 1986, 1989, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	From: @(#)if.h	8.1 (Berkeley) 6/10/93
 * $FreeBSD: src/sys/net/if_clone.h,v 1.1.2.1 2005/01/31 23:26:22 imp Exp $
 */

/*
modification history
--------------------
01b,21sep05,kch  Partially merged FreeBSD 5.x if_clone.h,v 1.1.2.1 to support
                 the free-form interface naming style (SPR #112724).
01a,28jul05,kch  Ported from FreeBSD.
*/

#ifndef	_NET_IF_CLONE_H_
#define	_NET_IF_CLONE_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _WRS_KERNEL

#define IF_CLONE_INITIALIZER(name, maxunit, attach, match, create, destroy)	\
	{ { 0 }, name, sizeof(name) - 1, maxunit, NULL, 0, attach, \
	  match, create, destroy }


/*
 * Structure describing a `cloning' interface.
 */
struct if_clone {
	LIST_ENTRY(if_clone) ifc_list;	/* on list of cloners */
	char *ifc_name;		/* name of device, e.g. `gif' */
	size_t ifc_namelen;		/* length of name */
	int ifc_maxunit;		/* (c) Maximum unit number */
	unsigned char *ifc_units;	/* (i) Bitmap to handle units. */
	int ifc_bmlen;			/* (c) Bitmap length. */

	/* (c) Driver specific cloning functions. */
	void (*ifc_attach)(struct if_clone *);
	int (*ifc_match)(struct if_clone *, const char *);
	int	(*ifc_create)(struct if_clone *, char *, int);
	int (*ifc_destroy)(struct if_clone *, struct ifnet *);
};

void if_clone_init(void);
int	if_clone_attach(struct if_clone *);
void if_clone_detach(struct if_clone *);

int	if_clone_create(char *, int);
int	if_clone_destroy(const char *);
int if_clone_list(struct if_clonereq *);

int	ifc_name2unit(const char *name, int *unit);
int ifc_alloc_unit(struct if_clone *ifc, int *unit);
void ifc_free_unit(struct if_clone *ifc, int unit);

#endif  /* _WRS_KERNEL */

#ifdef __cplusplus
}
#endif

#endif /* _NET_IF_CLONE_H_ */
