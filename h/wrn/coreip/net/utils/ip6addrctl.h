/* ip6addrtcl.h  - address selection policy for IPv6 */

/* Copyright 2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,09jun05,rp   written.
*/

#ifndef  __INCIP6ADDRTCLH
#define  __INCIP6ADDRTCLH

#ifdef __cplusplus
extern "C" {
#endif

#include <netVersion.h>

/* externs */

#if defined(__STDC__) || defined(__cplusplus)

extern STATUS ip6addrctl (char *);
extern STATUS ip6addrctlInit ();

#else	/* __STDC__ */

extern STATUS ip6addrctl ();
extern STATUS ip6addrctlInit ();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCIP6ADDRTCLH */
