/* ndp  - neighbor discovery cache print for IPv6 */

/* Copyright 1992 - 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01f,15mar04,rp   merged from orion
01e,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01d,04nov03,rlm  Ran batch header path update for header re-org.
01c,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01b,20aug02,hgo  added ndpInstInit() and ndpDestructor()
01c,06aug03,nee  Accordion Base6 merge from ACCORDION_BASE6_MERGE_BASELINE
                 label
01b,10jun03,vvv  include netVersion.h
01a,09apr02,ppp  written.
*/

#ifndef  __INCndph
#define  __INCndph

#ifdef __cplusplus
extern "C" {
#endif

#include <netVersion.h>

/* externs */

#ifdef VIRTUAL_STACK
extern VS_REG_ID ndpRegistrationNum;
#endif /* VIRTUAL_STACK */

#if defined(__STDC__) || defined(__cplusplus)

extern STATUS   ndpInstInit (void *);
#ifdef VIRTUAL_STACK
extern STATUS   ndpDestructor (VSNUM);
#endif
extern int ndp (char *options);

#else	/* __STDC__ */

extern STATUS   ndpInstInit ();
#ifdef VIRTUAL_STACK
extern STATUS   ndpDestructor ();
#endif
extern int ndp ();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCndph */
