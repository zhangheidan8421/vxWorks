/* if_subr.h - header for common routines for network interface drivers */

/* Copyright 1984 - 2003, 2005 Wind River Systems, Inc. */

/*
modification history
--------------------
02l,16may05,dlk  Deprecate unused netTypeAdd/Delete/Init, and unimplemented
		 ether_addmulti()/ether_delmulti().
02k,20nov03,niq  Remove copyright_wrs.h file inclusion
02j,05nov03,cdw  Removal of unnecessary _KERNEL guards.
02i,04nov03,rlm  Ran batch header path update for header re-org.
02h,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
02g,23oct03,rlm  updated #includes for header re-org
02f,09may03,vvv  included lstLib.h
02e,01apr02,ham  deleted do_protocol_with_type.
02d,28mar02,ppp  modifying it to be backward compatible with A.E. 1.1
02c,12sep01,ann  removing the definition of ETHERMTU from here
02b,12sep01,ann  adding some #defines
02a,15aug01,ann  ported to clarinet from AE1.1 version 01n
*/

#ifndef __INCif_subrh
#define __INCif_subrh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */
#include <sys/socket.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <net/mbuf.h>
#include <lstLib.h>

typedef struct net_type /* DEPRECATED */
    {
    NODE     	node;
    FUNCPTR     inputRtn;
    int     	etherType;
    } NET_TYPE;

/* function declarations */

extern STATUS netTypeAdd (int etherType, FUNCPTR inputRtn)
    _WRS_DEPRECATED("has no useful effect");
extern STATUS netTypeDelete (int etherType)
    _WRS_DEPRECATED("has no useful effect");
extern void netTypeInit ()
    _WRS_DEPRECATED("has no useful effect");
extern int	ether_addmulti (struct ifreq *, struct arpcom *)
    _WRS_DEPRECATED("unimplemented");
extern int	ether_delmulti (struct ifreq *, struct arpcom *)
    _WRS_DEPRECATED("unimplemented");

#ifdef __cplusplus
}
#endif

#endif /* __INCif_subrh */
