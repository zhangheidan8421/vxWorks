/* if.h - network interface header file */

/* Copyright 2001 - 2005 Wind River Systems, Inc. */

/*
 * Copyright (c) 1982, 1986, 1989, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)if.h	8.1 (Berkeley) 6/10/93
 * $FreeBSD: src/sys/net/if.h,v 1.58.2.9 2002/08/30 14:23:38 sobomax Exp $
 */

/*
modification history
--------------------
02i,30aug05,kch  Added ifdeladdrreq structure (moved from if.c) (SPR #112068).
02h,17aug05,mcm  Including sys/time.h for struct timeval
02g,26jul05,kch  Move if_clone structure definition to if_clone.h.
02f,30jun05,wap  Move ifi_link_state for binary compatibility
02e,24jun05,wap  Add link state support
02d,18feb05,wap  Allow sysctl init routines to be scaled out
02c,07feb05,vvv  _KERNEL cleanup
02b,14jan05,vvv  osdep.h cleanup
02a,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01z,19jul04,niq  Move sysctl initialization from configelette to the component
                 init routine
01y,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/2)
01x,09feb04,niq  Include if_var.h only in kernel context
01w,14dec03,ijm  added IFF_PROXY_ALL for spr#85896,for interface based proxy
01v,20nov03,niq  osdep.h cleanup
01u,04nov03,rlm  Ran batch header path update for header re-org.
01t,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01s,24oct03,rlm  updated #includes for header re-org
01r,15sep03,vvv  updated path for new headers
01q,18aug03,nee  Merging from ACCORDION_BASE6_MERGE_BASELINE
01p,15may03,spm  Tornado 2.2 FCS merge (SPR #69683; ver 01m,10oct01,rae:
                 TOR2_2-FCS-COPY label, tor2 branch, /wind/river VOB)
01o,09may03,vvv  included socket.h
01n,08may03,vvv  fixed value for IFF_PPROMISC (SPR #88237)
01m,01may03,spm  Tornado 2.2 CP1 merge (from ver 01n,17jan03,vvv:
                 TOR2_2-CP1-F label, tor2_2-patch branch, /wind/river VOB)
01l,01feb03,pas  merge from FreeBSD 4.7 - clone, announce interfaces
01k,17sep02,nee  added admin control macros
01j,09sep02,hsh  add c++ protection
01i,24aug02,nee  extended ifnet for admin control;defined admin states for
                 boot/up/down
01h,12jul02,hsh  fix head file for ppc
01g,03feb02,ham  modified for tornado build.
01f,16dec01,ann  temporarily moving the if_name* declerations out the _KERNEL
                 condition
01e,24sep01,ppp  adding the prototype for if_name and the macro for IFAREF
01d,12sep01,ann  adding some IFF flags
01c,11sep01,ann  uncomment ifi_lastchange
01b,09sep01,ham  changed header file's path, made ifi_lastchange obsolete.
01a,16jul01,ham  created from FreeBSD4.3-RELEASE(July-17-01).
*/

#ifndef __INCifh
#define	__INCifh

#ifdef _WRS_KERNEL
#include <sys/queue.h>
#include <sys/times.h>
#else
#include <sys/time.h>
#endif

#include <sys/socket.h>
#include <cfgDefs.h>



#ifdef __cplusplus
 extern "C" {
#endif

typedef struct if_config_params {
    CFG_DATA_HDR cfgh;
    int          if_index_limit;
    int          ifq_max_len;
    FUNCPTR	 cfg_privInitSysctl;
} IF_CONFIG_PARAMS;

struct ifnet;

/*
 * Length of interface external name, including terminating '\0'.
 * Note: this is the same size as a generic device's external name.
 */
#define		IFNAMSIZ	16
#define		IF_NAMESIZE	IFNAMSIZ

/*
 * Structure used to query names of interface cloners.
 */

struct if_clonereq {
	int	ifcr_total;		/* total cloners (out) */
	int	ifcr_count;		/* room for this many in user buffer */
	char	*ifcr_buffer;		/* buffer for cloner names */
};

/*
 * Structure describing information about an interface
 * which may be of interest to management entities.
 */
struct if_data {
	/* generic interface information */
	u_char	ifi_type;		/* ethernet, tokenring, etc */
	u_char	ifi_physical;		/* e.g., AUI, Thinnet, 10base-T, etc */
	u_char	ifi_addrlen;		/* media address length */
	u_char	ifi_hdrlen;		/* media header length */
	u_char	ifi_recvquota;		/* polling quota for receive intrs */
	u_char	ifi_xmitquota;		/* polling quota for xmit intrs */
	u_char	ifi_link_state;		/* current link state */
	u_long	ifi_mtu;		/* maximum transmission unit */
	u_long	ifi_metric;		/* routing metric (external only) */
	u_long	ifi_baudrate;		/* linespeed */
	/* volatile statistics */
	u_long	ifi_ipackets;		/* packets received on interface */
	u_long	ifi_ierrors;		/* input errors on interface */
	u_long	ifi_opackets;		/* packets sent on interface */
	u_long	ifi_oerrors;		/* output errors on interface */
	u_long	ifi_collisions;		/* collisions on csma interfaces */
	u_long	ifi_ibytes;		/* total number of octets received */
	u_long	ifi_obytes;		/* total number of octets sent */
	u_long	ifi_imcasts;		/* packets received via multicast */
	u_long	ifi_omcasts;		/* packets sent via multicast */
	u_long	ifi_iqdrops;		/* dropped on input, this interface */
	u_long	ifi_noproto;		/* destined for unsupported protocol */
	u_long	ifi_hwassist;		/* HW offload capabilities */
	u_long	ifi_unused;		/* XXX was ifi_xmittiming */
	struct	timeval ifi_lastchange;	/* time of last administrative change */
};

#define	IFF_UP		0x1		/* interface link is up */
#define	IFF_BROADCAST	0x2		/* broadcast address valid */
#define	IFF_DEBUG	0x4		/* turn on debugging */
#define	IFF_LOOPBACK	0x8		/* is a loopback net */
#define	IFF_POINTOPOINT	0x10		/* interface is point-to-point link */
#define	IFF_SMART	0x20		/* interface manages own routes */
#define	IFF_RUNNING	0x40		/* resources allocated */
#define	IFF_NOARP	0x80		/* no address resolution protocol */
#define	IFF_PROMISC	0x100		/* receive all packets */
#define	IFF_ALLMULTI	0x200		/* receive all multicast packets */
#define	IFF_OACTIVE	0x400		/* transmission in progress */
#define	IFF_SIMPLEX	0x800		/* can't hear own transmissions */
#define	IFF_LINK0	0x1000		/* forwarding disabled */
#define	IFF_LINK1	0x2000		/* per link layer defined bit */
#define	IFF_LINK2	0x4000		/* per link layer defined bit */
#define	IFF_ALTPHYS	IFF_LINK2	/* use alternate physical connection */
#define	IFF_MULTICAST	0x8000		/* supports multicast */
#define IFF_NOTRAILERS  0x20000         /* avoid use of trailers */
#define IFF_INET_UP	0x40000	        /* interface is up for ipv4 */
#define IFF_INET6_UP	0x80000	        /* interface is up for ipv6 */
#define IFF_UNNUMBERED  IFF_POINTOPOINT /* supports unnumbered interfaces */

#define IFF_FPV4_ENABLE  0x100000        /* supports V4 fast forwarding */ 
#define IFF_FPV6_ENABLE  0x200000        /* supports V6 fast forwarding */
#define IFF_PROXY_ALL    0x400000        /* enable Cisco style proxying */
 
#define IFF_DONT_FORWARD  IFF_LINK0     /* forwarding disabled */

/*
 * The following flag(s) ought to go in if_flags, but we cannot change
 * struct ifnet because of binary compatibility, so we store them in
 * if_ipending, which is not used so far.
 * If possible, make sure the value is not conflicting with other
 * IFF flags, so we have an easier time when we want to merge them.
 */
#define	IFF_POLLING	0x10000		/* Interface is in polling mode. */
#define	IFF_PPROMISC	0x800000	/* user-requested promisc mode */

/* flags set internally only: */
#define	IFF_CANTCHANGE \
	(IFF_BROADCAST|IFF_POINTOPOINT|IFF_RUNNING|IFF_OACTIVE|\
	    IFF_SIMPLEX|IFF_MULTICAST|IFF_ALLMULTI|IFF_SMART|IFF_POLLING)

#define IFF_UP_FLAGS (IFF_UP | IFF_INET_UP | IFF_INET6_UP)

/*
 * Values for if_link_state.
 */
#define LINK_STATE_UNKNOWN	0	/* link invalid/unknown */
#define LINK_STATE_DOWN		1	/* link is down */
#define LINK_STATE_UP		2	/* link is up */

/* Capabilities that interfaces can advertise. */
#define IFCAP_RXCSUM		0x0001  /* can offload checksum on RX */
#define IFCAP_TXCSUM		0x0002  /* can offload checksum on TX */
#define IFCAP_NETCONS		0x0004  /* can be a network console */

#define IFCAP_HWCSUM		(IFCAP_RXCSUM | IFCAP_TXCSUM)

#define	IFQ_MAXLEN	50
#define	IFNET_SLOWHZ	1		/* granularity is 1 second */


/* admin flags */
#define ADMIN_BOOT 0 /* for future use */
#define ADMIN_UP 1 /* for future use */
#define ADMIN_DOWN 2 /* for future use */

/* 
 * 
 * If admin status(RFC 2465) is ADMIN_BOOT/ADMIN_UP set the 
 * corresponding(v4/v6) interface flag.
 * Check for a valid ifa. For ipv6 should have 
 * atleast one (valid) ifa for which DAD has succeeded
 */

#define IFF_INET_UPDATE(ifp) \
if (ifp->admin_inet_status == ADMIN_UP || ifp->admin_inet_status == ADMIN_BOOT)\
 	   		{if (in_ifexists_ifa(ifp))							 \
			(ifp)->if_flags |= IFF_INET_UP;} 	  							 \
else 												 \
	log(LOG_WARNING, "Admin status is down for IPv4 traffic on interface %s%d\n",ifp->if_name,ifp->if_unit);	\

#define IFF_INET6_UPDATE(ifp) \
if (ifp->admin_inet6_status == ADMIN_UP || ifp->admin_inet6_status == ADMIN_BOOT)\
 	   		{if (in6_ifexists_ifa(ifp))							 \
			(ifp)->if_flags |= IFF_INET6_UP;} 	  							 \
else 	\
	log(LOG_WARNING, "Admin status is down for IPv6 traffic on interface %s%d\n",ifp->if_name,ifp->if_unit);	\

/*
 * Message format for use in obtaining information about interfaces
 * from getkerninfo and the routing socket
 */
struct if_msghdr {
	u_short	ifm_msglen;	/* to skip over non-understood messages */
	u_char	ifm_version;	/* future binary compatability */
	u_char	ifm_type;	/* message type */
	int	ifm_addrs;	/* like rtm_addrs */
	int	ifm_flags;	/* value of if_flags */
	u_short	ifm_index;	/* index for associated ifp */
	struct	if_data ifm_data;/* statistics and other data about if */
};

/*
 * Message format for use in obtaining information about interface addresses
 * from getkerninfo and the routing socket
 */
struct ifa_msghdr {
	u_short	ifam_msglen;	/* to skip over non-understood messages */
	u_char	ifam_version;	/* future binary compatability */
	u_char	ifam_type;	/* message type */
	int	ifam_addrs;	/* like rtm_addrs */
	int	ifam_flags;	/* value of ifa_flags */
	u_short	ifam_index;	/* index for associated ifp */
	int	ifam_metric;	/* value of ifa_metric */
};

/*
 * Message format for use in obtaining information about multicast addresses
 * from the routing socket
 */
struct ifma_msghdr {
	u_short	ifmam_msglen;	/* to skip over non-understood messages */
	u_char	ifmam_version;	/* future binary compatability */
	u_char	ifmam_type;	/* message type */
	int	ifmam_addrs;	/* like rtm_addrs */
	int	ifmam_flags;	/* value of ifa_flags */
	u_short	ifmam_index;	/* index for associated ifp */
};

/*
 * Message format announcing the arrival or departure of a network interface.
 */
struct if_announcemsghdr {
	u_short	ifan_msglen;	/* to skip over non-understood messages */
	u_char	ifan_version;	/* future binary compatibility */
	u_char	ifan_type;	/* message type */
	u_short	ifan_index;	/* index for associated ifp */
	char	ifan_name[IFNAMSIZ]; /* if name, e.g. "en0" */
	u_short	ifan_what;	/* what type of announcement */
};

#define	IFAN_ARRIVAL	0	/* interface arrival */
#define	IFAN_DEPARTURE	1	/* interface departure */

/*
 * Interface request structure used for socket
 * ioctl's.  All interface ioctl's must have parameter
 * definitions which begin with ifr_name.  The
 * remainder may be interface specific.
 */
struct sockaddr;

struct	ifreq {
	char	ifr_name[IFNAMSIZ];		/* if name, e.g. "en0" */
	union {
		struct	sockaddr ifru_addr;
		struct	sockaddr ifru_dstaddr;
		struct	sockaddr ifru_broadaddr;
		long	ifru_flags[2];
		int	ifru_metric;
		int	ifru_mtu;
		int	ifru_phys;
		int	ifru_media;
		caddr_t	ifru_data;
		int	ifru_cap[2];
	} ifr_ifru;
#define	ifr_addr	ifr_ifru.ifru_addr	/* address */
#define	ifr_dstaddr	ifr_ifru.ifru_dstaddr	/* other end of p-to-p link */
#define	ifr_broadaddr	ifr_ifru.ifru_broadaddr	/* broadcast address */
#define	ifr_flags	ifr_ifru.ifru_flags[0]	/* flags (low 16 bits) */
#define	ifr_flagshigh	ifr_ifru.ifru_flags[1]	/* flags (high 16 bits) */
#define	ifr_metric	ifr_ifru.ifru_metric	/* metric */
#define	ifr_mtu		ifr_ifru.ifru_mtu	/* mtu */
#define ifr_phys	ifr_ifru.ifru_phys	/* physical wire */
#define ifr_media	ifr_ifru.ifru_media	/* physical media */
#define	ifr_data	ifr_ifru.ifru_data	/* for use by interface */
#define	ifr_reqcap	ifr_ifru.ifru_cap[0]	/* requested capabilities */
#define	ifr_curcap	ifr_ifru.ifru_cap[1]	/* current capabilities */
};

#define	_SIZEOF_ADDR_IFREQ(ifr) \
	((ifr).ifr_addr.sa_len > sizeof(struct sockaddr) ? \
	 (sizeof(struct ifreq) - sizeof(struct sockaddr) + \
	  (ifr).ifr_addr.sa_len) : sizeof(struct ifreq))

struct ifaliasreq {
	char	ifra_name[IFNAMSIZ];		/* if name, e.g. "en0" */
	struct	sockaddr ifra_addr;
	struct	sockaddr ifra_broadaddr;
	struct	sockaddr ifra_mask;
};

struct ifmediareq {
	char	ifm_name[IFNAMSIZ];	/* if name, e.g. "en0" */
	int	ifm_current;		/* current media options */
	int	ifm_mask;		/* don't care mask */
	int	ifm_status;		/* media status */
	int	ifm_active;		/* active options */
	int	ifm_count;		/* # entries in ifm_ulist array */
	int	*ifm_ulist;		/* media words */
};

/*
 * This structure is used to pass the data to in_control() for an address
 * delete request. The ifaliasreq structure has the address to be deleted
 * and 'detachFlag' indicates whether the interface is being detached.
 */
struct ifdeladdrreq {
	struct ifaliasreq ifr;
	BOOL   detachFlag;
};

/* 
 * Structure used to retrieve aux status data from interfaces.
 * Kernel suppliers to this interface should respect the formatting
 * needed by ifconfig(8): each line starts with a TAB and ends with
 * a newline.  The canonical example to copy and paste is in if_tun.c.
 */

#define	IFSTATMAX	800		/* 10 lines of text */
struct ifstat {
	char	ifs_name[IFNAMSIZ];	/* if name, e.g. "en0" */
	char	ascii[IFSTATMAX + 1];
};

/*
 * Structure used in SIOCGIFCONF request.
 * Used to retrieve interface configuration
 * for machine (useful for programs which
 * must know all networks accessible).
 */
struct	ifconf {
	int	ifc_len;		/* size of associated buffer */
	union {
		caddr_t	ifcu_buf;
		struct	ifreq *ifcu_req;
	} ifc_ifcu;
#define	ifc_buf	ifc_ifcu.ifcu_buf	/* buffer address */
#define	ifc_req	ifc_ifcu.ifcu_req	/* array of structures returned */
};

/* 
 * Structure used in SIOCGMTU request.
 * Used to retrieve the MTU for point-to-multipoint devices.
 */

typedef struct mtuQuery_t 
	{
	UCHAR family;
	UINT32 dstIpAddr;
	UINT32 mtu;
	} MTU_QUERY;

/*
 * Structure for SIOC[AGD]LIFADDR
 */
struct if_laddrreq {
	char	iflr_name[IFNAMSIZ];
	u_int	flags;
#define	IFLR_PREFIX	0x8000  /* in: prefix given  out: kernel fills id */
	u_int	prefixlen;         /* in/out */
	struct	sockaddr_storage addr;   /* in/out */
	struct	sockaddr_storage dstaddr; /* out */
};

struct if_nameindex {
	u_int	if_index;	/* 1, 2, ... */
	char	*if_name;	/* null terminated name: "le0", ... */
};

u_int	 if_nametoindex __P((const char *));
char	*if_indextoname __P((u_int, char *));
struct	 if_nameindex *if_nameindex __P((void));
void	 if_freenameindex __P((struct if_nameindex *));


#ifdef _WRS_KERNEL
struct proc;
int	prison_if __P((struct proc *p, struct sockaddr *sa));

#include <net/if_var.h>

extern const char *if_name __P((struct ifnet *));
#define IFAREF(ifa)     do { ++(ifa)->ifa_refcnt; } while (0)
#endif /* _WRS_KERNEL */

#ifdef __cplusplus
}
#endif
    
#endif /* !__INCifh */
