/* etherLib.h - ethernet hook routines header */

/* Copyright 1984-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01d,04nov03,rlm  Ran batch header path update for header re-org.
01c,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01b,10jun03,vvv  include netVersion.h
01a,28sep02,ann  provided only for backward compatibility
*/

#ifndef __INCetherLibh
#define __INCetherLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <netVersion.h>

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
#pragma align 1                 /* tell gcc960 not to optimize alignments */
#endif  /* CPU_FAMILY==I960 */

/* defints */
typedef struct enet_hdr
    {
    char dst [6];
    char src [6];
    USHORT type;
    } ENET_HDR;

#define ENET_HDR_SIZ        sizeof(ENET_HDR)
#define ENET_HDR_REAL_SIZ   14

#ifdef __cplusplus
}
#endif

#endif /* __INCetherLibh */
