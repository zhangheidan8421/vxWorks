/* mbufSockLib.h - mbuf socket interface library header */

/* Copyright 1984-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,04nov03,rlm  Ran batch header path update for header re-org.
01b,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01a,19nov02,nee  Ported to Accordion from ""mbufSockLib.h@@/main/1 from view tor3_x/tor3_x.synth".
*/

#ifndef __INCmbufSockLibh
#define __INCmbufSockLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include <mbufLib.h>

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern void *	_mbufSockLibInit (void);
extern int	_mbufSockSend (int s, MBUF_ID mbufId, int mbufLen, int flags);
extern int	_mbufSockSendto (int s, MBUF_ID mbufId, int mbufLen, int flags,
                    struct sockaddr *to, int tolen);
extern int	_mbufSockBufSend (int s, char *buf, int bufLen,
                    VOIDFUNCPTR freeRtn, int freeArg, int flags);
extern int	_mbufSockBufSendto (int s, char *buf, int bufLen,
                    VOIDFUNCPTR freeRtn, int freeArg, int flags,
                    struct sockaddr *to, int tolen);
extern MBUF_ID	_mbufSockRecv (int s, int flags, int *pLen);
extern MBUF_ID	_mbufSockRecvfrom (int s, int flags, int *pLen,
                    struct sockaddr *from, int *pFromLen);
 
#else	/* __STDC__ */

extern void *	_mbufSockLibInit ();
extern int	_mbufSockSend ();
extern int	_mbufSockSendto ();
extern int	_mbufSockBufSend ();
extern int	_mbufSockBufSendto ();
extern MBUF_ID	_mbufSockRecv ();
extern MBUF_ID	_mbufSockRecvfrom ();
 
#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCmbufSockLibh */
