/* muxLib.h - definitions for the MUX library */

/* Copyright 1984 - 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
03p,25aug05,dlk  Add section tags.
03o,28jul05,dlk  Added muxErrorSkip() declaration.
03n,20feb05,dlk  Added MUX_FUNCS, muxEndTake(), muxEndGive(), MUX_PASSTHRU.
03m,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
03l,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
03k,24nov03,wap  Merge in changes from Snowflake
03j,20nov03,niq  Remove copyright_wrs.h file inclusion
03i,04nov03,rlm  Ran batch header path update for header re-org.
03h,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
03g,27oct03,cdw  update include statements post header re-org.
03f,08sep03,vvv  merged from ACCORDION_BASE6_MERGE_BASELINE
03e,06feb03,vvv  merged from tor2_2-patch.alameda, ver02c (SPR #83258)
03d,03feb02,ham  renamed dummyend.h to end.h.
03c,20feb02,ham  changed for tornado build.
03b,09jan02,ann  adding the field for the output function in MUX_ADDR_REC
03a,15may01,ann  major changes as per MUX design doc for clarinet.
                 added some prototypes. code inherited from AE ver 02c.
*/
 
/*
DESCRIPTION
This file includes function prototypes for the MUX.

INCLUDE FILES:
*/

#ifndef __INCmuxLibh
#define __INCmuxLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */
#include <net/if_types.h>

#ifdef _WRS_KERNEL
#include <end.h>
#else
#include <endCommon.h>
#endif /* _WRS_KERNEL */

/* defints */

/* MUX_MAX_IFTYPE associates with IFT_MAX_TYPE as default. If the user wants
 * to add a resource function, verify it's smaller than MUX_MAX_IFTYPE.
 */

#define MUX_MAX_IFTYPE M2_ifType_pmp

/* Error defines. */
#define S_muxLib_LOAD_FAILED                  (M_muxLib | 1)
#define S_muxLib_NO_DEVICE                    (M_muxLib | 2)
#define S_muxLib_INVALID_ARGS                 (M_muxLib | 3)
#define S_muxLib_ALLOC_FAILED                 (M_muxLib | 4)
#define S_muxLib_ALREADY_BOUND                (M_muxLib | 5)
#define S_muxLib_UNLOAD_FAILED                (M_muxLib | 6)
#define S_muxLib_NOT_A_TK_DEVICE              (M_muxLib | 7)
#define S_muxLib_NO_TK_DEVICE                 (M_muxLib | 8)
#define S_muxLib_END_BIND_FAILED              (M_muxLib | 9)


/* These are above all SAPs but still in the ethernet size range */
/* so won't interfere with RFC 1700 */
#define MUX_PROTO_PROMISC	0x100	
#define MUX_PROTO_SNARF         0x101	
#define MUX_PROTO_OUTPUT        0x102

/* 
 * The following defines are used for the various if-protocol functions 
 * that are maintained by the MUX function table.
 */

#define ADDR_RES_FUNC           1
#define IF_OUTPUT_FUNC          2
#define MULTI_ADDR_RES_FUNC     4

/* typedefs */

/* structure passed for SIOCMUXPASSTRHU ioctl */

typedef struct _MUX_PASSTHRU
    {
    char name[END_NAME_MAX];	/* device name, without unit number */
    int unit;			/* unit number. */
    u_int cmd;			/* Tunnelled ioctl command. Must properly
				   encode size and IOC_IN/IOC_OUT/IOC_VOID,
				   and IOC_USER must be set if RTP access is
				   allowed. */
    void * pData;		/* Tunnelled ioctl data argument */
    } MUX_PASSTHRU;

#ifdef _WRS_KERNEL

/*
 * These data structures define the 2D list of address resolution
 * functions.  This list is ordered by ifType and the protocol.
 */
typedef struct mux_addr_rec
    {
    NODE node;
    long protocol;
    int  setFuncs;
    FUNCPTR addrResFunc;
    FUNCPTR ifOutputFunc;
    FUNCPTR multiAddrResFunc;
    } MUX_ADDR_REC;

/* function pointers for scalability */

typedef struct _MUX_FUNCS
    {
    void * (*endTake) (const char * pName, int unit);
    void   (*endGive) (void * pCookie);
    int    (*ioctl) (void* pCookie, int cmd, caddr_t data);
    /* ADD MORE HERE AS NEEDED */
    } MUX_FUNCS;

/* globals */

IMPORT MUX_FUNCS _func_mux;

/* locals */

/* forward declarations */

_WRS_INITTEXT
IMPORT STATUS muxLibInit ();
IMPORT void * muxDevLoad(int unit, END_OBJ* (*endLoad) (char *, void*),
                           char *initString,
                           BOOL loaning, void*pBSP);
IMPORT STATUS muxDevStart(void* pCookie);
IMPORT STATUS muxDevStop(void* pCookie);
IMPORT void muxShow(char* pDevName, int unit);
IMPORT STATUS muxDevStopAll (int timeout);
IMPORT STATUS muxDevUnload(char* pName, int unit);
IMPORT void * muxBind (char * pName, int unit,
                  BOOL (*stackRcvRtn) (void*, long,M_BLK_ID, LL_HDR_INFO *,
                                       void*),
                  STATUS (*stackShutdownRtn) (void*, void*),
                  STATUS (*stackTxRestartRtn) (void*, void*),
                  void (*stackErrorRtn) (END_OBJ*, END_ERR*, void*),
                  long type, char* pProtoName, void* pSpare);
IMPORT STATUS muxUnbind(void* pCookie, long type, FUNCPTR stackShutdownRtn);
_WRS_FASTTEXT
IMPORT STATUS muxSend(void* pCookie, M_BLK_ID pNBuff);
_WRS_FASTTEXT
IMPORT STATUS muxReceive(void* pCookie, M_BLK_ID pNBuff);
IMPORT STATUS muxPollSend(void* pCookie, M_BLK_ID pNBuff);
IMPORT STATUS muxPollReceive(void* pCookie, M_BLK_ID pNBuff);
IMPORT STATUS muxIoctl(void* pCookie, int cmd, caddr_t data);
IMPORT STATUS muxMCastAddrAdd(void* pCookie, char* pAddress);
IMPORT STATUS muxMCastAddrDel(void* pCookie, char* pAddress);
IMPORT STATUS muxMCastAddrGet(void* pCookie, MULTI_TABLE* pTable);
IMPORT char* muxLoanGet(void* pCookie);
IMPORT UINT8* muxRefCntGet(void* pCookie);
IMPORT void muxLoanRet(void* pCookie, char* pData, UINT8* pRef);
IMPORT M_BLK_ID muxLinkHeaderCreate(void* pCookie, M_BLK_ID pPacket,
                               M_BLK_ID pSrcAddr, M_BLK_ID pDstAddr,
                               BOOL bcastFlag);
IMPORT M_BLK_ID muxAddressForm(void* pCookie, M_BLK_ID pMblk,
                               M_BLK_ID pSrcAddr, M_BLK_ID pDstAddr);
IMPORT STATUS muxPacketDataGet(void* pCookie, M_BLK_ID pMblk, LL_HDR_INFO *);
IMPORT STATUS muxPacketAddrGet (void* pCookie, M_BLK_ID pMblk,
                                M_BLK_ID pSrc,
                                M_BLK_ID pDst,
                                M_BLK_ID pESrc,
                                M_BLK_ID pEDst);
_WRS_FASTTEXT
IMPORT void muxTxRestart(void* pCookie);

IMPORT void muxError ( void* pCookie, END_ERR* pError );
IMPORT void muxErrorSkip (void * pCookie, END_ERR * pError,
			  void * pSkipProtoCookie);


IMPORT END_OBJ* endFindByName(char* pName, int unit);
IMPORT END_OBJ* endFirstUnitFind (char* pDevName);
IMPORT STATUS muxAddrResFuncDel ( long ifType, long protocol );
IMPORT FUNCPTR muxAddrResFuncGet ( long ifType, long protocol );
IMPORT STATUS muxAddrResFuncAdd ( long ifType, long protocol,
                                  FUNCPTR addrResFunc);
IMPORT STATUS muxIfFuncDel ( long ifType, long protocol, int funcType );
IMPORT FUNCPTR muxIfFuncGet ( long ifType, long protocol, int funcType );
IMPORT STATUS muxIfFuncAdd ( long ifType, long protocol, int funcType, 
                                                         FUNCPTR ifFunc);

IMPORT BOOL muxDevExists (char* pName, int unit);
IMPORT STATUS muxIterateByName (char * pName, FUNCPTR pCallbackRtn,
				void * pCallbackArg);
IMPORT void * muxProtoPrivDataGet (END_OBJ * pEnd, int proto);

IMPORT void * muxEndTake (const char * pName, int unit);
IMPORT void muxEndGive (void * pCookie);

#endif /* _WRS_KERNEL */

#ifdef __cplusplus
}
#endif

#endif /* __INCmuxLibh */
