/* icmpLib.h -- VxWorks ICMP Library header file */

/* Copyright 1990-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01j,18feb05,wap  Allow sysctl init routines to be scaled out
01i,07feb05,vvv  _KERNEL cleanup
01h,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01g,04nov03,rlm  Ran batch header path update for header re-org.
01f,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01e,15sep03,vvv  updated path for new headers
01d,07aug03,vvv  merged from ACCORDION_BASE6_MERGE_BASELINE
01c,09may03,vvv  included if.h and ip.h
01b,12feb02,pas  merged from automerge.vobadmin.tor3_x.ipv6.nashua
01a,14dec01,ppp  ported from A.E. 1.1.
*/

#ifndef __INCicmpLibh
#define __INCicmpLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include <vwModNum.h>
#include <cfgDefs.h>
#include <net/if.h>
#include <netinet/ip.h>

/* errors */

#define S_icmpLib_TIMEOUT		(M_icmpLib | 1)
#define S_icmpLib_NO_BROADCAST		(M_icmpLib | 2)
#define S_icmpLib_INVALID_INTERFACE	(M_icmpLib | 3)
#define S_icmpLib_INVALID_ARGUMENT	(M_icmpLib | 4)

/* typedefs */

typedef struct icmp_config_params
    {
    CFG_DATA_HDR cfgh;
    int cfg_priv_icmpmaskrepl;
    int cfg_priv_drop_redirect;
    int cfg_priv_log_redirect;
    int cfg_priv_icmplim;
    int cfg_priv_icmpbmcastecho;
    int cfg_icmpErrorLen;
    FUNCPTR cfg_privInitSysctl;
    } ICMP_CONFIG_PARAMS;

/* function prototypes */

#if defined(__STDC__) || defined(__cplusplus)

STATUS    icmpMaskGet  (char *ifName, char *src, char *dst, int *pSubnet);
void 	  ipHeaderCreate (int proto, struct in_addr *pSrcAddr,
		          struct in_addr *pDstAddr, struct ip *pih,
			  int length);
u_char * ipHeaderVerify (struct ip *pih, int length, int proto);
STATUS   etherSend (struct ifnet *pIf, struct ip *pDatagram, int length);


#else	/* __STDC__ */

STATUS 		icmpMaskGet  ();
void 		ipHeaderCreate ();
u_char *	ipHeaderVerify ();
STATUS		etherSend ();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCicmpLibh */
