/******************************************************************************
 ******************************************************************************
 **** This file was automatically generated by Wind River's
 **** WindManage MIB Compiler, version 9.3.
 **** This file was generated using the -leaf switch.
 **** 
 **** This file #defines C preprocessor macros providing a variety of
 **** information for the leaf objects in the MIB.
 **** 
 **** The file includes a LEAF_xxx macro for each leaf object in the
 **** MIB (xxx is replaced by the object's name).  The value of the
 **** LEAF_xxx macro is the final component of the object's object
 **** identifier.
 **** 
 **** If the object's SYNTAX clause included named INTEGER values,
 **** then there is a VAL_xxx_yyy macro for each named value (xxx is
 **** replaced by the object's name and yyy by the value's name).  The
 **** value of the VAL_xxx_yyy macro is the value associated with the
 **** named value.
 **** 
 **** If the object's SYNTAX clause specified a set of range limitations
 **** for the value of the object, then there are one or more sets of
 **** MIN_xxx and MAX_xxx macros specifying the lower and upper bound of
 **** each range limitation.
 **** 
 **** If the object's SYNTAX clause specified a set of size constraints
 **** for the value of the object, then there are one or more sets of
 **** MINSIZE_xxx and MAXSIZE_xxx macros specifying the lower and upper
 **** bound of each size constraint.  (If the size constraint is a single
 **** value rather than a range then the MINSIZE_xxx and MAXSIZE_xxx
 **** macros are replaced by a single SIZE_xxx macro.)
 **** 
 **** DO NOT MODIFY THIS FILE BY HAND.
 **** 
 **** Last build date: Wed Aug 14 10:50:14 2002
 **** from files:
 ****  rfc1213.mib, rfc1389.mib
 **** starting from node: rip2
 ******************************************************************************
 ******************************************************************************
 */

#define LEAF_rip2GlobalRouteChanges	1
#define LEAF_rip2GlobalQueries	2
#define LEAF_rip2IfStatAddress	1
#define LEAF_rip2IfStatRcvBadPackets	2
#define LEAF_rip2IfStatRcvBadRoutes	3
#define LEAF_rip2IfStatSentUpdates	4
#define LEAF_rip2IfStatStatus	5
#define VAL_rip2IfStatStatus_valid	1L
#define VAL_rip2IfStatStatus_invalid	2L
#define LEAF_rip2IfConfAddress	1
#define LEAF_rip2IfConfDomain	2
#define SIZE_rip2IfConfDomain	2L
#define LEAF_rip2IfConfAuthType	3
#define VAL_rip2IfConfAuthType_noAuthentication	1L
#define VAL_rip2IfConfAuthType_simplePassword	2L
#define LEAF_rip2IfConfAuthKey	4
#define MINSIZE_rip2IfConfAuthKey	0L
#define MAXSIZE_rip2IfConfAuthKey	16L
#define LEAF_rip2IfConfSend	5
#define VAL_rip2IfConfSend_doNotSend	1L
#define VAL_rip2IfConfSend_ripVersion1	2L
#define VAL_rip2IfConfSend_rip1Compatible	3L
#define VAL_rip2IfConfSend_ripVersion2	4L
#define LEAF_rip2IfConfReceive	6
#define VAL_rip2IfConfReceive_rip1	1L
#define VAL_rip2IfConfReceive_rip2	2L
#define VAL_rip2IfConfReceive_rip1OrRip2	3L
#define LEAF_rip2IfConfDefaultMetric	7
#define MIN_rip2IfConfDefaultMetric	0L
#define MAX_rip2IfConfDefaultMetric	15L
#define LEAF_rip2IfConfStatus	8
#define VAL_rip2IfConfStatus_valid	1L
#define VAL_rip2IfConfStatus_invalid	2L
#define LEAF_rip2PeerAddress	1
#define LEAF_rip2PeerDomain	2
#define SIZE_rip2PeerDomain	2L
#define LEAF_rip2PeerLastUpdate	3
#define LEAF_rip2PeerVersion	4
#define MIN_rip2PeerVersion	0L
#define MAX_rip2PeerVersion	255L
#define LEAF_rip2PeerRcvBadPackets	5
#define LEAF_rip2PeerRcvBadRoutes	6
