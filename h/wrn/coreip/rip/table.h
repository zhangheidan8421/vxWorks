/* table.h - definitions for managing RIP internal routing table */

/* Copyright 1984 - 2004 Wind River Systems, Inc. */

/*
 * Copyright (c) 1983, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)table.h	8.1 (Berkeley) 6/5/93
 */

/*
modification history
--------------------
01q,07oct04,spm  restored internal route classification; removed related SPR
                 #88035 changes [additional class-based routes] (SPR #102398)
01p,06dec03,niq  Merge from base6_itn3_networking-dev.al branch
01o,04nov03,rlm  Ran batch header path update for header re-org.
01n,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01m,27oct03,cdw  update include statements post header re-org.
01l,01may03,spm  Tornado 2.2 CP1 merge (from ver 01l,28mar03,rae:
                 TOR2_2-CP1-F label, tor2_2-patch branch, /wind/river VOB)
01k,09sep02,hsh  add c++ protection
01j,16may02,ant  hash definitions rt_xxx changed to rtu_xxx
01i,18dec01,hgo  modification of header files
01h,07aug01,niq  Add the RTS_PRIMARY flag definition
01g,10jul01,niq  Changes to use published routing extension interfaces
                 rather than private ones and support for static and other
                 non-rip routes.
01f,29mar01,spm  merged changes from version 01f of tor2_0.open_stack
                 branch (wpwr VOB, base 01e) for unified code base
01e,16mar99,spm  recovered orphaned code from tor1_0_1.sens1_1 (SPR #25770)
01d,29sep98,spm  added support for IP group MIB (SPR #9374); removed unused 
                 entries from rt_entry structure (SPR #22535)
01c,11sep98,spm  updated contents to prevent compilation problems (SPR #22352)
01b,24feb97,gnn  Reworked the rt_entry structure.
01a,26nov96,gnn  created from BSD4.4 routed
*/

#ifndef __INCtableh
#define __INCtableh

#ifdef __cplusplus
extern "C" {
#endif

#include <net/route.h>

/*
 * Routing table management daemon.
 */

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
#pragma align 1                 /* tell gcc960 not to optimize alignments */
#endif  /* CPU_FAMILY==I960 */

/*
 * Routing table structure; differs a bit from kernel tables.
 *
 * Note: the union below must agree in the first 4 members
 * so the ioctl's will work.
 */

struct rthash {
	struct	rt_entry *rtu_forw;
	struct	rt_entry *rtu_back;
};

struct rt_entry
    {
    struct	rt_entry *rtu_forw;
    struct	rt_entry *rtu_back;

    BOOL		 inKernel;	/* Route has been added to the kernel*/
    int			 rtu_tag;	/* Route tag - for RIP v2 only */
    short		 rtu_refcnt; 	/* How many additional interfaces */
      					/* need this route */
    short		 rtu_subnets_cnt;/* How many subnetted interfaces */
      					/* need an "internal" classful */
      					/* network route. Set only for a */
					/* classful route (internal or  */
      					/* otherwise) when atleast two */
					/* subnetted interfaces or one */
      					/* non-subnetted and atleast one */
					/* subnetted interface exists */
					/* for the network */
    int 			rtu_flags;
    struct interface *rtu_ifp;		/* Interface through which the */
				    	/* gateway is reachable */

    struct rtuentry 
        {
        u_long rtu_hash;
        struct sockaddr rtu_dst;
        struct sockaddr rtu_router;	/* Route's gateway - route supplier */
	  				/* for V1 or V2 with unspecified next*/
	  				/* hop. Set to next hop value for */
	  				/* V2 if specified */

        struct sockaddr rtu_netmask;
        int rtu_state;
        int rtu_timer;			/* Route timer */
        int rtu_metric;			/* Route metric */
	int rtu_proto;			/* Protocol that owns the route */
        } rtu_entry;
};

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
#pragma align 0                 /* turn off alignment requirement */
#endif  /* CPU_FAMILY==I960 */

#define	rtu_hash	rtu_entry.rtu_hash	/* for net or host */
#define	rtu_dst		rtu_entry.rtu_dst	/* match value */
#define	rtu_router	rtu_entry.rtu_router	/* who to forward to */
#define	rtu_netmask	rtu_entry.rtu_netmask	/* who to forward to */
#define	rtu_timer	rtu_entry.rtu_timer	/* for invalidation */
#define	rtu_state	rtu_entry.rtu_state	/* see below */
#define	rtu_metric	rtu_entry.rtu_metric	/* cost of route */
#define	rtu_proto	rtu_entry.rtu_proto	/* protocol that owns this */
						/* route */

#define	ROUTEHASHSIZ	32		/* must be a power of 2 */
#define	ROUTEHASHMASK	(ROUTEHASHSIZ - 1)

/*
 * "State" of routing table entry.
 */
#define	RTS_CHANGED	0x1		/* route has been altered recently */
#define	RTS_EXTERNAL	0x2		/* extern info, not installed or sent */
#define	RTS_INTERNAL	0x4		/* internal route, not installed */
#define	RTS_PASSIVE	IFF_PASSIVE	/* don't time out route */
#define	RTS_INTERFACE	IFF_INTERFACE	/* route is for network interface */
#define	RTS_REMOTE	IFF_REMOTE	/* route is for ``remote'' entity */
#define	RTS_SUBNET	IFF_SUBNET	/* route is for network subnet */
#define	RTS_OTHER	0x1000000	/* route installed by another */
                                        /* protocol */
#define	RTS_PRIMARY	0x2000000	/* This is a primary route */

/*
 * Flags are same as kernel, with this addition for af_rtflags
 */
#define	RTF_SUBNET	0x80000		/* pseudo: route to subnet */

#ifndef VIRTUAL_STACK
struct	rthash nethash[ROUTEHASHSIZ];
struct	rthash hosthash[ROUTEHASHSIZ];
#endif

struct	rt_entry *rtlookup();
struct	rt_entry *rtfind();

IMPORT STATUS mPrivRouteEntryAdd (long, long, long, int, int, int, 
                                  struct rtentry **);
IMPORT STATUS mPrivRouteEntryDelete (long, long, long, int, int, int, 
                                     struct rtentry *);
#ifdef __cplusplus
}
#endif
#endif /* __INCtableh */
