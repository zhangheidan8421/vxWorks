/* database.h -  DHCP server include file for database functions */

/* Copyright 1984 - 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01l,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
01k,16mar04,ann  merged from orion to create the MSP base
01j,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01i,04nov03,rlm  Ran batch header path update for header re-org.
01h,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01g,10jun03,vvv  include netVersion.h
01f,22aug02,wap  merge from T2.2
01e,11jul02,wap  add process_parameters() and all its associated machinery
01d,09oct01,rae  merge from truestack (remove some variables) 
01c,06aug97,spm  added definitions for C++ compilation
01b,09may97,spm  corrected declarations of hooks for storage routines
01a,07apr97,spm  created by modifying WIDE project DHCP implementation
*/

/*
 * WIDE Project DHCP Implementation
 * Copyright (c) 1995 Akihiro Tominaga
 * Copyright (c) 1995 WIDE Project
 * All rights reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided only with the following
 * conditions are satisfied:
 *
 * 1. Both the copyright notice and this permission notice appear in
 *    all copies of the software, derivative works or modified versions,
 *    and any portions thereof, and that both notices appear in
 *    supporting documentation.
 * 2. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *      This product includes software developed by WIDE Project and
 *      its contributors.
 * 3. Neither the name of WIDE Project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE DEVELOPER ``AS IS'' AND WIDE
 * PROJECT DISCLAIMS ANY LIABILITY OF ANY KIND FOR ANY DAMAGES
 * WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE. ALSO, THERE
 * IS NO WARRANTY IMPLIED OR OTHERWISE, NOR IS SUPPORT PROVIDED.
 *
 * Feedback of the results generated from any improvements or
 * extensions made to this software would be much appreciated.
 * Any such feedback should be sent to:
 * 
 *  Akihiro Tominaga
 *  WIDE Project
 *  Keio University, Endo 5322, Kanagawa, Japan
 *  (E-mail: dhcp-dist@wide.ad.jp)
 *
 * WIDE project has the rights to redistribute these changes.
 */

#ifndef __INCdatabaseh
#define __INCdatabaseh

#ifdef __cplusplus
extern "C" {
#endif

#include <netVersion.h>

/*
 * functions prototype definition
 */
void dump_bind_entry();
static time_t strtotime();
int bindcidcmp();
int paramcidcmp();
int resipcmp();
int relayipcmp();
time_t _mktime();

static void read_entry();
int process_entry();
void set_default(struct dhcp_resource *);
static void adjust();
static void get_string();
static void get_string_vend();
int eval_symbol();
static void eat_whitespace();
static long get_integer();
static int prs_inaddr();
static STATUS get_ip();
static void default_netmask();
int resnmcmp();
static int read_idtype();
static int read_cid();
static int read_haddr();
static int read_subnet();

int proc_sname();
int proc_file();
int proc_tblc();
int proc_mtpt();
int proc_clid();
int proc_class();
int proc_ip();
int proc_ips();
int proc_ippairs();
int proc_hl();
int proc_hs();
int proc_nl();
int proc_ns();
int proc_octet();
int proc_str();
int proc_bool();
int proc_vend();

STATUS process_parameters ();	/* pdr: retrieve parameters of an entry */

/*
 * pdr: reverse function for proc_xxx to retrieve parameters list
 * from a lease descriptor
 */
int proc_mtpt_rev();
int proc_clid_rev();
int proc_class_rev();
int proc_ip_rev();
int proc_ips_rev();
int proc_ippairs_rev();
int proc_hl_rev();
int proc_hs_rev();
int proc_nl_rev();
int proc_ns_rev();
int proc_octet_rev();
int proc_str_rev();
int proc_bool_rev();



#define SUCCESS	 		  0
#define E_END_OF_ENTRY		  1

#define MAXENTRYLEN             4096
#define MAXSTRINGLEN             260
#define MAX_MTUPLTSZ             127
#define MAX_IPS                   63
#define MAX_IPPAIR                31

#define OP_ADDITION		  1	/* Operations on tags */
#define OP_DELETION		  2

#ifndef BUFSIZ
#define BUFSIZ                  1024
#endif

#ifndef VIRTUAL_STACK
IMPORT FUNCPTR dhcpsLeaseHookRtn;
IMPORT FUNCPTR dhcpsAddressHookRtn;
#endif  /* !VIRTUAL_STACK */

struct symbolmap {
    char *symbol;
    int code;
    int (*func)();
    int (*funcRev) ();	/* pdr: reverse function to retrieve data */
};

static struct symbolmap symbol_list[] = {
  { "tblc", S_TABLE_CONT	, proc_tblc	, NULL		 },
  { "snam", S_SNAME		, proc_sname	, proc_str_rev	 },
  { "file", S_FILE		, proc_file	, proc_str_rev	 },
  { "siad", S_SIADDR		, proc_ip	, proc_ip_rev	 },
  { "albp", S_ALLOW_BOOTP	, proc_bool	, proc_bool_rev	 },
  { "ipad", S_IP_ADDR		, proc_ip	, NULL		 },
  { "maxl", S_MAX_LEASE		, proc_hl	, proc_hl_rev	 },
  { "dfll", S_DEFAULT_LEASE	, proc_hl	, proc_hl_rev	 },
  { "clid", S_CLIENT_ID		, proc_clid	, proc_clid_rev	 },
  { "pmid", S_PARAM_ID		, proc_clid	, proc_clid_rev	 },
  { "clas", S_CLASS_ID		, proc_class	, proc_class_rev },
  { "snmk", S_SUBNET_MASK	, proc_ip	, proc_ip_rev	 },
  { "tmof", S_TIME_OFFSET	, proc_nl	, proc_nl_rev	 },
  { "rout", S_ROUTER		, proc_ips	, proc_ips_rev	 },
  { "tmsv", S_TIME_SERVER	, proc_ips	, proc_ips_rev	 },
  { "nmsv", S_NAME_SERVER	, proc_ips	, proc_ips_rev	 },
  { "dnsv", S_DNS_SERVER	, proc_ips	, proc_ips_rev	 },
  { "lgsv", S_LOG_SERVER	, proc_ips	, proc_ips_rev	 },
  { "cksv", S_COOKIE_SERVER	, proc_ips	, proc_ips_rev	 },
  { "lpsv", S_LPR_SERVER	, proc_ips	, proc_ips_rev	 },
  { "imsv", S_IMPRESS_SERVER	, proc_ips	, proc_ips_rev	 },
  { "rlsv", S_RLS_SERVER	, proc_ips	, proc_ips_rev	 },
  { "hstn", S_HOSTNAME		, proc_str	, proc_str_rev	 },
  { "btsz", S_BOOTSIZE		, proc_ns	, proc_ns_rev	 },
  { "mdmp", S_MERIT_DUMP	, proc_str	, proc_str_rev	 },
  { "dnsd", S_DNS_DOMAIN	, proc_str	, proc_str_rev	 },
  { "swsv", S_SWAP_SERVER	, proc_ip	, proc_ip_rev	 },
  { "rpth", S_ROOT_PATH		, proc_str	, proc_str_rev	 },
  { "epth", S_EXTENSIONS_PATH	, proc_str	, proc_str_rev	 },
  { "ipfd", S_IP_FORWARD	, proc_bool	, proc_bool_rev	 },
  { "nlsr", S_NONLOCAL_SRCROUTE	, proc_bool	, proc_bool_rev	 },
  { "plcy", S_POLICY_FILTER	, proc_ippairs	, proc_ippairs_rev },
  { "mdgs", S_MAX_DGRAM_SIZE	, proc_ns	, proc_ns_rev	 },
  { "ditl", S_DEFAULT_IP_TTL	, proc_octet	, proc_octet_rev },
  { "mtat", S_MTU_AGING_TIMEOUT	, proc_nl	, proc_nl_rev	 },
  { "mtpt", S_MTU_PLATEAU_TABLE	, proc_mtpt	, proc_mtpt_rev	 },
  { "ifmt", S_IF_MTU		, proc_ns	, proc_ns_rev	 },
  { "asnl", S_ALL_SUBNET_LOCAL	, proc_bool	, proc_bool_rev	 },
  { "brda", S_BRDCAST_ADDR	, proc_ip	, proc_ip_rev	 },
  { "mskd", S_MASK_DISCOVER	, proc_bool	, proc_bool_rev	 },
  { "msks", S_MASK_SUPPLIER	, proc_bool	, proc_bool_rev	 },
  { "rtrd", S_ROUTER_DISCOVER	, proc_bool	, proc_bool_rev	 },
  { "rtsl", S_ROUTER_SOLICIT	, proc_ip	, proc_ip_rev	 },
  { "strt", S_STATIC_ROUTE	, proc_ippairs	, proc_ippairs_rev },
  { "trlr", S_TRAILER		, proc_bool	, proc_bool_rev	 },
  { "arpt", S_ARP_CACHE_TIMEOUT	, proc_nl	, proc_nl_rev	 },
  { "encp", S_ETHER_ENCAP	, proc_bool	, proc_bool_rev	 },
  { "dttl", S_DEFAULT_TCP_TTL	, proc_octet	, proc_octet_rev },
  { "kain", S_KEEPALIVE_INTER	, proc_nl	, proc_nl_rev	 },
  { "kagb", S_KEEPALIVE_GARBA	, proc_bool	, proc_bool_rev	 },
  { "nisd", S_NIS_DOMAIN	, proc_str	, proc_str_rev	 },
  { "nisv", S_NIS_SERVER	, proc_ips	, proc_ips_rev	 },
  { "ntsv", S_NTP_SERVER	, proc_ips	, proc_ips_rev	 },
  { "nnsv", S_NBN_SERVER	, proc_ips	, proc_ips_rev	 },
  { "ndsv", S_NBDD_SERVER	, proc_ips	, proc_ips_rev	 },
  { "nbnt", S_NB_NODETYPE	, proc_octet	, proc_octet_rev },
  { "nbsc", S_NB_SCOPE		, proc_str	, proc_str_rev	 },
  { "xfsv", S_XFONT_SERVER	, proc_ips	, proc_ips_rev	 },
  { "xdmn", S_XDISPLAY_MANAGER	, proc_ips	, proc_ips_rev	 },
  { "dht1", S_DHCP_T1		, proc_hs	, proc_hs_rev	 },
  { "dht2", S_DHCP_T2		, proc_hs	, proc_hs_rev	 },
  { "nspd", S_NISP_DOMAIN	, proc_str	, NULL		 },
  { "nsps", S_NISP_SERVER	, proc_ips	, NULL		 },
  { "miph", S_MOBILEIP_HA	, proc_ips	, NULL		 },
  { "smtp", S_SMTP_SERVER	, proc_ips	, NULL		 },
  { "pops", S_POP3_SERVER	, proc_ips	, NULL		 },
  { "nntp", S_NNTP_SERVER	, proc_ips	, NULL		 },
  { "wwws", S_DFLT_WWW_SERVER	, proc_ips	, NULL		 },
  { "fngs", S_DFLT_FINGER_SERVER, proc_ips	, NULL		 },
  { "ircs", S_DFLT_IRC_SERVER	, proc_ips	, NULL		 },
  { "stsv", S_STREETTALK_SERVER	, proc_ips	, NULL		 },
  { "stda", S_STDA_SERVER, proc_ips, NULL },
  { "vend", S_VEND_OPT, proc_vend, NULL }
};
#ifdef notdef
#ifndef VIRTUAL_STACK
struct hash_tbl cidhashtable;
struct hash_tbl iphashtable;
struct hash_tbl nmhashtable;
struct hash_tbl relayhashtable;
struct hash_tbl paramhashtable;
struct hash_member *bindlist;
struct hash_member *reslist;
#endif  /* !VIRTUAL_STACK */
#endif

#ifdef __cplusplus
}
#endif

#endif
