/* Copyright 1984-2005 Wind River Systems, Inc. */
#include "copyright_wrs.h"
/*
modification history
--------------------
01f,24jan05,vvv  added deprecation message
01e,23sep04,kc   Added coldfire arch support.
01d,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01c,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5-int/1)
01e,23mar03,dbt  Added SIMPENTIUM support
01d,08dec03,dat  cleanup CPU_FAMILY values
01c,08dec03,ann  fixed the incorrect path for SH
01b,08dec03,xli	 correct the path for sh
01a,04dec03,xli	 creation
*/

/*
DESCRIPTION
This file is a wrapper of machdep.h which include specific machdep.h depend on the CPU_FAMILY
*/

#warning "The use of this file is deprecated. Use vxWorks.h instead."

#if (CPU_FAMILY==PPC)
#include<arch/ppc/machdep.h>
#elif (CPU_FAMILY==MIPS) 
#include<arch/mips/machdep.h>
#elif (CPU_FAMILY==I80X86)
#include<arch/pentium/machdep.h>
#elif (CPU_FAMILY==ARM)
#include<arch/arm/machdep.h>
#elif (CPU_FAMILY==SH)
#include<arch/sh/machdep.h>
#elif (CPU_FAMILY==COLDFIRE)
#include<arch/coldfire/machdep.h>
#elif ((CPU_FAMILY==SIMPENTIUM) || (CPU_FAMILY==SIMNT) || \
	(CPU_FAMILY==SIMLINUX))
#include<arch/simpentium/machdep.h>
#elif (CPU_FAMILY==SIMSPARCSOLARIS)
#include<arch/simso/machdep.h>
#endif
