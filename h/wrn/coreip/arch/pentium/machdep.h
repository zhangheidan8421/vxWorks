/* 
 * machdep.h -- architecture dependent include files
 *              all other files should include this header for 
 *              machine related dependencies
 */

/* Copyright 1984-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01g,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
01f,20nov03,niq  Remove copyright_wrs.h file inclusion
01f,11nov03,sn   avoid duplicating int64_t typedef (already defined in vxTypes.h)
01e,23apr03,pas  removed ptr_t
01d,27jun02,ppp  merging TeamF1's t22 changes (01d,10jun02,rvr  port to tor22 (teamf1))
01c,01oct01,nee  adding defin. for intptr_t
01b,08aug01,ann  adding some more OS dependant includes
01a,06aug01,qli  adding MACRO redefinitons
*/


#ifndef _MACHINE_DEP_H_
#define _MACHINE_DEP_H_

#include "ansi.h"
#include "endian.h"
#include "param.h"

#include "../types.h"

typedef	__uintptr_t	uintptr_t;
typedef __intptr_t      intptr_t;

#if 0
typedef unsigned char		u_int8_t;
typedef unsigned short		u_int16_t;
typedef unsigned long		u_int32_t;


typedef	__uint64_t	uint64_t;

typedef	__intptr_t	intptr_t;
typedef	__uintptr_t	uintptr_t;

#endif


#endif

