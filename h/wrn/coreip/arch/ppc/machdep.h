/* 
 * machdep.h -- architecture dependent include files
 *              all other files should include this header for 
 *              machine related dependencies
 */

/* Copyright 1984-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01i,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
01h,09apr04,nee  Fixing redundant declaration of int64_t
01g,20nov03,niq  Remove copyright_wrs.h file inclusion
01f,23apr03,pas  removed ptr_t
01e,01aug02,hsh  clean up unused #if0 block
01d,18jul02,ham  added conditional int64_t declaration
01c,01oct01,nee  adding defin. for intptr_t
01b,08aug01,ann  adding some more OS dependant includes
01a,06aug01,qli  adding MACRO redefinitons
*/


#ifndef _MACHINE_DEP_H_
#define _MACHINE_DEP_H_

#include "ansi.h"
#include "endian.h"
#include "param.h"

#include "../types.h"

typedef	__uintptr_t	uintptr_t;
typedef __intptr_t      intptr_t;


#endif

