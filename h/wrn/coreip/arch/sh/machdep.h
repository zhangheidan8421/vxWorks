/* 
 * machdep.h -- architecture dependent include files
 *              all other files should include this header for 
 *              machine related dependencies
 */

/* Copyright 1984-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01h,20aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01g,16jun04,kc   Fixed SPR#98289 - removed duplicate int64_t typedef (already
                 defined in vxTypes.h).
01f,20nov03,niq  Remove copyright_wrs.h file inclusion
01e,23apr03,pas  removed ptr_t
01d,18jul02,ham  added conditional int64_t declaration
01c,01oct01,nee  adding defin. for intptr_t
01b,08aug01,ann  adding some more OS dependant includes
01a,06aug01,qli  adding MACRO redefinitons
*/


#ifndef _MACHINE_DEP_H_
#define _MACHINE_DEP_H_

#include "ansi.h"
#include "endian.h"
#include "param.h"

#include "../types.h"

typedef	__uintptr_t	uintptr_t;
typedef __intptr_t      intptr_t;

#if 0
typedef unsigned char		u_int8_t;
typedef unsigned short		u_int16_t;
typedef unsigned long		u_int32_t;

typedef __int64_t       int64_t;
typedef	__uint64_t	uint64_t;

typedef	__intptr_t	intptr_t;
typedef	__uintptr_t	uintptr_t;
#endif

#endif /* _MACHINE_DEP_H */

