/* vsMroute6.h - virtual stack data for mroute6 */

/*
 * Copyright (c) 2004-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */


/*
modification history
--------------------
01f,11jul05,dlk  Added expire_upcalls_ch6. Mod history clean-up.
01e,28apr05,kch  Fixed virtualization issues.
01d,27apr05,kch  Move ip6_mrouter definition to VS_IP6.
01c,19apr05,rp   merged from comp_wn_ipv6_mld_interim-dev
01b,28dec04,kc   Renamed pim6 to _pim6.
01a,22dec04,kc   written
*/

#ifndef __INCvsMcast6h
#define __INCvsMcast6h

#ifdef __cplusplus
extern "C" {
#endif

#include <netinet6/ip6_mroute.h>
#ifdef WRS_PIM6
#include <netinet6/pim6_var.h>
#endif /* WRS_PIM6 */
#include <sys/callout.h>

#define VS_IP_MROUTE6_DATA ((VS_IP_MROUTE6 *)vsTbl[myStackNum]->pMroute6Globals)

typedef struct vs_ip_mroute6
{
/*
 * Globals.  All but ip6_mrouter, ip6_mrtproto and mrt6stat could be static,
 * except for netstat or debugging purposes.
 */

    int             ip6_mrouter_ver;
    int             ip6_mrtproto;
    struct mrt6stat _mrt6stat;
    struct mf6c     *_mf6ctable[MF6CTBLSIZ];
    u_char          n6expire[MF6CTBLSIZ];
    struct mif6     _mif6table[MAXMIFS];

    struct callout  expire_upcalls_ch6;

#ifdef MRT6DEBUG    
    u_int           mrt6debug;    /* debug level */
#endif /* MRT6DEBUG */    

/*
 * 'Interfaces' associated with decapsulator (so we can tell
 * packets that went through it from ones that get reflected
 * by a broken gateway).  These interfaces are never linked into
 * the system ifnet list & no routes point to them.  I.e., packets
 * can't be sent this way.  They only exist as a placeholder for
 * multicast source verification.
 */
    struct ifnet    multicast_register_if;

    mifi_t          nummifs;
    mifi_t          reg_mif_num;

#ifdef WRS_PIM6
    struct pim6stat _pim6stat;
    int _pim6;
#endif /* WRS_PIM6 */

#ifdef UPCALL_TIMING
#define UPCALL_MAX	50
    u_long upcall_data[UPCALL_MAX + 1];    
#endif /* UPCALL_TIMING */

} VS_IP_MROUTE6;

#define ip6_mrouter_ver         VS_IP_MROUTE6_DATA->ip6_mrouter_ver
#define ip6_mrtproto            VS_IP_MROUTE6_DATA->ip6_mrtproto
#define _mrt6stat               VS_IP_MROUTE6_DATA->_mrt6stat
#define _mf6ctable              VS_IP_MROUTE6_DATA->_mf6ctable
#define n6expire                VS_IP_MROUTE6_DATA->n6expire
#define _mif6table              VS_IP_MROUTE6_DATA->_mif6table
#define expire_upcalls_ch6	VS_IP_MROUTE6_DATA->expire_upcalls_ch6
#define mrt6debug               VS_IP_MROUTE6_DATA->mrt6debug
#define multicast_register_if   VS_IP_MROUTE6_DATA->multicast_register_if
#define nummifs                 VS_IP_MROUTE6_DATA->nummifs
#define reg_mif_num             VS_IP_MROUTE6_DATA->reg_mif_num

#ifdef WRS_PIM6
#define _pim6stat               VS_IP_MROUTE6_DATA->_pim6stat
#define _pim6                   VS_IP_MROUTE6_DATA->_pim6
#endif /* WRS_PIM6 */

#ifdef UPCALL_TIMING
#define upcall_data             VS_IP_MROUTE6_DATA->upcall_data
#endif /* UPCALL_TIMING */

#ifdef __cplusplus
}
#endif

#endif /* __INCvsMcast6h */

