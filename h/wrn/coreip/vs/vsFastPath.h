/* vsFastPath.h - virtual stack data for Fastpath */

/* Copyright 1984 - 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01g,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01f,09jun04,spm  fixed fastpath function pointers for virtual stacks
01e,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
01d,20nov03,niq  Remove copyright_wrs.h file inclusion
01c,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01b,04nov03,rlm  Ran batch header path update for header re-org.
01a,07oct02,deg  written
*/

/*
DESCRIPTION
This file contains virtual stack data definitions for Fastpath.
 
*/

#ifndef __INCvsFastPathh
#define __INCvsFastPathh

/* includes */

#include <fastPath/fastPathLib.h>

/* defines */

#define VS_FAST_PATH_DATA ((VS_FAST_PATH *)vsTbl[myStackNum]->pFastFrwdGlobals)

#define ffIPv4Handle 		VS_FAST_PATH_DATA->_ffIPv4Handle
#define ffIPv6Handle 		VS_FAST_PATH_DATA->_ffIPv6Handle
#define ffLibLogLevel 		VS_FAST_PATH_DATA->_ffLibLogLevel

/* typedefs */

typedef struct vsFastPath
{
  struct ffObj *_ffIPv4Handle;  /* Handle of the IPv4 FF instance */
  struct ffObj *_ffIPv6Handle;  /* Handle of the IPv6 FF instance */
  int           _ffLibLogLevel; /* FF library log level */ 
} VS_FAST_PATH;

#endif /* __INCvsFastPathh */
