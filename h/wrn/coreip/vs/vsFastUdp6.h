/* vsFastUdp6.h - virtual stack data for Fast UDP */

/* Copyright 2002 - 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01c,04nov03,rlm  Ran batch header path update for header re-org.
01b,15sep03,vvv  updated path for new headers
01a,10dec02,hgo  copied from vsFastUdp.h
*/

#ifndef __INCvsfastudp6h
#define __INCvsfastudp6h


/* includes */

#include <net/fastUdp6Lib.h>

/* defines */

/* typedefs */

typedef struct vs_fastudp6
    {
    /* locals from fastUdpLib.c */

    FASTUDP6_ID _priv_fastUdp6Head;
    u_int       _priv_fastUdp6Debug;
    SEM_ID      _priv_mutexSem;
    LIST        _priv_portList;
    SEM_ID      _priv_portListSem;
    BOOL        _priv_vsWaitingForFastUdp6;
    SEM_ID      _priv_vsFastUdp6ListSync;
    } VS_FASTUDP6;


/* macros */

#define VS_FASTUDP6_DATA ((VS_FASTUDP6 *) vsTbl[myStackNum]->pFastUdp6Globals)

#define fastUdp6Initialized vsTbl[myStackNum]->fastUdp6Initialized

#endif /* __INCvsfastudp6h */
