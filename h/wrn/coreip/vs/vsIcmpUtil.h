/* vsIcmpUtil.h - virtual stack data for ICMP */

/* Copyright 2000 - 2003 Wind River Systems, Inc. */
 
/*
modification history
--------------------
01c,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01b,04nov03,rlm  Ran batch header path update for header re-org.
01a,15jan03,hgo  created to seperate utilities from ICMP
*/

#ifndef __INCvsIcmpUtilh
#define __INCvsIcmpUtilh


/* includes */

#include <sys/socket.h> 
#include <netinet/ip_icmp.h>
#include <netinet/icmp_var.h>

/* defines */

/* typedefs */

typedef struct vs_icmp_util_global_data
    {
    /* definitions from icmpUtilLib.c */
    BOOL  priv_maskReplyReceived;       /* recv mask reply 	*/
    int	  priv_icmpMask;		/* replied icmp mask    */
    struct
        {
        struct ip	ih;		/* IP header		*/
        struct icmp	icmph;		/* ICMP header		*/
        } priv_icmpMsg;
    } VS_ICMPUTIL;

/* macros */

#define VS_ICMPUTIL_DATA ((VS_ICMPUTIL *)vsTbl[myStackNum]->pIcmpUtilGlobals)

#endif /* __INCvsIcmpUtilh */

