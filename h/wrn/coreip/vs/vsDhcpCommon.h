/* vsDhcpCommon.h - virtual stack data for DHCP Server and Relay Agent routines */

/* Copyright 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01f,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01e,04nov03,rlm  Ran batch header path update for header re-org.
01d,24mar03,ant  added new variable, dhcp_started, into the VS_DHCPCOMMON
01c,13nov02,ant  added dhcp_exit_sock field in the vs_dhcpcommon_global_data
01b,28aug02,kal  must include dhcp.h before common.h for MAXOPT
01a,22jul02,ant  created from vsDhcps.h
*/

#ifndef __INCvsdhcpcommonh
#define __INCvsdhcpcommonh

#include <dhcp/dhcp.h>
#include <dhcp/common.h>
#include <dhcprLib.h>

typedef struct vs_dhcpcommon_global_data
    {
    /* Globals in dhcpRelay.c */

    DHCP_TARGET_DESC *pDhcpRelayTargetTbl;
    struct server *pDhcpTargetList;
    int dhcpNumTargets;
    struct msg dhcpMsgIn;
    struct msg dhcprMsgOut;
    u_short dhcps_port;                 /* Server port, in network byte order. */
    u_short dhcpc_port;                 /* Client port, in network byte order. */
    int dhcp_exit_sock;
    
    BOOL dhcp_started;			
    /* 
     * If TRUE Server or RAgent already started in a VS instance. Don't allow 
     * to start both in the same stack instance.
     */

    } VS_DHCPCOMMON;


/* Macro */

#define VS_DHCPCOMMON_DATA ((VS_DHCPCOMMON *)vsTbl[myStackNum]->pDhcpCommonGlobals)

/* Defines for the dhcp routine globals */

 /* Globals in dhcpRelay.c */
#define pDhcpRelayTargetTbl     VS_DHCPCOMMON_DATA->pDhcpRelayTargetTbl
#define pDhcpTargetList		VS_DHCPCOMMON_DATA->pDhcpTargetList
#define dhcpNumTargets		VS_DHCPCOMMON_DATA->dhcpNumTargets
#define dhcpMsgIn		VS_DHCPCOMMON_DATA->dhcpMsgIn
#define dhcprMsgOut		VS_DHCPCOMMON_DATA->dhcprMsgOut
#define dhcps_port		VS_DHCPCOMMON_DATA->dhcps_port
#define dhcpc_port		VS_DHCPCOMMON_DATA->dhcpc_port
#define dhcp_exit_sock		VS_DHCPCOMMON_DATA->dhcp_exit_sock
#define dhcp_started		VS_DHCPCOMMON_DATA->dhcp_started

#endif /* __INCvsdhcpcommonh */
