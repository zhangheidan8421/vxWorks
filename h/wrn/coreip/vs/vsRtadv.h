/* vsRtadv.h - virtual stack data for Router Advertisement */

/*
 * Copyright (c) 2000-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01i,14jul05,niq  added the rtadv_forwarding variable 
01h,07oct04,vvv  added rtadvSem (SPR #98895)
01g,11nov03,cdw  Removal of unnecessary _KERNEL guards.
01f,10nov03,rlm  2nd pass of include update for header re-org.
01e,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01d,04nov03,rlm  Ran batch header path update for header re-org.
01c,25nov02,ism  moved rtadv global data to its own array
                 define new global data for normal shutdown socket
01b,06sep02,hgo  removed #ifdef from VS_RTADV
01a,02sep02,hgo  created
*/

#ifndef __INCvsRtadvh
#define __INCvsRtadvh

#include <net/if.h>
#include <sys/socket.h>
#include <net/uio.h>
#include <netinet/in.h>
#include <rtadv/rtadvd.h>
#include <rtadv/timer.h>

/* typedefs */

    /* Encapsulated (former) globals for Rtadv */

typedef struct vsRtadv
    {
    /* defined in advcap.c */
    
    char               *_priv_tbuf;
    int                 _priv_hopcount;          /* detect infinite loops in termcap. init 0 */
    
    /* defined in if_rtadv.c */

    struct if_msghdr  **_iflist;
    size_t              _priv_ifblock_size;
    char               *_priv_ifblock;
    
    /* defined in rtadv.c */
    
    struct msghdr       _priv_rcvmhdr;
    u_char             *_priv_rcvcmsgbuf;
    size_t              _priv_rcvcmsgbuflen;
    u_char             *_priv_sndcmsgbuf;
    size_t              _priv_sndcmsgbuflen;
    int                 _rtadv_exit_flag;
    struct msghdr       _priv_sndmhdr;
    struct iovec        _priv_rcviov[2];
    struct iovec        _priv_sndiov[2];
    struct sockaddr_in6 _priv_from;
    struct sockaddr_in6 _priv_sin6_allnodes;
    int                 _priv_sock;
    int                 _priv_rtsock;
    int                 _priv_exit_sock;
    int                 _priv_exit_port;
    int                 _rtadv_mobileip6;
    int                 _priv_accept_rr;
    int                 _priv_dflag_rtadv;
    int                 _priv_sflag_rtadv;
    struct rainfo      *_ralist;
    SEM_ID              _rtadvSem;
    u_char             *_priv_answer;
    int                 _rtadv_forwarding;

    /* defined in timer.c */
    
    struct rtadvd_timer _priv_timer_head;
    struct timeval      _priv_returnval_check;
    struct timeval      _priv_returnval_rest;

    } VS_RTADV;

IMPORT VS_RTADV* rtadvGlobalTbl[];

/* macros for "non-private" global variables */

#define VS_RTADV_DATA ((VS_RTADV *)rtadvGlobalTbl[myStackNum])

#define iflist              VS_RTADV_DATA->_iflist
#define rtadv_exit_flag     VS_RTADV_DATA->_rtadv_exit_flag
#define rtadv_mobileip6     VS_RTADV_DATA->_rtadv_mobileip6
#define ralist              VS_RTADV_DATA->_ralist
#define rtadvSem            VS_RTADV_DATA->_rtadvSem
#define rtadv_forwarding    VS_RTADV_DATA->_rtadv_forwarding

#endif /* __INCvsRtadvh */
