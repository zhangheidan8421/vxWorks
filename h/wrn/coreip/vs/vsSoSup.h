/* vsSoSup.h - virtual stack support for sockets */

/* Copyright 2000 - 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,21jul04,spm  enabled socket operations from different virtual stacks
01b,15nov02,wie  prototype for vsSoDelete changed
01a,29jul02,wie  file creation

*/

#ifndef __INCvsSoSuph
#define __INCvsSoSuph


#ifdef VIRTUAL_STACK


/*
 * Prevent socket calls after a virtual stack is disabled,
 * deleted, or replaced. Also prevent calls from a different
 * virtual stack context if the socket is setup to reject them.
 */

#define VS_SOCKET_CHECK(pSocket)    \
    {    \
    if (pSocket->vsid & VSNUM_INVALID_MASK)    \
        {    \
        errnoSet (ENOTSUP);    \
        return (ERROR);        \
        }    \
    if ( (pSocket->vsid & VSNUMMASK) != myStackNum &&    \
        (pSocket->so_options & SO_VSLOCK))    \
        {    \
        errnoSet (ENOTSUP);    \
        return (ERROR);        \
        }    \
    }

#define VS_STACK_CHECK(pSocket)    \
    {    \
    if (pSocket->vsid & VSNUM_INVALID_MASK)    \
        {    \
        if (vsErrAnalyse (pSocket) != VS_OK)    \
            return (ERROR);    \
        }    \
    }

VS_STATUS vsErrAnalyse
	(
	struct socket *so
 	);


VS_STATUS vsSoCreate
    (
    void *so_l
    );

VS_STATUS vsSoDelete
    (
    VSID vsid,
	int vsCookie
    );


#ifdef __cplusplus
}
#endif

#endif /* VIRTUAL_STACK */

#endif /* __INCvsSoSuph */
