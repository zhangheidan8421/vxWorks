/* vsUdp.h - virtual stack data for RAW_CB */

/*
 * Copyright (c) 2002-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. No license to Wind River intellectual property rights
 * is granted herein. All rights not licensed by Wind River are reserved
 * by Wind River.
 */

/*
modification history
--------------------
01e,25feb05,niq  merge from niqbal.tor3.mroute for IP router alert changes
01d,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01c,04nov03,rlm  Ran batch header path update for header re-org.
01b,28aug02,kal  fixed header location for !_KERNEL
01a,01jul02,hgo  creation
*/

#ifndef __INCvsRawh
#define __INCvsRawh

/*
 * Remaining globals: shared between stacks (not part of virtual structure).
 *
 *  1) raw_usrreqs
 *
 */

/* includes */
#include <net/raw_cb.h>

/* typedefs */

    /* Encapsulated (former) globals for UDP. */

typedef struct vsRaw
    {
    /* defined in raw_cb.c */

    struct rawcb_list_head _rawcb_list;
    u_long                 _priv_raw_sendspace;
    u_long                 _priv_raw_recvspace;
    } VS_RAW;

/* macros for "non-private" global variables */

#define VS_RAW_DATA ((VS_RAW *)vsTbl[myStackNum]->pRawGlobals)

#define rawcb_list         VS_RAW_DATA->_rawcb_list

#endif /* __INCvsRawh */
