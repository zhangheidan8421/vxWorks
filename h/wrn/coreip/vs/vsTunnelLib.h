/* vsTunnelLib.h - virtual stack data for tunnelLib */

/* Copyright 2002 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,30oct02,kal  added virtualised MTU Refresh timeout
01a,02aug02,kal  created
*/

#ifndef __INCvsTunnelLibh
#define __INCvsTunnelLibh


/* includes */

/* defines */

typedef struct vstunnellib
    {
    SEM_ID      tunnelLibSem;
    LIST        tunnelIfList;
    int         mtuRefreshTimeout;

    AVL_TREE    autoList4Root;
    AVL_TREE    autoList6Root;
    AVL_TREE    configList4Root;
    AVL_TREE    configList6Root;
    AVL_TREE *  pAutoList4Root;
    AVL_TREE *  pAutoList6Root;
    AVL_TREE *  pConfigList4Root;
    AVL_TREE *  pConfigList6Root;
    } VS_TUNNELLIB;

/* macros */

#define VS_TUNNELLIB_DATA ((VS_TUNNELLIB *)(vsTunnelLibTbl[myStackNum]))

#define tunnelLibSem     	VS_TUNNELLIB_DATA->tunnelLibSem
#define tunnelIfList     	VS_TUNNELLIB_DATA->tunnelIfList
#define mtuRefreshTimeout   VS_TUNNELLIB_DATA->mtuRefreshTimeout
#define autoList4Root     	VS_TUNNELLIB_DATA->autoList4Root
#define autoList6Root     	VS_TUNNELLIB_DATA->autoList6Root
#define configList4Root     VS_TUNNELLIB_DATA->configList4Root
#define configList6Root     VS_TUNNELLIB_DATA->configList6Root
#define pAutoList4Root      VS_TUNNELLIB_DATA->pAutoList4Root
#define pAutoList6Root      VS_TUNNELLIB_DATA->pAutoList6Root
#define pConfigList4Root    VS_TUNNELLIB_DATA->pConfigList4Root
#define pConfigList6Root    VS_TUNNELLIB_DATA->pConfigList6Root

#endif /* __INCvsTunnelLibh */

