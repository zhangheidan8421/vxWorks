/* vsIpRoute.h - virtual stack definitions for the route interface library */

/*
 * Copyright (c) 1984-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01l,19may05,rp   added pRtAddrInfoMsgHook
01k,10may05,niq  Changes for debugging
01j,24feb05,spm  performance updates and code cleanup (SPR #100995)
01i,05oct04,rae  add _mrt_ioctl for route.c
01h,17sep04,niq  Code cleanup
01g,28may04,niq  Changes for VIRTUAL_STACK
01f,20nov03,niq  Remove copyright_wrs.h file inclusion
01e,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01d,04nov03,rlm  Ran batch header path update for header re-org.
01c,15sep03,vvv  updated path for new headers
01b,08nov02,niq  Don't generate routing messages upon stack destruction.
01a,15oct02,niq  file creation
*/

/*
DESCRIPTION
This file includes virtual stack definitions for the RTM
*/

#ifndef _INCvsIpRouteH
#define _INCvsIpRouteH

#ifdef __cplusplus
extern "C" {
#endif

#include <net/socketvar.h>
#include <net/mbuf.h>
#include <sys/socket.h>        /* needed for route.h */
#include <net/route.h>
#include <route/ipRouteLib.h>
#include <netinet/in.h>        /* RT_PROTO_SET and RT_PROTO_GET */
#include <m2Lib.h>

/* structure for the RTM global variables */
struct ipRouteVars
    {
#ifdef DEBUG
    ULONG 		_ipRouteLibLogLevel;
#endif /* DEBUG */
    UCHAR 		_ipRouteDefaultWeight;
    int 		_rtmodified;
    int 		priv_rttrash;   /* private to ipRouteLib.c */
    BOOL		ipRouteStackGoingDown;
    SEM_ID		ipRouteLockSem;
    IP_ROUTE_USAGE_T	ipRouteUsageTbl [ROUTE_USAGES_MAX + 1];
    BOOL 		ipRouteWalkDeleteFlag;

    /* definitions from route.c */
    struct route_cb 	_route_cb;
    struct rtstat 	_rtstat;
    FUNCPTR             _mrt_ioctl;

#ifdef WV_INSTRUMENTATION
#ifdef INCLUDE_WVNET
    UCHAR 		_rtWvNetLocalFilter;
    ULONG 		_rtWvNetEventId;
#endif /* INCLUDE_WVNET */
#endif /* WV_INSTRUMENTATION */
    };
typedef struct ipRouteVars VS_IP_ROUTE_VARS_T;
    
typedef struct vs_rtsock_global_data
    {
    /* definitions from rtsock.c */
    struct sockaddr 		priv_route_dst;
    struct sockaddr 		priv_route_src;
    struct sockaddr 		priv_sa_zero;
    struct sockproto 		priv_route_proto;
    struct protosw 		priv_routesw[IP_PROTO_NUM_MAX];
    struct domain 		routedomain;
    FUNCPTR 			pRtIfAnnounceMsgHook;
    FUNCPTR 			pRtIfMsgHook;
    FUNCPTR 			pRtNewMaddrMsgHook;
    FUNCPTR 			pRtNewAddrMsgHook;
    FUNCPTR 			pRtMissMsgHook;
    FUNCPTR 			pRtMsg1;
    FUNCPTR 			pRtAddrInfoMsgHook;
    } VS_RTSOCK;

/* macros */
#define pIpRouteVars	\
	((VS_IP_ROUTE_VARS_T *)(vsTbl [myStackNum]->pRouteGlobals))
#define ipRouteDefaultWeight 	pIpRouteVars->_ipRouteDefaultWeight
#define rtmodified		pIpRouteVars->_rtmodified
#define ipRouteWalkDeleteFlag   pIpRouteVars->ipRouteWalkDeleteFlag
#define ipRouteUsageTbl	        pIpRouteVars->ipRouteUsageTbl
#define ipRouteLockSem	        pIpRouteVars->ipRouteLockSem
#define ipRouteStackGoingDown	pIpRouteVars->ipRouteStackGoingDown



#ifdef DEBUG
#define ipRouteLibLogLevel	pIpRouteVars->_ipRouteLibLogLevel
#endif /* DEBUG */

#ifdef WV_INSTRUMENTATION
#ifdef INCLUDE_WVNET
#define	rtWvNetLocalFilter	pIpRouteVars->_rtWvNetLocalFilter
#define	rtWvNetEventId		pIpRouteVars->_rtWvNetEventId
#endif /* INCLUDE_WVNET */
#endif /* WV_INSTRUMENTATION */


/* definitions from route.c */

#define _route_cb		pIpRouteVars->_route_cb
#define	_rtstat			pIpRouteVars->_rtstat
#define _mrt_ioctl              pIpRouteVars->_mrt_ioctl
/* definitions from rtsock.c */

#define VS_RTSOCK_DATA (&(vsTbl[myStackNum]->rtsockGlobals))

#define routedomain		VS_RTSOCK_DATA->routedomain
#define pRtIfAnnounceMsgHook	VS_RTSOCK_DATA->pRtIfAnnounceMsgHook
#define pRtIfMsgHook		VS_RTSOCK_DATA->pRtIfMsgHook
#define pRtNewMaddrMsgHook	VS_RTSOCK_DATA->pRtNewMaddrMsgHook
#define pRtNewAddrMsgHook	VS_RTSOCK_DATA->pRtNewAddrMsgHook
#define pRtMissMsgHook		VS_RTSOCK_DATA->pRtMissMsgHook
#define pRtMsg1			VS_RTSOCK_DATA->pRtMsg1
#define pRtAddrInfoMsgHook	VS_RTSOCK_DATA->pRtAddrInfoMsgHook

#ifdef __cplusplus
}
#endif

#endif /* _INCvsIpRouteH */
