/* bpfDrv.h - include file for Berkeley Packet Filter (BPF) I/O interface */

/* Copyright 1999 - 2002 Wind River Systems, Inc. */

/*
modification history
--------------------
01g,20nov03,niq  Remove copyright_wrs.h file inclusion
01f,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01e,04nov03,rlm  Ran batch header path update for header re-org.
01d,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01c,01jul03,ann  dummy check-in to update the time-stamp on this file 
01b,13feb02,ham  changed for tornado build.
01a,15oct01,nee  Ported from from bpfdrv.h@@/main/tor3_x/tor3_x.windnet_ipv6/0 
*/

#ifndef __INCbpfDrvh
#define __INCbpfDrvh

#ifdef __cplusplus
extern "C" {
#endif

#include <net/bpf.h>

/* function declarations */

#if defined (__STDC__) || defined(__cplusplus)

extern STATUS bpfDrv (void);
extern STATUS bpfDevCreate (char *pDevName, int numUnits, int bufSize);
extern STATUS bpfDevDelete (char *pDevName);
extern STATUS bpfDrvRemove (void);
#else	/* __STDC__ */

extern STATUS bpfDrv ();
extern STATUS bpfDevCreate ();
extern STATUS bpfDevDelete ();
extern STATUS bpfDrvRemove ();
#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCbpfDrvh */


