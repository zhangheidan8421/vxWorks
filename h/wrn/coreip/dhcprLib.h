/* dhcprLib.h - DHCP relay agent include file for user interface */

/* Copyright 1996-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01l,22jul05,ijm  dhcprLib cannot be compiled on its own, SPR#109629
01k,20nov03,niq  Remove copyright_wrs.h file inclusion
01j,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01i,04nov03,rlm  Ran batch header path update for header re-org.
01h,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01g,15sep03,vvv  updated path for new headers
01f,06aug03,nee  Accordion Base6 merge from ACCORDION_BASE6_MERGE_BASELINE
                 label
01e,10jun03,vvv  include netVersion.h
01d,04dec97,spm  added code review modifications
01c,06aug97,spm  added definitions for C++ compilation
01b,28apr97,spm  moved DHCP_MAX_HOPS to configAll.h
01a,07apr97,spm  created.
*/

#ifndef __INCdhcprLibh
#define __INCdhcprLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <netVersion.h>
#include <netinet/in.h>
#include <ioLib.h>

#if CPU_FAMILY==I960
#pragma align 1                 /* tell gcc960 not to optimize alignments */
#endif  /* CPU_FAMILY==I960 */

typedef struct serverDesc
    {
    char *pAddress;             /* IP address of DHCP server target */
    } DHCP_TARGET_DESC;

typedef struct server 
    {
    struct in_addr ip;
    struct server *next;
    } DHCP_SERVER_DESC;
    
typedef struct ifrDesc
    {
    char ifName[MAX_FILENAME_LENGTH];	/* name of interface to use */
    } DHCPR_IF_DESC;    

IMPORT DHCP_TARGET_DESC * 	pDhcpRelayTargetTbl;
IMPORT int dhcpNumTargets;
IMPORT DHCP_SERVER_DESC * 	pDhcpTargetList;
IMPORT struct msg dhcpMsgIn;

#if CPU_FAMILY==I960
#pragma align 0                 /* turn off alignment requirement */
#endif  /* CPU_FAMILY==I960 */

#include <cfgDefs.h>
#ifdef VIRTUAL_STACK
#include <netinet/vsLib.h>
#endif  /* VIRTUAL_STACK */

typedef struct dhcpr_config_params
    {
    CFG_DATA_HDR cfgh;
    DHCP_TARGET_DESC *cfg_dhcpTargetTbl;
    int cfg_dhcpTargetTblSize;
    DHCPR_IF_DESC *cfg_dhcprIfTbl;		/* ptr to device table */
    int cfg_numDev;				/* # of devices in table */
    int cfg_dhcpCPort;
    int cfg_dhcpSPort;
    int cfg_dhcpMaxHops;
    int cfg_dhcpsMaxMsgSize;
    } DHCPR_CONFIG_PARAMS;

IMPORT int open_if();
IMPORT void read_server_db (int);
#ifdef VIRTUAL_STACK
IMPORT STATUS dhcprDestructor (VSNUM vsnum);
#endif /* VIRTUAL_STACK */

#ifdef __cplusplus
}
#endif

#endif
