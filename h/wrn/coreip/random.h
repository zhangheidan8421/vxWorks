/* random.h - a strong random number generator */

/* Copyright 2001-2004 Wind River Systems, Inc. */

/*
 * random.h -- A strong random number generator
 *
 * $FreeBSD: src/sys/sys/random.h,v 1.19.2.1 2000/05/10 02:04:52 obrien Exp $
 *
 * Version 0.95, last modified 18-Oct-95
 * 
 * Copyright Theodore Ts'o, 1994, 1995.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, and the entire permission notice in its entirety,
 *    including the disclaimer of warranties.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * 
 * ALTERNATIVELY, this product may be distributed under the terms of
 * the GNU Public License, in which case the provisions of the GPL are
 * required INSTEAD OF the above restrictions.  (This clause is
 * necessary due to a potential bad interaction between the GPL and
 * the restrictions contained in a BSD-style copyright.)
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
modification history
--------------------
01g,20aug04,dlk  Change return type of random() to u_long.
01f,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01e,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
01d,20nov03,niq  Remove copyright_wrs.h file inclusion
01c,29apr03,syy  Added #ifdef for __cplusplus
01b,04oct01,pas  added global random seed
01a,26sep01,pas  created from FreeBSD4.3-RELEASE(July-17-01).
*/

/*
 * Many kernel routines will have a use for good random numbers,
 * for example, for truely random TCP sequence numbers, which prevent
 * certain forms of TCP spoofing attacks.
 * 
 */

#ifndef	__INCrandomh
#define	__INCrandomh

#ifdef __cplusplus
extern "C" {
#endif

/* Exported functions */

void rand_initialize(void);

#ifdef notused
void get_random_bytes(void *buf, u_int nbytes);
u_int write_random(const char *buf, u_int nbytes);
#endif
u_int read_random(void *buf, u_int size);
u_int read_random_unlimited(void *buf, u_int size);
struct proc;
int random_poll(dev_t dev, int events, struct proc *p);

/* Clarinet additions: ---------------------------------------------------- */
/* random seed, fed by checksums */
extern unsigned long clarinet_random_seed;

/* replacements for random() and srandom(), found in more modern
 * versions (4.2BSD+) of stdlib.h, but not in vxworks
 */
extern u_long random (void);
extern void srandom (unsigned long seed);

#ifdef __cplusplus
}
#endif

#endif /* !__INCrandomh */
