/* ip6Lib.h - header file for ip6Lib.c */

/* Copyright 1984-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01k,25jun05,dlk  Added cfg_ip6qmaxlen for loopback.
01j,11may05,kch  Added cfg_ip6_prefer_tempaddr and cfg_ip6_mcast_pmtu to
                 IPV6_CONFIG_PARAMS.
01i,21apr05,rp   added ip6_maxfrags
01h,07feb05,wap  Allow sysctl init routines to be scaled out
01g,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01f,04nov03,rlm  Ran batch header path update for header re-org.
01e,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01d,15sep03,vvv  updated path for new headers
01c,13aug03,niq  Merging from Accordion label ACCORDION_BASE6_MERGE_BASELINE
01b,10jun03,vvv  include netVersion.h
01a,10jun03,ann written.
*/

#ifndef __INCip6Libh
#define __INCip6Libh

#ifdef __cplusplus
extern "C" {
#endif

#include <netVersion.h>

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
#pragma align 1                 /* tell gcc960 not to optimize alignments */
#endif  /* CPU_FAMILY==I960 */

#include <cfgDefs.h>

/* ip configuration parameters */

typedef struct ipv6cfgparams
    {
    CFG_DATA_HDR 	cfgh ;
    int 		cfg_ip6_auto_linklocal ;
    int			cfg_ip6_forwarding ;	        /* act as router? 1 yes  0 no*/
    int			cfg_ip6_sendredirects ;
    int			cfg_ip6_defhlim ;
    int			cfg_ip6_defmcasthlim ;
    int			cfg_ip6_accept_rtadv ;          /* "IPV6FORWARDING ? 0 : 1" is dangerous */
    int			cfg_ip6_maxfragpackets;		/* initialized in frag6.c:frag6_init() */
    int			cfg_ip6_maxfrags;			/* initialized in frag6.c:frag6_init() */
    int			cfg_ip6_log_interval ;
    int			cfg_ip6_hdrnestlimit ;	         
    int			cfg_ip6_dad_count ;	        /* DupAddrDetectionTransmits */
    int			cfg_ip6_auto_flowlabel ;
    int			cfg_ip6_gif_hlim ;
    int			cfg_ip6_use_deprecated ;	/* allow deprecated addr (RFC2462 5.5.4) */
    int			cfg_ip6_rr_prune ;	        /* router renumbering prefix walk list every 5 sec.    */
    int			cfg_ip6_prefer_tempaddr;
    int			cfg_ip6_mcast_pmtu;
    int			cfg_ip6_v6only ;
    int			cfg_ip6_keepfaith ;
    int 		cfg_rtq_reallyold  ;        	/* one hour is ``really old'' */
    int 		cfg_rtq_minreallyold ;   	/* never automatically crank down to less */
    int 		cfg_rtq_toomany ;        	/* 128 cached routes is ``too many'' */
    int 		cfg_rtq_timeout ;
    struct ipRouteDispatchTable *  cfg_ipv6RtDispTable; /* v6 dispatch table */
    int			cfg_if_index_limit;
    FUNCPTR		cfg_privInitSysctl;
    int			cfg_ip6qmaxlen;
    } IPV6_CONFIG_PARAMS;
    

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)


extern STATUS   ipv6InstInit (void * ip6Cfg);
#ifdef VIRTUAL_STACK
extern STATUS   ipv6Destructor (VSNUM vsnum);
#endif
extern void init_ip6round ();
extern void init_lastadjustedtimeout ();


#else   /* __STDC__ */


extern STATUS   ipv6InstInit ();
#ifdef VIRTUAL_STACK
extern STATUS   ipv6Destructor ();
#endif
extern void init_ip6round ();
extern void init_lastadjustedtimeout ();


#endif  /* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCip6Libh */

