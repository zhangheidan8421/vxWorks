/* nfs2Lib.h - Network File System library header */

/* Copyright 1984-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01b,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
01a,10oct03,snd  written                               
*/

#ifndef __INCnfs2Libh
#define __INCnfs2Libh

#ifdef __cplusplus
extern "C" {
#endif

#include "wrn/netVersion.h"
#include "vwModNum.h"
#include "dirent.h"
#include "sys/stat.h"
#include "nfsCommon.h"
#include "nfsDriver.h"
#include "iosLib.h"
#include "xdr_mnt1.h"

/* nfs file descriptor */


typedef struct                  /* NFS_FD - NFS file descriptor */
    {
    NFS_DEV     *nfsDev;                /* pointer to this file's NFS device */
    char         curFilename[NAME_MAX + 1];/* Current file name */
    nfs_fh2      fileHandle;            /* file's file handle */
    nfs_fh2      dirHandle;             /* file's directory file handle */
    fattr        fileAttr;              /* file's attributes */
    unsigned int mode;                  /* (O_RDONLY, O_WRONLY, O_RDWR) */
    unsigned int fileCurByte;           /* number of current byte in file */
    SEM_ID       nfsFdSem;              /* accessing semaphore */
    BOOL         cacheValid;            /* TRUE: cache valid and remaining */
                                        /* fields in structure are valid. */
                                        /* FALSE: cache is empty or */
                                        /* cacheCurByte not the same as the */
                                        /* current byte in the file. */
    char        *cacheBuf;              /* pointer to file's read cache */
    char        *cacheCurByte;          /* pointer to current byte in cache */
    unsigned int cacheBytesLeft;        /* number of bytes left in cache */
                                        /* between cacheCurByte and the end */
                                        /* of valid cache material.  NOTE: */
                                        /* This number may be smaller than */
                                        /* the amount of room left in for */
                                        /* the cache writing. */
    BOOL         cacheDirty;            /* TRUE: cache is dirty, not flushed */
    readdirres  *dirCache;              /* cache of dir list entries */
    } NFS_FD;


/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern void     nfs2ClientClose (void);
extern STATUS   nfs2DirMount (char *hostName, dirpath dirname,
                                nfs_fh2 *pFileHandle);

extern STATUS   nfs2DirUnmount ( char * hostName, dirpath dirName);
extern STATUS   nfs2DirReadOne (NFS_FD *pFd, DIR *pDir);
extern STATUS   nfs2ExportRead (char *hostName, exports *pExports);
extern void     nfs2FileAttrGet (fattr *pFattr, struct stat *pStat);

extern STATUS   nfs2FsAttrGet (char* pHostName,nfs_fh2* pDirHandle,
                               struct statfs* pArg );

extern int     nfs2FileRead (char *hostName, nfs_fh2 *pHandle,
                              unsigned int offset, unsigned int count,
                              char *buf, fattr *pNewAttr);

extern STATUS  nfs2FileRemove (char *hostName, nfs_fh2 *pMountHandle,
                                char *fullFileName);

extern int     nfs2FileWrite (char *hostName, nfs_fh2 *pHandle,
                               unsigned int offset, unsigned int count,
                               char *data, fattr *pNewAttr);
extern int     nfs2LookUpByName (char *hostName, char *fileName,
                                  nfs_fh2 *pMountHandle, diropres *pDirOpRes,
                                  nfs_fh2 *pDirHandle, BOOL removeFlag);
extern int     nfs2ThingCreate (char *hostName, char *fullFileName,
                                 nfs_fh2 *pMountHandle, diropres *pDirOpRes,
                                 nfs_fh2 *pDirHandle, u_int mode);
extern STATUS   nfs2MntUnmountAll (char *hostName);

extern STATUS   nfs2Rename (char * hostName, nfs_fh2 * pMntHndl,  
                            char * oldName, nfs_fh2 * pOldDirHndl, 
                            char * newName);

extern STATUS   nfs2DevInfoGet (unsigned long nfsDevHandle, 
                                NFS_DEV_INFO * pnfsInfo);

extern STATUS   nfs2MntDump (char * hostName);
extern STATUS   nfs2ExportRead (char * hostName, exports * pExports);


#else

extern STATUS   nfs2DirMount ();
extern STATUS   nfs2DirUnmount ();
extern STATUS   nfs2DirReadOne ();
extern STATUS   nfsExportFree ();
extern STATUS   nfs2ExportRead ();
extern void     nfs2FileAttrGet ();
extern STATUS   nfs2FsAttrGet ();
extern int      nfs2FileRead ();
extern STATUS   nfs2FileRemove ();
extern int      nfs2FileWrite ();
extern int      nfs2LookUpByName ();
extern int      nfs2ThingCreate ();
extern STATUS   nfs2MntUnmountAll ();
extern STATUS   nfs2Rename();
extern STATUS   nfs2DevInfoGet();
extern STATUS   nfs2MntDump();
extern STATUS   nfs2ExportRead ();
#endif    /* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCnfs2Libh */
