/* mip6_had.c - MIP6 home agent header file */

/* Copyright 2005 Wind River Systems, Inc. */


/*
modification history
--------------------
01d,01sep05,tlu  rename mip6 file name
01c,14jun05,tlu  add _cplusplus condition
01b,31may05,tlu  rename constants.h
01a,20apr05,tlu  initial creation
*/


#if !defined (_HAD_H_) 
#define _HAD_H_

#ifdef __cplusplus
extern "C" {
#endif


#include <mip6/mip6_constants.h>
#include <mip6/mip6.h>


/* HA data structure */
typedef struct HA_CLASS
{
    /* sockets for HA */
    int mipsock;  	/* an interface btw kernel/user and user/user space */
    int icmp6sock;	/* to handle new ICMPv6 messages */
    int mhsock;		/* to handle new mobile IPv6 messages */

    /* lists */
    struct mip6stat mip6stat;
    struct mip6_hpfx_list hpfx_head;

    /* home prefix */
    char homeprefix[V6ADDRPRESZ];
    int preference;

    /* task */
    TASK_DESCRIPTOR ha_tasks[1];  /* HA task */

} HA_CLASS;


STATUS ha_init();
STATUS ha_cfg();
STATUS ha_initialize_env();
STATUS ha_create_tasks();
STATUS ha_daemon_task_entry_ptr();
STATUS ha_shutdown();


#ifdef __cplusplus
}
#endif

#endif /* _HAD_H_ */
