/* mip6_stat.h - MIP6 statistics header file */

/* Copyright 2005 Wind River Systems, Inc. */

/*      $KAME: stat.h,v 1.1 2004/12/09 02:18:48 t-momose Exp $  */
/*
 * Copyright (C) 2004 WIDE Project.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
modification history
--------------------
01e,27sep05,tlu  rename mip6stat variable
01d,01sep05,tlu  rename mip6 file name
01c,09aug05,tlu  rename mip6stat to _mip6stat 
01b,14jun05,tlu  add _cplusplus condition
01a,22feb05,tlu  1st round of porting Kame MIP6
*/


#if !defined(_STAT_H_)
#define _STAT_H_

#ifdef __cplusplus
extern "C" {
#endif


struct _mip6stat {
        u_quad_t mip6s_mobility;        /* Mobility Header recieved */
        u_quad_t mip6s_omobility;       /* Mobility Header sent */
        u_quad_t mip6s_hoti;            /* HoTI recieved */
        u_quad_t mip6s_ohoti;           /* HoTI sent */
        u_quad_t mip6s_coti;            /* CoTI received */
        u_quad_t mip6s_ocoti;           /* CoTI sent */
        u_quad_t mip6s_hot;             /* HoT received */
        u_quad_t mip6s_ohot;            /* HoT sent */
        u_quad_t mip6s_cot;             /* CoT received */
        u_quad_t mip6s_ocot;            /* CoT sent */
        u_quad_t mip6s_bu;              /* BU received */
        u_quad_t mip6s_obu;             /* BU sent */
        u_quad_t mip6s_ba;              /* BA received */
        u_quad_t mip6s_ba_hist[256];    /* BA status input histgram */
        u_quad_t mip6s_oba;             /* BA sent */
        u_quad_t mip6s_oba_hist[256];   /* BA status output histgram */
        u_quad_t mip6s_br;              /* BR received */
        u_quad_t mip6s_obr;             /* BR sent */
        u_quad_t mip6s_be;              /* BE received */
        u_quad_t mip6s_be_hist[256];    /* BE status input histogram */
        u_quad_t mip6s_obe;             /* BE sent */
        u_quad_t mip6s_obe_hist[256];   /* BE status output histogram */
        u_quad_t mip6s_hao;             /* HAO received */
        u_quad_t mip6s_unverifiedhao;   /* unverified HAO received */
        u_quad_t mip6s_ohao;            /* HAO sent */
        u_quad_t mip6s_rthdr2;          /* RTHDR2 received */
        u_quad_t mip6s_orthdr2;         /* RTHDR2 sent */
        u_quad_t mip6s_revtunnel;       /* reverse tunnel input */
        u_quad_t mip6s_orevtunnel;      /* reverse tunnel output */
        u_quad_t mip6s_checksum;        /* bad checksum */
        u_quad_t mip6s_payloadproto;    /* payload proto != no nxt header */
        u_quad_t mip6s_unknowntype;     /* unknown MH type value */
        u_quad_t mip6s_nohif;           /* not my home address */
        u_quad_t mip6s_nobue;           /* no related BUE */
        u_quad_t mip6s_hinitcookie;     /* home init cookie mismatch */
        u_quad_t mip6s_cinitcookie;     /* careof init cookie mismatch */
        u_quad_t mip6s_unprotected;     /* not IPseced signaling */
        u_quad_t mip6s_haopolicy;       /* BU is discarded due to bad HAO */
        u_quad_t mip6s_rrauthfail;      /* RR authentication failed */
        u_quad_t mip6s_seqno;           /* seqno mismatch */
        u_quad_t mip6s_paramprobhao;    /* ICMP paramprob for HAO received */
        u_quad_t mip6s_paramprobmh;     /* ICMP paramprob for MH received */
        u_quad_t mip6s_invalidcoa;      /* Invalid Care-of address */
        u_quad_t mip6s_invalidopt;      /* Invalid mobility options */
        u_quad_t mip6s_circularrefered; /* Circular reference */
        u_quad_t mip6s_mhtoosmall;      /* MH too small */
};

extern struct _mip6stat _mip6stat;


#ifdef __cplusplus
}
#endif

#endif /* _STAT_H_ */
