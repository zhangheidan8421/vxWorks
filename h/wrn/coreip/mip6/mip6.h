/* mip6.h - MIP6 common data structure header file */

/* Copyright 2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01j,28sep05,xli  change parameter "mip6options" to "mip6CfgString"
01i,15aug05,xli  added mipShutdownMn as a public api 
01h,08aug05,tlu  modify the public APIs
01g,03jul05,xli  remove mip6InitializeMn()
01f,29jul05,xli  changed the mip6CfgMnDaemon and mip6CfgMnMd prototypes
01e,19jul05,xli  added configuration APIs
01d,23jun05,tlu  move in public APIs and delete errno definition
01c,14jun05,tlu  move errno definition to here and add _cplusplus condition
01b,31may05,tlu  rename constants.h
01a,20feb05,tlu  initial creation 
*/


#if !defined (_MIP6_H_) 
#define _MIP6_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <mip6/mip6_constants.h>


typedef struct TASK_DESCRIPTOR
{
    int                 task_id;
    int                 task_priority;
    MIP6_TASK_STATES    task_state;
    int                 task_stack_size;
    FUNCPTR             task_start_func;
    char                task_name[11];
} TASK_DESCRIPTOR;



/* Public APIs */
STATUS mnd(char* mip6CfgString);
STATUS mdd(char* mip6CfgString);
STATUS mip6MnDaemonShutdown();
STATUS mip6MnMdShutdown();
STATUS mip6MnShutdown();


#ifdef __cplusplus
}
#endif


#endif /* _MIP6_H_ */
