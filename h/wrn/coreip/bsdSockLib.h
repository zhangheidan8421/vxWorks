/* bsdSockLib.h -  UNIX BSD 4.4 compatible socket library header */

/*
 * Copyright (c) 2001, 2003-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
02n,05mar05,dlk  Added cfg_priv_soLingerForever.
02m,07feb05,wap  Allow sysctl init routines to be scaled out
02l,02jan04,vvv  changed prototype for bsdConnectWithTimeout
02k,08dec03,vvv  updated based on inspection comments
02j,03dec03,ann  added the inclusion of socketvar.h
02i,03dec03,ann  added the parameter names to avoid compilation errors
02h,13nov03,nee  Reewritten for base6
02g,05nov03,cdw  Removal of unnecessary _KERNEL guards.
02f,04nov03,rlm  Ran batch header path update for header re-org.
02e,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
02d,15sep03,vvv  updated path for new headers
02c,08sep03,vvv  merged from ACCORDION_BASE6_MERGE_BASELINE
02b,10jun03,vvv  include netVersion.h
02a,15aug01,ann  ported to clarinet from AE1.1 version 01f
*/

#ifndef __INCbsdSockLibh
#define __INCbsdSockLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include <netVersion.h>
#include <net/socketvar.h>
#include <sockFunc.h>
#include <cfgDefs.h>

/* externals */

#ifdef BSD43_COMPATIBLE
IMPORT BOOL 	bsdSock43ApiFlag;
#endif

/*
 * This structure contains the configuration parameters for the
 * INCLUDE_BSD_SOCKET component.
 */

typedef struct bsdSock_config_params {
    CFG_DATA_HDR         cfgh;

    /* defined in uipc_sock.c */
    int                  cfg_priv_somaxconn;

    /* defined in uipc_sock2.c */
    u_long               cfg_sb_max;                         
    u_long               cfg_priv_sb_efficiency;  /* parameter for sbreserve() */
    FUNCPTR		 cfg_privInitSysctl;
    BOOL		 cfg_priv_soLingerForever;
    } BSDSOCK_CONFIG_PARAMS;


/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern SOCK_FUNC *bsdSockLibInit (void);
extern STATUS 	bsdConnect (struct socket *so, struct sockaddr *name, int namelen);
extern STATUS 	bsdConnectWithTimeout (struct socket *so, struct sockaddr *adrs,
				    int adrsLen, struct timeval *timeVal);
extern STATUS 	bsdGetpeername (struct socket *so, struct sockaddr *name, int *namelen);
extern STATUS 	bsdGetsockname (struct socket *so, struct sockaddr *name, int *namelen);
extern STATUS 	bsdGetsockopt (struct socket *so, int level, int optname, char *optval,
			    int *optlen);
extern STATUS 	bsdSetsockopt (struct socket *so, int level, int optname, char *optval,
			    int optlen);
extern STATUS 	bsdShutdown (struct socket *so, int how);
extern int 	bsdAccept (struct socket **pSo, struct sockaddr *addr, int *addrlen);
extern int 	bsdRecv (struct socket *so, char *buf, int bufLen, int flags);
extern int 	bsdRecvfrom (struct socket *so, char *buf, int bufLen, int flags,
			  struct sockaddr *from, int *pFromLen);
extern int 	bsdRecvmsg (struct socket *so, struct msghdr *mp, int flags);
extern int 	bsdSend (struct socket *so, char *buf, int bufLen, int flags);
extern int 	bsdSendmsg (struct socket *so, struct msghdr *mp, int flags);
extern int 	bsdSendto (struct socket *so, caddr_t buf, int bufLen, int flags,
			struct sockaddr *to, int tolen);
extern STATUS   bsdZbufSockRtn (void);

#else	/* __STDC__ */

extern SOCK_FUNC *bsdSockLibInit ();
extern STATUS 	bsdConnect ();
extern STATUS 	bsdConnectWithTimeout ();
extern STATUS 	bsdGetpeername ();
extern STATUS 	bsdGetsockname ();
extern STATUS 	bsdGetsockopt ();
extern STATUS 	bsdSetsockopt ();
extern STATUS 	bsdShutdown ();
extern int 	bsdAccept ();
extern int 	bsdRecv ();
extern int 	bsdRecvfrom ();
extern int 	bsdRecvmsg ();
extern int 	bsdSend ();
extern int 	bsdSendmsg ();
extern int 	bsdSendto ();
extern STATUS   bsdZbufSockRtn ();
#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif    /* __INCbsdsocklibh */
