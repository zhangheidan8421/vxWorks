/* sockFunc.h - socket function table header */

/* Copyright 1984-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
02l,14sep04,dlk  Omit entire contents of this file unless _WRS_KERNEL is
                 defined.
02k,29jun04,dlk  Merged from BASE6_ITER5_FRZ27.
02l,28may04,dlk  Use single socket IO-system device driver with wrappers
                 calling back-end routines.
02k,24mar04,mcm  Including time.h instead of sys/time.h.
02j,15mar04,bwa  added multiple concurrent backends capabalities.
02j,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5-int/2)
02i,08dec03,vvv  updated based on inspection comments
02h,03dec03,asr  Fix compile errors after dinkum libc check-in
02g,14nov03,nee  Adding closeRtn to SOCK_FUNC
02f,05nov03,cdw  Removal of unnecessary _KERNEL guards.
02e,04nov03,rlm  Ran batch header path update for header re-org.
02d,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
02c,10jun03,vvv  include netVersion.h
02b,02feb02,ham  changed for tornado build
02a,15aug01,ann  ported to clarinet from AE1.1 version 01f
*/

#ifndef __INCsockFunch
#define __INCsockFunch

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _WRS_KERNEL

/* includes */
#include <netVersion.h>
#include <sys/times.h>
#include <sys/socket.h>

#if defined (_WRS_VXWORKS_MAJOR) && (_WRS_VXWORKS_MAJOR >= 6)
#include <ioLib.h> /* For type DEV_HDR */
#else
#include <iosLib.h>
#endif /* defined (_WRS_VXWORKS_MAJOR) && (_WRS_VXWORKS_MAJOR >= 6) */

/* HIDDEN */

/* typedefs */

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
#pragma align 1                 /* tell gcc960 not to optimize alignments */
#endif  /* CPU_FAMILY==I960 */

typedef struct sockFunc			/* SOCK_FUNC */
    {
    FUNCPTR	libInitRtn;		/* sockLibInit()	     */
    FUNCPTR	acceptRtn;		/* accept()		     */
    FUNCPTR	bindRtn;		/* bind()		     */
    FUNCPTR	connectRtn;		/* connect()		     */
    FUNCPTR	connectWithTimeoutRtn;	/* connectWithTimeout()	     */
    FUNCPTR	getpeernameRtn;		/* getpeername()	     */
    FUNCPTR	getsocknameRtn;		/* getsockname()	     */
    FUNCPTR	listenRtn;		/* listen()		     */
    FUNCPTR	recvRtn;		/* recv()		     */
    FUNCPTR	recvfromRtn;		/* recvfrom()		     */
    FUNCPTR	recvmsgRtn;		/* recvmsg()		     */
    FUNCPTR	sendRtn;		/* send()		     */
    FUNCPTR	sendtoRtn;		/* sendto()		     */
    FUNCPTR	sendmsgRtn;		/* sendmsg()		     */
    FUNCPTR	shutdownRtn;		/* shutdown()		     */
    FUNCPTR	socketRtn;		/* socket()		     */
    FUNCPTR	getsockoptRtn;		/* getsockopt()		     */
    FUNCPTR	setsockoptRtn;		/* setsockopt()		     */
    FUNCPTR     zbufRtn;		/* ZBUF support 	     */

    /*
     * The following IO-system handlers are called via wrappers in
     * sockLib.c:
     */
    FUNCPTR     closeRtn;               /* close()                   */
    FUNCPTR	readRtn;		/* read()                    */
    FUNCPTR     writeRtn;		/* write()                   */
    FUNCPTR	ioctlRtn;		/* ioctl()                   */
    } SOCK_FUNC;

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
#pragma align 0                 /* turn off alignment requirement */
#endif  /* CPU_FAMILY==I960 */

extern DEV_HDR socketDevHdr;   /* I/O system device driver header */

/* END_HIDDEN */

/* Memory validation function pointers */

extern int    (*pSockIoctlMemVal) (unsigned int cmd, void * data);
extern STATUS (*pUnixIoctlMemVal) (unsigned int cmd, const void * pData);

#endif /* _WRS_KERNEL */

#ifdef __cplusplus
}
#endif

#endif /* __INCsockFunch */

