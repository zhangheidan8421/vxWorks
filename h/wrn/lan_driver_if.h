/************************************************************************/
/*	Copyright (C) 1999 RouterWare, Inc.									*/
/*	Unpublished - rights reserved under the Copyright Laws of the		*/
/*	United States.  Use, duplication, or disclosure by the 				*/
/*	Government is subject to restrictions as set forth in 				*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 		*/
/*	Computer Software clause at 252.227-7013.							*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach, CA	*/
/************************************************************************/
#if !defined (_LAN_DRIVER_IF_H_)
#define _LAN_DRIVER_IF_H_


/****************************************************************************/
/* LAN_DRIVER		 														*/
/****************************************************************************/
typedef struct LAN_DRIVER_CALLBACK_IF
{

	bool (*fp_send_frame) (void);

}LAN_DRIVER_CALLBACK_IF;


typedef struct LAN_DRIVER_LOWER_IF
{
	bool (*fp_lan_if_receive) (UINT port_handle, BYTE *p_data, UINT packet_size);
	bool (*fp_lan_if_send)	(UINT port_handle, BYTE *p_data, UINT packet_size);

}LAN_DRIVER_LOWER_IF;

#endif /*_LAN_DRIVER_IF_H_*/