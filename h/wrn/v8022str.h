/*
 * $Log:: /Tornado/target/h/WRN/v8022str.h                                                      $
 * 
 * 2     3/23/00 6:06p Rob
 * Now uses Tornado _BYTE_ORDER predefined macro for endian-specific code.
 * 
 * 1     2/09/00 4:42p Admin
 * 
 * 2     8/26/99 7:42p Alex
 * 
 * 1     8/26/99 2:14p Alex
 * 
 * 1     5/26/99 9:53a Alex
 * 
 * 1     4/14/99 5:11p Rajive
 * Initial vesrion of Next Gen RouterWare Source code
 * 
 * 1     1/18/99 5:10p Mahesh
 * 
 * 1     1/18/99 4:24p Mahesh
 * 
 * 7     11/09/98 2:58p Rajive
 * Changed SNAP_IP_RARP_PACKET to 0x8035 from 0x0835
 * 
 * 6     8/14/98 2:18p Nishit
 * Added ETHERNET_TYPE for PPP over Ethernet
 * 
 * 5     4/30/98 1:16p Rajive
 * Overwrote with INCLUDE v4.2.1
 * 
 * 3     4/27/98 11:42a Release Engineer
 * Additions into include for L2TP and RTM modules.
 * INCLUDE v4.2.1
 * 
 * 1     2/13/98 12:18a Release Engineer
 * code cleanup, code style changes, linted, system level test
 * INCLUDE v4.2.0
 * 
 * 2     3/27/96 7:08p Ross
 * Initial check-in with source safe.
*/
/*	$Modname: v8022str.h$  $version: 1.9$      $date: 02/02/94$   */
/*
* 	$lgb$
1.0 08/05/92 ross
1.1 08/05/92 ross
1.2 09/03/92 ross
1.3 01/17/93 ross
1.4 01/23/93 ross added CISCO snap ids.
1.5 01/31/93 ross added some more SAP types.
1.6 10/11/93 ross added more pack pragmas.
1.7 10/11/93 ross changed 92 to 93.
1.8 12/03/93 ross fixes for lsl version 3.0
1.9 02/02/94 ross
* 	$lge$
*/
/************************************************************************/
/*	Copyright (C) 1989 - 1998 RouterWare, Inc										*/
/*	Unpublished - rights reserved under the Copyright Laws of the			*/
/*	United States.  Use, duplication, or disclosure by the 					*/
/*	Government is subject to restrictions as set forth in 					*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 			*/
/*	Computer Software clause at 252.227-7013.										*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach Ca	*/
/************************************************************************/
#ifndef _V8022STR_H_
#define _V8022STR_H_

#define MAC_HEADER_FILE

#if (_BYTE_ORDER == _BIG_ENDIAN)
	#define	BIG_ENDIAN_CONSTANT(ushort) ushort
	#define host_to_net_short(a) (a)
	#define net_to_host_short(a) (a)
	#define host_to_net_long(a) (a)
	#define net_to_host_long(a) (a)
	#define	swap(a) (a)
	#define	swap_long(a) (a)
#else
	#define	BIG_ENDIAN_CONSTANT(ushort) (((0x00ff & ushort) << 8) | (ushort >> 8))
#endif

#define MAXIMUM_ETHERNET_DATA_SIZE 1500
#define MINIMUM_ETHERNET_DATA_SIZE 46

#if !defined (RX_PACKET_STATE_DEFINED)

	#define RX_PACKET_STATE_DEFINED

	enum	RX_PACKET_STATE
	{
		PACKET_NOT_RECOGNIZED,
		PACKET_RECOGNIZED_BUT_NOT_FORWARDED,
		PACKET_RECOGNIZED_AND_FORWARDED
	};

#endif

enum SNAP_PROTOCOL_ID
{
	SNAP_IPX_PROTOCOL_ID = BIG_ENDIAN_CONSTANT (0x8137),

	SNAP_STP_BPDU_PACKET = BIG_ENDIAN_CONSTANT (0x000e),
	SNAP_8023_BRIDGED_PACKET_WITH_CRC = BIG_ENDIAN_CONSTANT (0x0001),
	SNAP_8023_BRIDGED_PACKET_WITHOUT_CRC = BIG_ENDIAN_CONSTANT (0x0007),
	SNAP_8025_BRIDGED_PACKET_WITH_CRC = BIG_ENDIAN_CONSTANT (0x0003),
	SNAP_8025_BRIDGED_PACKET_WITHOUT_CRC = BIG_ENDIAN_CONSTANT (0x0009),

	SNAP_IP_ARP_PACKET = BIG_ENDIAN_CONSTANT (0x0806),
	SNAP_IP_RARP_PACKET = BIG_ENDIAN_CONSTANT (0x8035),
	SNAP_IP_PACKET = BIG_ENDIAN_CONSTANT (0x0800),
	SNAP_IP_FRAGMENT_PACKET = BIG_ENDIAN_CONSTANT (0x000d),

	SNAP_XNS_PROTOCOL_ID = BIG_ENDIAN_CONSTANT (0x0600),
	SNAP_APPLETALK_PROTOCOL_ID = BIG_ENDIAN_CONSTANT (0x809b),
	SNAP_APPLETALK_AARP_PROTOCOL_ID = BIG_ENDIAN_CONSTANT (0x80f3),
	SNAP_CISCO_BPDU_PROTOCOL_ID = BIG_ENDIAN_CONSTANT (0x4242),
	SNAP_CISCO_BRIDGED_DATA = BIG_ENDIAN_CONSTANT (0x6558),
	SNAP_DECNET = BIG_ENDIAN_CONSTANT (0x6003),
	SNAP_ISO = BIG_ENDIAN_CONSTANT (0xfefe),

	SNAP_VINES_IP_PACKET = BIG_ENDIAN_CONSTANT (0x0bad),
	SNAP_VINES_ECHO_PACKET = BIG_ENDIAN_CONSTANT (0x0baf)
};

enum LLC_FRAME_TYPE
{
	INFORMATION_FRAME,
	UNNUMBERED_INFORMATION = 3,
	RECEIVER_READY,
	DISCONNECT,
	RECEIVER_NOT_READY,
	UNNUMBERED_ACKNOWLEDGE,
	REJECT,
	FRAME_REJECT,
	SET_ASYNCHRONOUS_BALANCE_MODE,
	DISCONNECT_MODE,
	EXCHANGE_IDENTIFICATION,
	LINK_TEST_FRAME
};

enum	LLC_SUBLAYER_MANAGEMENT
{
	INDIVIDUAL_LLC_SUBLAYER_MANAGEMENT = 2,
	GROUP_LLC_SUBLAYER_MANAGEMENT = 3
};

enum SAP_TYPES
{
	LSAP_SNAP_LLC = 0xaa,
	LSAP_BRIDGE_SPANNING_TREE_PROTOCOL = 0x42,
	NOVELL_SAP_FOR_IPX = 0xe0,
	SNA_SAP = 0x04,
	SNA_SAP_1 = 0x08,
	SNA_SAP_2 = 0x0c,
	NETBIOS_SAP = 0xf0
};

enum	ETHERNET_TYPE
{
	IP_TYPE 							= 0x0800, 				/* 2048 */
	ARP_TYPE 						= 0x0806, 				/* 2054 */
	VINES_IP_TYPE 					= 0x0bad,
	VINES_ECHO_TYPE 				= 0x0baf,
	APPLETALK_PHASE_1_DDP_TYPE = 0x809b,
	APPLETALK_PHASE_1_ARP_TYPE = 0x80f3,
	IPX_SPX_TYPE 					= 0x8137,
	PPP_OVER_ETHERNET_DISCOVERY_TYPE = 0x8863,
	PPP_OVER_ETHERNET_DATA_TYPE = 0x8864
};

typedef	struct	HDLC_CONTROL
{
#if (_BYTE_ORDER == _BIG_ENDIAN)
	unsigned	zero_or_one:1;
	unsigned	tx_sequence_or_function:3;
	unsigned	poll_or_final:1;
	unsigned	rx_sequence_or_function:3;
#else
	unsigned	rx_sequence_or_function:3;
	unsigned	poll_or_final:1;
	unsigned	tx_sequence_or_function:3;
	unsigned	zero_or_one:1;
#endif
} HDLC_CONTROL;

typedef _struct LLC_HEADER
{
	BYTE_ENUM (SAP_TYPES)			destination_address_DSAP;
	BYTE_ENUM (SAP_TYPES)			source_address_SSAP;
	BYTE_ENUM (LLC_FRAME_TYPE)		llc_frame_type;
} _pack LLC_HEADER;

typedef	_struct SNAP_HEADER
{
	BYTE										destination_address_DSAP; /* 0xaa */
	BYTE										source_address_SSAP;		  /* 0xaa */
	
	BYTE_ENUM (LLC_FRAME_TYPE)			llc_frame_type;		  /* 0x03 UNNUMBERED_INFORMATION */

	BYTE										protocol_id_filler[3];
	USHORT_ENUM (SNAP_PROTOCOL_ID)	protocol_id;
} _pack SNAP_HEADER;

typedef union UNION_SNAP_OR_LLC_HEADER
{
	SNAP_HEADER	snap;
	LLC_HEADER	llc;
} UNION_SNAP_OR_LLC_HEADER;

typedef	_struct MAC_ADDRESS
{
	ULONG		_ulong;
	USHORT	_ushort;
} _pack MAC_ADDRESS;

#if (_BYTE_ORDER == _BIG_ENDIAN)
	typedef	_struct ETHERNET_BIT_MAC_ADDRESS
	{
		unsigned	int	ls_address:6;
		unsigned	int	local_address:1;
		unsigned	int	group_address:1;

		BYTE		ms_address[5];
	} _pack ETHERNET_BIT_MAC_ADDRESS;

	typedef	_struct	TOKEN_RING_BIT_MAC_ADDRESS
	{
		unsigned	int	group_address:1;
		unsigned	int	local_address:1;
		unsigned	int	ls_address:6;

		BYTE		ms_address[5];
	} _pack TOKEN_RING_BIT_MAC_ADDRESS;
#else
	typedef	_struct ETHERNET_BIT_MAC_ADDRESS
	{
		unsigned	int	group_address:1;
		unsigned	int	local_address:1;
		unsigned	int	ls_address:6;

		BYTE		ms_address[5];
	} _pack ETHERNET_BIT_MAC_ADDRESS;

	typedef	_struct	TOKEN_RING_BIT_MAC_ADDRESS
	{
		unsigned	int	ls_address:6;
		unsigned	int	local_address:1;
		unsigned	int	group_address:1;

		BYTE		ms_address[5];
	} _pack TOKEN_RING_BIT_MAC_ADDRESS;
#endif

typedef	union UNION_BIT_MAC_ADDRESS
{
	MAC_ADDRESS						mac_address;
	TOKEN_RING_BIT_MAC_ADDRESS	token_ring_bit;
	ETHERNET_BIT_MAC_ADDRESS	ethernet_bit;
} UNION_BIT_MAC_ADDRESS;

#if !defined (ETHERNET_HEADER_FILE)

	typedef	union UNION_ETHERNET_LENGTH_OR_TYPE_FIELD
	{
		USHORT	length;
		USHORT	type;
	} UNION_ETHERNET_LENGTH_OR_TYPE_FIELD;

	typedef	_struct ETHERNET_MAC_HEADER
	{
		MAC_ADDRESS										destination_address;
		MAC_ADDRESS										source_address;
		UNION_ETHERNET_LENGTH_OR_TYPE_FIELD		length_or_type;
	} _pack ETHERNET_MAC_HEADER;

#endif

#if !defined (TOKEN_RING_HEADER_FILE)
	typedef	_struct	TOKEN_RING_MAC_HEADER
	{
		BYTE			access_control;
		BYTE			frame_control;

		MAC_ADDRESS		destination_address;
		MAC_ADDRESS		source_address;
	} _pack TOKEN_RING_MAC_HEADER;
#endif

typedef union UNION_MAC_HEADER
{
	ETHERNET_MAC_HEADER		ethernet;
	TOKEN_RING_MAC_HEADER	token_ring;
} UNION_MAC_HEADER;

typedef	_struct FRAME
{
	UNION_MAC_HEADER			mac_header;
	UNION_SNAP_OR_LLC_HEADER	snap_or_llc_header;

	BYTE						data[1];
} _pack FRAME;

typedef	_struct MAC_HEADER
{
		MAC_ADDRESS		destination_address;
		MAC_ADDRESS		source_address;

		USHORT			length;
} _pack MAC_HEADER;


#endif /* _V8022STR_H_ */
