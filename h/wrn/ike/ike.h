/* ike.h - */

/* Copyright 2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,11may05,djp  Added COOKIE_LEN define
01d,06may05,djp  Added ikeUsrAppInit()
01c,29apr05,rlm  Added missing #include <vxWorks.h>
01b,26apr05,rlm  Moved to new header file ike.h
01a,21jun04,hms  Created.
*/

#ifndef __IKE_H
#define __IKE_H

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif


/*
DESCRIPTION

INCLUDE FILES:
*/

/* includes */
#include <vxWorks.h>

/* defines */

#define COOKIE_LEN  (8)  /* Length of cookie associated with IKE session */

/* typedefs */

/* public functions */

void ikeUsrAppInit(void);

STATUS ikeSetXform
    (
    char * configuration_string
    );

STATUS ikeSetProp
    (
    char * configuration_string
    );

STATUS ikeSetPropAttrib
    (
    char * configuration_string
    );

STATUS ikeAddPeerAuth
    (
    char * configuration_string
    );

STATUS ikeSetIfAddr
    (
    char * configuration_string
    );

STATUS ikeSetRetransmissionTimeouts
    (
    int n_retries,
    int t_ms
    );

/* ike_tsi_show.c */
STATUS ikeShowXform
    (
    char * argument_string
    );

STATUS ikeShowProp
    (
    char * argument_string
    );

STATUS ikeShowPeerAuth
    (
    char * argument_string
    );

STATUS ikeShowPSAll (void);

STATUS ikeShowPS
    (
    char * argument_string
    );

STATUS ikeShowSessionsAll (void);

STATUS ikeShowSessions
    (
    char * argument_string
    );

STATUS ikePsShow (void);

int ikeGetRetransmissionNumRetries (void);

int ikeGetRetransmissionTime (void);

/* ike_tsi_misc.c */
int ikeMon
    (
    char * action_string
    );

int ikeDebug
    (
    char * action_string
    );

int ikeInitialContact
    (
    char * action_string
    );

STATUS ikeDeletePeerAuth
    (
    char * argument_string
    );

STATUS ikeOpenSession
    (
    char * argument_string
    );

STATUS ikeCloseSessions
    (
    char * argument_string
    );

STATUS ikeBuildPS
    (
    char * argument_string
    );

STATUS ikeTeardownPS
    (
    char * argument_string
    );

STATUS ikeRenewPS
    (
    char * argument_string
    );

void ikeTrafficInitiatedNegotiationSet
    (
    BOOL enable
    );

BOOL ikeTrafficInitiatedNegotiationGet(void);

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif /* __cplusplus */

#endif /* __IKE_H */

