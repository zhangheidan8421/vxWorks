/* ike_callbacks.h - definitions for IKE phase 1 and 2 callback hooks */

/* 
 * Copyright (c) 2004-2005 Wind River Systems, Inc. 
 * 
 * The right to copy, distribute, modify or otherwise make use 
 * of this software may be licensed only pursuant to the terms 
 * of an applicable Wind River license agreement. 
 */

/*
modification history
--------------------
01i,10may05,djp  Modified to support RTPs
01h,13apr05,djp  Fixed include paths
01g,30mar05,djp  added ikeCallbacksInit and ikeCallbacksShutdown
01f,30nov04,jfb  Beautified again
01e,29nov04,jfb  Beautified
01d,24jun04,rlm  Modified IKE_PS_CALLBACK_DATA to include ike_spi_group
                 sub-struct directly, rather than via a pointer.
01c,22jun04,hms  added cookies to ike session callbacks and SPIs to PS
                 callbacks. created structs for passed in callback parms.
01b,27apr04,rlm  Added support for PS renewal callback.   
01a,16apr04,hms  created
*/

#if !defined (__IKE_CALLBACKS_H)
#define __IKE_CALLBACKS_H

#if defined(__cplusplus) || defined(c_plusplus)
extern "C"
    {
#endif

/*
DESCRIPTION
   
This header contains definitions for the callback functions available to
catch significant events in IKE negotiation. IKE phase 1 and phase 2
can optionally be configured to call user-specified callback functions upon
completion (either successfully or unsuccessfully).

INCLUDE FILES:
*/

/* includes */
#include <vxWorks.h>
#include <semLib.h>
#include <wrn/rwos/rw_util.h>

/* defines */

/* typedefs */

#if (STACK_NAME == STACK_NAME_V4_V6) && defined (INET6)
#define IKE_CALLBACK_ADDR_LEN 16
#else
#define IKE_CALLBACK_ADDR_LEN 4
#endif

/* SPI is useful for retrieving statistics associated with a Protection Suite */

typedef struct IKE_SPI_GROUP
    {
    ULONG inbound_ah_spi;
    ULONG inbound_esp_spi;
    ULONG outbound_ah_spi;
    ULONG outbound_esp_spi;
    } IKE_SPI_GROUP;

/* Cookie information is useful for retrieving statistics associated with an IKE session */
typedef struct IKE_SESSION_CALLBACK_DATA
    {
    u_char sa_family;
    BYTE dest_address[IKE_CALLBACK_ADDR_LEN];
    BYTE source_address[IKE_CALLBACK_ADDR_LEN];
    BYTE initiator_cookie[8];
    BYTE responder_cookie[8];
    STATUS status;
    } IKE_SESSION_CALLBACK_DATA;

typedef struct IKE_PS_CALLBACK_DATA
    {
    u_char sa_family;
    BYTE dest_address[IKE_CALLBACK_ADDR_LEN];
    BYTE source_address[IKE_CALLBACK_ADDR_LEN];
    unsigned short dest_port;
    unsigned short source_port;
    int protocol;
    BOOL local_initiated;
    int suite_handle;
    IKE_SPI_GROUP spi_group;
    STATUS status;
    } IKE_PS_CALLBACK_DATA;

typedef void (*FP_IKE_SESSION_EVENT) (IKE_SESSION_CALLBACK_DATA *callback_data);

typedef void (*FP_IKE_PS_EVENT) (IKE_PS_CALLBACK_DATA *callback_data);

/* public prototypes */

STATUS ikeCallbacksInit(void);
void   ikeCallbacksShutdown(void);

STATUS ikeSessionCallbacksSet 
    (
    FP_IKE_SESSION_EVENT fp_session_opened,
    FP_IKE_SESSION_EVENT fp_session_closed
    );

STATUS ikeSessionCallbacksClear (void);

STATUS ikePSCallbacksSet 
    (
    FP_IKE_PS_EVENT fp_ps_built,
    FP_IKE_PS_EVENT fp_ps_torndown,
    FP_IKE_PS_EVENT fp_ps_renewed
    );

STATUS ikePSCallbacksClear (void);

#if defined(__cplusplus) || defined(c_plusplus)
    }
#endif /* __cplusplus */
#endif /* __IKE_CALLBACKS_H */
/* EOF */
