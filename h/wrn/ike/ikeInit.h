/************************************************************************/
/* Copyright 2000-2005 Wind River Systems, Inc.                         */
/************************************************************************/

/* ikeInit.h - header file for ike_init.c */

/*
modification history
--------------------
01c,06may05,rlm  Added prototype for usrIkeInit() configlette function.
01b,30nov04,jfb  Beautified again
01a,29nov04,jfb  Beautified

01b,11june01,tkp  pompeii enhancements.
01a,21june00,aos  created.

*/

#if !defined (__IKE_INIT_H__)
#define __IKE_INIT_H__

#ifdef __cplusplus
extern "C"
    {
#endif

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
#pragma align 1 /* tell gcc960 not to optimize alignments */
#endif          /* CPU_FAMILY==I960 */

/* ike configuration parameters */
typedef struct ikecfgparams
    {
    unsigned int task_priority;
    } IKE_CFG_PARAMS;

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
  #pragma align 0 /* turn off alignment requirement */
#endif          /* CPU_FAMILY==I960 */

#ifndef IKE_AUTO_START
  #define IKE_AUTO_START 1
#endif /* IKE_AUTO_START */

#ifndef IKE_TASK_PRIORITY
  #define IKE_TASK_PRIORITY 75
#endif /* IKE_TASK_PRIORITY */

/* function declarations */

extern STATUS ikeInit
    (
    char         * ikeCfg,
    unsigned int   task_priority
    );

STATUS usrIkeInit
    (
    int auto_start
    );


#ifdef __cplusplus
    }
#endif

#endif /* __IKE_INIT_H__ */
