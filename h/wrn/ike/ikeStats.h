/* ikeStats.h - WindNet IPsec and IKE: IKE Statistics Public Header File */

/* Copyright 2000-2004 Wind River Systems, Inc. 						*/

/*
modification history
-------------------------------------
01f,11may05,djp  Removed COOKIE_LEN definitions (provided by ike.h)
01e,30nov04,jfb  Beautified again
01d,29nov04,jfb  Beautified
01c,28jul04,cdw  Added initialized field to counts struct
01b,21may04,djp  Revised IKE stats
01a,05may04,djp  Initial version of IKE Statistics header file
*/

/*
DESCRIPTION

INCLUDE FILES: vxWorks.h

IPsec and IKE Statistics Header File. Provides structures and API's for 
retrieving counts associated with IPsec and IKE. 
*/

#if !defined (__IKESTATS_H__)
#define __IKESTATS_H__

#include <vxWorks.h>
#include <wrn/rwos/rw_util.h> /* import BYTE        */
#include <wrn/ike/ike.h>      /* import COOKIE_LEN  */

#ifdef INCLUDE_COUNTERS_IKE
#define COUNTS_STRUCT_INITIALIZED (0xfa11deed)

#ifdef __cplusplus
extern "C"
    {
#endif
    /* IKE_DIRECTIONAL_COUNTS
     *
     * This structure maintains unidirectional (either inbound or outbound) byte
     * and packet counts associated with the IKE statistics counters. The fields
     * are defined as follows:
     *
     * preprocessedByteCount     - Provides the accumulated preprocessed byte count
     *                             of all packets that were successfully processed.
     *                             For inbound traffic, this refers to the byte
     *                             count for packets prior to processing. For 
     *                             encrypted packets, this number will refer to the
     *                             encrypted packet size or more generally, the
     *                             "byte count on the wire". For outbound traffic,
     *                             this count refers to the byte count prior to 
     *                             final processing of the data. For encrypted
     *                             packets, this number will refer to the packet
     *                             size prior to encryption.
     * postprocessedByteCount    - Provides the accumulated postprocessed byte count
     *                             of all packets that were successfully processed.
     *                             For inbound traffic, this refers to the byte
     *                             count for packets after processing. For 
     *                             encrypted packets, this number will refer to the
     *                             decrypted packet size or more generally, the
     *                             size of the "data payload". For outbound traffic,
     *                             this count refers to the byte count after final 
     *                             processing of the data. For encrypted
     *                             packets, this number will refer to the packet
     *                             size after encryption or more generally the 
     *                             "byte count on the wire".
     * failedProcessingByteCount - Provides the accumulated byte count for all
     *                             packets that failed processing. This number will 
     *                             correspond to a "preprocessed" byte count. Thus,
     *                             for inbound packets, this will be a "data on the
     *                             wire" count, whereas for outbound traffic, this
     *                             will be an internal byte count.
     * packetCount               - Provides the total number of packets processed
     *                             in the direction of interest.
     *
     * Thus, for traffic between two nodes (A and B), the inbound and outbound byte
     * counts should match as follows:
     *     A inbound preprocessedByteCount   = B outbound postprocessedByteCount
     *     A outbound postprocessedByteCount = B inbound preprocessedByteCount
     */

    typedef struct ike_directional_counts
        {
        UINT64 preprocessedByteCount;     /* preprocessed byte count of successfully processed packets */
        UINT64 postprocessedByteCount;    /* postprocessed byte count of successfully processed packets*/
        UINT64 failedProcessingByteCount; /* byte count of unsuccessfully processed packets            */
        UINT32 packetCount;               /* No. of packets processed in negotiations                  */
        } IKE_DIRECTIONAL_COUNTS;

    /* IKE_COUNTS
     *
     * This structure maintains byte, packet, and error counts for both IKE peer 
     * and IKE SA statistics. All messages between the applicable
     * peer/SA are maintained here including Phase 1 and Phase 2 messages.
     * The additional fields are inbound counters only.
     */
    typedef struct ike_counts
        {
        IKE_DIRECTIONAL_COUNTS inbound;  /* inbound byte and packet counters  */
        IKE_DIRECTIONAL_COUNTS outbound; /* outbound byte and packet counters */
        UINT32 decryptionErrors;         /* No. of decryption errors (inbound)      */
        UINT32 authErrors;               /* No. of authentication errors (inbound)  */
        UINT32 proposalErrors;           /* No. of proposal related errors (inbound)*/
        UINT32 packetErrors;             /* No. of packets rcvd with errors         */
        UINT64 initialized;              /* initialized counts structure */
        } IKE_COUNTS;

    /* The following definitions provide API's for retrieving statistics associated
     * with IPsec and IKE
     */

    /* IKE_PEER_STATS
     *
     * This structure is used to retrieve statistics associated with an IKE peer.
     * A peer is identified by it's IP address. All messages between the applicable
     * peer are maintained here including Phase 1 and Phase 2 messages.
     */

    typedef struct ike_peer_stats
        {
        IKE_COUNTS ikeCounts;
        UINT32 phase1SuccessCount; /* No. of successful Phase 1 negotiations */
        UINT32 phase1FailureCount; /* No. of failed Phase 1 negotiations     */
        UINT32 phase2SuccessCount; /* No. of successful Phase 2 negotiations */
        UINT32 phase2FailureCount; /* No. of failed Phase 2 negotiations     */
        UINT32 noIKEPolicyErrors;  /* No. of IKE msg rx from Peer with no IKE Policy   */
        } IKE_PEER_STATS;

    /* IKE_SA_STATS
     *
     * This structure is used to retrieve statistics associated with an IKE 
     * Security Association (or IKE Channel). An IKE SA is created once Phase 1 
     * Negotiations are successfully completed. Statistics identified here refer 
     * to messages associated with Phase 2 negotiations only. Note that accumulated
     * statistics will also be retrievable via the IKEPeerStats structure.
     */

        #define MAX_XFORM_NAME_LEN  (256)

    typedef struct ike_sa_stats
        {
        IKE_COUNTS ikeCounts;
        UINT32 phase2SuccessCount;             /* No. of successful Phase 2 negotiations   */
        UINT32 phase2FailureCount;             /* No. of failed Phase 2 negotiations       */
        UINT32 upTime;                         /* Time since SA was created (in seconds)   */
        UINT32 timeToLive;                     /* Time until SA is destroyed (in seconds)  */
        UINT32 ipsecPSTotalCount;              /* No. of IPsec SA's during IKE SA lifetime */
        UINT32 ipsecPSCurrentCount;            /* No. of currently active IPsec SA's       */
        BYTE initiatorCookie[COOKIE_LEN];      /* Cookie associated with initiator */
        BYTE responderCookie[COOKIE_LEN];      /* Cookie associated with responder */
        char ikeTransform[MAX_XFORM_NAME_LEN]; /* Name of negotiated IKE Transform */
        } IKE_SA_STATS;

    /* function declarations */

        #if defined(__STDC__) || defined(__cplusplus)

        /* The following API is used to retrieve or print IKE statistics associated 
         * with a single IKE Peer. 
         */

        extern STATUS ikePeerAuthStatsGet
            (
            char *peerAddress,
            IKE_PEER_STATS *stats
            );

        extern void ikePeerAuthStatsPrint
            (
            char *peerAddress,
            IKE_PEER_STATS *stats
            );

        extern void ikePeerAuthStatsDump
            (
            char *peerAddress
            );

        /* The following API is used to retrieve and/or print IKE statistics
         * associated with an IKE Security Association or Channel using the Peer IP
         * address as the means to identify the statistics of interest. 
         */

        extern STATUS ikeSAStatisticsGet
            (
            char *peerAddress,
            IKE_SA_STATS *stats
            );

        extern void ikeSAStatisticsPrint
            (
            char *peerAddress,
            IKE_SA_STATS *stats
            );

        extern void ikeSAStatisticsDump
            (
            char *peerAddress
            );

        /* The following prints both Peer and IKE SA statistics associated with
         * a peer address. peerAddress is as described above.
         */

        extern void ikeStatisticsDump
            (
            char *peerAddress
            );

        /* The following API is used to retrieve and/or print IKE statistics
         * associated with an IKE Security Association or Channel using the negotiated
         * cookies as the means to identify the statistics of interest. 
         */

        extern STATUS ikeSAStatisticsGetByCookie
            (
            BYTE *initiatorCookie,
            BYTE *responderCookie,
            IKE_SA_STATS *stats
            );

        extern void ikeSAStatisticsDumpByCookie
            (
            BYTE *initiatorCookie,
            BYTE *responderCookie
            );
        #else /* __STDC__ */
        extern STATUS ikePeerAuthStatsGet ();
        extern void ikePeerAuthStatsPrint ();
        extern STATUS ikePeerAuthStatsDump ();
        extern STATUS ikeSAStatisticsGet ();
        extern void ikeSAStatisticsPrint ();
        extern STATUS ikeSAStatisticsDump ();
        extern void ikeStatisticsDump ();
        extern STATUS ikeSAStatisticsGetByCookie ();
        extern void ikeSAStatisticsDumpByCookie ();
        #endif /* __STDC__ */
        #ifdef __cplusplus
    }
        #endif
#endif /* INCLUDE_COUNTERS_IKE */
#endif /* __IKESTATS_H__*/
