/* ospfCommonUtils.h - OSPF common utilities header file */

/* Copyright 2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01a,05aug03,agi created (ported from OSPFv2)
*/

#ifndef __INCospfCommonUtilsh
#define __INCospfCommonUtilsh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */
#include <stddef.h>

/* Internet address class. Note the IP address is in host order. */

/*
#if !defined (__VxWORKS__)
#if !defined (WAIT_FOREVER)
#define WAIT_FOREVER 0xffffffff
#endif 
#endif
*/

#define PARAMETER_NOT_USED(parameter_one) (parameter_one = parameter_one)

/******************************************************************************/

#if !defined BOOLEAN
typedef enum BOOLEAN
{
	 _FALSE = 0 		/* FALSE and TRUE are already defined here */
	,_TRUE = 1
} BOOLEAN;
#endif

#if !defined _struct
#define _struct struct 
#endif

#if !defined _union
#define _union union
#endif

#if !defined _enum
#define	_enum  enum		
#endif

#if !defined _pack
#define _pack
#endif

#define INVALID_HANDLE ((UINT)NULL)

#ifndef _BYTE_
#define _BYTE_
typedef unsigned char BYTE;
#endif

#if !defined (__BIG_ENUMS__)
	#define BYTE_ENUM(enum_name) enum enum_name 
#else
	#define BYTE_ENUM(enum_name) BYTE
#endif

typedef enum  TEST
{
        PASS = 1,
        FAIL = 0
} TEST;

#if !defined STRINGS_MATCH
#define STRINGS_MATCH ((int) NULL)
#endif

typedef struct  LINK
{
        struct  LINK    *sptr_forward_link;
        struct  LINK    *sptr_backward_link;
} LINK;

#define MAXIMUM_ETHERNET_DATA_SIZE 1500
#define MINIMUM_ETHERNET_DATA_SIZE 46

typedef _struct MAC_ADDRESS
{
        ULONG           _ulong;
        USHORT  _ushort;
} _pack MAC_ADDRESS;

/* High byte of word double_word.   */
#define high_byte_of_dword(double_word)  (((double_word) >> 24) & 0x000000ff)
/* High byte of word double_word.   */
#define high_low_byte_of_dword(double_word)  (((double_word) >> 16) & 0x000000ff)
/* High byte of word double_word.   */
#define low_high_of_dword(double_word)  (((double_word) >> 8) & 0x000000ff) 
/* Low byte of word double_word.   */
#define low_byte_of_dword(double_word)  ((double_word) & 0x000000ff)
/* High byte of ushort ushort.   */
#define high_byte_of_ushort(ushort)  (((ushort)>>8)&0x00ff)
/* Low byte of ushort ushort.   */
#define low_byte_of_ushort(ushort)  ((ushort)&0x00ff)
/* High byte of ushort double_ushort.   */
#define low_ushort_of_ulong(double_ushort)  (WORD)(((double_ushort) >> 16) & 0x0000ffff)
/* Low byte of ushort double_ushort.   */
#define high_ushort_of_ulong(double_ushort)  (WORD)((double_ushort) & 0x0000ffff)
#define two_bytes_to_ushort(high_byte,low_byte) ((((high_byte)&0x00ff)<<8)|((low_byte)&0x00ff))

#ifdef __cplusplus
}
#endif

#endif /* __INCospfCommonUtilsh */
