/* ospfCommonUsrMemPartLib.h - OSPF memory partition manipulation header file */

/* Copyright 2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01d,22apr04,ram AVL & Memory migration to shared library
01c,22apr04,ram AVL & Memory migration to shared library
01b,25sep03,ram Second general modifications submission
01a,05aug03,agi created (ported from OSPFv2)
*/

#ifndef __INCospfCommonMemParth
#define __INCospfCommonMemParth

#include "vxWorks.h"
#include "errno.h"
#include "memPartLib.h"

/* type definitions */
typedef ulong_t OSPF_MEM_PART_ID;

extern OSPF_MEM_PART_ID ospfMemPartCreate( size_t memSize );

extern STATUS ospfMemPartExpand( OSPF_MEM_PART_ID memPartId, size_t memSize );

extern STATUS ospfMemPartDelete( OSPF_MEM_PART_ID memPartId );

extern void *ospfMemPartAlloc( OSPF_MEM_PART_ID memPartId, size_t numBytes );

extern STATUS ospfMemPartFree( OSPF_MEM_PART_ID  memPartId, void *ptr );

extern void ospfMemPartSetOptions( OSPF_MEM_PART_ID memPartId, size_t options);

extern STATUS ospfMemPartShow( OSPF_MEM_PART_ID memPartId, int type );


#endif /* __INCospfCommonMemParth */

