/* ospfCommonMibApi.h - application neutral MIB API header file */

/* Copyright 2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01b,06aug03,agi added MAPI_COMMIT_FORCE, MAPI_GET_FORCE
01a,30jul03,agi created (ported from OSPFv2)
*/

/*
DESCRIPTION

ospfCommonibApi.h contains the common enumerations and data structures used to
implement the "application neutral MIB API" (MIB API or MAPI).
*/

#ifndef __INCospfCommonMibApih
#define __INCospfCommonMibApih

#ifdef __cplusplus
extern "C" {
#endif

/* typedefs */

/******************************************************************************
* mApiReqType_t -- MIB API Request Type typedef
*
* The 'mApiReqType_t' is used in conjunction with the MIB API function calls
* defined for each application protocol (e.g., OSPF).  Note that a single SET
* request is not supported.  The caller must make a series of requests to
* implement a "phased" SET.  Shown below are ALL allowed request combinations
* for a single object SET.  For brevity, the "MAPI_" is not included.
*  - TEST, COMMIT, COMPLETE                 -- SET succeeds
*  - TEST_FORCE, COMMIT, COMPLETE           -- SET_FORCE read-only succeeds
*  - COMMIT_NVM, COMPLETE                   -- SET_NVM succeeds
*  - TEST, COMPLETE                         -- TEST failure
*  - TEST_FORCE, COMPLETE                   -- TEST_FORCE failure
*  - TEST, COMMIT, UNDO, COMPLETE           -- COMMIT failure
*  - TEST_FORCE, COMMIT, UNDO, COMPLETE     -- COMMIT read-only failure
*  - COMMIT_NVM, UNDO, COMPLETE             -- COMMIT_NVM failure
*
* Note that a "COMPLETE" is ALWAYS required.
*/
typedef enum
    {
    MAPI_GET        = 1,    /* GET request */
    MAPI_GET_NEXT   = 2,    /* GET-NEXT request */
    MAPI_TEST       = 3,    /* TEST request     -- for SET */
    MAPI_COMMIT     = 4,    /* COMMIT request   -- for SET */
    MAPI_UNDO       = 5,    /* UNDO request     -- for SET */
    MAPI_COMPLETE   = 6,    /* COMPLETE request -- for SET */
    MAPI_TEST_FORCE = 7,    /* FORCE_SET read-only object        -- for SET */
    MAPI_COMMIT_NVM = 8,    /* NVM_SET w/o TEST/TEST_FORCE phase -- for SET */
    MAPI_COMMIT_FORCE = 8,  /* NVM_SET w/o TEST/TEST_FORCE phase -- for SET */
    MAPI_GET_NVM    = 9,    /* NVM_GET request */
    MAPI_GET_FORCE    = 9   /* NVM_GET request */
    } mApiReqType_t;

/******************************************************************************
* mApiException_t -- MIB API object 'exception' typedef
*
* Defines the 'exception' enumerations for the 'mApiObject_t' structure.
*
* Note that only GETs and GET-NEXTs support non-zero exceptions.  SETs (i.e., 
* TESTs, COMMITs, UNDOs and COMPLETEs) do not support non-zero exceptions.
*/
typedef enum
    {
    MAPI_NO_EXCEPTION       = 0,      /* no exception occurred */
    MAPI_NO_SUCH_OBJECT     = 128,    /* SNMPv2 noSuchObject exception */
    MAPI_NO_SUCH_INSTANCE   = 129,    /* SNMPv2 noSuchInstance exception */
    MAPI_END_OF_MIB_VIEW    = 130,    /* SNMPv2 endOfMibView exception */
    MAPI_BUFFER_TOO_SHORT   = 1000    /* data buffer too short exception */
    } mApiException_t;

/******************************************************************************
* mApiError_t -- MIB API request 'error' typedef
*
* This typedef defines the SNMPv2 enumerations used with the MIB API for the
* 'error' value in the 'mApiRequest_t' structure.  Note that the less specific
* SNMPv1 errors are explicitly absent and SHOULD NOT be used.
*
* The individual application protocol (e.g., OSPF) can also create its own
* enumerations to supply 'error' values for the 'mApiRequest_t' structure.
* It is recommended that these start at '100'.  These should be added to the
* application protocol include file (not to this common MIB API header file).
*/
typedef enum
    {
    /* SNMPv2 specific errors */
    MAPI_NO_ERROR               = 0,    /* SNMPv2 noError(0) */
    MAPI_ERROR_TOO_BIG          = 1,    /* SNMPv2 errorTooBig(1) */
    /* do NOT use - SNMPv1 */           /* SNMPv1 noSuchName(2) */
    /* do NOT use - SNMPv1 */           /* SNMPv1 badValue(3) */
    /* do NOT use - SNMPv1 */           /* SNMPv1 readOnly(4) */
    MAPI_GEN_ERROR              = 5,    /* SNMPv2 genError(5) */
    MAPI_NO_ACCESS              = 6,    /* SNMPv2 noAccess(6) */
    MAPI_WRONG_TYPE             = 7,    /* SNMPv2 wrongType(7) */
    MAPI_WRONG_LENGTH           = 8,    /* SNMPv2 wrongLength(8) */
    MAPI_WRONG_ENCODING         = 9,    /* SNMPv2 wrongEncoding(9) */
    MAPI_WRONG_VALUE            = 10,   /* SNMPv2 wrongValue(10) */
    MAPI_NO_CREATION            = 11,   /* SNMPv2 noCreation(11) */
    MAPI_INCONSIST_VALUE        = 12,   /* SNMPv2 inconsistValue(12) */
    MAPI_RESOURCE_UNAVAILABLE   = 13,   /* SNMPv2 resourceUnavailable(13) */
    MAPI_COMMIT_FAILED          = 14,   /* SNMPv2 commitFailed(14) */
    MAPI_UNDO_FAILED            = 15,   /* SNMPv2 undoFailed(15) */
    MAPI_AUTHORIZATION_ERROR    = 16,   /* SNMPv2 authorizationError(16) */
    MAPI_NOT_WRITABLE           = 17,   /* SNMPv2 notWritable(17) */
    MAPI_INCONSIST_NAME         = 18    /* SNMPv2 inconsistName(18) */
    } mApiError_t;

/******************************************************************************
* mApiObject_t -- MIB API Object typedef
*
* This structure contains information for a single MIB object and is used in
* conjunction with the 'mApiRequest_t' structure.  When making the call,
* 'valueLen' contains the allocated byte length of the buffer.  When returned,
* 'valueLen' contains the actual byte size of the data.  The 'pValueBuf' void
* pointer is used to return the requested value.  This reduces the stack
* requirements and eliminates the need for the calling routine to copy the
* data into its final location.  Note that the data type is implied and is
* the value defined in the associated MIB for this object.
*
* The 'exception' contains one of the mApiException_t values.  GETs and
* GET-NEXTs support non-zero exceptions; SETs do not support non-zero
* exceptions.  If 'exception' contains MAPI_BUFFER_TOO_SHORT', then 'valueLen'
* contains the actual byte length of the data and no data is returned.  The
* calling routine then has the option of making a new request using the proper
* size buffer. 
*
* 'pObjCookie' points to object-level data defined by the MAPI handler and/or
* MAPI handler caller that can be a single variable or a structure.  Unlike
* 'pReqCookie' in the mApiRequest_t structure, the 'pObjCookie' field can be
* set/used also by the MAPI handler caller.  The use of this field must be
* clearly documented in specific header MAPI include file.
*/
typedef struct mApiObject_s
    {
    ulong_t         oidPrefixEnum;  /* enum for object OID Prefix */ 
    void *          pValueBuf;      /* pointer to value buffer */
    ulong_t         valueLen;       /* value byte length */ 
    mApiException_t exception;      /* exception value for object */
    void *          pObjCookie;     /* pointer to MAPI object data */
    } mApiObject_t;

/******************************************************************************
* mApiRequest_t -- MIB API Request typedef
*
* This structure is used to make a request for a single MIB object or for
* multiple objects in a single SNMP table row.  Consequently, only a single
* instance is required (and allowed) to process this request.
*
* 'pInstance' is a pointer to an array of unsigned long integers that contains
* the MIB object or table row instance information for a single object or row.
* It CANNOT be overloaded to contain index information for multiple rows.  For
* GET-NEXT operations, the caller must use an instance array size that supports
* the expected instance length.  'instanceLen' contains the number of
* sub-identifiers in the instance.  For a scalar, the instance is always '0'
* and the length is always '1'.  For a GET-NEXT operation, the MIB API updates
* both the 'pInstance' and 'instanceLen' information.
*
* 'pObjectList' is a pointer to an array of 'mApiObject_t' structures that
* contains 'numObjects' elements.
*
* 'error' contains the error status for the entire request.  It uses the
* 'mApiError_t' enumeration or an enumeration defined by the application
* protocol (e.g., OSPF). 'error' is zero (MAPI_NO_ERROR) if there is no error.
* If 'error' is non-zero, then the entire request has failed (i.e., no partial
* data is returned).  The 'errorObjIndex' is a one-based index to the object
* that caused the error.  For general errors not caused by any specific object,
* 'errorObjIndex' is '0'.  Note that if this is a MAPI_UNDO request, and UNDO
* is not supported, 'error' will contain 'MAPI_UNDO_FAILED'. 
*
* 'pReqCookie' points to request-level data defined by the MAPI handler that 
* can be a single variable or a structure.  This opaque "data", which is ONLY
* used by the MAPI handler, allows the handler to maintain (or remember)
* information between the various SET phases.  The 'pReqCookie' SHOULD NOT be
* initialized or modified by the calling routine (that is, only the MAPI
* handler can modify this field).  A typical use for this field is to point a
* structure that saves information from the TEST phase for use in the COMMIT
* phase.
*/
typedef struct mApiRequest_s
    {
    ulong_t *       pInstance;      /* pointer to OID instance array */
    ushort_t        instanceLen;    /* number of sub-ids in instance */ 
    ushort_t        numObjects;     /* number of objects in request */
    mApiObject_t *  pObjectList;    /* pointer to array of object info */ 
    ushort_t        error;          /* error (non-zero) for entire request */
    ushort_t        errorObjIndex;  /* 1-based index to object causing error */
    void *          pReqCookie;     /* pointer to MAPI request opaque data */
    } mApiRequest_t;

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif  /* __INCospfCommonMibApih */
