/* ospfCommonRowStatusLib.h - OSPF common row status header file. */

/* Copyright 2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01e,29apr04,ram Fix mapi AVL, compile, and warnings
01d,06apr04,ram OSPF v2/v3 coexistance changes
01c,08sep03,agi ported to Accordion stack
01b,12aug03,agi added ospf_convert_ip_address_to_dot_format()
01a,06aug03,agi created (ported from OSPFv2)
*/

/*
DESCRIPTION
This file defines the types and function prototypes for rowStatusLib
*/

#ifndef __INCospfCommonPrototypesh
#define __INCospfCommonPrototypesh

#include <avlLib.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

char * ospf_strlwr (char *cptr_string_to_lower_case);

char * ospf_convert_ip_address_to_dot_format
    (char *cptr_array_to_store_dot_format_address, ULONG ip_address);

BOOLEAN ospf_is_parameter_enabled (char *cptr_parameter_string);

void ospf_add_entry_to_list (LINK *sptr_link, LINK *sptr_link_to_add);

void ospf_delete_entry_from_list (
    LINK *sptr_list_link, LINK *sptr_link_to_delete);

void ospfAvlWalkAndExecute(AVL_TREE* root, void walkExec(void * node));
void ospfAvlExecuteAndWalk(AVL_TREE* root, void walkExec(void * node));


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCospfCommonPrototypesh */

