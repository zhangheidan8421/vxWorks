/************************************************************************/
/*	Copyright (C) 1999 RouterWare, Inc.     							*/
/*	Unpublished - rights reserved under the Copyright Laws of the		*/
/*	United States.  Use, duplication, or disclosure by the 				*/
/*	Government is subject to restrictions as set forth in 				*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 		*/
/*	Computer Software clause at 252.227-7013.							*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach, CA	*/
/************************************************************************/ 
#if !defined (__IP_IF_H__)
#define __IP_IF_H__
#include <ip_message_if.h>		/* IP_MESSAGE definition */
/* ************************************************************************************************************************ */
#define ANY_IP_IF ((UINT)-1)
#define MAX_OPTION_DATA_LENGTH							38

/*typedef UINT IP_TYPE_OF_SERVICE;*/
/*typedef UINT IP_IF_HANDLE;*/
typedef UINT ARP_INTERFACE_HANDLE;
/* ************************************************************************************************************************ */
/* IP Lower Layer Interface */ 
/* ************************************************************************************************************************ */
typedef enum IP_LOWER_CALLBACK_IF_TYPE
{
	IP_LOWER_CALLBACK_IF_ETHERNET_TYPE,
	IP_LOWER_CALLBACK_IF_PPP_TYPE
} IP_LOWER_CALLBACK_IF_TYPE;
/* ************************************************************************************************************************ */
typedef struct IP_LOWER_CALLBACK_IF
{
	bool (*fp_ip_lower_if_send)	(struct IP_LOWER_CALLBACK_IF*, IP_MESSAGE* p_ip_message, 
		IP_ADDRESS next_hop_ip_address);

		/*		NOTE: the next_hop_ip_address parameter should be set to the IP address of the next hop,
		 *			which could be:
		 *			1> either the IP address of the destination
		 *			2> or, the IP address of the next hop gateway
		 *		If this parameter is 0, the function would fail. */

} IP_LOWER_CALLBACK_IF;
/* ************************************************************************************************************************ */
typedef struct IP_LOWER_ETHERNET_CALLBACK_IF
{
	IP_LOWER_CALLBACK_IF				parent;
} IP_LOWER_ETHERNET_CALLBACK_IF;
/* ************************************************************************************************************************ */
typedef struct IP_LOWER_PPP_CALLBACK_IF
{
	IP_LOWER_CALLBACK_IF				parent;
} IP_LOWER_PPP_CALLBACK_IF;
/* ************************************************************************************************************************ */
typedef struct IP_LOWER_IF
{
	IP_IF_HANDLE (*fp_ip_lower_if_add_interface)		(struct IP_LOWER_IF*, IP_LOWER_CALLBACK_IF*);

	bool (*fp_ip_lower_if_remove_interface)			(struct IP_LOWER_IF*, IP_IF_HANDLE);

	bool (*fp_ip_lower_if_add_address)					(struct IP_LOWER_IF*, IP_ADDRESS ip_address, IP_ADDRESS subnet_mask);

	bool (*fp_ip_lower_if_remove_address)				(struct IP_LOWER_IF*, IP_ADDRESS ip_address);

	bool (*fp_ip_lower_if_receive)						(struct IP_LOWER_IF*, IP_IF_HANDLE, struct IP_MESSAGE*);

	bool (*fp_ip_lower_if_set_default_route)			(struct IP_LOWER_IF*, IP_ADDRESS next_hop_gateway);
} IP_LOWER_IF;
/* ************************************************************************************************************************ */
/* IP Upper Layer Interface */
/* ************************************************************************************************************************ */
typedef struct IP_UPPER_CALLBACK_IF
{
	bool (*fp_ip_upper_callback_if_up)					(struct IP_UPPER_CALLBACK_IF*, IP_IF_HANDLE);

	bool (*fp_ip_upper_callback_if_down)				(struct IP_UPPER_CALLBACK_IF*, IP_IF_HANDLE);

	bool (*fp_ip_upper_callback_if_add_address)		(struct IP_UPPER_CALLBACK_IF*, IP_IF_HANDLE, IP_ADDRESS ip_address, 
																		IP_ADDRESS subnet_mask);

	bool (*fp_ip_upper_callback_if_remove_address)	(struct IP_UPPER_CALLBACK_IF*, IP_IF_HANDLE, IP_ADDRESS ip_address);

	bool (*fp_ip_upper_callback_if_receive)			(struct IP_UPPER_CALLBACK_IF*, IP_IF_HANDLE, 
																		struct IP_MESSAGE* p_ip_message);
} IP_UPPER_CALLBACK_IF;
/* ************************************************************************************************************************ */
typedef struct IP_UPPER_IF
{
	bool (*fp_ip_upper_if_register)							(struct IP_UPPER_IF*, IP_TRANSPORT_PROTOCOL, 
																			struct IP_UPPER_CALLBACK_IF* p_transport);

	bool (*fp_ip_upper_if_deregister)						(struct IP_UPPER_IF*, IP_UPPER_CALLBACK_IF*);

	bool (*fp_ip_upper_if_send)								(struct IP_UPPER_IF*, IP_IF_HANDLE, struct IP_MESSAGE*, 
																			IP_ADDRESS next_hop_gateway);

	UINT (*fp_ip_upper_if_get_mtu)							(struct IP_UPPER_IF*, IP_IF_HANDLE);

	IP_ADDRESS (*fp_ip_upper_if_get_first_ip_interface_address)	(struct IP_UPPER_IF*, IP_IF_HANDLE);
} IP_UPPER_IF;
/* ************************************************************************************************************************ */
#endif /* __IP_IF_H__ */
