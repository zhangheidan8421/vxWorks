# make.MIPS32sfgnu - GNU tools definitions for MIPS32 with software floating pt.
#
# Copyright 2000-2001 Wind River Systems, Inc.
#
# modification history
# --------------------
# 01p,06may05,rlg  Changes for MIPS specific devices
# 01o,23apr04,yvp  added ABI_SPEC
# 01n,10jan02,pes  Move include of defs.mips after CPU and TOOL are defined
# 01m,09jan02,pes  Move common definitions to defs.mips
# 01l,07dec01,s_l  Add a flag to allow target side build (-fno-schedule-insn).
#                  Must be rmove when a comp fix goes in.
# 01k,28nov01,tpw  Remove -funroll-loops from MIPS*gnu* makefile fragments.
# 01j,30oct01,tpw  Standardize kernel links and bootrom conversions.
# 01i,11oct01,dat  SPR 20403, OPTIM_DRIVER must have -fvolatile
# 01h,25sep01,dat  Location of defs.gnu changed to tool/gnu
# 01g,27jun01,agf  add LONGCALL macro
# 01f,05mar01,mem  Added -DSOFT_FLOAT to command line args.
# 01e,08feb01,mem  Changed RES_LDFLAGS to -m vxr5ebmip
# 01d,18jan01,mem  Added -mno-branch-likely.
# 01c,08jan01,pes  Remove -pedantic switch from compiler command line.
# 01b,04jan01,mem  Updated to use new bootrom extraction
# 01a,08Dec00,pes  created based on make.R4000sfgnu.
#
# DESCRIPTION
# This file contains MIPS32 specific definitions and flags for the GNU
# software generation tools (compiler, assembler, linker etc) when used
# to generate a 32 bit compatible vxWorks.
#*/

CPU		= MIPS32
TOOL		= sfgnu
ABI_SPEC	= MIPS32

include $(TGT_DIR)/h/tool/gnu/defs.mips
#add the next two line for MIPS specific devices
LD_SCRIPT_RAM   = -T $(TGT_DIR)/h/tool/gnu/ldscripts/link.MIPSRAM
LD_SCRIPT_ROM   = -T $(TGT_DIR)/h/tool/gnu/ldscripts/link.MIPSROM

# end of make.MIPS32sfgnu
