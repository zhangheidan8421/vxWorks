/* toolMacros.h - compile time macros for GNU C compiler */

/*
 * Copyright (c) 2001-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. No license to Wind River intellectual property rights
 * is granted herein. All rights not licensed by Wind River are reserved
 * by Wind River.
 */

/*
modification history
--------------------
01n,09sep05,jln  added _WRS_ABSOLUTE_EXTERN
01m,08aug05,wap  Add section relocation macros
01l,02oct04,mcm  Fixing definition of _WRS_ABSOLUTE_ for Pentium.
01k,25aug04,h_k  updated _WRS_ABSOLUTE for the arches which require
                 specific semantics, such as arm and sh. (SPR #100726)
01j,30jul04,mcm  Undefining WRS_GNU_VAR_MACROS since GCC 3.3.2 supports the
                 ANSI syntax
01i,17mar04,dat  remove leading underscore from _absSymbols_xxx (95052)
01h,05mar04,dat  fix abs symbols
01g,05feb04,dat  add macros for _WRS_ABSOLUTE
01f,26nov03,dat  Adding INLINE and DEPRECATED
01e,16oct03,dat  New macros for INLINE both C and ASM
01d,23sep03,zl   added _WRS_CONSTRUCTOR definition
01c,26nov01,dat  New macros for alignment checking and unaligned copies
01b,14nov01,dat  Adding underscores to macro names
01a,18apr01,dat  written
*/

#ifndef __INCtoolMacrosh
#define __INCtoolMacrosh

#define _WRS_PACK_ALIGN(x) __attribute__((packed, aligned(x)))

#define _WRS_ASM(x) __asm__ volatile (x)

#define _WRS_DATA_ALIGN_BYTES(x) __attribute__((aligned(x)))

#undef _WRS_GNU_VAR_MACROS	/* GCC 3.3.2 supports the ANSI syntax */

/* Macros for alignment issues */

#define _WRS_ALIGNOF(x) 	__alignof__(x)

#define _WRS_ALIGN_CHECK(ptr, type) \
	(((int)(ptr) & ( _WRS_ALIGNOF(type) - 1)) == 0 ? TRUE : FALSE)

#define _WRS_UNALIGNED_COPY(pSrc, pDest, size) \
	( __builtin_memcpy ((pDest), (void *)(pSrc), size))

/*
 * Macros to force code or data into specific sections. These
 * can be used to force the compiler to place a given piece of
 * code or data into a separate section. The idea is to improve
 * cache usage by either clustering frequently referenced code
 * close together, or moving infrequenly used code (i.e. "run-once"
 * init routines that are only used during bootstrap) out of the
 * way. The "init" sections are used for the latter case, while
 * the "fast" sections are used to group performance-critical
 * routines together.
 */

#ifndef _WRS_NO_SPECIAL_SECTIONS
#define _WRS_INITTEXT	__attribute__ ((__section__ (".text.init")))
#define _WRS_FASTTEXT	__attribute__ ((__section__ (".text.fast")))
#define _WRS_INITDATA	__attribute__ ((__section__ (".data.init")))
#define _WRS_FASTDATA	__attribute__ ((__section__ (".data.fast")))
#else
#define _WRS_INITTEXT
#define _WRS_FASTTEXT
#define _WRS_INITDATA
#define _WRS_FASTDATA
#endif

/*
 * macro for static initializer (constructor) definitions; assumes 
 * GCC 3.3 or later with C constructor support.
 */

#define _WRS_CONSTRUCTOR(rtn,lvl) \
	__attribute__((constructor(lvl))) void _STI__##lvl##__##rtn (void)

/*
 * New Basix macros, (Portable Coding Guide, v7)
 *
 * HAS_GCC_ASM_SYNTAX if Gnu inline assembly syntax is supported.
 * HAS_DCC_ASM_SYNTAX if Diab inline assembly function syntax is supported.
 * INLINE for static inline C functions,
 * DEPRECATED for obsolete API warnings.
 */


#define _WRS_HAS_GCC_ASM_SYNTAX
#undef  _WRS_HAS_DCC_ASM_SYNTAX
#define _WRS_INLINE		static __inline__
#define _WRS_DEPRECATED(x) 	__attribute__((deprecated))

#ifndef	LEADING_UNDERSCORE
#define	LEADING_UNDERSCORE	FALSE	/* defualt in B6 except SH arch */
#endif	/* LEADING_UNDERSCORE */

#if	(LEADING_UNDERSCORE == TRUE)
#define _WRS_ABSOLUTE(name,value)	_WRS_ABSOLUTE_(_##name,value)
#else	/* LEADING_UNDERSCORE == FALSE */
#define _WRS_ABSOLUTE(name,value)	_WRS_ABSOLUTE_(name,value)
#endif	/* LEADING_UNDERSCORE == TRUE */

/* (New V7) For absolute symbol support. (use with semicolon) */

#if	(CPU_FAMILY == ARM) || (CPU_FAMILY == ARM_THUMB)
/*
 * GNU/ARM backend does not have a proper operand modifier which does not
 * produces prefix # followed by value, such as %0 for PPC, PENTIUM, MIPS
 * and %O0 for SH. The workaround made here is using %B0 which converts
 * the value to ~(value). Thus "n"(~(value)) is set in operand constraint
 * to output (value) in the ARM specific _WRS_ABSOLUTE macro.
 * (The related SPR is #101463.)
 */

#define _WRS_ABSOLUTE_(name,value) \
        __asm__ (".globl\t"#name \
                "\n\t.equ\t"#name",%B0" \
                "\n\t.type\t"#name",%%object" \
                :: "n"(~(value)))

#elif	(CPU_FAMILY == SH)
#define _WRS_ABSOLUTE_(name,value) \
        __asm__ (".globl\t"#name \
                "\n\t.equ\t"#name",%O0" \
                "\n\t.type\t"#name",@object" \
                :: "n"(value))

#elif	(CPU_FAMILY == I80X86) || (CPU_FAMILY == SIMNT) || (CPU_FAMILY == SIMLINUX) || (CPU_FAMILY == SIMPENTIUM)
#define _WRS_ABSOLUTE_(name,value) \
        __asm__ (".globl\t"#name \
                "\n\t.equ\t"#name",%c0" \
                "\n\t.type\t"#name",@object" \
                :: "n"(value))

#else	/* CPU_FAMILY != ARM && CPU_FAMILY != ARM_THUMB && CPU_FAMILY != SH && CPU_FAMILY != PENTIUM */
#define _WRS_ABSOLUTE_(name,value) \
        __asm__ (".globl\t"#name \
                "\n\t.equ\t"#name",%0" \
                "\n\t.type\t"#name",@object" \
                :: "n"(value))

#endif	/* CPU_FAMILY == ARM || CPU_FAMILY == ARM_THUMB || CPU_FAMILY == SH && CPU_FAMILY != PENTIUM */

#define _WRS_ABSOLUTE_EXTERN(name)  \
	extern const char name[]

/* Prefix and suffix for assembly declarations (private function) */

#define _WRS_ABSOLUTE_BEGIN(x) \
        STATUS absSymbols_##x (void) {

#define _WRS_ABSOLUTE_END   \
        return OK; }


/*
 * For compatibility with v5 of portable C coding guide
 *
 * These versions are obsolete, please don't use them
 */

#define WRS_PACK_ALIGN(x)	_WRS_PACK_ALIGN(x)
#define WRS_ASM(x)		_WRS_ASM(x)
#define WRS_DATA_ALIGN_BYTES(x)	_WRS_DATA_ALIGN_BYTES(x)
#undef WRS_GNU_VAR_MACROS

#endif /* __INCtoolMacrosh */
