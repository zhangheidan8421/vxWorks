/* taskUtilLib.h - kernel utility interface header */

/*
 * Copyright (c) 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */
 
/*
modification history
--------------------
01c,10oct05,kk  fix TASK_SCHED_INFO_SET macro
01b,31aug05,gls updated for checkin
01a,04mar05,kk  written 
*/

#ifndef __INCtaskUtilLibh
#define __INCtaskUtilLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <vxWorks.h>
#include <taskLib.h>

/* typdef */

typedef UINT32	SPARE_NUM;	/* allotted number for TCB spare field */

/* defines */

/*******************************************************************************
*
* TASK_SCHED_INFO_GET - get the pSchedInfo field in the TCB
*
* This macro gets the value of the pSchedInfo field in the TCB.
*
* PROTOTYPE: void * TASK_SCHED_INFO_GET(int tid)
*
* RETURNS: N/A
*
* \NOMANUAL
*/

#define TASK_SCHED_INFO_GET(tid)					\
    (									\
    ((WIND_TCB *)(tid))->pSchedInfo					\
    )

/*******************************************************************************
*
* TASK_SCHED_INFO_SET - set the pSchedInfo field in the TCB
*
* This macro sets the value of the pSchedInfo field in the TCB.
*
* PROTOTYPE: void TASK_SCHED_INFO_SET(int tid, void * schedInfo)
*
* RETURNS: N/A
*
* \NOMANUAL
*/

#define TASK_SCHED_INFO_SET(tid, schedInfo)				\
    (									\
    ((WIND_TCB *)(tid))->pSchedInfo = (void *) schedInfo		\
    )

/* function declarations */

extern void 	taskSpareNumAllot (int tid, SPARE_NUM * numAllotted);
extern int	taskSpareFieldGet (int tid, SPARE_NUM num);
extern STATUS  	taskSpareFieldSet (int tid, SPARE_NUM num, int value);

#ifdef __cplusplus
}
#endif

#endif /* __INCtaskUtilLibh */
