/* pmLib.h - persistent heap */

/* Copyright 2002 Wind River Systems, Inc. */

/*
modification history
--------------------
01i,14may04,md   track size of arena in header
01h,05aug03,dbs  allow more than one PM arena
01g,04aug03,dbs  ensure pmLib has no globals
01f,29jul03,dbs  re-introduce idea of 'closing' a region
01e,29jul03,dbs  remove pmRegionDelete API, add pmInvalidate
01d,18jul03,dbs  remove hard-coded limit on number of regions
01c,15jul03,dbs  increase size of key string
01b,14jul03,dbs  rename phpLib to pmLib
01a,11jul03,dbs  add phpFreeSpace API
000,28oct02, md  written, initial version
*/

#ifndef __INCpmLib_h
#define __INCpmLib_h

#ifdef __cplusplus
extern "C" {
#endif

/* general settings */

/* maximum number of regions available */
#define	PM_MAX_REGIONS	64
    
/* magic number of persistent heap */
#define	PM_MAGIC	((int) 0xBEEFBABE)

/* max length of key string */
#define PM_KEY_LEN	20

/* open region with 'read' enabled */
#define PM_PROT_RDONLY	0

/* open region with 'read' and 'write' enabled */
#define PM_PROT_RDWR	1

/* types */

typedef void* (*PM_ARENA_DEF) (UINT32*);

/* PM region header.
 *
 * Each distinct region in the PM has a header at it start, with a
 * short human-readable 'key' and some housekeeping data. It is 16
 * bytes long, including the checksum field.
 */
typedef struct pm_region
    {
    int  magic;			/* magic number			*/
    int  offset;		/* offset of data block		*/
    int  size;			/* size of data block		*/
    char key [PM_KEY_LEN];	/* textual key			*/
    } PM_REGION;
    
/* Persistent memory block header
 *
 * One of these exists at the start of the memory region which is
 * owned and managed by pmLib.
 */
typedef struct pm_arena
    {
    int   checksum;		/* header checksum			*/
    struct 
	{
	int   magic;		/* magic number				*/
	void *raddr;		/* real address of memory		*/
	void *vaddr;		/* aliased virtual address 		*/
	int   arena_size;	/* total size of arena in bytes		*/
	int   data_size;	/* total size of block in bytes		*/
	int   next_free_offset;	/* next free offset			*/
	int   max_region;	/* max number of allocated regions	*/
	PM_REGION region [1];	/* extensible array of regions		*/
	} field;
    }  PM_ARENA;

/* function declarations */

extern STATUS pmLibInit (void);
    
extern void*  pmDefaultArena (UINT32*);

extern STATUS pmInit (PM_ARENA_DEF arena);
extern int    pmFreeSpace (PM_ARENA_DEF arena);
extern int    pmRegionCreate (PM_ARENA_DEF arena, const char *key, unsigned int size, int mode);
extern int    pmRegionOpen (PM_ARENA_DEF arena, const char *key);
extern void * pmRegionAddr (PM_ARENA_DEF arena, int region);
extern int    pmRegionSize (PM_ARENA_DEF arena, int region);
extern STATUS pmRegionProtect (PM_ARENA_DEF arena, int region, int mode);
extern STATUS pmRegionClose (PM_ARENA_DEF arena, int region);
extern STATUS pmInvalidate (PM_ARENA_DEF arena);
extern STATUS pmValidate (PM_ARENA_DEF arena);
extern STATUS pmInvalidate (PM_ARENA_DEF arena);
extern STATUS pmShow (PM_ARENA_DEF arena);
    
#ifdef  __cplusplus
}
#endif

#endif /* __INCpmLib_h */
