/* shellPromptLib.h - header for the shell prompt module */

/* Copyright 2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,08feb05,bpn  Written.
*/

#ifndef __INCshellPromptLibh
#define __INCshellPromptLibh

#ifdef __cplusplus
extern "C" {
#endif

/* Includes */
#ifdef HOST
#include <host.h>
#include <shell.h>
#endif

/* Defines */

/* Structures */

/* Functions */

extern STATUS	shellPromptFmtStrAdd (char fmt, VOIDFUNCPTR fmtRtn, BOOL force);
extern STATUS	shellPromptFmtSet (SHELL_ID shellId, const char * interp,
				   const char * promptFmt);
extern STATUS	shellPromptFmtDftSet (const char * interp,
				      const char * promptFmt);

#ifdef __cplusplus
}
#endif

#endif /* __INCshellPromptLibh */
