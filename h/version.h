/* version.h - VxWorks version information */

/* 
 * Copyright (c) 1984-2005 Wind River Systems, Inc. 
 *
 * The right to copy, distribute, modify or otherwise make use 
 * of this software may be licensed only pursuant to the terms 
 * of an applicable Wind River license agreement. 
 */

/*
modification history
--------------------
03p,28sep05,tam  removed RDR from version string
03o,01sep05,pad  Moved #ifdef _WRS_KERNEL pragma lower in the file so as to
                 allow for the build of the host-side part of vxsim.
03n,24aug05,pad  this file is now shared between kernel and user sides
		 (SPR #105066) but only the _WRS_VXWORKS_xxx macros are visible
		 on the user side.
03m,11aug05,kkz  Updating version strings for 6.2
03l,12may05,tam  changed RUNTIME_VERSION to 6.1
03k,22apr05,tam  changed RUNTIME_VERSION to 6.1 - Reference Design Release
03j,17feb05,tam  changed RUNTIME_VERSION to 6.1 - Early Access Release
03i,18oct04,dbt  change RUNTIME_VERSION to 6.0.
03h,11may04,md   add global variables for VxWorks version numbers
03g,05may04,dat  move version number macros from vxWorks.c to version.h,
		 change version to 'Beta'
03f,16mar04,rhe  Change RUNTIME_VERSION to 6.0 - Technology Access Release 2
03e,12dec03,dat  change release to 6.0, TAR version 1, added a space to
		 VXWORKS_VERSION between the name and the version.
03d,05oct03,kk   change release to 5.5 (BASE 6 - Iter 2)
03c,02jul03,job  change release to 5.5 (BASE 6 - Iter 1)
03c,25mar03,sfp  update for CP-1 release: VxWorks 5.5.1
03b,12sep01,fmk  change release to 5.5
03a,17jan01,sn   simplify job of host val  
03a,08dec00,cmf  changed release to 5.4.2
02z,08jul98,ms   changed release to 5.4.
02y,02oct96,p_m  changed release to 5.3.1.
                 removed pre 02r history.
02x,15mar96,p_m  changed release to 5.3 for everything except 960 K.
02w,06mar96,dat  removed length macros, no longer needed
02v,23jan96,tpr  changed release to 5.3-PowerPC-beta
02t,06jun95,caf  changed release to 5.2-PowerPC-beta.
02u,25oct95,ms	 changed release to 5.3
02t,21jun95,ms	 changed release to 5.3-beta
02s,29mar95,kdl  changed release to 5.2.
02r,12nov94,dvs  changed release to 5.2-beta
*/

#ifndef __INCversionh
#define __INCversionh

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Release identification, major, minor, and maintenance numbers.
 * 
 * Current VxWorks release number is 6.2.0
 */

#define _WRS_VXWORKS_MAJOR 6
#define _WRS_VXWORKS_MINOR 2
#define _WRS_VXWORKS_MAINT 0

/* For printLogo(), the max combined length is 44 characters */

#define RUNTIME_NAME    "VxWorks"
#define RUNTIME_VERSION "6.2"

/* there is now a space between the runtime name, and the version */

#define VXWORKS_VERSION RUNTIME_NAME " " RUNTIME_VERSION

#ifdef _WRS_KERNEL 
#ifndef _ASMLANGUAGE
IMPORT char * creationDate;
IMPORT char * runtimeVersion;
IMPORT char * runtimeName;
IMPORT char * vxWorksVersion;

IMPORT const unsigned int vxWorksVersionMajor;
IMPORT const unsigned int vxWorksVersionMinor;
IMPORT const unsigned int vxWorksVersionMaint;
#endif
#endif /* _WRS_KERNEL */

#ifdef __cplusplus
}
#endif

#endif /* __INCversionh */
