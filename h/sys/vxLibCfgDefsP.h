/* vxLibCfgDefsP.h - Configuration head for the VxWorks system layers */

/* 
 * Copyright (c) 2005 Wind River Systems, Inc. 
 *
 * The right to copy, distribute, modify or otherwise make use 
 * of this software may be licensed only pursuant to the terms 
 * of an applicable Wind River license agreement. 
 */ 

/*
modification history
--------------------
01s,30sep05,yvp  Reworked SPR 112908 fix.
01r,26sep05,kk   renamed FORMATTED_IO_BASE to FORMATTED_OUT_BASIC
01q,29aug05,h_k  removed INCLUDE_CACHE_xxx.
01p,15aug05,mil  Added INCLUDE_VM_RTP for OSL.
01o,15aug05,mil  Added INCLUDE_OBJ_LIB for BKL.
01n,15aug05,md   Cleanup defines, add project build section
01m,10aug05,md   Turn on EDR_ERROR_INJECT_STUBS for MKL
01l,09aug05,md   Enable EDR_ERROR_INJECT_STUBS for all layers
01l,12aug05,mmi  Add INCLUDE_PWR_MGMT 
01k,27jul05,md   Fixed ED&R defines for non-layered build
01j,25jul05,md   Added new ED&R defines
01i,26jul05,mil  Added macro for isr objects.
01h,25jul05,yvp  Conditionalize library build configuration defines. 
                 Formatting cleanups.
01g,22jul05,h_k  Added macros for various layers.
01f,22jul05,h_k  undefed WV_INSTRUMENTATION for lower layers.
01e,21jul05,yvp  Added more component defines.
01d,12jul05,mil  Added INCLUDE_MEM_ALLOT to MKL.
01c,04jun05,yvp  Made OSL the default layer.
                 Updated copyright. 
01b,16may05,yvp  Layer numbers start with 1, not 0.
01a,26apr05,yvp  written.
*/

#ifndef __INCvxLibCfgDefsPh
#define __INCvxLibCfgDefsPh

/*
 * The six layers of a VxWorks system.
 */

#define _MKL_	1
#define _BKL_	2
#define _KSL_	3
#define _OSL_	4
#define _ASL_	5
#define _MSL_	6


#ifndef _WRS_LAYER
#define _WRS_LAYER	_OSL_
#endif  /* _WRS_LAYER */

#if ((_WRS_LAYER < _MKL_) || (_WRS_LAYER > _MSL_))
#error  "_WRS_LAYER value is out of range - cannot proceed"
#endif	/* ((_WRS_LAYER < _MKL_) || (_WRS_LAYER > _MSL_)) */


#if (defined _WRS_LIB_BUILD) && !(defined PRJ_BUILD)

/* defines for standard (non-project) VxWorks source code builds */

/*
 * Minimal Kernel Configuration
 */

#if (_WRS_LAYER == _MKL_)
#undef	INCLUDE_EDR_POLICY_HOOKS
#undef	INCLUDE_FULL_EDR_STUBS
#undef	INCLUDE_TASK_HOOKS
#undef	INCLUDE_WINDVIEW
#define INCLUDE_FORMATTED_OUT_BASIC
#define INCLUDE_MEM_ALLOT               /* for memAllotLib */
#endif	/* _WRS_LAYER	== _MKL_ */


/*
 * Basic Kernel Configuration
 */

#if (_WRS_LAYER == _BKL_)
#undef	INCLUDE_EDR_POLICY_HOOKS
#undef	INCLUDE_FULL_EDR_STUBS
#undef	INCLUDE_WINDVIEW
#define INCLUDE_FORMATTED_OUT_BASIC
#define INCLUDE_OBJ_LIB
#define INCLUDE_TASK_HOOKS
#endif	/* _WRS_LAYER	== _BKL_ */


/*
 * Basic OS Configuration
 */

#if (_WRS_LAYER == _KSL_)
#undef	INCLUDE_EDR_POLICY_HOOKS
#undef	INCLUDE_FULL_EDR_STUBS
#undef	INCLUDE_WINDVIEW
#define INCLUDE_FORMATTED_OUT_BASIC
#define INCLUDE_FORMATTED_IO
#define INCLUDE_OBJ_LIB
#define INCLUDE_SHELL
#define INCLUDE_SM_OBJ
#define INCLUDE_TASK_HOOKS
#endif	/* _WRS_LAYER	== _KSL_ */


/*
 * Standard OS Configuration
 */

#if (_WRS_LAYER == _OSL_)
#define	INCLUDE_EDR_POLICY_HOOKS
#define	INCLUDE_FULL_EDR_STUBS
#define INCLUDE_FORMATTED_OUT_BASIC
#define INCLUDE_FORMATTED_IO
#define INCLUDE_ISR_OBJECTS
#define INCLUDE_MMU_BASIC
#define INCLUDE_OBJ_LIB
#define INCLUDE_RTP
#define INCLUDE_SHELL
#define INCLUDE_SM_OBJ
#define INCLUDE_TASK_HOOKS
#define INCLUDE_TTY_DEV
#define INCLUDE_VM_RTP
#define INCLUDE_WINDVIEW
#define INCLUDE_CPU_PWR_MGMT
#endif	/* _WRS_LAYER	== _OSL_ */


/* general for all layers */

#ifdef	INCLUDE_WINDVIEW
#define WV_INSTRUMENTATION
#else	/* INCLUDE_WINDVIEW */
#undef	WV_INSTRUMENTATION
#endif	/* INCLUDE_WINDVIEW */

#else /* (defined _WRS_LIB_BUILD) && !(defined PRJ_BUILD) */

/* defines for project based VxWorks source code builds */

/* ED&R policy hooks are always minimal in a non-OSL build */

#if (_WRS_LAYER != _OSL_)

# undef INCLUDE_EDR_POLICY_HOOKS
# undef INCLUDE_FULL_EDR_STUBS

#else /* _WRS_LAYER != _OSL_ */

# define INCLUDE_EDR_POLICY_HOOKS
# define INCLUDE_FULL_EDR_STUBS

#endif /* _WRS_LAYER != _OSL_ */

#endif /* (defined _WRS_LIB_BUILD) && !(defined PRJ_BUILD) */

#endif /* __INCvxLibCfgDefsPh */
