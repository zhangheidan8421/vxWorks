/* elfMips.h - Mips specific Extended Load Format header */

/* Copyright 1984-2001 Wind River Systems, Inc. */

/*
 * This file has been developed or significantly modified by the MIPS Center of
 * Excellence Dedicated Engineering Staff. This notice is as per the MIPS
 * Center of Excellence Master Partner Agreement, do not remove this notice
 * without checking first with WR/Platforms MIPS Center of Excellence
 * engineering management.
 */

/*
modification history
--------------------
01h,26jul05,v_r Linked target/h/arch/mips to this one (as for all other shared
		ELF headers).
01g,03oct04,jn  Skip relocations used by shared library
01f,04nov03,jn  Temporarily expose some internals of elf mips for RTP loader
01e,11dec01,pad Removed all unnecessary types and definitions.
01d,16jul01,ros Add CofE comment
01c,14sep99,myz Added a new relocation type R_MIPS16_26 for mips16
01b,17nov95,kkk Added two new special section numbers
01a,28sep93,cd  Derived from elfSparc.h.
*/

#ifndef __INCelfMipsh
#define __INCelfMipsh

#ifdef	__cplusplus
extern "C" {
#endif

/* MIPS ABI relocation types */

#define R_MIPS_NONE		0
#define R_MIPS_16		1
#define R_MIPS_32		2
#define R_MIPS_REL32		3	/* unsupported in VxWorks */
#define R_MIPS_26		4
#define R_MIPS_HI16		5
#define R_MIPS_LO16		6
#define R_MIPS_GPREL16		7	/* unsupported in VxWorks */
#define R_MIPS_LITERAL		8	/* unsupported in VxWorks */
#define R_MIPS_GOT16		9	/* unsupported in VxWorks */
#define R_MIPS_PC16		10	/* unsupported in VxWorks */
#define R_MIPS_CALL16		11	/* unsupported in VxWorks */
#define R_MIPS_GPREL32		12	/* unsupported in VxWorks */

/* 
 * These relocation types are not ABI defined. They are required for
 * our compilers' shared library implementation (as per the VxWorks
 * Shared Library EABI). 
 */
#define	R_MIPS_COPY		126
#define	R_MIPS_JMP_SLOT		127

#define EM_ARCH_MACHINE		EM_MIPS
#define EI_ARCH_CLASS		ELFCLASS32
#define EI_ARCH_DATA		ELFDATE2MSB

/*
 * The HI16_RELOC_INFO type provides a way for elfMipsAddrHi16Reloc() to
 * communicate details to elfMipsAddrLo16Reloc() when SHT_REL-type relocation
 * entries are used.
 *
 * Note: the structure should be allocated on the stack to be thread-safe.
 * 
 * XXX Temporarily move this structure here from elfMips.c, to allow use by
 * the RTP loader.  
 */

typedef struct hi16RelocInfo {
    void *	pSymAdrs;	/* address of symbol involved in relocation */
    UINT32	instruction;	/* content of instruction being relocated */
    void *	pRelocAdrs;	/* address of field being relocated */
    BOOL	relocated;	/* TRUE if the relocation work is done */
} HI16_RELOC_INFO;

#ifdef	__cplusplus
}
#endif

#endif	/* __INCelfMipsh */
