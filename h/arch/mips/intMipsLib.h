/* intMipsLib.h - MIPS-specific interrupt library header file */

/* Copyright 1984-1999 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,26oct99,dra  created.
*/

#ifndef __INCintMipsLibh
#define __INCintMipsLibh

#if defined(__STDC__) || defined(__cplusplus)

#ifdef __cplusplus
extern "C" {
#endif

extern int	intCRGet (void);
extern void	intCRSet (int value);
extern int	intSRGet (void);
extern int	intSRSet (int value);
extern int	intDisable (int);
extern int	intEnable (int);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#else /* defined(__STDC__) || defined(__cplusplus) */

extern int	intCRGet ();
extern void	intCRSet ();
extern int	intSRGet ();
extern int	intSRSet ();
extern int	intDisable ();
extern int	intEnable ();

#endif /* defined(__STDC__) || defined(__cplusplus) */

#endif /* __INCintMipsLibh */
