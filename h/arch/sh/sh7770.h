/* sh7770.h - SH7770 header */

/* Copyright 2004      Wind River Systems, Inc. */

/*
modification history
--------------------
03b,24sep04,h_k  added RTC_CLKSEL and GPIO register definitions.
02b,16apr04,h_k  added UBC register addresses. (SPR #96445)
                 added INTC registers definitions.
01a,08apr04,h_k  written.
*/

/*
DESCRIPTION
This file contains I/O addresses and related constants for the SH7770 chip.
*/

#ifndef __INCsh7770h
#define __INCsh7770h

#ifdef __cplusplus
extern "C" {
#endif


/* INTC (INTerrupt Controller) register addresses */

#define INTC_ICR0	((volatile UINT32 *)0xffd00000)	/* control register 0 */
#define INTC_ICR1	((volatile UINT32 *)0xffd0001c)	/* control register 1 */
#define INTC_PRI	((volatile UINT32 *)0xffd00010)	/* priority setup */
#define INTC_REQ	((volatile UINT32 *)0xffd00024)	/* int request */
#define INTC_MSK0	((volatile UINT32 *)0xffd00044)	/* int mask 0 */
#define INTC_MSK1	((volatile UINT32 *)0xffd00048)	/* int mask 1 */
#define INTC_MSKCLR0	((volatile UINT32 *)0xffd00064)	/* int mask clear 0 */
#define INTC_MSKCLR1	((volatile UINT32 *)0xffd00068)	/* int mask clear 1 */
#define INTC_NMIFCR	((volatile UINT32 *)0xffd000c0)	/* NMI flag control */
#define INTC_USERIMASK	((volatile UINT32 *)0xff950000)	/* user int mask */

/* ICR0 (INTerrupt Controller Register 0) definitions */

#define INTC_ICR0_NMIL		(UINT32)0x80000000
#define INTC_ICR0_MAI		(UINT32)0x40000000
#define INTC_ICR0_NMIB		(UINT32)0x02000000
#define INTC_ICR0_NMIE		(UINT32)0x01000000
#define INTC_ICR0_IRLM0		(UINT32)0x00800000
#define INTC_ICR0_IRLM1		(UINT32)0x00400000

/* ICR1 (INTerrupt Controller Register 1) definitions */

#define INTC_ICR1_FAL_EDGE	0
#define INTC_ICR1_RIS_EDGE	1
#define INTC_ICR1_LOW_LVL	2
#define INTC_ICR1_HIGH_LVL	3

#define INTC_ICR1_IRQ0_SHIFT	30
#define INTC_ICR1_IRQ1_SHIFT	28
#define INTC_ICR1_IRQ2_SHIFT	26
#define INTC_ICR1_IRQ3_SHIFT	24
#define INTC_ICR1_IRQ4_SHIFT	22
#define INTC_ICR1_IRQ5_SHIFT	20

/* INTREQ (INTerrupt Request Register) definitions */

#define INTC_REQ_IR0		(UINT32)0x80000000
#define INTC_REQ_IR1		(UINT32)0x40000000
#define INTC_REQ_IR2		(UINT32)0x20000000
#define INTC_REQ_IR3		(UINT32)0x10000000
#define INTC_REQ_IR4		(UINT32)0x08000000
#define INTC_REQ_IR5		(UINT32)0x04000000

/* INTMSK and INTMSKCLR Regisrters definitions */

#define INTC_MSK0_00		(UINT32)0x80000000
#define INTC_MSK0_01		(UINT32)0x40000000
#define INTC_MSK0_02		(UINT32)0x20000000
#define INTC_MSK0_03		(UINT32)0x10000000
#define INTC_MSK0_04		(UINT32)0x08000000
#define INTC_MSK0_05		(UINT32)0x04000000
#define INTC_MSK0_ALL		(UINT32)0xfc000000

#define INTC_MSK1_10		(UINT32)0x80000000
#define INTC_MSK1_ALL		(UINT32)0x80000000

/* NMIFCR (NMI Fragment Controll Register) definitions */

#define INTC_NMIFCR_NMIL	(UINT32)0x80000000

/* USERIMASK (User Mode Interrupt Mask Register) definitions */

#define INTC_USERIMASK_WKEY	(UINT32)0xa5000000
#define INTC_USERIMASK_SHIFT	4

/* INTC2 (INTerrupt Controller 2) register addresses */

#define INTC2_PRI0	((volatile UINT32 *)0xffe00000)	/* priority 0  setup */
#define INTC2_PRI1	((volatile UINT32 *)0xffe00004)	/* priority 1  setup */
#define INTC2_PRI2	((volatile UINT32 *)0xffe00008)	/* priority 2  setup */
#define INTC2_PRI3	((volatile UINT32 *)0xffe0000c)	/* priority 3  setup */
#define INTC2_PRI4	((volatile UINT32 *)0xffe00010)	/* priority 4  setup */
#define INTC2_PRI5	((volatile UINT32 *)0xffe00014)	/* priority 5  setup */
#define INTC2_PRI6	((volatile UINT32 *)0xffe00018)	/* priority 6  setup */
#define INTC2_PRI7	((volatile UINT32 *)0xffe0001c)	/* priority 7  setup */
#define INTC2_PRI8	((volatile UINT32 *)0xffe00020)	/* priority 8  setup */
#define INTC2_PRI9	((volatile UINT32 *)0xffe00024)	/* priority 9  setup */
#define INTC2_PRI10	((volatile UINT32 *)0xffe00028)	/* priority 10 setup */
#define INTC2_PRI11	((volatile UINT32 *)0xffe0002c)	/* priority 11 setup */
#define INTC2_PRI12	((volatile UINT32 *)0xffe00030)	/* priority 12 setup */
#define INTC2_PRI13	((volatile UINT32 *)0xfffe0034)	/* priority 13 setup */

#define INTC2_A0	((volatile UINT32 *)0xffe00038)	/* int request */
#define INTC2_A1	((volatile UINT32 *)0xffe0003c)	/* int request */
#define INTC2_MSKRG	((volatile UINT32 *)0xffe00040)	/* int mask */
#define INTC2_MSKCR	((volatile UINT32 *)0xffe00044)	/* int mask clear */

#define INTC2_B0	((volatile UINT32 *)0xffe00048)	/* int request modul */	
#define INTC2_B1	((volatile UINT32 *)0xffe0004c)	/* int request modul */	
#define INTC2_B2	((volatile UINT32 *)0xffe00050)	/* int request modul */	
#define INTC2_B3	((volatile UINT32 *)0xffe00054)	/* int request modul */	
#define INTC2_B4	((volatile UINT32 *)0xffe00058)	/* int request modul */	
#define INTC2_B5	((volatile UINT32 *)0xffe0005c)	/* int request modul */	
#define INTC2_B6	((volatile UINT32 *)0xffe00060)	/* int request modul */	
#define INTC2_B7	((volatile UINT32 *)0xffe00064)	/* int request modul */	
#define INTC2_B8	((volatile UINT32 *)0xffe00068)	/* int request modul */	
#define INTC2_B9	((volatile UINT32 *)0xffe0006c)	/* int request modul */	

/* UBC (User Break Controller) register addresses */

#define UBC_CBR0	((volatile UINT32 *)0xff200000)	/* match cond. set 0 */
#define UBC_CRR0	((volatile UINT32 *)0xff200004)	/* match oper. set 0 */
#define UBC_CAR0	((volatile UINT32 *)0xff200008)	/* match addr. set 0 */
#define UBC_CAMR0	((volatile UINT32 *)0xff20000c)	/* match addr. mask 0 */
#define UBC_CBR1	((volatile UINT32 *)0xff200020)	/* match cond. set 1 */
#define UBC_CRR1	((volatile UINT32 *)0xff200024)	/* match oper. set 1 */
#define UBC_CAR1	((volatile UINT32 *)0xff200028)	/* match addr. set 1 */
#define UBC_CAMR1	((volatile UINT32 *)0xff20002c)	/* match addr. mask 1 */
#define UBC_CDR1	((volatile UINT32 *)0xff200030)	/* match data set 1 */
#define UBC_CDMR1	((volatile UINT32 *)0xff200034)	/* match data mask 1 */
#define UBC_CETR1	((volatile UINT32 *)0xff200038)	/* exe. cnt. enable 1 */
#define UBC_CCMFR	((volatile UINT32 *)0xff200600)	/* channel match flag */
#define UBC_CBCR	((volatile UINT32 *)0xff200620)	/* break control */

/* SCIF (Serial Communication Interface with FIFO) register addresses */

#define SCIF0_SMR	((volatile UINT16 *)0xff923000)	/* serial mode        */
#define SCIF0_BRR	((volatile UINT8  *)0xff923004)	/* bit rate           */
#define SCIF0_SCR	((volatile UINT16 *)0xff923008)	/* serial control     */
#define SCIF0_FTDR	((volatile UINT8  *)0xff92300c)	/* transmit FIFO data */
#define SCIF0_FSR	((volatile UINT16 *)0xff923010)	/* serial status      */
#define SCIF0_FRDR	((volatile UINT8  *)0xff923014)	/* receive FIFO data  */
#define SCIF0_FCR	((volatile UINT16 *)0xff923018)	/* FIFO control       */
#define SCIF0_RFDR	((volatile UINT16 *)0xff92301c)	/* rec FIFO data count*/
#define SCIF0_SPTR	((volatile UINT16 *)0xff923020)	/* serial port reg    */
#define SCIF0_LSR	((volatile UINT16 *)0xff923024)	/* line status        */

#define SCIF1_SMR	((volatile UINT16 *)0xff924000)	/* serial mode        */
#define SCIF1_BRR	((volatile UINT8  *)0xff924004)	/* bit rate           */
#define SCIF1_SCR	((volatile UINT16 *)0xff924008)	/* serial control     */
#define SCIF1_FTDR	((volatile UINT8  *)0xff92400c)	/* transmit FIFO data */
#define SCIF1_FSR	((volatile UINT16 *)0xff924010)	/* serial status      */
#define SCIF1_FRDR	((volatile UINT8  *)0xff924014)	/* receive FIFO data  */
#define SCIF1_FCR	((volatile UINT16 *)0xff924018)	/* FIFO control       */
#define SCIF1_RFDR	((volatile UINT16 *)0xff92401c)	/* rec FIFO data count*/
#define SCIF1_SPTR	((volatile UINT16 *)0xff924020)	/* serial port reg    */
#define SCIF1_LSR	((volatile UINT16 *)0xff924024)	/* line status        */

#define SCIF2_SMR	((volatile UINT16 *)0xff925000)	/* serial mode        */
#define SCIF2_BRR	((volatile UINT8  *)0xff925004)	/* bit rate           */
#define SCIF2_SCR	((volatile UINT16 *)0xff925008)	/* serial control     */
#define SCIF2_FTDR	((volatile UINT8  *)0xff92500c)	/* transmit FIFO data */
#define SCIF2_FSR	((volatile UINT16 *)0xff925010)	/* serial status      */
#define SCIF2_FRDR	((volatile UINT8  *)0xff925014)	/* receive FIFO data  */
#define SCIF2_FCR	((volatile UINT16 *)0xff925018)	/* FIFO control       */
#define SCIF2_RFDR	((volatile UINT16 *)0xff92501c)	/* rec FIFO data count*/
#define SCIF2_SPTR	((volatile UINT16 *)0xff925020)	/* serial port reg    */
#define SCIF2_LSR	((volatile UINT16 *)0xff925024)	/* line status        */

/* RTC (Real Time Clock) register addresses */

#define RTC_CLKSEL	((volatile UINT16 *)0xffc80040)	/* clock select reg   */

/* GPIO (General Purpose IO) register base addresses */

#define GPIO0_IOINTSEL	((volatile UINT32 *)0xff93e000)	/* IO interrupt select*/
#define GPIO0_INOUTSEL	((volatile UINT32 *)0xff93e004)	/* IO select reg      */
#define GPIO0_OUTDT	((volatile UINT32 *)0xff93e008)	/* output data reg    */
#define GPIO0_INDT	((volatile UINT32 *)0xff93e00c)	/* input data reg     */
#define GPIO0_INTDT	((volatile UINT32 *)0xff93e010)	/* interrupt data reg */
#define GPIO0_INTCLR	((volatile UINT32 *)0xff93e014)	/* interrupt clear    */
#define GPIO0_INTMSK	((volatile UINT32 *)0xff93e018)	/* interrupt mask     */
#define GPIO0_MSKCLR	((volatile UINT32 *)0xff93e01c)	/* interr. mask clear */
#define GPIO0_POSNEG	((volatile UINT32 *)0xff93e020)	/* +/- logic set reg  */
#define GPIO0_EDGLEVEL	((volatile UINT32 *)0xff93e024)	/* edge level set reg */
#define GPIO0_FILONOFF	((volatile UINT32 *)0xff93e028)	/* chattering on/off  */

#define GPIO1_IOINTSEL	((volatile UINT32 *)0xff93f000)	/* IO interrupt select*/
#define GPIO1_INOUTSEL	((volatile UINT32 *)0xff93f004)	/* IO select reg      */
#define GPIO1_OUTDT	((volatile UINT32 *)0xff93f008)	/* output data reg    */
#define GPIO1_INDT	((volatile UINT32 *)0xff93f00c)	/* input data reg     */
#define GPIO1_INTDT	((volatile UINT32 *)0xff93f010)	/* interrupt data reg */
#define GPIO1_INTCLR	((volatile UINT32 *)0xff93f014)	/* interrupt clear    */
#define GPIO1_INTMSK	((volatile UINT32 *)0xff93f018)	/* interrupt mask     */
#define GPIO1_MSKCLR	((volatile UINT32 *)0xff93f01c)	/* interr. mask clear */
#define GPIO1_POSNEG	((volatile UINT32 *)0xff93f020)	/* +/- logic set reg  */
#define GPIO1_EDGLEVEL	((volatile UINT32 *)0xff93f024)	/* edge level set reg */
#define GPIO1_FILONOFF	((volatile UINT32 *)0xff93f028)	/* chattering on/off  */

#define GPIO2_IOINTSEL	((volatile UINT32 *)0xff940000)	/* IO interrupt select*/
#define GPIO2_INOUTSEL	((volatile UINT32 *)0xff940004)	/* IO select reg      */
#define GPIO2_OUTDT	((volatile UINT32 *)0xff940008)	/* output data reg    */
#define GPIO2_INDT	((volatile UINT32 *)0xff94000c)	/* input data reg     */
#define GPIO2_INTDT	((volatile UINT32 *)0xff940010)	/* interrupt data reg */
#define GPIO2_INTCLR	((volatile UINT32 *)0xff940014)	/* interrupt clear    */
#define GPIO2_INTMSK	((volatile UINT32 *)0xff940018)	/* interrupt mask     */
#define GPIO2_MSKCLR	((volatile UINT32 *)0xff94001c)	/* interr. mask clear */
#define GPIO2_POSNEG	((volatile UINT32 *)0xff940020)	/* +/- logic set reg  */
#define GPIO2_EDGLEVEL	((volatile UINT32 *)0xff940024)	/* edge level set reg */
#define GPIO2_FILONOFF	((volatile UINT32 *)0xff940028)	/* chattering on/off  */

#define GPIO3_IOINTSEL	((volatile UINT32 *)0xff941000)	/* IO interrupt select*/
#define GPIO3_INOUTSEL	((volatile UINT32 *)0xff941004)	/* IO select reg      */
#define GPIO3_OUTDT	((volatile UINT32 *)0xff941008)	/* output data reg    */
#define GPIO3_INDT	((volatile UINT32 *)0xff94100c)	/* input data reg     */
#define GPIO3_INTDT	((volatile UINT32 *)0xff941010)	/* interrupt data reg */
#define GPIO3_INTCLR	((volatile UINT32 *)0xff941014)	/* interrupt clear    */
#define GPIO3_INTMSK	((volatile UINT32 *)0xff941018)	/* interrupt mask     */
#define GPIO3_MSKCLR	((volatile UINT32 *)0xff94101c)	/* interr. mask clear */
#define GPIO3_POSNEG	((volatile UINT32 *)0xff941020)	/* +/- logic set reg  */
#define GPIO3_EDGLEVEL	((volatile UINT32 *)0xff941024)	/* edge level set reg */
#define GPIO3_FILONOFF	((volatile UINT32 *)0xff941028)	/* chattering on/off  */

/* interrupt vector numbers */

/*---------------------------------------------------------------------------*/
/*      exception/interrupt name     (code >> 5)   No. vector  entry point   */
/*---------------------------------------------------------------------------*/
#undef	INUM_GPIO	/* undefine INUM_GPIO defined in sh7750.h */
#define INUM_GPIO                   (0x3e0 >> 5) /* 31  0x07c  (vbr + 0x600) */
#define INUM_TMU0_CH0_UNDERFLOW     (0x400 >> 5) /* 32  0x080  (vbr + 0x600) */
#define INUM_TMU0_CH1_UNDERFLOW     (0x420 >> 5) /* 33  0x084  (vbr + 0x600) */
#define INUM_TMU0_CH2_UNDERFLOW     (0x440 >> 5) /* 34  0x088  (vbr + 0x600) */
#define INUM_TMU0_CH2_INPUT_CAPTURE (0x460 >> 5) /* 35  0x08c  (vbr + 0x600) */
#define INUM_TMU1_CH0_UNDERFLOW     (0x480 >> 5) /* 36  0x090  (vbr + 0x600) */
#define INUM_TMU1_CH1_UNDERFLOW     (0x4a0 >> 5) /* 37  0x094  (vbr + 0x600) */
#define INUM_TMU1_CH2_UNDERFLOW     (0x4c0 >> 5) /* 38  0x098  (vbr + 0x600) */
#define INUM_TMU1_CH2_INPUT_CAPTURE (0x4e0 >> 5) /* 39  0x09c  (vbr + 0x600) */
#define INUM_TMU2_CH0_UNDERFLOW     (0x500 >> 5) /* 40  0x0a0  (vbr + 0x600) */
#define INUM_TMU2_CH1_UNDERFLOW     (0x520 >> 5) /* 41  0x0a4  (vbr + 0x600) */
#define INUM_TMU2_CH2_UNDERFLOW     (0x540 >> 5) /* 42  0x0a8  (vbr + 0x600) */
/*                                  (0x560 >> 5)    43  0x0ac  - reserved -  */
#define INUM_HAC                    (0x580 >> 5) /* 44  0x0b0  (vbr + 0x600) */
/*                                  (0x5a0 >> 5)    45  0x0b4  - reserved -  */
#define INUM_EXTERNAL               (0x5c0 >> 5) /* 46  0x0b8  (vbr + 0x600) */
#define INUM_SPDIF                  (0x5e0 >> 5) /* 47  0x0bc  (vbr + 0x600) */
/*      INUM_JTAG                   (0x600 >> 5) /@ 48  0x0c0   */
#define INUM_I2C                    (0x620 >> 5) /* 49  0x0c4  (vbr + 0x600) */
/*      INUM_DMAC_DMTE0             (0x640 >> 5) /@ 50  0x0c8  (vbr + 0x600) */
/*      INUM_DMAC_DMTE1             (0x660 >> 5) /@ 51  0x0cc  (vbr + 0x600) */
/*      INUM_DMAC_DMTE2             (0x680 >> 5) /@ 52  0x0d0  (vbr + 0x600) */
#define INUM_I2S0                   (0x6a0 >> 5) /* 53  0x0d4  (vbr + 0x600) */
#define INUM_I2S1                   (0x6c0 >> 5) /* 54  0x0d8  (vbr + 0x600) */
#define INUM_I2S2                   (0x6e0 >> 5) /* 55  0x0dc  (vbr + 0x600) */
#define INUM_I2S3                   (0x700 >> 5) /* 56  0x0c0  (vbr + 0x600) */
#define INUM_SRC_RX_FULL            (0x720 >> 5) /* 57  0x0e4  (vbr + 0x600) */
#define INUM_SRC_TX_EMPTY           (0x740 >> 5) /* 58  0x0e8  (vbr + 0x600) */
#define INUM_SRC_SPDIF              (0x760 >> 5) /* 59  0x0ec  (vbr + 0x600) */
#define INUM_DU                     (0x780 >> 5) /* 60  0x0f0  (vbr + 0x600) */
#define INUM_VIDEO_IN               (0x7a0 >> 5) /* 61  0x0f4  (vbr + 0x600) */
#define INUM_REMOCON                (0x7c0 >> 5) /* 62  0x0f8  (vbr + 0x600) */
#define INUM_YUV                    (0x7e0 >> 5) /* 63  0x0fc  (vbr + 0x600) */
/*      INUM_FPU_DISABLE            (0x800 >> 5) /@ 64  0x100  (vbr + 0x600) */
/*      INUM_SLOT_FPU_DISABLE       (0x820 >> 5) /@ 65  0x104  (vbr + 0x600) */
#define INUM_USB_H                  (0x840 >> 5) /* 66  0x108  (vbr + 0x600) */
#define INUM_ATAPI                  (0x860 >> 5) /* 67  0x10c  (vbr + 0x600) */
#define INUM_CAN                    (0x880 >> 5) /* 68  0x110  (vbr + 0x600) */
#define INUM_GPS                    (0x8a0 >> 5) /* 69  0x114  (vbr + 0x600) */
#define INUM_2D                     (0x8c0 >> 5) /* 70  0x118  (vbr + 0x600) */
/*                                  (0x8e0 >> 5)    71  0x11c  - reserved -  */
#define INUM_3D_MBX                 (0x900 >> 5) /* 72  0x120  (vbr + 0x600) */
#define INUM_3D_DMAC                (0x920 >> 5) /* 73  0x124  (vbr + 0x600) */
#define INUM_EX_BUS_ATA             (0x940 >> 5) /* 74  0x128  (vbr + 0x600) */
#define INUM_SPI0                   (0x960 >> 5) /* 75  0x12c  (vbr + 0x600) */
#define INUM_SPI1                   (0x980 >> 5) /* 76  0x130  (vbr + 0x600) */
#define INUM_SCIF0                  (0x9a0 >> 5) /* 77  0x134  (vbr + 0x600) */
#define INUM_SCIF1                  (0x9c0 >> 5) /* 78  0x138  (vbr + 0x600) */
#define INUM_SCIF2                  (0x9e0 >> 5) /* 79  0x13c  (vbr + 0x600) */
#define INUM_SCIF3                  (0xa00 >> 5) /* 80  0x140  (vbr + 0x600) */
#define INUM_SCIF4                  (0xa20 >> 5) /* 81  0x144  (vbr + 0x600) */
#define INUM_SCIF5                  (0xa40 >> 5) /* 82  0x148  (vbr + 0x600) */
#define INUM_SCIF6                  (0xa60 >> 5) /* 83  0x14c  (vbr + 0x600) */
#define INUM_SCIF7                  (0xa80 >> 5) /* 84  0x150  (vbr + 0x600) */
#define INUM_SCIF8                  (0xaa0 >> 5) /* 85  0x154  (vbr + 0x600) */
#define INUM_SCIF9                  (0xac0 >> 5) /* 86  0x158  (vbr + 0x600) */
/*                                  (0xae0 >> 5)    87  0x15c  (vbr + 0x600) */
/*                                  (0xb00 >> 5)    88  0x160  (vbr + 0x600) */
#define INUM_ADC                    (0xb20 >> 5) /* 89  0x164  (vbr + 0x600) */
/*                                  (0xb40 >> 5)    90  0x168  (vbr + 0x600) */
/*                                  (0xb60 >> 5)    91  0x16c  (vbr + 0x600) */
/*                                  (0xb80 >> 5)    92  0x170  (vbr + 0x600) */
#define INUM_BUSB_DMAC00            (0xba0 >> 5) /* 93  0x174  (vbr + 0x600) */
#define INUM_BUSB_DMAC01            (0xbc0 >> 5) /* 94  0x178  (vbr + 0x600) */
#define INUM_BUSB_DMAC02            (0xbe0 >> 5) /* 95  0x17c  (vbr + 0x600) */
#define INUM_BUSB_DMAC03            (0xc00 >> 5) /* 96  0x180  (vbr + 0x600) */
#define INUM_BUSB_DMAC04            (0xc20 >> 5) /* 97  0x184  (vbr + 0x600) */
#define INUM_BUSB_DMAC05            (0xc40 >> 5) /* 98  0x188  (vbr + 0x600) */
#define INUM_BUSB_DMAC06            (0xc60 >> 5) /* 99  0x18c  (vbr + 0x600) */
#define INUM_BUSB_DMAC07            (0xc80 >> 5) /* 100 0x190  (vbr + 0x600) */
#define INUM_BUSB_DMAC08            (0xca0 >> 5) /* 101 0x194  (vbr + 0x600) */
#define INUM_BUSB_DMAC09            (0xcc0 >> 5) /* 102 0x198  (vbr + 0x600) */
#define INUM_BUSB_DMAC10            (0xce0 >> 5) /* 103 0x19c  (vbr + 0x600) */
#define INUM_BUSB_DMAC11            (0xd00 >> 5) /* 104 0x1a0  (vbr + 0x600) */
#define INUM_BUSB_DMAC12            (0xd20 >> 5) /* 105 0x1a4  (vbr + 0x600) */
#define INUM_BUSB_DMAC13            (0xd40 >> 5) /* 106 0x1a8  (vbr + 0x600) */
#define INUM_BUSB_DMAC14            (0xd60 >> 5) /* 107 0x1ac  (vbr + 0x600) */
#define INUM_BUSB_DMAC15            (0xd80 >> 5) /* 108 0x1b0  (vbr + 0x600) */
#define INUM_BUSB_DMAC16            (0xda0 >> 5) /* 109 0x1b4  (vbr + 0x600) */
#define INUM_BUSB_DMAC17            (0xdc0 >> 5) /* 110 0x1b8  (vbr + 0x600) */
#define INUM_BUSB_DMAC18            (0xde0 >> 5) /* 111 0x1bc  (vbr + 0x600) */
#define INUM_BUSB_DMAC19            (0xe00 >> 5) /* 112 0x1c0  (vbr + 0x600) */
#define INUM_BUSB_DMAC20            (0xe20 >> 5) /* 113 0x1c4  (vbr + 0x600) */
#define INUM_BUSB_DMAC21            (0xe40 >> 5) /* 114 0x1c8  (vbr + 0x600) */
#define INUM_BUSB_DMAC22            (0xe60 >> 5) /* 115 0x1cc  (vbr + 0x600) */
#define INUM_BUSB_DMAC23            (0xe80 >> 5) /* 116 0x1d0  (vbr + 0x600) */
#define INUM_BUSB_DMAC24            (0xea0 >> 5) /* 117 0x1d4  (vbr + 0x600) */
#define INUM_BUSB_DMAC25            (0xee0 >> 5) /* 118 0x1d8  (vbr + 0x600) */
#define INUM_BUSB_DMAC26            (0xec0 >> 5) /* 119 0x1dc  (vbr + 0x600) */
#define INUM_BUSB_DMAC27            (0xf00 >> 5) /* 120 0x1e0  (vbr + 0x600) */
#define INUM_BUSB_DMAC28            (0xf20 >> 5) /* 121 0x1e4  (vbr + 0x600) */
#define INUM_BUSB_DMAC29            (0xf40 >> 5) /* 122 0x1e8  (vbr + 0x600) */
#define INUM_BUSB_DMAC30            (0xf60 >> 5) /* 123 0x1ec  (vbr + 0x600) */
#define INUM_BUSB_DMAC31            (0xf80 >> 5) /* 124 0x1f0  (vbr + 0x600) */


/* virtual interrupt vectors defined */

#define IV_IRQ0			INUM_TO_IVEC (INUM_IRL13)
#define IV_IRQ1			INUM_TO_IVEC (INUM_IRL11)
#define IV_IRQ2			INUM_TO_IVEC (INUM_IRL9)
#define IV_IRQ3			INUM_TO_IVEC (INUM_IRL7)
#define IV_IRQ4			INUM_TO_IVEC (INUM_IRL5)
#define IV_IRQ5			INUM_TO_IVEC (INUM_IRL3)

#define IV_SCIF0		INUM_TO_IVEC (INUM_SCIF0)
#define IV_SCIF1		INUM_TO_IVEC (INUM_SCIF1)
#define IV_SCIF2		INUM_TO_IVEC (INUM_SCIF2)


#ifdef __cplusplus
}
#endif

#endif /* __INCsh7770h */
