/* archShCommon.h - common Sh architecture specific header */

/* Copyright 2003-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,29apr04,h_k  enabled coprocessor abstraction.
01c,22mar04,zl   moved trcLib_PORTABLE and INSTR definitions here.
01b,12nov03,job  Moved some blib stuff from archSh.h
01a,11nov03,pes  written.
*/

#ifndef __INCarchShCommonh
#define __INCarchShCommonh

#ifdef __cplusplus
extern "C" {
#endif

#define _WRS_PAL_COPROC_LIB		/* enable coprocessor abstraction */

/* from bLib.c */

#define _WRS_BLIB_ALIGNMENT     3       /* quad byte alignment mask */
#define swab_PORTABLE

/* from trcLib.c */

#define trcLib_PORTABLE

/* moved here from h/types/vxTypesOld.h */

#ifndef _ASMLANGUAGE
typedef unsigned short INSTR;		/* word-aligned instructions */
#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCarchShCommonh */




