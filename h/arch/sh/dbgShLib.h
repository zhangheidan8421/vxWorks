/* dbgShLib.h - header file for arch dependent portion of debugger */

/* Copyright 1994-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01o,19apr04,h_k  added SH7770 support. (SPR #96445)
01n,26feb04,h_k  corrected BRK_HARDMASK and moved HW breakpoint bit to lower 16
                 bit. (SPR #94415)
01m,20feb04,h_k  added DBG_SYSCALL_SIZE.
01l,27nov01,h_k  defined DBG_INST_ALIGN 2 for SH architectures.
01k,16feb01,hk   prefix underscore to UBC_xxx macro (in conflict with sh7XXX.h).
01j,06sep00,zl   updated for new HW breakpoint support.
01i,17apr00,frf  Removed defs of TSH_BH_CHAN_XX, HWINFO and BRK_HW_BP
01h,02mar00,frf  Add SH4 support for T2
01g,29jun99,zl   included SH7750 in hardware breakpoint support
01f,11mar99,hk   placed wdb defs on top, dbg defs at bottom, commons in between.
02e,09mar99,hk   moved UBC register defs to each CPU specific header.
02d,09mar99,hk   merged DSP defs, retrieved defs for target shell debugger.
01C,21oct98,kab  added SH7410 hardware breakpoint support.
01B,11sep98,kab  added hardware breakpoint support for SH-DSP.
01A,08may98,jmc  added support for the SH-DSP and SH3-DSP CPU types.
02c,29sep98,hk   code review: simplified CPU conditionals.
02b,24sep98,st   moved-up SH7750 section. added BAMR_IGNORE_BAR_LOWER_16BITS,
		 BAMR_IGNORE_BAR_LOWER_20BITS, BBR_BREAK_AT_QUAD_ACCESS
		 and BRCR_USER_BREAK_DEBUG_ENABLE defs.
02a,16jul98,st   added SH7750 support.
01z,22apr97,hk   changed 704X to 7040.
01y,17feb97,hk   unified BREAK_ESF to ESFSH.
01x,09feb97,hk   added aa to BREAK_ESF for SH7700. changed s/w breakpoint for
                 SH7700 to use 'trapa #0'.
01w,19aug96,hk   changed to use 'trapa #255' for SH7700 s/w break.
01v,09aug96,hk   use 'trapa #1' for SH7700 s/w break. changed code layout.
01u,10may96,hk   added support for SH7700.
01t,18dec95,hk   added support for SH704X.
01s,25apr95,hk   enabled DBG_TT.
01r,31mar95,hk   comply with new IV_TRAP macro.
01q,28feb95,hk   changed DBG_TRAP_NUM to DBG_TRAP_VEC, followed new IV_ def.
01p,22feb95,hk   added SH7000 support.
01o,21feb95,hk   deleted BH_INTERACTIVE.
01n,17feb95,hk   simplified bh() access code.
01m,07feb95,hk   copyright year 1995. modified HWINFO, deleted HWINFO2.
01l,11jan95,hk   restored HW_REGS_NUM to 2. modified HWINFO structure.
01k,10jan95,hk   changed HW_REGS_NUM to 1.
01j,25dec94,hk   disabled cret.
01i,23dec94,hk   changing macro names.
01h,22dec94,hk   enabled cret.
01g,18dec94,hk   added UBC defs.
01f,15dec94,hk   deleted junks, set BRK_HW_BP as 0x80 (same to i960).
01e,07dec94,hk   #if FALSE'd junk.
01d,06dec94,hk   fixed DBG_BREAK_INST. adding misc defines.
01c,30nov94,hk   added DBG_TRAP_NUM. hardcoded DBG_BREAK_INST as "trapa #63".
01b,09oct94,hk   added BREAK_ESF, TRACE_ESF, DBG_BREAK_INST.
01a,14sep94,sa   written based on mc68k/dbgMc68kLib.h 01e version.
*/

#ifndef __INCShdbgh
#define __INCShdbgh

#ifdef __cplusplus
extern "C" {
#endif

#include "esf.h"
#include "reg.h"

/* register name definitions migrated from h/wdb/wdbBpLib.h */

#define reg_pc			pc
#define reg_sp			spReg
#define reg_fp			fpReg

/* software breakpoint related defines */

#define BREAK_ESF		ESFSH
#define TRACE_ESF		BREAK_ESF

#define DBG_INST_ALIGN		2
#define DBG_NO_SINGLE_STEP	1

#if   (CPU==SH7750 || CPU==SH7700)
# define DBG_TRAP_NUM		0x00
#elif (CPU==SH7600 || CPU==SH7000)
# define DBG_TRAP_NUM		0x3f
#endif

#define DBG_BREAK_INST		(0xc300 + (DBG_TRAP_NUM & 0xff))
#define	DBG_TRAP_VEC		IV_TRAP(DBG_TRAP_NUM)
#define DBG_SYSCALL_SIZE	(sizeof (INSTR) / sizeof (INSTR))


/* Hardware breakpoint related defines */

#define DBG_HARDWARE_BP		1
#define DEFAULT_HW_BP		0		/* default hw. breakpoint */
#define BRK_HARDWARE    	0x00001000	/* hardware breakpoint bit */
#define BRK_HARDMASK    	0x000003ff	/* hardware breakpoint mask */
#define BRK_INST		BH_BREAK_INSN
#define MAX_CHAN		4		/* 2 or 4 UBC channels */

/* Common HW BP UI macros; register bits are in <cpu>.h */

#define BH_BREAK_MASK		0x00000003	/* Instr/Data select */
# define BH_BREAK_ANY		0x00000000
# define BH_BREAK_INSN		0x00000001
# define BH_BREAK_DATA		0x00000002

#define BH_CYCLE_MASK		0x0000000C	/* Access type select */
# define BH_CYCLE_ANY		0x00000000
# define BH_CYCLE_READ		0x00000004
# define BH_CYCLE_WRITE		0x00000008
# define BH_CYCLE_RW		0x0000000C

#define BH_SIZE_MASK		0x00000030	/* Access size select */
# define BH_ANY			0x00000000
# define BH_8			0x00000010
# define BH_16			0x00000020
# define BH_32			0x00000030

#define BH_CPU_MASK		0x000000C0	/* Bus controller select */
# define BH_CPU			0x00000000
# define BH_DMAC		0x00000040
# define BH_DMAC_CPU		0x00000080

#define BH_BUS_MASK		0x00000300	/* Internal bus selection */
# define BH_IBUS		0x00000000	
# define BH_XBUS		0x00000100
# define BH_YBUS		0x00000200

/* Debug register access macros */

#define _UBC_TYPE(i)		(ubc.type[i])
#define _UBC_BAR(i)		((volatile UINT32 *) ubc.base[i])
#define _UBC_BBR(i)		((volatile UINT16 *) (ubc.base[i] + 8))
#define _UBC_BAMR32(i)		((volatile UINT32 *) (ubc.base[i] + 4))
#define _UBC_BAMR8(i)		((volatile UINT8  *) (ubc.base[i] + 4))
#define _UBC_CBR(i)		((volatile UINT32 *) (ubc.base[i] - 8))
#define _UBC_BRCR16		((volatile UINT16 *) ubc.pBRCR)
#define _UBC_BRCR32		((volatile UINT32 *) ubc.pBRCR)

/* UBC identifiers */

#define BRCR_NONE		0		/* No UBC support */
#define BRCR_0_1		1		/* UBC w/o BRCR */
#define BRCR_16_1		2		/* 16 bit BRCR, 1 channel */
#define BRCR_16_2		3		/* 16 bit BRCR, 2 channels */
#define BRCR_32_2		4		/* 32 bit BRCR, 2 channels */
#define BRCR_32_4		5		/* 32 bit BRCR, 4 channels */
#define CCMFR_32_2		6		/* 32 bit CCMFR,2 channels */

/* Supported BRCR (Break control register) bits on various SuperH CPUs */

#define UBC_BRCR_CONDITION_MASK_16_2	0xc000
#define UBC_BRCR_CMFA		(1<<15)		/* Ch A */
#define UBC_BRCR_CMFB		(1<<14)		/* Ch B */

#define UBC_BRCR_CONDITION_MASK_32_2	0x0000f000
#define UBC_BRCR_BASMA		(1<<21)         /* Disable ASID check A */
#define UBC_BRCR_BASMB		(1<<20)         /* Disable ASID check B */
#define UBC_BRCR_SCMFCA		(1<<15)		/* Ch A, CPU */
#define UBC_BRCR_SCMFCB		(1<<14)		/* Ch B, CPU */
#define UBC_BRCR_SCMFDA		(1<<13)		/* Ch A, DMAC */
#define UBC_BRCR_SCMFDB		(1<<12)		/* Ch B, DMAC */

#define UBC_BRCR_CONDITION_MASK_32_4	0xc0c0c0c0
#define UBC_BRCR_CMFCA		(1<<31)		/* Ch A, CPU */
#define UBC_BRCR_CMFPA		(1<<30)		/* Ch A, DMAC */
#define UBC_BRCR_CMFCB		(1<<23)		/* Ch B, CPU */
#define UBC_BRCR_CMFPB		(1<<22)		/* Ch B, DMAC */
#define UBC_BRCR_CMFCC		(1<<15)		/* Ch C, CPU */
#define UBC_BRCR_CMFPC		(1<<14)		/* Ch C, DMAC */
#define UBC_BRCR_CMFCD		(1<<7)		/* Ch D, CPU */
#define UBC_BRCR_CMFPD		(1<<6)		/* Ch D, DMAC */

/* Supported BBR (Break bus cycle register) bits on various SuperH CPUs */

#define UBC_BBR_XYE		(1<<9)		/* Select X/Y bus */
#define UBC_BBR_XYS		(1<<8)		/* Select Y Bus */
#define UBC_BBR_CD1		(1<<7)		/* DMAC bus master */
#define UBC_BBR_CD0		(1<<6)		/* CPU bus master */
#define UBC_BBR_ID1		(1<<5)		/* Data access cycle */
#define UBC_BBR_ID0		(1<<4)		/* Inst fetch cycle */
#define UBC_BBR_RW1		(1<<3)		/* Write cycle */
#define UBC_BBR_RW0		(1<<2)		/* Read cycle */
#define UBC_BBR_SZ1		(1<<1)		/* Word access */
#define UBC_BBR_SZ0		(1<<0)		/* Byte access */

/* BAMR (Break address mask register) bits on BRCR_16_2 type */

#define UBC_BAMR_BASM		(1<<2)		/* Disable ASID check */

/* CBR (Match condition setting register) bits on SH4A CPUs */

#define UBC_CBR_MFE		(1<<31)		/* Match flag enable */
#define UBC_CBR_AIE		(1<<30)		/* ASID check enable */
#define UBC_CBR_MFI0		(1<<24)		/* Match flag specify */
#define UBC_CBR_AIV0		(1<<16)		/* ASID specify */
#define UBC_CBR_DBE		(1<<15)		/* Data value enable */
#define UBC_CBR_SZ2		(1<<14)		/* Quadword access */
#define UBC_CBR_SZ1		(1<<13)		/* Word access */
#define UBC_CBR_SZ0		(1<<12)		/* byte access */
#define UBC_CBR_ETBE		(1<<11)		/* Exc. count value enable */
#define UBC_CBR_CD0		(1<<6)		/* Bus select */
#define UBC_CBR_ID1		(1<<5)		/* Data access cycle */
#define UBC_CBR_ID0		(1<<4)		/* Inst fetch cycle */
#define UBC_CBR_RW1		(1<<2)		/* Write cycle */
#define UBC_CBR_RW0		(1<<1)		/* Read cycle */
#define UBC_CBR_CE		(1<<0)		/* Channel enable */

/* CRR (Match operation setting register) bits on SH4A CPUs */

#define UBC_CRR_RESERVE_13	(1<<13)		/* reserved bit. write value
						 * always should be 1.
						 */
#define UBC_CRR_PCB		(1<<1)		/* PC break after inst exec */
#define UBC_CRR_BIE		(1<<0)		/* break enable */

/* CCMFR (Chennel match flag register) bits on SH4A CPUs */

#define UBC_CCMFR_MF1		(1<<1)		/* chan 1 cond. match flag */
#define UBC_CCMFR_MF0		(1<<0)		/* chan 0 cond. match flag */


#ifndef _ASMLANGUAGE

/* structure to manage hardware breakpoint registers */

typedef struct
    {
    UINT32	bar[MAX_CHAN];		/* break address register A */
    union {
        UINT16	bbr[MAX_CHAN];		/* break bus cycle register A */
        UINT32	cbr[MAX_CHAN >> 1];	/* match condition setting register */
	} u_mcr;
    UINT32	type[MAX_CHAN];		/* brkpt register type */
    UINT32	brcr;			/* break control register */
    } DBG_REGS;

typedef struct
    {
    UINT32	brcrSize;
    UINT32	brcrInit;
    UINT32	pBRCR;
    UINT32	type[MAX_CHAN];
    UINT32	base[MAX_CHAN];
    } UBC;

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern void	(* _func_wdbUbcInit)(UBC *);

#else

extern VOIDFUNCPTR	_func_wdbUbcInit;

#endif	/* __STDC__ */

#endif /* ! _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCShdbgh */
