/* elfsh.h - Hitachi SH especific elf loader header */

/* 
 * Copyright (c) 1984-2005 Wind River Systems, Inc. 
 *
 * The right to copy, distribute or otherwise make use of this software may be
 * licensed only pursuant to the terms of an applicable Wind River license
 * agreement. 
 */

/*
modification history
--------------------
01e,25jul05,v_r  Updated copyright.
01d,06jun05,mcm  Adding relocations required for SH shared library support.
01c,04jun99,zl   hitachi SH4 architecture port, provided by Highlander
                 Engineering
01b,21apr99,nd   Added support for SH7750
01a,26aug98,cs   initial try at SH elf based on elfPpc.h, v01c
*/

#ifndef __INCelfShLibh
#define __INCelfShLibh

#ifdef __cplusplus
extern "C" {
#endif

#define EM_ARCH_MACHINE		EM_SH

/* relocation type definition */
#define  R_SH_NONE 		0  /* No relocation.  */
#define  R_SH_DIR32		1  /* 32 bit absolute relocation */
#define  R_SH_REL32		2  /* 32 bit PC relative relocation.  */
#define  R_SH_DIR8WPN		3  /* 8 bit PC relative branch divided by 2.  */
#define  R_SH_IND12W		4  /* 12 bit PC relative branch divided by 2.  */
#define  R_SH_DIR8WPL		5  /* 8 bit unsigned PC relative divided by 4.  */
#define  R_SH_DIR8WPZ		6  /* 8 bit unsigned PC relative divided by 2.  */

  /* 8 bit GBR relative.  XXX: This only makes sense if we have some
     special symbol for the GBR relative area, and that is not
     implemented.  */
#define  R_SH_DIR8BP		7  

  /* 8 bit GBR relative divided by 2.  XXX: This only makes sense if
     we have some special symbol for the GBR relative area, and that
     is not implemented.  */
#define  R_SH_DIR8W		8  

  /* 8 bit GBR relative divided by 4.  XXX: This only makes sense if
     we have some special symbol for the GBR relative area, and that
     is not implemented.  */
#define  R_SH_DIR8L		9  

#define  FIRST_INVALID_RELOC	10
#define  LAST_INVALID_RELOC	24

  /* The remaining relocs are a GNU extension used for relaxing.  The
     final pass of the linker never needs to do anything with any of
     these relocs.  Any required operations are handled by the
     relaxation code. We use the same constants as COFF uses, not that
     it really matters.  */

  /* A 16 bit switch table entry.  This is generated for an expression
     such as ``.word L1 - L2''.  The offset holds the difference
     between the reloc address and L2.  */
#define  R_SH_SWITCH16		25 

  /* A 32 bit switch table entry.  This is generated for an expression
     such as ``.long L1 - L2''.  The offset holds the difference
     between the reloc address and L2.  */
#define  R_SH_SWITCH32		26 

  /* Indicates a .uses pseudo-op.  The compiler will generate .uses
     pseudo-ops when it finds a function call which can be relaxed.
     The offset field holds the PC relative offset to the instruction
     which loads the register used in the function call.  */
#define  R_SH_USES		27 

  /* The assembler will generate this reloc for addresses referred to
     by the register loads associated with USES relocs.  The offset
     field holds the number of times the address is referenced in the
     object file.  */
#define  R_SH_COUNT		28

  /* Indicates an alignment statement.  The offset field is the power
     of 2 to which subsequent portions of the object file must be
     aligned.  */
#define  R_SH_ALIGN		29

  /* The assembler will generate this reloc before a block of
     instructions.  A section should be processed as assumining it
     contains data, unless this reloc is seen.  */
#define  R_SH_CODE		30

  /* The assembler will generate this reloc after a block of
     instructions when it sees data that is not instructions.  */
#define  R_SH_DATA		31

  /* The assembler generates this reloc for each label within a block
     of instructions.  This permits the linker to avoid swapping
     instructions which are the targets of branches.  */
#define  R_SH_LABEL		32

#define  R_SH_GOT32		160
#define  R_SH_COPY		162
#define  R_SH_GLOB_DAT		163
#define  R_SH_JMP_SLOT		164
#define  R_SH_RELATIVE		165


#ifdef __cplusplus
}
#endif

#endif /* __INCelfShLibh */
