/* ivSh.h - interrupt vectors for Renesas SH CPUs */

/* Copyright 1994-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
02x,27aug04,h_k  added IP_MMU_VA_SHIFT and IP_MMU_VA_MASK for AIM MMU.
02w,11may04,h_k  changed sys call trap number to 32 to share the number with
                 SH2.
02v,12feb04,h_k  added INUM_TRAP_SC.
02u,04nov03,h_k  added TRAP_SC.
02t,12sep03,h_k  added system call trap and IP_TMP_FRAME definitions.
02s,20mar02,zl   add reserved trap number for RTI tools; correct TMU4 comments.
02r,27sep01,hk   added temporary storage for PTEH, PTEL, and MMUCR.
02q,11sep01,hk   renamed WRS private VBR offset macros to IP_XXX. shifted stub
               offsets to make room for temporal register storage.
02p,11sep01,h_k  added USB Host Controller support for SH7727 (SPR #69831).
02o,21aug00,hk   merge SH7729 to SH7700. merge SH7410 to SH7600.
		 move chip specific definitions to sh7xxx.h.
02n,06aug00,hk   moved SH7040 specific definitions to sh7040.h.
02m,01jun00,frf  SH7751 intevt: PCI, MPX, timer 3 and 4.
02l,18apr00,hk   rearranged vbr relative offset macros for SH4/SH3.
02k,10apr00,hk   added vbr relative offsets for text and data in P1/P2.
02j,05mar99,hk   sorted out CPU conditionals.
02i,29dec98,hk   merged for SH7750,SH7729,SH7700,SH7600,SH7410,SH7040,SH7000.
02h,07oct98,jmb  clean up SH7410.
02g,04jun98,jmc  added support for SH-DSP and SH3-DSP CPU types.
02f,25may98,jmb  Deleted unnecessary ifdef.
02e,25may98,jmb  Correct name of transmit-end interrupt for SCI.
02d,23may98,jmb  Added macros for serial FIFO interrupts for SH7700.
02e,16sep98,hk   merged SH7750/SH7700 defs.
02d,16jul98,st   added SH7750 support.
02b,26nov97,hms  added fpp support definition.
02c,25nov97,hk   changed interrupt vector number definition for SH7700.
02b,21nov97,hk   fixed IVEC_TO_INUM macro.
02a,22apr97,hk   changed 704X to 7040.
01z,16feb97,hk   deleted unused INUMs. changed INUM_EXC_LOW to 0x000, INUM_EXC
                 _HIGH to 0x1e0. added INUMs for SH7707. added INUM_TRAP_128.
01y,09feb97,hk   added INUM_TRAP_1 for SH7700 zero div, deleted INUM_TRAP_254.
01x,19aug96,hk   added INUM_TRAP_254 for SH7700 zero divide signal.
01w,19aug96,hk   added INUM_SPURIOUS_INTERRUPT for SH7700.
01v,09aug96,hk   moved in HIGH_VEC, LOW_VEC, TRAP_32_VEC, USER_VEC_START from
		 excShLib.h. created INUM_EXC_LOW, INUM_EXC_HIGH, INUM_INTR_LOW,
		 INUM_INTR_HIGH for SH7700.
01u,09aug96,hk   put CPU macro check code on top. added INUM_TRAP_0/INUM_TRAP_
		 255 for SH7700. changed IV_TRAP() macro for SH7700. changed
		 code layout.
01t,08aug96,hk   changed SH7700_VEC_OFFSET to SH7700_VEC_TABLE_OFFSET. added
		 SH7700_EXC_STUB_OFFSET, SH7700_TLB_STUB_OFFSET, SH7700_INT_
		 STUB_OFFSET.
01s,06aug96,hk   changed SH7700_VEC_OFFSET to 0x800 from 0x700.
01r,04aug96,hk   changed ENUM_xxx to INUM_xxx.
01q,26jun96,ja	 added IV_USER_BREAK & IV_TRAP for SH7700.
01p,12jun96,hk   added SH7700_VEC_OFFSET & _ASMLANGUAGE control.
01o,07jun96,hk   changed IVEC_TO_INUM & INUM_TO_IVEC defs for SH7700.
01n,24may96,ja   added macro IVEC_TO_INUM & INUM_TO_IVEC for SH7700.
01m,19may96,hk   changed ICODE_IRLx defs.
01l,12may96,hk   added ECODE/ICODE for SH7700.
01k,18jan96,hk   added comments.
01j,19dec95,hk   added support for SH704X.
01i,15nov95,hk   added comment for INUM_TRAP_62 (zero divide trap).
01h,15aug95,hk   deleted INUM_VIRT_BASE, made this configurable in BSP.
01g,19jun95,hk   added INUM_VIRT_BASE.
01f,31mar95,hk   added interrupt number definition.
01e,28feb95,hk   changed all IV_ defs from vector number to vector itself.
01d,08feb95,sa   deleted DGA-001 vectors (IV_DIVU ... IV_SW_IRQ7).
01c,05oct94,sa   added DGA-001 vectors ( must move to h/drv/vme/dga001.h )
01b,21jun94,sa   added '#if (CPU==SH7600) #else (CPU==SH7000)'.
01a,15jun94,hk   written.
*/

#ifndef __INCivShh
#define __INCivShh

#ifdef __cplusplus
extern "C" {
#endif

#if (CPU!=SH7750 && CPU!=SH7700 && CPU!=SH7600 && CPU!=SH7000)
#error The CPU macro is not defined correctly
#endif


#if (CPU==SH7750 || CPU==SH7700)

/* interrupt vector numbers */

/*---------------------------------------------------------------------------*/
/*      exception/interrupt name     (code >> 5)   No. vector  entry point   */
/*---------------------------------------------------------------------------*/
#define INUM_EXC_LOW		    (0x000 >> 5) /*  -exception lower limit- */
/*      INUM_RESET_PWRON	    (0x000 >> 5)     0  0x000  (0xa0000000)  */
/*      INUM_RESET_MANUAL	    (0x020 >> 5)     1  0x004  (0xa0000000)  */
#define INUM_TLB_READ_MISS	    (0x040 >> 5) /*  2  0x008  (vbr + 0x400) */
#define INUM_TLB_WRITE_MISS	    (0x060 >> 5) /*  3  0x00c  (vbr + 0x400) */
#define INUM_TLB_WRITE_INITIAL_PAGE (0x080 >> 5) /*  4  0x010  (vbr + 0x100) */
#define INUM_TLB_READ_PROTECTED	    (0x0a0 >> 5) /*  5  0x014  (vbr + 0x100) */
#define INUM_TLB_WRITE_PROTECTED    (0x0c0 >> 5) /*  6  0x018  (vbr + 0x100) */
#define INUM_READ_ADDRESS_ERROR	    (0x0e0 >> 5) /*  7  0x01c  (vbr + 0x100) */
#define INUM_WRITE_ADDRESS_ERROR    (0x100 >> 5) /*  8  0x020  (vbr + 0x100) */
#define INUM_FPU_EXCEPTION	    (0x120 >> 5) /*  9  0x024  (vbr + 0x100) */
#define INUM_TLB_MULTIPLE_HIT	    (0x140 >> 5) /* 10  0x028  (0xa0000000)  */
#define INUM_TRAPA_INSTRUCTION	    (0x160 >> 5) /* 11  0x02c  (vbr + 0x100) */
#define INUM_ILLEGAL_INST_GENERAL   (0x180 >> 5) /* 12  0x030  (vbr + 0x100) */
#define INUM_ILLEGAL_INST_SLOT	    (0x1a0 >> 5) /* 13  0x034  (vbr + 0x100) */
#define INUM_NMI		    (0x1c0 >> 5) /* 14  0x038  (vbr + 0x600) */
#define INUM_USER_BREAK_TRAP	    (0x1e0 >> 5) /* 15  0x03c  (vbr + 0x100) */
#define INUM_EXC_HIGH		    (0x1e0 >> 5) /*  -exception upper limit- */
#define INUM_INTR_LOW		    (0x200 >> 5) /*  -interrupt lower limit- */
#define INUM_IRL15		    (0x200 >> 5) /* 16  0x040  (vbr + 0x600) */
#define INUM_IRL14		    (0x220 >> 5) /* 17  0x044  (vbr + 0x600) */
#define INUM_IRL13		    (0x240 >> 5) /* 18  0x048  (vbr + 0x600) */
#define INUM_IRL12		    (0x260 >> 5) /* 19  0x04c  (vbr + 0x600) */
#define INUM_IRL11		    (0x280 >> 5) /* 20  0x050  (vbr + 0x600) */
#define INUM_IRL10		    (0x2a0 >> 5) /* 21  0x054  (vbr + 0x600) */
#define INUM_IRL9		    (0x2c0 >> 5) /* 22  0x058  (vbr + 0x600) */
#define INUM_IRL8		    (0x2e0 >> 5) /* 23  0x05c  (vbr + 0x600) */
#define INUM_IRL7		    (0x300 >> 5) /* 24  0x060  (vbr + 0x600) */
#define INUM_IRL6		    (0x320 >> 5) /* 25  0x064  (vbr + 0x600) */
#define INUM_IRL5		    (0x340 >> 5) /* 26  0x068  (vbr + 0x600) */
#define INUM_IRL4		    (0x360 >> 5) /* 27  0x06c  (vbr + 0x600) */
#define INUM_IRL3		    (0x380 >> 5) /* 28  0x070  (vbr + 0x600) */
#define INUM_IRL2		    (0x3a0 >> 5) /* 29  0x074  (vbr + 0x600) */
#define INUM_IRL1		    (0x3c0 >> 5) /* 30  0x078  (vbr + 0x600) */
/*				    (0x3e0 >> 5)    31  0x07c  - reserved -  */
#define INUM_TMU0_UNDERFLOW	    (0x400 >> 5) /* 32  0x080  (vbr + 0x600) */
#define INUM_TMU1_UNDERFLOW	    (0x420 >> 5) /* 33  0x084  (vbr + 0x600) */
#define INUM_TMU2_UNDERFLOW	    (0x440 >> 5) /* 34  0x088  (vbr + 0x600) */
#define INUM_TMU2_INPUT_CAPTURE	    (0x460 >> 5) /* 35  0x08c  (vbr + 0x600) */
#define INUM_RTC_ALARM		    (0x480 >> 5) /* 36  0x090  (vbr + 0x600) */
#define INUM_RTC_PERIODIC	    (0x4a0 >> 5) /* 37  0x094  (vbr + 0x600) */
#define INUM_RTC_CARRY		    (0x4c0 >> 5) /* 38  0x098  (vbr + 0x600) */
#define INUM_SCI_RX_ERROR	    (0x4e0 >> 5) /* 39  0x09c  (vbr + 0x600) */
#define INUM_SCI_RX		    (0x500 >> 5) /* 40  0x0a0  (vbr + 0x600) */
#define INUM_SCI_TX		    (0x520 >> 5) /* 41  0x0a4  (vbr + 0x600) */
#define INUM_SCI_TX_END	    	    (0x540 >> 5) /* 42  0x0a8  (vbr + 0x600) */
#define INUM_WDT_INTERVAL_TIMER	    (0x560 >> 5) /* 43  0x0ac  (vbr + 0x600) */
#define INUM_BSC_REFRESH_CMI	    (0x580 >> 5) /* 44  0x0b0  (vbr + 0x600) */
#define INUM_BSC_REFRESH_OVF	    (0x5a0 >> 5) /* 45  0x0b4  (vbr + 0x600) */
/*				    (0x5c0 >> 5)    46  0x0b8  (vbr + 0x600) */
/*				    (0x5e0 >> 5)    47  0x0bc  (vbr + 0x600) */
#endif /* CPU==SH7750 || CPU==SH7700 */

#if (CPU==SH7750)
#define INUM_JTAG		    (0x600 >> 5) /* 48  0x0c0   */
#define INUM_GPIO		    (0x620 >> 5) /* 49  0x0c4  (vbr + 0x600) */
#define INUM_DMAC_DMTE0		    (0x640 >> 5) /* 50  0x0c8  (vbr + 0x600) */
#define INUM_DMAC_DMTE1		    (0x660 >> 5) /* 51  0x0cc  (vbr + 0x600) */
#define INUM_DMAC_DMTE2		    (0x680 >> 5) /* 52  0x0d0  (vbr + 0x600) */
#define INUM_DMAC_DMTE3		    (0x6a0 >> 5) /* 53  0x0d4  (vbr + 0x600) */
#define INUM_DMAC_DMTER		    (0x6c0 >> 5) /* 54  0x0d8  (vbr + 0x600) */
/*				    (0x6e0 >> 5)    55  0x0dc  (vbr + 0x600) */
#define INUM_SCIF2_ERROR	    (0x700 >> 5) /* 56  0x0e0  (vbr + 0x600) */
#define INUM_SCIF2_RX		    (0x720 >> 5) /* 57  0x0e4  (vbr + 0x600) */
#define INUM_SCIF2_BREAK	    (0x740 >> 5) /* 58  0x0e8  (vbr + 0x600) */
#define INUM_SCIF2_TX		    (0x760 >> 5) /* 59  0x0ec  (vbr + 0x600) */
#define INUM_SH_BUS		    (0x780 >> 5) /* 60  0x0f0  (vbr + 0x600) */
/*				    (0x7a0 >> 5)    61  0x0f4  (vbr + 0x600) */
/*				    (0x7c0 >> 5)    62  0x0f8  (vbr + 0x600) */
/*				    (0x7e0 >> 5)    63  0x0fc  (vbr + 0x600) */
#define INUM_FPU_DISABLE	    (0x800 >> 5) /* 64  0x100  (vbr + 0x600) */
#define INUM_SLOT_FPU_DISABLE	    (0x820 >> 5) /* 65  0x104  (vbr + 0x600) */
/*	 (0x840 >> 5) - 66 0x108 to (0xbe0 >> 5) - 79 0x13c  (vbr + 0x600)   */
#define INUM_PCI_SERR               (0xa00 >> 5) /* 80  0x140  (vbr + 0x600) */
#define INUM_PCI_DMA3               (0xa20 >> 5) /* 81  0x144  (vbr + 0x600) */
#define INUM_PCI_DMA2               (0xa40 >> 5) /* 82  0x148  (vbr + 0x600) */
#define INUM_PCI_DMA1               (0xa60 >> 5) /* 83  0x14c  (vbr + 0x600) */
#define INUM_PCI_DMA0               (0xa80 >> 5) /* 84  0x150  (vbr + 0x600) */
#define INUM_PCI_PWONREQ            (0xaa0 >> 5) /* 85  0x154  (vbr + 0x600) */
#define INUM_PCI_PWDWREQ            (0xac0 >> 5) /* 85  0x158  (vbr + 0x600) */
#define INUM_PCI_ERR                (0xae0 >> 5) /* 87  0x15c  (vbr + 0x600) */
#define INUM_TMU3_UNDERFLOW	    (0xb00 >> 5) /* 88  0x160  (vbr + 0x600) */
/*				    (0xb20 >> 5)    89  0x164  (vbr + 0x600) */
/*				    (0xb40 >> 5)    90  0x168  (vbr + 0x600) */
/*				    (0xb60 >> 5)    91  0x16c  (vbr + 0x600) */
#define INUM_TMU4_UNDERFLOW	    (0xb80 >> 5) /* 92  0x170  (vbr + 0x600) */

#endif /* CPU==SH7750 */

#if (CPU==SH7700)
#define INUM_IRQ0		    (0x600 >> 5) /* 48  0x0c0  (vbr + 0x600) */
#define INUM_IRQ1		    (0x620 >> 5) /* 49  0x0c4  (vbr + 0x600) */
#define INUM_IRQ2		    (0x640 >> 5) /* 50  0x0c8  (vbr + 0x600) */
#define INUM_IRQ3		    (0x660 >> 5) /* 51  0x0cc  (vbr + 0x600) */
#define INUM_IRQ4		    (0x680 >> 5) /* 52  0x0d0  (vbr + 0x600) */
#define INUM_IRQ5		    (0x6a0 >> 5) /* 53  0x0d4  (vbr + 0x600) */
/*				    (0x6c0 >> 5)    54  0x0d8  (vbr + 0x600) */
/*				    (0x6e0 >> 5)    55  0x0dc  (vbr + 0x600) */
#define INUM_PINT0_TO_PINT7	    (0x700 >> 5) /* 56  0x0e0  (vbr + 0x600) */
#define INUM_PINT8_TO_PINT15	    (0x720 >> 5) /* 57  0x0e4  (vbr + 0x600) */
/*				    (0x740 >> 5)    58  0x0e8  (vbr + 0x600) */
/*				    (0x760 >> 5)    59  0x0ec  (vbr + 0x600) */
/*				    (0x780 >> 5)    60  0x0f0  (vbr + 0x600) */
/*				    (0x7a0 >> 5)    61  0x0f4  (vbr + 0x600) */
/*				    (0x7c0 >> 5)    62  0x0f8  (vbr + 0x600) */
/*				    (0x7e0 >> 5)    63  0x0fc  (vbr + 0x600) */
#define INUM_DMAC0		    (0x800 >> 5) /* 64  0x100  (vbr + 0x600) */
#define INUM_DMAC1		    (0x820 >> 5) /* 65  0x104  (vbr + 0x600) */
#define INUM_DMAC2		    (0x840 >> 5) /* 66  0x108  (vbr + 0x600) */
#define INUM_DMAC3		    (0x860 >> 5) /* 67  0x10c  (vbr + 0x600) */
#define INUM_SCIF1_ERROR	    (0x880 >> 5) /* 68  0x110  (vbr + 0x600) */
#define INUM_SCIF1_RX		    (0x8a0 >> 5) /* 69  0x114  (vbr + 0x600) */
#define INUM_SCIF1_BREAK	    (0x8c0 >> 5) /* 70  0x118  (vbr + 0x600) */
#define INUM_SCIF1_TX		    (0x8e0 >> 5) /* 71  0x11c  (vbr + 0x600) */
#define INUM_SCIF2_ERROR	    (0x900 >> 5) /* 72  0x120  (vbr + 0x600) */
#define INUM_SCIF2_RX		    (0x920 >> 5) /* 73  0x124  (vbr + 0x600) */
#define INUM_SCIF2_BREAK	    (0x940 >> 5) /* 74  0x128  (vbr + 0x600) */
#define INUM_SCIF2_TX		    (0x960 >> 5) /* 75  0x12c  (vbr + 0x600) */
#define INUM_AD			    (0x980 >> 5) /* 76  0x130  (vbr + 0x600) */
#define INUM_LCD		    (0x9a0 >> 5) /* 77  0x134  (vbr + 0x600) */
#define INUM_PC_CARD0		    (0x9c0 >> 5) /* 78  0x138  (vbr + 0x600) */
#define INUM_PC_CARD1		    (0x9e0 >> 5) /* 79  0x13c  (vbr + 0x600) */
#define INUM_USB_H		    (0xa00 >> 5) /* 80  0x140  (vbr + 0x600) */
#define INUM_USB_F0		    (0xa20 >> 5) /* 81  0x142  (vbr + 0x600) */
#define INUM_USB_F1		    (0xa40 >> 5) /* 82  0x144  (vbr + 0x600) */
#endif /* CPU==SH7700 */

#if (CPU==SH7750 || CPU==SH7700)
#define INUM_INTR_HIGH		    (0xfe0 >> 5) /*127  0x1fc  -upper limit- */
#define INUM_TRAP_0			  0	 /*  0  0x000  -lower limit- */
#define INUM_TRAP_1			  1	 /*  1  0x004  (zero divide) */
#define INUM_TRAP_32			 32	 /*  32 0x080  (system call) */
#define INUM_TRAP_SC	       INUM_TRAP_32
#define INUM_TRAP_128			128	 /*128  0x200  (=0xfe0+0x20) */
#define INUM_TRAP_255			255	 /*255  0x3fc  -upper limit- */

#define TRAP_SC	(INUM_TRAP_SC << 2)	/* 0x80 (system call trap number)<<2 */

/*--------------------------------------------------------------------------*/

/* VBR-relative offsets (WRS private).  These definitions are shared by:
 * excALib, excArchLib, intArchLib, sigCtxALib, vxALib, windALib
 */

#define SH7700_NULL_EVT_CNT_OFFSET	0x000	/* spurious interrupt counter */
#define SH7700_INT_EVT_ADRS_OFFSET	0x004	/* INTEVT register address */
#define SH7700_ARE_WE_NESTED_OFFSET	0x008	/* areWeNested */
#define SH7700_INT_STACK_BASE_OFFSET	0x00c	/* intStackBase */
#define SH7700_MMU_VA_SHIFT_OFFSET	0x01c	/* mmuVaddrShift */
#define SH7700_MMU_VA_MASK_OFFSET	0x01e	/* mmuVaddrMask */
#define SH7700_DISPATCH_STUB_OFFSET	0x040	/* _dispatchStub: */
#define SH7700_INT_EXIT_STUB_OFFSET	0x080	/* _intExitStub: */
#define SH7700_EXC_BERR_STUB_OFFSET	0x0b0	/* _excBErrStub: */
#define SH7700_MEMPROBE_INT_STUB_OFFSET	0x0e0	/* _vxMemProbeIntStub: */
#define SH7700_EXC_STUB_OFFSET		0x100	/* EXception Vector offset  */
#define SH7700_TLB_STUB_OFFSET		0x400	/* TLB miss Vector offset   */
#define SH7700_INT_STUB_OFFSET		0x600	/* INTerrupt Vector offset  */
#define SH7700_VEC_TABLE_OFFSET		0x800	/* virtual vector table     */
#define SH7700_INT_PRIO_TABLE_OFFSET	0xc00	/* interrupt priority table */

#define IP_NULL_EVT_CNT			SH7700_NULL_EVT_CNT_OFFSET
#define IP_INT_EVT_ADRS			SH7700_INT_EVT_ADRS_OFFSET
#define IP_ARE_WE_NESTED		SH7700_ARE_WE_NESTED_OFFSET
#define IP_INT_STACK_BASE		SH7700_ARE_WE_NESTED_OFFSET
#define IP_MMU_VA_SHIFT			SH7700_MMU_VA_SHIFT_OFFSET
#define IP_MMU_VA_MASK			SH7700_MMU_VA_MASK_OFFSET
#define IP_DISPATCH_STUB		SH7700_DISPATCH_STUB_OFFSET
#define IP_INT_EXIT_STUB		SH7700_INT_EXIT_STUB_OFFSET
#define IP_EXC_BERR_STUB		SH7700_EXC_BERR_STUB_OFFSET
#define IP_MEMPROBE_INT_STUB		SH7700_MEMPROBE_INT_STUB_OFFSET

#define IP_TMP_MMUCR			0x020
#define IP_TMP_PTEL			0x024
#define IP_TMP_PTEH			0x028
#define IP_TMP_SP			0x02c

#define IP_TMP_R3			0x030
#define IP_TMP_R2			0x034
#define IP_TMP_R1			0x038
#define IP_TMP_R0			0x03c	/* vbr + 60 */
#define IP_TMP_FRAME			IP_TMP_R0 + 4

#ifndef _ASMLANGUAGE

/* macros to convert interrupt vectors <-> interrupt numbers */

#define IVEC_TO_INUM(intVec) \
		((int) ((UINT32)intVec - SH7700_VEC_TABLE_OFFSET) >> 2)
#define INUM_TO_IVEC(intNum) \
		((VOIDFUNCPTR *) (((intNum) << 2) + SH7700_VEC_TABLE_OFFSET))
#define INUM_TO_IEVT(intNum) \
		((int) ((intNum) << 5))
#define IEVT_TO_INUM(intEvt) \
		((int) ((intEvt) >> 5))

/* virtual interrupt vectors defined for SH7750/SH7700 */

#define IV_SCI_ERI		INUM_TO_IVEC (INUM_SCI_RX_ERROR)
#define IV_SCI_RXI		INUM_TO_IVEC (INUM_SCI_RX)
#define IV_SCI_TXI		INUM_TO_IVEC (INUM_SCI_TX)
#define IV_SCI_TEI		INUM_TO_IVEC (INUM_SCI_TX_END)

#define IV_SCIF_ERI		INUM_TO_IVEC (INUM_SCIF2_ERROR)
#define IV_SCIF_RXI		INUM_TO_IVEC (INUM_SCIF2_RX)
#define IV_SCIF_TXI		INUM_TO_IVEC (INUM_SCIF2_TX)
#define IV_SCIF_BRK		INUM_TO_IVEC (INUM_SCIF2_BREAK)

#define IV_SH_BUS_0		INUM_TO_IVEC (INUM_IRL11)
#define IV_SH_BUS_1		INUM_TO_IVEC (INUM_IRL10)
#define IV_PCIINTA              INUM_TO_IVEC (INUM_IRL15)
#define IV_PCIINTB              INUM_TO_IVEC (INUM_IRL14)
#define IV_PCIINTC              INUM_TO_IVEC (INUM_IRL13)
#define IV_PCIINTD              INUM_TO_IVEC (INUM_IRL12)

#define IV_USER_BREAK		INUM_TO_IVEC (INUM_USER_BREAK_TRAP)
#define	IV_TRAP(trapNum) \
		((VOIDFUNCPTR *) (((trapNum) << 2) + SH7700_VEC_TABLE_OFFSET))

#define IV_TRAP_RTI		IV_TRAP(251)	/* reserve 255-252 */

#endif /* _ASMLANGUAGE */

#endif /* CPU==SH7750 || CPU==SH7700 */


#if (CPU==SH7600 || CPU==SH7000)

/* vector numbers of specific exceptions */

#define LOW_VEC			4	/* lowest vector initialized  */
#define TRAP_32_VEC		32	/* start of user trap vectors */
#define USER_VEC_START		64	/* start of user interrupt vectors */
#if (CPU==SH7000)
#define HIGH_VEC		127	/* highest vector initialized */
#else
#define HIGH_VEC		255
#endif

/* macros to convert interrupt vectors <-> interrupt numbers */

#define IVEC_TO_INUM(intVec)	((int) (intVec) >> 2)
#define INUM_TO_IVEC(intNum)	((VOIDFUNCPTR *) ((intNum) << 2))

/* interrupt vector numbers */

#define	INUM_RESET_PWRON_PC		 0  /* Power-on reset PC              */
#define	INUM_RESET_PWRON_SP		 1  /* Power-on reset SP              */
#define	INUM_RESET_MANUAL_PC		 2  /* Manual reset PC                */
#define	INUM_RESET_MANUAL_SP		 3  /* Manual reset SP                */
#define	INUM_ILLEGAL_INST_GENERAL	 4  /* General illegal instruction    */
#define	INUM_RESERVED_5			 5  /*       (Reserved)               */
#define	INUM_ILLEGAL_INST_SLOT		 6  /* Illegal slot instruction       */
#define	INUM_RESERVED_7			 7  /*       (Reserved)               */
#define	INUM_RESERVED_8			 8  /*       (Reserved)               */
#define	INUM_BUS_ERROR_CPU		 9  /* CPU bus error                  */
#define	INUM_BUS_ERROR_DMA		10  /* DMA bus error                  */
#define	INUM_NMI			11  /* Non-maskable interrupt         */
#define	INUM_USER_BREAK			12  /* User break interrupt           */
/*				        13-31        (Reserved)               */
					/* Trap instruction (user vectors)    */
#define	INUM_TRAP_32			32  /* used for system call */
#define	INUM_TRAP_SC	      INUM_TRAP_32
#define	INUM_TRAP_33			33
#define	INUM_TRAP_34			34
#define	INUM_TRAP_35			35
#define	INUM_TRAP_36			36
#define	INUM_TRAP_37			37
#define	INUM_TRAP_38			38
#define	INUM_TRAP_39			39
#define	INUM_TRAP_40			40
#define	INUM_TRAP_41			41
#define	INUM_TRAP_42			42
#define	INUM_TRAP_43			43
#define	INUM_TRAP_44			44
#define	INUM_TRAP_45			45
#define	INUM_TRAP_46			46
#define	INUM_TRAP_47			47
#define	INUM_TRAP_48			48
#define	INUM_TRAP_49			49
#define	INUM_TRAP_50			50
#define	INUM_TRAP_51			51
#define	INUM_TRAP_52			52
#define	INUM_TRAP_53			53
#define	INUM_TRAP_54			54
#define	INUM_TRAP_55			65
#define	INUM_TRAP_56			56
#define	INUM_TRAP_57			57
#define	INUM_TRAP_58			58
#define	INUM_TRAP_59			59  /* reserved for RTI tools */
#define	INUM_TRAP_60			60  /* reserved */
#define	INUM_TRAP_61			61  /* reserved */
#define	INUM_TRAP_62			62  /* used for zero divide trap */
#define	INUM_TRAP_63			63  /* used by dbgShLib.h */

/* interrupt vector definitions */

#define	IV_RESET_PWRON_PC	INUM_TO_IVEC (INUM_RESET_PWRON_PC)
#define	IV_RESET_PWRON_SP	INUM_TO_IVEC (INUM_RESET_PWRON_SP)
#define	IV_RESET_MANUAL_PC	INUM_TO_IVEC (INUM_RESET_MANUAL_PC)
#define	IV_RESET_MANUAL_SP	INUM_TO_IVEC (INUM_RESET_MANUAL_SP)
#define	IV_ILLEGAL_INST_GENERAL	INUM_TO_IVEC (INUM_ILLEGAL_INST_GENERAL)
#define	IV_RESERVED_5		INUM_TO_IVEC (INUM_RESERVED_5)
#define	IV_ILLEGAL_INST_SLOT	INUM_TO_IVEC (INUM_ILLEGAL_INST_SLOT)
#define	IV_RESERVED_7		INUM_TO_IVEC (INUM_RESERVED_7)
#define	IV_RESERVED_8		INUM_TO_IVEC (INUM_RESERVED_8)
#define	IV_BUS_ERROR_CPU	INUM_TO_IVEC (INUM_BUS_ERROR_CPU)
#define	IV_BUS_ERROR_DMA	INUM_TO_IVEC (INUM_BUS_ERROR_DMA)
#define	IV_NMI			INUM_TO_IVEC (INUM_NMI)
#define	IV_USER_BREAK		INUM_TO_IVEC (INUM_USER_BREAK)
#define	IV_TRAP(trapNum)	INUM_TO_IVEC (trapNum)

#define IV_TRAP_RTI		IV_TRAP(INUM_TRAP_59)

#endif /* CPU==SH7600 || CPU==SH7000 */

#ifdef __cplusplus
}
#endif

#endif /* __INCivShh */
