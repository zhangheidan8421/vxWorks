/* asmSh.h - SH assembler definitions header file */

/* Copyright 1994-2004 Wind River Systems, Inc. */
/*
modification history
--------------------
01e,08sep04,h_k  added _SH7750 condition.
01d,23aug00,hk   change to use default _ALIGN_TEXT for SH7040(merged to SH7600).
01c,19apr00,hk   added alignment macro _ALIGN_COPY_TEXT for VBR-relative stubs.
01b,17mar00,zl   added alignment macro _ALIGN_TEXT
01a,21sep94,sa   derived from version 01i of mc68k/asmMc68k.h.
*/

#ifndef __INCasmShh
#define __INCasmShh

#ifdef __cplusplus
extern "C" {
#endif

#if (CPU==SH7750) || defined (_SH7750)
#define _ALIGN_TEXT		5		/* 32 byte aligned */
#else	/* CPU != SH7750 && !defined (_SH7750) */
#define _ALIGN_TEXT		4		/* 16 byte alignment */
#endif	/* CPU==SH7750 || defined (_SH7750) */

#define _ALIGN_COPY_TEXT	2		/* for text to be copied */

#ifdef __cplusplus
}
#endif

#endif /* __INCasmShh */
