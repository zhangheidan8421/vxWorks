/* sh7760.h - SH7760 header */

/* Copyright 2002-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,11apr03,???  Merge GPIO and DMA register definitions.
01b,05mar03,rgp  added CAN device and clocking register defs
01a,17oct02,h_k  written.
*/

/*
DESCRIPTION
This file contains I/O addresses and related constants for the SH7760 chip.
*/

#ifndef __INCsh7760h
#define __INCsh7760h

#ifdef __cplusplus
extern "C" {
#endif


/* INTC (INTerrupt Controller) register addresses */

#define INTC_IPRD	((volatile UINT16 *)0xffd00010)	/* priority setup D */
#define INT_PRI_00	((volatile UINT32 *)0xfe080000)	/* priority setup 00 */
#define INT_PRI_04	((volatile UINT32 *)0xfe080004)	/* priority setup 04 */
#define INT_PRI_08	((volatile UINT32 *)0xfe080008)	/* priority setup 08 */
#define INT_PRI_0C	((volatile UINT32 *)0xfe08000c)	/* priority setup 0C */
#define INT_REQ_00	((volatile UINT32 *)0xfe080020)	/* int source 00     */
#define INT_REQ_04	((volatile UINT32 *)0xfe080024)	/* int source 04     */
#define INT_MSK_00	((volatile UINT32 *)0xfe080040)	/* int mask 00       */
#define INT_MSK_04	((volatile UINT32 *)0xfe080044)	/* int mask 04       */
#define INT_MSKCLR_00	((volatile UINT32 *)0xfe080060)	/* int mask clear 00 */
#define INT_MSKCLR_04	((volatile UINT32 *)0xfe080064)	/* int mask clear 04 */

/* INTC INTREQ00, INTMSK00 and INTMSKCLR00 bit definitions */

#define INT00_IRQ4		((UINT32)0x80000000)
#define INT00_IRQ5		((UINT32)0x40000000)
#define INT00_IRQ6		((UINT32)0x20000000)
#define INT00_IRQ7		((UINT32)0x10000000)
#define INT00_IRQ	(INT00_IRQ4 | INT00_IRQ5 | INT00_IRQ6 | INT00_IRQ7)
#define INT00_HCANI0		((UINT32)0x02000000)
#define INT00_HCANI1		((UINT32)0x01000000)
#define INT00_HCAN2	(INT00_HCANI0 | INT00_HCANI1)
#define INT00_SSII0		((UINT32)0x00800000)
#define INT00_SSII1		((UINT32)0x00400000)
#define INT00_SSI	(INT00_SSII0 | INT00_SSII1)
#define INT00_AC97I0		((UINT32)0x00200000)
#define INT00_AC97I1		((UINT32)0x00100000)
#define INT00_AC97	(INT00_AC97I0 | INT00_AC97I1)
#define INT00_IICI0		((UINT32)0x00080000)
#define INT00_IICI1		((UINT32)0x00040000)
#define INT00_IIC	(INT00_IICI0 | INT00_IICI1)
#define INT00_USB		((UINT32)0x00020000)
#define INT00_LCDC		((UINT32)0x00010000)
#define INT00_DMABRGI0		((UINT32)0x00004000)
#define INT00_DMABRGI1		((UINT32)0x00002000)
#define INT00_DMABRGI2		((UINT32)0x00001000)
#define INT00_DMABRG	(INT00_DMABRGI0 | INT00_DMABRGI1 | INT00_DMABRGI2)
#define INT00_ERI0		((UINT32)0x00000800)
#define INT00_RXI0		((UINT32)0x00000400)
#define INT00_BRI0		((UINT32)0x00000200)
#define INT00_TXI0		((UINT32)0x00000100)
#define INT00_SCIF0	(INT00_ERI0 | INT00_RXI0 | INT00_BRI0 | INT00_TXI0)
#define INT00_ERI1		((UINT32)0x00000080)
#define INT00_RXI1		((UINT32)0x00000040)
#define INT00_BRI1		((UINT32)0x00000020)
#define INT00_TXI1		((UINT32)0x00000010)
#define INT00_SCIF1	(INT00_ERI1 | INT00_RXI1 | INT00_BRI1 | INT00_TXI1)
#define INT00_ERI2		((UINT32)0x00000008)
#define INT00_RXI2		((UINT32)0x00000004)
#define INT00_BRI2		((UINT32)0x00000002)
#define INT00_TXI2		((UINT32)0x00000001)
#define INT00_SCIF2	(INT00_ERI2 | INT00_RXI2 | INT00_BRI2 | INT00_TXI2)
#define INT00_ALL	(INT00_IRQ | INT00_HCAN2 | INT00_SSI | INT00_AC97 | \
			 INT00_IIC | INT00_USB | INT00_LCDC | INT00_DMABRG | \
			 INT00_SCIF0 | INT00_SCIF1 | INT00_SCIF2)

/* INTC INTREQ04, INTMSK04 and INTMSKCLR04 bit definitions */
#define INT04_SIM_ERI		((UINT32)0x00800000)
#define INT04_SIM_RXI		((UINT32)0x00400000)
#define INT04_SIM_TXI		((UINT32)0x00200000)
#define INT04_SIM_TEI		((UINT32)0x00100000)
#define INT04_SIM	(INT04_SIM_ERI | INT04_SIM_RXI | INT04_SIM_TXI | \
			 INT04_SIM_TEI)
#define INT04_SPI		((UINT32)0x00080000)
#define INT04_MMCI0		((UINT32)0x00040000)
#define INT04_MMCI1		((UINT32)0x00020000)
#define INT04_MMCI2		((UINT32)0x00010000)
#define INT04_MMCI3		((UINT32)0x00008000)
#define INT04_MMCIF	(INT04_MMCI0 | INT04_MMCI1 | INT04_MMCI2 | INT04_MMCI3)
#define INT04_MFI		((UINT32)0x00000040)
#define INT04_FLSTE		((UINT32)0x00000020)
#define INT04_FLTEND		((UINT32)0x00000010)
#define INT04_FLTRQ0		((UINT32)0x00000008)
#define INT04_FLTRQ1		((UINT32)0x00000004)
#define INT04_FLCTL	(INT04_FLSTE | INT04_FLTEND | INT04_FLTRQ0 | \
			 INT04_FLTRQ1)
#define INT04_ADC		((UINT32)0x00000002)
#define INT04_CMT		((UINT32)0x00000001)
#define INT04_ALL	(INT04_SIM | INT04_SPI | INT04_MMCIF | INT04_MFI | \
			 INT04_FLCTL | INT04_ADC | INT04_CMT)


/* SCIF (Serial Communication Interface with FIFO) register addresses */

#define SCIF0_SMR	((volatile UINT16 *)0xfe600000)	/* serial mode        */
#define SCIF0_BRR	((volatile UINT8  *)0xfe600004)	/* bit rate           */
#define SCIF0_SCR	((volatile UINT16 *)0xfe600008)	/* serial control     */
#define SCIF0_FTDR	((volatile UINT8  *)0xfe60000c)	/* transmit FIFO data */
#define SCIF0_FSR	((volatile UINT16 *)0xfe600010)	/* serial status      */
#define SCIF0_FRDR	((volatile UINT8  *)0xfe600014)	/* receive FIFO data  */
#define SCIF0_FCR	((volatile UINT16 *)0xfe600018)	/* FIFO control       */
#define SCIF0_TFDR	((volatile UINT16 *)0xfe60001c)	/* tra FIFO data count*/
#define SCIF0_RFDR	((volatile UINT16 *)0xfe600020)	/* rec FIFO data count*/
#define SCIF0_SPTR	((volatile UINT16 *)0xfe600024)	/* serial port reg    */
#define SCIF0_LSR	((volatile UINT16 *)0xfe600028)	/* line status        */
#define SCIF0_RER	((volatile UINT16 *)0xfe60002c)	/* serial error reg   */

#define SCIF1_SMR	((volatile UINT16 *)0xfe610000)	/* serial mode        */
#define SCIF1_BRR	((volatile UINT8  *)0xfe610004)	/* bit rate           */
#define SCIF1_SCR	((volatile UINT16 *)0xfe610008)	/* serial control     */
#define SCIF1_FTDR	((volatile UINT8  *)0xfe61000c)	/* transmit FIFO data */
#define SCIF1_FSR	((volatile UINT16 *)0xfe610010)	/* serial status      */
#define SCIF1_FRDR	((volatile UINT8  *)0xfe610014)	/* receive FIFO data  */
#define SCIF1_FCR	((volatile UINT16 *)0xfe610018)	/* FIFO control       */
#define SCIF1_TFDR	((volatile UINT16 *)0xfe61001c)	/* tra FIFO data count*/
#define SCIF1_RFDR	((volatile UINT16 *)0xfe610020)	/* rec FIFO data count*/
#define SCIF1_SPTR	((volatile UINT16 *)0xfe610024)	/* serial port reg    */
#define SCIF1_LSR	((volatile UINT16 *)0xfe610028)	/* line status        */
#define SCIF1_RER	((volatile UINT16 *)0xfe61002c)	/* serial error reg   */

#define SCIF2_SMR	((volatile UINT16 *)0xfe620000)	/* serial mode        */
#define SCIF2_BRR	((volatile UINT8  *)0xfe620004)	/* bit rate           */
#define SCIF2_SCR	((volatile UINT16 *)0xfe620008)	/* serial control     */
#define SCIF2_FTDR	((volatile UINT8  *)0xfe62000c)	/* transmit FIFO data */
#define SCIF2_FSR	((volatile UINT16 *)0xfe620010)	/* serial status      */
#define SCIF2_FRDR	((volatile UINT8  *)0xfe620014)	/* receive FIFO data  */
#define SCIF2_FCR	((volatile UINT16 *)0xfe620018)	/* FIFO control       */
#define SCIF2_TFDR	((volatile UINT16 *)0xfe62001c)	/* tra FIFO data count*/
#define SCIF2_RFDR	((volatile UINT16 *)0xfe620020)	/* rec FIFO data count*/
#define SCIF2_SPTR	((volatile UINT16 *)0xfe620024)	/* serial port reg    */
#define SCIF2_LSR	((volatile UINT16 *)0xfe620028)	/* line status        */
#define SCIF2_RER	((volatile UINT16 *)0xfe62002c)	/* serial error reg   */



/* HCAN2 controlers base addresses and register offsets (incomplete) */

#define HCAN0_MCR       ((volatile UINT16 *)0xfe380000) /* module control     */
#define HCAN0_GSR       ((volatile UINT16 *)0xfe380002) /* general status     */
#define HCAN0_BCR1      ((volatile UINT16 *)0xfe380004) /* bit timing config l*/
#define HCAN0_BCR0      ((volatile UINT16 *)0xfe380006) /* bit timing config 0*/
#define HCAN0_IRR       ((volatile UINT16 *)0xfe380008) /* interrupt          */
#define HCAN0_IMR       ((volatile UINT16 *)0xfe38000a) /* interrupt mask     */
#define HCAN0_TECREC    ((volatile UINT16 *)0xfe38000c) /* tx error/rx error  */
#define HCAN0_TXPR1     ((volatile UINT16 *)0xfe380020) /* tx pending reg 1   */
#define HCAN0_TXPR0     ((volatile UINT16 *)0xfe380022) /* tx pending reg 0   */
#define HCAN0_TXCR1     ((volatile UINT16 *)0xfe380028) /* tx cancel reg 1    */
#define HCAN0_TXCR0     ((volatile UINT16 *)0xfe38002a) /* tx cancel reg 0    */
#define HCAN0_TXACK1    ((volatile UINT16 *)0xfe380030) /* tx ack reg 1       */
#define HCAN0_TXACK0    ((volatile UINT16 *)0xfe380032) /* tx ack reg 0       */
#define HCAN0_ABACK1    ((volatile UINT16 *)0xfe380038) /* abort ack reg 1    */
#define HCAN0_ABACK0    ((volatile UINT16 *)0xfe38003a) /* abort ack reg 0    */
#define HCAN0_RXPR1     ((volatile UINT16 *)0xfe380040) /* rx pending reg 1   */
#define HCAN0_RXPR0     ((volatile UINT16 *)0xfe380042) /* rx pending reg 0   */
#define HCAN0_RFPR1     ((volatile UINT16 *)0xfe380048) /* rem. pending reg 1 */
#define HCAN0_RFPR0     ((volatile UINT16 *)0xfe38004a) /* rem. pending reg 0 */
#define HCAN0_MBIMR1    ((volatile UINT16 *)0xfe380050) /* mb intr. mask reg 1*/
#define HCAN0_MBIMR0    ((volatile UINT16 *)0xfe380052) /* mb intr. mask reg 0*/
#define HCAN0_UMSR1     ((volatile UINT16 *)0xfe380058) /* unread status reg 1*/
#define HCAN0_UMSR0     ((volatile UINT16 *)0xfe38005a) /* unread status reg 0*/
#define HCAN0_TCNTR     ((volatile UINT16 *)0xfe380080) /* timer counter      */
#define HCAN0_TCR       ((volatile UINT16 *)0xfe380082) /* timer control      */
#define HCAN0_TSR       ((volatile UINT16 *)0xfe380084) /* timer prescalar    */
#define HCAN0_TDCR      ((volatile UINT16 *)0xfe380086) /* timer drift        */
#define HCAN0_LOSR      ((volatile UINT16 *)0xfe380088) /* local offset       */
#define HCAN0_MBRx_ADDRESS(mbrn) \
                        ((volatile UINT16 *)((char*)(0xfe380000) + \
                                             (0x100+(mbrn*32))))

#define HCAN1_MCR       ((volatile UINT16 *)0xfe390000) /* module control     */
#define HCAN1_GSR       ((volatile UINT16 *)0xfe390002) /* general status     */
#define HCAN1_BCR1      ((volatile UINT16 *)0xfe390004) /* bit timing config l*/
#define HCAN1_BCR0      ((volatile UINT16 *)0xfe390006) /* bit timing config 0*/
#define HCAN1_IRR       ((volatile UINT16 *)0xfe390008) /* interrupt          */
#define HCAN1_IMR       ((volatile UINT16 *)0xfe39000a) /* interrupt mask     */
#define HCAN1_TECREC    ((volatile UINT16 *)0xfe39000c) /* tx error/rx error  */
#define HCAN1_TXPR1     ((volatile UINT16 *)0xfe390020) /* tx pending reg 1   */
#define HCAN1_TXPR0     ((volatile UINT16 *)0xfe390022) /* tx pending reg 0   */
#define HCAN1_TXCR1     ((volatile UINT16 *)0xfe390028) /* tx cancel reg 1    */
#define HCAN1_TXCR0     ((volatile UINT16 *)0xfe39002a) /* tx cancel reg 0    */
#define HCAN1_TXACK1    ((volatile UINT16 *)0xfe390030) /* tx ack reg 1       */
#define HCAN1_TXACK0    ((volatile UINT16 *)0xfe390032) /* tx ack reg 0       */
#define HCAN1_ABACK1    ((volatile UINT16 *)0xfe390038) /* abort ack reg 1    */
#define HCAN1_ABACK0    ((volatile UINT16 *)0xfe39003a) /* abort ack reg 0    */
#define HCAN1_RXPR1     ((volatile UINT16 *)0xfe390040) /* rx pending reg 1   */
#define HCAN1_RXPR0     ((volatile UINT16 *)0xfe390042) /* rx pending reg 0   */
#define HCAN1_RFPR1     ((volatile UINT16 *)0xfe390048) /* rem. pending reg 1 */
#define HCAN1_RFPR0     ((volatile UINT16 *)0xfe39004a) /* rem. pending reg 0 */
#define HCAN1_MBIMR1    ((volatile UINT16 *)0xfe390050) /* mb intr. mask reg 1*/
#define HCAN1_MBIMR0    ((volatile UINT16 *)0xfe390052) /* mb intr. mask reg 0*/
#define HCAN1_UMSR1     ((volatile UINT16 *)0xfe390058) /* unread status reg 1*/
#define HCAN1_UMSR0     ((volatile UINT16 *)0xfe39005a) /* unread status reg 0*/
#define HCAN1_TCNTR     ((volatile UINT16 *)0xfe390080) /* timer counter      */
#define HCAN1_TCR       ((volatile UINT16 *)0xfe390082) /* timer control      */
#define HCAN1_TSR       ((volatile UINT16 *)0xfe390084) /* timer prescalar    */
#define HCAN1_TDCR      ((volatile UINT16 *)0xfe390086) /* timer drift        */
#define HCAN1_LOSR      ((volatile UINT16 *)0xfe390088) /* local offset       */
#define HCAN1_MBRx_ADDRESS(mbrn) \
                        ((volatile UINT16 *)((char*)(0xfe390000) + \
                                             (0x100+(mbrn*32))))


/* Clock Pulse Generator (CPG) module */

#define CPG_FRQCR       ((volatile UINT16 *)0xffc00000) /* freq control reg  */
#define CPG_STBCR       ((volatile UINT8  *)0xffc00004) /* standby control   */
#define CPG_WTCNT       ((volatile UINT16 *)0xffc00008) /* watchdog counter  */
#define CPG_WTCSR       ((volatile UINT16 *)0xffc0000c) /* watchdog control  */
#define CPG_STBCR2      ((volatile UINT8  *)0xffc00010) /* standby control 2 */
#define CPG_CLKSTP00    ((volatile UINT32 *)0xfe0a0000) /* clock stop        */
#define CPG_CLKSTPCLR00 ((volatile UINT32 *)0xfe0a0010) /* clock stop clear  */
#define CPG_DCKDR       ((volatile UINT32 *)0xfe0a0020) /* clk division reg  */
#define CPG_MCKCR       ((volatile UINT32 *)0xfe0a0024) /* module clk control*/

/* --------------------------------------------------------------------------*/
/* CLKSTP module assignments                               bit  module       */
/* --------------------------------------------------------------------------*/
#define CLKSTP_INTC     ((UINT32)0x00000001)            /* 0    INTC         */
#define CLKSTP_UNDEF0   ((UINT32)0x0000001e)            /* 1-4  unused       */
#define CLKSTP_FCLK     ((UINT32)0x00000020)            /* 5    FCLK (FLCTL) */
#define CLKSTP_UNDEF1   ((UINT32)0x000000c0)            /* 6-7  unused       */
#define CLKSTP_DMAC     ((UINT32)0x00000100)            /* 8    DMAC         */
#define CLKSTP_MMCIF    ((UINT32)0x00000200)            /* 9    MMCIF        */
#define CLKSTP_GPIO     ((UINT32)0x00000400)            /* 10   GPIO         */
#define CLKSTP_DMABRG   ((UINT32)0x00000800)            /* 11   DMABRG       */
#define CLKSTP_HCAN0    ((UINT32)0x00001000)            /* 12   HCAN 0       */
#define CLKSTP_HCAN1    ((UINT32)0x00002000)            /* 13   HCAN 1       */
#define CLKSTP_ADC      ((UINT32)0x00004000)            /* 14   ADC          */
#define CLKSTP_AC97_0   ((UINT32)0x00008000)            /* 15   AC97 0       */
#define CLKSTP_AC97_1   ((UINT32)0x00010000)            /* 16   AC97 1       */
#define CLKSTP_CMT      ((UINT32)0x00020000)            /* 17   CMT          */
#define CLKSTP_FLCTL    ((UINT32)0x00040000)            /* 18   FLCTL        */
#define CLKSTP_USB      ((UINT32)0x00080000)            /* 19   USB          */
#define CLKSTP_LCDC     ((UINT32)0x00100000)            /* 20   LCDC         */
#define CLKSTP_UNDEF2   ((UINT32)0x00200000)            /* 21   unused       */
#define CLKSTP_SPI      ((UINT32)0x00400000)            /* 22   SPI          */
#define CLKSTP_MFI      ((UINT32)0x00800000)            /* 23   MFI          */
#define CLKSTP_SIM      ((UINT32)0x01000000)            /* 24   SIM          */
#define CLKSTP_IIC0     ((UINT32)0x02000000)            /* 25   IIC 0        */
#define CLKSTP_IIC1     ((UINT32)0x04000000)            /* 26   IIC 1        */
#define CLKSTP_SCIF0    ((UINT32)0x08000000)            /* 27   SCIF 0       */
#define CLKSTP_SCIF1    ((UINT32)0x10000000)            /* 28   SCIF 1       */
#define CLKSTP_SCIF2    ((UINT32)0x20000000)            /* 29   SCIF 2       */
#define CLKSTP_SSI0     ((UINT32)0x40000000)            /* 30   SSI 0        */
#define CLKSTP_SSI1     ((UINT32)0x80000000)            /* 31   SSI 1        */

/* GPIO Registers*/
#define GPIO_PCCR   ((volatile UINT16 *)0xfe400008) /* port C ctrl register */
#define GPIO_PDCR   ((volatile UINT16 *)0xfe40000C) /* port D ctrl register */
#define GPIO_PECR   ((volatile UINT16 *)0xfe400010) /* port E ctrl register */ 
/* Peripheral Module Select Register*/
#define PFC_IPSELR      ((volatile UINT16 *)0xfe400034) 
/* The registers related to the DMA controller*/
#define DMA_DMAOR ((volatile UINT32 *)0xFFA00040)
#define DMA_DMATCR1 ((volatile UINT32 *)0xFFA00018)
#define	DMA_CHCR1 ((volatile UINT32 *)0xFFA0001C)
#define	DMA_DMAOR ((volatile UINT32 *)0xFFA00040)
#define	DMA_DMARCR ((volatile UINT32 *)0xFE090008)	/*Bridge Enable*/
#define	DMA_DMARSRA ((volatile UINT32 *)0xFE090000)
#define	DMA_DMABRGCR ((volatile UINT32 *)0xFE3C0000)


/* interrupt vector numbers */

/*---------------------------------------------------------------------------*/
/*      exception/interrupt name     (code >> 5)   No. vector  entry point   */
/*---------------------------------------------------------------------------*/
#define INUM_DMAC_DMTE4             (0x780 >> 5) /* 60  0x0f0  (vbr + 0x600) */
#define INUM_DMAC_DMTE5             (0x7a0 >> 5) /* 61  0x0f4  (vbr + 0x600) */
#define INUM_DMAC_DMTE6             (0x7c0 >> 5) /* 62  0x0f8  (vbr + 0x600) */
#define INUM_DMAC_DMTE7             (0x7e0 >> 5) /* 63  0x0fc  (vbr + 0x600) */
#define INUM_IRQ4                   (0x800 >> 5) /* 64  0x100  (vbr + 0x600) */
#define INUM_IRQ5                   (0x820 >> 5) /* 65  0x104  (vbr + 0x600) */
#define INUM_IRQ6                   (0x840 >> 5) /* 66  0x108  (vbr + 0x600) */
#define INUM_IRQ7                   (0x860 >> 5) /* 67  0x10c  (vbr + 0x600) */
#define INUM_SCIF0_ERI              (0x880 >> 5) /* 68  0x110  (vbr + 0x600) */
#define INUM_SCIF0_RXI              (0x8a0 >> 5) /* 69  0x114  (vbr + 0x600) */
#define INUM_SCIF0_BRI              (0x8c0 >> 5) /* 70  0x118  (vbr + 0x600) */
#define INUM_SCIF0_TXI              (0x8e0 >> 5) /* 71  0x11c  (vbr + 0x600) */
#define INUM_SHCAN2_CANI0           (0x900 >> 5) /* 72  0x120  (vbr + 0x600) */
#define INUM_SHCAN2_CANI1           (0x920 >> 5) /* 73  0x124  (vbr + 0x600) */
#define INUM_SSI_SSI0               (0x940 >> 5) /* 74  0x128  (vbr + 0x600) */
#define INUM_SSI_SSI1               (0x960 >> 5) /* 75  0x12c  (vbr + 0x600) */
#define INUM_AC97_AC97I0            (0x980 >> 5) /* 76  0x130  (vbr + 0x600) */
#define INUM_AC97_AC97I1            (0x9a0 >> 5) /* 77  0x134  (vbr + 0x600) */
#define INUM_IIC_IICI0              (0x9c0 >> 5) /* 78  0x138  (vbr + 0x600) */
#define INUM_IIC_IICI1              (0x9e0 >> 5) /* 79  0x13c  (vbr + 0x600) */
#define INUM_USB_USBI               (0xa00 >> 5) /* 80  0x140  (vbr + 0x600) */
#define INUM_LCDC_VINT              (0xa20 >> 5) /* 81  0x144  (vbr + 0x600) */
/*                                  (0xa40 >> 5)    82  0x148  (vbr + 0x600) */
/*                                  (0xa60 >> 5)    83  0x14c  (vbr + 0x600) */
#define INUM_DMABRG_DMABRGI0        (0xa80 >> 5) /* 84  0x150  (vbr + 0x600) */
#define INUM_DMABRG_DMABRGI1        (0xaa0 >> 5) /* 85  0x154  (vbr + 0x600) */
#define INUM_DMABRG_DMABRGI2        (0xac0 >> 5) /* 86  0x158  (vbr + 0x600) */
/*                                  (0xae0 >> 5)    87  0x15c  (vbr + 0x600) */
#define INUM_SCIF1_ERI              (0xb00 >> 5) /* 88  0x160  (vbr + 0x600) */
#define INUM_SCIF1_RXI              (0xb20 >> 5) /* 89  0x164  (vbr + 0x600) */
#define INUM_SCIF1_BRI              (0xb40 >> 5) /* 90  0x168  (vbr + 0x600) */
#define INUM_SCIF1_TXI              (0xb60 >> 5) /* 91  0x16c  (vbr + 0x600) */
#define INUM_SCIF2_ERI              (0xb80 >> 5) /* 92  0x170  (vbr + 0x600) */
#define INUM_SCIF2_RXI              (0xba0 >> 5) /* 93  0x174  (vbr + 0x600) */
#define INUM_SCIF2_BRI              (0xbc0 >> 5) /* 94  0x178  (vbr + 0x600) */
#define INUM_SCIF2_TXI              (0xbe0 >> 5) /* 95  0x17c  (vbr + 0x600) */
#define INUM_SIM_ERI                (0xc00 >> 5) /* 96  0x180  (vbr + 0x600) */
#define INUM_SIM_RXI                (0xc20 >> 5) /* 97  0x184  (vbr + 0x600) */
#define INUM_SIM_TXI                (0xc40 >> 5) /* 98  0x188  (vbr + 0x600) */
#define INUM_SIM_TEI                (0xc60 >> 5) /* 99  0x18c  (vbr + 0x600) */
#define INUM_SPI_SPII               (0xc80 >> 5) /* 100 0x190  (vbr + 0x600) */
/*                                  (0xca0 >> 5)    101 0x194  (vbr + 0x600) */
/*                                  (0xcc0 >> 5)    102 0x198  (vbr + 0x600) */
/*                                  (0xce0 >> 5)    103 0x19c  (vbr + 0x600) */
#define INUM_MMCIF_MMCI0            (0xd00 >> 5) /* 104 0x1a0  (vbr + 0x600) */
#define INUM_MMCIF_MMCI1            (0xd20 >> 5) /* 105 0x1a4  (vbr + 0x600) */
#define INUM_MMCIF_MMCI2            (0xd40 >> 5) /* 106 0x1a8  (vbr + 0x600) */
#define INUM_MMCIF_MMCI3            (0xd60 >> 5) /* 107 0x1ac  (vbr + 0x600) */
/*                                  (0xd80 >> 5)    108 0x1b0  (vbr + 0x600) */
/*                                  (0xda0 >> 5)    109 0x1b4  (vbr + 0x600) */
/*                                  (0xdc0 >> 5)    110 0x1b8  (vbr + 0x600) */
/*                                  (0xde0 >> 5)    111 0x1bc  (vbr + 0x600) */
/*                                  (0xe00 >> 5)    112 0x1c0  (vbr + 0x600) */
/*                                  (0xe20 >> 5)    113 0x1c4  (vbr + 0x600) */
/*                                  (0xe40 >> 5)    114 0x1c8  (vbr + 0x600) */
/*                                  (0xe60 >> 5)    115 0x1cc  (vbr + 0x600) */
#define INUM_MFI_MFII               (0xe80 >> 5) /* 116 0x1d0  (vbr + 0x600) */
/*                                  (0xea0 >> 5)    117 0x1d4  (vbr + 0x600) */
/*                                  (0xec0 >> 5)    118 0x1d8  (vbr + 0x600) */
/*                                  (0xee0 >> 5)    119 0x1dc  (vbr + 0x600) */
#define INUM_FLCTL_FLSTE            (0xf00 >> 5) /* 120 0x1e0  (vbr + 0x600) */
#define INUM_FLCTL_FLTEND           (0xf20 >> 5) /* 121 0x1e4  (vbr + 0x600) */
#define INUM_FLCTL_FLTRQ0           (0xf40 >> 5) /* 122 0x1e8  (vbr + 0x600) */
#define INUM_FLCTL_FLTRQ1           (0xf60 >> 5) /* 123 0x1ec  (vbr + 0x600) */
#define INUM_ADC_ADI                (0xf80 >> 5) /* 124 0x1f0  (vbr + 0x600) */
#define INUM_CMT_CMTI               (0xfa0 >> 5) /* 125 0x1f4  (vbr + 0x600) */


/* virtual interrupt vectors defined */

#define IV_IRQ0			INUM_TO_IVEC (INUM_IRL15)
#define IV_IRQ1			INUM_TO_IVEC (INUM_IRL14)
#define IV_IRQ2			INUM_TO_IVEC (INUM_IRL13)
#define IV_IRQ3			INUM_TO_IVEC (INUM_IRL12)
#define IV_IRQ4			INUM_TO_IVEC (INUM_IRL11)
#define IV_IRQ5			INUM_TO_IVEC (INUM_IRL10)
#define IV_IRQ6			INUM_TO_IVEC (INUM_IRL9)
#define IV_IRQ7			INUM_TO_IVEC (INUM_IRL8)
#define IV_IRQ8			INUM_TO_IVEC (INUM_IRL7)
#define IV_IRQ9			INUM_TO_IVEC (INUM_IRL6)
#define IV_IRQ10		INUM_TO_IVEC (INUM_IRL5)
#define IV_IRQ11		INUM_TO_IVEC (INUM_IRL4)
#define IV_IRQ12		INUM_TO_IVEC (INUM_IRL3)
#define IV_IRQ13		INUM_TO_IVEC (INUM_IRL2)
#define IV_IRQ14		INUM_TO_IVEC (INUM_IRL1)

#define IV_SCIF1_ERI		INUM_TO_IVEC (INUM_SCIF1_ERI)
#define IV_SCIF1_RXI		INUM_TO_IVEC (INUM_SCIF1_RXI)
#define IV_SCIF1_BRI		INUM_TO_IVEC (INUM_SCIF1_BRI)
#define IV_SCIF1_TXI		INUM_TO_IVEC (INUM_SCIF1_TXI)


#ifdef __cplusplus
}
#endif

#endif /* __INCsh7760h */
