/* esf.h - SH exception stack frames */

/* Copyright 1994-2004 Wind River Systems, Inc. */
/*
modification history
--------------------
01i,06may04,h_k  added pteh in ESFSH.
01h,22oct02,hk   updated comment on ESFSH structure.
01g,31aug01,hk   put PTEH.ASID in upper half of event field in ESFSH.
01f,21aug00,hk   merge SH7729 to SH7700.
01e,16jul98,st   added SH7750 support.
01d,06may98,jmc  added support for SH-DSP and SH3-DSP CPU types.
01c,17feb97,hk   added event to ESFSH for SH7700, renamed aa to info.
01b,09feb97,hk   added aa to ESFSH for SH7700.
01a,30oct94,hk   taken from 68k 01h.
*/

#ifndef __INCesfShh
#define __INCesfShh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef	_ASMLANGUAGE

/* Exception stack frames.  Most of these can happen only for one
   CPU or another, but they are all defined for all CPU's */

/* ESFSH: just one stack frame defined for SH architecture. */

typedef struct
    {
    INSTR *pc;		/* program counter */
    ULONG sr;		/* status register */
#if (CPU==SH7750 || CPU==SH7700)
    ULONG pteh;		/* page table entry high */
    UINT32 event;	/* (MMUCR.AT << 31) | (PTEH.ASID << 16) | (EXPEVT) */
    UINT32 info;	/* TRA/TEA/FPSCR register */
#endif
    } ESFSH;

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCesfShh */
