/* mmuShLib.h - mmuLib header for SH77xx */

/* 
 * Copyright (c) 1984-2005 Wind River Systems, Inc. 
 *
 * The right to copy, distribute, modify or otherwise make use 
 * of this software may be licensed only pursuant to the terms 
 * of an applicable Wind River license agreement. 
 */

/*
modification history
--------------------
01n,02mar05,h_k  added macros and changed PTE structure for PTEA support. (SPR
                 #106555)
01m,29oct04,h_k  added lock API. (SPR #102634)
01l,27aug04,h_k  updated for AIM MMU based on mmuMipsLib.h.
01k,04dec03,h_k  enabled MMU_GLOBAL_CONTEXT definition for SH7700.
01j,05sep03,h_k  merged mmuShLib.h from AE.
01i,03aug00,hk   merged mmuSh7750Lib.h. disabled unused MMUCR_REG definition.
01h,18apr00,zl   updated for little endian support
01g,30sep98,hk   restored MMUCR_REG by hms, deleted union.
01f,29sep98,hk   deleted MMU_STATE_CACHEABLE_WRITETHROUGH. changed MMU_STATE_
		 MASK_CACHEABLE for SH7750 to 0x9. defaulted MMU_STATE_CACHEABLE
		 for SH7750 to MMU_STATE_CACHEABLE_COPYBACK.
01e,22sep98,hms  changed MMU_STATE_MASK_WT, MMU_STATE_WT_ON to 
		 MMU_STATE_MASK_WRITETHROUGH,
                 MMU_MMU_STATE_CACHEABLE_WRITETHROUGH.
                 deleted MMU_STATE_WT_OFF.
01d,17sep98,hk   merged mmuSh7750Lib.h by hms. added comments.
01c,28dec96,hk   code review.
01b,25dec96,hk   disabled MMUCR_REG def. changed indent.
01a,15nov96,wt   written based on mmu30Lib.h-01d.
*/

#ifndef __INCmmuShLibh
#define __INCmmuShLibh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _ASMLANGUAGE
#include "aimMmuLib.h"
#endif

/* Defines */

#define	MMU_SH_LOCK_API				/* Enable Lock APIs */

/* default RTP base address */

#define MMU_SH_RTP_BASE_ADRS	0x40000000	/* within U0 region and above
						 * 29 bit address space.
						 */

/* TLB definitions */

#define MMU_SH_TLB_ENTRY_NB		64	/* 64 entries */
#define MMU_SH_UNLOCK_TLB_ENTRY(x)	(MMU_SH_TLB_ENTRY_NB - x)
#define MMU_SH_TLB_ENTIRE		-1	/* invalidate entire TLB */

/* MMU register definitions */

#define MMU_PTEH	((volatile UINT32 *)0xff000000)	/* PTEH address */
#define MMU_MMUCR	((volatile UINT32 *)0xff000010) /* MMUCR address */

/* MMUCR (memory management unit controler) register bit definitions */

#define MMU_MMUCR_URX_MASK	(MMU_SH_TLB_ENTRY_NB - 1)
#define MMU_MMUCR_URB_SHIFT	18		/* urb start bit */
#define MMU_MMUCR_URC_SHIFT	10		/* urc start bit */
#define MMU_MMUCR_URB_MASK	(MMU_MMUCR_URX_MASK << MMU_MMUCR_URB_SHIFT)
#define MMU_MMUCR_URC_MASK	(MMU_MMUCR_URX_MASK << MMU_MMUCR_URC_SHIFT)

/* Page table sizes */

#define MMU_REGION_TBL_SIZE	1024	/* number of region table entries */

/* PTE definitions */

#define MMU_PTE_SIZE_SHIFT	3	/* log2 (sizeof (PTE)) */

/*------ Protection, Caching, Mask Tables -------------------------*/

#define PTEL_VALID			0x00000100
#define PTEL_VALID_NOT			0x00000000
#define PTEL_INVALID			0xFFFFFEFF
#define PTEL_PROTECT_ALL_WRITABLE	0x00000060
#define PTEL_PROTECT_ALL_READONLY	0x00000040
#define PTEL_PROTECT_SUP_WRITABLE	0x00000020
#define PTEL_PROTECT_SUP_READONLY	0x00000000
#define PTEL_CACHEABLE_WRITETHRU	0x00000009
#define PTEL_CACHEABLE_COPYBACK		0x00000008
#define PTEL_CACHEABLE			0x00000008
#define PTEL_CACHEABLE_NOT		0x00000000

#define PTEL_MASK_VALID			0x00000100
#define PTEL_MASK_PROTECT		0x00000060
#define PTEL_MASK_CACHE			0x00000009	/* C | WT bits */
#define PTEL_MASK_CACHEABLE		PTEL_CACHEABLE

#define PTEL_MASK_PPN			0x1FFFFC00

/* These bits are reserved on PTEL and internally used for storing PTEA bits */

#define PTEL_PTEA_VALID                 0x00000200      /* internally used */
#define PTEL_PTEA_MASK                  0xe0000200      /* internally used */
#define PTEL_PTEA_SA_MASK               0xe0000000      /* internally used */
#define PTEL_PTEA_TC_MASK               0x00000200      /* internally used */

/* PTEA assistant MASK Tables */

#define PTEA_MASK                       0x0000000f      /*  TC | SA */
#define PTEA_TC_MASK                    0x00000008      /* PTEA.TC */
#define PTEA_SA_MASK                    0x00000007      /* PTEA.SA[2:0] */

#define PTEA_STATE_SHIFT                12		/* least bit of
							 * MMU_ATTR_SPL_MASK
							 */
#define PTEA_STATE_MASK                 (PTEA_MASK << PTEA_STATE_SHIFT)
							/* (TC | SA) << 12 */

#define PTEA_TC_TRANS_SHIFT             6
#define PTEA_SA_TRANS_SHIFT             29

/* Context table information */

#define MMU_GLOBAL_CONTEXT	0
#define MMU_MIN_CONTEXT		0
#define MMU_MAX_CONTEXT		255

/* Page size support information */

#define MMU_PAGE_SIZES_ALLOWED  (MMU_PAGE_MASK_1K | MMU_PAGE_MASK_4K | \
				 MMU_PAGE_MASK_64K | MMU_PAGE_MASK_1M)

/* SH Hardware TLB definitions */

#define TLB_1K_PAGE_SIZE	0x00000400
#define TLB_4K_PAGE_SIZE	0x00001000
#define TLB_64K_PAGE_SIZE	0x00010000
#define TLB_1M_PAGE_SIZE	0x00100000

#define TLB_1K_PAGE_SIZE_MASK	0x00000000 
#define TLB_4K_PAGE_SIZE_MASK	0x00000C00 
#define TLB_64K_PAGE_SIZE_MASK	0x0000FC00 
#define TLB_1M_PAGE_SIZE_MASK	0x000FFC00 

/* SH TLB definitions
 * must not exceed 0x7fff. (See mmuStub() and cacheSh7750ILineInvalOp().)
 */

#define TLB_1K_VADDR_MASK	(0x0FFF << MMU_PTE_SIZE_SHIFT)
#define TLB_4K_VADDR_MASK	(0x03FF << MMU_PTE_SIZE_SHIFT)
#define TLB_64K_VADDR_MASK	(0x003F << MMU_PTE_SIZE_SHIFT)
#define TLB_1M_VADDR_MASK	(0x0003 << MMU_PTE_SIZE_SHIFT)

/* Vertual address shift definitions
 * must be negative value. (See mmuStub() and cacheSh7750ILineInvalOp().)
 */

#define TLB_1K_VADDR_SHIFT	(MMU_PTE_SIZE_SHIFT - 10)
#define TLB_4K_VADDR_SHIFT	(MMU_PTE_SIZE_SHIFT - 12)
#define TLB_64K_VADDR_SHIFT	(MMU_PTE_SIZE_SHIFT - 16)
#define TLB_1M_VADDR_SHIFT	(MMU_PTE_SIZE_SHIFT - 20)

/* Page Table Size defines
 * These define the page table size for each of the page sizes.
 * Use the smallest page size supported
 */

#define TLB_1M_TABLE_SIZE	4
#define TLB_64K_TABLE_SIZE	(16 * TLB_1M_TABLE_SIZE)
#define TLB_4K_TABLE_SIZE	(16 * TLB_64K_TABLE_SIZE)
#define TLB_1K_TABLE_SIZE	(4 * TLB_4K_TABLE_SIZE)

/* State Defines */

#define MMU_SH_BASE_STATE		(1 << MMU_SH_DIRTY_START)

/* PTEL Defines */

#define MMU_SH_STATE_VALID		PTEL_VALID	/* V flag set      */
#define MMU_SH_STATE_VALID_NOT		PTEL_VALID_NOT	/* V flag not set  */
#define MMU_SH_STATE_WRITABLE		PTEL_PROTECT_SUP_WRITABLE
#define MMU_SH_STATE_WRITABLE_NOT	PTEL_PROTECT_SUP_READONLY
#define MMU_SH_STATE_INVALID_STATE	PTEL_INVALID	/* Invalid State   */
#define MMU_SH_GLOBAL			0x00000002	/* SH flag set      */
#define MMU_SH_UNCACHED			PTEL_CACHEABLE_NOT
#define MMU_SH_CACHED			PTEL_CACHEABLE

/* SZ filed */
#define MMU_SH_SZ_1K_PAGE		0x00000000	/* 1K page size  */
#define MMU_SH_SZ_4K_PAGE		0x00000010	/* 4K page size  */
#define MMU_SH_SZ_64K_PAGE		0x00000080	/* 64K page size */
#define MMU_SH_SZ_1M_PAGE		0x00000090	/* 1M page size  */

#define MMU_SH_GLOBAL_START		1	/* Start point of global bit */
#define MMU_SH_DIRTY_START		2	/* Start point of dirty bit  */
#define MMU_SH_CACHE_START		3	/* Start point of cache field*/
#define MMU_SH_VALID_START		8	/* Start point of valid bit  */

/* Masks */

#define MMU_SH_STATE_MASK_VALID		PTEL_MASK_VALID
#define MMU_SH_STATE_MASK_WRITABLE	PTEL_PROTECT_SUP_WRITABLE
#define MMU_SH_STATE_MASK_PROTECTION	PTEL_PROTECT_ALL_WRITABLE
#define MMU_SH_STATE_MASK_CACHEABLE	PTEL_MASK_CACHEABLE
#define MMU_SH_STATE_MASK_CACHE		PTEL_MASK_CACHE
#define MMU_SH_STATE_MASK		(MMU_SH_STATE_MASK_VALID | \
					 MMU_SH_STATE_MASK_PROTECTION | \
					 MMU_SH_STATE_MASK_CACHE)
#define MMU_SH_MASK_GLOBAL		MMU_SH_GLOBAL
#define MMU_SH_SZ_MASK			0x00000090

#define MMU_SH_VPN_MASK			0xfffffc00
#define MMU_SH_PPN_MASK			PTEL_MASK_PPN
#define MMU_SH_ASID_MASK		0x000000FF

/*
 * Table Entry Definitions
 * These are definitions for the indexes for the cache, protection
 * and mask tables defined for every device-dependent library
 */

/* Cache Table Indexes */

#define MMU_INVALID		0
#define MMU_CACHEOFF		1
#define MMU_CPYBACK		2
#define MMU_WRTHRU		3
#define MMU_COHRNT		4

#ifndef _ASMLANGUAGE

#define	MMU_BYTES_PER_ENTRY	sizeof (PTE)	/* size of PTE structure */

/* Define the Page-Table Entry (PTE) Structure */

typedef struct
    {
#if (_BYTE_ORDER == _BIG_ENDIAN)
    UINT32  locked:1;
    UINT32  reserved2:2;
    UINT32  pageSize:21;
    UINT32  reserved1:4;	/* keep the bits always 0 (see mmuShALib.s) */
    UINT32  assist:4;		/* LSB */
#else	/* _BYTE_ORDER == _LITTLE_ENDIAN */
    UINT32  assist:4;		/* LSB */
    UINT32  reserved1:4;	/* keep the bits always 0 (see mmuShALib.s) */
    UINT32  pageSize:21;
    UINT32  reserved2:2;
    UINT32  locked:1;
#endif	/* _BYTE_ORDER == _BIG_ENDIAN */
    } PTE_ASSIST;

typedef struct
    {
    UINT32	entry;
    union
	{
	PTE_ASSIST fields;
	UINT32  bits;
	} ea;	/* entry assistant */
    } PTE;

/* Define the TLB Structure. */

typedef struct
    {
    UINT32      utlbAddr;		/* UTLB address array */
    UINT32      utlbData1;		/* UTLB data array 1 */
    } SH_TLB_ENTRY;

/* TLB Setup structure
 *
 * This structure holds the information needed by the TLB Miss Handler
 * to setup the TLB entry.
 */

typedef struct
    {
    UINT32 pageSize;
    UINT32 tableSize;
    UINT16 vAddrShift;
    UINT16 vAddrMask;
    } SH_TLB_SETUP_STRUCT;

/*
 * This is the structure used to configure the mmu library
 * for the varying SH architectures.	
 */

typedef struct mmuShConfigStruct
    {
    UINT32 kMemBase;			/* Kernel Memory Base */

#ifdef	MMU_SH_LOCK_API
    UINT lockedTlbCnt;
    UINT pciLockedTlbCnt;
#endif	/* MMU_SH_LOCK_API */

    /* Pointers to subroutines for SH device-specific needs */

    VOIDFUNCPTR CrSet;
    VOIDFUNCPTR On;
    VOIDFUNCPTR TlbClr;
    VOIDFUNCPTR ATTRSet;
    VOIDFUNCPTR CTXSet;			/* Set Context */
    VOIDFUNCPTR TTBSet;
    VOIDFUNCPTR Dump;

#ifdef	MMU_SH_LOCK_API
    VOIDFUNCPTR Move;
    VOIDFUNCPTR TlbSet;
#endif	/* MMU_SH_LOCK_API */
    } MMU_SH_CONFIG;

#endif  /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCmmuShLibh */
