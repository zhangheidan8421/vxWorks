/* excShLib.h - exception library header */

/* Copyright 1994-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
02b,02dec04,h_k  removed excFormatDbl and excFormatFlt prototype. (SPR #104932)
02a,26nov03,pes  Correct _WRS_IS_SUPV_EXC macro.
01z,18nov03,pes  Add _WRS_IS_SUPV_EXC macro.
01y,22oct01,hk   added excPciMapInit() prototype for SH7751 virtual PCI support.
01x,27sep01,hk   inserted asid to EXC_INFO, changed vecNum field to UINT8.
01w,21aug00,hk   merge SH7729 to SH7700. merge SH7410 and SH7040 to SH7600.
01v,12jul00,hk   added excFormatFlt()/excFormatDbl() function prototypes.
01u,20apr00,hk   declared excBErrVecInit() and _func_excBErrIntAck.
01t,16jul98,st   added SH7750 support.
01s,06may98,jmc  added support for the SH-DSP and SH3-DSP CPU types.
01r,12may98,hk   added EXC_FPSCR def.
01q,25nov97,hk   changed comment for vecNum in EXC_INFO.
01p,22apr97,hk   changed 704X to 7040.
01o,17feb97,hk   changed accessAddr to info in EXC_INFO. added EXC_TRAP def.
01n,04jan97,hk   added accessAddr and pad to EXC_INFO. def'd EXC_ACCESS_ADDR.
01m,15nov96,wt   added declarations for mmuStub[] and mmuStubSize.
01l,09aug96,hk   moved HIGH_VEC, LOW_VEC, TRAP_32_VEC, USER_VEC_START to ivSh.h.
01k,08aug96,hk   localized TRAP_32_VEC/USER_VEC_START to non-SH3 cpus. changed
		 HIGH_VEC for SH7700. deleted SH7700_TRA, SH7700_EXPEVT, and
		 SH7700_INTEVT. also localized excBsrTblBErr/excBsrTbl to non-
		 SH3 cpus.
01j,29jul96,hk   deleted declarations of intStub/intStubSize.
01i,11jul96,ja   added SH7700_OFF_EXP SH7700_OFF_TLB SH7700_OFF_INT for SH7700.
01h,13jun96,hk   added LOW_VEC and HIGH_VEC for SH7700.
01g,21may96,hk   deleted HIGH_VEC for SH7700.
01f,13may96,hk   added support for SH7700.
01e,18dec95,hk   added support for SH704X.
01d,03jul95,hk   fixed HIGH_VEC to 127 from 255.
01c,27mar95,hk   added bus error support. copyright 1995.
01b,30oct94,hk   EXC_INFO has been truncated to 12 bytes in alpha stage.
		 renamed excType to vecNum, EXC_TYPE to EXC_VEC_NUM.
		 added LOW_VEC, TRAP_32_VEC, USER_VEC_START, HIGH_VEC.
		 added excBsrTbl[] declaration.
01a,29jun94,hk   written (EXC_INFO is temporalily adjusted to 20 bytes.)
*/

#ifndef __INCexcShLibh
#define __INCexcShLibh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _ASMLANGUAGE

/* generic exception information -
 * kept in the tcb of tasks that have caused exceptions
 */
typedef struct
    {
    UINT16 valid;		/* indicators that following fields are valid */
    UINT8  vecNum;		/* Vector Number (0 - 255) */
    UINT8  asid;		/* Address Space ID (0 - 255) */
    INSTR *pc;			/* Program Counter */
    UINT32 sr;			/* Status Register */
    UINT32 info;		/* TRA/TEA/FPSCR Register */
    UINT32 pad;			/* (reserved for future use) */
    } EXC_INFO;

/* exception info valid bits */

#define EXC_VEC_NUM		0x01	/* vector number valid */
#define EXC_PC			0x02	/* pc valid */
#define EXC_STATUS_REG		0x04	/* status register valid */
#define EXC_ACCESS_ADDR		0x08	/* info: access address valid */
#define EXC_TRAP		0x10	/* info: TRA register valid */
#define EXC_FPSCR		0x20	/* info: fpscr valid */
#define EXC_ASID		0x40	/* asid valid */

#define _WRS_IS_SUPV_EXC() (((excInfo.sr & SR_BIT_MD) == SR_BIT_MD) ? TRUE : FALSE)

/* variable declarations */

extern FUNCPTR  excExcepHook;   /* add'l rtn to call when exceptions occur */

#if	(CPU==SH7600 || CPU==SH7000)
extern int	excBsrTblBErr [];	/* bus error entry in excALib.s */
extern int	excBsrTbl [];		/* table of BSRs in excALib.s */
#elif	(CPU==SH7750 || CPU==SH7700)
extern int	excStub [];		/* Exception handler (excALib) */
extern int	excStubSize;
extern int	mmuStub [];		/* TLB mishit handler (excALib) */
extern int	mmuStubSize;
#endif	/* CPU==SH7750 || CPU==SH7700 */
#if	(CPU==SH7750)
extern int	mmuPciStub [];	/* TLB mishit handler for SH7751 (excALib) */
extern int	mmuPciStubSize;
extern int	mmuPciStubParams;
extern int	mmuPciIoStub[];
extern int	mmuPciIoStubSize;
extern int	mmuPciIoStubParams;
extern int	mmuStubProper[];
extern int	mmuStubProperSize;
#endif	/* CPU==SH7750 */

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern STATUS 	excBErrVecInit (int inum);
extern void	(* _func_excBErrIntAck)(void);
#if (CPU==SH7750)
extern STATUS	excPciMapInit (void *virtAddr, void *physAddr, UINT len,
				void *virtIoAddr, void *physIoAddr, UINT lenIo);
#endif /* CPU==SH7750 */

#else

extern STATUS 	excBErrVecInit ();
extern VOIDFUNCPTR	_func_excBErrIntAck;
#if (CPU==SH7750)
extern STATUS	excPciMapInit ();
#endif /* CPU==SH7750 */

#endif	/* __STDC__ */

#endif /* ! _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCexcShLibh */
