/* dspShLib.h - SH Version of DSP Include File */

/* Copyright 1984-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,15oct04,h_k  added externs for PAL coprocessor abstraction.
01c,31aug98,kab  added MOD reg.
01b,08aug98,kab  filled in dspContext struct.
01a,22jul98,mem  written.
*/

#ifndef __INCdspShLibh
#define __INCdspShLibh

#ifdef __cplusplus
extern "C" {
#endif

#include "reg.h"
#include "vxWorks.h"

/* DSP_CONTEXT structure offsets */
#define DSPREG_SET_RS		0x00
#define DSPREG_SET_RE		0x04
#define DSPREG_SET_DSR		0x08
#define DSPREG_SET_A0		0x0c
#define DSPREG_SET_A1		0x10
#define DSPREG_SET_A0G		0x14
#define DSPREG_SET_A1G		0x18
#define DSPREG_SET_M0		0x1c
#define DSPREG_SET_M1		0x20
#define DSPREG_SET_X0		0x24
#define DSPREG_SET_X1		0x28
#define DSPREG_SET_Y0		0x2c
#define DSPREG_SET_Y1		0x30
#define DSPREG_SET_MOD		0x34

/* DSP DSR configurations */
#define DSR_CS_OFFSET		1
#define DSR_CARRY_MODE		(0x0 << DSR_CS_OFFSET)
#define DSR_NEGATIVE_MODE	(0x1 << DSR_CS_OFFSET)
#define DSR_ZERO_MODE		(0x2 << DSR_CS_OFFSET)
#define DSR_OVERFLOW_MODE	(0x3 << DSR_CS_OFFSET)
#define DSR_SIGNED_GT_MODE	(0x4 << DSR_CS_OFFSET)
#define DSR_SIGNED_GE_MODE	(0x5 << DSR_CS_OFFSET)

#ifndef _ASMLANGUAGE

typedef struct dspContext		/* Dsp Context */
    {
    ULONG 	rs;			/* repeat start reg */
    ULONG 	re;			/* repeat end reg */
    ULONG 	dsr;			/* DSP status reg */
    ULONG 	a0;			/* A0 data register */
    ULONG 	a1;			/* A1 data register */
    ULONG 	a0g;			/* A0 Guard register */
    ULONG 	a1g;			/* A1 Guard register */
    ULONG 	m0;			/* M0 data register */
    ULONG 	m1;			/* M1 data register */
    ULONG 	x0;			/* X0 data register */
    ULONG 	x1;			/* X1 data register */
    ULONG 	y0;			/* Y0 data register */
    ULONG 	y1;			/* Y1 data register */
    ULONG	mod;			/* MODulo register */
    } DSP_CONTEXT;

#define DSPREG_SET	DSP_CONTEXT

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

IMPORT void	dspArchInit (void);
IMPORT void	dspArchTaskCreateInit (DSP_CONTEXT *pDspContext);
IMPORT void     dspFlushInit (void);
IMPORT UINT     dspQueueInit (void);
IMPORT STATUS   dspProbeSup (void);
IMPORT void     dspSave (DSP_CONTEXT *);
IMPORT void     dspRestore (DSP_CONTEXT *);
IMPORT STATUS   dspEnable (void);
IMPORT STATUS   dspDisable (void);
IMPORT void     dspCtxShow (DSP_CONTEXT *);
IMPORT STATUS   dspCtxDelete (DSP_CONTEXT *);
IMPORT DSP_CONTEXT * dspCtxCreate (int);

#else

IMPORT void	dspArchInit ();
IMPORT void	dspArchTaskCreateInit ();
IMPORT void     dspFlushInit ();
IMPORT UINT     dspQueueInit ();
IMPORT STATUS   dspProbeSup ();
IMPORT void     dspSave ();
IMPORT void     dspRestore ();
IMPORT STATUS   dspEnable ();
IMPORT STATUS   dspDisable ();
IMPORT void     dspCtxShow ();
IMPORT STATUS   dspCtxDelete ();
IMPORT DSP_CONTEXT * dspCtxCreate ();

#endif	/* __STDC__ */

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCdspShLibh */
