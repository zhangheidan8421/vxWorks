/* cacheShLib.h - SH cache library header file */

/* Copyright 1994-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
02k,28aug04,h_k  added SH_Px_BASE for AIM MMU Lib.
02j,02apr04,h_k  added CACHE_SH4A_MODE. (SPR #96128)
02i,09feb04,h_k  added SH_PC_INVAL_BASE.
02h,04nov03,h_k  added SH_P4_BASE.
02g,26aug03,h_k  added SH_PHYS_MASK.
02f,05aug03,h_k  added cacheSh7710LibInit (). (SPR #90073)
02e,05jun01,frf  added CACHE_2WAY_MODE for enhanced Sh7751R mode.
02d,01feb01,frf  added definition for SH7726.
02c,06dec00,hk   delete CCR-related and unused definitions for SH7750.
02c,06nov00,zl   added cacheSh7622LibInit and SH7622_CACHE_THRU.
02b,27sep00,zl   exclude function declarations for asm files.
02a,09sep00,hk   merge SH7729 to SH7700.
01z,23aug00,hk   merge SH7410 to SH7600. get together function prototypes.
01y,06aug00,hk   merged SH7040 to SH7600. hide local definitions for SH7700.
01x,31may00,frf  SH7751 for T2
01w,05mar99,hk   minor clean-up for CPU conditional and comment.
01v,20sep98,hk   code review: moved-up SH7750 section. simplified caching modes.
		 changed CCR_[IO]C_FLUSH to CCR_[IO]C_INVALIDATE.
		 changed CCR_[IO]C_INDEX_MODE to CCR_[IO]C_INDEX_ENABLE.
		 disabled unused defs for SH7750 store queue control.
01u,16jul98,st   added SH7750 support.
01v,24jun98,jmc  added definition of SH7729 cache control register 2.
01u,08may98,jmc  added support for SH-DSP and SH3-DSP.
01t,06feb98,jmc  renamed macro CACHE_WRITEBACK_P1 to CACHE_COPYBACK_P1.
01s,30jan98,jmc  added CACHE_WRITEBACK_P1 for write-back cache configuration of
                 P1 region and CCR bit CCR_WRITE_BACK_P1 for same, SH7709 only.
01r,19jun97,hk   fixed SH7040_CAC_ADRS_ARRAY/SH7040_CAC_DATA_ARRAY addresses.
01q,22apr97,hk   changed 704X to 7040.
01p,16feb97,hk   added CACHE_1WAY_MODE and CCR_1WAY_MODE for SH7702.
                 changed CCR_RAM_MODE to CCR_2WAY_MODE for SH7708.
01o,18jan97,hk   added CACHE_DMA_BYPASS_P[013]. made cacheArchXXX local for
                 SH7700. added cacheArchInvalidate().
01n,30dec96,hk   renamed SH7700_P* to SH7700_P?_BASE.
01m,05sep96,hk   renamed SH7700_CACHE_THRU to SH7700_P2_CACHE_THRU. added more
		 macros for SH7700.
01l,23aug96,hk   changed code layout. added CCR bit defs for SH7700. also added
		 CACHE_2WAY_MODE for SH7700/SH7600.
01k,21may96,hk   added SH7700 def.
01j,18jan96,hk   changed cacheSh704XLibInit() to cacheSh704xLibInit().
01i,14jan96,hk   added CCR_ENABLED for SH704X. deleted SH7000 stuff.
		 changed C_PURGE_ADRS to SH7604_CACHE_PURGE.
01h,19dec95,hk   added support for SH704X.
01g,21nov95,hk   changed SH to Sh.
01f,21nov95,hk   renamed SH7000 to SH7032, SH7600 to SH7604.
01e,16aug95,hk   added prototypes for cache{SH7000|SH7600}LibInit().
01d,23may95,hk   made SH7604_CACHE_CCR to volatile SH7604_CCR, copyright to '95.
01c,09noV94,sa   added C_ENABLE, ... , C_PURGE, C_PURGE_ADRS.
                 added cacheArchDmaMalloc(), cacheArchDmaFree();
01b,21sep94,sa   added SH_CACHE_CCR.
01a,21sep94,sa   drrived from version 02b of arch/mc68k/cacheMc68kLib.h.
*/

#ifndef __INCcacheShLibh
#define __INCcacheShLibh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"


#if (CPU==SH7750) /*==========================================================*/

/* Additional caching mode */
#define CACHE_RAM_MODE		0x00000100	/* use half of cache as RAM */
#define CACHE_DMA_BYPASS_P0	0x00000200	/* allocate on P2, free to P0 */
#define CACHE_DMA_BYPASS_P1	0x00000400	/* allocate on P2, free to P1 */
#define CACHE_DMA_BYPASS_P3	0x00000800	/* allocate on P2, free to P3 */
#define CACHE_A25_INDEX		0x00001000	/* use A25 as cache index MSB */
#define CACHE_COPYBACK_P1	0x00002000	/* set P1 to copy-back */
#define CACHE_2WAY_MODE		0x00004000	/* enhanced mode: 2way assoc. */
#define CACHE_SH4A_MODE		0x00008000	/* SH4A cache support */
						   
#define SH7750_PHYS_MASK	0x1fffffff
#define SH7750_P1_BASE		0x80000000	/* cacheable,     bypass mmu */
#define SH7750_P2_BASE		0xa0000000	/* non cacheable, bypass mmu */
#define SH7750_P3_BASE		0xc0000000	/* cacheable */
#define SH7750_P4_BASE		0xe0000000	/* non cacheable, bypass mmu */
#define SH_PHYS_MASK		SH7750_PHYS_MASK
#define SH_P1_BASE		SH7750_P1_BASE
#define SH_P2_BASE		SH7750_P2_BASE
#define SH_P3_BASE		SH7750_P3_BASE
#define SH_P4_BASE		SH7750_P4_BASE
#define SH_PC_INVAL_BASE	SH_P4_BASE	/* pc invalidate base address */

/* These are referenced by hmse7751.h */

#define SH7751_PHYS_MASK        0x1fffffff
#define SH7751_P1_BASE          0x80000000      /* cacheable,     bypass mmu */
#define SH7751_P2_BASE          0xa0000000      /* non cacheable, bypass mmu */
#define SH7751_P3_BASE          0xc0000000      /* cacheable */
#define SH7751_P4_BASE          0xe0000000      /* non cacheable, bypass mmu */


#elif (CPU==SH7700) /*===========================================*/

/* Additional caching mode for SH7700 */
#define CACHE_2WAY_MODE		0x00000100	/* use half of cache as RAM */
#define CACHE_DMA_BYPASS_P0	0x00000200	/* allocate on P2, free to P0 */
#define CACHE_DMA_BYPASS_P1	0x00000400	/* allocate on P2, free to P1 */
#define CACHE_DMA_BYPASS_P3	0x00000800	/* allocate on P2, free to P3 */
#define CACHE_1WAY_MODE		0x00001000	/* SH7702 only */
#define CACHE_COPYBACK_P1	0x00002000	/* SH7729/SH7709 only */

#define SH7700_PHYS_MASK	0x1fffffff
#define SH7700_P1_BASE		0x80000000	/* cacheable (write thru) */
#define SH7700_P2_BASE		0xa0000000	/* non cacheable */
#define SH7700_P3_BASE		0xc0000000	/* cacheable */
#define SH7700_P4_BASE		0xe0000000	/* non cacheable */
#define SH_PHYS_MASK		SH7700_PHYS_MASK
#define SH_P1_BASE		SH7700_P1_BASE
#define SH_P2_BASE		SH7700_P2_BASE
#define SH_P3_BASE		SH7700_P3_BASE
#define SH_P4_BASE		SH7700_P4_BASE
#define SH_PC_INVAL_BASE	SH_P4_BASE	/* pc invalidate base address */


#elif (CPU==SH7600) /*=========================================*/

/* Additional caching mode for SH7604 */
#define CACHE_2WAY_MODE		0x100		/* use half of cache as RAM */
#define CACHE_COPYBACK_P1	0x00002000	/* set P1 to copy-back
						   only for SH7622  */

/* Address space definitions */
#define SH7604_CACHE_THRU	0x20000000
#define SH7622_CACHE_THRU	0xa0000000
#define SH7622_PHYS_MASK   	0x1fffffff
#define SH_PHYS_MASK		SH7622_PHYS_MASK
#define SH_PC_INVAL_BASE	0xc0000000	/* pc invalidate base address */

/* Additional caching mode for SH7040 */
#define CACHE_SH7040_DRAM	0x1000
#define CACHE_SH7040_CS3	0x0800
#define CACHE_SH7040_CS2	0x0400
#define CACHE_SH7040_CS1	0x0200
#define CACHE_SH7040_CS0	0x0100

#endif /* CPU==SH7600 =========================================*/

#ifndef _ASMLANGUAGE

#if defined(__STDC__) || defined(__cplusplus)
extern STATUS cacheSh7750LibInit (CACHE_MODE instMode, CACHE_MODE dataMode);
extern STATUS cacheSh7729LibInit (CACHE_MODE instMode, CACHE_MODE dataMode);
extern STATUS cacheSh7710LibInit (CACHE_MODE instMode, CACHE_MODE dataMode);
extern STATUS cacheSh7700LibInit (CACHE_MODE instMode, CACHE_MODE dataMode);
extern STATUS cacheSh7622LibInit (CACHE_MODE instMode, CACHE_MODE dataMode);
extern STATUS cacheSh7604LibInit (CACHE_MODE instMode, CACHE_MODE dataMode);
extern STATUS cacheSh7040LibInit (CACHE_MODE instMode, CACHE_MODE dataMode);
#else
extern STATUS cacheSh7750LibInit ();
extern STATUS cacheSh7729LibInit ();
extern STATUS cacheSh7710LibInit ();
extern STATUS cacheSh7700LibInit ();
extern STATUS cacheSh7622LibInit ();
extern STATUS cacheSh7604LibInit ();
extern STATUS cacheSh7040LibInit ();
#endif

#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif 

#endif /* __INCcacheShLibh */
