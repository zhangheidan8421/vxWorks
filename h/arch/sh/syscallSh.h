/* syscallSh.h - SH specific System Call Infrastructure header */

/* Copyright 1984-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,30aug04,h_k  added user stack align validation.
01a,18sep03,h_k  written.
*/

/*
DESCRIPTION
This header contains SH-specific definitions and constants used by
the System Call Infrastructure library.

*/

#ifndef __INCsyscallShh
#define __INCsyscallShh

#ifdef __cplusplus
extern "C" {
#endif

#define syscallDispatch_PORTABLE	/* Use the portable dispatcher */

    /* defines */

#define SYSCALL_ENTRY_FRAME_SIZE	56

#define _SYSCALL_USER_SP_CHECK(sp) \
  { \
  if ((UINT32)(sp) & (_STACK_ALIGN_SIZE - 1)) \
    { \
    errnoSet (S_syscallLib_INVALID_USER_SP); \
    return (ERROR); \
    } \
  }

    /* typedefs */

#ifndef _ASMLANGUAGE
    /* 
     * The SYSCALL_ENTRY_STATE structure defines the saved machine state
     * when the system call trap is taken. This information is architecture
     * specific, and is used by the system call dispatcher to restart system
     * calls that have been interrupted by the occurance of signals.
     * System call restart is achieved by restoring the saved state at the
     * time of the system call trap, and re-issuing the system call.
     * Arguments to the system call are saved on the kernel stack, and 
     * the address of that array is passed as an argument to the dispatcher.
     * The layout of this structure must exactly match the ordering of members
     * of the system call entry frame in src/arch/sh/syscallALib.s.
     */

typedef struct syscall_entry_state
    {
    int    args[8];             /* argument list (r4-r7 and sp[0]-sp[3]) */
    int    scn;			/* System Call Number (SCN) in r3 */
    int    statusReg;		/* saved Status Register (ssr) */ 
    void * pc;			/* Trap return address (ssp) */
    void * pPR;			/* saved return register value */
    int  * pUStack;		/* user-mode stack pointer */
    int    spare1;		/* reserved */
    } SYSCALL_ENTRY_STATE;
#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCsyscallShh */
