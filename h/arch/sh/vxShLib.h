/* vxShLib.h - header for miscellaneous Hitachi-SH support routines */

/* Copyright 2000-2001 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,02nov01,zl   changed VXPOWERMODEREGS to use pointers.
01b,07sep01,h_k  added power control support (SPR #69838).
01a,20apr00,hk	 created to export vxMemProbeSup() and vxMemProbeTrap() to BSP.
*/

#ifndef __INCvxShLibh
#define __INCvxShLibh

#ifdef __cplusplus
extern "C" {
#endif

/* power management mode definitions - used by vxPowerModeSet/Get */

#define VX_POWER_MODE_DISABLE      0x0     /* power mgt disable */
#define VX_POWER_MODE_SLEEP        0x1     /* sleep power management mode */
#define VX_POWER_MODE_DEEP_SLEEP   0x2     /* deep sleep power mgt mode */
#define VX_POWER_MODE_USER         0xff    /* user specific mode */
#define VX_POWER_MODE_MASK         0xff    /* mask for the mode */

/* power management registers definitions */

#define STBCR2_DSLP	0x80		/* deep sleep for SH7750 only */

#ifndef	_ASMLANGUAGE

/* structure to manage power mode registers */

typedef struct
    {
    volatile UINT8 * pSTBCR1;
    volatile UINT8 * pSTBCR2;
    volatile UINT8 * pSTBCR3;
    } VXPOWERMODEREGS;

extern VXPOWERMODEREGS vxPowerModeRegs;

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

IMPORT	STATUS	vxMemProbeSup (int length, char * src, char * dest);
IMPORT	void	vxMemProbeTrap (void);
IMPORT	STATUS	vxPowerDown (void);
IMPORT	STATUS	vxPowerModeSet (UINT32);
IMPORT	UINT32	vxPowerModeGet (void);
IMPORT	STATUS	(* _func_vxIdleLoopHook) (void);

#else  	/* __STDC__ */

IMPORT	STATUS	vxMemProbeSup ();
IMPORT	void	vxMemProbeTrap ();
IMPORT	STATUS	vxPowerDown ();
IMPORT	STATUS	vxPowerModeSet ();
IMPORT	UINT32	vxPowerModeGet ();
IMPORT	FUNCPTR	_func_vxIdleLoopHook;

#endif 	/* __STDC__ */
#endif  /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxShLibh */
