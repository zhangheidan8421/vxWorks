/* fppLib.h - floating-point coprocessor support library header */

/* Copyright 1984-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,19aug03,dbt  Added PAL support.
01d,17jun03,jmp  code cleanup.
01c,27mar03,jmp  rewrote from i86.
01b,03may02,jmp  added fppDtoDx() & fppDxtoD().
01a,29apr98,cym  derived from i86
*/

#ifndef	__INCfppSimntLibh
#define	__INCfppSimntLibh

#ifdef __cplusplus
extern "C" {
#endif

/* number of fp registers on coprocessor */

#define FP_NUM_REGS	8	/* number of FP registers	*/

/* FPREG_SET structure offsets */

#define FPREG_FPCR	0x00	/* offset to FPCR in FPREG_SET	*/
#define FPREG_FPSR	0x04	/* offset to FPSR in FPREG_SET	*/
#define FPREG_FPTAG	0x08	/* offset to FPTAG in FPREG_SET	*/
#define FPREG_IP	0x0c	/* offset to IP in FPREG_SET	*/
#define FPREG_CS	0x10	/* offset to CS in FPREG_SET	*/
#define FPREG_OP	0x12	/* offset to OP in FPREG_SET	*/
#define FPREG_DP	0x14	/* offset to DP in FPREG_SET	*/
#define FPREG_DS	0x18	/* offset to DS in FPREG_SET	*/
#define FPREG_FPX(n)	(0x1c + (n)*sizeof(DOUBLEX))

/* FPU Control Word register */

#define FPCR_PC_MASK    0xfffffcff      /* Precision Control Field mask */
#define FPCR_PC_DOUBLE  0x0200          /* Double precision */

#ifndef _ASMLANGUAGE

/* DOUBLEX - double extended precision */

typedef struct
    {
    unsigned char f[10];
    } DOUBLEX;

/* FP_CONTEXT - FP context used by fsave/frstor instruction */

typedef struct fpContext	/* FP_CONTEXT */
    {
    int		fpcr;			/* 4    control word (16 low bits)   */
    int		fpsr;			/* 4    status word (16 low bits)    */
    int		fptag;			/* 4    tag word (16 low bits)	     */
    int		ip;			/* 4    instruction pointer offset   */
    short	cs;			/* 2    instruction pointer selector */
    short	op;			/* 2    last FP instruction op code  */
    int		dp;			/* 4    operand pointer offset	     */
    int		ds;			/* 4    operand pointer selector     */
    DOUBLEX	fpx[FP_NUM_REGS];	/* 8*10 FR[0-7] non-TOS rel. order   */
    } FP_CONTEXT;			/* 108  bytes total		     */

#define FPREG_SET FP_CONTEXT

/* variable declarations */

extern REG_INDEX fpRegName[];           /* f-point data register table */
extern REG_INDEX fpCtlRegName[];        /* f-point control register table */
extern FUNCPTR   fppCreateHookRtn;      /* arch dependent create hook routine */
extern FUNCPTR   fppDisplayHookRtn;     /* arch dependent display routine */

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern	void		fppArchInit (void);
extern	STATUS		fppProbe (void);
extern	void		fppDtoDx (DOUBLEX * pDx, double * pDouble);
extern	void		fppDxtoD (double * pDouble, DOUBLEX * pDx);
extern  void		fppRegsToCtx (FPREG_SET * pFpRegSet,
						FP_CONTEXT * pFpContext);
extern  void		fppCtxToRegs (FP_CONTEXT * pFpContext,
						FPREG_SET * pFpRegSet);
extern FP_CONTEXT *	fppCtxCreate (int tid);
extern STATUS		fppCtxDelete (FP_CONTEXT * pCtx);
extern void		fppCtxShow (FP_CONTEXT * pFpContext);
extern STATUS		fppEnable (void);
extern STATUS		fppDisable (void);

#else

extern	void		fppArchInit ();
extern	void		fppArchTaskCreateInit ();
extern	STATUS		fppProbe ();
extern	void		fppDtoDx ();
extern	void		fppDxtoD ();
extern  void		fppRegsToCtx ();
extern  void		fppCtxToRegs ();
extern FP_CONTEXT *	fppCtxCreate ();
extern STATUS		fppCtxDelete ();
extern void		fppCtxShow ();
extern STATUS		fppEnable ();
extern STATUS		fppDisable ();

#endif  /* __STDC__ */

#endif  /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCfppSimntLibh */
