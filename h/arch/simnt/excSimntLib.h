/* excSimntLib.h - exception library header */

/* Copyright 1984-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01f,10mar04,dbt  Fixed _WRS_IS_SUP_EXC() macro.
01e,09feb04,jmp  added _WRS_IS_SUPV_EXC() macro.
01d,21oct03,jmp  replaced padding field of EXC_INFO by access type.
01c,28mar03,jmp  added exception numbers.
		 replaced INSTR by UCHAR to use this file from host side.
01b,30may00,elp  renamed errCode in accessAddr.
01a,29apr98,cym  written based x86 version.
*/

#ifndef __INCexcSimntLibh
#define __INCexcSimntLibh

#ifdef __cplusplus
extern "C" {
#endif

/* typedefs */

typedef struct
    {
    UINT16	valid;		/* indicators that following fields are valid */
    UINT16	vecNum;		/* vector number */
    ULONG	accessAddr;	/* access address */
    UCHAR *	pc;		/* program counter */
    ULONG	statusReg;	/* status register */
    UINT16	csReg;		/* code segment register */
    UINT16	accessType;	/* type of access (read or write) */
    } EXC_INFO;

/* defines */

#define _WRS_IS_SUPV_EXC()	VXSIM_SUP_MODE_GET(pRegs->reg_status)

#define LOW_VEC		0		/* lowest initialized vector */
#define HIGH_VEC	0xff		/* highest initialized vector */

/* exception info valid bits */

#define EXC_VEC_NUM		0x01	/* vector number valid */
#define EXC_ACCESS_READ		0x02	/* accessAddr is valid (READ access) */
#define EXC_ACCESS_WRITE	0x04	/* accessAddr is valid (WRITE access) */
#define EXC_PC			0x08	/* pc valid */
#define EXC_STATUS_REG		0x10	/* status register valid */
#define EXC_CS_REG		0x20	/* code segment register valid */
#define EXC_INVALID_TYPE	0x80	/* special indicator: ESF type was bad;
					 * type is in funcCode field */

/* exception numbers */

#define EXC_INT_DIVIDE_BY_ZERO		0
#define EXC_SINGLE_STEP			1
#define EXC_DATATYPE_MISALIGNMENT	2
#define EXC_BREAKPOINT			3
#define EXC_IN_PAGE_ERROR		4
#define EXC_ILLEGAL_INSTRUCTION		5
#define EXC_INVALID_DISPOSITION		6
#define EXC_ARRAY_BOUNDS_EXCEEDED	7
#define EXC_FLT_DENORMAL_OPERAND	8
#define EXC_FLT_DIVIDE_BY_ZERO		9
#define EXC_FLT_INEXACT_RESULT		10
#define EXC_FLT_INVALID_OPERATION	11
#define EXC_FLT_OVERFLOW		12
#define EXC_ACCESS_VIOLATION		13
#define EXC_FLT_STACK_CHECK		14
#define EXC_FLT_UNDERFLOW		15
#define EXC_INT_OVERFLOW		16
#define EXC_PRIV_INSTRUCTION		17
#define EXC_STACK_OVERFLOW		18
#define EXC_UNKNOWN			19

/* variable declarations */

extern FUNCPTR  excExcepHook;   /* add'l rtn to call when exceptions occur */

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern void	excStub (void);
extern void	excIntStub (void);

#else	/* __STDC__ */

extern void	excStub ();
extern void	excIntStub ();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCexcSimntLibh */
