/* ivSimnt.h - simnt interrupt vectors */

/* Copyright 1993-1995 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,09feb04,dat  Diab lint fix, chg INUM_TO_IVEC
01b,21may03,jmp  moved IV_MAX from intArchLib.c.
01a,06oct97,cym  written
*/

#ifndef __INCivSimnth
#define __INCivSimnth

#ifdef __cplusplus
extern "C" {
#endif

/* maximun number of interrupt vectors */

#define IV_MAX	0x0300		/* This value must be coherent with	*/
				/* VXSIM_MAX_INTS defined on host side.	*/
				/* See vxsimwin32.h .			*/

/* macros to convert interrupt vectors <-> interrupt numbers */

#define IVEC_TO_INUM(intVec)    	(intVec)
#define INUM_TO_IVEC(intNum)    	((VOIDFUNCPTR *)intNum)

#define IVEC_TO_MESSAGE(intVec) 	(intVec)
#define MESSAGE_TO_IVEC(message)        (message)

/* interrupt vector definitions */

#ifdef __cplusplus
}
#endif

#endif /* __INCivSimnth */
