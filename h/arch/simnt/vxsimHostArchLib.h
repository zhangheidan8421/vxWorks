/* vxsimHostArchLib.h - Windows/VxSim interface header */

/* Copyright 2003-2005 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01h,19jan05,jmp  removed vxsimHostMailslot*
01g,13oct03,jeg  removed vxsimHostFileNReadGet () prototype.
01f,30jul03,jmp  remove buffer managements routine specific to Ulip.
01e,30apr03,dbt  Moved timestamp code to vxsimHostLib.h.
01d,04apr03,jmp  reworked vxsimHostMailslotInit() to fix system mode.
01c,04apr03,jeg  moved common routine definitions to target/h/vxsimHostLib.h
01b,25mar03,jeg  Adapted to common Unix/Windws passFs
01b,25mar03,jmp  added vxsimHostConsoleSioIntInit.
01a,30jan03,jmp  written.
*/

#ifndef __INCvxsimHostArchLibh
#define __INCvxsimHostArchLibh

/* includes */

#include "passFsLib.h"
#include "stat.h"

/* defines */

/* externals */

/* function declarations */

extern STATUS	vxsimHostVxWorksInstrCacheFlush	(void * address, UINT32 size);
extern void	vxsimHostConsoleFlush		(void);
extern void	vxsimHostConsoleSioIntInit	(int intVecNum);
extern void	vxsimHostPuts			(char * string);
extern void	vxsimHostPutc			(char character);
extern void	vxsimHostIntHandlingModeSet	(BOOL mode);
extern STATUS	vxsimHostUlipInit		(int ulipIfNum,
						 int ulipDeviceNum,
						 int ulipIntVec,
						 int ** ppSimUlipRcvCount,
						 int ** ppSimUlipReadCount);
extern int	vxsimHostUlipWrite		(int ulipIfNum, char * pBuf,
						 int length);
extern void	vxsimHostUlipStop		(int ulipIfNum);
extern int	vxsimHostUlipIpAddrGet		(char * ulipIpAddr);
extern int	vxsimHostTimerRateGet		(int clk);
extern void	vxsimHostTimerEnable		(int clk);
extern STATUS	vxsimHostTimerRateSet		(int clk, int ticksPerSecond);
extern char *	vxsimHostBootFileNameGet	(void);
extern void	vxsimHostDbg			(char * format, ...);
extern void	vxsimHostDebugBreak		(void);
extern void	vxsimHostCriticalSectionEnter	(UINT32 * pCriticalSection);
extern void	vxsimHostCriticalSectionLeave	(UINT32 * pCriticalSection);
#endif	/* __INCvxsimHostArchLibh */
