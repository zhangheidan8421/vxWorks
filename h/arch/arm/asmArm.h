/* asmArm.h - ARM assembler definitions header file */

/* Copyright 1996-1997 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,06jul04,md   added function definition macros
01a,09may96,cdp  created
*/

#ifndef	__INCasmArmh
#define	__INCasmArmh

#ifdef __cplusplus
extern "C" {
#endif

/* Build for Gnu elf or Diab compiler */
/* useful, assembly-language macros */
#define FUNC(a) a
#define VAR(name) name

#define FUNC_LABEL(func)      func:
#define VAR_LABEL(name)               name:

#if ARM_THUMB 
#define _ARM_FUNCTION_CALLED_FROM_C(a) \
	.code	16	;\
	.balign	4	;\
	.thumb_func	;\
a:			;\
	BX	pc	;\
	NOP		;\
	.code	32	;\
A##a:
#else
#define _ARM_FUNCTION_CALLED_FROM_C(a) \
	.code	32	;\
	.balign	4	;\
a:
#endif

#define _ARM_FUNCTION(a)	\
	.code	32	;\
	.balign	4	;\
a:

#if ARM_THUMB
#define _THUMB_FUNCTION(a)	\
	.code	16	;\
	.balign	2	;\
	.thumb_func	;\
a:
#endif

#ifdef __cplusplus
}
#endif

#endif	/* __INCasmArmh */
