/* coprocArm.h - coprocessor management library header */

/* Copyright 2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01b,04aug04,scm  fix permissions...
01a,03aug04,scm  written based on coprocI86.h 01a.
*/

#ifndef __INCcoprocArmh
#define __INCcoprocArmh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "taskLib.h"

/* defines */

#define VX_FP_TASK         VX_COPROC1_TASK

#ifdef __cplusplus
}
#endif

#endif /* __INCcoprocArmh */
