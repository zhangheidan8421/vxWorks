/* esfArm.h - ARM exception stack frames */

/* Copyright 1996-1997 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,09sep04,scm  add ttbase to ESF, so structs REG_SET & ESF overlap correctly
01a,09may96,cdp  created
*/

#ifndef	__INCesfArmh
#define	__INCesfArmh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef	_ASMLANGUAGE

/* The Exception Stack Frame
 * On the ARM, this is faked by a stub and the pointer
 * passed back points to the PC and CPSR at the end of the
 * register structure. If that changes, so must this.
 *
 * I.E. REG_SET struct & ESf struct 'overlap' on last 3 
 * elements of REG_SET (pc,cpsr,ttbase)...
 */

typedef struct
    {
    INSTR *pc;		/* program counter */
    ULONG cpsr;		/* current PSR */
    ULONG ttbase;       /* Trans Table Base */
    UINT32 vecAddr;	/* address of exception vector => type */
    } ESF;

#endif	/* _ASMLANGUAGE */


#ifdef __cplusplus
}
#endif

#endif	/* __INCesfArmh */
