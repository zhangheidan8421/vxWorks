/* cacheArmArch6.h - ARM ARCH6 cache library header file */

/* Copyright 2004-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,20apr05,jb   Fix for SPR 108034
01a,07dec04,jb   created
*/

#ifndef	__INCcacheArmArch6h
#define	__INCcacheArmArch6h

#ifdef __cplusplus
extern "C" {
#endif

#ifndef	_ASMLANGUAGE

/*
 * Common definitions for Arm Arch6 Cache support
 */

extern STATUS cacheAimArch6IProbe (void);
extern STATUS cacheAimArch6DProbe (void);

extern BOOL   cacheAimArch6IIsOn (void);
extern BOOL   cacheAimArch6DIsOn (void);

extern STATUS cacheAimArch6TextUpdate (void * start, UINT32 count, UINT32 IcacheLineSize, UINT32 DcacheLineSize);

extern void * cacheAimArch6DmaMalloc (size_t bytes);
extern STATUS cacheAimArch6DmaFree (void * pBuf);

extern BOOL   cacheAimArch6MmuIsOn (void);

extern STATUS cacheAimArch6DInvalidate (void * start, void * end, UINT32 step);
extern STATUS cacheAimArch6DInvalidateAll (void);

extern STATUS cacheAimArch6IInvalidate (void * start, void * end, UINT32 step);
extern STATUS cacheAimArch6IInvalidateAll (void);

extern STATUS cacheAimArch6DFlush (void * start, void * end, UINT32 step);

extern STATUS cacheAimArch6DClear (void * start, void * end, UINT32 step);

extern STATUS cacheAimArch6PipeFlush (void);

extern void cacheAimArch6IMB (void);

extern void cacheAimArch6IClearDisable (void);
extern void cacheAimArch6DClearDisable (void);

extern UINT32 cacheAimArch6Identify (void);

extern  FUNCPTR  sysCacheLibInit;
extern  UINT32   cacheArchState;

#endif

#ifdef __cplusplus
}
#endif

#endif	/* __INCcacheArmArch6h */
