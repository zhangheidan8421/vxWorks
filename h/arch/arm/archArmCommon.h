/* archArmCommon.h - common Arm architecture specific header */

/* Copyright 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01g,08sep04,scm  move INSTR from archArm.h
01e,25aug04,scm  move to portable trcLib...
01d,03aug04,scm  Enable Coprocessor Abstraction, disable Portable trcLib.
01c,25nov03,pes  Move setting of _BYTE_ORDER macro into archArmCommon.h
01b,12nov03,job  Moved some blib stuff from archArm.h
01a,11nov03,pes  written.
*/

#ifndef __INCarchArmCommonh
#define __INCarchArmCommonh

#ifdef __cplusplus
extern "C" {
#endif

/* from bLib.c */

#define _WRS_BLIB_ALIGNMENT     3       /* quad byte alignment mask */
#define swab_PORTABLE
#define _WRS_IMPROVED_PORTABLE_SWAB

#if defined(ARMEB) || defined(__ARMEB__)
#define	_BYTE_ORDER		_BIG_ENDIAN
#else
#define	_BYTE_ORDER		_LITTLE_ENDIAN
#endif

/* Enable Coprocessor Abstraction.  Must do all or none. */
	
#define _WRS_PAL_COPROC_LIB

/* Disable Portable trcLib */

#define trcLib_PORTABLE

#ifndef _ASMLANGUAGE
#if     (ARM_THUMB)
typedef unsigned short INSTR;           /* 16 bit instructions */
#else /* ARM_THUMB */
typedef unsigned long INSTR;            /* 32 bit word-aligned instructions */
#endif /* ARM_THUMB */
#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCarchArmCommonh */
