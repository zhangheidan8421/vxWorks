/* archArm.h - ARM specific header */

/*
 * Copyright (c) 1996-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. 
*/

/*
modification history
--------------------
01t,24aug05,h_k  Added _WRS_SUPPORT_CACHE_XLATE.
01s,12apr05,kk   remove _WRS_OBJLIB_SUPPORT_VERIFY_TRAP macro (SPR# 106486)
01r,28mar05,scm  add _ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK
01q,18feb05,dbt  Moved some loader specific macros to elfArm.h.
01p,17feb05,scm  add _WRS_OSM_INIT definition, and clarify
                 PORTABLE/NON-PORTABLE inclussion
01o,17jan05,jb   Adding architecture specific objVerify support
01n,08sep04,scm  move INSTR to archArmCommon.h
01m,10aug04,scm  undef "OPTIMIZE", define "PORTABLE"...
01l,27apr04,cjj  Defined workQLib_PORTABLE
01k,01dec03,scm  clean up, SPR #90358... remove obnoxious warning...
01j,25nov03,pes  Move setting of _BYTE_ORDER macro into archArmCommon.h
01i,14nov03,job  Moved some blib stuff to archArmCommon.h
01h,13nov03,pes  Add include of archArmCommon.h
01h,29oct03,sn   gcc 3.3.2 does not currently support arm or sh
01g,27aug03,to   added defines for semLib.
01f,07may03,pes  PAL conditional compilation cleanup. Phase 2.
01e,29apr03,pes  PAL conditional compilation cleanup.
01d,13mar01,sn   SPR 73723 - define supported toolchains
01c,20jan99,cdp  removed support for old ARM libraries.
01b,17aug98,cdp  added big-endian and multiple cachelib support.
01a,08may96,cdp  written based on archI86.h
*/

#ifndef __INCarchArmh
#define __INCarchArmh

#ifdef __cplusplus
extern "C" {
#endif


/*
 * In the cases where vxWorks.h is not defined first...
 */
#if !defined (TRUE)
#define TRUE 1
#endif

#if !defined (FALSE)
#define FALSE 0
#endif

#undef  _ARCH_SUPPORTS_GCC
#define _ARCH_SUPPORTS_DCC

#define	_DYNAMIC_BUS_SIZING	FALSE		/* require alignment for swap */

#define _ARCH_MULTIPLE_CACHELIB	TRUE		/* multiple cache libraries */

/* macros for AIM Cache */
#define _WRS_SUPPORT_CACHE_XLATE		/* cache virt-phys translate */

#include "arm.h"

/* PAL additions */

/* moved here from h/private/eventP.h */

/*
 * (Borrowed from PPC.)
 *
 * Macro for event logging of intEnt events for ARM architechtures.
 * The ARM architecture is different in that there is only one external
 * interrupt line coming into the CPU. At the time of the intEnt, the
 * external interrupt number is unknown, meaning that logging cannot
 * take place at this time. In this architechture, the logging of an
 * intEnt event is deferred until the interrupt controller driver
 * is exectued.
 * Since the logging has to take place in drivers and/or BSP world files,
 * the details of how the logging to be done has been deferred to a macro 
 * which is defined here.
 *
 * Note: This replaces the previous technique of saving the timestamp
 * during the intEnt code to be used later, when the logging was done.
 * SPR 21868 refers to a problem in which this can give rise to out-of-order
 * timestamps in the event log.
 */
#define WV_EVT_INT_ENT(intNum)	    EVT_CTX_0(EVENT_INT_ENT((UINT32)intNum))

/* moved here from h/private/classLibP.h */

#define OBJ_CLASS_initRtn	0x24	/* Offsets into OBJ_CLASS */

/* status codes */

#define S_vmLib_NO_FREE_REGIONS			(M_vmLib | 6)
#define S_vmLib_ADDRS_NOT_EQUAL			(M_vmLib | 7)

/* Make this architecture use the PORTABLE code set */
#define PORTABLE

/* from ffsLib.c */

#if !defined (PORTABLE)
#if (ARM_THUMB)
#define ffsLib_PORTABLE
#else
#undef ffsLib_PORTABLE
#endif /* ARM_THUMB */
#else
#define ffsLib_PORTABLE
#endif

#if defined (PORTABLE)
#define workQLib_PORTABLE		/* use workQLib.c */
#else
#undef workQLib_PORTABLE 
#endif /* ARM_THUMB */

#define ffsLib_FFS_TABLE

/* from dbgLib.c */

#define _WRS_FRAME_PRINT_STRING	"                   : "

/* from loadElfLib.c */

#define _WRS_USE_ELF_LOAD_FORMAT

/* from periodHost.c, repeatHost.c, shell.c, and taskLib.c */

#if (ARM_THUMB)
#define _WRS_CHANGE_INSN_SET_ENABLE
#define _WRS_CHANGE_INSN_SET(p)	(p) = ((FUNCPTR)(((UINT32)(p))|1))
#define _WRS_CHANGE_INSN_SET_PTR(p)	((FUNCPTR)(((UINT32)(p))|1))
#define _WRS_REV_INSN_SET_CHANGE(p)	((FUNCPTR)(((UINT32)(p)) & ~1))
#endif /* ARM_THUMB */

/* OSM suppport for stack protection, used in sysOsmLib.c */
#define _WRS_OSM_INIT

/* Interrupt stack protection, used in usrDepend.c and usrKernelStack.c */
#define _ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK

/* if OSM support not available for stack protection,
 * then support can not be available for interrupt
 * stack protection.
 */
#if !defined(_WRS_OSM_INIT)
#undef _ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK
#endif

#if defined(_ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK)
#undef _WRS_SVC_INT_STACK
#undef _WRS_IRQ_INT_STACK
#undef _WRS_DUAL_INT_STACK
#undef _WRS_SPLIT_STACKS
#else
/*
 * Old split interrupt stack method...,
 * not used when OSM stack protection is
 * enabled...
 */

/* from usrLib.c */
                                                                                    
#define _WRS_SVC_INT_STACK
#define _WRS_IRQ_INT_STACK
#define _WRS_DUAL_INT_STACK
                                                                                    
/* from kernelLib.c */
    /*
     * The ARM family uses 3 interrupt stacks. The ratio of the sizes of
     * these stacks is dependent on the interrupt structure of the board
     * and so is handled in the BSP code. Note that FIQ is now external to
     * VxWorks.
     */
#define _WRS_SPLIT_STACKS    {if (_func_armIntStackSplit != NULL) \
	        (_func_armIntStackSplit)(vxIntStackBase, intStackSize);}
#endif  /* _ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK */

/* from xdr_float.c */

#define FLOAT_NORM

/* Set to force the use of the big-endian call to XDR_GETLONG() */

#define _WRS_XDR_FORCE_BE

/* from bLib.c */

#if !defined (PORTABLE)
#if (ARM_THUMB)
#undef _WRS_BLIB_OPTIMIZED
#else /* ARM_THUMB */
#define _WRS_BLIB_OPTIMIZED
#undef bLib_PORTABLE
#endif /* ARM_THUMB */
#else
#undef _WRS_BLIB_OPTIMIZED
#define bLib_PORTABLE
#endif

/* from windALib.s */
/* from schedLib.c */

#if defined (PORTABLE)
#undef _WRS_SCHEDLIB_OPTIMIZED
#define schedLib_PORTABLE
#define _WRS_BASE6_SCHEDULER
#else
#define _WRS_SCHEDLIB_OPTIMIZED
#undef schedLib_PORTABLE
#undef _WRS_BASE6_SCHEDULER
#endif

#define _WRS_ENABLE_IDLE_INTS	{intUnlock(0);}

/* from sem*Lib.c, semA*Lib.s */

#if !defined (PORTABLE)
#if !(ARM_THUMB)
#define _WRS_HAS_OPTIMIZED_SEM
#define _WRS_USE_OPTIMIZED_SEM		/* XXX use optimized sem, for now */
#undef semMLib_PORTABLE
#else
#undef  _WRS_HAS_OPTIMIZED_SEM
#undef  _WRS_USE_OPTIMIZED_SEM
#define semMLib_PORTABLE
#endif /* !ARM_THUMB */
#else
#undef  _WRS_HAS_OPTIMIZED_SEM
#undef  _WRS_USE_OPTIMIZED_SEM
#define semMLib_PORTABLE
#endif

/* macros for getting frame and return PC from a jmp_buf */
#define _WRS_FRAMEP_FROM_JMP_BUF(env)   ((char *) (env)[0].reg.reg_fp)
#define _WRS_RET_PC_FROM_JMP_BUF(env)   ((INSTR *) (env)[0].reg.reg_pc)


#include "arch/arm/archArmCommon.h"

/* End PAL */

#ifdef __cplusplus
}
#endif

#endif /* __INCarchArmh */

