/* unShow.h - header file for AF_LOCAL show routine functionalities */

/* Copyright 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,26jan04,bwa  written
*/

#ifndef __INCunShowh
#define __INCunShowh

#ifdef __cplusplus
extern "C" {
#endif

/* function declarations */

extern void unShowInit     (void);
extern void uncompShowInit (void); 
extern void unstatShow     (void); 

#ifdef __cplusplus
}
#endif

#endif /* __INCunShowh */

