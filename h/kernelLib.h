/* kernelLib.h - header file for kernelLib.c */

/*
 * Copyright (c) 1990-1992, 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01h,31aug05,jln  added kernelRoundRobinInstall() 
01g,16aug05,jln  remove roundRobinLib.h
01f,02aug05,jln  Added WIND_SCHED_DESC struct
01e,22sep92,rrr  added support for c++
01d,04jul92,jcf  cleaned up.
01c,26may92,rrr  the tree shuffle
01b,04oct91,rrr  passed through the ansification filter
		  -fixed #else and #endif
		  -changed VOID to void
		  -changed copyright notice
01a,05oct90,shl created.
*/

#ifndef __INCkernelLibh
#define __INCkernelLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <qClass.h>
#include <taskLib.h>
/* typedefs */

/* kernel scheduer description structure */

typedef struct wind_sched_desc 
     {
     Q_CLASS_ID readyQClassId;  /* readyQ Id    */
     void *	readyQInitArg1;	/* readyQ init arg 1 */
     void *	readyQInitArg2;	/* readyQ init arg 2 */
     } WIND_SCHED_DESC; 

/* extern */

extern WIND_SCHED_DESC vxKernelSchedDesc; /* kernel scheduler structure */
extern BOOL roundRobinHookInstalled;	  /* flag shows round robin hooked */
extern FUNCPTR _func_kernelRoundRobinHook;/* pointer to kernelRoundRobinHook */

/* function declarations */

extern char *	kernelVersion (void);
extern STATUS 	kernelTimeSlice (int ticks);
extern void 	kernelInit (FUNCPTR rootRtn, unsigned rootMemSize,
			    char *pMemPoolStart, char *pMemPoolEnd,
			    unsigned intStackSize, int lockOutLevel);
extern STATUS 	kernelRoundRobinInstall (void);

#ifdef __cplusplus
}
#endif

#endif /* __INCkernelLibh */
