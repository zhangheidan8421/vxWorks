/* wdbSvcLib.h - header file for remote debug server */

/*
 * Copyright (c) 1994-2005 Wind River Systems, Inc.  
 *
 * The right to copy, distribute, modify or otherwise make use  of this 
 * software may be licensed only pursuant to the terms of an applicable Wind 
 * River license agreement.
 */

/*
modification history
--------------------
01h,28jul05,bpn  Remove DSA support.
01g,27feb03,elg  Merge file with BSD.
01f,25apr02,jhw  Added C++ support (SPR 76304).
01e,11jan99,dbt  added wdbSvcHookAdd() prototype (SPR #24323).
01d,12feb98,dbt  added "dynamic" field in WDB_SVC structure
		 added wdbSvcDsaSvcRemove() declaration.
01c,05apr95,ms   new data types.
01b,06feb95,ms	 added XPORT handle to dispatch routine.
01a,20sep94,ms   written.
*/

#ifndef __INCwdbSvcLibh
#define __INCwdbSvcLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include <wdb/wdb.h>
#include <wdb/wdbRpcLib.h>

/* data types */

typedef struct			/* hidden */
    {
    u_int	serviceNum;	
    UINT32	(*serviceRtn)();
    xdrproc_t	inProc;
    xdrproc_t	outProc;
    } WDB_SVC;

/* function prototypes */

IMPORT void	wdbSvcLibInit	(WDB_SVC * wdbSvcArray, UINT32 size);
IMPORT STATUS	wdbSvcAdd	(UINT32 procNum, UINT32 (*rout) (),
				 xdrproc_t xdrIn, xdrproc_t xdrOut);
IMPORT void	wdbSvcDispatch	(WDB_XPORT * pXport, UINT32 procNum);
IMPORT void	wdbSvcHookAdd (FUNCPTR hookRtn, UINT32 arg);

#ifdef __cplusplus
}
#endif

#endif  /* __INCwdbSvcLibh */
