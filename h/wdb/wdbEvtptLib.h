/* wdbEvtptLib.h - header file for remote debug eventpoints */

/* Copyright 1998-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,28feb03,elg  Merge file from BSD/OS.
01d,25apr02,jhw  Added C++ support (SPR 76304).
01c,25jan00,elg  Add WDB_CORE in evtptAdd().
01b,12feb99,dbt  use wdb/ prefix to include wdb.h header file.
01a,12feb98,dbt	 written.
*/

#ifndef __INCwdbEvtptLibh
#define __INCwdbEvtptLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "wdb/dll.h"
#include "wdb/wdb.h"

/* defines */

#define wdbEvtptDeleteAll()	{ if (__wdbEvtptDeleteAll != NULL)	\
				     (*__wdbEvtptDeleteAll) (); }

/* data types */

typedef struct			/* hidden */
    {
    dll_t		evtptList;
    WDB_EVT_TYPE	evtptType;
    UINT32		(*evtptAdd) (WDB_CORE * pWdbCore,
    				     WDB_EVTPT_ADD_DESC * pEvtpt,
				     UINT32 * pId);
    UINT32		(*evtptDel) (TGT_ADDR_T * pId);
    } WDB_EVTPT_CLASS;

/* function prototypes */

IMPORT void   	wdbEvtptClassConnect	(WDB_EVTPT_CLASS * pEvtList);
IMPORT void	(*__wdbEvtptDeleteAll)	(void);

#ifdef __cplusplus
}
#endif

#endif  /* __INCwdbEvtptLibh */
