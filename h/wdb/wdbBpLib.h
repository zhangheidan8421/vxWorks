/* wdbBpLib.h - wdb break point library */

/* Copyright 1984-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01v,08mar04,elg  Change WDB_CTX structure.
01u,13nov03,elg  Change WDB_BP_CTX_BP_ADD() prototype.
01t,10oct03,elg  Remove debug handler function pointers.
01s,29sep03,tbu  go back
01r,15sep03,tbu  Added wdbSysBpLibInit prototype
01q,27feb03,elg  Merge file with BSD and define new interface.
01p,25apr02,jhw  Added C++ support (SPR 76304).
01o,28oct99,elg  Add support for all-tasks breakpoints.
01n,07oct99,elg  Remove _wdbPdStop() and _wdbPdCont() declarations.
01m,04aug99,elg  Fix WDB dependencies problem.
01l,26may99,elg  Add new debug facilities for protection domains.
01k,21apr98,dbt  removed useless defines for ARM.
01j,04feb98,dbt  moved wdbSysBpLibInit() and wdbTaskBpLibInit() to wdbLib.h
01i,12jan98,dbt  modified for new debugger scheme
01h,17apr97,cdp added Thumb (ARM7TDMI_T) support.
01g,28nov96,cdp added ARM support.
01f,22jul96,jmb  merged mem's (ease) patch for HPSIM.
01e,20may96,ms   imported prototypes and macros from wdb[Task]BpLib.h.
01d,25feb96,tam  added single step support for PPC403.
01d,04apr96,ism  added simsolaris support
01c,23jan96,tpr  added PowerPC support.
01c,28nov95,mem  added MIPS support, added macros WDB_CTX_{LOAD,SAVE}.
01b,08jun95,ms   change CPU==SPARC to CPU_FAMILY==SPARC
01a,20nov94,rrr  written.
*/

#ifndef __INCwdbBpLibh
#define __INCwdbBpLibh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _ASMLANGUAGE

/* includes */

#include "wdb/wdbRegs.h"
#include "wdb/wdbDbgLib.h"

/* typedefs */

typedef struct wdb_ctx_bp_if
    {
    UINT32 (*bpAdd)	(WDB_CTX * pCtx, WDB_ACTION * pAction, INSTR * addr,
    			 UINT32 count, UINT32 type, UINT32 * pId);
    UINT32 (*step)	(UINT32 contextId, TGT_ADDR_T startAddr,
			 TGT_ADDR_T endAddr);
    UINT32 (*stop)	(UINT32 contextId);
    UINT32 (*cont)	(UINT32 contextId);
    } WDB_CTX_BP_IF;

/* defines */

#define	WDB_BP_CTX(pCtx)	(pWdbCtxBpIf [(pCtx)->contextType])
#define	WDB_BP_ENTRY(pCtx)	(pWdbCtxBpIf [(pCtx)->context.contextType])

#define	WDB_BP_CTX_BP_ADD(pCtx, pAction, addr, count, type, pId)	\
	(WDB_BP_CTX (pCtx) == NULL ?					\
	    WDB_ERR_NO_RT_CTX :						\
	    ((WDB_BP_CTX (pCtx))->bpAdd == NULL ?			\
	    	WDB_ERR_NO_RT_PROC :					\
		(WDB_BP_CTX (pCtx))->bpAdd ((pCtx), (pAction), (addr),	\
					    (count), (type), (pId))))

#define	WDB_BP_CTX_STEP(pCtx)						\
	(WDB_BP_ENTRY (pCtx) == NULL ?					\
	    WDB_ERR_NO_RT_CTX :						\
	    (WDB_BP_ENTRY (pCtx))->step == NULL ?			\
	    	WDB_ERR_NO_RT_PROC :					\
		(WDB_BP_ENTRY (pCtx))->step (WDB_CTX_ID_GET (&(pCtx)->context),\
					     (pCtx)->startAddr,		\
			 		     (pCtx)->endAddr))

#define	WDB_BP_CTX_STOP(pCtx)						\
	(WDB_BP_CTX (pCtx) == NULL ?					\
	    WDB_ERR_NO_RT_CTX :						\
	    (WDB_BP_CTX (pCtx))->stop == NULL ?				\
	    	WDB_ERR_NO_RT_PROC :					\
		(WDB_BP_CTX (pCtx))->stop (WDB_CTX_ID_GET (pCtx)))

#define	WDB_BP_CTX_CONT(pCtx)						\
	(WDB_BP_CTX (pCtx) == NULL ?					\
	    WDB_ERR_NO_RT_CTX :						\
	    (WDB_BP_CTX (pCtx))->cont == NULL ?				\
	    	WDB_ERR_NO_RT_PROC :					\
		(WDB_BP_CTX (pCtx))->cont (WDB_CTX_ID_GET (pCtx)))

/* context specific debug interface */

IMPORT WDB_CTX_BP_IF *	pWdbCtxBpIf [WDB_CTX_TYPE_NUM];

IMPORT VOIDFUNCPTR	wdbBpSysEnterHook;
IMPORT VOIDFUNCPTR	wdbBpSysExitHook;

/* function declarations */

IMPORT void		wdbBpInstall (void);

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif	/* __INCwdbBpLibh */
