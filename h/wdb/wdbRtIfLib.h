/* wdbRtIfLib.h - header file for the WDB agents runtime interface */

/* Copyright 1984-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01t,13aug04,dbt  Added signal information in exception notification.
01s,31mar04,elg  Change prototype of excHookAdd() pointer.
01r,30sep03,elg  Fix build warnings.
		 Fix pointer error with memory probe runtime interface.
01q,12sep03,tbu  Moved inclusion of netinet/in_systm.h to wdbOsLib.h
01p,27feb03,elg  Merge file with BSD and define new runtime interface.
01o,25apr02,jhw  Added C++ support (SPR 76304).
01n,01feb01,dtr  Adding in call for test for Altivec Registers.
01m,28aug00,elg  Change pdIsValid() to pdIdVerify().
01l,16aug00,elg  Change memProtect() prototype.
01k,25apr00,elg  Add taskCtxGet() routine.
01j,12apr00,elg  Add parmeters in pdCreate().
01i,02feb00,dbt  Added pdIsValid and removed pdListGet.
01h,15nov99,elg  Add returnNodeGet() in WDB runtime interface.
01g,08nov99,elg  Add ltExitCreate().
01f,15oct99,elg  Change pdCreate() interface.
01e,09sep99,elg  Add taskCtxSwitch() to Runtime interface.
01d,06may99,elg  Add 2 new routines in the Runtime Interface: taskDebugStart()
                 and taskDebugEnd().
01c,01jun95,ms	added taskLock/Unlock, removed timers
01b,05apr95,ms  new data types.
01a,23sep94,ms  written.
*/

#ifndef __INCwdbRtIfLibh
#define __INCwdbRtIfLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "wdb/wdb.h"
#include "wdb/wdbCtxIfLib.h"
#include "wdb/wdbRegs.h"

/* data types */

typedef struct wdb_rt_if
    {
    /* this function must be provided always */

    UINT32	(*rtInfoGet)	(WDB_RT_INFO * pRtInfo);

    /* function call to reboot machine */

    UINT32	(*reboot)	(void);

    /* function to call after loading text */

    UINT32	(*cacheTextUpdate)	(char * addr, UINT32 nBytes);

    /* function to call to write protect memory */

    STATUS	(*memProtect)		(
    					UINT32		pdId,
					char *		addr,
					UINT32		nBytes,
					BOOL		protect,
					UINT32 *	oldAttr
					);

    /* function needed to probe memory (required) */

    STATUS	(*memProbe)		(
					char *	addr,
					UINT32	mode,
					UINT32	len,
					char *	pVal
					);

    /* memory operations */

    void *	(*memalloc)		(size_t nBytes);
    UINT32 	(*memfree)		(void * pData);

    /* Context switch for a task */

    UINT32	(*memCtxSwitch)	(UINT32 newCtx);
    UINT32	(*memCtxGet)	(void);

    /* premption disable/reenable routine (for gopher interlocking) */

    UINT32	(*schedLock)	(void);
    UINT32	(*schedUnlock)	(void);

    UINT32	(*intLock)	(void);
    UINT32	(*intUnlock)	(int level);

    /* errno management */

    int		(*errnoGet)	(void);
    STATUS	(*errnoSet)	(int);

    /* function to install an exception hook (for exc event notification) */

    UINT32	(*excHookAdd)	(void (*hook)	(
						WDB_CTX		stoppedCtx,
						WDB_CTX		excCtx,
						UINT32		vec,
						UINT32		sigNum,
						char *		pESF,
						WDB_IU_REGS *	pRegs,
						BOOL		systemSuspend
						));

    /* Page Size information */

    int		pageSize;

    /* node on context specific routines */

    WDB_CTX_IF *	pWdbCtxIf [WDB_CTX_TYPE_NUM];
    } WDB_RT_IF;

/* global variables */

IMPORT WDB_RT_IF *      pWdbRtIf;	/* runtime interface */

/* macros */

/* info macro */

#define	WDB_RT_INFO_GET(pRtInfo)					\
	(pWdbRtIf->rtInfoGet == NULL ?					\
	    WDB_ERR_NO_RT_PROC :					\
	    pWdbRtIf->rtInfoGet (pRtInfo))

/* reboot macro */

#define	WDB_RT_REBOOT()							\
	(pWdbRtIf->reboot == NULL ?					\
	    WDB_ERR_NO_RT_PROC :					\
	    pWdbRtIf->reboot ())

/* memory macros */

#define	WDB_RT_MEM_CACHE_UPDATE(addr, nBytes)				\
	(pWdbRtIf->cacheTextUpdate == NULL ?				\
	    WDB_ERR_NO_RT_PROC :					\
	    pWdbRtIf->cacheTextUpdate ((addr), (nBytes)))

#define	WDB_RT_MEM_PROTECT(memCtx, dest, nBytes, protect, oldAttr)	\
	(pWdbRtIf->memProtect == NULL ?					\
	    WDB_ERR_NO_RT_PROC :					\
	    pWdbRtIf->memProtect ((memCtx), (dest), (nBytes),		\
	    			  (protect), (oldAttr)))

#define	WDB_RT_MEM_PROBE(dest, option, nBytes, source)			\
	(pWdbRtIf->memProbe == NULL ?					\
	    WDB_ERR_NO_RT_PROC :					\
	    pWdbRtIf->memProbe ((dest), (option), (nBytes), (source)))

#define	WDB_RT_MEM_ALLOC(type, size)					\
	(pWdbRtIf->memalloc == NULL ?					\
	    (type *) NULL :						\
	    (type *) pWdbRtIf->memalloc ((size_t) ((size) * sizeof (type))))

#define	WDB_RT_MEM_FREE(addr)						\
	(pWdbRtIf->memfree == NULL ?					\
	    WDB_ERR_NO_RT_PROC :					\
	    pWdbRtIf->memfree (addr))

#define	WDB_RT_MEM_SWITCH(memCtx)					\
	(pWdbRtIf->memCtxSwitch == NULL ?				\
	    WDB_ERR_NO_RT_PROC :					\
	    pWdbRtIf->memCtxSwitch (memCtx))

#define	WDB_RT_MEM_CTX_GET()						\
	(pWdbRtIf->memCtxGet == NULL ?					\
	    WDB_ERR_NO_RT_PROC :					\
	    pWdbRtIf->memCtxGet ())

/* scheduler macros */

#define WDB_RT_SCHED_LOCK()						\
	(!WDB_IS_NOW_EXTERNAL () ?					\
	    ((pWdbRtIf->schedLock == NULL) ?				\
	    	WDB_ERR_NO_RT_PROC :					\
                pWdbRtIf->schedLock ()) : 0)

#define WDB_RT_SCHED_UNLOCK()      					\
        (!WDB_IS_NOW_EXTERNAL () ?					\
	    ((pWdbRtIf->schedUnlock == NULL) ?				\
	    	WDB_ERR_NO_RT_PROC :					\
                pWdbRtIf->schedUnlock ()) : 0)

#define WDB_RT_INT_LOCK()						\
	(pWdbRtIf->intLock == NULL ?					\
	    WDB_ERR_NO_RT_PROC :					\
	    pWdbRtIf->intLock ())

#define WDB_RT_INT_UNLOCK(level)					\
	(pWdbRtIf->intUnlock == NULL ?					\
	    WDB_ERR_NO_RT_PROC :					\
	    pWdbRtIf->intUnlock (level))

/* errno management */

#define	WDB_RT_ERRNO_GET()						\
	(pWdbRtIf->errnoGet == NULL ?					\
	    ERROR :							\
	    pWdbRtIf->errnoGet ())

#define	WDB_RT_ERRNO_SET(x)						\
	(pWdbRtIf->errnoSet == NULL ?					\
	    ERROR :							\
	    pWdbRtIf->errnoSet (x))

/* hooks */

#define	WDB_RT_EXC_HOOK_ADD(hook)					\
	(pWdbRtIf->excHookAdd == NULL ?					\
	    WDB_ERR_NO_RT_PROC :					\
	    pWdbRtIf->excHookAdd (hook))

#ifdef __cplusplus
}
#endif

#endif  /* __INCwdbRtIfLibh */
