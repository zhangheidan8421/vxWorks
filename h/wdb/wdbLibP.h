/* wdbLibP.h - private header file for remote debug agent */

/*
 * Copyright (c) 1994-2005 Wind River Systems, Inc.  
 *
 * The right to copy, distribute, modify or otherwise make use  of this 
 * software may be licensed only pursuant to the terms of an applicable Wind 
 * River license agreement.
 */

/*
modification history
--------------------
01z,01aug05,bpn  Add new register buffer.
01y,28may04,elg  Add new function pointers.
01x,08apr04,elg  Define wdbMemChunk.
		 Remove obsolete type WDB_REG_SET_TYPE.
01w,13jan04,elg  Add prototype of wdbCnctModePost().
01v,26sep03,tbu  Moved definition of wdbMbufInit & wdbSioTest  to wdbOsLib.h
01u,15sep03,elg  Add definition of wdbSioTest().
01t,27aug03,elg  Add declarations of context event routines.
01s,15jul03,pch  Conditional compilation cleanup
01r,26jun03,elg  Add definition of wdbSuspendSystem() as extern routine.
01q,27feb03,elg  Merge file with BSD.
01p,04feb03,elg  Add WDB merged protocol.
01o,20jul00,dbt  Added wdbExternCmdRun() prototype.
01n,03feb00,elg  Fix component dependencies problems.
01m,31jan00,dbt  Added wdbPdUpdateNotify() prototype.
01l,21jan00,dbt  Added WDB_USR_SVC_STRUCT structure.
01k,25oct99,elg  Rename PD creation and deletion hooks.
01j,15oct99,dbt  added wdbConnectHookAdd() and wdbDisconnectHookAdd()
                 prototypes.
01i,19aug99,elg  Add hooks for protection domains.
01h,29jun99,elg  Add WDB_CORE support.
01g,25apr02,jhw  Added C++ support (SPR 76304).
01f,14sep01,jhw  Added wdbExternRegsGet() and wdbExternRegsSet() function
                 prototypes.
		 Removed __STDC__ precompiler conditional.
01e,11jun98,dbt  removed wdbTgtHasFpp() prototype.
01d,03jun95,ms	 added prototype for wdbTargetIsConnected()
01c,05apr95,ms   new data types.
01b,06feb95,ms	 removes wdbRpcLib specific things.
01a,20sep94,ms   written.
*/

#ifndef __INCwdbLibPh
#define __INCwdbLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include <wdb/wdb.h>
#include <wdb/wdbRtIfLib.h>

/* defines */

#define WDB_STATE_EXTERN_RUNNING        1

#define	WDB_IS_NOW_EXTERNAL()		(wdbMode & WDB_MODE_SYSTEM)

/* data types */

typedef struct wdb_usr_svc		/* user service structure */
    {
    FUNCPTR	func;			/* service entry point */
    UINT32 	svcId;			/* service ID */
    } WDB_USR_SVC;

/* agent variables */

extern UINT32  		wdbCommMtu;
extern UINT32		wdbTgtMemBase;
extern UINT32		wdbTgtMemSize;
extern UINT32		wdbTgtNumMemRegions;
extern WDB_MEM_REGION *	pWdbTgtMemRegions;
extern UINT32		wdbAvailOptions;
extern VOIDFUNCPTR	_func_dbgPdDelete;
extern UINT32		wdbMode;
extern BOOL		wdbOneShot;
extern BOOL		wdbSystemModeSupported;
extern FUNCPTR		_func_IsNowExternal;
extern UINT32		wdbAvailModes;
extern BOOL		wdbIsInitialized;
extern FUNCPTR		wdbTaskRestartHook;
extern char 		wdbRegsBuffer[];
extern UINT		wdbRegsBufferSize;
extern VOIDFUNCPTR	_func_wdbExternRegSave;
extern VOIDFUNCPTR	_func_wdbExternRegRestore;

/* function prototypes */

extern void	wdbInfoGet	  (WDB_AGENT_INFO * pInfo);
extern void	wdbNotifyHost	  (void);
extern BOOL	wdbTargetIsConnected (void);

extern STATUS	wdbModeSet	  (INT32 newMode);
extern BOOL	wdbIsNowExternal  (void);
extern BOOL	wdbRunsExternal   (void);
extern BOOL	wdbIsNowTasking   (void);
extern BOOL	wdbRunsTasking	  (void);

extern void	wdbSuspendSystem	(WDB_IU_REGS * pRegs,
					 void (*callBack) (),
					 int arg);
extern void	wdbSuspendSystemHere	(VOIDFUNCPTR callBack, INT32 arg);
extern void	wdbExternCmdRun		(VOIDFUNCPTR callBack, UINT32 arg);
extern STATUS	wdbExternInfRegsGet	(UINT32 type, char ** ppRegs);
extern STATUS	wdbExternRegsGet	(UINT32 type, char ** ppRegs);
extern void     wdbExternEnterHook	(void);
extern void     wdbExternExitHook	(void);
extern void	wdbCtxCreateNotify	(void * pNode);
extern void	wdbCtxDeleteNotify	(void * pNode);
extern void	wdbCtxEvtLibInit	(void);
extern STATUS	wdbConnectHookAdd	(FUNCPTR connectHook);
extern STATUS	wdbDisconnectHookAdd	(FUNCPTR disconnectHook);
extern void	wdbMemRtLibInit		(WDB_RT_IF * pWdbRtIf);
extern void	wdbCmdLoop		(void);
extern void	wdbRtLibInit		(WDB_RT_IF * pWdbRtIf);
extern void	wdbCnctModePost		(UINT32 mode);
extern STATUS	wdbExternRegsSet	(UINT32 type, char * pRegs);
extern STATUS	wdbExternRegLibInit	(void);
extern STATUS	wdbTaskRegLibInit	(void);

/* globals */

extern WDB_IU_REGS *    pWdbExternSystemRegs;

#ifdef __cplusplus
}
#endif

#endif /* __INCwdbLibPh */
