/* wdbRtpLibP.h - private header file for RTP runtime interface */

/*
 * Copyright (c) 2005 Wind River Systems, Inc.  
 *
 * The right to copy, distribute, modify or otherwise make use  of this 
 * software may be licensed only pursuant to the terms of an applicable Wind 
 * River license agreement.
 */

/*
modification history
--------------------
01a,25jul05,bpn  Created.
*/

#ifndef __INCwdbRtpLibPh
#define __INCwdbRtpLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include <rtpLib.h>

/* data types */

typedef struct wdb_rtp_info
    {
    VOIDFUNCPTR	wdbDeleteHook;	/* RTP exit hook for WDB */
    VOIDFUNCPTR wdbDetachHook;	/* RTP Detach routine */
    BOOL	childProcessStop;	/* TRUE if CPS is set for that task */
    } WDB_RTP_INFO;

/* function prototypes */

extern void		wdbRtpCtrlLibInit (void);
extern STATUS		wdbRtpDeleteHookManage (BOOL addHook);
extern WDB_RTP_INFO *	wdbRtpInfoAlloc (RTP_ID pid);

#ifdef __cplusplus
}
#endif

#endif /* __INCwdbRtpLibPh */
