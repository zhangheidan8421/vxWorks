/* pthreadCommon.h - common header for user-side POSIX threads (pthreads) */
 
/* Copyright 2004 Wind River Systems, Inc. */
 
/*
modification history
--------------------
01f,09sep04,pad  created.
*/
 
#ifndef __INCpthreadCommonh
#define __INCpthreadCommonh

/* includes */

#if defined(__cplusplus)
extern "C" {
#endif	/* __cplusplus */

/*
 * The following set of macros ("Compile-Time Symbolic Constants") corresponds
 * to the supported set of POSIX set features supported for applications that
 * the kernel needs to know about.
 */

#define PTHREAD_CANCEL_ASYNCHRONOUS	0
#define PTHREAD_CANCEL_DEFERRED		1

#if defined(__cplusplus)
}
#endif	/* __cplusplus */

#endif /* __INCpthreadCommonh */
