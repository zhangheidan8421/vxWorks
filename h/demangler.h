/* demangler.h - WR wrapper around GNU libiberty C++ demangler */

/* Copyright 2001 Wind River Systems, Inc. */

/*
modification history
--------------------
02d,02aug04,sn   added demangleToBuffer entry point
02c,16dec03,sn   DEBUG_DEMANGLER really no longer on by default!
02b,05may03,sn   DEBUG_DEMANGLER no longer on by default
02a,15apr03,sn   moved to share/src/demangler
01b,10dec01,sn   move enum defs to demanglerTypes.h
01a,28nov01,sn   wrote
*/

#ifndef __INCdemanglerh
#define __INCdemanglerh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

/* defines */

/* typedefs */

#include "demanglerTypes.h"

char * demangle
    (
    const char * mangledSymbol,
    DEMANGLER_STYLE style,      /* the scheme used to mangle symbols */
    DEMANGLER_MODE mode         /* how hard to work */
    );

DEMANGLER_RESULT demangleToBuffer
    (
    char * mangledSymbol,
    char * buffer,		/* output buffer for demangled name */
    size_t * pBufferSize,	/* in: actual buffer size / out: required buffer size */
    DEMANGLER_MODE mode,	
    char ** pResult		/* out: result string */
    );

DEMANGLER_STYLE demanglerStyleFromName
    (
    const char * styleName,
    DEMANGLER_STYLE defaultStyle
    );

const char * demanglerNameFromStyle
    (
    DEMANGLER_STYLE style
    );

/* Add #define DEBUG_DEMANGLER here to turn on debugging */

#ifdef DEBUG_DEMANGLER
#define debug_dmgl(fmt, x) (printf("debug_dmgl: "), printf((fmt), (x)))
#else
#define debug_dmgl(fmt, x)
#endif

#ifdef __cplusplus
}
#endif

#endif /* __INCdemanglerh */

