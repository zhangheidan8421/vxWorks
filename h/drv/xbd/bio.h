/* bio.h - Block I/O Transaction Header File */

/* Copyright 2004 Wind River Systems, Inc */

/*
modification history
--------------------
01d,07mar05,rfr  Added NULLSECTOR constant
01c,10feb05,rfr  Added bio_alloc and bio_free.
01b,10feb05,rfr  Changed block_t to sector_t
01a,03feb05,rfr  Added global initialization function.
*/

#ifndef __INCbioh
#define __INCbioh


#include <drv/manager/device.h>

typedef long long sector_t;	/* a block index */

#define NULLSECTOR -1L

#define BIO_READ 0x0001
#define BIO_WRITE 0x0002

struct xbd;

struct bio {
    device_t bio_dev;		/* The allocator of this bio */
    sector_t bio_blkno;		/* The Block Number of this bio */
    unsigned bio_bcount;	/* Total number of bytes in this segment */
    void *bio_data;		/* bio_bcount bytes of data */
    unsigned bio_resid;		/* Residual count for this segment */
    unsigned bio_error;		/* Error code for this segment */
    unsigned bio_flags;		/* Flags. At least BIO_READ and BIO_WRITE */
    void (*bio_done)(struct bio *); /* Completion function for this segment */
    void *bio_caller1;		/* Caller supplied data */
    struct bio *bio_chain;	/* The next segment or NULL */
};


STATUS bioInit();

void bio_done(struct bio *, int);

void *bio_alloc(device_t, int);
void bio_free(void *);

#endif
