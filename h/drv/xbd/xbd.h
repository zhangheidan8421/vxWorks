/* xbd.h - Extended Block Device Header File */

/*
 * Copyright (c) 2004-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01m,25aug05,pcm  added XBD_STACK_COMPLETE ioctl and xbdEventInstantiated
01l,12aug05,rfr  Added media changed event
01k,08aug05,rfr  Added XBD_TEST ioctl command
01j,01jul05,rfr  Added XBD_GETBASENAME ioctl() command
01i,24jun05,rfr  Added total block count to struct xbd_geometry
01h,24jun05,rfr  Fixed name collision in XBD_GEOMETRY
01g,23may05,rfr  Added ioctl commands
01f,14apr05,rfr  Added event and category types
01e,25mar05,rfr  Adapting to the new device.h
01d,10feb05,rfr  Changed block_t to sector_t
01c,04feb05,rfr  Added initialization function.
01b,03feb05,pcm  formatting changes
*/

#ifndef __INCxbdh
#define __INCxbdh

/* includes */
#include "vxWorks.h"

#include <drv/manager/device.h>
#include <drv/xbd/bio.h>


extern UINT16 xbdEventCategory;
extern UINT16 xbdEventPrimaryInsert;
extern UINT16 xbdEventRemove;
extern UINT16 xbdEventSecondaryInsert;
extern UINT16 xbdEventSoftInsert; 
extern UINT16 xbdEventMediaChanged;
extern UINT16 xbdEventInstantiated;

struct xbd;

struct xbd_funcs
    {
    int (*xf_ioctl) (struct xbd * dev, int cmd, void * arg);
    int (*xf_strategy) (struct xbd * dev, struct bio * bio);
    int (*xf_dump) (struct xbd * dev, sector_t pos, void * data, size_t size);
    };

/* typedefs */

typedef struct xbd
    {
    struct device       xbd_dev;
    struct xbd_funcs *  xbd_funcs;
    unsigned            xbd_blocksize;
    sector_t             xbd_nblocks;
    } XBD, * XBD_ID;

typedef struct xbd_geometry
    {
    unsigned heads;
    unsigned long long cylinders;
    unsigned secs_per_track;
    sector_t total_blocks;
    unsigned blocksize;
    } XBD_GEOMETRY;

typedef enum xbd_level {XBD_TOP=0, XBD_PART=1, XBD_BASE=2} XBD_LEVEL;


STATUS xbdInit();

int xbdAttach (XBD * xbd, struct xbd_funcs * funcs, const char * name, 
                   unsigned blocksize, sector_t nblocks, device_t * result);
void xbdDetach (XBD * xbd);

/* These functions access the driver */

int xbdIoctl (device_t d, int cmd, void * arg);
int xbdStrategy (device_t d, struct bio * bio);
int xbdDump (device_t d, sector_t pos, void * data, size_t size);
int xbdSize (device_t d, long long * result);

int xbdNBlocks (device_t d, sector_t * result);
int xbdBlockSize (device_t d, unsigned * result);

/* ioctl commands */

#define XBD_GETGEOMETRY	   0xbd000001	/* Extract disk Geometry */
#define XBD_SYNC	   0xbd000002	/* Flush Block device */
#define XBD_SOFT_EJECT	   0xbd000003	/* Eject and raise SoftInsert */   
#define XBD_HARD_EJECT	   0xbd000004	/* Eject and raise Insert */
#define XBD_GETBASENAME    0xbd000005	/* Retrieve name of base XBD */
#define XBD_TEST	   0xbd000006	/* Request XBD test its validity */
#define XBD_STACK_COMPLETE 0xbd000007   /* XBD stack is initialized */

#endif
