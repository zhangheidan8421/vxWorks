/* simNetEndDrv.h - END style Simulator Ethernet driver header */

/* Copyright 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,15apr04,elp  added defines to get/set parameters.
01a30jul03,elp written.
*/

#ifndef __INCsimEndh
#define __INCsimEndh

#ifdef __cplusplus
extern "C" {
#endif

/* SIMNET defines */

/* macros to get SUBNET information from vxsimnetd */
#define SIMNET_SUBNET_MTU		1	/* get subnet mtu */
#define SIMNET_SUBNET_PHYS_ADDRLEN	2	/* get MAC address length */
#define SIMNET_SUBNET_BROAD_ADDRESS	3	/* get subnet bcast address */
#define SIMNET_SUBNET_ADDRESS		4	/* get subnet address */
#define SIMNET_SUBNET_MASK		5	/* get subnet mask */
#define SIMNET_SUBNET_ADDRESSV6		6	/* get subnet IPv6 prefix */
#define SIMNET_SUBNET_ADDRESSV6_LEN	7	/* get subnet IPv6 prefix len */
#define SIMNET_SUBNET_BROADCAST		8 	/* broadcast support ? */
#define SIMNET_SUBNET_MULTICAST		9	/* multicast support ? */

/* macro to get/set NODE information */
#define SIMNET_NODE_PHYS_ADDRESS	10	/* node MAC address */
#define SIMNET_NODE_INTERRUPT		11	/* node interrupt enabled */
#define SIMNET_NODE_PROMISCUOUS		12	/* node promiscuous */
#define SIMNET_NODE_MULTICAST		13	/* node multicasting */
#define SIMNET_NODE_ADDRESS		14	/* node address */
#define SIMNET_NODE_ADDRESSV6		15	/* IPv6 node address */

/* typedefs */

typedef struct simnet_pkt
    {
    void *	ldp_handle; 	/* Reserved for internal use */
    int		ldp_maxsize;	/* Maximum size of the packet */
    int		ldp_size;	/* size of the packet */
    int		ldp_offset;	/* data offset from the packet ptr. */
    UINT8	ldp_buf[1];	/* 1 or more bytes for data */
    } SIMNET_PKT;

#ifdef __cplusplus
}
#endif

#endif /* __INCsimNetEndDrvh */

