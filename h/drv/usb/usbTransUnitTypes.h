/* usbtuTypes.h - Typedefines used in USB 2.0 host stack */

/* Copyright 2003-2004 Wind River Systems, Inc. */


/*
DESCRIPTION

This file contains the data types used in the USB Host Stack
*/
#ifndef  __INCusbtuTypesh
#define __INCusbtuTypesh

/* typedefs */


#ifdef TYPE_REDEFINED
typedef enum _BOOLEAN
    {
    	FALSE = 0,
    	TRUE
    }BOOLEAN;
#else
typedef long BOOLEAN;
#endif



typedef void * PVOID;
typedef unsigned char* PUCHAR;
typedef unsigned int   *PUINT32;

#endif  /* __INCusbtuTypesh */
/* end of file. */
