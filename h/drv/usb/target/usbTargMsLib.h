/* usbTargMsLib.h - Defines for USB mass storage driver */

/* Copyright 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,23jul04,ami  Coding Convention Changes
01c,21jul04,pdg  Changed IRP_CALLBACK to ERP_CALLBACK
01b,19jul04,hch  created the file element 
01a,15mar04,jac  written.
*/

/*
DESCRIPTION

Defines the USB mass storage driver interface to the USB target controller 
driver (TCD).

*/

#ifndef _USBTARGMSLIB_H
#define _USBTARGMSLIB_H

#ifdef	__cplusplus
extern "C" {
#endif

/* defines */

#define USB_DEBUG_PRINT

#define USE_SCSI_SUBCLASS  /* SCSI Protocol */

#ifdef USE_SCSI_SUBCLASS
#undef  USE_RBC_SUBCLASS
#else
#define USE_RBC_SUBCLASS 	/* RBC Protocol */	
#endif

#ifdef USB_DEBUG_PRINT
    void usbDbgPrint(char *fmt, ...); /* Debug messages */
#else
    #define usbDbgPrint 
#endif


/* function declaration */

extern USB_BULK_CBW     *usbMsCBWGet(void);
extern USB_BULK_CBW     *usbMsCBWInit(void);
extern USB_BULK_CSW     *usbMsCSWInit(void);
extern USB_BULK_CSW     *usbMsCSWGet(void);

extern STATUS	usbMsBulkInStall(void);
extern STATUS	usbMsBulkInUnStall(void);
extern STATUS	usbMsBulkOutStall(void);
extern STATUS	usbMsBulkOutUnStall(void);
extern BOOL	usbMsIsConfigured(void);
extern BOOL	usbMsBulkInErpInUseFlagGet(void);
extern BOOL	usbMsBulkOutErpInUseFlagGet(void);
extern VOID	usbMsBulkInErpInUseFlagSet(BOOL state);
extern VOID	usbMsBulkOutErpInUseFlagSet(BOOL state);

extern VOID usbTargMsCallbackInfo (struct usbTargCallbackTable ** ppCallbacks,
                                   VOID ** pCallbackParam);

extern STATUS usbMsBulkInErpInit (UINT8 * pData, UINT32 size, 
                                  ERP_CALLBACK    erpCallback, pVOID usrPtr);

extern STATUS usbMsBulkOutErpInit (UINT8 * pData, UINT32 size,
                                   ERP_CALLBACK    erpCallback,pVOID usrPtr);

#ifdef	__cplusplus
}
#endif

#endif
