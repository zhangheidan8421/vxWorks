/* dot11Bsp.h - Contains definitions used to communicate with the BSP */

/* Copyright 2004 Wind River Systems, Inc. */

/* 
Modification History
--------------------
02e,09sep05,rb  Added countryCode as boot parameter
02d,25jan05,rb  Made security components optionally buildable
02c,07jan05,rb  Added members to dot11BspFuncs for 802.1X Integration
02b,01nov04,rb  Added settings for security policy
02a,25aug04,rb  Wind River Wireless Ethernet Driver 2.0 FCS
*/

/*
DESCRIPTION

This file contains the definitions necessary for the DOT11 Frameowrk to load
the Atheros cards from the BSP, as defined in sysAr521xEnd.c

*/

#ifndef __INCdot11Bsph
#define __INCdot11Bsph
#ifdef  __cplusplus
extern "C" {
#endif /* __cplusplus */

/* This structure is used to hold basic default WLAN configuration parameters 
that are used by the device driver on start up. They can be defined by
the user through the project facility or by making modifications to this
file and using the command line build option */
typedef struct dot11_default_config_s
    {
    char*  ssid;     /* Default SSID */
    UINT32 mode;     /* Default Station mode */ 
    UINT32 radio;    /* Default radio */
    UINT32 channel;  /* Default channel */ 
    UINT32 country;
    
    UINT32 secPol;   /* Security Policy : a bitmap of allowed sec. policies */
    UINT32 authPol;  /* Authentication Policy: specifies 802.1X or PSK */
    UINT32 ciphPol;  /* Cipher Policy: Specifies list of allowed ciphers */
    
    UINT8  preSharedKey[64]; /* If authPol == PSK, then this contains the PSK*/
    char *  rsnPassphrase;
    } DOT11_DEFAULT_CONFIG;

typedef struct dot11_bsp_func
    {
    /* Connect interrupt handler to vector */
    void (*sysIntConnect)(int, int, int, STATUS*); 
    /* Disconnect interrupt handler from vector */
    void (*sysIntDisconnect)(int, int, int, STATUS*);
    /* System level interrupt enable  */ 
    void (*sysIntEnable)(INT32, STATUS*);         
    /* System level interrupt disable */
    void (*sysIntDisable)(INT32, STATUS*);
    /* Micro second delay */
    void (*sysUsDelay)(int);
    /* Cache invalidate function */   
    STATUS (*sysCacheInvalidate)(void*, UINT32);
    /* Cache flush function */  
    STATUS (*sysCacheFlush)(void*, UINT32);  
    /* Converts bus address to CPU address */
    UINT32 (*sysPhysToVirt) (UINT32);
    /* Converts CPU address to bus address */
    UINT32 (*sysVirtToPhys) (UINT32);
    /* Write buffer flush function */
    STATUS (*sysWbufFlush)(); 
    /* Memory poll deallocation function */
    void (*sysDmaMemFree)(UINT32);

    /* SME types declared here so that the configlettes / cmd line can
    include or exclude them at will */
    STATUS (*essInit)(DOT11_FW*);
    STATUS (*ibssInit)(DOT11_FW*);
    STATUS (*apInit)(DOT11_FW*);
    STATUS (*rsnInit)(DOT11_FW*);
    

    } DOT11_BSP_FUNC;

typedef struct dot11_bsp_info
    {
    UINT32 magicNum; /* version */
    UINT32 memBaseAddr; /* Mem base */
    UINT32 ivec; /* IRQ vector */
    UINT32 ilevel; /* IRQ level */
    UINT16 pciVendorId; /* PCI Vendor Id */
    UINT16 pciSubVendorId; /* PCI Subvendor Id*/
    UINT16 pciDeviceId; /* PCI Dev ID */ 
    UINT16 pciSubDeviceId; /* PCI Sub dev Id */
    UINT32 dmaBuffAddr; /* DMA buffer addr */
    UINT32 dmaBufSize; /* dma Buffer Size */
    UINT32 cacheLineSz; /* cache line size */
    UINT32 cfgFlag; /* configuration flag */
    UINT8 pciChipSetRev; /* chipset version */
    DOT11_BSP_FUNC* binding; /* access function provided by BSP */
    DOT11_DEFAULT_CONFIG* config; /* Default configuration */ 
    } DOT11_BSP_INFO;

#define DOT11_BSP_INFO_VER_0 0x1001

#ifdef  __cplusplus
}
#endif /* __cplusplus */
#endif /* __INCdot11Bsph */

