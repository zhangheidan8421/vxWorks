/* dot11TimerLib.h - Contains the main definitions for the 802.11 timer fw*/

/* Copyright 2004 Wind River Systems, Inc. */

/* 
Modification History
--------------------
02a,25aug04,rb  Wind River Wireless Ethernet Driver 2.0 FCS
*/

#include <vxWorks.h>

STATUS dot11TimerInit();
STATUS dot11TimerDestroy();
int dot11TimerAdd(int delayTicks, FUNCPTR action, int param0, int param1);
STATUS dot11TimerDel(int timerId);
STATUS dot11TimerShow();
