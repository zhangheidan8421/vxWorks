/* dot11SmeLib.h - Contains PRIVATE definitions for intra-SME use */

/* Copyright 2004 Wind River Systems, Inc. */

/* 
Modification History
--------------------
01b,22sep05,rb  Fix bad prototype of dot11ChannelSet()
01a,12sep05,rb  Initial verson
*/

/*
DESCRIPTION

This file contains prototypes for using dot11Test.c.  To use this module, include "802.11 command line utilities and test routines" from Workbench

*/


#ifndef __INCdot11Testh
#define __INCdot11Testh
#ifdef  __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <drv/wlan/dot11UsrLib.h>


STATUS dot11TestIoctl(int ifNum, int cmd, caddr_t data);

VOID dot11TestInit(void);
STATUS dot11SSIDSet(int ifNum, char * ssid);
STATUS dot11SSIDGet(int ifNum);
STATUS dot11ModeSet(int ifNum,int mode);
STATUS dot11ModeNone(int ifNum);
STATUS dot11ModeEss(int ifNum);
STATUS dot11ModeIbss(int ifNum);
STATUS dot11ModeAp(int ifNum);
STATUS dot11PmSet(int ifNum, BOOL pmEnabled);
STATUS dot11ChannelSet(int ifNum,int channel);
STATUS dot11SecPolSet(int ifNum, int secPol);
STATUS dot11AuthPolSet(int ifNum,int authPol);
STATUS dot11CipherPolSet(int ifNum, int cipherPol);
STATUS dot11PassphraseSet(int ifNum, char * passphrase);
STATUS dot11PskSet(int ifNum,UINT8 *psk);
STATUS dot11WepSet(int ifNum,int encryptType);
STATUS dot11WepDefaultKeySet(int ifNum,int keySlot);
STATUS dot11WepSkSet(int ifNum,int authType);
STATUS dot11TxPowerScaleSet(int ifNum, UINT32 scale );
STATUS dot11WepKeySet(int ifNum,int keySlot, UINT8 * keyData );
STATUS dot11RateSet(int ifNum, int numRates, ...);
STATUS dot11CtsRtsSet(int ifNum, int rtsThreshold);
STATUS dot11FragThreshSet(int ifNum, int fragThresh);
STATUS dot11RadioModeSet(int ifNum, int radioMode );
STATUS dot11LongRetrySet(int ifNum, int retry);
STATUS dot11ShortRetrySet(int ifNum,int retry);
STATUS dot11LongRetryGet(int ifNum);
STATUS dot11ShortRetryGet(int ifNum);
STATUS dot11CountryCodeSet(int ifNum,int cc);
STATUS dot11SuppRateGet(int ifNum);
STATUS dot111ChannelGet(int ifNum);
STATUS dot11AuthTypeSet(int ifNum,int cc);
STATUS dot11Stats(int ifNum);
STATUS dot11Scan(int ifNum,char * ssid);
STATUS dot11ChannelListGet(int ifNum);
STATUS dot11BeaconSet(int ifNum,int beaconInterval);
STATUS dot11MultiDomainSet(int ifNum,BOOL enable);
STATUS dot11SSIDHide(int ifNum,UINT32 ssidFeatures);
STATUS dot11ScanTypeSet(int ifNum, UINT32 scanType);
STATUS dot11ScanTypeGet(int ifNum);



#ifdef  __cplusplus
}
#endif /* __cplusplus */
#endif /* __INCdot11Testh */


