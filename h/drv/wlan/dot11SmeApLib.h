/* dot11SmeApLib.h - Contains the main definitions for Ap Mode.  */

/* Copyright 2004 Wind River Systems, Inc. */

/* 
Modification History
--------------------
02a,25aug04,rb  Wind River Wireless Ethernet Driver 2.0 FCS
*/

#ifndef __INCdot11SmeApLibh
#define __INCdot11SmeApLibh
#ifdef  __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "vxWorks.h"

/* These are the state variables for the ACL */
#define DOT11_AP_ACL_DISABLED   0
#define DOT11_AP_ACL_ALLOW      1
#define DOT11_AP_ACL_DENY       2

/* Values used for dot11SmeApPsFlush() */
#define DOT11_PM_FLUSH          TRUE
#define DOT11_PM_SINGLE         FALSE

/* Return values from dot11SmeApPsFlush() */
#define DOT11_PM_NO_DATA        0
#define DOT11_PM_LAST_DATA      1
#define DOT11_PM_MORE_DATA      2

#ifdef  __cplusplus
}
#endif /* __cplusplus */
#endif /* __INCdot11SmeApLibh */
