/* dot11UsrLib.h - Contains the definitions required by users of DOT11 lib  */

/* Copyright 2005 Wind River Systems, Inc. */

/* 
Modification History
--------------------
01k,12sep05,rb  Added WIOCSSCANTYPE
01j,25aug05,rb  Modified channel structures for 802.11d
01j,30aug05,rb  Removed legacy IOCTL definitions and added WIOCDPAIRWISEKEY
01i,19aug05,rb  Added macros for turbo mode
01h,04aug05,rb  Added WIOCGCHANNELLIST
01g,03aug05,rb  Added WIOCSUSERAUTHCALLBACK
01f,28apr05,rb  Added _BIT definition to dot11UsrLib.h; Moved encryption types
                 to usrLib
01e,27apr05,rb  Extended DOT11_STATS for SPR 108017
01d,25apr05,rb  Moved definitions needed for IOCTLs from dot11Lib.h to
                 dot11UsrLib.h
01c,14apr05,rb  Added support fr Transmit Power Scaling (WIOCSTXPOWERSCALE)
01b,09mar05,rb  Added WIOCGHWVERS
01a,21feb05,rb  Initial split from dot11Lib.h
*/

/*
DESCRIPTION

This file contains all of the definitions required to manage the Wind River 
Wirelss Ethernet Driver.

*/


#ifndef __INCdot11UsrLibh
#define __INCdot11UsrLibh
#ifdef  __cplusplus
extern "C" {
#endif /* __cplusplus */

/*****************************************************************************
* Public DOT11 Data structures and definitions
*****************************************************************************/
#ifndef _BIT
#define _BIT(x) (1<<(x))
#endif


/**************************************************************************
*  DOT11_SSID - Structure representing an SSID
***************************************************************************/
#define DOT11_SSID_LEN                  32
typedef char  DOT11_SSID[DOT11_SSID_LEN + 1];

/**************************************************************************
*  DOT11_ADDR - An 802.11 hardware address.   Same as MAC address.
***************************************************************************/
#define DOT11_ADDR_LEN                  6
typedef UINT8 DOT11_ADDR[DOT11_ADDR_LEN];

/**************************************************************************
* DOT11_RATES - A set of rates in IEEE format (units of 500 kb/s).  Rates
*                  are listed in ascending order.  If relevent, required
*                  rates are specified by setting bit 7, otherwise this
*                  bit is clear.
**************************************************************************/
#define DOT11_MAX_RATES            256
typedef struct
    {
    int      length;
    UINT8    rates[DOT11_MAX_RATES];
    } _WRS_PACK_ALIGN(1) DOT11_RATES;

/* Routines for manipulating rates according to the IEEE standard */
#define DOT11_RATE(x)              (((x) & 0x7f) / 2)
#define DOT11_RATE_MASK(x)         (((x) & 0x7f))
#define DOT11_IS_BRATE(x)          (((x) & 0x80) == 0x80)

/**************************************************************************
*  DOT11_KEY - An 802.11 Encryption Key. 
***************************************************************************/
typedef unsigned long int  DOT11_KEY_TYPE;

#define DOT11_KEY_TYPE_NONE           0
#define DOT11_KEY_TYPE_WEP40          DOT11_CIPHPOL_WEP40
#define DOT11_KEY_TYPE_WEP104         DOT11_CIPHPOL_WEP104
#define DOT11_KEY_TYPE_TKIP           DOT11_CIPHPOL_TKIP
#define DOT11_KEY_TYPE_AES            DOT11_CIPHPOL_AES

#define DOT11_KEY_TYPE_MAX            (DOT11_KEY_TYPE_AES + 1)

#define DOT11_WEP40_KEY_SIZE          5  /* Bytes */
#define DOT11_WEP104_KEY_SIZE         13 /* Bytes */
#define DOT11_AES_KEY_SIZE            16 /* Bytes */
#define DOT11_TKIP_KEY_SIZE           32 /* 16 Bytes key, 2x8 bytes MIC */

typedef struct
    {
    DOT11_KEY_TYPE keyType;
    INT32 keySlot;                      /* Keyslot key was installed to  */
    UINT8 macAddr[DOT11_ADDR_LEN];
    union
        {
        UINT8 wep40[DOT11_WEP40_KEY_SIZE];
        UINT8 wep104[DOT11_WEP104_KEY_SIZE];
        UINT8 aes[DOT11_AES_KEY_SIZE];
        UINT8 tkip[DOT11_TKIP_KEY_SIZE];
        } type;
    } DOT11_KEY;

/* This is the length of the pre-shared key for WPA and 802.11i.  */
#define DOT11_PSK_LEN                       64 /* bytes */
typedef UINT8 DOT11_PSK[DOT11_PSK_LEN];


#define DOT11_PASSPHRASE_LEN                63
typedef char DOT11_PASSPHRASE[DOT11_PASSPHRASE_LEN];

#define DOT11_HWVERS_BUF_LEN               256
typedef char DOT11_HWVERS_BUF[DOT11_HWVERS_BUF_LEN];

/****************************************************************************
* DOT11_CAPABILITIES - This is the Capabilities field of Management Frames as
*                      per 7.3.1.4 of IEEE 802.11(2003)
****************************************************************************/
typedef UINT16 DOT11_CAPABILITIES;

#define DOT11_CAP_ISESS                 _BIT(0)
#define DOT11_CAP_ISIBSS                _BIT(1)
#define DOT11_CAP_CFPOLLABLE            _BIT(2)
#define DOT11_CAP_CFPOLLREQ             _BIT(3)
#define DOT11_CAP_PRIVACY               _BIT(4)
#define DOT11_CAP_SHORT_PREAMBLE        _BIT(5)
#define DOT11_CAP_PBCC                  _BIT(6)
#define DOT11_CAP_CHANNEL_AGILITY       _BIT(7)
#define DOT11_CAP_SHORT_TIME_SLOT       _BIT(10)

/****************************************************************************
* DOT11_COUNTRY_CODES  - the list of available country codes that can be passed
*                        to WIOCSCOUNTRYCODE.  More country codes can be found
*                        in ISO 3166, but this is the current list supported 
*                        by the available hardware.
*****************************************************************************/

/* DOT11_COUNTRY_NONE can only be used as a default country on a station.  If
it is set as the default country, then 802.11d MUST be used to find the
current country.  If no country-advertising APs are found, then the station
will not transmit anything */ 

#define DOT11_COUNTRY_NONE                  0       

#define DOT11_COUNTRY_ALBANIA               8       
#define DOT11_COUNTRY_ALGERIA               12      
#define DOT11_COUNTRY_ARGENTINA             32      
#define DOT11_COUNTRY_ARMENIA               51      
#define DOT11_COUNTRY_AUSTRALIA             36      
#define DOT11_COUNTRY_AUSTRIA               40      
#define DOT11_COUNTRY_AZERBAIJAN            31      
#define DOT11_COUNTRY_BAHRAIN               48      
#define DOT11_COUNTRY_BELARUS               112     
#define DOT11_COUNTRY_BELGIUM               56      
#define DOT11_COUNTRY_BELIZE                84      
#define DOT11_COUNTRY_BOLIVIA               68      
#define DOT11_COUNTRY_BRAZIL                76      
#define DOT11_COUNTRY_BRUNEI_DARUSSALAM     96      
#define DOT11_COUNTRY_BULGARIA              100     
#define DOT11_COUNTRY_CANADA                124     
#define DOT11_COUNTRY_CHILE                 152     
#define DOT11_COUNTRY_CHINA                 156     
#define DOT11_COUNTRY_COLOMBIA              170     
#define DOT11_COUNTRY_COSTA_RICA            188     
#define DOT11_COUNTRY_CROATIA               191     
#define DOT11_COUNTRY_CYPRUS                196
#define DOT11_COUNTRY_CZECH                 203     
#define DOT11_COUNTRY_DENMARK               208     
#define DOT11_COUNTRY_DOMINICAN_REPUBLIC    214     
#define DOT11_COUNTRY_ECUADOR               218     
#define DOT11_COUNTRY_EGYPT                 818     
#define DOT11_COUNTRY_EL_SALVADOR           222     
#define DOT11_COUNTRY_ESTONIA               233     
#define DOT11_COUNTRY_FAEROE_ISLANDS        234     
#define DOT11_COUNTRY_FINLAND               246     
#define DOT11_COUNTRY_FRANCE                250     
#define DOT11_COUNTRY_FRANCE2               255     
#define DOT11_COUNTRY_GEORGIA               268     
#define DOT11_COUNTRY_GERMANY               276     
#define DOT11_COUNTRY_GREECE                300     
#define DOT11_COUNTRY_GUATEMALA             320     
#define DOT11_COUNTRY_HONDURAS              340     
#define DOT11_COUNTRY_HONG_KONG             344     
#define DOT11_COUNTRY_HUNGARY               348     
#define DOT11_COUNTRY_ICELAND               352     
#define DOT11_COUNTRY_INDIA                 356     
#define DOT11_COUNTRY_INDONESIA             360     
#define DOT11_COUNTRY_IRAN                  364     
#define DOT11_COUNTRY_IRAQ                  368     
#define DOT11_COUNTRY_IRELAND               372     
#define DOT11_COUNTRY_ISRAEL                376     
#define DOT11_COUNTRY_ITALY                 380     
#define DOT11_COUNTRY_JAMAICA               388     
#define DOT11_COUNTRY_JAPAN                 392     
#define DOT11_COUNTRY_JAPAN1                393     
#define DOT11_COUNTRY_JAPAN2                394     
#define DOT11_COUNTRY_JAPAN3                395     
#define DOT11_COUNTRY_JAPAN4                396     
#define DOT11_COUNTRY_JAPAN5                397     
#define DOT11_COUNTRY_JORDAN                400     
#define DOT11_COUNTRY_KAZAKHSTAN            398     
#define DOT11_COUNTRY_KENYA                 404     
#define DOT11_COUNTRY_KOREA_NORTH           408     
#define DOT11_COUNTRY_KOREA_ROC             410     
#define DOT11_COUNTRY_KOREA_ROC2            411     
#define DOT11_COUNTRY_KUWAIT                414     
#define DOT11_COUNTRY_LATVIA                428     
#define DOT11_COUNTRY_LEBANON               422     
#define DOT11_COUNTRY_LIBYA                 434     
#define DOT11_COUNTRY_LIECHTENSTEIN         438     
#define DOT11_COUNTRY_LITHUANIA             440     
#define DOT11_COUNTRY_LUXEMBOURG            442     
#define DOT11_COUNTRY_MACAU                 446     
#define DOT11_COUNTRY_MACEDONIA             807     
#define DOT11_COUNTRY_MALAYSIA              458     
#define DOT11_COUNTRY_MEXICO                484     
#define DOT11_COUNTRY_MONACO                492     
#define DOT11_COUNTRY_MOROCCO               504     
#define DOT11_COUNTRY_NETHERLANDS           528     
#define DOT11_COUNTRY_NEW_ZEALAND           554     
#define DOT11_COUNTRY_NICARAGUA             558     
#define DOT11_COUNTRY_NORWAY                578     
#define DOT11_COUNTRY_OMAN                  512     
#define DOT11_COUNTRY_PAKISTAN              586     
#define DOT11_COUNTRY_PANAMA                591     
#define DOT11_COUNTRY_PARAGUAY              600     
#define DOT11_COUNTRY_PERU                  604     
#define DOT11_COUNTRY_PHILIPPINES           608     
#define DOT11_COUNTRY_POLAND                616     
#define DOT11_COUNTRY_PORTUGAL              620     
#define DOT11_COUNTRY_PUERTO_RICO           630     
#define DOT11_COUNTRY_QATAR                 634     
#define DOT11_COUNTRY_ROMANIA               642     
#define DOT11_COUNTRY_RUSSIA                643     
#define DOT11_COUNTRY_SAUDI_ARABIA          682     
#define DOT11_COUNTRY_SINGAPORE             702     
#define DOT11_COUNTRY_SLOVAKIA              703     
#define DOT11_COUNTRY_SLOVENIA              705     
#define DOT11_COUNTRY_SOUTH_AFRICA          710     
#define DOT11_COUNTRY_SPAIN                 724     
#define DOT11_COUNTRY_SWEDEN                752     
#define DOT11_COUNTRY_SWITZERLAND           756     
#define DOT11_COUNTRY_SYRIA                 760     
#define DOT11_COUNTRY_TAIWAN                158     
#define DOT11_COUNTRY_THAILAND              764     
#define DOT11_COUNTRY_TRINIDAD_Y_TOBAGO     780     
#define DOT11_COUNTRY_TUNISIA               788     
#define DOT11_COUNTRY_TURKEY                792     
#define DOT11_COUNTRY_UAE                   784     
#define DOT11_COUNTRY_UKRAINE               804     
#define DOT11_COUNTRY_UNITED_KINGDOM        826     
#define DOT11_COUNTRY_UNITED_STATES         840     
#define DOT11_COUNTRY_URUGUAY               858     
#define DOT11_COUNTRY_UZBEKISTAN            860     
#define DOT11_COUNTRY_VENEZUELA             862     
#define DOT11_COUNTRY_VIET_NAM              704     
#define DOT11_COUNTRY_YEMEN                 887     
#define DOT11_COUNTRY_ZIMBABWE              716      


/*****************************************************************************
* Public DOT11 IOCTL Commands 
*****************************************************************************/
#include "sys/ioctl.h"

/* For backwards compatability, define the "user mode" IOCTL macros manually
if they are not already defined.  They should be defined in vxWorks 6.1 and 
higher */

#ifndef IOC_USER
#define IOC_USER 0x8000
#endif /* IOC_USER */

#ifndef _IOU
#define _IOU(x,y)              (IOC_USER | _IO(x,y))
#endif

#ifndef _IORU
#define _IORU(x,y,t)           (IOC_USER | _IOR(x,y,t))
#endif

#ifndef _IOWU
#define _IOWU(x,y,t)           (IOC_USER | _IOW(x,y,t))
#endif

#ifndef _IOWRU
#define _IOWRU(x,y,t)          (IOC_USER | _IOWR(x,y,t))
#endif

/***************************************************************************
* WIOCGSTATS - Get Card Statistics 
****************************************************************************/

typedef struct /* DOT11_STATS */
    {
    /* total number of packets (after reassembly) both unicast and
    multicast data frames. */
    UINT32        rxFrames; 

    /* total number of received fragments, both data and 
    management, multicast and unicast.    */    
    UINT32        rxFragments;
    
    /* total number of received multicast data fragments */
    UINT32        rxMcastFragments;
    
    /* total bytes of data received, all frame types and destinations */
    unsigned long long int        rxTotalBytes;

    /* Number of frames erroneously retransmitted; frames with same sequence
    number as the one previous */
    UINT32        rxDuplicates;
    
    /* Number of frames dropped by the hardware due to Frame CheckSum 
    failure */
    UINT32        rxFCSError;
    
    /* Number of frames dropped due to miscellaneous errors */
    UINT32        rxHardwareError;
    
    /* Number of frames received that were unable to be decoded, most likely 
    due to incorrect encryption keys */
    UINT32        rxDecryptError;
    
    /* Michael failures.  This statistic only applies to TKIP frames */
    UINT32        rxMicFailure;
    
    /* Number of unencrypted frames received and dropped while encryption is
    turned on */
    UINT32        rxExcludeUnencrypted;

    /* Number of received frames that were dropped due to the receive port 
    being 802.1X unauthorized */
    UINT32        rxDot1xDropped;
    
    /* Frames that were dropped because there wasn't room in the reassembly
    queue, because all fragments were not received in the specified timeout
    period, or beacause an out-of-order fragment was received. */
    UINT32        rxCantReassemble;

    /* The number of data bytes that were received in the last second, updated 
    every second. */
    UINT32        rxBytesPerSec;
    
    /* The nubmer of databytes received in the last sixteen seconds, updated 
    every sixteen seconds. */
    UINT32        rxBytesPer16Sec;
    
    /* The total number of data frames (before fragmentation) transmitted */
    UINT32        txFrames;
    
    /* The total number of fragments (data and management) transmitted */
    UINT32        txFragments;
    
    /* The total number of multicast data frames transmitted */
    UINT32        txMcastFragments;

    /* The total number of bytes in all packets transmitted */
    unsigned long long int txTotalBytes;
    
    /* The number of packets that were succesfully transmitted after one or 
    more retries */
    UINT32        txRetry;

    /* The number of packets that were transmitted after more than one retry */
    UINT32        txRetryMultiple;
    
    /* The number of packets that were not transmitted due to too many 
    retries */
    UINT32        txRetryFailure;
    
    /* The number of times a transmitted frame failed to get an ACK from the 
    receiver */
    UINT32        txAckFailure;

    /* The number of RTS frames transmitted to which a CTS was never 
    received */
    UINT32        txRTSFailure;
       
    /* The number of RTS frames transmitted to which a CTS was received */
    UINT32        txRTSSuccess;
    
    /* The number of egress frames dropped due to hardware failure or queue 
    overflow */
    UINT32        txHardwareError;
    
    /* The number of egress data frames dropped due to the 802.1X port being 
    unauthorized. */
    UINT32        txDot1xDropped;
    
    /* The number of data bytes transmitted in the last second, updated every 
    second */
    UINT32        txBytesPerSec;
    
    /* The number of data bytes transmitted in the last sixteen seconds, 
    updated every sixteen seconds */
    UINT32        txBytesPer16Sec;

    /* The number of four-way handshakes that completed successfully, as far 
    as this unit can tell. Note that this cannot catch errors that occured on 
    a station on the last frame sent to the AP */
    UINT32        fourWaySuccess;
    
    /* The number of four-way handshakes that failed at this unit. */
    UINT32        fourWayFailure;

    /* The number of group key exchanges that completed successfully, as far as
    this unit can tell.  Note that this cannot detect problems with the last
    packet a station sends to the AP if this unit is a station. */
    UINT32        groupExchangeSuccess;

    /* The number of group key exchanges that failed */
    UINT32        groupExchangeFailure;

    } DOT11_STATS;

#define WIOCGSTATS             _IORU('w', 0x001, DOT11_STATS)

/***************************************************************************
* WIOCGLINKSTAT - Get Link Status 
****************************************************************************/

/* Link Status */
#define DOT11_LINK_SLEEP                2
#define DOT11_LINK_UP                   1
#define DOT11_LINK_DOWN                 0

#define WIOCGLINKSTAT          _IORU('w', 0x002, UINT32)

/***************************************************************************
* WIOCGDESIREDSSID - Get Wireless Network Name 
****************************************************************************/
#define WIOCGDESIREDSSID       _IORU('w', 0x003, DOT11_SSID)

/***************************************************************************
* WIOCSSSID - Set Wireless Network Name 
****************************************************************************/
#define WIOCSSSID              _IOWU('w', 0x004, DOT11_SSID)

/***************************************************************************
* WIOCGCONNECTEDSSID - Get current BSS 
****************************************************************************/
#define WIOCGCONNECTEDSSID     _IORU('w', 0x005, DOT11_SSID)

/***************************************************************************
* WIOCGCONNECTEDBSSID - Get connected BSSID 
****************************************************************************/
#define WIOCGCONNECTEDBSSID    _IORU('w', 0x006, DOT11_ADDR)

/***************************************************************************
* WIOCGTXRATE - Get current transmit rate 
****************************************************************************/
#define WIOCGTXRATE            _IORU('w', 0x007, UINT32)

/***************************************************************************
* WIOCSTXRATE - Set Transmit Speed 
****************************************************************************/
#define WIOCSTXRATE            _IOWU('w', 0x008, DOT11_RATES)

/***************************************************************************
* WIOCGPWRMAN - Get Power Management Status 
****************************************************************************/
#define WIOCGPWRMAN            _IORU('w', 0x009, UINT32)

/***************************************************************************
* WIOCSPWRMAN - Set Power Management Status 
****************************************************************************/
#define DOT11_PWRMAN_ENABLED   TRUE
#define DOT11_PWRMAN_DISABLED  FALSE

#define WIOCSPWRMAN            _IOU('w', 0x00a)

/***************************************************************************
* WIOCGCOMMQUAL - Get Communications Quality 
****************************************************************************/
#define WIOCGCOMMQUAL          _IORU('w', 0x00b, UINT32) 

/***************************************************************************
* WIOCGCARDTYPE - Get Card Type 
****************************************************************************/
#define WIOCGCARDTYPE          _IORU('w', 0x00c, UINT32) 

/***************************************************************************
* WIOCGCHANNEL - Get Channel 
****************************************************************************/
#define WIOCGCHANNEL           _IORU('w', 0x00d, UINT32) 

/***************************************************************************
* WIOCSCHANNEL - Set Channel 
****************************************************************************/
#define WIOCSCHANNEL           _IOU('w', 0x00e)            

/***************************************************************************
* WIOCGWEPAVAIL - Get WEP Availability 
****************************************************************************/
#define WIOCGWEPAVAIL          _IORU('w', 0x00f, UINT32) 

/***************************************************************************
* WIOCSWEPKEY0 - Set value for WEP KEY 0 
****************************************************************************/
#define WIOCSWEPKEY0           _IOWU('w', 0x010, DOT11_KEY) 

/***************************************************************************
* WIOCSWEPKEY1 - Set value for WEP KEY 1 
****************************************************************************/
#define WIOCSWEPKEY1           _IOWU('w', 0x011, DOT11_KEY) 

/***************************************************************************
* WIOCSWEPKEY2 - Set value for WEP KEY 2 
****************************************************************************/
#define WIOCSWEPKEY2           _IOWU('w', 0x012, DOT11_KEY) 

/***************************************************************************
* WIOCSWEPKEY3 - Set value for WEP KEY 3 
****************************************************************************/
#define WIOCSWEPKEY3           _IOWU('w', 0x013, DOT11_KEY) 

/***************************************************************************
* WIOCSWEPDEFAULTKEY - Set WEP default key 
****************************************************************************/
#define WIOCSWEPDEFAULTKEY     _IOU('w', 0x014)            

/***************************************************************************
* WIOCGENCRYPTTYPE - Get current encryption type 
****************************************************************************/
#define WIOCGENCRYPTTYPE       _IORU('w', 0x015, UINT32) 

/***************************************************************************
* WIOCSENCRYPTTYPE - Set current encryption type 
****************************************************************************/
typedef enum 
    { 
    DOT11_ENCRYPT_NONE, 
    DOT11_ENCRYPT_WEP40, 
    DOT11_ENCRYPT_WEP104,
    DOT11_ENCRYPT_MAX      /* Number of entries in enumeration */
    } DOT11_ENCRYPT_TYPE;

#define WIOCSENCRYPTTYPE       _IOU('w', 0x016)            

/***************************************************************************
* WIOCAACLSTA - Add a STA to AP authentication list 
****************************************************************************/
#define WIOCAACLSTA            _IOWU('w', 0x017, DOT11_ADDR)

/***************************************************************************
* WIOCDACLSTA - Delete a STA from AP auth  list 
****************************************************************************/
#define WIOCDACLSTA            _IOWU('w', 0x018, DOT11_ADDR)

/***************************************************************************
* WIOCSEXUNENCRYPT - Set exclude unencrypted 
****************************************************************************/
typedef enum 
    {
    DOT11_EX_UNENCRYPTED,  /* Exclude all unencrypted packets */
    DOT11_EX_DOT1X,        /* Exclude all unencrypted packets except EAPOL */
    DOT11_EX_NONE          /* Allow unencrypted and encrypted traffic */
    } DOT11_EX_POLICY;

#define WIOCSEXUNENCRYPT       _IOU('w', 0x019)            

/***************************************************************************
* WIOCGEXUNENCRYPT - Get exclude unencrypted              
****************************************************************************/
#define WIOCGEXUNENCRYPT       _IORU('w', 0x01a, UINT32) 

/***************************************************************************
* WIOCGAUTHTYPE - Get Authentication protocol          UINT32 *  
****************************************************************************/
#define DOT11_AUTHTYPE_OPEN    1  /* Open system authentication */
#define DOT11_AUTHTYPE_SKA     2  /* Shared key authentication */
#define DOT11_AUTHTYPE_ALL     3  /* Allow all available authentication type */
#define WIOCGAUTHTYPE          _IORU('w', 0x01b, UINT32) 

/***************************************************************************
* WIOCSAUTHTYPE - Set Authentication protocol          UINT32     
****************************************************************************/
#define WIOCSAUTHTYPE          _IOU('w', 0x01c)            

/***************************************************************************
* WIOCSBEACONRATE - Set rate for AP beaconing            UINT32  
****************************************************************************/
#define WIOCSBEACONRATE        _IOU('w', 0x01e)            

/***************************************************************************
* WIOCGBEACONRATE - Get rate for AP beaconing            UINT32 *  
****************************************************************************/
#define WIOCGBEACONRATE        _IORU('w', 0x01f, UINT32) 

/***************************************************************************
* WIOCGMAC - Get MAC address                      UNIT32 *  
****************************************************************************/
#define WIOCGMAC               _IORU('w', 0x020, DOT11_ADDR)

/***************************************************************************
* WIOCSCTSRTS - Set CTS/RTS                          UINT32  
****************************************************************************/
#define DOT11_CTSRTS_MAX       2307
#define WIOCSCTSRTS            _IOU('w', 0x021)            

/***************************************************************************
* WIOCGCTSRTS - Get CTS/RTS                          UINT32 *  
****************************************************************************/
#define WIOCGCTSRTS            _IORU('w', 0x022, UINT32) 

/***************************************************************************
* WIOCSSHORTRETRY - Set Short Retry Limit                
****************************************************************************/
#define WIOCSSHORTRETRY        _IOU('w', 0x023)            

/***************************************************************************
* WIOCGSHORTRETRY - Get Short Retry Limit                
****************************************************************************/
#define WIOCGSHORTRETRY        _IORU('w', 0x024, UINT32) 

/***************************************************************************
* WIOCSLONGRETRY - Set Long Retry Limit                 
****************************************************************************/
#define WIOCSLONGRETRY         _IOU('w', 0x025)            

/***************************************************************************
* WIOCGLONGRETRY - Get Long Retry Limit                 
****************************************************************************/
#define WIOCGLONGRETRY         _IORU('w', 0x026, UINT32) 

/***************************************************************************
* WIOCSFRAGTHRESH - Set Fragmentation threshold          UINT32  
****************************************************************************/
#define WIOCSFRAGTHRESH        _IOU('w', 0x027)            

/***************************************************************************
* WIOCGFRAGTHRESH - Get Fragmentation threshold          UINT32 *  
****************************************************************************/
#define WIOCGFRAGTHRESH        _IORU('w', 0x028, UINT32) 

/***************************************************************************
* WIOCSMAXTXLIFE - Set Max Transmit MSDU Life           
****************************************************************************/
#define WIOCSMAXTXLIFE         _IOU('w', 0x029)            

/***************************************************************************
* WIOCGMAXTXLIFE - Get Max Transmit MSDU Life           
****************************************************************************/
#define WIOCGMAXTXLIFE         _IORU('w', 0x02a, UINT32) 

/***************************************************************************
* WIOCSMAXRXLIFE - Set Max Receive Lifetime             
****************************************************************************/
#define WIOCSMAXRXLIFE         _IOU('w', 0x02b)            

/***************************************************************************
* WIOCGMAXRXLIFE - Get Max Receive Lifetime             
****************************************************************************/
#define WIOCGMAXRXLIFE         _IORU('w', 0x02c, UINT32) 

/***************************************************************************
* WIOCSPREAMBLE - Set preamble length                  UINT32  
****************************************************************************/
#define WIOCSPREAMBLE          _IOU('w', 0x02d)            

/***************************************************************************
* WIOCGPREAMBLE - Get preamble length                  UINT32 *  
****************************************************************************/
#define WIOCGPREAMBLE          _IORU('w', 0x02e, UINT32) 

/***************************************************************************
* WIOCSAUTHRESPTO - Set Authentication response timeout  UINT32  
****************************************************************************/
#define WIOCSAUTHRESPTO        _IOU('w', 0x02f)            

/***************************************************************************
* WIOCGAUTHRESPTO - Get Authentication response timeout  UINT32 *    
****************************************************************************/
#define WIOCGAUTHRESPTO        _IORU('w', 0x030, UINT32) 

/* WIOCSUNICASTKEYNUM, WIOCGUNICASTKEYNUM, WIOCSMULTICASTKEYNUM, 
WIOCGMULTICASTKEYNUM are all invalid IOCTLs as of the 2.2 version. */

/***************************************************************************
* WIOCSADVSECURITY - Set advanced security options        UINT32  
****************************************************************************/
#define DOT11_ADVSEC_HIDE_SSID     _BIT(0)
#define DOT11_ADVSEC_BLOCK_BCAST_SSID _BIT(1)

#define WIOCSADVSECURITY       _IOU('w', 0x035)            

/***************************************************************************
* WIOCGADVSECURITY - Get advanced security options        UINT32 *  
****************************************************************************/
#define WIOCGADVSECURITY       _IORU('w', 0x036, UINT32) 

/***************************************************************************
* WIOCSOCCUPANCY - Set Medium Occupancy Limit           
****************************************************************************/
#define WIOCSOCCUPANCY         _IOU('w', 0x037)            

/***************************************************************************
* WIOCGOCCUPANCY - Get Medium Occupancy Limit           
****************************************************************************/
#define WIOCGOCCUPANCY         _IORU('w', 0x038, UINT32) 

/***************************************************************************
* WIOCGCFPPOLL - Get CFP Pollable                     UNIT 32 *  
****************************************************************************/
#define WIOCGCFPPOLL           _IORU('w', 0x039, UINT32) 

/***************************************************************************
* WIOCSCFPPERIOD - Set CFP Period                       
****************************************************************************/
#define WIOCSCFPPERIOD         _IOU('w', 0x03a)            

/***************************************************************************
* WIOCGCFPPERIOD - Get CFP Period                       
****************************************************************************/
#define WIOCGCFPPERIOD         _IORU('w', 0x03b, UINT32) 

/***************************************************************************
* WIOCSCFPMAX - Set CFP Max Duration                 
****************************************************************************/
#define WIOCSCFPMAX            _IOU('w', 0x03c)            

/***************************************************************************
* WIOCGCFPMAX - Get CFP Max duration                 
****************************************************************************/
#define WIOCGCFPMAX            _IORU('w', 0x03d, UINT32) 

/***************************************************************************
* WIOCSDTIMRATE - Set DTIM rate                        
****************************************************************************/
#define WIOCSDTIMRATE          _IOU('w', 0x03e)            

/***************************************************************************
* WIOCGDTIMRATE - Get DTIM rate                        
****************************************************************************/
#define WIOCGDTIMRATE          _IORU('w', 0x03f, UINT32) 

/***************************************************************************
* WIOCGSUPPTXRATE - Get Supported Tx rates               
****************************************************************************/
#define WIOCGSUPPTXRATE        _IORU('w', 0x040, DOT11_RATES)

/***************************************************************************
* WIOCGSUPPRXRATE - Get Supported Rx Rates               
****************************************************************************/
#define WIOCGSUPPRXRATE        _IORU('w', 0x041, DOT11_RATES)

/***************************************************************************
* WIOCSASSOCRESPTIME - Set association response timeout     
****************************************************************************/
#define WIOCSASSOCRESPTIME     _IOU('w', 0x042)            

/***************************************************************************
* WIOCGASSOCRESPTIME - Get association response timeout     
****************************************************************************/
#define WIOCGASSOCRESPTIME     _IORU('w', 0x043, UINT32)

/***************************************************************************
* WIOCSBCASTRATE - Set Transmit Speed (Multi & broad) |  UINT32 
****************************************************************************/
#define WIOCSBCASTRATE         _IOU('w', 0x044)

/***************************************************************************
* WIOCGBCASTRATE - Get Transmit Speed (Multi & broad) |  UINT32 
****************************************************************************/
#define WIOCGBCASTRATE         _IORU('w', 0x045, UINT32)

/***************************************************************************
* WIOCSRADIOMODE - Set Radio Mode (allowed freqs)     |  UINT32 
****************************************************************************/
#define DOT11_RADIO_NONE                 0
#define DOT11_RADIO_11a                  _BIT(0)
#define DOT11_RADIO_11b                  _BIT(1)
#define DOT11_RADIO_11g                  _BIT(2)
#define DOT11_RADIO_TURBOg               _BIT(3)
#define DOT11_RADIO_TURBOa               _BIT(4)
#define DOT11_RADIO_ANY                  0x1f

#define WIOCSRADIOMODE         _IOU('w', 0x046)

/***************************************************************************
* WIOCGRADIOMODE - Get Radio Mode (allowed freqs)     |  UINT32 * 
****************************************************************************/
#define WIOCGRADIOMODE         _IORU('w', 0x047, UINT32)

/***************************************************************************
* WIOCSDOT11MODE - Set Dot11 Mode (SME mode)          |  UINT32   
****************************************************************************/
#define DOT11_MODE_NONE                 0
#define DOT11_MODE_ESS                  1
#define DOT11_MODE_IBSS                 2
#define DOT11_MODE_AP                   3

#define WIOCSDOT11MODE         _IOU('w', 0x048)

/***************************************************************************
* WIOCGDOT11MODE - Get Dot11 Mode (SME mode)          |  UINT32 * 
****************************************************************************/
#define WIOCGDOT11MODE         _IORU('w', 0x049, UINT32)

/***************************************************************************
* WIOCSACLMODE - Set the Access Control List Mode   |  UINT32   
****************************************************************************/
#define DOT11_ACL_DISABLED      0
#define DOT11_ACL_ALLOW         1
#define DOT11_ACL_DENY          2

#define WIOCSACLMODE           _IOU('w', 0x04a)

/***************************************************************************
* WIOCGACLMODE - Set the Access Control List Mode   |  UINT32   
****************************************************************************/
#define WIOCGACLMODE           _IORU('w', 0x04b, UINT32)

/***************************************************************************
* WIOCGWEPDEFAULTKEY - Set WEP default key                  UINT32  
****************************************************************************/
#define WIOCGWEPDEFAULTKEY     _IORU('w', 0x04c, UINT32)

/***************************************************************************
* WIOCSDESIREDSSID - Set Wireless Network Name            char *  
****************************************************************************/
#define WIOCSDESIREDSSID        _IOWU('w', 0x04d, DOT11_SSID)

/***************************************************************************
* WIOCUNSUPPORTED - Used to force an ERROR code from the IOCTL   
****************************************************************************/
#define WIOCUNSUPPORTED        _IOU('w', 0x04e)

/***************************************************************************
* WIOCAPAIRWISEKEY - Adds a pairwise key and returns the keyslot in the passed structure 
****************************************************************************/
#define WIOCAPAIRWISEKEY       _IOWU('w', 0x04f, DOT11_KEY)

/***************************************************************************
* WIOCSSECPOL - Set the available security policies 
****************************************************************************/
#define DOT11_SECPOL_NONE      0
#define DOT11_SECPOL_TSN       _BIT(0)
#define DOT11_SECPOL_WPA       _BIT(1)
#define DOT11_SECPOL_11i       _BIT(2)

#define WIOCSSECPOL            _IOU('w', 0x050)

/***************************************************************************
* WIOCGSECPOL - Gt the available security policies 
****************************************************************************/
#define WIOCGSECPOL            _IORU('w', 0x051, UINT32)

/***************************************************************************
* WIOCSAUTHPOL - Set the available authentication policies 
****************************************************************************/
#define DOT11_AUTHPOL_NONE     0
#define DOT11_AUTHPOL_8021X    _BIT(0)
#define DOT11_AUTHPOL_PSK      _BIT(1)
#define DOT11_AUTHPOL_MAX      (DOT11_AUTHPOL_PSK + 1)

#define WIOCSAUTHPOL           _IOU('w', 0x052)

/***************************************************************************
* WIOCGAUTHPOL - Get the available authentication policies 
****************************************************************************/
#define WIOCGAUTHPOL           _IORU('w', 0x053, UINT32)

/***************************************************************************
* WIOCSCIPHPOL - Set the available cipher suites 
****************************************************************************/
#define DOT11_CIPHPOL_NONE     0
#define DOT11_CIPHPOL_WEP40    _BIT(0)
#define DOT11_CIPHPOL_WEP104   _BIT(1)
#define DOT11_CIPHPOL_TKIP     _BIT(2)
#define DOT11_CIPHPOL_AES      _BIT(3)
#define DOT11_CIPHPOL_MAX      (DOT11_CIPHPOL_AES + 1)

#define WIOCSCIPHPOL           _IOU('w', 0x054)

/***************************************************************************
* WIOCGCIPHPOL - Get the available cipher suites 
****************************************************************************/
#define WIOCGCIPHPOL           _IORU('w', 0x055, UINT32)

/***************************************************************************
* WIOCSPSK - Set the pre-shared key 
****************************************************************************/
#define WIOCSPSK               _IOWU('w', 0x56, DOT11_PSK)

/***************************************************************************
* WIOCSPASSPHRASE - Sets the pre-shared key using a passphrase 
****************************************************************************/
#define WIOCSPASSPHRASE        _IOWU('w', 0x57, DOT11_PASSPHRASE)

/***************************************************************************
* WIOCBSSSCAN - Performs a BSS scan and waits for the results 
****************************************************************************/

typedef struct 
    {
    /* SSID of the detected BSS.  empty string if SSID is hidden. */
    char                  ssid[DOT11_SSID_LEN + 1];
    
    /* 802.11 capabilities field.  See DOT11_CAP_... macros */
    UINT16                capabilities;
    DOT11_RATES           rates;
    UINT16                beaconInterval;
    int                   channel;
    UINT8                 ssi;
    } DOT11_SCAN_RESULTS;

typedef struct 
    {
    /* The SSID to search for */   
    char                   ssid[DOT11_SSID_LEN + 1];

    /* The number of entries in the pre-allocated array */
    int                    maxResults;
    
    /* The pointer to the pre-allocated array in which to put the results */
    DOT11_SCAN_RESULTS *   pResults;
    
    /* After WIOCBSSCAN is called, the number of results placed in the array */
    int                    numResults;

    } DOT11_SCAN_REQ;

#define WIOCBSSSCAN            _IOWRU('w', 0x058, DOT11_SCAN_REQ)

/***************************************************************************
* WIOCSCOUNTRYCODE - Sets the current country code 
****************************************************************************/
#define WIOCSCOUNTRYCODE       _IOU('w', 0x059)

/***************************************************************************
* WIOCGCOUNTRYCODE - Returns the current country code 
****************************************************************************/
#define WIOCGCOUNTRYCODE       _IORU('w', 0x5a, UINT32)

/***************************************************************************
* WIOCS80211D - Enable 802.11d support 
****************************************************************************/
#define WIOCS80211D            _IOU('w', 0x5b)

/***************************************************************************
* WIOCG80211D - Returns current status of 802.11d 
****************************************************************************/
#define WIOCG80211D            _IORU('w', 0x5c, UINT32)

#define WIOCSBULK              _IOU('w', 0x5d)

/***************************************************************************
*WIOCGHWVERS -  Returns hardware information string 
****************************************************************************/
#define WIOCGHWVERS            _IORU('w', 0x5e, DOT11_HWVERS_BUF)

/***************************************************************************
* WIOCGTXPOWERSCALE - Gets the current tx power scaling factor 
****************************************************************************/
#define WIOCGTXPOWERSCALE      _IORU('w', 0x5f, UINT32)

/***************************************************************************
* WIOCSTXPOWERSCALE - Sets the current tx power scaling factor 
****************************************************************************/
#define DOT11_TXPOWER_MAX  0
#define DOT11_TXPOWER_66   1
#define DOT11_TXPOWER_50   2
#define DOT11_TXPOWER_33   3
#define DOT11_TXPOWER_MIN  4

#define WIOCSTXPOWERSCALE      _IOU('w', 0x60)

/***************************************************************************
* WIOCSUSERAUTHCALLBACK - Sets the current user authentication callback
****************************************************************************/
#define DOT11_CALLBACK_CONNECT     (1) /* Callback called due to association*/
#define DOT11_CALLBACK_DISCONNECT  (2) /* Callback called due to disassociation*/

#define WIOCSUSERAUTHCALLBACK  _IOW('w', 0x61, FUNCPTR)

/***************************************************************************
* WIOCDPAIRWISEKEY - Removes a WEP key mapping key added by WIOCAPAIRWISEKEY
****************************************************************************/
#define WIOCDPAIRWISEKEY       _IOWU('w', 0x62, DOT11_ADDR)


/***************************************************************************
* WIOCGCHANNELLIST - Returns a list of the channels supported by the current
*                    country code.
****************************************************************************/

/* The maximum number of channels in a channel list */
#define DOT11_MAX_CHANNELS              64

typedef struct
    {
    int channel;
    int freq;
    UINT32 mode;
    BOOL activeScanAllowed;
    } DOT11_CHANNEL_INFO;

typedef struct
    {
    int numChannels;
    DOT11_CHANNEL_INFO channels[DOT11_MAX_CHANNELS];
    } DOT11_CHANNEL_LIST;

#define WIOCGCHANNELLIST      _IOWRU('w', 0x63, DOT11_CHANNEL_LIST)

/***************************************************************************
* WIOCSSCANTYPE - Enables active and/or passive scanning for ESS/IBSS modes
****************************************************************************/
/* Values for sme.scanType.  One or both of these values can be set */
#define DOT11_SCANTYPE_ACTIVE           _BIT(0)
#define DOT11_SCANTYPE_PASSIVE          _BIT(1)

#define WIOCSSCANTYPE          _IOU('w', 0x64)

/***************************************************************************
* WIOCGSCANTYPE - Returns current scan types in use.
****************************************************************************/
#define WIOCGSCANTYPE          _IORU('w', 0x64, UINT32)




#ifdef  __cplusplus
}
#endif /* __cplusplus */
#endif /* __INCdot11UsrLibh */
