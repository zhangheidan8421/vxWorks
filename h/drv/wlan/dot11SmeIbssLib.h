/* dot11SmeIbssLib.h - Contains the main definitions for IBSS Mode.  */

/* Copyright 2004 Wind River Systems, Inc. */

/* 
Modification History
--------------------
02a,25aug04,rb  Wind River Wireless Ethernet Driver 2.0 FCS
*/

#ifndef __INCdot11SmeIbssLibh
#define __INCdot11SmeIbssLibh
#ifdef  __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "vxWorks.h"

/* Wait two ticks for a task to delete itself */
#define DOT11_IBSS_TASK_DELETE_TIMEOUT 2 

/***********************************************************************
* IBSS STATE TASK - This section contains definitions used by the state 
*                   task
************************************************************************/

/* These are the parameters used to set up the stat task */
#define DOT11_IBSS_STATE_TASK_NAME   "tIbssState%d"
#define DOT11_IBSS_STATE_TASK_PRI    166
#define DOT11_IBSS_STATE_TASK_STACK  3000
#define DOT11_IBSS_STATE_MAX_MSGS    64

/* States that are possible in the IBSS state machine */
typedef enum
    {
    DOT11_IBSS_STATE_NOIBSS,            /* Initial state */
    DOT11_IBSS_STATE_SEARCHING,         /* Performing a search for the IBSS */
    DOT11_IBSS_STATE_SYNC,
    DOT11_IBSS_STATE_CONNECTED          /* Connected to an IBSS */
   } DOT11_IBSS_STATE;

/* Events that are passed to the event queue thought sme->linkEventNotify() */
typedef enum
    {
    DOT11_IBSS_EV_RESET,
    DOT11_IBSS_EV_JOIN_REQ,
    DOT11_IBSS_EV_SYNC_IBSS,
    DOT11_IBSS_EV_CREATE_IBSS,
    DOT11_IBSS_EV_JOIN_IBSS,
    DOT11_IBSS_EV_SHUTDOWN
    } DOT11_IBSS_EV;

#define DOT11_IBSS_BEACON_START_IMMED  0
#define DOT11_IBSS_BEACON_START_RESUME  -1


#ifdef  __cplusplus
}
#endif /* __cplusplus */
#endif /* __INCdot11SmeIbssLibh */









