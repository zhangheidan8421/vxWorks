/* esd_can_pc104_200.h - definitions for ESD CAN PC104/200 board */

/* Copyright 2001 Wind River Systems, Inc. */

/* 
modification history 
--------------------
20dec01, dnb written

*/

/* 

DESCRIPTION
This file contains definitions used only in esd_can_pc104_200.c
and esd_can_pc104_200_cfg.c 

*/

#ifndef ESD_CAN_PC104_200_H
#define ESD_CAN_PC104_200_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define ESD_CAN_PC104_200_MAX_CONTROLLERS  (2)

struct ESD_CAN_PC104_200_DeviceEntry
{
    ULONG  ioAddress;
    UINT   irq;
    BOOL   inUse;

    struct WNCAN_Device     canDevice[ESD_CAN_PC104_200_MAX_CONTROLLERS];
    struct WNCAN_Controller canControllerArray[ESD_CAN_PC104_200_MAX_CONTROLLERS];
    struct WNCAN_Board      canBoardArray[ESD_CAN_PC104_200_MAX_CONTROLLERS];
    BOOL                    allocated[ESD_CAN_PC104_200_MAX_CONTROLLERS];

    WNCAN_ChannelMode i82527chnMode[I82527_MAX_MSG_OBJ]; 
    struct i82527ChipSpecific  can82527;

    WNCAN_ChannelMode sja1000chnMode[SJA1000_MAX_MSG_OBJ]; 
    struct TxMsg txMsg;

};


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* ESD_CAN_PC104_200_H */
