/* msmcan.h - definitions for MSMCAN PC104 board */

/* Copyright 2001 Wind River Systems, Inc. */

/* 
modification history 
--------------------
20dec01, dnb written

*/

/* 

DESCRIPTION
This file contains definitions used only in msmcan_pc104.c
and msmcan_pc104_cfg.c 

*/
#ifndef MSMCAN_H
#define MSMCAN_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define MSMCAN_PC104_MAX_CONTROLLERS (1)

struct MSMCAN_PC104_DeviceEntry
{
    ULONG  ioAddress;
    UINT   irq;
    BOOL   inUse;

    struct WNCAN_Device     canDevice;
    struct WNCAN_Controller canController;
    struct WNCAN_Board      canBoard;
    BOOL                    allocated;

    WNCAN_ChannelMode i82527chnMode[I82527_MAX_MSG_OBJ]; 
	struct i82527ChipSpecific  can82527;
};

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* MSMCAN_H */
