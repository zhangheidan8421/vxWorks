/* mpc5200Can.h - definitions for MPC5200 based CAN controller */

/* Copyright 1984-2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01b,25jun04,bjn  Adding extern "C" for CPP.
01a,23oct03,bjn  written
*/

/*
DESCRIPTION
This file contains definitions used only in mpc5200Can.c
and mpc5200Can_cfg.c
*/

#ifndef _MPC5200_CAN_H_
#define _MPC5200_CAN_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "CAN/motMsCan.h"

/* external WindNet CAN required functions */

/*
 * Since all MSCAN devices are internal to the processor, only one
 * board can exist.
 */

#define MAX_NUM_MPC5200CAN_BOARDS (1)

#define BRD_NDX (0) /* Only one board possible */

/* We need an identifier for each individual MSCAN controllers */

typedef enum
    {
    MSCAN1_CTRL,
    MSCAN2_CTRL
    }
    MSCAN_CTRL_ID;

/*
 * Define a type to record MSCAN device instance.
 * Used to pass BSP configuration data to WNCAN
 */

typedef struct
    {
    MSCAN_CTRL_ID MSCANcontrollerId; /* MSCAN controller Identifier. */
    void *        pDevBaseAddr;      /* Base address of MSCAN device */
    VOIDFUNCPTR * pIntVec;           /* Interrupt vector */
    int           intNum;            /* Interrupt number */
    UINT32        clockSource;       /* 0=Oscillator Clock, 1=IP Bus Clock */
    UINT32        freq;              /* frequency */
    }
    mpc5200_mscan_instance;

/* Defined in mpc5200Can.c */
IMPORT STATUS mpc5200Can_init (mpc5200_mscan_instance * pMSCANboardCfg);
IMPORT STATUS mpc5200Can_establishLinks (struct WNCAN_Device *);
IMPORT struct WNCAN_Device * mpc5200Can_open (UINT, UINT);
IMPORT STATUS mpc5200Can_close (struct WNCAN_Device *);

/* Defined in mpc5200_cfg.c */
IMPORT BOOL mpc5200_cfgGet_inUse (UINT brdNdx);
IMPORT void mpc5200_cfgSet_inUse (UINT brdNdx, BOOL val);
IMPORT BOOL mpc5200_cfgGet_allocated (UINT brdNdx, UINT ctrlNdx);
IMPORT void mpc5200_cfgSet_allocated (UINT brdNdx, UINT ctrlNdx, BOOL val);
IMPORT struct WNCAN_Board * mpc5200_cfgGet_canBoardPtr (UINT brdNdx);
IMPORT struct WNCAN_Device * mpc5200_cfgGet_canDevicePtr (UINT brdNdx, UINT ctrlNdx);
IMPORT struct WNCAN_Controller * mpc5200_cfgGet_canControllerPtr (UINT brdNdx, UINT ctrlNdx);
IMPORT MSCAN_DEV * mpc5200_cfgGet_mscanDevPtr (UINT brdNdx, UINT ctrlNdx);
IMPORT WNCAN_ChannelMode * mpc5200_cfgGet_chModePtr (UINT brdNdx, UINT ctrlNdx);
IMPORT UINT32 mpc5200_cfgGet_NumControllers (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _MPC5200_CAN_H_ */
