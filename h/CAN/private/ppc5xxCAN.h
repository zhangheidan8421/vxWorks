/* ppc5xxCAN.h - definitions for PPC5xx baords */

/* Copyright 2001 Wind River Systems, Inc. */

/* 
modification history
--------------------
27dec01, lsg 

*/

/* 

DESCRIPTION
This file contains the declarations and definitions that are used for
PPC5xx boards

*/

#ifndef _ppc5xxCAN_H_
#define _ppc5xxCAN_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*Define the number of controllers on-board (or on-chip)*/
#define PPC5XXCAN_MAX_CONTROLLERS (1)

struct ppc5xxCAN_DeviceEntry
{
    BOOL                    inUse;
    struct WNCAN_Device     canDevice;
    struct WNCAN_Controller canController;
    struct WNCAN_Board      canBoard;
    struct canAccess        canRegAccess;
    BOOL                    allocated;
    WNCAN_ChannelMode       TouCANchnMode[TOUCAN_MAX_MSG_OBJ];
      
}; 

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
