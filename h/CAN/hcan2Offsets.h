/* hcan2Offsets.h  */

/* Copyright 2002 Wind River Systems, Inc. */

/*
Description:  Supply offsets and macros for use with te register
              access APIs.
 

modification history
--------------------
12nov02,rgp written
*/


#ifndef HCAN2_OFFSETS_H_
#define HCAN2_OFFSETS_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/********************************/
/* controller registers offsets */
/********************************/
#define HCAN2_MCR_OFFSET                  0x00
#define HCAN2_GSR_OFFSET                  0x02
#define HCAN2_BCR1_OFFSET                 0x04
#define HCAN2_BCR0_OFFSET                 0x06
#define HCAN2_IRR_OFFSET                  0x08
#define HCAN2_IMM_OFFSET                  0x0A
#define HCAN2_ERRCTNRS_OFFSET             0x0C
#define HCAN2_TXPR1_OFFSET                0x20
#define HCAN2_TXPR0_OFFSET                0x22
#define HCAN2_TXCR1_OFFSET                0x28
#define HCAN2_TXCR0_OFFSET                0x2A
#define HCAN2_TXACK1_OFFSET               0x30
#define HCAN2_TXACK0_OFFSET               0x32
#define HCAN2_ABACK1_OFFSET               0x38
#define HCAN2_ABACK0_OFFSET               0x3A
#define HCAN2_RXPR1_OFFSET                0x40
#define HCAN2_RXPR0_OFFSET                0x42
#define HCAN2_RFPR1_OFFSET                0x48
#define HCAN2_RFPR0_OFFSET                0x4A
#define HCAN2_MBIMR1_OFFSET               0x50
#define HCAN2_MBIMR0_OFFSET               0x52
#define HCAN2_UMSR1_OFFSET                0x58
#define HCAN2_UMSR0_OFFSET                0x5A
#define HCAN2_TCNTR_OFFSET                0x80
#define HCAN2_TCR_OFFSET                  0x82
#define HCAN2_TSR_OFFSET                  0x84
#define HCAN2_TDCR_OFFSET                 0x86
#define HCAN2_LOSR_OFFSET                 0x88
#define HCAN2_ICR0CC_OFFSET               0x8A
#define HCAN2_ICR0TM_OFFSET	          0x8C
#define HCAN2_ICR1_OFFSET                 0x8E
#define HCAN2_TCMR0_OFFSET                0x90
#define HCAN2_TCMR1_OFFSET                0x92
#define HCAN2_TCMR2_OFFSET                0x94
#define HCAN2_CCR_OFFSET                  0x96
#define HCAN2_CMAX_OFFSET                 0x98
#define HCAN2_TMR_OFFSET                  0x9A

#define HCAN2_ENDOFREGISTERS              0x9C


/************************/
/* mailbox offsets      */
/************************/
#define HCAN2_MB0_OFFSET                  0x100
#define HCAN2_MB1_OFFSET                  0x120
#define HCAN2_MB2_OFFSET                  0x140
#define HCAN2_MB3_OFFSET                  0x160
#define HCAN2_MB4_OFFSET                  0x180
#define HCAN2_MB5_OFFSET                  0x1A0
#define HCAN2_MB6_OFFSET                  0x1C0
#define HCAN2_MB7_OFFSET                  0x1E0
#define HCAN2_MB8_OFFSET                  0x200
#define HCAN2_MB9_OFFSET                  0x220
#define HCAN2_MB10_OFFSET                 0x240
#define HCAN2_MB11_OFFSET                 0x260
#define HCAN2_MB12_OFFSET                 0x280
#define HCAN2_MB13_OFFSET                 0x2A0
#define HCAN2_MB14_OFFSET                 0x2C0
#define HCAN2_MB15_OFFSET                 0x2E0
#define HCAN2_MB16_OFFSET                 0x300
#define HCAN2_MB17_OFFSET                 0x320
#define HCAN2_MB18_OFFSET                 0x340
#define HCAN2_MB19_OFFSET                 0x360
#define HCAN2_MB20_OFFSET                 0x380
#define HCAN2_MB21_OFFSET                 0x3A0
#define HCAN2_MB22_OFFSET                 0x3C0
#define HCAN2_MB23_OFFSET                 0x3E0
#define HCAN2_MB24_OFFSET                 0x400
#define HCAN2_MB25_OFFSET                 0x420
#define HCAN2_MB26_OFFSET                 0x440
#define HCAN2_MB27_OFFSET                 0x460
#define HCAN2_MB28_OFFSET                 0x480
#define HCAN2_MB29_OFFSET                 0x4A0
#define HCAN2_MB30_OFFSET                 0x4C0
#define HCAN2_MB31_OFFSET                 0x4E0

#define HCAN2_ENDOFMAILBOXES              0x500

/*************************/
/* mailbox field offsets */
/*************************/
#define HCAN2_MBCTRL0_OFFSET              0x00
#define HCAN2_MBCTRL1_OFFSET              0x02
#define HCAN2_MBCTRL2_OFFSET              0x04
#define HCAN2_MBTIMESTAMP_OFFSET          0x06
#define HCAN2_MBDATA01_OFFSET             0x08
#define HCAN2_MBDATA23_OFFSET             0x0A
#define HCAN2_MBDATA45_OFFSET             0x0C
#define HCAN2_MBDATA67_OFFSET             0x0E
#define HCAN2_MBFILTER0_OFFSET            0x10
#define HCAN2_MBFILTER1_OFFSET            0x12

#define HCAN2_ENDOFMAILBOXFIELDS          0x14


#ifdef __cplusplus
}
#endif /* __cplusplus */
   
#endif
