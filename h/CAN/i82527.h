/* i82527.h - definitions for Intel 82527 CAN Controller */

/* Copyright 2001 Wind River Systems, Inc. */

/* 
modification history 
--------------------
09nov01,dnb modified for integration into Tornado
12jul01,jac written

*/

/* 

DESCRIPTION
This file contains the functions declarations, specific to the 
Intel 82527 CAN controller, that implement the interface defined 
in the wnCAN.h header file. 

 */

#ifndef _I82527_H_
#define _I82527_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* max number of message objects for i82527 */
#define I82527_MAX_MSG_OBJ 15

/* bit constants */
#define I82527_B_INIT    0x01 /* is 1 after reset or busoff, must be set to 0 1st time */
#define I82527_B_IE      0x02 /* interrupt enable, must be 1 for using msg obj int's */
#define I82527_B_SIE     0x04 /* status interrupt enable, can be 0 */
#define I82527_B_EIE     0x08 /* Error interrupt enable, can be 0 */
#define I82527_B_CCE     0x40 /* config change enable, must be 0 for accessing cfg reg's*/

/* status register */
#define I82527_B_LEC0    0x01
#define I82527_B_LEC1    0x02
#define I82527_B_LEC2    0x04
#define I82527_B_TXOK    0x08
#define I82527_B_RXOK    0x10 
#define I82527_B_WAKE    0x20
#define I82527_B_WARN    0x40 /* read-only, must be 0 for bus transfer */
#define I82527_B_BOFF    0x80 /* read-only, must be 0 for bus transfer */

/* CPU interface register */
#define I82527_B_CEN     0x01
#define I82527_B_MUX     0x04
#define I82527_B_SLEEP   0x08
#define I82527_B_PWD     0x10
#define I82527_B_DMC     0x20
#define I82527_B_DSC     0x40
#define I82527_B_RSTST   0x80

struct i82527ChipSpecific
{
    BOOL busOffInt;
	BOOL wakeUpInt;
	BOOL errorInt;
};


extern const UINT g_i82527chnType[I82527_MAX_MSG_OBJ];

void i82527_registration(void);
void i82527ISR(ULONG context);


#ifdef __cplusplus
}
#endif /* __cplusplus */
    
#endif /* _I82527_H_ */
