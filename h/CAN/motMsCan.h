/* motMsCan.h - definitions for the Motorola MSCAN */

/* Copyright 1984-2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/* 
modification history
--------------------
01c,25jun04,bjn  Adding extern "C" for CPP.
01b,21apr04,bjn  support DevIO interface
01a,23oct03,bjn  written
*/

/*
DESCRIPTION
This file contains the functions declarations, specific to the
new CAN controller, that implement the interface defined
in the wnCAN.h header file. 
*/

#ifndef _MOTMSCAN_H_
#define _MOTMSCAN_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* 
 * Max number of message objects for Motorola MSCAN
 * TX = 0,1 and 2. RX = 3.
 */

#define MOTMSCAN_MAX_TX_MSG_OBJ (3)
#define MOTMSCAN_MAX_RX_MSG_OBJ (1)

#define MOTMSCAN_RX_MSG_OBJ_NUM (3)

#define MOTMSCAN_MAX_MSG_OBJ (MOTMSCAN_MAX_TX_MSG_OBJ + MOTMSCAN_MAX_RX_MSG_OBJ)

typedef struct _mscan_dev_
    {
    /* Configuration options, set before MOTMSCAN_Init() is called */

    void *      pMSCANregs;

    UCHAR       CANCTL0value;
    UCHAR       CANCTL1value;
    UCHAR       CANIDACvalue;
    UCHAR       CANRIERvalue;
    int         intNum;
    UINT        freq;

    /* Global data for the MSCAN device driver */

    BOOL        rtr[MOTMSCAN_MAX_TX_MSG_OBJ];

    UCHAR       can_RIERshadow;
    UCHAR       tempDisableRXFIE;

    WNCAN_IntType txChanInt[MOTMSCAN_MAX_TX_MSG_OBJ];
    }
    MSCAN_DEV;


/* Data structures for the controller */

struct MSCAN_ChannelData
    {
    WNCAN_ChannelMode chnMode[MOTMSCAN_MAX_MSG_OBJ];
    };


/* external globals for the controller */

extern const UINT g_MSCANchnType[MOTMSCAN_MAX_MSG_OBJ];


/* external functions for the  controller */

void MSCAN_registration (void);
void MSCAN_ISR (ULONG context);

/*****************************************************************************
 * Register: CANCTL0
 */

#define CR0_RXFRM  (0x80)
#define CR0_RXACT  (0x40)
#define CR0_CSWAI  (0x20)
#define CR0_SYNCH  (0x10)
#define CR0_TIME   (0x08)
#define CR0_WUPE   (0x04)
#define CR0_SLPRQ  (0x02)
#define CR0_INITRQ (0x01)

/*****************************************************************************
* Register: CANCTL1
*/

#define CR1_CANE    (0x80)
#define CR1_CLKSRC  (0x40)
#define CR1_LOOPB   (0x20)
#define CR1_LISTEN  (0x10)
#define CR1_WUPM    (0x04)
#define CR1_SLPAK   (0x02)
#define CR1_INITAK  (0x01)

/*****************************************************************************
* Register: CANRFLG/CANRIER
*/

#define RX_WUPIE   (0x80)
#define RX_CSCIE   (0x40)
#define RX_RSTATE1 (0x20)
#define RX_RSTATE0 (0x10)
#define RX_TSTATE1 (0x08)
#define RX_TSTATE0 (0x04)
#define RX_OVRIE   (0x02)
#define RX_RXFIE   (0x01)

#define RX_RX_STATUS_CHANGE_0_ENB (0x00<<4)
#define RX_RX_STATUS_CHANGE_1_ENB (0x01<<4)
#define RX_RX_STATUS_CHANGE_2_ENB (0x02<<4)
#define RX_RX_STATUS_CHANGE_3_ENB (0x03<<4)

#define RX_TX_STATUS_CHANGE_0_ENB (0x00<<2)
#define RX_TX_STATUS_CHANGE_1_ENB (0x01<<2)
#define RX_TX_STATUS_CHANGE_2_ENB (0x02<<2)
#define RX_TX_STATUS_CHANGE_3_ENB (0x03<<2)

#define RSTATE_BITS (0x30)
#define TSTATE_BITS (0x0C)

/*****************************************************************************
* Register: CANBTR0
*/

#define BTR0_BRP_BITS (0x3f)

#define BTR0_SJW_BS   (6)

/*****************************************************************************
* Register: CANBTR1
*/

#define BTR1_TSEG1_BITS    (0x0f)
#define BTR1_TSEG2_BITS    (0x70)

#define BTR1_TSEG2_BS (4)
#define BTR1_SAMP_BS  (7)

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _MOTMSCAN_H_ */
