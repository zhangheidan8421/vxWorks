/*
 i82527Offsets.h - register offset definitions for 
                   Intel 82527 CAN Controller 

 Copyright 2001 Wind River Systems, Inc. 
 
*/

/* 
modification history 
--------------------
29may02,lsg modified for CAN driver update

*/

/* 

DESCRIPTION
This file contains the functions i82527 register offsets

*/

#ifndef _I82527_OFFSETS_H_
#define _I82527_OFFSETS_H_
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define I82527_R_CTRL 0
#define I82527_R_SR   1
#define I82527_R_INFR 2
#define I82527_R_HSR  4
#define I82527_R_GMS  6
#define I82527_R_GME  8
#define I82527_R_M15M 0x0C
#define I82527_R_CLKO 0x1F
#define I82527_R_BCR  0x2F
#define I82527_R_BTR0 0x3F
#define I82527_R_BTR1 0x4F
#define I82527_R_INT  0x5F
#define I82527_R_P1C  0x9F
#define I82527_R_P2C  0xAF
#define I82527_R_P1I  0xBF
#define I82527_R_P2I  0xCF
#define I82527_R_P1O  0xDF
#define I82527_R_P2O  0xEF
#define I82527_R_SRA  0xFF

#define I82527_MAX_OFFSET 0x100

#define I82527_R_RCV 0xF0
#define I82527_R_XMT 0x10

/* Register Offsets */
#define I82527_OFFS_MSG   0x10  /* message address offset */
#define I82527_OFFS_DATA  0x07  /* message offset for data bytes */
#define I82527_OFFS_CTRL0 0x00  /* first message control register */
#define I82527_OFFS_CTRL1 0x01  /* second message control register */
#define I82527_OFFS_ARBIT 0x02  /* message arbitration register for setting the id*/
#define I82527_OFFS_MCR   0x06  /* message configuration register */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /*_I82527_OFFSETS_H_*/
