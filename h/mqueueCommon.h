/* mqueueCommon.h - POSIX usr/kernel shared message queue header file */

/*
 * Copyright (c) 2003, 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. 
 */

/*
modification history
--------------------
01c,22aug05,kk   moved RTP or kernel specific prototypes to mqueue.h
01b,07jul05,kk   move MQ_PRIO_MAX into kernel, user level has it in limits.h
01a,14nov03,m_s  code-inspection changes
01a,31oct03,m_s  Written
*/

#ifndef __INCmqueueCommonh
#define __INCmqueueCommonh


#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include <fcntl.h>
#include <sigevent.h>

/* defines */

#define MQ_NUM_MSGS_DEFAULT	16	/* default messages per queue */
#define MQ_MSG_SIZE_DEFAULT	16	/* default message size in bytes */

/* typedefs */

/* message queue attributes */

struct mq_attr
    {
    size_t	mq_maxmsg;
    size_t	mq_msgsize;
    unsigned	mq_flags;
    size_t	mq_curmsgs;
    };


/* message queue descriptor for applications */
struct mq_des;
typedef struct mq_des * mqd_t;

/* function declarations */

/* mq_receive() and mq_send() are in mqueue.h */

extern mqd_t mq_open    (const char *, int, ...);
extern int   mq_close   (mqd_t);
extern int   mq_unlink  (const char *);
extern int   mq_notify  (mqd_t, const struct sigevent *);
extern int   mq_setattr (mqd_t, const struct mq_attr *, struct mq_attr *);
extern int   mq_getattr (mqd_t, struct mq_attr *);

#ifdef __cplusplus
}
#endif

#endif	/* INCmqueueCommonh */
