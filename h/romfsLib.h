/* romfsLib.h - ROMFS file system driver */

/* Copyright 2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01a,05may03,md   written
*/

#ifndef romfsLib_h
#define romfsLib_h

#ifdef __cplusplus
extern "C" {
#endif

#include <vxWorks.h>
#include <sys/types.h>
#include <iosLib.h>
#include <blkIo.h>
#include <semLib.h>
#include <romfsDrv.h>

				/* block size of underlying device */
#define	DEV_BSIZE	ROMFS_BLOCK_SIZE

				/* magic number to identify ROMFS_VOL_DESC */
#define	ROMFS_MAGIC	0xDEADF00D

typedef struct romfsVolDescr	/* volume descriptor, one per mounted volume */
    {
    DEV_HDR   devHdr;		  /* for adding to device table		    */
    BLK_DEV  *pBlkDev;		  /* pointer to block device structure	    */
    int	      magic;		  /* magic number for identifying structure */
    u_char    cache[DEV_BSIZE];	  /* buffer cache for one block		    */
    int	      cBlockNum;	  /* cached block number		    */
    SEM_ID    devSem;		  /* device mutual-exclusion semaphore	    */
} ROMFS_VOL_DESC;

typedef ROMFS_VOL_DESC *	ROMFS_VOL_DESC_ID;

/* user-callable functions prototypes */

#if defined(__STDC__) || defined(__cplusplus)
extern ROMFS_VOL_DESC_ID romfsFsDevCreate( char * devName, BLK_DEV * pBlkDev );
#else	/* __STDC__ */
extern ROMFS_VOL_DESC_ID romfsFsDevCreate ();
#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* romfsLib_h */
