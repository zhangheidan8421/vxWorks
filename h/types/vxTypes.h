/* vxTypes.h - POSIX compliant (_t) types header file */

/*
 * Copyright (c) 1991-1992, 2001, 2003-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. No license to Wind River intellectual property rights
 * is granted herein. All rights not licensed by Wind River are reserved
 * by Wind River.
*/

/*
modification history
--------------------
01p,08sep05,pad  Added protection around ssize_t type definition (SPR 112064).
01o,24aug05,mcm  Adding the definition of the protection macros to help
                 prevent redefinition for ssize_t, time_t and fpos_t.
01n,23aug05,mcm  Adding protection for type definitions like uid_t, off_t etc.
01m,31mar05,kk   merged user and kernel version
01l,28sep04,gls  changed off_t to long long
01k,04dec03,mcm  Protecting the definition of fpos_t, time_t
01j,29aug03,cjj  written based on kernel version 01d of vxTypes.h 
01i,25jan05,vvv  added new types to support the dual-stack
01h,28sep04,gls  added off_t64
01g,05dec01,mem  Added support for 64-bit types.
01f,13nov92,dnw  added include of vxANSI.h
		 pruned some non-POSIX stuff.
01e,22sep92,rrr  added support for c++
01d,08sep92,smb  made some MIPS specific additions.
01c,07sep92,smb  added __STDC__ conditional and some documentation.
01b,29jul92,smb  added fpos_t type for stdio.h
01a,03jul92,smb  written.
*/

/*
DESCRIPTION
This file actually typedef's the system types defined in vxArch.h or
vxTypesBase.h.  

Architecture specific values for the standard types are defined in vxArch.h
as follows:

	#ifndef _TYPE_sometype_t
	#define _TYPE_sometype_t 	typedef unsigned int sometype_t
	#endif

Defaults for each type are provided in vxTypesBase.h as follows:

	#ifndef _TYPE_sometype_t
	#define _TYPE_sometype_t 	typedef int sometype_t
	#endif

When vxTypesBase.h is included following the include of vxArch.h,
_TYPE_sometype_t will already be defined and so will not be redefined
in vxTypesBase.h.

Alternatively, if it not defined in vxArch.h, it will be defined in
vxTypesBase.h.  So after vxArch.h and vxTypesBase.h are included all ANSI
and POSIX types will be defined but they will not have been typedef'ed yet.

The typedef happens in this file, vxTypes.h

	#ifdef _TYPE_sometype_t
	_TYPE_sometype_t
	#undef _TYPE_sometype_t
	#endif

The '#undef _TYPE_sometype_t' is necessary because a type may be defined in
different header files. For example, ANSI says that size_t must be defined
in time.h and stddef.h

*/

#ifndef __INCvxTypesh
#define __INCvxTypesh

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _WRS_KERNEL
#include "types/vxANSI.h"
#endif /* _WRS_KERNEL */

#ifdef _TYPE_ssize_t
#ifndef _SSIZE_T
#define _SSIZE_T
_TYPE_ssize_t;
#undef _TYPE_ssize_t
#endif
#endif

#if !defined(__RTP__)
#ifdef _TYPE_time_t
#define _TIME_T
_TYPE_time_t;
#undef _TYPE_time_t
#endif
#endif /* __RTP__ */

#if !defined(__RTP__)
#ifdef _TYPE_fpos_t
#define _FPOS_T
_TYPE_fpos_t;
#undef _TYPE_fpos_t
#endif
#endif /* __RTP__ */

#ifdef _TYPE_int8_t
_TYPE_int8_t;
#undef _TYPE_int8_t
#endif

#ifdef _TYPE_uint8_t
_TYPE_uint8_t;
#undef _TYPE_uint8_t
#endif

#ifdef _TYPE_int16_t
_TYPE_int16_t;
#undef _TYPE_int16_t
#endif

#ifdef _TYPE_uint16_t
_TYPE_uint16_t;
#undef _TYPE_uint16_t
#endif

#ifdef _TYPE_int32_t
_TYPE_int32_t;
#undef _TYPE_int32_t
#endif

#ifdef _TYPE_uint32_t
_TYPE_uint32_t;
#undef _TYPE_uint32_t
#endif

#ifdef _TYPE_int64_t
_TYPE_int64_t;
#undef _TYPE_int64_t
#endif

#ifdef _TYPE_uint64_t
_TYPE_uint64_t;
#undef _TYPE_uint64_t
#endif

/* The following types have been added to support the dual-stack */

#ifdef _TYPE_u_int8_t
_TYPE_u_int8_t;
#undef _TYPE_u_int8_t
#endif

#ifdef _TYPE_u_int16_t
_TYPE_u_int16_t;
#undef _TYPE_u_int16_t
#endif

#ifdef _TYPE_u_int32_t
_TYPE_u_int32_t;
#undef _TYPE_u_int32_t
#endif

#ifdef _TYPE_u_int64_t
_TYPE_u_int64_t;
#undef _TYPE_u_int64_t
#endif

#ifdef _TYPE_u_quad_t
_TYPE_u_quad_t;
#undef _TYPE_u_quad_t
#endif

#ifdef _TYPE_quad_t
_TYPE_quad_t;
#undef _TYPE_quad_t
#endif


/* old Berkeley definitions */

typedef unsigned char	uchar_t;
typedef unsigned short	ushort_t;
typedef unsigned int	uint_t;
typedef unsigned long	ulong_t;

typedef	struct	_quad { long val[2]; } quad;
typedef	long	daddr_t;
typedef	char *	caddr_t;
typedef	char *	addr_t;
typedef	long	swblk_t;

/* POSIX required */

#ifndef _DEV_T
#define _DEV_T
typedef short		dev_t;
#endif /* _DEV_T */

#ifndef _GID_T
#define _GID_T
typedef unsigned short	gid_t;
#endif /* _GID_T */

#ifndef _INO_T
#define _INO_T
typedef	unsigned long	ino_t;
#endif /* _INO_T */

#ifndef _MODE_T
#define _MODE_T
typedef int		mode_t;
#endif /* _MODE_T */

#ifndef _NLINK_T
#define _NLINK_T
typedef unsigned long	nlink_t;
#endif /* _NLINK_T */

#ifndef _OFF_T
#define _OFF_T
#ifdef __RTP__
typedef long long	off_t;
#else
typedef long		off_t;
#ifndef _OFF_T64
#define _OFF_T64
typedef long long	off_t64;
#endif /* _OFF_T64 */
#endif /* __RTP__ */
#endif /* _OFF_T */

#ifndef _PID_T
#define _PID_T
typedef int		pid_t;
#endif /* _PID_T */

#ifndef _UID_T
#define _UID_T
typedef unsigned short	uid_t;
#endif /* _UID_T */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxTypesh */
