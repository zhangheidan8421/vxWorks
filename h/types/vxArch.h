/* vxArch.h - architecture header */

/*
 * Copyright (c) 1984-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. 
 */

/*
modification history
--------------------
01z,07oct05,yvp  Defined VX_OBJ_ALIGN_SIZE (SPR 113405).
01y,07jun05,yvp  Updated copyright. #include now with angle-brackets.
01x,26apr05,yvp  Transferred previous 2 changes into vxLayerConfig.h
01w,16mar05,yvp  Undef'ed WV_INSTRUMENTATION and EDR_ERROR_INJECT_STUBS.
		 Defined MINIMAL_KERNAL and _FREE_VERSION for now.
01v,04apr05,scm  re-enable WV_INSTRUMENTATION for ARM/XScale...
01u,22mar05,scm  disable WV_INSTRUMENTATION until windview work complete for ARM...
01t,17dec04,scm  add WV_INSTRUMENTATION for arm/xscale...
01s,15nov04,h_k  enabled WV_INSTRUMENTATION for SH.
01r,04may04,tcr  add WV_INSTRUMENTATION to MIPS
01q,06apr04,dbt  Enabled WindView support for VxSim.
01p,13mar04,dlk  disable WV_INSTRUMENTATION if _FREE_VERSION defined.
01o,25mar04,dbt  Added SIMPENTIUM support.
01n,19nov04,job  Combined user & kernel mode versions. Moved kernel mode
		 STACK, ALLOC & CACHE_ALIGN_SIZE macros here so that they
		 are set correctly.
01m,16dec03,kk   re-enable WV_INSTRUMENTATION
01l,14dec03,kk   temporarily disable WV_INSTRUMENTATION until network builds
01k,11nov03,tcr  enable WindView on selected archs
01j,15jul03,kam  rebased to Base6 integration branch
01i,26jun03,dcc  temporarily comment out the definition of WV_INSTRUMENTATION
01h,17apr03,dbt  Added SIMLINUX support. Removed SIMHPPA & SIMSPARCSUNOS
01g,16nov01,dee  add COLDFIRE architecture
01f,14nov01,tcr  Undo previous change
01e,29oct01,tcr  add INCLUDE_WVNET for instrumented network stack
01d,25feb00,frf  Add SH support for T2
01c,06Aug98,ms   added WV_INSTRUMENTATION.
02b,15aug97,cym  added SIMNT support.
02b,28nov96,cdp  added ARM support.
02a,26may94,yao  added PPC support.
01n,30oct95,ism  added SIMSOLARIS support.
01m,19mar95,dvs  removed #ifdef TRON - tron no longer supported.
01l,02dec93,pme  added Am29K family support.
01k,11aug93,gae  vxsim hppa.
01j,12jun93,rrr  vxsim.
01i,09jun93,hdn  added support for I80X86
01h,22sep92,rrr  added support for c++
01g,07sep92,smb  added documentation
01f,07jul92,rrr  added defines for BIG & LITTLE ENDIAN and STACK direction
01e,07jul92,ajm  added _ARCH_MULTIPLE_CACHELIB for cache init
01d,03jul92,smb  changed name from arch.h.
01c,06jun92,ajm  corrected definition of mips CPU_FAMILY
01b,26may92,rrr  the tree shuffle
		  -changed includes to have absolute path from h/
01a,22apr92,yao	 written.
*/

/*
DESCRIPTION
This header file includes a architecture specific header file depending
on the value of CPU_FAMILY defined in vxCpu.h. The header file contains
definitions specific to this CPU family. This header file must be
preceded by an include of vxCpu.h
*/

#ifndef __INCvxArchh
#define __INCvxArchh

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _BIG_ENDIAN
#undef _BIG_ENDIAN
#endif
#define _BIG_ENDIAN		1234

#ifdef _LITTLE_ENDIAN
#undef _LITTLE_ENDIAN
#endif
#define _LITTLE_ENDIAN		4321

#define _STACK_GROWS_DOWN	(-1)
#define _STACK_GROWS_UP		1

#ifdef _WRS_KERNEL

#if (CPU_FAMILY==MC680X0)
#include <arch/mc68k/archMc68k.h>
#endif /* (CPU_FAMILY== MC680X0) */

#if (CPU_FAMILY==COLDFIRE)
#include <arch/coldfire/archColdfire.h>
#endif /* (CPU_FAMILY== COLDFIRE) */

#if (CPU_FAMILY==SPARC)
#include <arch/sparc/archSparc.h>
#endif /* (CPU_FAMILY== SPARC) */

#if (CPU_FAMILY==I960)
#include <arch/i960/archI960.h>
#endif /* (CPU_FAMILY== I960) */

#if (CPU_FAMILY==AM29XXX)
#include <arch/am29k/archAm29k.h>
#endif /* (CPU_FAMILY==AM29XXX) */

#else /* _WRS_KERNEL */

#if (CPU_FAMILY==SIMPENTIUM)
#include <arch/simpentium/archSimpentium.h>
#endif /* (CPU_FAMILY== SIMPENTIUM) */

#endif /* _WRS_KERNEL */

#if (CPU_FAMILY==SIMSPARCSOLARIS)
#ifdef _WRS_KERNEL
#ifndef WV_INSTRUMENTATION
#define	WV_INSTRUMENTATION
#endif
#endif /* _WRS_KERNEL */
#include <arch/simsolaris/archSimsolaris.h>
#endif /* (CPU_FAMILY== SIMSPARCSOLARIS) */

#if (CPU_FAMILY==SIMLINUX)
#ifdef _WRS_KERNEL
#ifndef WV_INSTRUMENTATION
#define	WV_INSTRUMENTATION
#endif
#endif /* _WRS_KERNEL */
#include <arch/simlinux/archSimlinux.h>
#endif /* (CPU_FAMILY== SIMLINUX) */

#if (CPU_FAMILY==SIMNT)
#ifdef _WRS_KERNEL
#ifndef WV_INSTRUMENTATION
#define	WV_INSTRUMENTATION
#endif
#endif /* _WRS_KERNEL */
#include <arch/simnt/archSimnt.h>
#endif /* (CPU_FAMILY== SIMNT) */

#if (CPU_FAMILY==MIPS)
#ifdef _WRS_KERNEL
#ifndef WV_INSTRUMENTATION
#define WV_INSTRUMENTATION
#endif
#endif /* _WRS_KERNEL */
#include <arch/mips/archMips.h>
#endif /* (CPU_FAMILY==MIPS) */

#if (CPU_FAMILY==PPC)
#ifdef _WRS_KERNEL
#ifndef WV_INSTRUMENTATION
#define	WV_INSTRUMENTATION
#endif
#endif /* _WRS_KERNEL */
#include <arch/ppc/archPpc.h>
#endif /* (CPU_FAMILY==PPC) */

#if (CPU_FAMILY==I80X86)
#ifdef _WRS_KERNEL
#ifndef WV_INSTRUMENTATION
#define	WV_INSTRUMENTATION
#endif
#endif /* _WRS_KERNEL */
#include <arch/i86/archI86.h>
#endif /* (CPU_FAMILY==I80X86) */

#if (CPU_FAMILY==SH)
#ifdef _WRS_KERNEL
#ifndef WV_INSTRUMENTATION
#define	WV_INSTRUMENTATION
#endif
#endif /* _WRS_KERNEL */
#include <arch/sh/archSh.h>
#endif /* (CPU_FAMILY==SH) */

#if (CPU_FAMILY==ARM)
#ifdef _WRS_KERNEL
#ifndef WV_INSTRUMENTATION
#define WV_INSTRUMENTATION
#endif
#endif /* _WRS_KERNEL */
#include <arch/arm/archArm.h>
#endif /* (CPU_FAMILY==ARM) */

#ifdef _FREE_VERSION
#undef WV_INSTRUMENTATION
#undef EDR_ERROR_INJECT_STUBS
#endif /* _FREE_VERSION */

#ifndef _BYTE_ORDER
#define	_BYTE_ORDER		_BIG_ENDIAN
#endif /* _BYTE_ORDER */

#ifndef	_STACK_DIR
#define	_STACK_DIR		_STACK_GROWS_DOWN
#endif	/* _STACK_DIR */

#ifndef	_ALLOC_ALIGN_SIZE
#define	_ALLOC_ALIGN_SIZE	4	/* 4 byte boundary */
#endif	/* _ALLOC_ALIGN_SIZE */

/* Note: for architectures not reguiring stack alignment, enforcing 4 byte
 * alignment is recommended for better efficiency.
 */

#ifndef	_STACK_ALIGN_SIZE
#define	_STACK_ALIGN_SIZE	4	/* 4 byte boundary */
#endif	/* _STACK_ALIGN_SIZE */

#ifndef _CACHE_ALIGN_SIZE
#define _CACHE_ALIGN_SIZE	16
#endif	/* _CACHE_ALIGN_SIZE */

#ifndef _ARCH_MULTIPLE_CACHELIB
#define _ARCH_MULTIPLE_CACHELIB	FALSE
#endif	/* _ARCH_MULTIPLE_CACHELIB */

/* alignment requirement when accessing bus */

#ifndef	_DYNAMIC_BUS_SIZING
#define _DYNAMIC_BUS_SIZING	TRUE	/* dynamic bus sizing */
#endif	/* _DYNAMIC_BUS_SIZING */


/* 
 * VX_OBJ_ALIGN_SIZE is the default allocation boundary for kernel 
 * objects allocated statically. If the CPU defines a default allocation 
 * alignment value, use it. Otherwise default to an 8-byte alignment 
 * boundary. This is because for some CPU's _ALLOC_ALIGN_SIZE may not be
 * known at compile time. 
 */

#ifdef	_CPU_ALLOC_ALIGN_SIZE
#define VX_OBJ_ALIGN_SIZE	_CPU_ALLOC_ALIGN_SIZE
#else	/* _CPU_ALLOC_ALIGN_SIZE */
#define VX_OBJ_ALIGN_SIZE	8
#endif	/* _CPU_ALLOC_ALIGN_SIZE */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxArchh */
