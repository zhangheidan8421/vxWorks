/* coreDumpLib.h - core dump interface header */

/*
 * Copyright (c) 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */
 
/*
modification history
--------------------
01k,09sep05,jmp  updated core dump types.
01j,02sep05,jmp  renamed coreDumpFormat() into coreDumpDevFormat().
		 added S_coreDumpLib_CORE_DUMP_FILTER_NOT_FOUND.
01i,22aug05,dbt  Added coreDumpOpen() & coreDumpClose() prototypes. Updated
                 coreDumpRead() prototype.
01h,11aug05,dbt  Added valid & errnoVal fields in CORE_DUMP_INFO structure.
		 Added device size in CORE_DUMP_RAW_DEV_DESC structure.
		 Added coreDumpDevShow() prototype.
01g,11aug05,dbt  Added coreDumpUsrGenerate() & coreDumpMemDump() prototypes;
                 removed coreDumpGenerate().
01f,05aug05,jmp  added CORE_DUMP_RAW_DEV, moved some definitions to 
		 private/coreDumpLibP.h.
01e,04aug05,jmp  added more errnos.
01d,03aug05,dbt  Added errnos & additional APIs.
01c,29jul05,jmp  added core dump memory filtering.
01b,18jul05,jmp  updated CORE_DUMP_INFO structure.
01a,29jun05,jmp  splited & cleanup.
01a,22mar05,jmp  written.
*/

#ifndef __INCcoreDumpLibh
#define __INCcoreDumpLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include <time.h>
#ifndef HOST
#include <taskLib.h>
#include <rtpLib.h>
#include <regs.h>
#include <excLib.h>
#include <vwModNum.h>
#endif	/* !HOST */

/* defines */

/* generic status codes */

#ifndef	HOST
#define S_coreDumpLib_CORE_DUMP_HOOK_TABLE_FULL		(M_coreDumpLib | 1)
#define S_coreDumpLib_CORE_DUMP_HOOK_NOT_FOUND		(M_coreDumpLib | 2)
#define S_coreDumpLib_CORE_DUMP_INVALID_DEVICE		(M_coreDumpLib | 3)
#define S_coreDumpLib_CORE_DUMP_DEVICE_READ_ERROR	(M_coreDumpLib | 4)
#define S_coreDumpLib_CORE_DUMP_DEVICE_WRITE_ERROR	(M_coreDumpLib | 5)
#define S_coreDumpLib_CORE_DUMP_DEVICE_ERASE_ERROR	(M_coreDumpLib | 6)
#define S_coreDumpLib_CORE_DUMP_DEVICE_OPEN_ERROR	(M_coreDumpLib | 7)
#define S_coreDumpLib_CORE_DUMP_DEVICE_CLOSE_ERROR	(M_coreDumpLib | 8)
#define S_coreDumpLib_CORE_DUMP_INVALID_ARGS		(M_coreDumpLib | 9)
#define S_coreDumpLib_CORE_DUMP_INVALID_CORE_DUMP	(M_coreDumpLib | 10)
#define S_coreDumpLib_CORE_DUMP_STORAGE_NOT_FORMATED	(M_coreDumpLib | 11)
#define S_coreDumpLib_CORE_DUMP_DEVICE_NOT_INITIALIZED	(M_coreDumpLib | 12)
#define S_coreDumpLib_CORE_DUMP_TOO_MANY_CORE_DUMP	(M_coreDumpLib | 13)
#define S_coreDumpLib_CORE_DUMP_COMPRESSION_ERROR	(M_coreDumpLib | 14)
#define S_coreDumpLib_CORE_DUMP_FILTER_TABLE_FULL	(M_coreDumpLib | 15)
#define S_coreDumpLib_CORE_DUMP_GENERATE_ALREADY_RUNNING (M_coreDumpLib | 16)
#define S_coreDumpLib_CORE_DUMP_PATH_TOO_LONG		(M_coreDumpLib | 17)
#define S_coreDumpLib_CORE_DUMP_GENERATE_NOT_RUNNING	(M_coreDumpLib | 18)
#define S_coreDumpLib_CORE_DUMP_DEVICE_TOO_SMALL	(M_coreDumpLib | 19)
#define S_coreDumpLib_CORE_DUMP_FILTER_NOT_FOUND	(M_coreDumpLib | 20)
#endif	/* !HOST */

/* core dump info macros */

#define MAX_CORE_DUMP_LEN	20		/* max len for core dump name */
#define MAX_NAME_LENGTH		256		/* max length for RTP or task */

/* typedefs */

/* core dump types */

typedef enum		/* CORE_DUMP_TYPE */
    {
    CORE_DUMP_USER,		/* 0: user coredump (on-demand) */
    CORE_DUMP_KERNEL_INIT,	/* 1: fatal error during kernel intialization */
    CORE_DUMP_INTERRUPT,	/* 2: error in a VxWorks interrupt handler */
    CORE_DUMP_KERNEL_PANIC,	/* 3: kernel panic */
    CORE_DUMP_KERNEL_TASK,	/* 4: kernel task error */
    CORE_DUMP_RTP,		/* 5: process coredump */
    CORE_DUMP_KERNEL		/* 6: VxWorks kernel error */
    } CORE_DUMP_TYPE;

typedef struct core_dump_desc *	CORE_DUMP_ID;	/* core dump descriptor */

typedef struct core_dump_info		/* core dump information */
    {
    UINT32	coreDumpIndex;		/* core dump index */
    BOOL	valid;			/* core dump is valid? */
    UINT32	errnoVal;		/* core dump errno */
    char	name[MAX_CORE_DUMP_LEN];/* name of the core dump */
    size_t	size;			/* size of the core dump */
    CORE_DUMP_TYPE	type;			/* origin of the core dump */
    int		taskId;			/* task Id (for kernel core dump) */
    char	taskName [MAX_NAME_LENGTH]; /* name of task */
    UINT32	rtpId;			/* process Id (for process core dump) */
    char	rtpName [MAX_NAME_LENGTH]; /* path to RTP */
    int		excNum;			/* exception number (Not valid for */
					/* on-demand  & workQPanic) */
    UINT32	pc;			/* program counter */
    UINT32	sp;			/* stack pointer */
    UINT32	fp;			/* frame pointer */
    time_t	time;			/* core dump generation calendar time */
    UINT32	ticks;			/* VxWorks time stamp */
    } CORE_DUMP_INFO;

typedef struct core_dump_raw_dev_desc	/* coredump raw device descriptor */
    {
    UINT32	devWrtBlkSize;		/* device write block size */
    void *	deviceId;		/* device ID used for various rtns */
    size_t	size;			/* device size */
    char *	pDevWrtBlkCache;	/* device write cache buffer */
    FUNCPTR	pDevEraseRtn;		/* device erase() routine */
    FUNCPTR	pDevWriteRtn;		/* device write() routine */
    FUNCPTR	pDevReadRtn;		/* device read() routine */
    FUNCPTR	pDevFlushRtn;		/* device flush() routine */
    } CORE_DUMP_RAW_DEV_DESC;

/* externals */

#ifndef HOST
extern STATUS	coreDumpDevFormat	(UINT32 coreDumpMax);
extern BOOL	coreDumpIsAvailable	(void);
extern STATUS	coreDumpNextGet		(UINT32 currentIx,
					 UINT32 * pNextIx);
extern CORE_DUMP_INFO * coreDumpInfoGet (UINT32 coreDumpIndex);
extern CORE_DUMP_ID	coreDumpOpen	(UINT32 coreDumpIx);
extern STATUS	coreDumpClose		(CORE_DUMP_ID coreDumpId);
extern int	coreDumpRead		(CORE_DUMP_ID coreDumpId,
					 void * buffer, size_t size);
extern STATUS   coreDumpShowInit        (void);
extern STATUS   coreDumpShow            (UINT32 coreDumpIndex, UINT32 level);
extern STATUS   coreDumpDevShow		(void);
extern STATUS	coreDumpMemFilterAdd	(void * addr, size_t size);
extern STATUS	coreDumpMemFilterDelete	(void * addr, size_t size);
extern STATUS	coreDumpCreateHookAdd	(FUNCPTR createHook);
extern STATUS	coreDumpCreateHookDelete(FUNCPTR createHook);
extern STATUS	coreDumpUsrGenerate	(void);
extern STATUS	coreDumpMemDump		(void * addr, size_t size,
					 void * destAddr);
#endif	/* !HOST */

#ifdef __cplusplus
}
#endif

#endif /* __INCcoreDumpLibh */
