/* xbdPartition.h - XBD partition manager */

/* 
* Copyright (c) 2005 Wind River Systems, Inc. 
* 
* The right to copy, distribute, modify or otherwise make use 
* of this software may be licensed only pursuant to the terms 
* of an applicable Wind River license agreement. 
*/

/*
modification history
--------------------
01b,15sep05,pcm  added xbdCreatePartition()
01a,xxXXX05,xxx  written
*/

#ifndef __INCxbdPartitionh
#define __INCxbdPartitionh


#include <drv/xbd/xbd.h>


extern device_t xbdPartitionDevCreate (device_t subDev);
extern STATUS   xbdCreatePartition    (char * pathName, int nPart,
                                       int size1, int size2, int size3);

#endif
