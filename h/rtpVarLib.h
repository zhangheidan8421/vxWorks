/* rtpVarLib.h - header for RTP variables */

/* Copyright 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,02mar04,kk  written
*/

#ifndef __INCrtpVarLibh
#define __INCrtpVarLibh

#ifdef __cplusplus
extern "C" {
#endif

#include "rtpLib.h"
#include "rtpVarLibCommon.h"

/* RTP variable structure */

typedef struct rtp_var
    {
    struct rtp_var *	next;	/* link the RTP vars */
    RTP_VAR_ADDR	addr;   /* address of variable to switch */
    RTP_VAR		value;	/* value of variable */
    } RTP_VAR_INFO;

extern STATUS 	rtpVarAdd (RTP_VAR_ADDR pVar, RTP_VAR value, RTP_ID rtpId);
extern STATUS   rtpVarDelete (RTP_VAR_ADDR pVar, RTP_ID rtpId);
extern STATUS 	rtpVarDeleteAll (RTP_ID rtpId);

#ifdef __cplusplus
}
#endif

#endif /* __INCrtpVarLibh */
