/* rtpVarLibCommon.h - header for user and kernel RTP variables */

/* Copyright 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,02mar04,kk  written
*/

#ifndef __INCrtpVarLibCommonh
#define __INCrtpVarLibCommonh

#ifdef __cplusplus
extern "C" {
#endif

#include "rtpLib.h"

/* typedefs */

typedef void * 		RTP_VAR;
typedef RTP_VAR * 	RTP_VAR_ADDR;

#ifdef __cplusplus
}
#endif

#endif /* __INCrtpVarLibCommonh */
