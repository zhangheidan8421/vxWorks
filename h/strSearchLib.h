/* strSearchLib.h - String search library header file */

/* 
 * Copyright (c) 2005 Wind River Systems, Inc. 
 * 
 * The right to copy, distribute, modify or otherwise make use 
 * of this software may be licensed only pursuant to the terms 
 * of an applicable Wind River license agreement. 
 */

/*
modification history
--------------------
01a,04apr05,svk  written
*/

#ifndef __INCstrSearchLibh
#define __INCstrSearchLibh

#ifdef __cplusplus
extern "C" {
#endif

/* function declarations */

extern char * fastStrSearch (char * pattern, int patternLen, 
                             char * buffer,  int bufferLen, 
                             BOOL caseSensitive);
extern char * bmsStrSearch  (char * pattern, int patternLen, 
                             char * buffer,  int bufferLen, 
                             BOOL caseSensitive);
extern char * bfStrSearch   (char * pattern, int patternLen, 
                             char * buffer,  int bufferLen, 
                             BOOL caseSensitive);

#ifdef __cplusplus
}
#endif

#endif /* __INCstrSearchLibh */

