/* signal.h - signal facility library header */

/*
 * Copyright (c) 1992-1994, 2000, 2003-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. 
 */

/*
modification history
--------------------
02w,22aug05,kk   updated prototype of sigtimedwait() and sigwaitinfo()
02v,19aug05,kk   updated prototype for RTP sigqueue() to conform to POSIX
02u,04aug05,kk   added sys/types.h to get pid_t for POSIX
02t,07feb05,hya  added missing prototype
02s,30sep04,ans  added _rtpSigqueue()
02r,27sep04,ans  updated job control signals documentation.
02q,13sep04,yvp  Added aliases for posix functions to get API uniformity across
                 both kernel/userland.
02p,19may04,ans  alternate signal stack support.
02o,04may04,ans  added SIG_NODEFER
02n,27oct03,nrj  added support for waitpid
02m,17oct03,nrj  added SA_RESTART
02l,14oct03,ans  Moved SIGEV_NONE SIGEV_SIGNAL to sigeventCommon.h
02k,09nov00,jgn  remove inaccurate comment from SIGKILL (SPR #35996)
02j,19jul00,jgn  add thread cancellation signal for pthreads support +
		 update the sigwait prototype to match the POSIX version
02i,11nov94,kdl  provide paramless func ptrs in structs, for non-ANSI (SPR 3742)
02h,10nov94,caf  adjusted placement of _ASMLANGUAGE conditional.
02g,06oct93,yao  added _ASMLANGUAGE conditional.
02h,12jan94,kdl  added sigqueue() prototype.
02g,09nov93,rrr  update to posix 1003.4 draft 14
02f,05feb93,rrr  fixed spr 1986 (SIG_ERR ... prototype) and
		 spr 1906 (signal numbers to match sun os)
02e,15oct92,rrr  silenced warnings
02d,22sep92,rrr  added support for c++
02c,22aug92,rrr  added bsd prototypes.
02b,27jul92,smb  added prototype for raise().
02a,04jul92,jcf  cleaned up.
01b,26may92,rrr  the tree shuffle
01a,19feb92,rrr  written from posix spec
*/

#ifndef __INCsignalh
#define __INCsignalh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef	_ASMLANGUAGE
#include <sigevent.h>		/* for sigevent structure */
#include <sys/types.h>		/* for pid_t for POSIX */

struct timespec;
#endif	/* _ASMLANGUAGE */

/*
 * Signal Numbers:
 * 	Required .1 signals	 1-13
 * 	Job Control signals	17-22
 *        These are no implemented for kernel tasks. The default action in this
 *        case is SIG_IGN.
 *        For RTP tasks, these are implemented as per the below signal number
 *        definition.
 *                              A bas
 * 	Realtime signals	20-27
 */
#define	SIGHUP	1	/* hangup */
#define	SIGINT	2	/* interrupt */
#define	SIGQUIT	3	/* quit */
#define	SIGILL	4	/* illegal instruction (not reset when caught) */
#define	SIGTRAP	5	/* trace trap (not reset when caught) */
#define	SIGABRT 6	/* used by abort, replace SIGIOT in the future */
#define	SIGEMT	7	/* EMT instruction */
#define	SIGFPE	8	/* floating point exception */
#define	SIGKILL	9	/* kill */
#define	SIGBUS	10	/* bus error */
#define	SIGSEGV	11	/* segmentation violation */
#define SIGFMT	12	/* STACK FORMAT ERROR (not posix) */
#define	SIGPIPE	13	/* write on a pipe with no one to read it */
#define	SIGALRM	14	/* alarm clock */
#define	SIGTERM	15	/* software termination signal from kill */
#define SIGCNCL 16	/* pthreads cancellation signal */
#define	SIGSTOP	17	/* sendable stop signal not from tty */
#define	SIGTSTP	18	/* stop signal from tty */
#define	SIGCONT	19	/* continue a stopped process */
#define	SIGCHLD	20	/* to parent on child stop or exit */
#define	SIGTTIN	21	/* to readers pgrp upon background tty read */
#define	SIGTTOU	22	/* like TTIN for output if (tp->t_local&LTOSTOP) */

#define	SIGUSR1 30	/* user defined signal 1 */
#define	SIGUSR2 31	/* user defined signal 2 */

#define SIGRTMIN	23	/* Realtime signal min */
#define SIGRTMAX	29	/* Realtime signal max */


#define _NSIGS		31

#ifndef _ASMLANGUAGE
/*
 * ANSI Args and returns from signal()
 */
#if defined(__STDC__) || defined(__cplusplus)

#define SIG_ERR         (void (*)(int))-1
#define SIG_DFL         (void (*)(int))0
#define SIG_IGN         (void (*)(int))1

#else

#define SIG_ERR         (void (*)())-1
#define SIG_DFL         (void (*)())0
#define SIG_IGN         (void (*)())1

#endif

/*
 * The sa_flags in struct sigaction
 */
#define SA_NOCLDSTOP	0x0001	/* Do not generate SIGCHLD when children stop */
#define SA_SIGINFO	0x0002	/* Pass additional siginfo structure */
#define SA_ONSTACK	0x0004	/* Run on sigstack */
#define SA_INTERRUPT	0x0008	/* (Not posix) Don't restart the function */
#define SA_RESETHAND	0x0010	/* (Not posix) Reset the handler, like sysV */
#define SA_RESTART	0x0020	/* (Not posix) restart the system call */
#define SA_NODEFER      0x0040  /* 
                                 * signal not automatically blocked on entry
                                 * to signal handler 
                                 */
#define SA_NOCLDWAIT    0x0080  /* no zombie created on child death */

/*
 * The how in sigprocmask()
 */
#define SIG_BLOCK	1
#define SIG_UNBLOCK	2
#define SIG_SETMASK	3

/*
 * The si_code returned in siginfo
 */
#define SI_SYNC		0	/* (Not posix) gernerated by hardware */
#define SI_KILL		1	/* signal from .1 kill() function */
#define SI_QUEUE	2	/* signal from .4 sigqueue() function */
#define SI_TIMER	3	/* signal from expiration of a .4 timer */
#define SI_ASYNCIO	4	/* signal from completion of an async I/O */
#define SI_MESGQ	5	/* signal from arrival of a message */
#define SI_CHILD	6	/* signal from child, stopped or terminated */
#define SI_USER		SI_KILL 	/* signal from kill() function */

typedef unsigned long sigset_t;
typedef unsigned char sig_atomic_t;

typedef struct siginfo
    {
    int			si_signo;
    int			si_code;
    union sigval	si_value;
    } siginfo_t;

struct sigaction
    {
    union
	{
#if defined(__STDC__) || defined(__cplusplus)
	void		(*__sa_handler)(int);
	void		(*__sa_sigaction)(int, siginfo_t *, void *);
#else
	void		(*__sa_handler)();
	void		(*__sa_sigaction)();
#endif /* __STDC__ */
	}		sa_u;
#define sa_handler	sa_u.__sa_handler
#define sa_sigaction	sa_u.__sa_sigaction
    sigset_t		sa_mask;
    int			sa_flags;
    };


/*
 * From here to the end is not posix, it is for bsd compatibility.
 */
#define SV_ONSTACK      SA_ONSTACK
#define SV_INTERRUPT    SA_INTERRUPT
#define SV_RESETHAND    SA_RESETHAND

#define sigmask(m)      (1 << ((m)-1))
#define SIGMASK(m)      (1 << ((m)-1))

struct sigvec
    {
#if defined(__STDC__) || defined(__cplusplus)
    void (*sv_handler)(int);    /* signal handler */
#else
    void (*sv_handler)();       /* signal handler */
#endif /* __STDC__ */
    int sv_mask;                /* signal mask to apply */
    int sv_flags;               /* see signal options */
    };

struct sigcontext;

/* end for bsd compatibilty */

typedef struct sig_stack
    {
    int   * ss_sp;
    long  ss_size;
    int   ss_flags;
    } stack_t;

struct rtpSigCtx;

/* Bit definitions for ss_flags */

#define SS_DISABLE 0x1
#define SS_ONSTACK 0x2

#define MINSIGSTKSZ 1024
#define SIGSTKSZ  MINSIGSTKSZ * 4

/* prototypes */

extern void 	(*signal(int __sig, void (*__handler)(int)))(int);
extern int      raise(int __signo);

extern int 	sigemptyset(sigset_t *__set);
extern int 	sigfillset(sigset_t *__set);
extern int 	sigaddset(sigset_t *__set, int __signo);
extern int 	sigdelset(sigset_t *__set, int __signo);
extern int 	sigismember(const sigset_t *__set, int __signo);
extern int 	sigaction(int __sig, const struct sigaction *__act,
			  struct sigaction *__oact);
extern int 	sigprocmask(int __how, const sigset_t *__set, sigset_t *__oset);
extern int 	sigpending(sigset_t *__set);
extern int 	sigsuspend(const sigset_t *__sigmask);
extern int 	sigwait(const sigset_t *__set, int* sig);
extern int 	sigvec(int __sig, const struct sigvec *__vec,
		       struct sigvec *__ovec);
extern void 	sigreturn(struct sigcontext *__context);
extern int 	sigsetmask(int __mask);
extern int 	sigblock(int __mask);
extern int      sigaltstack (const stack_t *ss, stack_t *oss);
extern int 	sigwaitinfo(const sigset_t *__set, siginfo_t *__value);
extern int 	sigtimedwait(const sigset_t *__set, siginfo_t *__value,
				const struct timespec *);

#ifndef _WRS_KERNEL		/* user-mode API's */

extern int	kill (pid_t rtpId, int signo);
extern int	taskKill (int taskId, int signo);
extern int	sigqueue (pid_t rtpId, int signo, const union sigval value);
extern int	taskSigqueue (int taskId, int signo, const union sigval value);
extern int	_taskSigqueue (int taskId, int signo, 
                               const union sigval *value, int sigCode);

/* thread APIs */

#include <pthread.h>	/* to get pthread_t */

int pthread_sigmask (int how, const sigset_t *set, sigset_t *oset);
int pthread_kill (pthread_t thread, int sig);

/* These aliases provide consistent meaning in both kernel & user modes */

#define rtpKill(rtpId,signo)		kill((rtpId),(signo))
#define rtpSigqueue(rtpId,signo,value)	sigqueue((rtpId),(signo),(value))
#define _rtpSigqueue(rtpId,signo,value,code)	\
                                       _sigqueue((rtpId),(signo),(value),(code))
#define taskRaise(signo)		taskKill(taskIdCurrent,(signo))
#define rtpRaise(signo)			raise((signo))

#else				/* kernel-mode API's */

extern int	kill(int __tid, int __signo);
extern int	sigqueue (int tid, int signo, const union sigval value);

    /* These aliases provide consistent meaning in both kernel & user modes */

#define taskKill(tid,signo)		kill((tid),(signo))
#define taskRaise(signo)		raise((signo))
#define taskSigqueue(tid,signo,value)	sigqueue((tid),(signo),(value))

#endif /* _WRS_KERNEL */

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCsignalh */
