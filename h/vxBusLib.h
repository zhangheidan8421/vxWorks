/* vxBusLib.h - vxBus definitions and API to OS/middleware */

/* Copyright (c) 2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,01sep05,mdo  Add vxb prefix
01a,06jul05,tor  Written
*/

#ifndef _INC_VXBUSLIB_H
#define _INC_VXBUSLIB_H

#ifdef __cplusplus
extern "C" {
#endif

/* types */

typedef struct vxbDev VXB_DEVICE;
typedef struct vxbDev * VXB_DEVICE_ID;

/*
 * Driver Method callback function for device instances.
 */

typedef STATUS (*VXB_ITERATE_FUNC)
    (
    VXB_DEVICE_ID   devID,  /* device ID */
    void *          pArg    /* user-supplied argument */
    );

/* defines */

/*
 * when OS/Middleware define or use driver methods, they
 * should use the following macros to reference and declare
 * the methodID.
 */

#define VXB_DRIVER_METHOD(NAME)      ((UINT32)&NAME##_desc[0])
#define VXB_DRIVER_METHOD_DECL(NAME) IMPORT char NAME##_desc[];

/* forward declarations */

/*
 * Run the specified driver method on all devices supporting
 * it, giving pArg as an argument to each VXB_ITERATE_FUNC  */

IMPORT STATUS vxbDeviceMethodRun
    (
    VXB_DEVICE_ID   devID,  /* device ID */
    void *          pArg
    );

/*
 * Fetch the entry point to a specified driver method
 * point for a specific device.
 */

IMPORT VXB_ITERATE_FUNC vxbDeviceMethodGet
    (
    VXB_DEVICE_ID   devID,  /* device ID */
    UINT32          method  /* method to find */
    );

/*
 * Iterate through devices and/or instances, and perform
 * the specified action on each.
 */

IMPORT STATUS vxbDeviceIterate
    (
    VXB_ITERATE_FUNC    func,   /* function to call */
    void *      pArg,   /* 2nd argument to func */
    UINT32      flags   /* flags to determine what to do */
    );

/*
 * Show vxBus state
 */

IMPORT int vxbShow(int verboseLevel);

#ifdef __cplusplus
}
#endif

#endif /* _INC_VXBUSLIB_H */
