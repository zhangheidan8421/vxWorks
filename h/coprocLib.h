/* coprocLib.h - coprocessor management library header */

/* Copyright 1984-2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01e,02oct04,yvp  Added prototypes for coprocCtxReplicate and coprocMRegs.
01d,16feb04,yvp  Prototype change for coprocTaskRegsShow().
01c,26nov03,yvp  Added coprocCtxStorageSizeGet() + cleanup.
01b,02may03,pes  Add prototype for coprocShowInit().
01a,21mar03,yvp	 written.
*/

#ifndef __INCcoprocLibh
#define __INCcoprocLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "taskLib.h"

/* defines */

#define COPROCS_MAX        8       /* Max. number of coprocessors per task */

/* Coprocessor option bits occupy the upper 8 bits of the task option word */

#define COPROC_FIELD_MASK  \
    (VX_COPROC1_TASK | VX_COPROC2_TASK | VX_COPROC3_TASK | VX_COPROC4_TASK | \
     VX_COPROC5_TASK | VX_COPROC6_TASK | VX_COPROC7_TASK | VX_COPROC8_TASK )

#define COPROCS_ALL	COPROC_FIELD_MASK

/* generic status codes */

#define S_coprocLib_ALLOC_FAILED                  (M_coprocLib | 1)
#define S_coprocLib_COPROC_ENABLE_FAILED          (M_coprocLib | 2)
#define S_coprocLib_COPROC_DISABLE_FAILED         (M_coprocLib | 3)
#define S_coprocLib_EXCHANDLER_REGISTER_FAILED    (M_coprocLib | 4)
#define S_coprocLib_HOOK_ATTACH_FAILED            (M_coprocLib | 5)
#define S_coprocLib_INVALID_ARGUMENT              (M_coprocLib | 6)
#define S_coprocLib_INVALID_OPERATION             (M_coprocLib | 7)
#define S_coprocLib_NO_COPROC_SUPPORT             (M_coprocLib | 8)

#ifndef _ASMLANGUAGE

/* function declarations */

STATUS coprocLibInit (void);
void   coprocShowInit (void);
STATUS coprocEnable (int);
STATUS coprocDisable (int);
void   coprocTaskCreateHook (WIND_TCB *);
void   coprocTaskDeleteHook (WIND_TCB *);
void   coprocTaskSwapHook (WIND_TCB *, WIND_TCB *);
STATUS coprocTaskRegsGet (int, int, void *);
STATUS coprocTaskRegsSet (int, int, void *);
int    coprocCtxStorageSizeGet (int);
int    coprocShowOnDebug (BOOL, int);
void   coprocSave (int);
void   coprocShow (void);
void   coprocTaskRegsShow (int, int);
void * coprocCtxReplicate (int, int);
STATUS coprocMRegs (int, char *, BOOL *);
void   coprocRegListShow (void);

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCcoprocLibh */
