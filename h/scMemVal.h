/* scMemVal.h - header file for system call validation routines */

/* Copyright 2003-2004 Wind River Systems, Inc. */
/*
modification history
--------------------
01a,04nov04,zl  created from private file.
*/

#ifndef __INCscMemValh
#define __INCscMemValh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"

/* defines */

#define SC_PROT_READ	0x1	/* read access in supervisor mode  */
#define SC_PROT_WRITE	0x2	/* write access in supervisor mode */

#ifndef  _ASMLANGUAGE

/* type definitions */

typedef unsigned int SC_PROT_ATTR;

/* function declarations */

IMPORT STATUS scMemValidate (const void * addr, UINT size, SC_PROT_ATTR prot);
IMPORT void   scMemValEnable (BOOL enable);

#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCscMemValh */
