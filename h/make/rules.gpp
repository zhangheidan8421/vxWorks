# rules.gpp - makefile target rules for gpp
#
# Copyright (c) 2001-2005 Wind River Systems, Inc.
#
# The right to copy, distribute, modify or otherwise make use
# of this software may be licensed only pursuant to the terms
# of an applicable Wind River license agreement.
# 
#
# modification history
# --------------------
# 01c,18aug05,kch  Redefined MSP networking features that are not supported
#                  in GPP products.
# 01b,15apr05,kch  Modified to use $FEATURE_SET environment variable.
# 01a,06apr05,kch  created.
#
#
# DESCRIPTION
# This file is included in defs.default for both the kernel and user builds 
# to establish rules that pertains to the gpp product requirements.
#

# For GPP build, the environment variable $FEATURE_SET is either set to "gpp"
# or simply not defined at all. Here it is assumed that the top-level Feature 
# Set Makefile (if exist) has verified the setting of the $FEATURE_SET 
# variable (if defined)

ifeq ($(FEATURE_SET),)
_GPP_BUILD = TRUE
else
ifeq ($(FEATURE_SET),gpp)
_GPP_BUILD = TRUE
endif
endif

# $(MSP_ONLY) defines features that are available for the MSP products
# only.
#
MSP_ONLY = -DVIRTUAL_STACK -DSCTP -DSUBNET_VLAN -DSOCKET_VLAN -DVLAN_TAG \
           -DMLDV2 -DMIP6 -DIGMPV3
             
ifneq ($(_GPP_BUILD),)

# First check the $(ADDED_CFLAGS)
ifneq ($ADDED_CFLAGS),)
GPP_UNSUPPORTED = $(filter $(MSP_ONLY), $(ADDED_CFLAGS))
ifneq ($(GPP_UNSUPPORTED),)
$(error Unsupported ADDED_CFLAGS="$(GPP_UNSUPPORTED)")
endif
endif

# now look at the $(EXTRA_DEFINE)
ifneq ($(EXTRA_DEFINE),)
GPP_UNSUPPORTED = $(filter $(MSP_ONLY), $(EXTRA_DEFINE))
ifneq ($(GPP_UNSUPPORTED),)
$(error Unsupported EXTRA_DEFINE="$(GPP_UNSUPPORTED)")
endif
endif

endif  # GPP build
