/* semaphoreCommon.h - POSIX usr/kernel shared semaphore header file */

/*
 * Copyright (c) 2003, 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. 
 */

/*
modification history
--------------------
01d,22aug05,kk   moved sem_init() to RTP and kernel specific semaphore.h files
01c,02aug05,kk   move SEM_NSEMS_MAX  and SEM_VALUE_MAX into kernel 
		 semaphore.h, user side has these in limits.h
01b,18nov03,m_s  code-inspection changes
01a,31oct03,m_s  Written
*/

#ifndef __INCsemaphoreCommonh
#define __INCsemaphoreCommonh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "fcntl.h"
#include "limits.h"

/* defines */

#define SEM_FAILED    ((sem_t *) ERROR)   /* failure for sem_open */

#undef _POSIX_NO_TRUNC		/* do not limit pathname length */

/* typedefs */

/* function declarations */

/* sem_init() is in semaphore.h */

extern int      sem_destroy  (sem_t *);
extern sem_t *  sem_open     (const char *, int, ...);
extern int      sem_close    (sem_t *);
extern int      sem_unlink   (const char *);
extern int      sem_wait     (sem_t *);
extern int      sem_trywait  (sem_t *);
extern int      sem_post     (sem_t *);
extern int      sem_getvalue (sem_t *, int *);

#ifdef __cplusplus
}
#endif

#endif /* __INCsemaphoreCommonh */
