/* dirent.h - POSIX directory handling definitions */

/*
 * Copyright 1984-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01m,28jul05,act  add an "end of dir" flag to a DIR for FIOREADDIR ops
01l,20jlk05,jlk  added d_ino field to dirent struct for POSIX.
01k,09apr04,dat  adding const to char ptrs
01j,22sep92,rrr  added support for c++
01i,04jul92,jcf  cleaned up.
01h,26may92,rrr  the tree shuffle
01g,04oct91,rrr  passed through the ansification filter
		  -fixed #else and #endif
		  -changed VOID to void
		  -changed copyright notice
01f,10jun91.del  added pragma for gnu960 alignment.
01e,05oct90,dnw  added rewinddir() and closedir().
01d,05oct90,shl  added ANSI function prototypes.
                 made #endif ANSI style.
                 added copyright notice.
01c,07aug90,shl  added IMPORT type to function declarations.
01b,25may90,dnw  moved NAME_MAX to limits.h
		 added copyright
01a,17apr90,kdl  written.
*/


#ifndef __INCdirenth
#define __INCdirenth

#ifdef __cplusplus
extern "C" {
#endif

#include "limits.h"


/* Directory entry */

struct dirent		/* dirent */
    {
    ino_t	d_ino;                  /* file serial number */
    char	d_name [NAME_MAX + 1];	/* file name, null-terminated */
    };


/* HIDDEN - not for end-customer use */
/* Directory descriptor */

typedef struct		/* DIR */
    {
    int		  dd_fd;		/* file descriptor for open directory */
    int		  dd_cookie;		/* filesys-specific marker within dir */
    BOOL	  dd_eof;		/* readdir EOF flag */
    struct dirent dd_dirent;		/* obtained directory entry */
    } DIR;

/* END_HIDDEN */

/* function declarations */

extern DIR *		opendir   (const char *dirName);
extern STATUS 		closedir  (DIR *pDir);
extern struct dirent *	readdir   (DIR *pDir);
extern void 		rewinddir (DIR *pDir);

#ifdef __cplusplus
}
#endif

#endif /* __INCdirenth */
