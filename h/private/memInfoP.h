/* memInfoP.h - Memory management information */

/* Copyright 1998-2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01d,03jun05,yvp  Added #ifndef	_ASMLANGUAGE. 
                 Updated copyright. #include now with angle-brackets.
01c,17may04,tam  removed unused fields heapId, rtpId & virtPgSize
01b,04nov03,tam  removed unecessary definitions
01a,22oct03,gls  ported from AE version 01n
*/

#ifndef __INCmemInfoPh
#define __INCmemInfoPh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "vmLib.h"
#include "private/pgPoolLibP.h"
#include "private/pgMgrLibP.h"
   
/* defines */


#ifndef	_ASMLANGUAGE

/* typedefs */

typedef struct mem_info
    {
    VM_CONTEXT_ID	vmContextId;	/* virtual memory context 	     */
    PAGE_POOL_ID	virtPgPoolId;	/* virtual page pool 		     */
    PAGE_POOL_ID	physPgPoolId;	/* physical page pool list 	     */
    PAGE_MGR_ID		pgMgrId;	/* primary page manager 	     */
    UINT                adrsSpaceId;	/* address space ID		     */
    DL_LIST		mmapList;	/* list of mmap blocks		     */
    } MEM_INFO;

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCmemInfoPh */
