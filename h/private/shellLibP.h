/* shellLibP.h - private header file for the kernel shell module */

/* Copyright 2003-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01v,14feb05,bpn  Moved shellPromptFormatSet() prototype to shellPromptLib.h
01u,01feb05,bpn  Added shellPromptFormatSet() prototype.
01t,10jan05,bpn  Removed dependencies with dbgLib.o.
01s,23sep04,bpn  Updated include files.
01r,28jul04,bpn  Added APIs from shellLib.h.
01q,25may04,bpn  Added SHELL_LKUP_PG_KEY string.
01p,11may04,bpn  Added fgndShellId field to SHELL_CTX structure.
01o,15apr04,bpn  Added isBackground field to SHELL_CTX structure.
01n,07apr04,bpn  Export shellConsoleTaskId value.
01m,26mar04,bpn  Added errnoValue field to SHELL_CTX structure.
01l,28jan04,bpn  Added several fields to SHELL_CTX structure to fix I/O
                 redirection bugs.
01k,14jan04,bpn  Added inputLine field to SHELL_CTX structure to fix memory
                 leaks.
01j,28oct03,bpn  Moved SHELL_USER_DATA structure to shellDataLibP.h. Removed
                 shellLedCompletion() prototype (local function now).
01i,24oct03,bpn  Added shellLedCompletion() prototype.
01h,22sep03,bpn  Added shellInterpEvaluate() prototype.
01g,25jun03,bpn  Removed shellHistSize field from SHELL_CTX structure.
01f,19may03,bpn  Add the user data structure definition.
01e,17apr03,lcs  Add routines to configure shell environment.
01d,08apr03,bpn  Added fields to the shell_ctx structure to detect a shell task
                 restart and to avoid saving user/passwd several times.
01c,12mar03,lcs  Added support for host shell.
01b,10mar03,bpn  Added interpreter switch mechanism. Changed SHELL_CONTEXT 
		 structure name to SHELL_CTX.
01a,20feb03,bpn  Written.
*/

#ifndef __INCshellLibPh
#define __INCshellLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* Includes */

#ifndef HOST
#include <ioLib.h>
#include <remLib.h>
#else
#include <host.h>
#include <shell.h>
#endif

#include <ledLib.h>
#include <lstLib.h>

#include <shellInterpLib.h>
#include <shellLib.h>
#include <private/shellInternalLibP.h>

/* Defines */

#define SHELL_LKUP_PG_KEY	"_WRS_lkupPgSz"

/* Typedefs */

typedef struct shell_inline_node
    {
    NODE	node;
    char *	inputLine;	/* buffer for the input line */
    } SHELL_INLINE_NODE;

typedef struct shell_ctx_login
    {
    FUNCPTR	loginFunc;		/* shell login function */
    FUNCPTR	logoutFunc;		/* shell logout function */
    int	    	loginParam;		/* param. passed to login function */
    int	    	logoutParam;		/* param. passed to logout function */
    char	user[MAX_IDENTITY_LEN];		/* login name*/
    char	passwd[MAX_IDENTITY_LEN];	/* password */
    BOOL	isIdentitySaved;		/* was login saved ? */
    } SHELL_CTX_LOGIN;

typedef struct shell_ctx_io
    {
    int		    shellStdin;		/* standard stdin */
    int		    shellStdout;	/* standard stdout */
    int		    shellStderr;	/* standard stdout */
    } SHELL_CTX_IO;

typedef struct shell_ctx_values
    {
    BOOL	    isInteractive;	/* interactive or script? */
    const char *    config;		/* configuration string */
    SHELL_CTX_LOGIN login;		/* login stucture */
    SHELL_CTX_IO    io;			/* io structure */
    } SHELL_CTX_VALUES;

typedef struct shell_ctx_compat
    {
    SHELL_CTX_VALUES	initial;	/* initial values */ 
    SHELL_CTX_VALUES	current;	/* initial values */ 
    BOOL		loginAccess;	/* connection with a login? */
    } SHELL_CTX_COMPAT;

/* The shell context structure */

typedef struct shell_ctx
    {
    NODE	    node;
    const char *    taskName;		/* shell task name */
    SHELL_INTERP_CTX * pInterpCtx;	/* interpreter parameters */
    SHELL_INTERP *  pInterpSwitch;	/* next interpreter to use */
    SHELL_ID	    bgndShellId;	/* background session Id */
    SHELL_ID	    fgndShellId;	/* foreground session Id */
    LIST            userData;           /* list of the user data */
    LIST	    interpCtxLst;	/* list of interp context */
    LIST	    ioFileDescLst;	/* list of IO file descriptors */
    LIST	    inputLineLst;	/* list input line buffers */
    SHELL_CTX_VALUES	initial;	/* initial values */ 
    SHELL_CTX_VALUES	current;	/* initial values */ 
    SHELL_CTX_COMPAT	compatible;	/* for compatibility */
    BOOL	    loginAccess;	/* connection with a login? */
    int		    errnoValue;		/* errno value */
    int		    taskStackSize;	/* shell task stack size */
    LED_ID	    ledId;		/* line editor ID */
    int		    taskId;		/* shell task ID */
    BOOL	    isAbortReq;		/* abort requested? */
    BOOL	    isRestarted;	/* task restarted? */
    BOOL	    isBackground;	/* background session? */
    char	    path[MAX_FILENAME_LENGTH];	/* default pathname */
    } SHELL_CTX;

extern int	shellConsoleTaskId;
extern FUNCPTR	_func_shellDbgStackTrace;
extern FUNCPTR	_func_shellDbgCallPrint;

/* Function Declarations */

extern STATUS	shellLogout2 (SHELL_ID shellId, const char * errorStr);
extern STATUS	shellScriptNoAbort (void);
extern void	shellPromptPrint (SHELL_ID shellId);
extern SHELL_ID	shellBackgroundInit (const char * interpName,
				     const char * evalString);
extern char *	shellBackgroundRead (LIST * pMemList, INT32 memFlags);

extern UINT32	shellLineLenGet (void);

extern STATUS	shellExec (SHELL_CTX * pShellCtx);

extern STATUS	shellInterpEvaluate (char * arg,
				     const char * interpreterName,
				     SHELL_EVAL_VALUE * pValue);
#ifdef HOST
extern STATUS	shellHostInitialize (SHELL_CTX * pShellCtx,
				     struct timeval * pStdinPollTimeout,
				     VOIDFUNCPTR eventPoll);
extern void	shellContextHostFinalize (SHELL_CTX * pShellCtx);
#endif

#ifdef __cplusplus
}
#endif

#endif /* __INCshellLibPh */
