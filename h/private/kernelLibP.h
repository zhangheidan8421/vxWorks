/* kernelLibP.h - private header file for kernelLib.c */

/*
 * Copyright (c) 1992-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. No license to Wind River intellectual property rights
 * is granted herein. All rights not licensed by Wind River are reserved
 * by Wind River.
 */

/*
modification history
--------------------
01e,11aug05,dbt  Added KERNEL_CONTEXT(), KERNEL_ENTER(), KERNEL_EXIT() macros.
01d,04feb05,pch  SPR 102772: add usrKernelIntStkSizeAdj()
01c,07apr03,to   removed activeQHead.
01b,22sep92,rrr  added support for c++
01a,04jul92,jcf  created.
*/

#ifndef __INCkernelLibPh
#define __INCkernelLibPh

#ifdef __cplusplus
extern "C" {
#endif
#include <kernelLib.h>
#include <qLib.h>

/* defines */

#define KERNEL_CONTEXT()	(kernelState == TRUE) /* are we in kernel */
						      /* context? */
#define KERNEL_ENTER()		(kernelState = TRUE)  /* enter kernel context */
#define KERNEL_EXIT()		(windExit())	      /* exit kernel context */

/* variable declarations */

extern BOOL	kernelState;		/* mutex to enter kernel state */
extern BOOL	kernelIsIdle;		/* boolean reflecting idle state */
extern BOOL	roundRobinOn;		/* state of round robin scheduling */
extern ULONG	roundRobinSlice;	/* round robin task slice in ticks */
extern int 	rootTaskId;		/* root task id */
extern char *	pRootMemStart;		/* bottom of root task's memory */
extern unsigned rootMemNBytes;		/* actual root task memory size */
extern Q_HEAD	tickQHead;		/* queue for timeouts/delays/wdogs */
extern Q_HEAD	readyQHead;		/* queue for task ready queue */

/* prototypes */

extern void usrKernelIntStkSizeAdj(unsigned *, unsigned *, unsigned *,
				   unsigned *);
					/* stack calculation configlette */

#ifdef __cplusplus
}
#endif

#endif /* __INCkernelLibPh */
