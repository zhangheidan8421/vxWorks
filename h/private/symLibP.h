/* symLibP.h - private symbol library header file */

/*
 * Copyright (c) 2001-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement.
 */

/*
modification history
--------------------
01f,07mar05,v_r  Fixed SPR #98274 (loader backward compatibility errno review).
		 Cleanups + code conventions compliancy work.
01e,24jan05,aeg  added symTblSymSetRemove prototype (SPR #106381).
01d,06oct04,jn   Clean up comments
01c,05apr04,jn   Clean up - remove unused declarations and development-related
                 comments
01b,03apr03,to   replaced OBJ_CORE with HANDLE in SYMTAB (de-classify).
01a,15oct01,jn   created.
*/

#ifndef __INCsymLibPh
#define __INCsymLibPh

#ifdef __cplusplus
extern "C" {
#endif

#include "hashLib.h"
#include "symLib.h"

#ifdef HOST
#include "vmutex.h"
#else
#include "private/semLibP.h"
#include "private/handleLibP.h"
#endif

/* defines */

#ifdef HOST
/* size of hash table given number of elements in hash table log 2 */
#ifndef HASH_TBL_SIZE
#define HASH_TBL_SIZE(sizeLog2)                                         \
        (((1 << sizeLog2) * sizeof (SL_LIST)) + sizeof (HASH_TBL))
#endif /* ndef HASH_TBL_SIZE */
#endif /* HOST */


/* structure definitions */

typedef struct symtab	/* SYMTAB - symbol table */
    {
#ifdef HOST
    OBJ_CORE	handle;		/* handle management */
#else
    HANDLE	handle;		/* object maintanance */
#endif /* HOST */
    HASH_ID	nameHashId;	/* hash table for names */
    SEMAPHORE   symMutex;       /* symbol table mutual exclusion sem */
    PART_ID	symPartId;	/* memory partition id for symbols */
    BOOL	sameNameOk;	/* symbol table name clash policy */
    int		nsymbols;	/* current number of symbols in table */
    } SYMTAB;

/* function declarations */

extern STATUS	symByCNameFind(SYMTAB_ID symTblId, char * name, char ** pValue,
			       SYM_TYPE *pType );
#ifdef HOST
extern SYMBOL * symEachSym (SYMTAB_ID symTblId, FUNCPTR routine, int routineArg);
#endif
extern STATUS	symFindSymbol(SYMTAB_ID symTblId, char * name, void * value,
			      SYM_TYPE type, SYM_TYPE mask,
			      SYMBOL_ID * pSymbolId );
extern STATUS	symNameGet(SYMBOL_ID symbolId, char ** pName);
extern SYMBOL_ID symRegister(SYMTAB_ID symTblId, char * name, void * value, 
			     SYM_TYPE type, UINT16 group, UINT32 symRef, 
			     BOOL callSyncRtn);
extern STATUS	symTblShutdown(SYMTAB_ID symTblId );
extern STATUS	symTblSymSetRemove(SYMTAB_ID symTblId, UINT32 reference, 
				   SYM_TYPE type, SYM_TYPE typeMask);
extern STATUS	symTypeGet(SYMBOL_ID symbolId, SYM_TYPE * pType);
extern STATUS	symValueGet(SYMBOL_ID symbolId, void ** pValue);

#ifdef __cplusplus
}
#endif

#endif /* __INCsymLibPh */

