/* timerSysCall.h - POSIX timer system call definitions */

/* Copyright 2003-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,22apr04,dcc  added 'context' parameter to _timer_open().
01b,02dec03,ans  code inspection comments.
01a,02oct03,ans  written
*/

#ifndef __INCtimerSysCallh
#define __INCtimerSysCallh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"


/* command codes for the timerCtl() system call */

typedef enum
    {
    TIMER_CTL_GETTIME,
    TIMER_CTL_SETTIME,
    TIMER_CTL_GETOVERRUN,
    TIMER_CTL_MODIFY
    } TIMER_CTL_CMD;


/* system call function prototypes */

extern STATUS   timerCtl      (TIMER_CTL_CMD cmdCode, int timerId, 
                               void * pArgs, int argSize);
extern OBJ_HANDLE _timer_open (const char * name, int mode, clockid_t clockId,
                               struct sigevent * evp, void * context);


#ifdef __cplusplus
}
#endif

#endif /* __INCtimerSysCallh */
