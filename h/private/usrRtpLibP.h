/* usrRtpLibP.h - private header file for rtp user interface library */

/* Copyright 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,03aug04,bpn  Written.
*/

#ifndef __INCusrRtpLibPh
#define __INCusrRtpLibPh

#ifdef __cplusplus
extern "C" {
#endif

#include <rtpLib.h>

extern int rtpSpPriority;
extern int rtpSpOptions;
extern int rtpSpStackSize;
extern int rtpSpDelay;
extern int rtpSpTaskOptions;

/* function declarations */

extern STATUS	rtpSymsAdd (RTP_ID rtpId, UINT32 regPolicy, char * filePath);
extern STATUS	rtpTaskShow (RTP_ID rtpId);
extern STATUS	shlSymsAdd (void * shlId, RTP_ID rtpId, UINT32 regPolicy,
			    char * filePath);
extern STATUS	shlSymsRemove (void * shlId, RTP_ID rtpId, UINT32 remPolicy);
extern STATUS	rtpSymsRemove (RTP_ID rtpId, UINT32 remPolicy);
extern STATUS	rtpSymsOverride (int overridePolicy);

#ifdef __cplusplus
}
#endif

#endif /* __INCusrRtpLibPh */

