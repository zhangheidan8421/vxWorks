/* excLibP.h - private exception handling subsystem header file */

/* 
 * Copyright (c) 1984-2005 Wind River Systems, Inc. 
 *
 * The right to copy, distribute, modify or otherwise make use 
 * of this software may be licensed only pursuant to the terms 
 * of an applicable Wind River license agreement. 
 */ 

/*
modification history
--------------------
01e,07jun05,yvp  Added prototype for jobAdd(). Updated copyright.
01d,09mar05,yvp  Added ISR_JOB definition.
01c,20jan05,yvp  Reverted prototype for excInit().
01b,25aug04,ans  updated declaration of excInit()
01a,13nov01,yvp  created.
*/

#ifndef __INCexcLibPh
#define __INCexcLibPh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef	_ASMLANGUAGE

/* defines */

#define ISR_JOB_MAX_ARGS     6

/* typedefs */

/* Structure describing ISR-level job deferral */

typedef struct
    {
    VOIDFUNCPTR func;                   /* pointer to function to invoke */
    int         arg [ISR_JOB_MAX_ARGS]; /* args for function */
    } ISR_JOB;

/* Select extended (32-bit) calls from the vector table to handlers */

extern BOOL excExtendedVectors;

/* function declarations */

extern STATUS 	excVecInit (void);
extern STATUS 	excInit    (int maxIsrJobs);
extern STATUS   jobAdd	   (FUNCPTR func, int arg1, int arg2, int arg3,
			    int arg4, int arg5, int arg6, int * pResult);
extern STATUS   jobExcAdd  (VOIDFUNCPTR func, int arg1, int arg2, int arg3,
			    int arg4, int arg5, int arg6);

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCexcLibPh */
