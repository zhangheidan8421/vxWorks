/* pppIpcpComponentP.h - Private IPCP component header file */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01h,22may02,vk  modified component, stack and profile strcuture for code
                code reusability with IPCP and IPV6CP
01g,30jan02,ak  modified option list structures as per the memory reduction 
				recommendations
01f,03nov01,ijm added maximum_number_of_configuration_failures
                to IPCP_PROFILE_DATA
01e,29sep00,sj  merging with TOR2_0-WINDNET_PPP-CUM_PATCH_2
01d,19may00,qli add a new field in ipcpProfileData, 
                a reference to stack data
01c,10feb00,sj  options array should be +1 number of IPCP options
01b,09feb00sj   added PPP_IPCP_MIB_INTERFACE + clean up
01a,07nov99,sgv derived from target/h/ppp/pppIpcpComponent.h
*/

#ifndef __INCpppIpcpComponentPh
#define __INCpppIpcpComponentPh

#include "vxWorks.h"
#include "logLib.h"
#include "errnoLib.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "ctype.h"
#include "memLib.h"
#include "netBufLib.h"
#include "sockLib.h"
#include "netinet/in.h"
#include "arpa/inet.h"
#include "pfw/pfw.h"
#include "pfw/pfwStack.h"
#include "pfw/pfwProfile.h"
#include "pfw/pfwComponent.h"
#include "pfw/pfwLayer.h"
#include "pfw/pfwEvent.h"
#include "pfw/pfwTimer.h"
#include "pfw/pfwMemory.h"
#include "pfw/pfwInterface.h"
#include "ppp/kstart.h"
#include "ppp/kppp.h"
#include "private/ppp/vpppstr.h"
#include "private/ppp/vnipstr.h"
#include "private/ppp/vlcpstr.h"
#include "private/ppp/vncpstr.h"
#include "private/ppp/pppstate.h"
#include "private/ppp/pppoptn.h"
#include "ppp/interfaces/pppControlLayerInterface.h"
#include "ppp/interfaces/pppControlProtocolInterface.h"
#include "ppp/interfaces/pppIpInterfaces.h"
#include "ppp/interfaces/ipcpInterfaces.h"
#include "ppp/interfaces/pppVjcInterfaces.h"
#include "private/ppp/pppNcpLibP.h"
#include "private/ppp/pppNcpComponentP.h"


/* defines */
/*
 * Inline versions of get/put char/short/long.
 * Pointer is advanced; we assume that both arguments
 * are lvalues and will already be in registers.
 * cp MUST be u_char *.
 */
#define GETCHAR(c, cp) { \
	(c) = *(cp)++; \
}
#define PUTCHAR(c, cp) { \
	*(cp)++ = (u_char) (c); \
}

#define GETSHORT(s, cp) { \
	register u_char *t_cp = (u_char *)(cp); \
	(s) = ((uint16_t)t_cp[0] << 8) \
	    | ((uint16_t)t_cp[1]) ;\
	(cp) += sizeof(uint16_t); \
}

#if 0
#define GETSHORT(s, cp) { \
	(s) = *(cp)++ << 8; \
	(s) |= *(cp)++; \
}
#define PUTSHORT(s, cp) { \
	*(cp)++ = (u_char) ((s) >> 8); \
	*(cp)++ = (u_char) (s); \
}

#define GETLONG(l, cp) { \
	(l) = *(cp)++ << 8; \
	(l) |= *(cp)++; (l) <<= 8; \
	(l) |= *(cp)++; (l) <<= 8; \
	(l) |= *(cp)++; \
}
#define PUTLONG(l, cp) { \
	*(cp)++ = (u_char) ((l) >> 24); \
	*(cp)++ = (u_char) ((l) >> 16); \
	*(cp)++ = (u_char) ((l) >> 8); \
	*(cp)++ = (u_char) ((l)); \
}
#endif

#define IPCP_PACKET_HEADER_SIZE sizeof(NCP_HEADER)

#define REMOTE_STRING

typedef struct ipcpOption
    {
    char                        name[PFW_MAX_NAME_LENGTH];
    BYTE_ENUM(IPCP_OPTION_TYPE) optionType;
    char                        optionTypeString[16];
    } IPCP_CONFIG_OPTION; 

/* Component data per instance; one per pfw */

typedef struct pppIpcpComponent
    {
    PPP_NCP_COMPONENT           pppNcpComponent;
    CONTROL_PROTOCOL_INTERFACE  ipcpControlInterface;
    PPP_IPCP_MIB_INTERFACE      ipcpMibStatusEntry;
    IPCP_CONFIG_OPTION          option[NUMBER_OF_IPCP_OPTIONS+1];
    } PPP_IPCP_COMPONENT;

typedef struct alternateStrings
    {
    struct alternateStrings *   next;
    char                        configString[1];
    }ALTERNATE_OPTION_STRING;

typedef struct ipcpConfigStrings
    {
    char *                      configString;
    ALTERNATE_OPTION_STRING *   alternateConfigString;
    char *                      remoteConfigString;
    } IPCP_CONFIG_STRINGS; 

/* IPCP configuration option types */

typedef	struct ipcpStackData
    {
    NCP_STACK_DATA              stackData;
    PFW_INTERFACE_STATE_PAIR    pppIpInterface;
    PFW_INTERFACE_STATE_PAIR    pppHCInterface;
    BOOL                        hcConfigured;
    PPP_IPCP_MIBS               ipcp_mibs; 
    char                        localAddr[INET_ADDR_LEN];
    char                        remoteAddr[INET_ADDR_LEN];
    char 					    primaryDnsAddr[INET_ADDR_LEN];
    char 					    secondaryDnsAddr[INET_ADDR_LEN];
    BOOLEAN            		    useRasAddress;
    BOOLEAN            		    validRasAddress;
    USHORT                      compressionProtocolId;
    } IPCP_STACK_DATA;                              


typedef struct ipcpProfileData
    {
    NCP_PROFILE_DATA            ncpProfileData;
    IPCP_CONFIG_STRINGS	*       pIpcpConfigStrings;    
    IPCP_STACK_DATA *           pIpcpStackData;
    } IPCP_PROFILE_DATA;

#endif /* __INCpppipcpComponentPh */

