/* pppIpv6cpComponentP.h - Private IPV6CP component header file */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01c,17jul02,ijm added INTERFACE_ADDRESS_LENGTH 
01b,08jul02,as Added two new variables in IPV6CP_STACK_DATA
01a,22may02,vk derived from target/h/ppp/pppIpcpComponentP.h
*/

#ifndef __INCpppIpv6cpComponentPh
#define __INCpppIpv6cpComponentPh

#include "vxWorks.h"
#include "logLib.h"
#include "errnoLib.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "ctype.h"
#include "memLib.h"
#include "netBufLib.h"
#include "sockLib.h"
#include "netinet/in.h"
#include "arpa/inet.h"
#include "pfw/pfw.h"
#include "pfw/pfwStack.h"
#include "pfw/pfwProfile.h"
#include "pfw/pfwComponent.h"
#include "pfw/pfwLayer.h"
#include "pfw/pfwEvent.h"
#include "pfw/pfwTimer.h"
#include "pfw/pfwMemory.h"
#include "pfw/pfwInterface.h"
#include "ppp/kstart.h"
#include "ppp/kppp.h"
#include "private/ppp/vpppstr.h"
#include "private/ppp/vnipstr.h"
#include "private/ppp/vlcpstr.h"
#include "private/ppp/vncpstr.h"
#include "private/ppp/pppstate.h"
#include "private/ppp/pppoptn.h"
#include "ppp/interfaces/pppControlLayerInterface.h"
#include "ppp/interfaces/pppControlProtocolInterface.h"
#include "private/ppp/pppNcpLibP.h"
#include "private/ppp/pppIpcpComponentP.h"

#define IPV6CP_PACKET_HEADER_SIZE       sizeof(NCP_HEADER)
#define INTERFACE_IDENTIFIER_LENGTH     8
#define INTERFACE_ADDRESS_LENGTH        46 /*
                                            * same as INET6_ADDR_LEN which
                                            * is defined only in ipv6 stack.
                                            * This definition is added
                                            * to allow compilation in non-ipv6
                                            * stacks.
                                            */

#define REMOTE_STRING

typedef struct ipv6cpOption
    {
    char                            name[PFW_MAX_NAME_LENGTH];
    BYTE_ENUM(IPV6CP_OPTION_TYPE)   optionType;
    char                            optionTypeString[16];
    } IPV6CP_CONFIG_OPTION; 

typedef struct ipv6cpConfigStrings
    {
    char *                          configString;
    ALTERNATE_OPTION_STRING *       alternateConfigString;
    char *                          remoteConfigString;
    } IPV6CP_CONFIG_STRINGS; 

/* Component data per instance; one per pfw */

typedef struct ipv6cpStackData
    {
    NCP_STACK_DATA              stackData;
    PFW_INTERFACE_STATE_PAIR    pppIpv6Interface;
    PFW_INTERFACE_STATE_PAIR    pppHCInterface;
    USHORT                      hcProtocolId;
    BOOL                        headerCompressionNegotiated;
    char                        localAddr [INTERFACE_ADDRESS_LENGTH];
    char                        remoteAddr [INTERFACE_ADDRESS_LENGTH];
    char                        lastSentNakIdentifier [INTERFACE_IDENTIFIER_LENGTH];
    BOOL                        previousNakSent;
    } IPV6CP_STACK_DATA;


typedef struct pppIpv6cpComponent
    {
    PPP_NCP_COMPONENT           pppNcpComponent;
    CONTROL_PROTOCOL_INTERFACE  ipv6cpControlInterface;
    IPV6CP_CONFIG_OPTION        option[NUMBER_OF_IPV6CP_OPTIONS+1];
    } PPP_IPV6CP_COMPONENT;


typedef struct ipv6cpProfileData
    {
    NCP_PROFILE_DATA         ncpProfileData;
    IPV6CP_CONFIG_STRINGS *  pIpv6cpConfigStrings;    
    IPV6CP_STACK_DATA *      pIpv6cpStackData;
    } IPV6CP_PROFILE_DATA;



#endif /* __INCpppipv6cpComponentPh */

