/* pppNcpComponent.h - NCP component header file */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01i,22may02,vk  moved the ncpStackData to pppNcpLibP.h 
01h,08nov01,ijm added ras parameters and ncp_configuration_naks
01g,29sep00,sj  merging with TOR2_0-WINDNET_PPP-CUM_PATCH_2
01f,20jul00,cn   removed unnecessary sockFd.
01e,08jul00,bsn  merged from the PPP radius-ras branch
01d,27jun00,bsn  added NCP up and down event objects
01c,09feb00,sj  changed NPC_MIBS to IPCP_MIBS
01b,19nov99,sgv Modified stack data src and dst address lengths
01a,12oct99,sgv derived from routerware source base
*/

#ifndef __INCpppNcpComponentPh
#define __INCpppNcpComponentPh

#ifdef __cplusplus
extern "C" {
#endif


#include "vxWorks.h"
#include "logLib.h"
#include "errnoLib.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "ctype.h"
#include "memLib.h"
#include "inetLib.h"
#include "netBufLib.h"
#include "pfw/pfw.h"
#include "pfw/pfwStack.h"
#include "pfw/pfwProfile.h"
#include "pfw/pfwComponent.h"
#include "pfw/pfwLayer.h"
#include "pfw/pfwEvent.h"
#include "pfw/pfwTimer.h"
#include "ppp/kstart.h"
#include "ppp/kppp.h"
#include "private/ppp/vnipstr.h"
#include "private/ppp/vpppstr.h"
#include "private/ppp/vlcpstr.h"
#include "private/ppp/vncpstr.h"
/*
typedef	struct ncpStackData
    {
    PPP_CONTROL_PROTOCOL_STATE_DATA    stateData;
    OPTION_LISTS                  option_lists;
    PPP_CONTROL_LAYER_INTERFACE   *controlLayerInterface;
    PFW_INTERFACE_STATE_PAIR      pppIpInterface;
    PFW_INTERFACE_STATE_PAIR      pppVjcInterface;
    BOOL			  vjcConfigured;
    PFW_TIMER_OBJ *               ncp_timer;
    PFW_EVENT_OBJ *               ncpUpEventObj;
    PFW_EVENT_OBJ *               ncpDownEventObj;
    BYTE                          last_id_of_ncp_packet_sent;
    BYTE                          last_request_id;
    BYTE                          id_sequence_number;
    BYTE		          state;
    BYTE		          old_state;
    USHORT			  ncp_protocol;
    int				  length_of_last_txed_request;
    USHORT                        number_of_ncp_configuration_requests;
    USHORT                        number_of_ncp_termination_requests;
    USHORT                        number_of_ncp_configuration_naks;
    USHORT                        configuration_request_backoff_interval;
    USHORT                        configuration_request_send_interval;
    USHORT                        termination_request_send_interval;
    PPP_IPCP_MIBS                 ipcp_mibs; 
    M_BLK_ID                      retry_ncp_configuration_request_packet;
    M_BLK_ID                      last_txed_ncp_configuration_request_packet;
    M_BLK_ID                      last_rxed_ncp_configuration_request_packet;
    BYTE                          last_rxed_ncp_id_sequence_number;
    char 			  localAddr[INET_ADDR_LEN];
    char 			  remoteAddr[INET_ADDR_LEN];
    char 			  primaryDnsAddr[INET_ADDR_LEN];
    char 			  secondaryDnsAddr[INET_ADDR_LEN];
    PPP_NCP_STATISTICS            statistics;  
    PFW_PLUGIN_OBJ_CALLBACKS    * callbacks;
    BOOLEAN                       configuration_request_backoff_period_started;
    BOOLEAN                       useRasAddress;
    BOOLEAN                       validRasAddress;
    } NCP_STACK_DATA;
*/

#ifdef __cplusplus
}
#endif

#endif /* __INCpppNcpComponentPh */

