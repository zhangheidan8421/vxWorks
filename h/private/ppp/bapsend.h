/* bapsend.h - BAP protocol send header file */

/* Copyright 1999 Wind River Systems, Inc. */

/* 
modification history
--------------------
02b,08oct01,as  updated function prototypes for bap_send_call_response and
				bap_create_callback_request to support 
				multiple phone delta option.
01a,01feb01,as  created from routerware source
*/

#ifndef __INCbapsendh
#define __INCbapsendh

#ifdef __cplusplus
extern "C" {
#endif

#include "pfw/pfw.h"

/* function declarations */

/* Functions for sending BAP protocol packets */

extern TEST bap_send_call_response (PFW_PLUGIN_OBJ_STATE * pluginState,
			BYTE id, char * reason_string, BYTE response_code,
			BAP_LINK_TYPES link_type, USHORT link_speed, 
			SL_LIST	* portList);

extern TEST bap_send_callback_response (PFW_PLUGIN_OBJ_STATE * pluginState, 
			BYTE id, char * reason_string, BYTE response_code,            
			BAP_LINK_TYPES link_type, USHORT link_speed);

extern TEST bap_send_link_drop_query_response 
			(PFW_PLUGIN_OBJ_STATE * pluginState, BYTE id,
			 char * reason_string, BYTE response_code);

extern TEST bap_send_call_status_response 
			(PFW_PLUGIN_OBJ_STATE * pluginState, BYTE id, char * reason_string);

extern TEST bap_send_call_status_indication (PFW_PLUGIN_OBJ_STATE * pluginState,
			BYTE id, char * reason_string, PORT_CONNECTION_INFO * port_info);


/* Functions for creating BAP protocol packets */

extern BAP_BUFFER * bap_create_call_request (PFW_PLUGIN_OBJ_STATE * pluginState,
			BYTE id, PORT_CONNECTION_INFO * port_info, char * reason_string);

extern BAP_BUFFER * bap_create_callback_request 
			(PFW_PLUGIN_OBJ_STATE * pluginState, BYTE id,
			SL_LIST	* portList, char * reason_string,
			BOOL callBackRequest);

extern BAP_BUFFER * bap_create_link_drop_request 
			(PFW_PLUGIN_OBJ_STATE * pluginState, BYTE id,
			 PORT_CONNECTION_INFO * port_info, char * reason_string);

#ifdef __cplusplus
}
#endif

#endif /* __INCbapsendh */
