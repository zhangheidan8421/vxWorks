/* pppIpState.h - header file for state machine functions for PPP 
                             Network control protocol(IPCP and IPV6CP) */

/* Copyright 2002 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01a,12jun02,vk  derived from pppIpcpComponent.c and pppIpv6cpComponent.c files
*/

/*
DESCRIPTION

This file contains the function declarations for the pppIpStateMachineFunction.c file

*/

#ifndef __INCpppIpStateh
#define __INCpppIpStateh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "logLib.h"
#include "errnoLib.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "ctype.h"
#include "memLib.h"
#include "inetLib.h"
#include "netBufLib.h"
#include "pfw/pfw.h"
#include "pfw/pfwStack.h"
#include "pfw/pfwProfile.h"
#include "pfw/pfwComponent.h"
#include "pfw/pfwLayer.h"
#include "pfw/pfwEvent.h"
#include "pfw/pfwTimer.h"
#include "ppp/kstart.h"
#include "ppp/kppp.h"
#include "private/ppp/pppstate.h"
#include "private/ppp/vnipstr.h"
#include "private/ppp/vpppstr.h"
#include "private/ppp/vlcpstr.h"
#include "private/ppp/vncpstr.h"
#include "ppp/interfaces/pppControlLayerInterface.h"
#include "ppp/interfaces/pppControlProtocolInterface.h"



/* IPCP state machine table function definition */

void ipcp_set_ppp_state
    (
    PFW_PLUGIN_OBJ_STATE *         pPfwPlugingObjState,
    M_BLK_ID                       pMblk,
    PPP_STATE                      pppEndState
    );

void ipcp_this_layer_up
    (
    PFW_PLUGIN_OBJ_STATE *         pPfwPlugingObjState,
    M_BLK_ID                       pMblk, 
    PPP_STATE                      pppEndState
    );

void ipcp_this_layer_down
    (
    PFW_PLUGIN_OBJ_STATE *         pPfwPlugingObjState,
    M_BLK_ID                       pMblk, 
    PPP_STATE                      pppEndState
    );



/* IPV6CP state machine table function definition */

void ipv6cp_set_ppp_state
    (
    PFW_PLUGIN_OBJ_STATE *         pPfwPlugingObjState,
    M_BLK_ID                       pMblk,
    PPP_STATE                      pppEndState
    );

void ipv6cp_this_layer_up
    (
    PFW_PLUGIN_OBJ_STATE *         pPfwPlugingObjState,
    M_BLK_ID                       pMblk,
    PPP_STATE                      pppEndState
    );

void ipv6cp_this_layer_down
    (
    PFW_PLUGIN_OBJ_STATE *         pPfwPlugingObjState,
    M_BLK_ID                       pMblk,
    PPP_STATE                      pppEndState
    );


/* Common state machine functions for both IPCP and IPV6CP */ 

void ipcp_this_layer_start 
    (
    PFW_PLUGIN_OBJ_STATE *         pPfwPlugingObjState,
    M_BLK_ID                       pMblk,
    PPP_STATE                      pppEndState
    );

void ipcp_this_layer_finished 
    (
    PFW_PLUGIN_OBJ_STATE *         pPfwPlugingObjState,
    M_BLK_ID                       pMblk,
    PPP_STATE                      pppEndState
    );

void ipcp_initialize_restart_counter
    (
    PFW_PLUGIN_OBJ_STATE *         pPfwPlugingObjState,
    M_BLK_ID                       pMblk,
    PPP_STATE                      pppEndState
    );

void ipcp_zero_restart_counter
    (
    PFW_PLUGIN_OBJ_STATE *         pPfwPlugingObjState,
    M_BLK_ID                       pMblk,
    PPP_STATE                      pppEndState
    );

void ipcp_null_state 
    (
    PFW_PLUGIN_OBJ_STATE *         pPfwPlugingObjState,
    M_BLK_ID                       pMblk,
    PPP_STATE                      pppEndState
    );


#endif   /* __INCpppIpStateh */

