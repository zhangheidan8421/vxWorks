/* vpppstr.h - PPP structure definitions */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01f,04Feb02,ak  redefined option list structures as per the memory reduction 
				recomendations
01e,07dec01,mk  added macros _WRS_PACK_ALIGN(x),to fix alignment
                problems for PPP for arm
01d,29sep00,sj  merging with TOR2_0-WINDNET_PPP-CUM_PATCH_2
01c,01aug00,adb  Merging with openstack view
01b,11jul00,md  fixed type redefinition
01a,28sep99,sj 	derived from routerware source base
*/

/*
 *$Log:: /Rtrware/devdrvrs/ppp/vpppstr.h                                                            $
 * 
 * 11    11/20/98 11:36a Nishit
 * close_ppp moved to pppinit.c from ppp_winrouter.c; made non-static
 * 
 * 10    11/18/98 7:49a Nishit
 * Configurable Booleans are now configed by set_enum_enable
 * 
 * 9     11/17/98 8:41a Nishit
 * Added a mapping to lsl_port_number in PPP_PORT_CLASS
 * 
 * 8     10/01/98 11:43a Alex
 * Updated the PPP source code to conform to a single build. 
 * 
 * 7     9/23/98 5:52p Nishit
 * Added support for RADIUS and TACACS
 * 
 * 6     4/30/98 3:03p Alex
 * Ppp v4.2.0 check in
 * 
 * 1     4/24/98 12:10a Release Engineer
 * code cleanup, code style changes,
 * linted, system level test
 * PPP v4.2.0
 */

#ifndef __INCvpppstrh
#define __INCvpppstrh

#include "ppp/kstart.h"
#include "ppp/kppp.h"

#ifdef __cplusplus
extern "C" {
#endif

#define UNNUMBERED_INFORMATION 3

typedef	union UNION_OPTION_TYPES
    {
    BYTE_ENUM (LCP_OPTION_TYPE)   lcp;
    BYTE_ENUM (IPCP_OPTION_TYPE)  ipcp;
#if 0
    BYTE_ENUM (ATCP_OPTION_TYPE)  atcp;
    BYTE_ENUM (IPXCP_OPTION_TYPE) ipxcp;
#endif /* 0 */ 
    BYTE                          generic;
    } UNION_OPTION_TYPES;

typedef	union UNION_OPTION_DATA_TYPES
    {
    BYTE_ENUM (BOOLEAN) boolean;
    BYTE                byte_array[VARIABLE_NUMBER_OF_BYTES];
    ULONG               _ulong;
    USHORT              _ushort;
    BYTE                _byte;
    } UNION_OPTION_DATA_TYPES;

typedef struct PPP_OPTION
    {
    UNION_OPTION_TYPES      type;
    BYTE                    length;
    /*UNION_OPTION_DATA_TYPES data;*/
    BYTE	            data[1];
    } PPP_OPTION;

#if !defined _LSLPROTO_H_
typedef	struct LINK
    {
    struct LINK *sptr_forward_link;
    struct LINK *sptr_backward_link;
    } LINK; 
#endif /* _LSLPROTO_H_ */

typedef struct VALUE_TYPE
    {
    char *cptr_value_type_string;
    BYTE_ENUM (BOOLEAN)	value_swapped;
    BYTE length; 
    BYTE offset;	
    } VALUE_TYPE;

typedef	struct ALTERNATE_OPTION_LINK
    {
    struct ALTERNATE_OPTION *sptr_forward_link;
    struct ALTERNATE_OPTION *sptr_backward_link;
    } ALTERNATE_OPTION_LINK;

typedef	struct ALTERNATE_OPTION
    {
    ALTERNATE_OPTION_LINK   links;
    UNION_OPTION_DATA_TYPES *uptr_data;
    int                     preference;
    BYTE                    length;
    } ALTERNATE_OPTION; 

typedef struct rangeOptionHolder
    {
    UNION_OPTION_DATA_TYPES  *uptr_lowest_value;
    UNION_OPTION_DATA_TYPES  *uptr_highest_value;
    UNION_OPTION_DATA_TYPES  *uptr_step;
    } RANGE_OPTION_HOLDER; 

typedef struct optionFlags
    {
    BYTE        automatic_nak_processing:1;
    BYTE        negotiation_required:1;
    BYTE        negotiable:1;
    BYTE        value_swapped:1;
    BYTE        alternate_checking_enabled:1;
    BYTE        range_checking_enabled:1;
    } OPTION_FLAGS; 

typedef	struct OPTION_LIST_ENTRY
    {
    struct OPTION_LIST_ENTRY *sptr_forward_link;
    struct OPTION_LIST_ENTRY *sptr_backward_link;
    char                     *cptr_option_name;
    UNION_OPTION_DATA_TYPES  *uptr_data;
    ALTERNATE_OPTION_LINK    alternate_option_list;
    RANGE_OPTION_HOLDER      *range_option;
    BYTE                     nak_option_selected;
    BYTE_ENUM (OPTION_STATE) state;
    UNION_OPTION_TYPES       type;
    BYTE                     length;
    OPTION_FLAGS             option_flags;
    } OPTION_LIST_ENTRY; 

typedef	struct OPTION_LIST
    {
    struct OPTION_LIST_ENTRY *sptr_forward_link;
    struct OPTION_LIST_ENTRY *sptr_backward_link;
    } OPTION_LIST;

typedef	struct OPTION_LISTS
    {
    OPTION_LIST configured;
    OPTION_LIST	remote_configured;
    OPTION_LIST	received;
    OPTION_LIST	tx_nak;
    OPTION_LIST	rx_nak;
    OPTION_LIST	tx_reject;
    OPTION_LIST	rx_reject;
    OPTION_LIST	tx_accepted;
    OPTION_LIST	rx_accepted;
    } OPTION_LISTS;

typedef void (*FPTR_LCP_STATE_FUNCTION) 
		(
		USHORT real_port_number,
		void *vptr_packet,
		USHORT number_of_bytes,
		PPP_STATE end_state
		);

typedef TEST (*FPTR_PROCESS_RECEIVE_OPTIONS)
		(
		OPTION_LIST_ENTRY *sptr_option,
		void *vptr_class
		);

typedef	FPTR_PROCESS_RECEIVE_OPTIONS * RECEIVE_OPTION_FUNCTIONS;

typedef struct PROTOCOL_STACK_ID_CLASS
    {
    ULONG id;	
    } PROTOCOL_STACK_ID_CLASS;

#if !defined (WAN_CRC_DEFINE)

#define WAN_CRC_DEFINE

typedef	struct WAN_CRC
    {
    USHORT crc;
    } WAN_CRC;
#endif

typedef	struct PPP_HEADER
    {
#if 0
    BYTE                             hdlc_address;
    BYTE_ENUM (LLC_FRAME_TYPE)       hdlc_control;
#endif
    USHORT_ENUM (PPP_PROTOCOL_TYPE)  protocol_type;
    } _WRS_PACK_ALIGN(1)  PPP_HEADER;

typedef	struct PPP_PACKET_WITH_MAC_HEADER
    {
    BYTE       filler[10];
    PPP_HEADER header;
    BYTE       protocol_data[1];
    } _WRS_PACK_ALIGN(1)  PPP_PACKET_WITH_MAC_HEADER;

typedef	struct PPP_PACKET
    {
    PPP_HEADER                   header;
    BYTE_ENUM (PPP_CONTROL_CODE) code;
    BYTE                         id;
    USHORT                       length;
    BYTE                         data[VARIABLE_NUMBER_OF_BYTES];
    } _WRS_PACK_ALIGN(1)  PPP_PACKET;

typedef	struct PPP_PACKET_WITH_COMPRESSED_ADDRESS_AND_CONTROL
    {
    USHORT_ENUM (PPP_PROTOCOL_TYPE) protocol_type;
    BYTE                            data[VARIABLE_NUMBER_OF_BYTES];
    } _WRS_PACK_ALIGN(1)  PPP_PACKET_WITH_COMPRESSED_ADDRESS_AND_CONTROL;

typedef	struct PPP_PACKET_WITH_COMPRESSED_PROTOCOL_FIELD
    {
    BYTE                       hdlc_address;
    BYTE_ENUM (LLC_FRAME_TYPE) hdlc_control;
    BYTE                       protocol_type;
    BYTE                       data[VARIABLE_NUMBER_OF_BYTES];
    } _WRS_PACK_ALIGN(1)  PPP_PACKET_WITH_COMPRESSED_PROTOCOL_FIELD;

typedef	struct HEADER_COMPRESSED_PPP_PACKET
    {
    BYTE protocol_type;
    BYTE data[VARIABLE_NUMBER_OF_BYTES];
    } _WRS_PACK_ALIGN(1)  HEADER_COMPRESSED_PPP_PACKET;

#ifdef __cplusplus
}
#endif

#endif /* __INCvpppstrh */
