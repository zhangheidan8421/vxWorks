/* mpFramingLayerP.h - mpFraming layer component private header file */

/* Copyright 2003 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01d,15may03,ijm include semLib.h
01c,30jan03,ijm fix for SPR# 86001, cannot re-establish bundle if peer reboots  
01b,22oct02,ijm include mpP.h
01a,19feb01,ak created
*/

#ifndef __INCmpFramingLayerPh
#define __INCmpFramingLayerPh

#ifdef __cplusplus
extern "C" {
#endif

#include "ppp/mp.h"
#include "private/ppp/mpP.h"
#include "ppp/interfaces/lcpInterfaces.h"
#include "ppp/interfaces/mpFramingLayerInterfaces.h"
#include "semLib.h"

/* MP_FRAMING_LAYER_COMPONENT structure */

typedef struct MP_FRAMING_LAYER
	{
	PFW_LAYER_OBJ					layer;
	USHORT							no_of_bundles;
	USHORT      					no_of_active_bundles;
	USHORT 							no_of_ports;
	RING_ID							mpSendRing;
	SEM_ID							sendSem;
	int								mpSendQTaskId;
	RING_ID							mpReceiveRing;
	SEM_ID							receiveSem;
	int								mpReceiveQTaskId;
	MP_BUNDLE_COUNT_TO_MANAGER_STACK_LOOKUP_CLASS 	  
				bundle_count_to_manager_stackObj_lookup [MAX_NO_OF_PPP_PORTS];

	PFW_PLUGIN_OBJ_STATE 			*pMpFramingLayerState[MAX_NO_OF_PPP_PORTS];
	MP_BUNDLE_ASSIGNMENT_CLASS port_to_bundle_assignment[MAX_NO_OF_PPP_PORTS];
	MP_BUNDLE_MANAGEMENT_INTERFACE	mpBundleManagementInterface;
	MP_PACKET_RECEIVE_INTERFACE		mpPacketReceiveInterface; 
	SEM_ID							componentSem; 
	}MP_FRAMING_LAYER;


/* MP_FRAMING_LAYER_STACK_DATA structure */ 

typedef struct MP_FRAMING_LAYER_STACK_DATA	
	{
	NET_POOL_ID						netPoolId; 
	MP_BUNDLE_CLASS					bundle;
	UINT 							authMpInterfaceId;	
	BOOL							bundle_state;
	LCP_BUNDLE_PROFILE 				lcpBundleProfile;
	PFW_EVENT_OBJ					*pMpBundleOpenedEventObj;
	PFW_EVENT_OBJ					*pMpBundleClosedEventObj;
	PFW_EVENT_OBJ					*pMpMemberAddedEventObj; 
	PFW_EVENT_OBJ					*pMpMemberDroppedEventObj;
    PFW_EVENT_OBJ                   *pMpSubLayerDeadEventObj;     
	LCP_BUNDLE_OPTIONS_INTERFACE 	*pLcpBundleOptionsInterfaceObj; 
	PFW_INTERFACE_STATE_PAIR 		proxyLcpInterfaceStatePair;	
	
	PFW_INTERFACE_STATE_PAIR 		
								pppLinkStatusCounterIncrementInterfaceStatePair;

#if 0 /* NOT SUPPORTED IN THIS RELEASE */
	LCP_MP_TIME_INTERFACE			*pLcpMpLinkTimeInterfaceObj;
#endif

	LCP_PACKET_SEND_INTERFACE		*pLcpPacketSendInterfaceObj;
	PFW_PLUGIN_OBJ_CALLBACKS		*callbacks;
	} MP_FRAMING_LAYER_STACK_DATA;


/* MP_FRAMING_LAYER profile data structure */

typedef struct mpFramingLayerProfileData
	{
	ULONG	mpFraming_smallPacketLength;
	char	mpFraming_localUserName[MAX_PROFILE_PARAM_LENGTH];
/* ANVL */
	BOOL	mpFraming_disableAuthOnBundle;
/* ANVL */

	} MP_FRAMING_LAYER_PROFILE_DATA;

#ifdef __cplusplus
}
#endif

#endif /* __INCmpFramingLayerPh */ 
