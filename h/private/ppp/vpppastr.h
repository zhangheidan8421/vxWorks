/* vpppastr.h - PPP Auth protocol structure definitions */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01c,07dec01,mk  added macros _WRS_PACK_ALIGN(x),to fix alignment
                problems for PPP for arm
01b,08mar00,sj 	added "padding" byte to CHAP ACK and NAK messages for allignment
01a,05nov99,sj 	derived from routerware source base
*/

#ifndef __INCvpppastrh
#define __INCvpppastrh

#include "ppp/kstart.h"
#include "ppp/kppp.h"
#include "private/ppp/vpppstr.h"
#include "private/ppp/vlcpstr.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
*$Log:: /Rtrware/devdrvrs/ppp/vpppast $
 * 
 * 8     10/01/98 11:43a Alex
 * Updated the PPP source code to conform
 * to a single build. 
 * 
 * 7     4/30/98 3:03p Alex
 * Ppp v4.2.0 check in
 * 
 * 1     4/24/98 12:10a Release Engineer
 * code cleanup, code style changes,
 * linted, system level test
 * PPP v4.2.0
*/
typedef struct AUTHENTICATION_REQUEST_PACKET
    {
    PPP_HEADER                          header;
    BYTE_ENUM (PPP_CONTROL_CODE)        code;
    BYTE                                id;
    USHORT                              length;
    BYTE                                peer_id_length;
    char                                peer_id[MAXIMUM_STRING_LENGTH];
    BYTE peer_password_length;
    char peer_password[MAXIMUM_STRING_LENGTH];
    } _WRS_PACK_ALIGN(1)  AUTHENTICATION_REQUEST_PACKET;

typedef struct AUTHENTICATION_ACK_PACKET
    {
    PPP_HEADER                    header;
    BYTE_ENUM (PPP_CONTROL_CODE)  code;
    BYTE                          id;
    USHORT                        length;
    BYTE                          message_length;
    BYTE                          padding;
    char                          message[MAXIMUM_STRING_LENGTH];
    } _WRS_PACK_ALIGN(1)  AUTHENTICATION_ACK_PACKET;

typedef struct AUTHENTICATION_NAK_PACKET
    {
    PPP_HEADER                    header;
    BYTE_ENUM (PPP_CONTROL_CODE)  code;
    BYTE                          id;
    USHORT                        length;
    BYTE                          message_length;
    BYTE                          padding;
    char                          message[MAXIMUM_STRING_LENGTH];
    } _WRS_PACK_ALIGN(1)  AUTHENTICATION_NAK_PACKET;

typedef	struct PAP_PACKET
    {
    PPP_HEADER ppp_header;
    LCP_HEADER lcp_header;
    } _WRS_PACK_ALIGN(1)  PAP_PACKET;

typedef	union UNION_CHAP_VALUE
    {
    BYTE challenge[CHALLENGE_VALUE_SIZE];
    BYTE response[RESPONSE_VALUE_SIZE];
    } UNION_CHAP_VALUE;

typedef	struct CHAP_PACKET
    {
    PPP_HEADER         ppp_header;
    LCP_HEADER         lcp_header;
    BYTE               value_size;
    BYTE               value[CHALLENGE_VALUE_SIZE];
    char               name[VARIABLE_NUMBER_OF_BYTES];
    } _WRS_PACK_ALIGN(1)  CHAP_PACKET;

typedef	struct PPP_AUTHENTICATION_STATISTICS
    {
    ULONG number_of_tx_packets;
    ULONG number_of_tx_bytes;
    ULONG number_of_rx_packets;
    ULONG number_of_rx_bytes;
    } PPP_AUTHENTICATION_STATISTICS;

typedef	struct	PPP_SECURITY_MIBS
    {
    ULONG  pppSecurityConfigLink;
    ULONG  pppSecurityConfigPreference;
    BYTE   pppSecurityConfigProtocol[20];
    USHORT pppSecurtiyConfigStatus;
    ULONG  pppSecuritySecretsLink;
    ULONG  pppSecuritySecretsIdIndex;
    USHORT pppSecuritySecretsDirection;
    BYTE   pppSecuritySecretsProtocol[20];
    USHORT pppSecuritySecretsIdentity;
    BYTE   pppSecuritySecretsSecret[20];
    USHORT pppSecuritySecretsStatus;
    } PPP_SECURITY_MIBS;

typedef void (*FPTR_AUTH_PACKET_RECEIVED)
         (
         PFW_PLUGIN_OBJ_STATE * pluginState,
         M_BLK_ID packet
         );

#ifdef __cplusplus
}
#endif

#endif /* __INCvpppastrh */
