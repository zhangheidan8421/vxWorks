/* vbacpstr.h - BACP structure definitions */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
02c,19apr02,jr	packed the structures for ARM fixes.
02b,13feb02,as	removed the member BACP_NO_FAVORED_PEER_NEGOTIATED
			from the structure BACP_OPTION_TYPE
01a,01feb01,sd 	derived from routerware source base
*/

/*
*$Log:: $
 * 
 * 4     4/
 * Bacp
 * v4.2.0
 * check in
 * 
 * 1     4
 * code
 * cleanup
 * , code
 * style
 * changes
 * ,
 * linted,
 * system
 * level
 * test
 * BACP
 * v4.2.0
*/

#ifndef __INCvbacpstrh
#define __INCvbacpstrh

#ifdef __cplusplus
extern "C" {
#endif

#include "ppp/kstart.h"
#include "ppp/kppp.h"
#include "private/ppp/vpppstr.h"
#include "pfw/pfw.h"
#include "ppp/kbacpif.h"

typedef enum 
    {
	BACP_FAVORED_PEER		 = 	0x01,
	NUMBER_OF_BACP_OPTIONS 
    }BACP_OPTION_TYPE;

typedef	struct	BACP_HEADER
	{
	BYTE_ENUM (PPP_CONTROL_CODE) 	code;
	BYTE							id;
	USHORT 							length;
	} BACP_HEADER;

typedef struct FAVORED_PEER_OPTION
	{
	BYTE_ENUM (BACP_OPTION_TYPE) 	type;
	BYTE							length;
	ULONG							magic_number;
	}_WRS_PACK_ALIGN(1) FAVORED_PEER_OPTION;

typedef	union		UNION_BACP_OPTIONS
	{
	FAVORED_PEER_OPTION				favored_peer_option;
	}_WRS_PACK_ALIGN(1) UNION_BACP_OPTIONS;

typedef	struct BACP_GENERIC_OPTION
    {
    BYTE type;
    BYTE length;
    BYTE data[VARIABLE_NUMBER_OF_BYTES];					
    }_WRS_PACK_ALIGN(1) BACP_GENERIC_OPTION;

typedef	struct	BACP_PACKET
	{
	PPP_HEADER				ppp_header;
	BACP_HEADER				bacp_header;
	BACP_GENERIC_OPTION		options; 
	}_WRS_PACK_ALIGN(1) BACP_PACKET;

typedef	struct	BACP_CONFIGURE_REQUEST
	{
	PPP_HEADER						header;
	BYTE_ENUM (PPP_CONTROL_CODE) 	code;
	BYTE							id;
	USHORT 							length;
    BACP_GENERIC_OPTION             options;
	} BACP_CONFIGURE_REQUEST;

typedef	struct	BACP_CONFIGURE_REQUEST_PACKET
	{
	PPP_HEADER						header;
	BYTE_ENUM (PPP_CONTROL_CODE) 	code;
	BYTE							id;
	USHORT 							length;
	UNION_BACP_OPTIONS				options;
	} BACP_CONFIGURE_REQUEST_PACKET;

typedef	struct	BACP_CONFIGURE_ACK_PACKET
	{
	PPP_HEADER			  			header;
	BYTE_ENUM (PPP_CONTROL_CODE) 	code;
	BYTE				  			id;
	USHORT 				  			length;
	UNION_BACP_OPTIONS	  			options;
	} BACP_CONFIGURE_ACK_PACKET;

typedef	struct	BACP_CONFIGURE_NAK_PACKET
	{
	PPP_HEADER			  			header;
	BYTE_ENUM (PPP_CONTROL_CODE) 	code;
	BYTE				  			id;
	USHORT 				  			length;
   	BACP_GENERIC_OPTION             options;
	}_WRS_PACK_ALIGN(1) BACP_CONFIGURE_NAK_PACKET;

typedef	struct	BACP_CONFIGURE_REJECT_PACKET
	{
	PPP_HEADER			  			header;
	BYTE_ENUM (PPP_CONTROL_CODE) 	code;
	BYTE				  			id;
	USHORT 				  			length;
   	BACP_GENERIC_OPTION             options;
	}_WRS_PACK_ALIGN(1) BACP_CONFIGURE_REJECT_PACKET;

typedef	struct	BACP_TERMINATE_REQUEST
	{
	PPP_HEADER			  			header;
	BYTE_ENUM (PPP_CONTROL_CODE) 	code;
	BYTE				  			id;
	USHORT 				  			length;
	}_WRS_PACK_ALIGN(1) BACP_TERMINATE_REQUEST;

typedef	struct	BACP_TERMINATE_ACK
	{
	PPP_HEADER			  			header;
	BYTE_ENUM (PPP_CONTROL_CODE) 	code;
	BYTE				  			id;
	USHORT 				  			length;
	}_WRS_PACK_ALIGN(1) BACP_TERMINATE_ACK;

typedef	struct	BACP_CODE_REJECT_PACKET
	{
	PPP_HEADER	  		  			header;
	BYTE_ENUM (PPP_CONTROL_CODE) 	code;
	BYTE			  	  			id;
	USHORT 		  		  			length;
	BACP_PACKET	  		  			rejected_packet;
	}_WRS_PACK_ALIGN(1) BACP_CODE_REJECT_PACKET;

#define BACP_PACKET_HEADER_SIZE (sizeof(PPP_HEADER) + sizeof(BACP_HEADER))


/******************************************************************************/

typedef	struct	PPP_BACP_STATISTICS
	{
	ULONG	number_of_tx_packets;
	ULONG	number_of_tx_bytes;
	ULONG	number_of_rx_packets;
	ULONG	number_of_rx_bytes;
	ULONG	number_of_packets_rxed_greater_than_maximum_size;
	ULONG	number_of_packets_rxed_less_than_minimum_size;
	ULONG	number_of_control_tx_packets[NUMBER_OF_PPP_CONTROL_CODES];
	ULONG	number_of_control_rx_packets[NUMBER_OF_PPP_CONTROL_CODES];
	} PPP_BACP_STATISTICS;

typedef void (*FPTR_BACP_PACKET_RECEIVED) (PFW_PLUGIN_OBJ_STATE * pluginState,
					M_BLK_ID packet);

/* bacptimer.c */

extern STATUS retry_bacp_termination_request 
			(PFW_PLUGIN_OBJ_STATE * pluginState, int arg);

extern STATUS retry_bacp_configure_request (PFW_PLUGIN_OBJ_STATE *pluginState,
											int backoffStarted);

#ifdef __cplusplus
}
#endif

#endif /* __INCvbacpstrh */
