/* mpP.h - MP header file */

/* Copyright 2002 - 2004 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01c,12aug04,ijm updated bitfields for new diab compiler, SPR#95672
01b,22oct02,ijm removed redundant MP_PROFILE_STRING_SIZE.
                Use MAX_VALUE_STRING_LENGTH defined in pfwProfile.h instead.
01a,22oct02,ijm created by breaking up target/h/ppp/mp.h into two.
*/

#ifndef __INCmpPh
#define __INCmpPh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "vxWorks.h"
#include "sllLib.h"
#include "pfw/pfw.h"
#include "pfw/pfwStack.h"
#include "pfw/pfwComponent.h"
#include "pfw/pfwLayer.h"
#include "pfw/pfwTable.h"
#include "pfw/pfwEvent.h"
#include "pfw/pfwMemory.h"
#include "pfw/pfwInterface.h"
#include "rngLib.h"
#include "private/ppp/vpppstr.h" 
#include "ppp/kppp.h"


/* defines */

#define NULL_STRING 							"" 
#define MAX_PROFILE_PARAM_LENGTH 				512 
#define NOT_A_MP_LINK							1
#define MAX_CLUSTER_SIZE						2048

#ifndef MAX_NO_OF_PPP_PORTS
#define MAX_NO_OF_PPP_PORTS 					30	
#define PPP_WITH_MAC_HEADER_LENGTH				14
#endif


#define MAXIMUM_NUMBER_OF_PENDING_FRAGMENTS 	10000

#ifndef VARIABLE_NUMBER_OF_BYTES
#define VARIABLE_NUMBER_OF_BYTES 				1
#endif

#define MAX_MRRU_LENGTH 						4
#define MP_SMALL_PACKET_LENGTH  				200 
#define NUMBER_OF_BITS_IN_A_BYTE 				8


/* Bundle Identifier structures */


typedef struct LCP_BUNDLE_PROFILE
	{
	ULONG 					localMRRU;
	ULONG 					remoteMRRU; 
	BOOL  					use_short_sequence_number_in_local_end;
	BOOL  					use_short_sequence_number_in_remote_end; 
	BYTE				local_station_discriminator [MAX_PROFILE_PARAM_LENGTH];
	BYTE				remote_station_discriminator [MAX_PROFILE_PARAM_LENGTH];
	USHORT 					localAuthProtocol; 
	USHORT 					remoteAuthProtocol;
	char					localUserName[NAME_SIZE];
	}LCP_BUNDLE_PROFILE;

typedef struct MP_BUNDLE_ASSIGNMENT_CLASS
	{
	BOOL				is_port_assigned_to_a_bundle;
	PFW_STACK_OBJ		*pManagerStackObj;
	PFW_STACK_OBJ		*pMemberStackObj; 
	BOOL				port_used_in_sending_end;
	BOOL				port_used_in_receiving_end;
	} MP_BUNDLE_ASSIGNMENT_CLASS;

typedef struct MP_BUNDLE_COUNT_TO_MANAGER_STACK_LOOKUP_CLASS
	{
	PFW_STACK_OBJ *pManagerStackObj;
	} MP_BUNDLE_COUNT_TO_MANAGER_STACK_LOOKUP_CLASS;

typedef struct MP_PORT_CLASS
	{
	ULONG				speed;
	PFW_STACK_OBJ		*pMemberStackObj;
	LCP_BUNDLE_OPTIONS  *pLcpBundleOptions;
	} MP_PORT_CLASS;

enum FRAGMENT_FLAG 
	{
	FIRST_FRAGMENT,
	LAST_FRAGMENT,
	NOT_END_FRAGMENT,
	ONLY_FRAGMENT /* it is both the first and last fragment */ 
	};

enum FLAG_TYPE
	{
	MP_OFF,
	MP_ON 
	};

/* Receiving end structures */

typedef struct MP_FRAGMENT_CLASS
	{ 
	struct MP_FRAGMENT_CLASS	*next;
	ULONG				sequence_number;
	enum FRAGMENT_FLAG	fragment_flag;
	M_BLK_ID			mblk_fragment;
	USHORT				length; 
	ULONG				indexInList;
	} MP_FRAGMENT_CLASS;

typedef struct MP_BUNDLE_RECEIVING_END_LINK_CLASS
	{ 
	PFW_STACK_OBJ			*pMemberStackObj;
	ULONG					most_recently_received_sequence_number;
	BOOL				 	link_has_been_timed;
	BOOL 					mp_is_timing_the_link;
	ULONG					clock_ticks_when_timer_starts;
	double 					delay;
	USHORT					localMRU;
	} MP_BUNDLE_RECEIVING_END_LINK_CLASS;

typedef struct MP_BUNDLE_RECEIVING_END_BUFFER_CLASS
	{
	SL_LIST				ppp_fragments;
	USHORT				number_of_ppp_fragments_buffered;
	USHORT				number_of_beginning_fragments_buffered;
	USHORT				number_of_ending_fragments_buffered;
	BOOL				receiver_is_resynchronizing;
	ULONG				M;
	BOOL				size_estimated;
	ULONG				estimated_size;
	} MP_BUNDLE_RECEIVING_END_BUFFER_CLASS;

typedef struct MP_BUNDLE_RECEIVING_END_CLASS
	{ 
	USHORT									no_of_links;
	BOOL									use_short_sequence_number;
	ULONG									localMRRU;
	MP_BUNDLE_RECEIVING_END_LINK_CLASS 		links[MAX_NO_OF_PPP_PORTS];	
	MP_BUNDLE_RECEIVING_END_BUFFER_CLASS	buffer;
	M_BLK_ID								pMblkId;
	} MP_BUNDLE_RECEIVING_END_CLASS;

/* Sending end structures */

typedef struct MP_BUNDLE_SENDING_END_LINK_CLASS
	{ 
	PFW_STACK_OBJ	   		*pMemberStackObj;
	PFW_PLUGIN_OBJ_STATE 	*pMpInterfaceLayerState; 
	PFW_PLUGIN_OBJ_STATE 	*pMpControlLayerState;	
	ULONG					remoteMru;
	} MP_BUNDLE_SENDING_END_LINK_CLASS;

typedef struct MP_BUNDLE_SENDING_END_CLASS
	{ 
	USHORT							 	no_of_links;
	BOOL				 				use_short_sequence_number;
	ULONG							 	remoteMRRU;
	MP_BUNDLE_SENDING_END_LINK_CLASS 	links [MAX_NO_OF_PPP_PORTS];	
	ULONG							 	total_bandwidth;

	double							 	bandwidth_share 
											[MAX_NO_OF_PPP_PORTS];
	double							 	mru_share 
											[MAX_NO_OF_PPP_PORTS];

	USHORT								small_packet_length;
	unsigned int 						master_short_sequence_number : 12; 
	unsigned int						master_long_sequence_number  : 24;
	USHORT								linkForNonFragmentedPacket;
	UINT								aggregateMru;
} MP_BUNDLE_SENDING_END_CLASS;

/* Bundle Class Structures */

typedef struct MP_BUNDLE_CLASS 
	{
	USHORT							no_of_links;	
	MP_PORT_CLASS					memberLinks [MAX_NO_OF_PPP_PORTS]; 
	MP_BUNDLE_RECEIVING_END_CLASS	receiving_end;
	MP_BUNDLE_SENDING_END_CLASS 	sending_end;
	MP_BUNDLE_IDENTIFIER			id;
	} MP_BUNDLE_CLASS;


typedef struct MP_LINK_INFO
	{
	PFW_STACK_OBJ	*pLinkStackObj;	
	USHORT 			local_link_discriminator;
	USHORT 			remote_link_discriminator;
	USHORT 			port_speed;
	} MP_LINK_INFO;

typedef struct MP_LINK_INFO_ITEM
	{
	struct MP_LINK_INFO_ITEM	*pNext;
	MP_LINK_INFO	mpLinkInfo;
	} MP_LINK_INFO_ITEM;

/* MP components related event data structures */

typedef struct MP_MEMBER_ADDED_EVENT_DATA
	{
	MP_LINK_INFO	*pMpLinkInfo;
	} MP_MEMBER_ADDED_EVENT_DATA;

typedef struct MP_BUNDLE_OPENED_EVENT_DATA 
	{
	ULONG			localMRRU;
	ULONG			remoteMRRU;
	} MP_BUNDLE_OPENED_EVENT_DATA;


/* MULTILINK_PACKET related structures */

typedef struct LONG_SEQUENCE_NUMBER_MP_HEADER 
	{

/*#if defined (BIG_ENDIAN)*/
#if (_BYTE_ORDER ==	_BIG_ENDIAN) /* TESTING */


	unsigned int first_fragment_flag : 1;
	unsigned int last_fragment_flag  : 1;
	unsigned int reserved_field      : 6;
	unsigned int sequence_number     : 24;

#else

	unsigned int sequence_number     : 24;
	unsigned int reserved_field      : 6;
	unsigned int last_fragment_flag  : 1;
	unsigned int first_fragment_flag : 1;

#endif
	}_WRS_PACK_ALIGN(1) LONG_SEQUENCE_NUMBER_MP_HEADER;

typedef struct SHORT_SEQUENCE_NUMBER_MP_HEADER
	{
/*#if defined (BIG_ENDIAN)*/
#if (_BYTE_ORDER ==	_BIG_ENDIAN) /* TESTING */

	unsigned int first_fragment_flag : 1;
	unsigned int last_fragment_flag	 : 1;
	unsigned int reserved_field      : 2;
	unsigned int sequence_number     : 12;

#else
	unsigned int sequence_number     : 12;
	unsigned int reserved_field      : 2;
	unsigned int last_fragment_flag	 : 1;
	unsigned int first_fragment_flag : 1;

#endif
	}_WRS_PACK_ALIGN(1)  SHORT_SEQUENCE_NUMBER_MP_HEADER;

typedef struct MP_PACKET_WITH_LONG_SEQUENCE_NUMBER
	{
	LONG_SEQUENCE_NUMBER_MP_HEADER	header;
	BYTE							data [VARIABLE_NUMBER_OF_BYTES];
	} __attribute__((__packed__))
	MP_PACKET_WITH_LONG_SEQUENCE_NUMBER;

typedef struct MP_PACKET_WITH_SHORT_SEQUENCE_NUMBER
	{
	SHORT_SEQUENCE_NUMBER_MP_HEADER		header;
	BYTE								data [VARIABLE_NUMBER_OF_BYTES];
	}__attribute__((__packed__))
	MP_PACKET_WITH_SHORT_SEQUENCE_NUMBER;

typedef union UNION_MP_PACKET
	{
	MP_PACKET_WITH_LONG_SEQUENCE_NUMBER	 mp_packet_with_long_sequence_number;
	MP_PACKET_WITH_SHORT_SEQUENCE_NUMBER mp_packet_with_short_sequence_number;
	} UNION_MP_PACKET;

typedef struct PPP_MP_PACKET
	{
	PPP_HEADER header;
	char protocol_data[VARIABLE_NUMBER_OF_BYTES];
	}__attribute__((__packed__))
	PPP_MP_PACKET;


/* Miscellaneous structures */

typedef struct mpJobRingItem
	{
   	UINT size;
   	FUNCPTR  jobHandlerFunc;
	} MP_JOB_RING_ITEM;

/* MP Send Queue interface structure */

typedef struct mpSendInterface 
    {
    PFW_PLUGIN_OBJ_STATE *state;
    M_BLK_ID packet;
    }MP_SEND_INTERFACE;


typedef struct		
	{
	PFW_PLUGIN_OBJ_STATE 	* pMemberState;
	M_BLK_ID				 pPacket; 
	}MP_RECEIVE_DATA;

typedef enum  
	{
	LOCAL_END_AUTH_PHASE  =	0x00,
	REMOTE_END_AUTH_PHASE =	0x01
	}AUTH_PHASE; 


#ifdef __cplusplus
}
#endif

#endif /* __INCmpPh */

 
