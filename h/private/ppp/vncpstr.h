/* vncpstr.h - NCP structure definitions */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01d,07jun02,vk  moved FPTR_NCP_PACKET_RECEIVED function declaration 
                to pppNcpLibP.h 
01c,20dec01,mk  added macros _WRS_PACK_ALIGN(x)
01b,07dec01,mk  added macros _WRS_PACK_ALIGN(x),to fix alignment
                problems for PPP for arm,
01a,130ct99,sgv derived from routerware source base
*/

/*
*$Log:: /Rtrware/devdrvrs/ppp/vncpstr $
 * 
 * 7     10/01/98 11:43a Alex
 * Updated the PPP source code to conform
 * to a single build. 
 * 
 * 6     4/30/98 3:03p Alex
 * Ppp v4.2.0 check in
 * 
 * 1     4/24/98 12:10a Release Engineer
 * code cleanup, code style changes,
 * linted, system level test
 * PPP v4.2.0
*/

#ifndef __INCvncpstrh
#define __INCvncpstrh

#include "ppp/kstart.h"
#include "ppp/kppp.h"


typedef	struct	NCP_HEADER
    {
    BYTE_ENUM (PPP_CONTROL_CODE) 	code;

    BYTE				id;
    USHORT 				length;
} _WRS_PACK_ALIGN(1)  NCP_HEADER;

/* This is declared in vlcpstr.h */
#if 0
typedef	struct	GENERIC_OPTION
{
	BYTE	type;
	BYTE	length;

	BYTE	data[VARIABLE_NUMBER_OF_BYTES];					
} GENERIC_OPTION;
#endif

typedef	struct	NCP_PACKET
{
	PPP_HEADER	ppp_header;
	NCP_HEADER	ncp_header;													  
} _WRS_PACK_ALIGN(1)  NCP_PACKET;

#define NCP_PACKET_HEADER_SIZE (sizeof(PPP_HEADER) + sizeof(NCP_HEADER))

typedef	struct	NCP_CONFIGURE_REQUEST
{
	PPP_HEADER		ppp_header;

	NCP_HEADER		ncp_header;

	GENERIC_OPTION	options;
} _WRS_PACK_ALIGN(1)  NCP_CONFIGURE_REQUEST;

typedef	struct	NCP_CONFIGURE_ACK
{
	PPP_HEADER	ppp_header;

	NCP_HEADER	ncp_header;													  
} _WRS_PACK_ALIGN(1)  NCP_CONFIGURE_ACK;

typedef	struct	NCP_CONFIGURE_NAK
{
	PPP_HEADER		ppp_header;

	NCP_HEADER		ncp_header;
	GENERIC_OPTION	options;
} _WRS_PACK_ALIGN(1)  NCP_CONFIGURE_NAK;

typedef	struct	NCP_CONFIGURE_REJECT
{
	PPP_HEADER		ppp_header;

	NCP_HEADER		ncp_header;													  
	GENERIC_OPTION	options;
} _WRS_PACK_ALIGN(1)  NCP_CONFIGURE_REJECT;

typedef	struct	NCP_TERMINATE_REQUEST
{
	PPP_HEADER	ppp_header;

	NCP_HEADER	ncp_header;													  
} _WRS_PACK_ALIGN(1)  NCP_TERMINATE_REQUEST;

typedef	struct	NCP_TERMINATE_ACK
{
	PPP_HEADER	ppp_header;

	NCP_HEADER	ncp_header;													  
} _WRS_PACK_ALIGN(1)  NCP_TERMINATE_ACK;

typedef	struct	NCP_CODE_REJECT_PACKET
{
	PPP_HEADER	ppp_header;

	NCP_HEADER	ncp_header;													  
	NCP_PACKET  rejected_packet;
} _WRS_PACK_ALIGN(1)  NCP_CODE_REJECT_PACKET;

typedef	struct	NCP_PACKET_WITH_OPTIONS
{
	PPP_HEADER		ppp_header;

	NCP_HEADER		ncp_header;

	GENERIC_OPTION	options;
} _WRS_PACK_ALIGN(1)  NCP_PACKET_WITH_OPTIONS;

typedef	union	UNION_NCP_GENERIC_PACKET
{
	NCP_CONFIGURE_REQUEST		configure_request;
	NCP_CONFIGURE_ACK				configure_ack;
	NCP_CONFIGURE_NAK				configure_nak;
	NCP_CONFIGURE_REJECT			configure_reject;
	NCP_TERMINATE_REQUEST		terminate_request;
	NCP_TERMINATE_ACK				terminate_ack;
	NCP_CODE_REJECT_PACKET		reject;
	NCP_PACKET_WITH_OPTIONS		packet_with_options;
} _WRS_PACK_ALIGN(1) UNION_NCP_GENERIC_PACKET;

typedef	struct	PPP_NCP_STATISTICS
{
	ULONG	number_of_tx_packets;
	ULONG	number_of_tx_bytes;

	ULONG	number_of_rx_packets;
	ULONG	number_of_rx_bytes;

	ULONG	number_of_packets_rxed_greater_than_maximum_size;
	ULONG	number_of_packets_rxed_less_than_minimum_size;
	ULONG	number_of_packets_rxed_in_down_port;

	ULONG	number_of_tx_control_packets[NUMBER_OF_PPP_CONTROL_CODES];
	ULONG	number_of_rx_control_packets[NUMBER_OF_PPP_CONTROL_CODES];

} PPP_NCP_STATISTICS;

typedef	union	UNION_NCP_OPTIONS
{
	IPCP_OPTIONS		ip;
#if 0
	IPXCP_OPTIONS		ipx;
	ATCP_OPTIONS		appletalk;
#endif
} UNION_NCP_OPTIONS;

typedef	union	UNION_NCP_OPTION_TYPES
{
	BYTE_ENUM (IPCP_OPTION_TYPE) 	ipcp;
#if 0
	BYTE_ENUM (ATCP_OPTION_TYPE) 	atcp;
	BYTE_ENUM (IPXCP_OPTION_TYPE) ipxcp;
#endif
	BYTE									generic;
} UNION_NCP_OPTION_TYPES;

typedef	union	UNION_NCP_PACKET
{
	UNION_IPCP_PACKET				ip;
#if 0
	UNION_IPXCP_PACKET			ipx;
	UNION_ATCP_PACKET				appletalk;
#endif
	UNION_NCP_GENERIC_PACKET	generic;
} UNION_NCP_PACKET;

typedef struct VJC_OPTION
{
	USHORT	vjc_code;
	BYTE		maximum_slot_id;
	BYTE		compression_slot_id;
}  _WRS_PACK_ALIGN(1) VJC_OPTION;



#endif /* __INCvncpstrh */

