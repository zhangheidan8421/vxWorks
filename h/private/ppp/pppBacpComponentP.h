/* pppBacpComponentP.h - BACP component private header file */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01b,11feb02,ak	code changes as part of memory reduction process
01a,01feb01,sd 	created
*/

#ifndef __INCpppBacpComponentPh
#define __INCpppBacpComponentPh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "tickLib.h"
#include "logLib.h"
#include "errnoLib.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "ctype.h"
#include "memLib.h"
#include "netBufLib.h"
#include "pfw/pfw.h"
#include "pfw/pfwStack.h"
#include "pfw/pfwProfile.h"
#include "pfw/pfwComponent.h"
#include "pfw/pfwLayer.h"
#include "pfw/pfwEvent.h"
#include "pfw/pfwTimer.h"
#include "pfw/pfwMemory.h"
#include "pfw/pfwInterface.h"
#include "private/ppp/vpppstr.h"
#include "private/ppp/vbacpstr.h"
#include "private/ppp/vbapstr.h"
#include "private/ppp/bacpstate.h"
#include "ppp/interfaces/pppControlProtocolInterface.h"
#include "ppp/interfaces/pppControlLayerInterface.h"
#include "ppp/interfaces/lcpInterfaces.h"
#include "ppp/interfaces/componentAcceptableProtocolsInterface.h"
#include "ppp/interfaces/mpFramingLayerInterfaces.h"
#include "ppp/bacpInterfaces.h"
#include "private/ppp/vbacputil.h"

typedef struct bacpOption
	{
    char                       name[PFW_MAX_NAME_LENGTH];
    BYTE_ENUM(BACP_OPTION_TYPE) optionType;
    char                       optionTypeString[16];
	} BACP_CONFIG_OPTION; 

/* Component data per instance; one per pfw */

typedef struct  pppBacpComponent
	{
	PFW_COMPONENT_OBJ					component;
	FPTR_BACP_PACKET_RECEIVED 			* fptr_bacp_packet_received;	
	CONTROL_PROTOCOL_INTERFACE			bacpControlInterface;
	COMPONENT_ACCEPTABLE_PROTOCOLS_INTERFACE	
				bacpComponentAcceptableProtocolsInterface;
	FPTR_BAP_PACKET_RECEIVED			* fptr_bap_packet_received; 
	BACP_CONFIG_OPTION   				option[NUMBER_OF_BACP_OPTIONS + 1];
	} PPP_BACP_COMPONENT;

/* BACP Component configuration option */

typedef struct LINK_QUALITY_COUNTERS
    {
    ULONG OutLQRs;
    ULONG InLQRs;
    ULONG InGoodOctets;
    ULONG ifInOctets;
    ULONG ifInErrors;
    ULONG ifInDiscards;
    ULONG ifOutOctets;
    ULONG ifOutUniPackets;
    ULONG ifInUniPackets;
    } LINK_QUALITY_COUNTERS;

typedef struct alternateStrings
	{
	struct	alternateStrings * next;
	UINT32	preference;	/* key for this ordered list */
	char	configString[1];
	}ALTERNATE_OPTION_STRING;

typedef struct bacpConfigStrings
	{
	char						*configString;
	ALTERNATE_OPTION_STRING		*localAlternateConfigString;
	ALTERNATE_OPTION_STRING		*remoteAlternateConfigString;
	char						*remoteConfigString;
	} BACP_CONFIG_STRINGS;


/* component profile data structure */

typedef struct bacpProfileData
	{
	BACP_CONFIG_STRINGS *		bacpConfigStrings; 
	ULONG   		maximum_number_of_bacp_configuration_requests;
	ULONG    		maximum_number_of_bacp_termination_requests;
	ULONG    		maximum_number_of_configuration_failures;
	ULONG    		maximum_bacp_configuration_request_send_interval;
	ULONG    		maximum_bacp_termination_request_send_interval;
	ULONG    		maximum_bacp_configuration_request_backoff_interval;
	ULONG    		maximum_number_of_bap_call_requests;
	ULONG    		maximum_bap_call_request_send_interval;
	ULONG    		maximum_number_of_bap_callback_requests;
	ULONG    		maximum_bap_callback_request_send_interval;
	ULONG    		maximum_number_of_bap_linkdrop_requests;
	ULONG    		maximum_bap_linkdrop_request_send_interval;
	ULONG    		maximum_number_of_bap_status_indications;
	ULONG    		maximum_bap_status_indication_send_interval;
	struct bacpStackData *       stackData;
	}BACP_PROFILE_DATA;


/* component stack data structure */

typedef struct bacpStackData
	{ 
	BACP_CONTROL_PROTOCOL_STATE_DATA	stateData;	
	NET_POOL_ID							netPoolId; 
	PPP_CONTROL_LAYER_INTERFACE  		* controlLayerInterface;
	LCP_PACKET_SEND_INTERFACE			*lcpPacketSendInterface;
	PFW_INTERFACE_STATE_PAIR			mpBundleManagementInterface;
	PFW_EVENT_OBJ						* bacpUpEventObj;
	PFW_EVENT_OBJ						* bacpDownEventObj;
	PFW_EVENT_OBJ						* mpLinkUpEventObj;
	PFW_EVENT_OBJ						* mpLinkDownEventObj;
	PFW_EVENT_OBJ						* mpBundleCloseEventObj;
	PFW_EVENT_OBJ						* mpBundleOpenEventObj;
	PFW_TIMER_OBJ						* retryTimer;
 	PFW_TIMER_OBJ						* callRequestRetryTimer;
 	PFW_TIMER_OBJ						* callbackRequestRetryTimer; 
	PFW_TIMER_OBJ						* indicationRetryTimer; 
	PFW_TIMER_OBJ						* dropRequestRetryTimer; 
	BYTE_ENUM (BOOLEAN)					favored_peer_is_me;
	ULONG								local_favored_peer_id;
	ULONG								remote_favored_peer_id;
	ULONG								bundleLocalMRRU;
	ULONG								bundleRemoteMRRU;
	OPTION_LISTS						option_lists;
	LINK_QUALITY_COUNTERS				link_quality_counters;
	BYTE								id_sequence_number;  
												/* BACP sequence number */
	BYTE								last_id_of_bacp_packet_sent;
	BYTE								last_bap_id; 	
												/* BAP sequence number */
	BYTE								last_remote_bap_id;	 
	USHORT								number_of_configuration_requests;
	USHORT								number_of_termination_requests;
	USHORT								number_of_configuration_naks; 
	M_BLK_ID					last_txed_bacp_configuration_request_packet;
	USHORT						length_of_last_txed_bacp_packet;  
	M_BLK_ID					last_rxed_bacp_configuration_request_packet;
    M_BLK_ID                    last_rxed_bacp_termination_request_packet;     
	USHORT						number_of_bap_call_requests;
	USHORT						number_of_bap_callback_requests;
	USHORT						number_of_bap_linkdrop_requests;
	USHORT						number_of_bap_status_indications;
	PFW_PLUGIN_OBJ_CALLBACKS	* callbacks;
	BACP_UPCALL_FUNCTIONS		* bacpUpcall; 
	BAP_ACTIVE_PORT_LINK		active_port_queue;
	BAP_BUFFER_LINK				call_request_send_queue;
	BAP_BUFFER_LINK				callback_request_send_queue;
	BAP_BUFFER_LINK				call_indication_send_queue;
	BAP_BUFFER_LINK				link_drop_request_send_queue;
	BOOLEAN						configuration_request_backoff_period_started;
	PPP_BACP_STATISTICS			bacp_statistics; 
	PPP_BAP_STATISTICS			bap_statistics;
	} BACP_STACK_DATA;


typedef struct MEMBER_LINK_INFO
	{
	PFW_STACK_OBJ	*pLinkStackObj;	
	USHORT 			local_link_discriminator;
	USHORT 			remote_link_discriminator;
	USHORT 			port_speed;
	} MEMBER_LINK_INFO;

typedef struct MEMBER_LINK_INFO_ITEM
	{
	struct MEMBER_LINK_INFO_ITEM	*pNext;
	MEMBER_LINK_INFO	mpLinkInfo;
	} MEMBER_LINK_INFO_ITEM;

typedef struct MP_BUNDLE_INFO 
	{
	ULONG			localMRRU;
	ULONG			remoteMRRU;
	} MP_BUNDLE_INFO;

/* pppbacprx.c */

void bacpSetReceiveFunctions ( PPP_BACP_COMPONENT * bacpComponent);

/* pppbaprx.c */

void bapSetReceiveFunctions ( PPP_BACP_COMPONENT * bacpComponent);

/* pppbacptx.c */

void bacpSetSendActionFunctions ( STATE_MACHINE_ACTION_TABLE * actionTable);


#ifdef __cplusplus
}
#endif

#endif /* __INCpppBacpComponentPh */

