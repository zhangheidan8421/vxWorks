/* pppoptn.h - option processor header file */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01g,15jul02,emr  This code was developed on the windnet_ppp2_0-ipv6 branch and
                 should have been developed on the windnet_ppp2_0 branch, so
                 this brings it into line.
01h,08jul02,as  added option_name parameter to add_new_ppp_option_to_list
01g,22may02,as  added definition for generateNewInterfaceIdentifier for IPV6 support
01f,04feb02,vk  modified function prototypes to remove VALUE_TYPE structures
01e,04feb02,jr  modified function prototypes to remove VALUE_TYPE structures 
01d,23feb00,sj  removed extra ';' in declaratio of store_ppp_options_in_packet
01c,26jan00,sj  added changeOptionValueInConfiguredList and 
              	free_ppp_option_list_entry 
01b,10nov99,sj  added generate_option_list_entry
01a,01oct99,sj 	created
*/

#include "ppp/kstart.h"
#include "ppp/kppp.h"
#include "private/ppp/vpppstr.h"
#include "private/ppp/listutls.h"
#include "pfw/pfw.h"

#ifndef __INCpppoptnh
#define __INCpppoptnh

#ifdef __cplusplus
extern "C" {
#endif

/* pppoptn.c */

extern void add_configuration_option (PFW_OBJ *,OPTION_LIST *sptr_option_list,
		OPTION_STATE state, BYTE option_type,BYTE length,
		void *vptr_option_data,char *cptr_option_name,
		char *cptr_ncp_name, BOOLEAN swap_value,
		BOOLEAN negotiation_required,
		BOOLEAN	negotiable,
		BOOLEAN automatic_nak_processing); 

extern void remove_configuration_option (OPTION_LIST *sptr_option_list,
							    BYTE option_type);
extern void replace_configuration_option (OPTION_LIST *sptr_option_list,
		OPTION_LIST_ENTRY *sptr_option_to_replace, OPTION_STATE state);

extern void free_ppp_option_lists (OPTION_LISTS *sptr_option_lists);

extern void add_alternate_to_configuration_option ( PFW_OBJ * pfw,
		OPTION_LIST *sptr_option_list,BYTE option_type,
		BYTE length,void *vptr_option_data,
		char *cptr_option_name,char *cptr_ncp_name,
		BOOLEAN swap_value); 

extern void add_configuration_option_range (PFW_OBJ *,
		OPTION_LIST *sptr_option_list,
		BYTE option_type,BYTE length,void *vptr_option_data,
		char *cptr_option_name,char *cptr_ncp_name,BOOLEAN swap_value 
		); 

extern TEST add_new_ppp_option_to_list (PFW_OBJ *,OPTION_LIST *sptr_option_list,
		OPTION_STATE state,BYTE option_type,char * option_name,BYTE length, /* Updated */
		void *vptr_option_data,BOOLEAN negotiation_required,
		BOOLEAN negotiable,BOOLEAN automatic_nak_processing);

extern STATUS changeOptionValueInConfiguredList ( PFW_OBJ *, OPTION_LIST *,
			  BYTE optionType, int newDataLength, void * newData);

extern OPTION_LIST_ENTRY *duplicate_option (PFW_OBJ *,
		OPTION_LIST_ENTRY *sptr_to_option_to_copy);

extern TEST copy_option_from_entry (OPTION_LIST_ENTRY *sptr_matching_option,
		void *vptr_destination,USHORT maximum_size);

extern TEST copy_data_to_option (OPTION_LIST_ENTRY *sptr_matching_option,
		void *vptr_source,USHORT size);

extern void delete_entry_from_option_list (OPTION_LIST *sptr_option_list,
		OPTION_LIST_ENTRY *sptr_option_to_delete);

extern void free_ppp_option_list (OPTION_LIST *sptr_option_list);
extern void free_ppp_option_list_entry ( OPTION_LIST_ENTRY *sptr_source_option);
extern void free_ppp_tx_accepted_option_list (OPTION_LIST *sptr_option_list);
extern BOOLEAN is_option_present (OPTION_LIST *sptr_option_list,
		BYTE option_type);

extern void replace_data_field_in_option (PFW_OBJ *,
		OPTION_LIST_ENTRY *sptr_option,
		UNION_OPTION_DATA_TYPES *uptr_new_data,BYTE new_length);

extern OPTION_LIST_ENTRY *find_matching_option (OPTION_LIST *sptr_option_list,
		BYTE type);

extern USHORT get_size_of_ppp_options (OPTION_LIST *sptr_option_list);

extern void store_ppp_options_in_packet (OPTION_LIST *sptr_option_list,
						PPP_OPTION *sptr_tx_option);

extern TEST copy_option (OPTION_LIST *sptr_option_list,BYTE option_type,
		void *vptr_destination,USHORT maximum_size);

extern OPTION_PARSE_RESULT parse_ppp_options_from_configure_request (
		PFW_OBJ *,
		OPTION_LISTS *sptr_option_lists, M_BLK_ID packet,
		PFW_PLUGIN_OBJ_STATE *pluginState);

extern OPTION_PARSE_RESULT parse_ppp_options_from_nak_configure (
		PFW_OBJ *,
		OPTION_LISTS *sptr_option_lists, M_BLK_ID packet,
		PFW_PLUGIN_OBJ_STATE *pluginState);

extern OPTION_PARSE_RESULT parse_ppp_options_from_reject_configure (
		PFW_OBJ *,
		OPTION_LISTS *sptr_option_lists,M_BLK_ID packet);

/* pppoptna.c */

extern void copy_configuration_options_list (PFW_OBJ *,
				OPTION_LIST * from_list,OPTION_LIST * to_list);

extern void copy_configuration_options_to_tx_accepted_options (PFW_OBJ *,
		OPTION_LISTS *sptr_option_lists);

extern TEST insertAlternateOptionToList ( PFW_OBJ *, OPTION_LIST_ENTRY *,
					BYTE length, void *vptr_option_data,
					ALTERNATE_OPTION *previousOption);

extern TEST add_option_to_alternate_list (PFW_OBJ *,
				OPTION_LIST *sptr_option_list,
				BYTE option_type,BYTE length,
				void *vptr_option_data
                ); 
extern char * getIpcpOptionName (PFW_OBJ * pfw, char * optionName);
extern char * getLcpOptionName (PFW_OBJ * pfw, char * optionName);
/* Updated */


/*	pppoptnp.c */

extern TEST ppp_configure_request_option_processor (
		OPTION_LIST_ENTRY *sptr_remote_option,
		OPTION_LISTS      *sptr_option_lists,
		PFW_PLUGIN_OBJ_STATE * pluginState);

extern TEST ppp_configure_nak_option_processor (
		OPTION_LIST_ENTRY *sptr_remote_option,
		OPTION_LISTS      *sptr_option_lists,
		PFW_PLUGIN_OBJ_STATE * pluginState);

extern OPTION_LIST_ENTRY *option_value_match (
		OPTION_LIST_ENTRY *sptr_local_option,
		OPTION_LIST_ENTRY *sptr_remote_option);

extern ALTERNATE_OPTION *alternate_option_value_match (
		OPTION_LIST_ENTRY *sptr_local_option,
		OPTION_LIST_ENTRY *sptr_remote_option);

extern BOOLEAN range_option_value_match (OPTION_LIST_ENTRY *sptr_local_option,
		OPTION_LIST_ENTRY *sptr_remote_option);

/* IPV6 Support */
extern void generateNewInterfaceIdentifier (OPTION_LIST_ENTRY * sptr_remote_option);
/* IPV6 Support */

/* pppconfg.c */

extern STATUS generate_option_list_entry (PFW_OBJ * pfw,
						OPTION_LISTS *sptr_option_lists,
						char *value_type_string,
						char *value_string,
						BYTE   option_number,
						char * protocol_name,
						char * type_string);

extern void get_option_value (char *value_type_string,char *value_string,
		BYTE *bptr_return_data, BYTE *bptr_return_length,
		BOOLEAN *eptr_return_value_swapped);

extern char *copy_parameter (char *cptr_parameter,char *cptr_option,
		char **pHolder, BYTE *bptr_number_of_parameters_processed);

extern ULONG get_ppp_configuration_table_address (void);

extern void add_ppp_option (PFW_OBJ * pfw,char *option_string,
		OPTION_LISTS *sptr_option_lists);
extern void add_remote_ppp_option(PFW_OBJ *,char *option_string,
		OPTION_LISTS *sptr_option_lists);

/* pppcfgar.c */

extern TEST get_alternate_option_values (char *cptr_option_string,
		char *cptr_return_ncp_name,char *cptr_return_option_name,
		BYTE *bptr_return_option_number,BYTE *bptr_return_data,
		BYTE *bptr_return_length,BOOLEAN *eptr_return_value_swapped);

extern TEST get_range_option_values (char *cptr_option_string,
		char *cptr_return_ncp_name,char *cptr_return_option_name,
		BYTE *bptr_return_option_number,BYTE *bptr_return_data,
		BYTE *bptr_return_length,BOOLEAN *eptr_return_value_swapped);

extern void parse_option_value (PFW_OBJ *,char *cptr_value,
		BYTE *bptr_option_data,
		BYTE *bptr_length_of_all_options,
		BOOLEAN *eptr_return_value_swapped);

#ifdef __cplusplus
}
#endif

#endif /* __INCpppoptnh */

