/* pppNcpLibP.h - header file for common functions for PPP 
                             Network control protocol(IPCP and IPV6CP) */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01b,22may02,vk  declared the files used in pppNcpComponent.c 
01a,17may02,as  derived from WRS header base of NCP component header file
*/

#ifndef __INCpppNcpLibPh
#define __INCpppNcpLibPh

#include "vxWorks.h"
#include "logLib.h"
#include "errnoLib.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "ctype.h"
#include "memLib.h"
#include "inetLib.h"
#include "netBufLib.h"
#include "pfw/pfw.h"
#include "pfw/pfwStack.h"
#include "pfw/pfwProfile.h"
#include "pfw/pfwComponent.h"
#include "pfw/pfwLayer.h"
#include "pfw/pfwEvent.h"
#include "pfw/pfwTimer.h"
#include "ppp/kstart.h"
#include "ppp/kppp.h"
#include "private/ppp/pppstate.h"
#include "private/ppp/vnipstr.h"
#include "private/ppp/vpppstr.h"
#include "private/ppp/vlcpstr.h"
#include "private/ppp/vncpstr.h"
#include "ppp/interfaces/pppControlLayerInterface.h"
#include "ppp/interfaces/pppControlProtocolInterface.h"


typedef void (*FPTR_NCP_PACKET_RECEIVED)			
    (
    PFW_PLUGIN_OBJ_STATE *         pPluginState,
    M_BLK_ID                       pMblk
    );

typedef struct pppNcpComponent
    {
    PFW_COMPONENT_OBJ      pfwComponentObj;
    FPTR_NCP_PACKET_RECEIVED
                  fptr_ncp_packet_received[NUMBER_OF_PPP_CONTROL_CODES];

    } PPP_NCP_COMPONENT;

typedef struct ncpStackData
    {
    PPP_CONTROL_PROTOCOL_STATE_DATA    stateData;
    OPTION_LISTS                       option_lists;
    PPP_CONTROL_LAYER_INTERFACE *      pControlLayerInterface;
    PFW_PLUGIN_OBJ_CALLBACKS           * callbacks;

    PFW_TIMER_OBJ *        ncp_timer;
    PFW_EVENT_OBJ *        pNcpUpEventObj;
    PFW_EVENT_OBJ *        pNcpDownEventObj;

    BYTE                   state;
    BYTE                   old_state;

    BYTE                   last_id_of_ncp_packet_sent;
    BYTE                   last_request_id;
    BYTE                   id_sequence_number;

    USHORT                 ncp_protocol;

    int                    length_of_last_txed_request;
    BYTE                   last_rxed_ncp_id_sequence_number;
    PPP_NCP_STATISTICS     statistics;

    M_BLK_ID               retry_ncp_configuration_request_packet;
    M_BLK_ID               last_txed_ncp_configuration_request_packet;
    M_BLK_ID               last_rxed_ncp_configuration_request_packet;

    BOOL                   configuration_request_backoff_period_started;
    USHORT                 number_of_ncp_configuration_requests;
    USHORT                 number_of_ncp_termination_requests;
    USHORT                 number_of_ncp_configuration_naks;
    USHORT                 configuration_request_backoff_interval;
    USHORT                 configuration_request_send_interval;
    USHORT                 termination_request_send_interval;
    } NCP_STACK_DATA;


typedef struct ncpProfileData
    {
    ULONG	               maximum_number_of_configuration_requests;
    ULONG	               maximum_number_of_termination_requests;
    ULONG	               maximum_configuration_request_send_interval;
    ULONG	               maximum_termination_request_send_interval;
    ULONG	               maximum_configuration_request_backoff_interval;
    ULONG	               maximum_number_of_configuration_failures;
    } NCP_PROFILE_DATA;


typedef struct hcConfirurationOption
    {
    USHORT  ipCompressionProtocol;
    USHORT  tcpSpace;
    USHORT  nonTcpSpace;
    USHORT  fMaxPeriod;
    USHORT  fMaxTime;
    USHORT  maxHeader;
    BYTE    rtCompressionType;
    BYTE    rtCompressionLength;
    }_WRS_PACK_ALIGN(1) HC_CONFIGURATION_OPTION;  



STATUS ipcp_maxTerminationRequests 
    (
    PFW_OBJ *                         pPfw,
    PFW_PARAMETER_OPERATION_TYPE      type,
    void *                            pProfileData, 
    char *                            pValue
    );

STATUS ipcp_maxConfigRequests 
    (
    PFW_OBJ *                         pPfw,
    PFW_PARAMETER_OPERATION_TYPE      type,
    void *                            pProfileData, 
    char *                            pValue
    );

int ipcp_configReqSendInterval 
    (
    PFW_OBJ *                         pPfw,
    PFW_PARAMETER_OPERATION_TYPE      type,
    void *                            pProfileData, 
    char *                            pValue
    );

int ipcp_TerminationReqInterval 
    (
    PFW_OBJ *                         pPfw,
    PFW_PARAMETER_OPERATION_TYPE      type,
    void *                            pProfileData, 
    char *                            pValue
    );


/* IPCP state functions */
void ipcp_initialize_restart_counter
    (
    PFW_PLUGIN_OBJ_STATE *            pPluginObjState,
    M_BLK_ID                          pMblk, 
    PPP_STATE                         end_state
    );

void ipcp_zero_restart_counter
    (
    PFW_PLUGIN_OBJ_STATE *            pPluginObjState,
    M_BLK_ID                          pMblk, 
    PPP_STATE                         end_state
    );

PPP_CONTROL_PHASE ipcp_pppPhaseGet ();

PPP_STATE ipcp_pppStateGet 
    (
    PFW_PLUGIN_OBJ_STATE *            pState
    );

/* IPCP timer handlers */

STATUS ipcp_periodic_timer 
    (
    PFW_PLUGIN_OBJ_STATE *            pPluginState, 
    int                               arg
    );

void periodic_ipcp_configure_or_termination_request 
    (
    PFW_PLUGIN_OBJ_STATE *            pPluginState, 
    UINT32                            arg
    );

STATUS retry_ipcp_configure_request 
    (
    PFW_PLUGIN_OBJ_STATE *            pPluginState,
    int                               backoffStarted
    );

STATUS retry_ipcp_termination_request 
    (
    PFW_PLUGIN_OBJ_STATE *            pPluginState,
    int                               arg
    );
TEST ncp_packet_received 
    (
    PFW_PLUGIN_OBJ_STATE *            pPluginState, 
    M_BLK_ID                          packet
    );


#endif /* __INCpppNcpLibPh */

