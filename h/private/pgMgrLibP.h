/* pgMgrLibP.h - page manager library private header */

/* 
 * Copyright (c) 1999-2005 Wind River Systems, Inc. 
 *
 *
 * The right to copy, distribute, modify or otherwise make use 
 * of this software may be licensed only pursuant to the terms 
 * of an applicable Wind River license agreement. 
 */ 

/*
modification history
--------------------
01g,03jun05,yvp  Added #ifndef	_ASMLANGUAGE. 
                 Updated copyright. #include now with angle-brackets.
01f,24jan05,aeg  added pgMgrPrivateUnmap() function prototype (SPR #106381).
01e,13sep04,zl   cleaned up MM private API
01d,10sep04,gls  cleanup
01c,08jul04,tam  changed PAGE_MGR_ATTR_ALLOC_CONTIG_DEFAULT to be 
		 PAGE_MGR_ATTR_ALLOC_CONTIG
01b,19may04,tam  moved definition from pgMgrLib.h
01a,22oct03,gls  proted from AE version 01b
*/

#ifndef __INCpgMgrLibPh
#define __INCpgMgrLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include <vxWorks.h>
#include <private/vmLibP.h>
#include <private/pgPoolLibP.h>


/* status codes */
/* XXX temporary */

#include <vwModNum.h>


#define S_pgMgrLib_NOT_IMPLEMENTED		        (M_pgMgrLib |  1)
#define S_pgMgrLib_MMU_ATTR_NOT_SPECIFIED		(M_pgMgrLib |  2)
#define S_pgMgrLib_VIRT_ADDR_OUT_OF_RANGE		(M_pgMgrLib |  3)
#define S_pgMgrLib_VIRT_PAGES_NOT_AVAILABLE		(M_pgMgrLib |  4)
#define S_pgMgrLib_CONTIG_PHYS_PAGES_NOT_AVAILABLE	(M_pgMgrLib |  5)
#define S_pgMgrLib_PHYS_PAGES_NOT_AVAILABLE		(M_pgMgrLib |  6)
#define S_pgMgrLib_VIRT_PAGE_COUNT_IS_NULL		(M_pgMgrLib |  7)
#define S_pgMgrLib_PHYS_PAGE_POOL_NO_MATCH		(M_pgMgrLib |  8)
#define S_pgMgrLib_VIRT_PAGES_NOT_ALLOCATED		(M_pgMgrLib |  9)
#define S_pgMgrLib_PHYS_PAGE_POOL_NOT_AVAILABLE		(M_pgMgrLib | 10)
#define	S_pgMgrLib_ADDR_NOT_ALIGNED			(M_pgMgrLib | 11)
#define S_pgMgrLib_INVALID_PAGE_POOL			(M_pgMgrLib | 12)
#define	S_pgMgrLib_INVALID_MMU_ATTR			(M_pgMgrLib | 13)
#define S_pgMgrLib_INVALID_ALLOC_ATTR			(M_pgMgrLib | 14)
#define S_pgMgrLib_NON_1_1_MAPPINGS_NOT_AVAILABLE	(M_pgMgrLib | 15)

/* defines */
    
#define	PAGE_MGR_OPTS_USR_TEXT	(PAGE_MGR_ATTR_ALLOC_NONCONTIG	|       \
                                 PAGE_MGR_ATTR_ALLOC_MAPPED	|       \
                                 MMU_ATTR_USR_TEXT		|       \
				 MMU_ATTR_SUP_RW)

#define	PAGE_MGR_OPTS_USR_DATA	(PAGE_MGR_ATTR_ALLOC_CONTIG	|       \
                                 PAGE_MGR_ATTR_ALLOC_MAPPED	|       \
                                 MMU_ATTR_USR_DATA		|       \
				 MMU_ATTR_SUP_RW)

#define	PAGE_MGR_OPTS_USR_DATA_RO                                       \
    				(PAGE_MGR_ATTR_ALLOC_CONTIG	|       \
                                 PAGE_MGR_ATTR_ALLOC_MAPPED	|       \
                                 MMU_ATTR_USR_DATA_RO		|       \
				 MMU_ATTR_SUP_RW)

#define	PAGE_MGR_OPTS_SUP_TEXT	(PAGE_MGR_ATTR_ALLOC_CONTIG	|       \
                                 PAGE_MGR_ATTR_ALLOC_MAPPED	|       \
                                 MMU_ATTR_SUP_TEXT)

#define	PAGE_MGR_OPTS_SUP_DATA	(PAGE_MGR_ATTR_ALLOC_CONTIG	|       \
                                 PAGE_MGR_ATTR_ALLOC_MAPPED	|       \
                                 MMU_ATTR_SUP_DATA)

#define	PAGE_MGR_OPTS_SUP_DATA_RO                                       \
    				(PAGE_MGR_ATTR_ALLOC_CONTIG	|       \
                                 PAGE_MGR_ATTR_ALLOC_MAPPED	|       \
                                 MMU_ATTR_SUP_DATA_RO)

#define	PAGE_MGR_BITMAP_SET(dst, src, msk)                                \
    	(dst) = (((dst) & ~(msk)) | ((src) & (msk)))

#define	PAGE_MGR_ATTR_ALLOC_GET(psAttr)	((psAttr) & PAGE_MGR_ATTR_ALLOC_MSK)
#define	PAGE_MGR_ATTR_ALLOC_SET(psAttr, allocAttr)                        \
	PAGE_MGR_BITMAP_SET(psAttr, allocAttr, PAGE_MGR_ATTR_ALLOC_MSK)

#define PAGE_MGR_ATTR_MMU_GET(psAttr)	((psAttr) & PAGE_MGR_ATTR_MMU_MSK)
#define	PAGE_MGR_ATTR_MMU_SET(psAttr, mmuAttr)                            \
	PAGE_MGR_BITMAP_SET(psAttr, mmuAttr, PAGE_MGR_ATTR_MMU_MSK)

#ifndef	_ASMLANGUAGE

/* Access macros */    

#define	PS_VM_CONTEXT_ID(pgMgrId)   (((PAGE_MGR_OBJ *)(pgMgrId))->vmContextId)
#define	PS_VIRT_PAGESIZE(pgMgrId)   (((PAGE_MGR_OBJ *)(pgMgrId))->virtPageSize)
#define	PS_VM_PAGESIZE(pgMgrId)     (((PAGE_MGR_OBJ *)(pgMgrId))->vmPageSize)
#define	PS_OBJCORE(pgMgrId)	    (((PAGE_MGR_OBJ *)(pgMgrId))->objCore)
#define	PS_VIRT_PGPOOL_ID(pgMgrId)  (((PAGE_MGR_OBJ *)(pgMgrId))->virtPgPoolId)
#define	PAGE_MGR_OPTIONS(pgMgrId)   (((PAGE_MGR_OBJ *)(pgMgrId))->options)

#define	PAGE_MGR_OPTIONS_ALLOWED                                        \
    				(PAGE_MGR_ATTR_MMU_MSK   |              \
				 PAGE_MGR_ATTR_ALLOC_MSK)

/* typedef */

typedef enum pgMgrAttr {    
    PAGE_MGR_ATTR_MMU_MSK	   = 0x000FFFFF, /* Mask for MMU attributes */

    PAGE_MGR_ATTR_ALLOC_MSK	   = 0x00F00000, /* Mask for allocation attr */
    PAGE_MGR_ATTR_ALLOC_CONTIG_MSK = 0x00300000,
    PAGE_MGR_ATTR_ALLOC_NONCONTIG  = 0x00100000,
    PAGE_MGR_ATTR_ALLOC_CONTIG	   = 0x00200000, /* Alloc contig phys pages */
    PAGE_MGR_ATTR_ALLOC_MAP_MSK    = 0x00C00000,
    PAGE_MGR_ATTR_ALLOC_MAPPED	   = 0x00400000,
    PAGE_MGR_ATTR_ALLOC_UNMAPPED   = 0x00800000, /* Alloc unmapped pages */
    PAGE_MGR_ATTR_ALLOC_CONTIG_DEFAULT = PAGE_MGR_ATTR_ALLOC_CONTIG,
    PAGE_MGR_ATTR_ALLOC_MAP_DEFAULT    = PAGE_MGR_ATTR_ALLOC_MAPPED,
    PAGE_MGR_OPTS_MSK		= 0xF0000000, /* Mask for other options */
    PAGE_MGR_OPTS_NONE		= 0x00000000,

    PAGE_MGR_ATTR_DEFAULT	= 0x00000000
    } PAGE_MGR_ATTR;

typedef PAGE_MGR_ATTR PAGE_MGR_OPTS;

typedef struct pgMgrObj {
    OBJ_CORE		objCore;
    PAGE_POOL_ID	virtPgPoolId;
    PAGE_POOL_ID 	physPgPoolId; 
    VM_CONTEXT_ID	vmContextId;
    UINT		vmPageSize;
    UINT		virtPageSize;
    PAGE_MGR_OPTS	options;
    void *		pExt;
    } PAGE_MGR_OBJ;


/* function prototypes */

STATUS	    pgMgrLibInit (void);
PAGE_MGR_ID pgMgrCreate (RTP_ID rtpId, PAGE_MGR_OPTS options);
STATUS	    pgMgrDelete (PAGE_MGR_ID pgMgrId);
VIRT_ADDR   pgMgrPageAlloc (PAGE_MGR_ID pgMgrId, UINT numPages,
                          PAGE_MGR_ATTR attr);
VIRT_ADDR   pgMgrPageAllocAt (PAGE_MGR_ID pgMgrId, VIRT_ADDR virtAdr,
                            PHYS_ADDR physAdr, UINT numPages,
                            PAGE_MGR_ATTR attr);
STATUS 	    pgMgrPageFree (PAGE_MGR_ID pgMgrId, VIRT_ADDR virtAdr,
                              UINT numPages);
STATUS	    pgMgrPageMap (PAGE_MGR_ID pgMgrId, VIRT_ADDR virtAdr,
                          PHYS_ADDR physAdr, UINT numPages, PAGE_MGR_ATTR attr);
STATUS	    pgMgrPageUnmap (PAGE_MGR_ID, VIRT_ADDR, UINT);
STATUS	    pgMgrPageAttrSet (PAGE_MGR_ID pgMgrId, VIRT_ADDR virtAdr,
                              UINT numPages, MMU_ATTR mmuAttr);
STATUS	    pgMgrPageAttrGet (PAGE_MGR_ID pgMgrId, VIRT_ADDR virtAdr,
                              MMU_ATTR * pMmuAttr);
STATUS	    pgMgrVirtToPhys (PAGE_MGR_ID pgMgrId, VIRT_ADDR virtAdr,
                             PHYS_ADDR * pPhysAdr);
STATUS	    pgMgrPhysToVirt (PAGE_MGR_ID pgMgrId, PHYS_ADDR physAdr,
                             VIRT_ADDR * pVirtAdr);
PAGE_MGR_OPTS pgMgrOptsGet (PAGE_MGR_ID	pgMgrId);
PAGE_MGR_ID pgMgrInit (PAGE_MGR_OBJ*, RTP_ID, PAGE_MGR_OPTS);
STATUS      pgMgrOptsSet (PAGE_MGR_ID, PAGE_MGR_OPTS);
void        pgMgrShowInit (void);
STATUS	    pgMgrPrivateUnmap (PAGE_MGR_ID pgMgrId, BOOL takePgPoolMutex);

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCpgMgrLibPh */
