/* pfwStackP.h - private Protocol FrameWork stack header */

/*
 * Copyright (c) 1999,2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* 
modification history
--------------------
01g,15mar05,ijm  added pfwPluginObjStateLockNoWait
01f,22nov99,koz  Added pfwStackTableCreate routine
01e,11oct99,koz  Added callbacks and delete cause id attributes
01d,22sep99,sj   moved pfwStackAddDone and pfwStackDeleteDone to pfwStack.h
01c,08sep99,koz  Added event information to the stack object
01b,18Aug99,sj   modified for generic framework 
01a,09jul99,koz  created 
*/

#ifndef __INCpfwStackPh
#define __INCpfwStackPh

#include "pfw/pfw.h"
#include "pfw/pfwStack.h"
#include "semLib.h"
#include "private/pfw/pfwProfileP.h"
#include "private/pfw/pfwEventP.h"

#ifdef __cplusplus
extern "C" {
#endif


typedef struct pfwStackItem PFW_STACK_ITEM;

typedef enum pfwStackItemStatus
    {
    PFW_ADDED_STACK_ITEM,
    PFW_ATTACHED_STACK_ITEM
    } PFW_STACK_ITEM_STATUS;

/*
 * Each stack object has a list of stack item as described below. Each 
 * component and layer in a stack is represented by a stack item.
 */

struct pfwStackItem
    {
    PFW_PLUGIN_OBJ_STATE state;   /* state of a layer or component */
    PFW_STACK_ITEM *nextRcv;      /* pointer to the next receiving item */ 
    PFW_STACK_ITEM *nextSend;     /* pointer to the next sending item */
    PFW_STACK_ITEM *relative;     /* pointer to the parent layer (for a */
			          /* component) or a child component (for a */
				  /* layer) */
    PFW_STACK_ITEM *lastVisited;  /* the last visited component of a layer */
    unsigned int addedOrAttached : 1; /* if stackAdd is called for the stack */
				      /* item */ 
    };

/*
 * For each stack instance, a stack object is created. This object holds 
 * all the information for that stack.
 */

struct pfwStackObj
    {
    unsigned int id;           	        /* position in the stack table */
    SEM_ID stackMutex;             	/* single thread access to stackObj */
    PFW_OBJ * pfwObj;                   /* framework this belongs to */
    PFW_STACK_STATUS status;            /* current status of stack */
    PFW_STACK_ITEM *
    firstItem[PFW_LAYERED_PLANE_NUMBER];/*array of pointers to first receiving*/
                                        /* items in each layered plane */
				        /* traversed in this stack */
    PFW_EVENT_ITEM *eventItemHead;      /* head of the event item list */
    PFW_STACK_OBJ_CALLBACKS *callbacks; /* callback functions for stack add */
					/* delete processing */
    void *userHandle;                   /* user provided handle */ 
    PFW_LAYER_PROFILE_ITEM              /* profile data */              
	*firstLayer[PFW_LAYERED_PLANE_NUMBER];
    PFW_STACK_ITEM *lastAddedItem;      /* the last added item in the add */
					/* processes initiated by pfwStackAdd */

    PFW_STACK_ITEM *lastDeletedItem;    /* the last deleted item in the delete*/
					/* processes initiated by pfwStackAdd */
    };

extern void pfwStackTableCreate (PFW_OBJ *pfwObj, unsigned int initialSize,
				 int maxSize);
extern void pfwStackTableDelete (PFW_OBJ *pfwObj);

extern STATUS pfwPluginObjStateLockNoWait (PFW_PLUGIN_OBJ_STATE * pluginState);

#ifdef __cplusplus
}
#endif

#endif /* __INCpfwStackPh */

