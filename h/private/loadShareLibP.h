/* loadShareLib.c - support for sharing loader code between host and target */

/* 
 * Copyright (c) 2004 Wind River Systems, Inc. 
 *
 * The right to copy, distribute or otherwise make use of this software may be
 * licensed only pursuant to the terms of an applicable Wind River license
 * agreement. 
 */

/*
modification history
--------------------
01e,08sep05,v_r  Refined loadShareTgtMemFree() prototype + cleanups.
01d,25jul05,v_r  Updated copyright.
01c,02jun04,jmp  fixed win32 build.
01b,16may04,jn   Finish implementation of loadShareCacheRefresh
01a,14may04,jn   Create
*/

extern void * loadShareTgtMemAlloc ();

#ifdef HOST
DLL_EXPORT
#endif	/* HOST */
extern void * loadShareTgtMemAlign (UINT alignment, UINT size);
void loadShareTgtMemFree (void * addr);
STATUS loadShareTgtMemRead ();

#ifdef HOST
DLL_EXPORT
#endif  /* HOST */
void loadShareTgtMemSet (void * addr, int value, UINT size);
STATUS loadShareTgtMemWrite ();
void loadShareCacheFlush ();

#ifdef HOST
DLL_EXPORT
#endif  /* HOST */
STATUS loadShareCacheRefresh ();
