/* mmuSoftLibP.h - private header for software MMU simulation library */

/* Copyright 1984-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,08apr04,zl   written.
*/

#ifndef __INCmmuSoftLibPh
#define __INCmmuSoftLibPh

#ifdef __cplusplus
extern "C" {
#endif


STATUS  mmuSoftLibInit (int mmuPageSize, VIRT_ADDR startAddr, 
			VIRT_ADDR kernelTop);

#ifdef __cplusplus
}
#endif

#endif /* __INCvmLibPh */
