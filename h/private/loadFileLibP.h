/* loadFileLibP.h - header for utility func. lib. shared by loader libraries */

/*
 * Copyright (c) 2004-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement.
 */

/*
modification history
--------------------
01g,07mar05,v_r  Included symbol.h
01f,01mar05,v_r  Moved segment type enum to moduleLib.h.
01e,06oct04,jn   Clean up comments
01d,17sep04,jn   Move towards shared files
01c,08apr04,jn   Adjustment needed in segment descriptor structure to handle
                 and track alignments correctly
01b,05apr04,jn   Clean up development-related comments
01a,19feb04,jn   Derived from AE's malLib.h
*/

#ifndef __INCloadFileLibPh
#define __INCloadFileLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* Includes */

#ifdef HOST
#include "host.h"
#include "objLib.h"
#else
#include "vxWorks.h"
#include "errno.h"
#include "vwModNum.h"
#include "private/handleLibP.h"
#endif	/* HOST */

#include "symbol.h"
#include "private/symbolP.h"
#include "dllLib.h"

/* Status codes */

/* Defines */

/* 
 * For internal use only, within load directives.  Section names will
 * be truncated to this number of characters when used in load
 * directives.  
 */

#define SEC_NAME_MAX 256

/* indexes into the array of segment descriptors */

#define TEXT_SEG_DESC_INDEX 0
#define DATA_SEG_DESC_INDEX 1
#define BSS_SEG_DESC_INDEX 2

#define NUM_KERNEL_MODULE_SEGS  3

/* Type definitions */

typedef unsigned int LOAD_FLAG_T;       /* load flag type */

/* information associated with module while loading is in progress */

typedef enum module_source		/* method file was specified by */
    {
    FROM_FD	   = 0,			/* opened from file descriptor */
    FROM_FILENAME  = 1			/* opened from file name */
    } MODULE_SOURCE;
						   
typedef enum mseek_mode			/* Module position mode flags     */
    {
    MSEEK_SET	= 0,			/* Set pos. from the beginnning   */
    MSEEK_CUR	= 1			/* Set pos. from the current loc. */
    } MSEEK_MODE;


/* Types used throughout various loader header files */

typedef void *          SYM_ADRS;               /* symbol's address */
typedef void **         SYM_ADRS_TBL;           /* table of symbol's addr */

typedef void *          SCN_ADRS;               /* section's address */
typedef void **         SCN_ADRS_TBL;           /* table of section's addr */

typedef enum load_type			/* type of load being performed */
    {
    LOAD_UNKNOWN       = 0,
    LOAD_SYS_SYM_TBL   = 1,  /* Load the system symbol table */
    LOAD_VXWORKS_IMG   = 2,  /* Load vxWorks image (copy on host for tgtsvr) */
    LOAD_STAT_EXE      = 3,  /* Load statically linked executable */
    LOAD_DYN_EXE       = 4,  /* Load dynamically linked executable */
    LOAD_KERNEL_MODULE = 5,  /* Load relocatable module into kernel */
    LOAD_SYMS_ONLY     = 6   /* Load symbols only */
    } LOAD_TYPE;

typedef struct load_module_info		/* information about file to load */
    {
    int			fd;		/* file descriptor */
    int			options;
    char *		fileName;
    MODULE_SOURCE       moduleOpened;     /* FROM_FD or FROM_FILENAME */
    LOAD_TYPE           loadType;      /* relocatable, executable, ... */
    } LOAD_MODULE_INFO;

typedef struct
    {
    SYM_ADRS    pAddr;  /* symbol's address */
    SYM_TYPE    type;   /* symbol's type */
    SYMBOL_ID   symId;  /* ID of the symbol */
    } SYM_INFO;
 
typedef SYM_INFO *      SYM_INFO_TBL;   /* table of symbol address/type info */
typedef SYM_INFO_TBL *  SYMINFO_REFS;   /* table of ptr to symbol info tables */ 
/* section types */

typedef enum section_type       /* Section types */
    {
    SECTION_UNKNOWN =   0,
    SECTION_RODATA =    1,      /* read-only data section */
    SECTION_TEXT =      2,      /* text section */
    SECTION_DATA =      3,      /* data section */
    SECTION_BSS =       4       /* bss section */
    } SECTION_TYPE;

typedef struct modListDesc * MODLIST_ID;        /* module list ID */
typedef struct sectionDesc * SECTION_ID;        /* section ID */

typedef enum section_type SECTION_T;

typedef struct sectionDesc      /* module section descriptor */
    {
#ifdef HOST
    OBJ_CORE    handle;                 /* handle management */
#else
    HANDLE  handle;                     /* handle management */
#endif  /* HOST */
    DL_NODE             sectionNode;    /* double-linked list node information*/    char *              name;           /* section name */
    void *              address;        /* section address */
    int                 size;           /* section size */
    enum section_type   type;           /* section type */
    int                 flags;          /* section flags */
    u_short             checksum;       /* section cksum; u_short is returned by                                         * checksum() */
    } SECTION_DESC;

typedef struct section_info_desc        /* Info about a loadable section   */ 
    {
    SL_NODE             scnInfoNode;    /* Point to next node in list      */
    char *              name;           /* Section's name                  */
    enum section_type   type;           /* Section's type                  */
    UINT                offset;         /* Offset of section in object module*/
    UINT                size;           /* Section's size                    */
    UINT                alignment;      /* Section's alignment               */
    UINT                flags;          /* Section's flags                   */
    void *              address;    /* Section's address             */
    UINT                offsetInSeg;    /* Section's offset in segment  */
    UINT                directiveIdx;  /* Index of the related load directive*/
#ifdef HOST
    BOOL                scnIsCached;
#endif
    SECTION_ID          sectionId;
    } SECTION_INFO_DESC;

typedef struct
    {
    char * addrText;    /* text segment address */
    char * addrData;    /* data segment address */
    char * addrBss;     /* bss segment address */
    UINT   sizeText;    /* text segment size */
    UINT   sizeProtectedText;   /* protected text segment size */
    UINT   sizeData;    /* data segment size */
    UINT   sizeBss;     /* bss segment size */
    int    flagsText;   /* text flags for module */
    int    flagsData;   /* data flags for module */
    int    flagsBss;    /* bss flags for module */
    } SEG_INFO;

typedef struct load_segment_directive
    {
    UINT32	type;                    /* type of the segment */
    void *      address;                 /* address of the segment */
    BOOL        memAllocatedByCaller; /* True iff the caller allocated mem. */
    } LOAD_SEGMENT_DIRECTIVE;

/* declarations */

extern void     loadFileLibInit (void);
extern STATUS   loadModuleInfoFromFd (int fd, 
				      LOAD_MODULE_INFO * pLoadModuleInfo);
extern STATUS   loadModuleInfoFromFilenameOpen (char * fileName, 
						LOAD_MODULE_INFO * pLoadModuleInfo);
extern STATUS   loadModuleInfoRelease (LOAD_MODULE_INFO loadModuleInfo);
extern STATUS   loadModuleSeek (LOAD_MODULE_INFO * pLoadModuleInfo, 
				int offset, MSEEK_MODE mode);
extern STATUS   loadModuleStringRead (LOAD_MODULE_INFO * pLoadModuleInfo, 
				      char * buffer, UINT32 nBytes);
extern STATUS   loadModuleValueRead (LOAD_MODULE_INFO * pLoadModuleInfo, 
				     void * buffer, UINT32 nBytes);

#ifdef __cplusplus
}
#endif

#endif /* __INCloadFileLibPh */
