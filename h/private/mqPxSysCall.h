/* mqPxSysCall.h - POSIX message queue system calls */

/* Copyright 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,24mar04,mcm  Including time.h instead of sys/time.h in user space.
01a,17nov03,m_s  written
*/

#ifndef __INCmqPxSysCallh
#define __INCmqPxSysCallh

#ifdef __cplusplus
extern "C" {
#endif

#include "objLib.h"
#include "pxObjSysCall.h"

#include "time.h"

/* typedefs */

/* structures for pxCtl system call */

struct mqNotifyArgs       /* PX_MQ_NOTIFY */
    {
    const struct sigevent * pNotification;
    };

struct mqAttrGetArgs      /* PX_MQ_ATTR_GET */
    {
    struct mq_attr * pOldMqStat;
    };


/* system call function prototypes */

extern ssize_t pxMqReceive (OBJ_HANDLE handle, char * pMsg, size_t msgLen,
                            unsigned int * pMsgPrio, PX_WAIT_OPTION waitOption,
                            struct timespec * time_out);
extern int     pxMqSend    (OBJ_HANDLE handle, const char * pMsg, size_t msgLen,
                            unsigned int msgPrio, PX_WAIT_OPTION waitOption,
                            struct timespec * time_out);

#ifdef __cplusplus
}
#endif

#endif /* __INCmqPxSysCallh */
