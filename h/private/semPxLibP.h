/* semPxLibP.h - private POSIX semaphore library header */

/* Copyright 1984-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01h,29sep04,dcc  updated _sem_destroy API to accept a dealloc parameter.
01g,28sep04,fr   removed semPxCreateRtn prototype (SPR 101349)
01f,30mar04,dcc  updated to use posixated object management.
01e,05dec03,m_s  code-inspection changes
01d,31oct03,m_s  moved the semaphore descriptor definition from semaphore.h
                 and ported to Base 6
01c,12jan94,kdl  changed semaphoreInit() to semPxLibInit().
01b,17dec93,dvs  added semPxClassId
01a,29nov93,dvs  written
*/

#ifndef __INCsemPxLibPh
#define __INCsemPxLibPh

#ifdef __cplusplus
extern "C" {
#endif

#include "stdarg.h"
#include "private/objLibP.h"
#include "private/classLibP.h"

extern CLASS_ID semPxClassId;            /* POSIX sem class ID */

/* posix semaphore descriptor */

typedef struct sem_des          /* sem_t */
    {
    OBJ_CORE         objCore;   /* object core */
    SEM_ID           wSemId;    /* Wind semaphore identifier */
    struct sem_des * pSelf;     /* pointer to self */
    } PSEMAPHORE;


/* function declarations */

extern STATUS        semPxLibInit (void);
extern int           _sem_destroy  (PSEMAPHORE * pxSemId, BOOL dealloc);
extern int           _sem_close    (PSEMAPHORE * pxSemId);
extern int           _sem_wait     (PSEMAPHORE * pxSemId);
extern int           _sem_trywait  (PSEMAPHORE * pxSemId);
extern int           _sem_post     (PSEMAPHORE * pxSemId);
extern int           _sem_getvalue (PSEMAPHORE * pxSemId, int * sval);


#ifdef __cplusplus
}
#endif

#endif /* __INCsemPxLibPh */
