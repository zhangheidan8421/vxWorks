/* dbgRtpLibP.h - private RTP debugging facility header */

/* Copyright 2004-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,10jan05,bpn  Added dbgRtpInit() prototype. Removed dbgRtpBpAllRemove().
01a,05apr04,bpn  Written.
*/

#ifndef __INCdbgRtpLibPh
#define __INCdbgRtpLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* Defines */

/* Function declarations */

extern STATUS	dbgRtpInit (void);

#ifdef __cplusplus
}
#endif

#endif /* __INCdbgRtpLibPh */

