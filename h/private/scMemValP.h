/* scMemValP.h - private header file for system call validation routines */

/* Copyright 2003-2004 Wind River Systems, Inc. */
/*
modification history
--------------------
01c,05nov04,zl   moved function prototype to public header.
01b,19apr04,tam  updated prototype
01a,23oct03,tam  written.
*/

#ifndef __INCscMemValPh
#define __INCscMemValPh

#ifdef __cplusplus
extern "C" {
#endif

#include "scMemVal.h"

#define SC_PROT_MSK	0x3	/* protection mask */


#ifdef __cplusplus
}
#endif

#endif /* __INCscMemValPh */
