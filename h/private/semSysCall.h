/* semSysCall.h - VxWorks semaphore system call definitions */

/* Copyright 2003-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,22apr04,dcc  added "context" to _semOpen() parameters.
01a,31oct03,md   written
*/

#ifndef __INCsemSysCallh
#define __INCsemSysCallh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"


/* command codes for the semCtl() system call */

typedef enum
    {
    VX_SEM_CTL_MTAKE_PROXY,
    VX_SEM_CTL_SEM_OWNER,
    VX_SEM_CTL_FLUSH
    } VX_SEM_CTL_CMD;


/* command struct for VX_SEM_CTL_VAR_* commands */

typedef struct vx_sem_ctl_var_cmd
    {
    int *pVariable;
    int  value;
    } VX_SEM_CTL_VAR_CMD;


/* system call function prototypes */

#if !defined(_WRS_KERNEL)

extern STATUS        semCtl     (SEM_ID_KERNEL semId, VX_SEM_CTL_CMD command, 
                                 void * pArg, UINT * pArgSize);
extern SEM_ID_KERNEL _semOpen	(const char * name, SEM_TYPE type, 
				 int initState, int options, int mode, 
				 void * context);
extern STATUS 	     _semTake 	(OBJ_HANDLE semId, int timeout);
extern STATUS 	     _semGive 	(OBJ_HANDLE semId);
extern STATUS 	     _semFlush 	(OBJ_HANDLE semId);

#endif /* !_WRS_KERNEL */

#ifdef __cplusplus
}
#endif

#endif /* __INCsemSysCallh */
