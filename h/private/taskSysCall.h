/* taskSysCall.h - VxWorks tasking system call definitions */

/*
 * Copyright (c) 2003-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01h,28jun05,jln  Add VX_TASK_CTL_PX_ATTR_XXX cases
01g,18apr04,hya  added VX_TASK_CTL_RESET.
01g,18feb04,aeg  changed _taskOpen() system call API.
01f,03dec03,kk   added VX_TASK_CTL_EXIT_REGISTER enum to taskCtl()
01e,27oct03,md   added VX_TASK_CTL_UTCB_SET enum value.
01d,27nov03,aeg  added _taskOpen() function declaration.
01c,22sep03,aeg  added VX_TASK_CTL_TASK_EXIT enum value.
01b,05sep03,dcc  moved taskOpen() and taskDelay() prototypes to taskLibCommon.h
01a,19aug03,aeg  written
*/

#ifndef __INCtaskSysCallh
#define __INCtaskSysCallh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"


/* command codes for the taskCtl() system call */

typedef enum
    {
    VX_TASK_CTL_ACTIVATE,           
    VX_TASK_CTL_SUSPEND,     
    VX_TASK_CTL_RESUME,         
    VX_TASK_CTL_RESTART,         
    VX_TASK_CTL_RESET,         
    VX_TASK_CTL_PRIORITY_GET,     
    VX_TASK_CTL_PRIORITY_SET,    
    VX_TASK_CTL_VERIFY,        
    VX_TASK_CTL_VAR_ADD,      
    VX_TASK_CTL_VAR_DELETE,     
    VX_TASK_CTL_VAR_GET,        
    VX_TASK_CTL_VAR_SET,
    VX_TASK_CTL_TASK_EXIT,
    VX_TASK_CTL_UTCB_SET,
    VX_TASK_CTL_EXIT_REGISTER,
    VX_TASK_CTL_PX_ATTR_SET,
    VX_TASK_CTL_PX_ATTR_GET
    } VX_TASK_CTL_CMD;

/* command struct for VX_TASK_CTL_PX_ATTR_* commands */

typedef struct vx_task_ctl_px_attr
    {
    void * pAttr;
    int    attrSz;
    } VX_TASK_CTL_PX_ATTR;

/* command struct for VX_TASK_CTL_VAR_* commands */

typedef struct vx_task_ctl_var_cmd
    {
    int *pVariable;
    int  value;
    } VX_TASK_CTL_VAR_CMD;


/* argument structure for the _taskOpen() system call */

typedef struct vx_task_open_sc_args
    {
    const char * name;           /* task name - default name will be chosen */
    int          priority;       /* task priority */
    int          options;        /* VX_ task option bits */
    int		 mode;           /* object management mode bits */
    char       * pStackBase;     /* location of execution stack */
    int		 stackSize;      /* execution stack size */
    BOOL       * pTaskCreated;   /* new kernel task created? */
    void       * context;	 /* context value */
    FUNCPTR	 entryPt;	 /* application entry point */
    int		 argc;		 /* number of arguments to entry point */
    char      ** argv;	 	 /* arguments to application entry point */
    } VX_TASK_OPEN_SC_ARGS;

/* system call function prototypes */

extern STATUS   taskCtl	  (int tid, VX_TASK_CTL_CMD command, void * pArg, 
			   UINT * pArgSize);
extern int 	_taskOpen (VX_TASK_OPEN_SC_ARGS *pArgs);

#ifdef __cplusplus
}
#endif

#endif /* __INCtaskSysCallh */
