/* jobLibP.h - private job facility library header */

/* 
 * Copyright (c) 2004-2005 Wind River Systems, Inc. 
 *
 * The right to copy, distribute, modify or otherwise make use 
 * of this software may be licensed only pursuant to the terms 
 * of an applicable Wind River license agreement. 
 */ 

/*
modification history
--------------------
01d,07jun05,yvp  Deleted jobTaskLibInit, jobExcAdd. jobAdd moved to excLibP.h.
                 Updated copyright.
01c,06sep04,aeg  added jobExcAdd () prototype.
01b,07sep04,bpn  Added isrJobMsgQId definition.
01a,27aug04,ans  written
*/

#ifndef __INCjobLibPh
#define __INCjobLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* XXX bpn - the ISR job queue is defined in that file for dbgLib.c */

extern MSG_Q_ID	isrJobMsgQId;	/* message queue ID used by excTask */

/* function declarations */

extern STATUS jobLibInit 	(int jobTaskStackSz);

#ifdef __cplusplus
}
#endif

#endif /* __INCjobLibPh */
