/* ledLibP.h - private header file for ledLib.c */

/* Copyright 2003-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01n,06jul04,bpn  Added ledHistCurrentNumGet() prototype (SPR#98947).
01m,26apr04,bpn  Added inactivity timeout feature.
01l,21jan04,bpn  Added lineBuf field to LED structure to avoid memory leaks.
01k,14nov03,bpn  Changed CTX_HIST structure.
01j,24oct03,bpn  Added ledDfltComplete() prototype. Added completionRtn
                 field to LED structure.
01i,23sep03,bpn  Moved LED_MODE_FUNCS structure to ledLib.h.
01h,29aug03,bpn  Cleaned up. Added necessary fields to HIST and CTX_HIST 
                 structures to implement historical line pre-allocation.
01g,05aug03,bpn  Updated function prototypes.
01f,08jul03,bpn  Updated ledNameComplete() API.
01e,25jun03,bpn  Changes structures in order to have on command history list
                 by interpreter.
01d,28mar03,lcs  Reworked some APIs.
01c,18mar03,lcs  Add led mode registry.
01b,14mar03,bpn  Changed LED structure.
01a,17feb03,bpn  Written from version "01c,26apr00,jmp" of ledLib.h of the 
                 tor3_1 branch.
*/

#ifndef __INCledLibPh
#define __INCledLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* Includes */

#ifdef HOST
#include <host.h>
#include <sys/time.h>
#else
#include <sys/times.h>
#endif	/* HOST */

#include <lstLib.h>

/* Defines */

#define	LED_BACKWARD	(-1)
#define	LED_FORWARD	1

/* Typedefs */

typedef struct	/* HIST */
    {
    NODE    node;
    UINT    lineSize;
    char *  line;
    } HIST;

typedef struct ctx_hist
    {
    HIST *	pCurrentHist;	/* current history node */
    LIST	histList;	/* command history list */
    int		histNum;	/* current history number */
    UINT	histSize;	/* command history size */
    int		nodeNumber;	/* current historical line node number */
    } CTX_HIST;

typedef struct led /* LED */
    {
    int		inFd;		/* fd to get characters from */
    int		outFd;		/* fd to put characters to */
    CTX_HIST *  pHistory;
    CTX_HIST *  pInitialHistory;

    /*
     * XXX the following are not needed between ledRead's
     * but are used in support routines.
     */

    char *	lineBuf;	/* line buffer pointer */
    char *	deleteBuf;	/* hold deletions from curLn */
    UINT	deleteBufSize;
    BOOL	cmdMode;	/* insert or command mode */
    
    LED_MODE_FUNCS * pLedModeFuncs; /* indicates led mode of this ledId */
    FUNCPTR	completionRtn;	/* completion routine or NULL for default */
    struct timeval  timeout;	/* inactivity timeout or polling period */

#ifdef HOST
    int		    idle;	/* whether we're sitting in read () */
    /* the following is needed for select loop support */
    VOIDFUNCPTR	    callback;
    fd_set	    readfds;
#endif
    } LED;

/* Function declarations */

#ifdef HOST
extern LED_ID	ledHostOpen (int inFd, int outFd, int histSize,
			     struct timeval * pTimeout, VOIDFUNCPTR callback);
extern int 	ledIdle (LED * ledId);
extern void 	ledModeAllUnregister ();
#endif

extern int	ledNWrite (int fd, char ch, int nbytes);
extern void	ledPreemptSet (FUNCPTR * catchFunc, FUNCPTR func);
extern int	ledPreempt (FUNCPTR * catchFunc, char ch, char * curLn,
			    UINT * curPs, int * number, LED * ledId);
extern void	ledRedraw (int outFd, char * oldLn, UINT lastPs, char * curLn,
			   UINT * curPs);
extern void	ledBeep (int outFd);
extern int	ledHistCurrentNumGet (LED_ID ledId);
extern BOOL	ledHistNumGet (LED * ledId, int n, char * line, int lineSize);
extern BOOL	ledHistFind (LED * ledId, char * match, char * line, int lineSize);
extern STATUS	ledHistPrevGet (LED * ledId, char * line, int lineSize);
extern STATUS	ledHistNextGet (LED * ledId, char * line, int lineSize);
extern void	ledSearch (BOOL ignorePunct, BOOL endOfWord, char * curLn,
			   UINT * curPs, int dir);
extern STATUS	ledFwdFind (char ch, char * curLn, UINT * curPs, int * number);
extern STATUS	ledBwdFind (char ch, char * curLn, UINT * curPs, int * number);
extern STATUS	ledReplace (char ch, char * curLn, UINT * curPs, int * number);
extern STATUS	ledChange (char ch, char * curLn, UINT * curPs, int * number,
			   LED * ledId);
extern STATUS	ledCDelete (char ch, char * curLn, UINT * curPs, int * number,
			    LED * ledId);

#ifndef HOST
extern void	ledInactivityDelaySet (LED_ID ledId, UINT delay);
extern STATUS	ledDfltComplete (LED_ID ledId, char * line, UINT lineSize, 
				 UINT * pCursorPos, char completionChar);
#endif

#ifdef __cplusplus
}
#endif

#endif /* __INCledLibPh */
