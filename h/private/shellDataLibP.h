/* shellDataLibP.h - private header for the shell data management module */

/* Copyright 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,28oct03,bpn  Written.
*/

#ifndef __INCshellDataLibPh
#define __INCshellDataLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* Includes */
#ifndef HOST
#include <vxWorks.h>
#else
#include <host.h>
#endif

#include <shellLib.h>

/* Defines */

/* Structures */

/* Function declarations */

void shellDataAllRemove (SHELL_ID shellId, BOOL finalize);

#ifdef __cplusplus
}
#endif

#endif /* __INCshellDataLibPh */
