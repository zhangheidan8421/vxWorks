/* dbgTaskLibP.h - private header file for dbgTaskLib.c */

/* Copyright 2003-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01i,10jan05,bpn  Removed dbgRtpInit() prototype.
01h,09jan04,elg  Add notification function pointer for breakpoint
                 synchronization.
01g,25nov03,elg  Add RTP support.
01f,14nov03,elg  Change dbgTaskBpEpSet() and dbgTaskHwBpSet() prototype.
01e,21oct03,bpn  Added definitions from dbgLibP.h. Added BP_LOCATION enum.
01d,17oct03,elg  Add dbgLibP.h to define EVT_CALL_ARGS.
01c,08jul03,bpn  Updated.
01b,27jun03,bpn  Changed enum value DBG_CRET to DBG_CONT_RET.
01a,26feb03,bpn  Created.
*/

#ifndef __INCdbgTaskLibPh
#define __INCdbgTaskLibPh

#ifdef __cplusplus
extern "C" {
#endif

#include <vxWorks.h>
#include <taskLib.h>
#include <wvLib.h>
#include <wdb/wdbDbgLib.h>

/* Defines */

#define ALL             0       /* breakpoint applies to all tasks */

#define DBG_INFO(p)	(&(((WIND_TCB *)(p))->wdbInfo))

#define ANY_TASK_BP(pBp)	(((pBp)->bp_flags & BP_ANY_TASK) == BP_ANY_TASK)
#define ONE_TASK_BP(pBp, tid)	((((pBp)->bp_flags & BP_TASK) == BP_TASK) \
				&& ((pBp)->bp_ctxId == (UINT32) (tid)))
#define ONE_RTP_BP(pBp, rtpid)	((((pBp)->bp_flags & BP_RTP) == BP_RTP) \
				&& ((pBp)->bp_ctxId == (UINT32) (rtpid)))

/* typedefs */

typedef enum
    {
    BP_NUMBER_LOCATION = 1,
    BP_ADDRESS_LOCATION,
    BP_ALL_LOCATION
    } BP_LOCATION;

typedef struct                  /* EVT_CALL_ARGS */
    {
    FUNCPTR	evtRtn;		/* event routine */
    int 	evtRtnArg;	/* event routine argument */
    event_t	eventId;	/* event ID */
    } EVT_CALL_ARGS;

/* externals */

extern FUNCPTR		_func_dbgRtpStop;
extern FUNCPTR		_func_dbgRtpCont;
extern VOIDFUNCPTR	_func_bpSyncNotify;

/* function declarations */

extern STATUS	dbgTaskInit ();
extern STATUS	dbgTaskTmpBpRemove (int taskId, INSTR * pc);
extern BOOL	dbgTaskBpRemove (void * ctxId, INSTR * addrNum,
				 UINT ctx, BP_LOCATION location);
extern STATUS	dbgTaskBpEpSet (INSTR * addr, void * ctxId, UINT flags,
				UINT count, UINT action,
				VOIDFUNCPTR callRtn, int callArg,
				UINT * pBpId);
#if	DBG_HARDWARE_BP
extern STATUS	dbgTaskHwBpSet (INSTR * addr, void * ctxId, UINT flags,
				UINT count, UINT action,
				VOIDFUNCPTR callRtn, int callArg,
				UINT * pBpId);
#endif	/* DBG_HARDWARE_BP */
extern STATUS	dbgTaskCont (UINT32 contextId);
extern STATUS	dbgTaskStop (UINT32 contextId);
extern STATUS	dbgTaskStep (UINT32 contextId, UINT32 startAddr,
			     UINT32 endAddr);

#ifdef __cplusplus
}
#endif

#endif /* __INCdbgTaskLibPh */
