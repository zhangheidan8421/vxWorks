/* objLibP.h - private object management library header */

/* 
 * Copyright (c) 1992-1993, 2003-2005 Wind River Systems, Inc. 
 *
 * The right to copy, distribute, modify or otherwise make use 
 * of this software may be licensed only pursuant to the terms 
 * of an applicable Wind River license agreement. 
 */ 

/*
modification history
--------------------
02c,16aug05,gls  added objOwnerLibInstalled
02b,25jul05,yvp  Reworked OBJ_VERIFY conditional definition.
02a,07jun05,yvp  Updated copyright. #include now with angle-brackets.
01z,02jun05,yvp  Reworked OBJ_VERIFY conditional for MKL.
01y,25mar05,job  OBJ_VERIFY only defined in checked build
01x,05may05,gls  added objShowInit()
01w,07apr05,gls  added objOwnershipInit() (SPR #106150)
01v,24jan05,aeg  added objRtpLibInit() function prototype (SPR #106381).
01u,01feb05,kk   moved objOwnerGet(), objHandleShow(), objHandleTblShow()
                 to objLib.h
01t,07sep04,dcc  added accessCnt to OBJ_CORE.
01s,14sep04,lei  moved objOwnerSet() prototype to objLib.h
01r,28sep04,fr   removed objCreateRtn from objOpen (SPR 101349)
01q,16aug04,dcc  added _objClose() prototype.
01p,16jul04,aeg  added objArchLibInit() function prototype, and changed
		 OBJ_VERIFY() to invoke arch specific objVerify() (SPR# 98168).
01o,05jul04,dcc  added handle table growing support. 
01n,05may04,cjj  added objIdToObjHandle() definition.  Removed I960
		 preprocessor directives.
01m,29apr04,dcc  added objRtpIdToObjHandle() definition.
01l,09mar04,dcc  added WIND_OBJ_NAMED and WIND_OBJ_DELETE_ON_LAST_CLOSE 
	         attributes. Moved objNameSet(), objOwnerGet(), and 
		 objOwnerSet() prototypes from objLib.h
01k,04feb04,dcc  added reference counter to OBJ_CORE.
01j,06jan04,dcc  added OBJ_IS_REFERENCED macro
01i,26nov03,dcc  updated definition of OBJ_HANDLE
01h,31oct03,dcc  implemented modifications from code review.
01g,11sep03,dcc  added objHandleList member to the OBJ_CORE structure, and 
		 added handle table management support.
01f,21aug03,dcc  removed WIND_OBJ_DISTRIBUTED and WIND_OBJ_SHARED flags. Added
		 objOpen() prototype.
01e,05apr03,to   replace PD with RTP
01d,14mar03,dcc  ported from AE1.1
01c,15oct93,cd   added #ifndef _ASMLANGUAGE.
01c,10dec93,smb  removed objHelp
01b,22sep92,rrr  added support for c++
01a,04jul92,jcf  extracted from v1j objLib.h.
*/

#ifndef __INCobjLibPh
#define __INCobjLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* defines */

/* 
 * number of slots that an individual handle table is made of. This is 
 * the size by which an RTP's handle table grows each time that the
 * table is full, and a request for a new handle is made. This number must 
 * be a power of two. 
 * NOTE: Whenever this value is changed, the macro OBJ_TABLE_INDEX_GET() 
 * must be also updated with the number of bits that OBJ_HANDLE_ENTRY_MASK is
 * wide.
 */

#define OBJ_HANDLE_TBL_SIZE	256

/* number of individual handle tables that a master table references */

#define OBJ_MASTER_TBL_SIZE	(0x10000 / OBJ_HANDLE_TBL_SIZE) /* 256 */

/* a handle index is made of the 16 LSB of an OBJECT_HANDLE */

#define OBJ_HANDLE_INDEX_MASK	0xffff 

/* mask to get the index of an entry in an individual handle table */

#define OBJ_HANDLE_ENTRY_MASK	(OBJ_HANDLE_TBL_SIZE - 1) /* currently 0xff */ 

/* mask used to get the index of an individual handle table in a master table */
 
#define OBJ_HANDLE_TABLE_MASK	(OBJ_HANDLE_INDEX_MASK - OBJ_HANDLE_ENTRY_MASK)

/* get the full index from an object handle */

#define OBJ_INDEX_GET(objHdl) ((objHdl) & OBJ_HANDLE_INDEX_MASK)

/* get the entry index from an object handle */

#define OBJ_ENTRY_INDEX_GET(objHdl) ((objHdl) & OBJ_HANDLE_ENTRY_MASK)

/* get the table index from an object handle */

#define OBJ_TABLE_INDEX_GET(objHdl) (((objHdl) & OBJ_HANDLE_TABLE_MASK) >> 8) 


/* convert a specific object type's struct ptr to an OBJ_CORE ptr */

#define OBJ_RESOLVE(pObj) ((OBJ_CORE *)(pObj))

/* convert an OBJ_CORE ptr to a specific object type's struct ptr */

#define OBJ_UNRESOLVE(pObj) ((OBJ_ID)(pObj))

/* size of OBJ_CORE in bytes for architecture dependent routines */

#define WIND_OBJ_CORE_SIZE	0x40

/* assembler offsets */

#define WIND_OBJ_CORE_HANDLE_ATTRIBUTES 0x8
#define WIND_OBJ_CORE_CLASS_ID	0x30

/* object attributes */

#define WIND_OBJ_NAME_DYNAMIC  0x01	/* object name dynamically allocated */
#define WIND_OBJ_DEALLOC_MEM   0x02	/* free object memory on destroy */
#define WIND_OBJ_INSTRUMENTED  0x04	/* WindView instrumented object */
#define WIND_OBJ_DELETE_ON_LAST_CLOSE 0x08  /* object will be deleted on the 
					     * last close even if it is not 
					     * previously unlinked */	
#define WIND_OBJ_ORPHANED      0x10	/* unable to reclaim object */
#define WIND_OBJ_NO_RECLAIM    0x20	/* automatic reclamation disabled */
#define WIND_OBJ_PUBLIC	       0x40	/* object is public */
#define WIND_OBJ_NAMED	       0x80	/* object is named */
#define WIND_OBJ_DELETED       0x100    /* object is deleted */

#ifndef _ASMLANGUAGE
#include <vxWorks.h>
#include <dllLib.h>
#include <objLib.h>
#include <classLib.h>
#include <private/handleLibP.h>


/* variable declarations */

extern BOOL objOwnerLibInstalled;

extern FUNCPTR _func_ownerListLock;
extern FUNCPTR _func_ownerListUnlock;
extern VOIDFUNCPTR _func_handleFreeAll;
extern FUNCPTR _func_handleAlloc;
extern FUNCPTR _func_objCoreRelease;

typedef struct wind_obj		/* OBJ_CORE */
    {
    HANDLE		handle;		/* 0x00: handle */
    DL_LIST		ownerList;	/* 0x0c: list of owned objects */
    DL_NODE		ownerNode;	/* 0x14: owner list node */
    DL_NODE		classNode;	/* 0x1c: class list node */
    struct wind_obj *	ownerId;	/* 0x24: pointer to object's owner */
    RTP_ID		ownerRtpId;	/* 0x28: object owner's RTP */
    char *		name;		/* 0x2c: object name */
    struct wind_class *	pObjClass;	/* 0x30: pointer to object's class */
    DL_LIST		objHandleList;  /* 0x34: list of handles */
    UINT16		refCnt;		/* 0x3c: reference counter */
    UINT16		accessCnt;	/* 0x3e: access counter */
    } OBJ_CORE;

typedef struct handle_table_entry
    {
    DL_NODE		handleNode;
    OBJ_ID		entryObjId;
    OBJ_HANDLE		objHandle; 
    } HANDLE_TABLE_ENTRY;

/* 
 * the order of the members in HANDLE_TABLE is important. Some routines such
 * as objHandleFreeAll() rely on the fact that handleTable is the first
 * member.
 */

typedef struct handle_table
    {
    HANDLE_TABLE_ENTRY	handleTable [OBJ_HANDLE_TBL_SIZE];
    struct master_handle_table * pMasterTbl;
    } HANDLE_TABLE;

typedef struct master_handle_table
    {
    struct handle_table * pHandleTable [OBJ_MASTER_TBL_SIZE];
    int 		firstFree; /* index of most recent free slot */
    int 		size;      /* size of all handle tables combined */
    UINT32 		initIndex;
    UINT32		genNumber;
    SEM_ID 		hdlTblSemId;
    } MASTER_HANDLE_TABLE;


/*******************************************************************************
*
* OBJ_SET_DEALLOC_MEM - object memory to be freed when destroyed
*
* This macro sets the appropriate object attribute bit such that the
* object memory is deallocated when the object destroy routine is called
* through the automatic resource reclamation mechanism.
*
* This macro is only useful for object classes that register a destroy
* routine that requires a 'dealloc' parameter.
*
* RETURNS: N/A
*
* NOMANUAL
*/

#define OBJ_SET_DEALLOC_MEM(objId)					     \
    (									     \
    ((OBJ_CORE *)(objId))->handle.attributes |= WIND_OBJ_DEALLOC_MEM	     \
    )

/*******************************************************************************
*
* OBJ_RECLAIM_DISABLE - disable automatic resource reclamation
*
* This macro sets the appropriate object attribute bit such that the
* object will not be destroyed by the automatic resource reclamation
* mechanism.
*
* RETURNS: N/A
*
* NOMANUAL
*/

#define OBJ_RECLAIM_DISABLE(objId)					     \
    (									     \
    ((OBJ_CORE *)(objId))->handle.attributes |= WIND_OBJ_NO_RECLAIM	     \
    )

/*******************************************************************************
*
* OBJ_IS_RECLAIM_DISABLE - is object disabled from automatic RR?
*
* This macro checks whether the specified object is disabled from automatic
* resource reclamation.
*
* RETURNS: TRUE if object is disabled from RR, otherwise FALSE.
*
* NOMANUAL
*/

#define OBJ_IS_RECLAIM_DISABLE(objId)					    \
    (									    \
    (((OBJ_CORE *)(objId))->handle.attributes & WIND_OBJ_NO_RECLAIM) != 0   \
    )


/*******************************************************************************
*
* OBJ_INSTRUMENTATION_ENABLE - enable WindView instrumentation
*
* This macro sets the appropriate object attribute bit such that the
* WindView instrumentation is enabled for the specified object.
*
* RETURNS: N/A
*
* NOMANUAL
*/

#define OBJ_INSTRUMENTATION_ENABLE(objId)				     \
    (									     \
    ((OBJ_CORE *)(objId))->handle.attributes |= WIND_OBJ_INSTRUMENTED	     \
    )

/*******************************************************************************
*
* OBJ_INSTRUMENTATION_DISABLE - disable WindView instrumentation
*
* This macro clears the appropriate object attribute bit such that the
* WindView instrumentation is disabled for the specified object.
*
* RETURNS: N/A
*
* NOMANUAL
*/

#define OBJ_INSTRUMENTATION_DISABLE(objId)				     \
    (									     \
    ((OBJ_CORE *)(objId))->handle.attributes &= ~WIND_OBJ_INSTRUMENTED	     \
    )

/*******************************************************************************
*
* OBJ_VERIFY - check the validity of an object pointer
*
* This macro verifies the existence of the specified object by comparing the
* object's classId with the expected class id.  If the object does not check
* out, errno is set with the invalid id status.
*
* RETURNS: OK or ERROR if invalid object id
*
* NOMANUAL
*/

#ifdef INCLUDE_OBJ_LIB
#define OBJ_VERIFY(objId,classId)					     \
    (									     \
    objVerify (objId, classId)						     \
    )
#else
#define OBJ_VERIFY(objId, classId)                                           \
    (                                                                        \
    OK                                                                       \
    )
#endif	/* INCLUDE_OBJ_LIB */

/*******************************************************************************
*
* OBJ_IS_OWNER - does the object own any other objects?
*
* This macro checks whether the specified object is the owner of
* any other object.
*
* RETURNS: TRUE if object owns at least one other object, otherwise FALSE.
*
* NOMANUAL
*/

#define OBJ_IS_OWNER(objId)						    \
    (									    \
    DLL_FIRST (&((OBJ_CORE *)(objId))->ownerList) != NULL		    \
    )

/*******************************************************************************
*
* OBJ_IS_REFERENCED - is this object referenced by an object handle?
*
* This macro checks whether there is any object handle referencing this object.
*
* RETURNS: TRUE if object has at least one object handle, otherwise FALSE.
*
* NOMANUAL
*/

#define OBJ_IS_REFERENCED(objId)						    \
    (									    \
    DLL_FIRST (&((OBJ_CORE *)(objId))->objHandleList) != NULL		    \
    )

/*******************************************************************************
*
* OBJ_IS_PUBLIC - is the object public ?
*
* This macro checks if the object is public or not
*
* RETURNS: TRUE if object is public, otherwise FALSE.
*
* NOMANUAL
*/

#define OBJ_IS_PUBLIC(objId)						    \
    (									    \
    (((OBJ_CORE *)(objId))->handle.attributes & WIND_OBJ_PUBLIC) != 0	    \
    )

/*******************************************************************************
*
* OBJ_SET_PUBLIC - set an object as public
*
* This macro sets the bit WIND_OBJ_PUBLIC in OBJ_CORE.handle.attributes
*
* NOMANUAL
*/

#define OBJ_SET_PUBLIC(objId)						    \
    (									    \
    ((OBJ_CORE *)(objId))->handle.attributes |= WIND_OBJ_PUBLIC		    \
    )

/*******************************************************************************
*
* OBJ_IS_NAMED - is the object named ?
*
* This macro checks if the object is named or not. This macro is used to 
* determine if an object must follow the POSIX model for deletion 
* (i.e. unlink + close). 
*
* RETURNS: TRUE if object is public, otherwise FALSE.
*
* NOMANUAL
*/

#define OBJ_IS_NAMED(objId)						    \
    (									    \
    (((OBJ_CORE *)(objId))->handle.attributes & WIND_OBJ_NAMED) != 0	    \
    )

/*******************************************************************************
*
* OBJ_SET_NAMED - set an object as named
*
* This macro sets the bit WIND_OBJ_NAMED in OBJ_CORE.handle.attributes. The 
* setting of this bit indicates that an object must follow the POSIX model
* for deletion, i.e. must be unlinked and closed.
*
* NOMANUAL
*/

#define OBJ_SET_NAMED(objId)						    \
    (									    \
    ((OBJ_CORE *)(objId))->handle.attributes |= WIND_OBJ_NAMED		    \
    )


/*******************************************************************************
*
* OBJ_IS_DELETE_ON_LAST_CLOSE - is the object to be deleted on the last close?
*
* This macro checks if the object is set to be deleted on the last close, i.e.
* no need to be unlinked first.
*
* RETURNS: TRUE if object is public, otherwise FALSE.
*
* NOMANUAL
*/

#define OBJ_IS_DELETE_ON_LAST_CLOSE(objId)				    \
    (									    \
    (((OBJ_CORE *)(objId))->handle.attributes & 			    \
    WIND_OBJ_DELETE_ON_LAST_CLOSE) != 0	    			    	    \
    )

/*******************************************************************************
*
* OBJ_SET_DELETE_ON_LAST_CLOSE - set an object to be deleted on the last close
*
* This macro sets the bit WIND_OBJ_DELETE_ON_LAST_CLOSE in 
* OBJ_CORE.handle.attributes. The setting of this bit indicates that a named 
* object does not need to be unlinked before being deleted.
*
* NOMANUAL
*/

#define OBJ_SET_DELETE_ON_LAST_CLOSE(objId)				    \
    (									    \
    ((OBJ_CORE *)(objId))->handle.attributes |= 			    \
    WIND_OBJ_DELETE_ON_LAST_CLOSE		    			    \
    )

/*******************************************************************************
*
* OBJ_IS_DELETED - is the object deleted?
*
* This macro checks if the object has been already deleted or not. This macro
* is used to determine if an object's memory should be freed when it access
* counter is zero.
*
* RETURNS: TRUE if object is being deleted, otherwise FALSE.
*
* NOMANUAL
*/

#define OBJ_IS_DELETED(objId)					            \
    (									    \
    (((OBJ_CORE *)(objId))->handle.attributes & WIND_OBJ_DELETED) != 0      \
    )

/*******************************************************************************
*
* OBJ_SET_DELETED - set an object as deleted
*
* This macro sets the bit WIND_OBJ_DELETED in OBJ_CORE.handle.attributes
* The setting of this bit indicates that a the object has been deleted and its 
* memory is ready to be freed when its access counter is zero.
*
* NOMANUAL
*/

#define OBJ_SET_DELETED(objId)					            \
    (									    \
    ((OBJ_CORE *)(objId))->handle.attributes |= WIND_OBJ_DELETED	    \
    )

/* function declarations */

extern void *	objAlloc	(CLASS_ID classId);
extern void *	objAllocExtra	(CLASS_ID classId, unsigned nExtraBytes,
				 void ** ppExtra);
extern CLASS_ID objClassIdGet	(enum windObjClassType classType);
extern void	objCoreInit	(OBJ_CORE * pObjCore, CLASS_ID pObjClass);
extern STATUS	objCoreTerminate(OBJ_CORE * pObjCore);
extern void	objCoreUnlink	(OBJ_CORE * pObjCore, CLASS_ID classId);
extern OBJ_ID	objEach		(enum windObjClassType classType,
				 FUNCPTR routine, int arg, BOOL objPublic);
extern STATUS	objFree		(CLASS_ID classId, OBJ_ID objectId);
extern int	objInfo		(enum windObjClassType classType,
				 OBJ_ID objIdArray[],
				 int maxIds);
extern char *	objNamePtrGet	(OBJ_ID objId);
extern STATUS	objNamePtrSet	(OBJ_ID objId, char * name);
extern STATUS	objNameSet 	(OBJ_ID objId, const char *name);
extern STATUS	objVerify	(OBJ_ID objId, CLASS_ID classId);
extern STATUS	objGenericVerify(OBJ_ID objId);
extern STATUS	objRtpTaskDelete(RTP_ID rtpId);
extern STATUS	objOwnerSetBase (OBJ_CORE * pObj, OBJ_CORE * pOwnerObj);
extern STATUS	objLibInit	(FUNCPTR objMemAllocRtn, FUNCPTR objMemFreeRtn,
				 void * objMemPoolId, int options);
extern void	objOwnershipInit (void);
extern BOOL	objOwnerListInstalled (void);
extern STATUS	objArchLibInit	(void);
extern void	objShowInit (void);

extern STATUS	objCoreReclaim		(OBJ_CORE * pObjCore);
extern void	objCoreInvalidate	(OBJ_CORE * pObjCore);
extern OBJ_HANDLE objHandleAlloc 	(OBJ_ID objId, RTP_ID rtpId); 
extern OBJ_ID   objHandleToObjId 	(OBJ_HANDLE handle, RTP_ID rtpId, 
					 BOOL access);
extern STATUS   objHandleDelete	(OBJ_HANDLE handle, int options);

extern OBJ_ID   objOpen 	(const char * name, int flags, CLASS_ID classId,
			 	 ...);
extern STATUS	objHandleClose 	(OBJ_HANDLE handle, RTP_ID rtpId); 
extern STATUS   objClose	(OBJ_ID objId, CLASS_ID	classId);
extern STATUS	objUnlink	(const char * name, CLASS_ID classId); 

extern void	objHandleTblTerminate	(RTP_ID rtpId);
extern STATUS 	objHandleTblCreate	(RTP_ID rtpId);
extern OBJ_HANDLE objIdToObjHandle   	(OBJ_ID objId, RTP_ID rtpId);
extern OBJ_HANDLE objRtpIdToObjHandle   (RTP_ID rtpId);
extern STATUS 	_objClose		(OBJ_CORE * pObjCore, int level, 
				         BOOL dealloc);
extern void * 	objMemAlloc		(unsigned nBytes);
extern STATUS 	objMemFree		(char * pBuffer);
extern STATUS 	objCoreRelease		(OBJ_ID objId);
extern STATUS 	objRtpLibInit 		(void);

#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCobjLibPh */
