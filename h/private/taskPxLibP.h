/* taskPxLibP.h - header for POSIX thread support in VxWorks task */

/*
 * Copyright (c) 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,05aug05,jln  added taskPxWindTickAnnounceHook
01a,25jun05,jln  Created
*/

#ifndef __INCtaskPxLibPh
#define __INCtaskPxLibPh

#ifdef __cplusplus
extern "C" {
#endif

#include <private/taskLibP.h>
#include <private/taskPxLibCommonP.h>

/* Function declarations */

extern void 		taskPxLibInit (void);
extern STATUS 		taskPxAttrSet (int tid, void * pAttr);
extern STATUS           taskPxAttrGet (int tid, void * pAttr);
extern void		taskPxWindTickAnnounceHook (WIND_TCB *);
#ifdef __cplusplus
}
#endif

#endif /* __INCtaskPxLibPh */
