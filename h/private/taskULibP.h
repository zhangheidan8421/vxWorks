/* taskLibP.h - VxWorks user tasking library private interface header */

/*
 * Copyright (c) 2003-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. 
 */

/*
modification history
--------------------
01h,03jun05,kk   fix syntax error (SPR# 109861)
01g,22sep04,md   CI changes
01f,05may04,cjj  moved errorStatus from WIND_TCB to WIND_UTCB.
01e,29apr04,dcc  added WIND_UTCB_LOCK_CNT and WIND_UTCB_REL_CPU offsets.
01d,29mar04,aeg  added argv pointer to WIND_TCB.
01c,27oct03,md   added kernel/user tcb separation
01b,08oct03,aeg  added definitions for task create/delete hooks;
		 moved TASK_UXXX() macros from taskLib.h
01a,24sep03,aeg  written
*/

#ifndef __INCtaskLibPh_u
#define __INCtaskLibPh_u

#include "vxWorks.h"

#ifdef __cplusplus
extern "C" {
#endif

#define	WIND_UTCB_VERSION	1

#if !defined(_WRS_KERNEL)

#include "semLib.h"
#include "tlsLib.h"		/* TLS_KEY */

/* defines */

#define VX_MAX_TASK_CREATE_RTNS 16
#define VX_MAX_TASK_DELETE_RTNS 16

#endif /* _WRS_KERNEL */

/* offsets */

#define WIND_UTCB_LOCK_CNT	0x08 /* (WIND_UTCB *)->kernel.lockCnt */
#define WIND_UTCB_REL_CPU	0x0c /* (WIND_UTCB *)->kernel.relinquishCpu */

#ifndef _ASMLANGUAGE

/* typedefs */

/*
 * The user level TCB is available in both the kernel and the RTP environments.
 * It is however divided into two sections, a kernel section and a user section.
 * In the kernel environment *only* the kernel section of the TCB is visible,
 * whereas in the RTP environment both sections are visible.
 *
 * The user level TCB is also known by two different names.  In the RTP
 * environment it goes by the name "struct windTcb" or WIND_TCB, whereas in
 * the kernel it is known as "struct windUTcb" or WIND_UTCB.  This is done
 * so that the RTP environment remains similar to the kernel environment.
 *
 * WARNING, any changes to the kernel portion of the TCB structure must be
 * backwards compatible with previous versions otherwise binary backwards
 * compatibility will be broken.  In essence this basically means that fields
 * may only ever be added to the kernel portion and never removed.  The user
 * level portion of the TCB may change at any time.
 *
 * A TCB version identifier is present as the first member of the TCB
 * structure.  The version identifier is initialized by the task init code
 * to the value of the WIND_UTCB_VERSION define.  The version member must be
 * checked by the kernel to verify the TCB member they are interested in is
 * available in the TCB version being used.
 *
 */

#if defined(_WRS_KERNEL)

typedef struct windUTcb		  /* user-level version of task control block */

#else  /*_WRS_KERNEL */

typedef struct windTcb		  /* user-level version of task control block */

#endif /* _WRS_KERNEL */
    {
    int	version;	  	  /* 0x00: TCB structure version identifier */
    struct
	{
	int	tid;		  /* 0x04: handle returned by _taskOpen */
	volatile 
	UINT	lockCnt;	  /* 0x08: preemption lock count */
	volatile 
	BOOL    relinquishCpu;	  /* 0x0c: higher pri task waiting for cpu? */
	int	errorStatus;	  /* 0x10: task's most recent 'errno' */
	} kernel;
#if defined(_WRS_KERNEL)
    } WIND_UTCB;
#else  /* _WRS_KERNEL */

    /*
     * The following portion of the WIND_TCB structure is only visible in
     * the RTP environment and is not accessible by the kernel.
     */
    struct
	{
	volatile
	UINT	safeCnt;	  /* 0x14: safe-from-delete count */
	SEM_ID	safeQFlushSemId;  /* 0x18: semaphore to flush deleters */
	BOOL	safeDeleteFlag;   /* 0x1c: task delete requested? */
	int	exitCode; 	  /* 0x20: exit code from entry function */
	FUNCPTR	entryPoint;	  /* 0x24: application task entry function */
	char *	pName;		  /* 0x28: ptr to task's name */
	char *	pTls;		  /* 0x2c: ptr to TLS area */
	char ** argv;		  /* 0x30: ptr to argument buffer */
	} user;
    } WIND_TCB;
#endif /* _WRS_KERNEL */

/* variable declarations */

#if !defined(_WRS_KERNEL)

extern TLS_KEY windTcbKey;
extern FUNCPTR taskCreateTable[];
extern FUNCPTR taskDeleteTable[];


/*******************************************************************************
*
* TASK_RTPLOCK - lock preemption for calling task
*
* Utilization of this macro invokes prevents other tasks in the same RTP from
* preempting the current task.  In other words, a task residing in
* another RTP can still preempt the current task.
*
* This macro should only be used if it has already been determined that the
* current task has a valid WIND_TCB structure.  Otherwise the taskRtpLock()
* function should be used.
*
* RETURNS: N/A
*
* NOMANUAL
*/

#define TASK_RTPLOCK()							     \
    do									     \
	{								     \
	_WRS_ASM ("");	/* avoid re-ordering */				     \
	  								     \
	((WIND_TCB *) TLS_VALUE_GET (windTcbKey))->lockCnt++;		     \
           								     \
	_WRS_ASM ("");							     \
	} while (0)

/*******************************************************************************
*
* TASK_RTPUNLOCK - unlock preemption for calling task
*
* This macro invokes the taskRtpUnlock (2) routine.
*
* NOMANUAL
*/

#define TASK_RTPUNLOCK()						\
    (									\
    taskRtpUnlock ()							\
    )

/*******************************************************************************
*
* TASK_RTPSAFE - guard self from deletion
*
* Utilization of this macro protects the calling task from deletion by other
* tasks in the same RTP.  In other words, a task residing in another RTP can
* still delete the current task.
*
* This macro should only be used if it has already been determined that the
* current task has a valid WIND_TCB structure.  Otherwise the taskUSafe()
* function should be used.
*
* RETURNS: N/A
*
* NOMANUAL
*/

#define TASK_RTPSAFE()							     \
    do									     \
	{								     \
	_WRS_ASM ("");							     \
	  								     \
	((WIND_TCB *) TLS_VALUE_GET (windTcbKey))->safeCnt++;		     \
           								     \
	_WRS_ASM ("");							     \
	} while (0)

/*******************************************************************************
*
* TASK_RTPUNSAFE - unguard self from deletion
*
* This macro invokes the taskUnsafe (2) routine.
*
* NOMANUAL
*/

#define TASK_RTPUNSAFE()						\
    (									\
    taskUnsafe ()							\
    )


/* function declarations */

extern STATUS taskTlsOfInitTaskCreate (void);

#endif /* !_WRS_KERNEL */

#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCtaskLibPh_u */
