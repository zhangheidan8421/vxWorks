/* rtpPthreadLibP.h - private header for user-side POSIX threads (pthreads) */
 
/* Copyright 2004 Wind River Systems, Inc. */
 
/*
modification history
--------------------
01a,07sep04,pad  Created to share common type and macro definitions.
*/

/*
DESCRIPTION
This header file holds information that must be known to both the user-side
library for POSIX threads and the support code for this library located in the
kernel. These information, type definitions and macro definitions, represents
a convention between the two sides.
*/
 
#ifndef __INCrtpPthreadLibPh
#define __INCrtpPthreadLibPh

#if defined(__cplusplus)
extern "C" {
#endif	/* __cplusplus */

/* defines */

/*
 * POSIX defines zero as the return value on success for many routines. This
 * macro makes the code more readable.
 */

#define _RETURN_PTHREAD_SUCCESS		0

/* typedefs */

/*
 * The PTHREAD_INFO structure is used internally by the user-side pthread
 * library to pass information to the kernel code handling the cancellability
 * feature of pthreads for those cancellation points which code actually is
 * in the kernel. In the version 1 of this implementation, this allows the
 * kernel code to get to the canceltype field of the user-side thread's
 * internal data structure via the caller's RTP's rtpTlsCurrent variable:
 *
 *                               RTP Space
 *                               ---------
 *  
 *  rtpTlsCurrent
 *  +-----------+     TLS (per task)
 *  |   &TLS   -|--->+---------------+
 *  +-----------+ (0)|               |
 *                   +---------------+
 *                   :               :
 *                   +---------------+     pthread data struct
 *                (x)|&pthread data -|--->+-------------------+
 *                   +---------------+ (0)|                   |
 *                   :               :    +-------------------+
 *                                        :                   :
 *                                        +-------------------+
 *                                     (y)|    cancel type    |
 *                                        +-------------------+
 *                                        :                   :
 *
 * The address of the rtpTlsCurrent variable, the slot number (x) of the
 * current pthread data structure in the Task Local Storage area pointed to by
 * the rtpTlsCurrent variable, as well as the offset (y) to the user pthread
 * data structure's cancel type are the information required for the version
 * 1 of this implementation.
 *
 * The order of the fields in the PTHREAD_INFO structure is crucial for the
 * good functioning of the pthread cancellability mechanism. By convention
 * the version field is always the first in the structure since it drives the
 * interpretation of the rest of the structure by the kernel-side code.
 */


typedef struct
    {
    int version;		/* version of this structure (currently 1) */
    void * rtpTlsCurrentAddr;	/* address of the RTP's rtpTlsCurrent var */
    int tlsKey;			/* key of the pthread data in the TLS */
    int cancelTypeOffset;	/* offset (in bytes) to canceltype field */
    } PTHREAD_INFO;

#if defined(__cplusplus)
}
#endif	/* __cplusplus */

#endif /* __INCrtpPthreadLibPh */
