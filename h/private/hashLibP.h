/* hashLibP.h - private hash table library header */

/* Copyright 1998-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,26jun03,to   ported form AE1.1.
*/

#ifndef __INChashLibPh
#define __INChashLibPh

#ifdef __cplusplus
extern "C" {
#endif

#include "sllLib.h"
#include "private/handleLibP.h"

/* size of hash table given number of elements in hash table log 2 */

#define HASH_TBL_SIZE(sizeLog2)						\
	(((1 << (sizeLog2)) * sizeof (SL_LIST)) + sizeof (HASH_TBL))

/* type definitions */

typedef struct hashtbl		/* HASH_TBL */
    {
    HANDLE	handle;			/* handle management */
    int		elements;		/* number of elements in table */
    FUNCPTR	keyCmpRtn;		/* comparator function */
    FUNCPTR	keyRtn;			/* hash function */
    int		keyArg;			/* hash function argument */
    SL_LIST *	pHashTbl;		/* pointer to hash table array */
    } HASH_TBL;

#ifdef __cplusplus
}
#endif

#endif /* __INChashLibPh */
