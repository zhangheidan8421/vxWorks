/* dbgLibP.h - private debugging facility header */

/* Copyright 1984-2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01n,17mar04,bpn  Exported several functions for command line interpreter.
01m,09feb04,elg  Add definition of _dbgInstSizeGet().
01l,25nov03,bpn  Added several prototypes.
01k,21oct03,bpn  Moved several definitions to dbgTaskLibP.h.
01j,05mar03,bpn  Removed some definitions and external declatations.
01i,28feb03,bpn  Moved in definitions from dbgLib.h. Added declaration of
		 dbgBpAdd(), dbgBpRemove() and dbgHwBpSet(). Clean up.
01h,18jan99,elg  Authorize breakpoints in branch delay slot (SPR 24356).
01g,13jan98,dbt  modified for new breakpoint scheme
01f,20aug93,dvs  removed declaration of dsmNbytes(), added include dsmLib.h 
		 (SPR #2266).
01e,01oct92,yao  added to pass pEsf, pRegSet to _dbgStepAdd().
01d,22sep92,rrr  added support for c++
01c,21jul92,yao  added declaration of trcDefaultArgs.  changed IMPORT
		 to extern.
01b,06jul92,yao  made user uncallable globals started with '_'.
01a,16jun92,yao  written based on mc68k dbgLib.c ver08f.
*/

#ifndef __INCdbgLibPh
#define __INCdbgLibPh

#ifdef __cplusplus
extern "C" {
#endif

#include <vxWorks.h>
#include <regs.h>
#include <symLib.h>
#include <wdb/wdbDbgLib.h>	/* for BREAK_ESF struct use in dbg<arch>Lib.c */

/* Defines */

#undef INST_CMP
#define INST_CMP(addr,inst,mask)  ((*(addr) & (mask)) == (inst))	/* XXX bpn - should be in dbgArchLibP.h */

/* Function declarations */

extern STATUS	dbgBpListPrint (void * ctxId, UINT ctx, BOOL compat);
extern void	dbgCallPrint (INSTR * callAdrs, int funcAdrs, int nargs,
			      int * args, int taskId, BOOL isKernelAdrs);
extern STATUS	dbgStackTrace (int taskId, const char * errorStr);
extern STATUS	dbgDisassemble (INSTR *	addr, int count, SYMTAB_ID symTabId,
				const char * errorStr);
extern int	_dbgInstSizeGet (INSTR * pBrkInst);

extern STATUS	dbgBpEpCoreRtn (INSTR * addr, int ctxId, UINT ctxType,
				UINT bpAction, int count, BOOL hard, 
				VOIDFUNCPTR callRtn, int callArg,
				const char * errorStr);
extern STATUS	dbgTaskTraceCoreRtn (int taskId, const char * errorStr);

#ifdef __cplusplus
}
#endif

#endif /* __INCdbgLibPh */
