/* usrLibP.h - private header file for user interface library */

/* Copyright 2003-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,25jan05,bpn  Added changeFpReg(), changeReg(), substrcmp() prototypes.
01d,08nov04,bpn  Added taskRegsModify() prototype (SPR#103922).
01c,02jun04,bpn  Added usrModuleLoad() prototype.
01b,18may04,bpn  Added function prototypes.
01a,12nov03,jn   written
*/

#ifndef __INCusrLibPh
#define __INCusrLibPh

#ifdef __cplusplus
extern "C" {
#endif

#include <moduleLib.h>
#include <objLib.h>
#include <symLib.h>

/* function declarations */

extern STATUS	lkAddrInternal (void * addr, SYMTAB_ID symTabId);
extern STATUS	objectShow (OBJ_ID objId, int level, const char * errorStr);
extern STATUS	memoryDump (void * adrs, int nunits, int width,
			    const char * errorStr);
extern STATUS	memoryModify (void * adrs, int width, const char * errorStr);
extern MODULE_ID 	usrModuleLoad (int fd, int loadFlags,
				       BOOL noAbort, const char * errorStr);
extern STATUS	taskRegsModify (char * regName, int taskId,
				const char * errorStr);

extern STATUS	changeFpReg (char * pPrompt, double value, double * pValue);
extern STATUS	changeReg (char * pPrompt, void * pValue, int width);
extern BOOL	substrcmp (char * s, char * s1);

/* usrFsShellCmd.c functions below this comment */

extern STATUS	usrFsShellCmdInit ();

#ifdef __cplusplus
}
#endif

#endif /* __INCusrLibPh */
