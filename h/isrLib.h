/* isrLib.h - ISR objects header */

/* Copyright 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,29oct03,kam  ISR Object code inspection mods
01b,04jun03,kam  removed definition of ISR_ID and moved to vxWind.h
01a,03jun03,kam  written.
*/

#ifndef __INCisrLibh
#define __INCisrLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */
    
#include "vxWorks.h"
#include "vwModNum.h"

/* status codes */

#define S_isrLib_ISR_NOT_INIT			(M_isrLib | 1)
#define S_isrLib_INVALID_PARAM			(M_isrLib | 2)

/* typedefs */

typedef struct isr_desc
    {
    ISR_ID       isrId;            /* ISR_ID                               */
    char *       name;             /* name                                 */
    UINT         isrTag;           /* interrupt tag                        */
    UINT         count;            /* # of times this ISR has been invoked */
    UINT    	 serviceCount;	   /* # of times this ISR has returned OK  */
    UINT64	 cpuTime;	   /* cpu time spent in ISR 		   */
    int          options;          /* ISR object options                   */  
    
    FUNCPTR      handlerRtn;       /* pointer to handler routine           */
    int          arg;              /* parameter to be passed to routine    */
    } ISR_DESC;
    
/* externs */

/* variables */

extern ISR_ID isrIdCurrent;

/* routines */
    
extern ISR_ID isrCreate (char * name, UINT isrTag, FUNCPTR handler,
        int parameter, UINT options);
extern STATUS isrDelete (ISR_ID isrId);
extern STATUS isrInvoke (ISR_ID isrId);
extern ISR_ID isrIdSelf (void);
extern STATUS isrInfoGet (ISR_ID isrId, ISR_DESC * pIsrDesc);

extern STATUS isrShow (ISR_ID isrId);

#ifdef __cplusplus
}
#endif

#endif /* __INCisrLibh */
