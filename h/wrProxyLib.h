/* wrProxyLib.h - Wind River proxy interface header */

/*
 * Copyright (c) 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */
 
/*
modification history
--------------------
01b,22aug05,dbt  Added TIPC support for host proxy.
01a,15apr05,dbt  written
*/

#ifndef __INCwrProxyLibh
#define __INCwrProxyLibh

#ifdef __cplusplus
extern "C" {
#endif

/* function declarations */

void	wrProxyUdpSockRegister (void);
void	wrProxyTipcSockRegister (void);
#ifdef	HOST
void	wrProxyTcpSockRegister (void);
STATUS	wrProxyLibInit (unsigned short portNumber);
#else	/* HOST */
STATUS	wrProxyLibInit (UINT16 portNumber, UINT32 priority,
			UINT32 stackSize, UINT32 options);
#endif	/* HOST */

#ifdef __cplusplus
}
#endif

#endif	/* __INCwrProxyLibh */
