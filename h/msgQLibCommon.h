/* msgQLibCommon.h - common message queue library header file */

/* Copyright 2003-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01g,03sep04,bwa  added S_msgQLib_INVALID_MSG_COUNT errno.
01f,22apr04,dcc  added 'context' to msgQOpen() parameters.
01e,30mar04,ans  added MSG_Q_INTERRUPTIBLE  option.
01d,22mar04,dcc  added msgQClose() and msgQUnlink() prototypes.
01c,25nov03,aeg  removed fields from MSG_Q_INFO in user-space.
01b,31oct03,aeg  added S_msgQLib_ILLEGAL_OPTIONS, S_msgQLib_ILLEGAL_PRIORITY
		 and S_msgQLib_UNSUPPORTED_OPERATION errnos.
01a,24oct03,aeg  written (based on kernel version 02o).
*/

#ifndef __INCmsgQLibCommonh
#define __INCmsgQLibCommonh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "vwModNum.h"
#include "objLib.h"


/* generic status codes */

#define S_msgQLib_INVALID_MSG_LENGTH		(M_msgQLib | 1)
#define S_msgQLib_NON_ZERO_TIMEOUT_AT_INT_LEVEL	(M_msgQLib | 2)
#define S_msgQLib_INVALID_QUEUE_TYPE		(M_msgQLib | 3)
#define S_msgQLib_ILLEGAL_OPTIONS		(M_msgQLib | 4)
#define S_msgQLib_ILLEGAL_PRIORITY		(M_msgQLib | 5)
#define S_msgQLib_UNSUPPORTED_OPERATION		(M_msgQLib | 6)
#define S_msgQLib_INVALID_MSG_COUNT	        (M_msgQLib | 7)

/* message queue options */

#define MSG_Q_TYPE_MASK	0x01	/* mask for pend queue type in options */
#define MSG_Q_FIFO	0x00	/* tasks wait in FIFO order */
#define MSG_Q_PRIORITY	0x01	/* tasks wait in PRIORITY order */
#define MSG_Q_EVENTSEND_ERR_NOTIFY 0x02 /* notify when eventRsrcSend fails */
#define MSG_Q_INTERRUPTIBLE 0x04  /* interruptible on RTP signal */


/* message send priorities */

#define MSG_PRI_NORMAL	0	/* normal priority message */
#define MSG_PRI_URGENT	1	/* urgent priority message */

/* message queue typedefs */

typedef struct			/* MSG_Q_INFO */
    {
    int     numMsgs;		/* OUT: number of messages queued */
    int     numTasks;		/* OUT: number of tasks waiting on msg q */

    int     sendTimeouts;	/* OUT: count of send timeouts */
    int     recvTimeouts;	/* OUT: count of receive timeouts */

    int     options;		/* OUT: options with which msg q was created */
    int     maxMsgs;		/* OUT: max messages that can be queued */
    int     maxMsgLength;	/* OUT: max byte length of each message */

#ifdef _WRS_KERNEL
    int     taskIdListMax;	/* IN: max tasks to fill in taskIdList */
    int *   taskIdList;		/* PTR: array of task ids waiting on msg q */

    int     msgListMax;		/* IN: max msgs to fill in msg lists */
    char ** msgPtrList;		/* PTR: array of msg ptrs queued to msg q */
    int *   msgLenList;		/* PTR: array of lengths of msgs */
#endif /* _WRS_KERNEL */

    } MSG_Q_INFO;


/* function declarations */

extern MSG_Q_ID msgQCreate 	(int maxMsgs, int maxMsgLength, int options);
extern STATUS 	msgQDelete 	(MSG_Q_ID msgQId);
extern STATUS 	msgQSend 	(MSG_Q_ID msgQId, char *buffer, UINT nBytes,
			  	 int timeout, int priority);
extern int 	msgQReceive 	(MSG_Q_ID msgQId, char *buffer, UINT maxNBytes,
			     	 int timeout);
extern STATUS 	msgQInfoGet 	(MSG_Q_ID msgQId, MSG_Q_INFO *pInfo);
extern int 	msgQNumMsgs 	(MSG_Q_ID msgQId);
extern MSG_Q_ID msgQOpen	(const char * name, int maxMsgs, 
				 int maxMsgLength, int options, int mode,
				 void * context);
extern STATUS 	msgQClose	(MSG_Q_ID msgQId);
extern STATUS 	msgQUnlink	(const char * name);

#ifdef __cplusplus
}
#endif

#endif /* __INCmsgQLibCommonh */
