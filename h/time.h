/* time.h - POSIX time header */

/*
 * Copyright (c) 1992-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement.
 */

/*
modification history
--------------------
01o,18aug05,mcm  Merging contents of timeCommon.h with time.h.
01n,01mar05,gls  moved _POSIX macro definitions to limits.h (SPR #105358)
                 removed _POSIX_INTERVAL_MAX definition
01m,04dec03,pad  Further re-arrangement of content with share/timeCommon.h
01l,02dec03,ans  removed ANSI C definition.
01k,01oct03,ans  Rearranged contents with share/h/timeCommon.h.
01j,00oct00,sn   Removed defns of size_t etc.
01i,17feb99,mrs  Add C++ support for NULL, (SPR #25079).
01h,13aug96,dbt  modifed comment for tm_sec (SPR #4436).
		 Updated copyright.
01g,02jul96,dbt  added #include "vxWorks.h" (SPR #4370).
01f,14jul94,dvs  changed def of CLOCKS_PER_SEC to sysClkRateGet() (SPR# 2486).
01f,29nov93,dvs  folded in timers.h, made draft 14 POSIX compliant
01e,15oct92,rrr  silenced warnings.
01d,22sep92,rrr  added support for c++
01c,31jul92,gae  added undef of _TYPE_timer_t.
01b,27jul92,gae  added _TYPE_timer_t, CLOCK_REALTIME, TIMER_ABSTIME.
01a,22jul92,smb  written
           +rrr
*/

#ifndef __INCtimeh
#define __INCtimeh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"

#include "types/vxANSI.h"
#include "sigevent.h"
#include "objLib.h"
#include "limits.h"

#ifndef NULL
#if defined __GNUG__
#define NULL (__null)
#else
#if !defined(__cplusplus) && 0
#define NULL ((void*)0)
#else
#define NULL (0)
#endif
#endif
#endif

#ifdef  _TYPE_timer_t
_TYPE_timer_t;
#undef  _TYPE_timer_t
#endif

#ifdef _TYPE_clock_t
_TYPE_clock_t;
#undef _TYPE_clock_t
#endif

#ifdef _TYPE_time_t
_TYPE_time_t;
#undef _TYPE_time_t
#endif

typedef int clockid_t;

#define CLOCKS_PER_SEC	sysClkRateGet()

#define CLOCK_REALTIME	0x0	/* system wide realtime clock */
#define TIMER_ABSTIME	0x1	/* absolute time */
#define TIMER_RELTIME   (~TIMER_ABSTIME)        /* relative time */

struct timespec
    {
    					/* interval = tv_sec*10**9 + tv_nsec */
    time_t tv_sec;			/* seconds */
    long tv_nsec;			/* nanoseconds (0 - 1,000,000,000) */
    };

struct itimerspec
    {
    struct timespec it_interval;	/* timer period (reload value) */
    struct timespec it_value;		/* timer expiration */
    };

struct tm
    {
    int tm_sec;		/* seconds after the minute     - [0, 59] */
    int tm_min;		/* minutes after the hour       - [0, 59] */
    int tm_hour;	/* hours after midnight         - [0, 23] */
    int tm_mday;	/* day of the month             - [1, 31] */
    int tm_mon;		/* months since January         - [0, 11] */
    int tm_year;	/* years since 1900     */
    int tm_wday;	/* days since Sunday            - [0, 6] */
    int tm_yday;	/* days since January 1         - [0, 365] */
    int tm_isdst;	/* Daylight Saving Time flag */
    };

/* function declarations */

extern uint_t      _clocks_per_sec(void);
extern char *	   asctime (const struct tm *_tptr);
extern clock_t	   clock (void);
extern char *	   ctime (const time_t *_cal);
extern double	   difftime (time_t _t1, time_t _t0);
extern struct tm * gmtime (const time_t *_tod);
extern struct tm * localtime (const time_t *_tod);
/* function declarations */

extern int 	timer_create (clockid_t clock_id, struct sigevent *evp,
			      timer_t *ptimer);
extern int 	timer_delete (timer_t timerid);
extern int 	timer_gettime (timer_t timerid, struct itimerspec *value);
extern int 	timer_settime (timer_t timerid, int flags,
		               const struct itimerspec *value,
			       struct itimerspec *ovalue);
extern int 	timer_getoverrun (timer_t timerid);

extern int      timer_connect (timer_t timerid, VOIDFUNCPTR routine, int arg);
extern int      timer_cancel (timer_t timerid);
extern int      nanosleep (const struct timespec *rqtp, struct timespec *rmtp);
extern timer_t  timer_open (const char * name, int mode, clockid_t clockId, 
                            struct sigevent * evp, void * context);
extern STATUS   timer_close (timer_t timerId);
extern STATUS   timer_unlink (const char * name);

extern int	clock_gettime (clockid_t clock_id, struct timespec *tp);
extern int	clock_settime (clockid_t clock_id, const struct timespec *tp);
extern int	clock_getres (clockid_t clock_id, struct timespec *res);

extern time_t	   mktime (struct tm *_tptr);
extern size_t	   strftime (char *_s, size_t _n, const char *_format,
		   	      const struct tm *_tptr);
extern time_t	   time (time_t *_tod);

extern int      timer_show (timer_t timerid);
extern STATUS   timer_modify (timer_t timerId, struct sigevent * pSigev);

#if _EXTENSION_POSIX_REENTRANT		/* undef this for ANSI */

extern int	   asctime_r(const struct tm *_tm, char *_buffer,
			     size_t *_buflen);
extern char *	   ctime_r (const time_t *_cal, char *_buffer, size_t *_buflen);
extern int	   gmtime_r (const time_t *_tod, struct tm *_result);
extern int	   localtime_r (const time_t *_tod, struct tm *_result);

#endif	/*  _EXTENSION_POSIX_REENTRANT */

#ifdef __cplusplus
}
#endif

#endif /* __INCtimeh */
