/* pwfComponent.h - protocol framework component header file */

/* Copyright 1999 Wind River Systems, Inc. */

/* 
modification history
--------------------
01c,30oct99,koz  Added component object get by protocol routine
01b,15sep99,koz  Added component delete interface
01a,16aug99,koz  Converted to a framework file
*/

#ifndef __INCpwfComponenth
#define __INCpwfComponenth

#ifdef __cplusplus
extern "C" {
#endif

#include "pfw/pfw.h"

/* typedefs */

/* 
 * The protocol framework is based on independent plugin objects. A component 
 * object is a type of plugin object. The data structure for a component
 * object is defined below.
 */

typedef struct pfwComponentObj
    {
    PFW_PLUGIN_OBJ pluginObj;    /* generic plugin object */
    struct pfwLayerObj *layerObj;/* pointer to the layer obj of the component */
    unsigned int protocol;       /* RFC 1700 defined protocol id */
    unsigned int sequence;       /* set and used by the layer to indicate */
                                 /* in which order the component is to be */
                                 /* traversed within the layer */
    } PFW_COMPONENT_OBJ;

/* function declarations */

extern STATUS             pfwComponentAdd (PFW_COMPONENT_OBJ *componentObj);
extern STATUS             pfwComponentDelete (PFW_COMPONENT_OBJ *componentObj);
extern PFW_COMPONENT_OBJ *pfwComponentObjGetByName (PFW_OBJ *pfwObj, 
						    char *name);
extern PFW_COMPONENT_OBJ *pfwComponentObjGetByProtocol (PFW_OBJ *pfwObj, 
						        unsigned int protocol);
extern STATUS             pfwComponentListShow (PFW_OBJ *pfwObj);
extern STATUS             pfwComponentShow (PFW_COMPONENT_OBJ *componentObj);

#ifdef __cplusplus
}
#endif

#endif /* __INCpwfComponenth */

