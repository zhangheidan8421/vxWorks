/* pfwStack.h - Protocol framework stack header file */

/* Copyright 1999 Wind River Systems, Inc. */

/* 
modification history
--------------------
01g,15feb00,sj  added pfwPluginObjStateLock()
01f,17nov99,koz  Changed the return type of the pfwPluginObjStateRelease
                 routine to STATUS
01e,06nov99,sj   changed pfwPluginObjectStateRelease to pfwPluginObjStateRelease
01d,11oct99,koz  Made callback fuction expilicit in asynchronous function
                 signitures
01c,16sep99,sj   brought in pfwStackAdd(Delete)Done from private file
01b,26aug99,sj   added pfwStackObjPfwGet() pfwLayerStateGet and pfwStackShow
01a,xxxxxxx,koz  created
*/

#ifndef __INCpfwStackh
#define __INCpfwStackh

#ifdef __cplusplus
extern "C" {
#endif

#include "pfw/pfw.h"
#include "pfw/pfwProfile.h"

/* typedefs */

typedef unsigned int PFW_STACK_ID;
typedef enum pfwStackStatus 
    {
    PFW_STACK_ADD_IN_PROGRESS, 
    PFW_STACK_READY, 
    PFW_STACK_DELETE_IN_PROGRESS
    } PFW_STACK_STATUS;

/*
 * The framework provides asynchronous functions for adding and deleting
 * stack instances. The following defines the callback functions for these
 * operations. The stack object accepts callback functions in the signiture of
 * pfwStackAdd function.
 */

typedef struct pfwStackObjCallbacks PFW_STACK_OBJ_CALLBACKS;

struct pfwStackObjCallbacks
    {
    void (*stackAddComplete)      /* called by a stack object to inform */
         (void *userHandle,       /* stack add processing is completed */
          PFW_STACK_OBJ *);
    void (*stackDeleteComplete)   /* called by a stack object to inform */
         (void *userHandle,       /* stack delete processing is completed*/
          PFW_STACK_OBJ *);        
    void (*stackAddError)         /* called by a stack object to inform */
	 (void *userHandle,       /* an error occurred during stack add */
	  PFW_STACK_OBJ *);
    void (*stackDeleteError)      /* called by a stack object to inform */
         (void *userHandle,       /* an error occurred during stack delete */ 
          PFW_STACK_OBJ *);          
    };


/* function declarations */

extern PFW_STACK_OBJ* pfwStackAdd (PFW_PROFILE_OBJ *profileObj, 
                                   PFW_STACK_OBJ_CALLBACKS *callbacks,
				   void *userHandle);
extern STATUS         pfwStackDelete (PFW_STACK_OBJ *stackObj);
extern void           pfwStackDestroy (PFW_STACK_OBJ *stackObj);
extern PFW_STACK_ID   pfwStackIdGet (PFW_STACK_OBJ *stackObj);
extern PFW_STACK_OBJ* pfwStackObjGet (PFW_OBJ *pfw, PFW_STACK_ID stackId);
extern STATUS         pfwStackPluginObjAdd (PFW_STACK_OBJ *stackObj,
					    PFW_PLUGIN_OBJ *pluginObj);
extern STATUS         pfwStackPluginObjDelete (PFW_STACK_OBJ *stackObj,
					       PFW_PLUGIN_OBJ *pluginObj);
extern PFW_PLUGIN_OBJ_STATE 
                     *pfwPluginObjStateGet (PFW_STACK_OBJ *stackObj,
		                            PFW_PLUGIN_OBJ *pluginObj);
extern PFW_PLUGIN_OBJ_STATE 
                     *pfwPluginObjStateExclusiveGet (PFW_STACK_OBJ *stackObj,
						     PFW_PLUGIN_OBJ *pluginObj);
extern STATUS         pfwPluginObjStateLock (PFW_PLUGIN_OBJ_STATE *state);
extern STATUS         pfwPluginObjStateRelease (PFW_PLUGIN_OBJ_STATE *state);

extern PFW_PLUGIN_OBJ_STATE 
                     *pfwLayerStateGet(PFW_PLUGIN_OBJ_STATE *compState);
extern PFW_OBJ       *pfwStackObjPfwGet (PFW_STACK_OBJ *pfwStackObj);
extern STATUS         pfwStackStatusGet (PFW_STACK_OBJ *stackObj, 
					 PFW_STACK_STATUS *status);
extern STATUS         pfwStackListShow (PFW_OBJ *pfwObj); 
extern STATUS         pfwStackShow (PFW_STACK_OBJ *pfwStackObj);

#ifdef __cplusplus
}
#endif

#endif /* __INCpfwStackh */

