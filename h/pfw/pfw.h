/* pfw.h - protocol framework header file */

/* Copyright 2003 Wind River Systems, Inc. */

/* 
modification history
--------------------
01p,15may03,ijm  removed adv_net.h
01n,26aug02,ijm  include adv_net.h for IPv6
01m,29jul02,ijm  moved dual stack declarations to kppp.h
01l,26jun02,ijm  added include files required to support dual stack
01k,23feb00,sj   removed extra comma after last PLANE_TYPE enum
01j,24nov99,koz  Added pfwIdGet and pfwObjGet routines
01i,22nov99,koz  Added defines for job data size and job queue size
01h,11oct99,koz  Made callback fuction expilicit in asynchronous function
                 signitures
01g,21sep99,koz  Removed callback interfaces from plugin object
01f,16sep99,sj   removed definition of PFW_PARAMETER_HANDLER
01e,08sep99,koz  Added framework private data pointer to plugin object
01d,26jul99,koz  Added pppJobAdd
01c,19jul99,koz  Added connectionShow, connectionDeleteReq and
                 connectionDeleteDone fields to GENERIC_PLUGIN_OBJ
01b,14jul99,koz  changed send and receive interfaces and added
                 _pppConnectionAdd ()
01a,09jul99,koz  written
*/

#ifndef __INCpfwh
#define __INCpfwh


#ifdef __cplusplus
extern "C" {
#endif

#include "memLib.h"
#include "netBufLib.h"

/* defines */

#define PFW_MAX_NAME_LENGTH 32 
#define PFW_LAYERED_PLANE_NUMBER 3 
#define PFW_MAX_JOB_DATA_SIZE 64
#define PFW_MIN_JOB_QUEUE_SIZE 64

/* typedefs */

/*
 * The protocol framework is broken into planes: user, comtrol, framing and     
 * management. The user, control and framing planes are broken into layers. 
 * The layers are broken into components.                   
 */

typedef void (*PFW_FUNC) (void *);

typedef enum pfwPlaneType 
    {
    FRAMING_PLANE,
    CONTROL_PLANE,
    DATA_PLANE,
    MANAGEMENT_PLANE
    } PFW_PLANE_TYPE;

typedef enum pfwPluginObjectType
    {
    PFW_LAYER_OBJ_TYPE,
    PFW_COMPONENT_OBJ_TYPE
    } PFW_PLUGIN_OBJ_TYPE;


typedef enum pfwParameterOperationType
    {
    PFW_PARAMETER_SET,
    PFW_PARAMETER_GET
    } PFW_PARAMETER_OPERATION_TYPE;

typedef unsigned int PFW_ID; 
typedef struct pfwObj PFW_OBJ; 
typedef struct pfwPluginObj PFW_PLUGIN_OBJ; 
typedef struct pfwProfileObj PFW_PROFILE_OBJ;
typedef struct pfwStackObj PFW_STACK_OBJ;
typedef struct pfwPrivateData PFW_PRIVATE_DATA;
typedef struct pfwPluginObjCallbacks PFW_PLUGIN_OBJ_CALLBACKS;

/* Protocol framework plugin object state structure */ 

typedef struct pfwPluginObjState
    {
    PFW_PLUGIN_OBJ *pluginObj; /* pointer to the plugin object */
    PFW_STACK_OBJ *stackObj;   /* pointer to the stack object */
    void *profileData;         /* pointer to the profile data of the object */
    void *stackData;           /* pointer to the stack data of the object */
    } PFW_PLUGIN_OBJ_STATE;

/*
 * When pfwStackAdd (or pfwStackDelete) is called, the framework creates a 
 * stack object and calls the asynchronous function stackAdd (or stackDelete)
 * of plugin objects. If the plugin object is a layer, it calls its own 
 * components stackAdd (or stackDelete) function. Eventually the components
 * call back their layers and the layers call back the framework to indicate
 * that the add (or delete) operation is completed. The callback function
 * structure for plugin objects is defined as follows.
 */

struct pfwPluginObjCallbacks
    {
    void (*stackAddComplete)          /* called by a plugin obj to inform */
         (PFW_PLUGIN_OBJ_CALLBACKS *, /* stack add processing is completed */
          PFW_PLUGIN_OBJ_STATE *);
    void (*stackDeleteComplete)       /* called by a plugin obj to inform */
         (PFW_PLUGIN_OBJ_CALLBACKS *, /* stack delete is completed */
          PFW_PLUGIN_OBJ_STATE *);
    void (*stackAddError)             /* called by a plugin obj to inform */
         (PFW_PLUGIN_OBJ_CALLBACKS *, /* an error happened during stack add */
	  PFW_PLUGIN_OBJ_STATE *);    
    void (*stackDeleteError)          /* called by a plugin obj to inform */
	 (PFW_PLUGIN_OBJ_CALLBACKS *, /* an error happened during delete */
	  PFW_PLUGIN_OBJ_STATE *);
    };
    
struct pfwPluginObj 
    {
    PFW_PRIVATE_DATA *privateData;   /* pointer to the framework private data */
				     /* only used by the framework */
    char name[PFW_MAX_NAME_LENGTH];  /* name */
    PFW_OBJ *pfwObj;                 /* pointer to the framework object */
    unsigned int profileDataSize;    /* size of the configuration data */
    unsigned int stackDataSize;      /* size of the stack data */
    STATUS (*profileDataConstruct)   /* called to initialize the profile data */
	   (PFW_OBJ *,               /* which is allocated by the framework */ 
	    void *profileData);       
    STATUS (*profileDataCopy)        /* called to copy profile data, arguments*/
	   (PFW_OBJ *, void *src,    /* are framework object, and source and */ 
	    void *dst);              /* destination profile pointers */
    STATUS (*profileDataDestruct)    /* called before the profile data is */
	   (PFW_OBJ *,               /* freed by the framework */
	    void *profileData);
    STATUS (*stackDataConstruct)     /* called to initialize the stack data */
	   (PFW_OBJ *,               /* which is allocated by the framework. */
	    void *stackData,         /* Stack and profile data pointers are */
            void *profileData);	     /* are passed to the routine */  
    STATUS (*stackDataDestruct)      /* called before stack data is freed by */
	   (PFW_OBJ *,               /* the framework. Arguments are pointers */
	    void *stackData,         /* to framework object, stack and profile*/
            void *profileData);	     /* data */
    STATUS (*interfaceBind)          /* plugin objects bind to their */       
	   (PFW_PLUGIN_OBJ *);       /* interfaces within this routine which */
				     /* is called by the framework only */
    STATUS (*stackAdd)               /* called when a stack is added to the */ 
	   (PFW_PLUGIN_OBJ_STATE *,  /* framework. Layer plugin objects are  */
	    PFW_PLUGIN_OBJ_CALLBACKS *); /* responsible for calling stackAdd */
				     /* routines of their components within */
				     /* this routine */
    STATUS (*stackDelete)            /* called while a stack is deleted. */ 
	   (PFW_PLUGIN_OBJ_STATE *); /* Layer plugin objects are responsible */
				     /* for calling stackDelete routine of */
				     /* their components within this routine */
    STATUS (*receive)                /* entry point for packets received from */
	   (PFW_PLUGIN_OBJ_STATE *,  /* the network */
	    M_BLK_ID *);             
    STATUS (*send)                   /* entry point for packets to be sent to */
	   (PFW_PLUGIN_OBJ_STATE *,  /* the network */ 
	    M_BLK_ID *);           
    STATUS (*stackDataShow)          /* called to print the stack data of the */
	   (PFW_PLUGIN_OBJ_STATE *); /* object for debuging purpose */
    };

/* function declarations */

extern PFW_OBJ              *pfwCreate(char *name, 
				       unsigned int controlJobQueueSize,
				       unsigned int dataJobQueueSize,
				       unsigned int controlTaskPriority,
				       unsigned int dataTaskPriority,
				       NET_POOL_ID  netPoolId,
				       PART_ID partId);
extern STATUS                pfwDelete(PFW_OBJ *pfwObj); 
extern STATUS                pfwSend (PFW_PLUGIN_OBJ_STATE *state, 
			              M_BLK_ID packet);
extern STATUS                pfwReceive (PFW_PLUGIN_OBJ_STATE 
					 *state, M_BLK_ID packet);
extern STATUS                pfwControlJobAdd (PFW_OBJ *pfwObj, 
					       PFW_FUNC pfwFunc, void *data,
			                       int size);  
extern STATUS                pfwDataJobAdd (PFW_OBJ *pfwObj, PFW_FUNC pfwFunc, 
					    void *data, int size);  
extern PFW_OBJ              *pfwObjGet (char *name);
extern PFW_ID                pfwIdGet (PFW_OBJ *pfwObj);
extern PFW_PLUGIN_OBJ       *pfwPluginObjGet (PFW_OBJ *pfwObj, char *name);
extern NET_POOL_ID           pfwNetPoolIdGet (PFW_OBJ *pfwObj);
extern void                  pfwPrintError (char *file, char *function, 
					    int line, PFW_OBJ *pfwObj, 
					    PFW_STACK_OBJ *stackObj, 
					    char *diagnose);
extern STATUS                pfwListShow (void);
extern STATUS                pfwShow (PFW_OBJ *pfwObj);
extern void                  pfwInit();

#ifdef __cplusplus
}
#endif

#endif /* __INCpfwh */

